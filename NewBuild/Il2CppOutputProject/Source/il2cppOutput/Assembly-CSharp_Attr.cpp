﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100;
// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC;
// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F;
// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA;
// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerBrowsableAttribute
struct DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// System.Diagnostics.DebuggerStepThroughAttribute
struct DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F;
// Microsoft.MixedReality.Toolkit.ExperimentalAttribute
struct ExperimentalAttribute_t3BDEE74CA71C892C46D946384DF1580F65514882;
// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* ARAnchorManager_t969330AB785F0DC41EF9F4390D77F7ABA30F7D0F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ARPlaneManager_t4700B0BC3E8B6CD35F8D925701C89A5A21DDBAD4_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ARPlaneMeshVisualizer_t4C981AECE943267AD0363C57F2F3D32273E932F2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ARPlane_t6336725EC68CE9029844CBE72A7FE7374AD74891_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* AudioLoFiEffect_tA5E7B3166B04B25ED8BEDBDC74161D90A979CADD_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* EyeTrackingTarget_tC638599E70EEE5CDC5E0A3EAEE31E158C221C0FA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* Interactable_tA1E638AA4938DBD6D4E2CCC082762389FC37F6E2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* PointerBehaviorControls_tD6A9DA05A72646CCCA2D127CFF34F006A1CB524F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* TMP_InputField_tD50B4F3E6822EAC2720FAED56B86E98183F61D59_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* TargetGroupCreatorRadial_t980AFA83ECED3B9654D24511FEB07DAC3118D9F1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* TextToSpeech_t918EA567E482DA071F2DE44C0AC7657FB8324DE9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAddToCounterU3Ed__44_t9030FB67DA45E920F753C3098D5B310AB504A561_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAnimatePropertiesU3Ed__6_t15A2A4AE8295FA8A4B453E8A9EDFE3A92E52D517_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAnimateVertexColorsU3Ed__10_t096128B3F18D445CA2BC7915A490343F5CB40293_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAnimateVertexColorsU3Ed__10_t80A253D2D22F54FF4777892E88E3C49CF40C0283_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAnimateVertexColorsU3Ed__11_tAC0216BA39C3BA66767A320D95DA1C392389AADE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAnimateVertexColorsU3Ed__11_tCF4677CE6128B93FB1DA8390DAA5B7BE85EB9B79_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAnimateVertexColorsU3Ed__3_t9EE128818D22D10FB704C73831F62C8D89E37DE6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CComputeHeatmapAtU3Ed__20_t57778DEFEFEA9FE64A0801F0D2B0B59F4398058F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCreateNewLogFileU3Ed__14_tE9FFFDE66A83EFB7925BBCF9E4D461E949829D7A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDeflateCoroutineU3Ed__17_t51DB69BD5458E3680AFD5FEB84AFF52D3EAEBEDA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDisplayTextMeshFloatingTextU3Ed__16_t45DFD3865E7CF6058313A502EEE4823924DF0ADF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDisplayTextMeshProFloatingTextU3Ed__15_t2C450C526125FE0890D8581F5A5E60EA4BEA4177_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDrawAtU3Ed__19_tF9694B76A7C548907397EF887A0AF5273444C7A1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CHandleButtonClickU3Ed__14_t6DC14582F351A525C8BBBA55020C98E0EC7871E7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CInflateCoroutineU3Ed__18_tDEF4EE704B4C848D5E10E502151CC371CBFF0164_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadInUWPU3Ed__21_t24C5FD3475E2D7A787B3874D4713F17B3B33C400_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadLogsU3Ed__21_t71C6912789A78D48E47827228CE2FB8377D5655F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadNewSceneU3Ed__6_t0E26EB5F61490AF9926F108D5E2AF8BFD3A41D9B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadNewSceneU3Ed__8_t6CC37138968FD381723B0793506AC695ED553EF9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CObjectGlowU3Ed__13_t2420E3BC6E90FDA3AE8047F3A262A9A0ECA0FC66_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CObjectGlowU3Ed__4_t3296F14F1E3002A882E4B18A0F910BCD3D8654E8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3COpenProgressIndicatorU3Ed__17_tCA040FFC07AD3E4D55D008542D434CD6D80B4388_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CPopulateHeatmapU3Ed__42_tEE23C9F4ED699F6431644212A126E0B0D685DA84_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRevealCharactersU3Ed__7_t14708E46E1A66B01F15CACFFEB7FB12A271C7B2C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRevealWordsU3Ed__8_tC3EA2AB6154D5D909B175AE61A9A717D42A02ADA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSaveLogsU3Ed__22_tA5C05FED470C577465EB69A3BAA3AE8EA4ADF0EE_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSequenceU3Ed__12_t056AE53EBAF2B9CF286C48D22F0C486DD67863C0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CSequenceU3Ed__12_t1CC8D60F8F8D94D9A497136AC0611C33C7913A73_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__10_t13C254D46B39AB253CE2B5B25479F038B54DBF01_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__10_tF18344D372584F361358E78B7DB8CCC4BBC449D1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__4_t488FF63DDCD818027484B14F0FF95DD298E72D62_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__4_t6C5C20C50A666BDE5941872858E901994274B948_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CTestAnchorsU3Ed__7_t60EB0100F609816FA418017838B859C42D501123_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CU3CRunSyncU3Eb__0U3Ed_t274F9BB76D6EF41A733C34C50B5394D62F080C76_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CU3CRunSyncU3Eb__0U3Ed_tA0DB1D14F5469B811AB9737E394CE355699216C8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUWP_FileExistsU3Ed__14_t39C80B99CD2D1B2F41522AEED4CFC6EEF2436CC8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUWP_LoadNewFileU3Ed__13_t77C4C264E32F46DFAE7A7B03CF7068C5D031CB71_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUWP_LoadU3Ed__12_t160E61830FAF366112986A0A34F22E9EE073E64D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUWP_ReadDataU3Ed__15_t2AB9B79A3BFC04423D02231A2D501DDCFF3FB0D8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUpdateListOverTimeU3Ed__33_t6F615CB841917C7A9D914665B70E25E5A2B78D61_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUpdateStatusU3Ed__43_t04A5CD192106000D7D3513F48FA80B8D35403EBB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForSpeechCommandU3Ed__14_t02FCE23E8825096EA938587649A5E60F44836910_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForSpeechCommandU3Ed__14_tF24D07507705A5DC8FE5B96BC8EF0EF1223BA8B9_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWarpTextU3Ed__7_t0A70FD8329D91B599DDB7EBCEAB30A1168404906_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWarpTextU3Ed__8_t5E5B765727F5E411B819B7A5855C31D921A4CD4F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CZoomAndStopU3Ed__78_t5DD82D6ECCE58A5515F5D95AABB14F522D219E46_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.AddComponentMenu
struct AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.AddComponentMenu::m_AddComponentMenu
	String_t* ___m_AddComponentMenu_0;
	// System.Int32 UnityEngine.AddComponentMenu::m_Ordering
	int32_t ___m_Ordering_1;

public:
	inline static int32_t get_offset_of_m_AddComponentMenu_0() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_AddComponentMenu_0)); }
	inline String_t* get_m_AddComponentMenu_0() const { return ___m_AddComponentMenu_0; }
	inline String_t** get_address_of_m_AddComponentMenu_0() { return &___m_AddComponentMenu_0; }
	inline void set_m_AddComponentMenu_0(String_t* value)
	{
		___m_AddComponentMenu_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AddComponentMenu_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Ordering_1() { return static_cast<int32_t>(offsetof(AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100, ___m_Ordering_1)); }
	inline int32_t get_m_Ordering_1() const { return ___m_Ordering_1; }
	inline int32_t* get_address_of_m_Ordering_1() { return &___m_Ordering_1; }
	inline void set_m_Ordering_1(int32_t value)
	{
		___m_Ordering_1 = value;
	}
};


// System.Reflection.AssemblyCopyrightAttribute
struct AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyCopyrightAttribute::m_copyright
	String_t* ___m_copyright_0;

public:
	inline static int32_t get_offset_of_m_copyright_0() { return static_cast<int32_t>(offsetof(AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC, ___m_copyright_0)); }
	inline String_t* get_m_copyright_0() const { return ___m_copyright_0; }
	inline String_t** get_address_of_m_copyright_0() { return &___m_copyright_0; }
	inline void set_m_copyright_0(String_t* value)
	{
		___m_copyright_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_copyright_0), (void*)value);
	}
};


// System.Reflection.AssemblyFileVersionAttribute
struct AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyFileVersionAttribute::_version
	String_t* ____version_0;

public:
	inline static int32_t get_offset_of__version_0() { return static_cast<int32_t>(offsetof(AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F, ____version_0)); }
	inline String_t* get__version_0() const { return ____version_0; }
	inline String_t** get_address_of__version_0() { return &____version_0; }
	inline void set__version_0(String_t* value)
	{
		____version_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____version_0), (void*)value);
	}
};


// System.Reflection.AssemblyProductAttribute
struct AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.AssemblyProductAttribute::m_product
	String_t* ___m_product_0;

public:
	inline static int32_t get_offset_of_m_product_0() { return static_cast<int32_t>(offsetof(AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA, ___m_product_0)); }
	inline String_t* get_m_product_0() const { return ___m_product_0; }
	inline String_t** get_address_of_m_product_0() { return &___m_product_0; }
	inline void set_m_product_0(String_t* value)
	{
		___m_product_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_product_0), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerStepThroughAttribute
struct DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.Serialization.FormerlySerializedAsAttribute
struct FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String UnityEngine.Serialization.FormerlySerializedAsAttribute::m_oldName
	String_t* ___m_oldName_0;

public:
	inline static int32_t get_offset_of_m_oldName_0() { return static_cast<int32_t>(offsetof(FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210, ___m_oldName_0)); }
	inline String_t* get_m_oldName_0() const { return ___m_oldName_0; }
	inline String_t** get_address_of_m_oldName_0() { return &___m_oldName_0; }
	inline void set_m_oldName_0(String_t* value)
	{
		___m_oldName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_oldName_0), (void*)value);
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.ObsoleteAttribute
struct ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.ObsoleteAttribute::_message
	String_t* ____message_0;
	// System.Boolean System.ObsoleteAttribute::_error
	bool ____error_1;

public:
	inline static int32_t get_offset_of__message_0() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____message_0)); }
	inline String_t* get__message_0() const { return ____message_0; }
	inline String_t** get_address_of__message_0() { return &____message_0; }
	inline void set__message_0(String_t* value)
	{
		____message_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_0), (void*)value);
	}

	inline static int32_t get_offset_of__error_1() { return static_cast<int32_t>(offsetof(ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671, ____error_1)); }
	inline bool get__error_1() const { return ____error_1; }
	inline bool* get_address_of__error_1() { return &____error_1; }
	inline void set__error_1(bool value)
	{
		____error_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Runtime.CompilerServices.AsyncStateMachineAttribute
struct AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggerBrowsableState
struct DebuggerBrowsableState_t2A824ECEB650CFABB239FD0918FCC88A09B45091 
{
public:
	// System.Int32 System.Diagnostics.DebuggerBrowsableState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggerBrowsableState_t2A824ECEB650CFABB239FD0918FCC88A09B45091, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Microsoft.MixedReality.Toolkit.ExperimentalAttribute
struct ExperimentalAttribute_t3BDEE74CA71C892C46D946384DF1580F65514882  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String Microsoft.MixedReality.Toolkit.ExperimentalAttribute::Text
	String_t* ___Text_0;

public:
	inline static int32_t get_offset_of_Text_0() { return static_cast<int32_t>(offsetof(ExperimentalAttribute_t3BDEE74CA71C892C46D946384DF1580F65514882, ___Text_0)); }
	inline String_t* get_Text_0() const { return ___Text_0; }
	inline String_t** get_address_of_Text_0() { return &___Text_0; }
	inline void set_Text_0(String_t* value)
	{
		___Text_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Text_0), (void*)value);
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// UnityEngine.TooltipAttribute
struct TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.TooltipAttribute::tooltip
	String_t* ___tooltip_0;

public:
	inline static int32_t get_offset_of_tooltip_0() { return static_cast<int32_t>(offsetof(TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B, ___tooltip_0)); }
	inline String_t* get_tooltip_0() const { return ___tooltip_0; }
	inline String_t** get_address_of_tooltip_0() { return &___tooltip_0; }
	inline void set_tooltip_0(String_t* value)
	{
		___tooltip_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___tooltip_0), (void*)value);
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Diagnostics.DebuggerBrowsableAttribute
struct DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggerBrowsableState System.Diagnostics.DebuggerBrowsableAttribute::state
	int32_t ___state_0;

public:
	inline static int32_t get_offset_of_state_0() { return static_cast<int32_t>(offsetof(DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53, ___state_0)); }
	inline int32_t get_state_0() const { return ___state_0; }
	inline int32_t* get_address_of_state_0() { return &___state_0; }
	inline void set_state_0(int32_t value)
	{
		___state_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Reflection.AssemblyProductAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8 (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * __this, String_t* ___product0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyCopyrightAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3 (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * __this, String_t* ___copyright0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Reflection.AssemblyFileVersionAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * __this, String_t* ___version0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type,System.Type,System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m1D311DE3AB4EF7FE3D80292644967218D92B00B6 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, Type_t * ___requiredComponent21, Type_t * ___requiredComponent32, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.TooltipAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042 (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * __this, String_t* ___tooltip0, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerBrowsableAttribute::.ctor(System.Diagnostics.DebuggerBrowsableState)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5 (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * __this, int32_t ___state0, const RuntimeMethod* method);
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549 (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * __this, String_t* ___menuName0, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void Microsoft.MixedReality.Toolkit.ExperimentalAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExperimentalAttribute__ctor_m4A28B36A1580F20C34A776EB137C53DF47A55F55 (ExperimentalAttribute_t3BDEE74CA71C892C46D946384DF1580F65514882 * __this, String_t* ___experimentalText0, const RuntimeMethod* method);
// System.Void UnityEngine.Serialization.FormerlySerializedAsAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * __this, String_t* ___oldName0, const RuntimeMethod* method);
// System.Void System.ObsoleteAttribute::.ctor(System.String,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * __this, String_t* ___message0, bool ___error1, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.AsyncStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerStepThroughAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerStepThroughAttribute__ctor_m2B40F019B0DF22CF7A815AAB3D2D027225D59D85 (DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA * tmp = (AssemblyProductAttribute_t6BB0E0F76C752E14A4C26B4D1E230019068601CA *)cache->attributes[0];
		AssemblyProductAttribute__ctor_m26DF1EBC1C86E7DA4786C66B44123899BE8DBCB8(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\xC2\xAE\x20\x4D\x69\x78\x65\x64\x20\x52\x65\x61\x6C\x69\x74\x79\x20\x54\x6F\x6F\x6C\x6B\x69\x74\x20\x45\x78\x61\x6D\x70\x6C\x65\x73"), NULL);
	}
	{
		AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC * tmp = (AssemblyCopyrightAttribute_tA6A09319EF50B48D962810032000DEE7B12904EC *)cache->attributes[1];
		AssemblyCopyrightAttribute__ctor_mB0B5F5C1A7A8B172289CC694E2711F07A37CE3F3(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x70\x79\x72\x69\x67\x68\x74\x20\xC2\xA9\x20\x4D\x69\x63\x72\x6F\x73\x6F\x66\x74\x20\x43\x6F\x72\x70\x6F\x72\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[2];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[3];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F * tmp = (AssemblyFileVersionAttribute_tCC1036D0566155DC5688D9230EF3C07D82A1896F *)cache->attributes[4];
		AssemblyFileVersionAttribute__ctor_mF855AEBC51CB72F4FF913499256741AE57B0F13D(tmp, il2cpp_codegen_string_new_wrapper("\x32\x2E\x37\x2E\x32\x2E\x30"), NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[5];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 263LL, NULL);
	}
}
static void ARFeatheredPlaneMeshVisualizer_t1327CCD0B254624AEE8A13935E9BDB1BECA4F304_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARPlaneMeshVisualizer_t4C981AECE943267AD0363C57F2F3D32273E932F2_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARPlane_t6336725EC68CE9029844CBE72A7FE7374AD74891_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m1D311DE3AB4EF7FE3D80292644967218D92B00B6(tmp, il2cpp_codegen_type_get_object(ARPlaneMeshVisualizer_t4C981AECE943267AD0363C57F2F3D32273E932F2_0_0_0_var), il2cpp_codegen_type_get_object(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var), il2cpp_codegen_type_get_object(ARPlane_t6336725EC68CE9029844CBE72A7FE7374AD74891_0_0_0_var), NULL);
	}
}
static void ARFeatheredPlaneMeshVisualizer_t1327CCD0B254624AEE8A13935E9BDB1BECA4F304_CustomAttributesCacheGenerator_m_FeatheringWidth(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x77\x69\x64\x74\x68\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x65\x78\x74\x75\x72\x65\x20\x66\x65\x61\x74\x68\x65\x72\x69\x6E\x67\x20\x28\x69\x6E\x20\x77\x6F\x72\x6C\x64\x20\x75\x6E\x69\x74\x73\x29\x2E"), NULL);
	}
}
static void AnchorCreator_t07D98CC500EA5D614148A24F0E8DCE5772B56BEC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARAnchorManager_t969330AB785F0DC41EF9F4390D77F7ABA30F7D0F_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARPlaneManager_t4700B0BC3E8B6CD35F8D925701C89A5A21DDBAD4_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARAnchorManager_t969330AB785F0DC41EF9F4390D77F7ABA30F7D0F_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARRaycastManager_t76CDCF27810673048562A85CAD0E3FEEB3D7328F_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[2];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ARPlaneManager_t4700B0BC3E8B6CD35F8D925701C89A5A21DDBAD4_0_0_0_var), NULL);
	}
}
static void AnchorCreator_t07D98CC500EA5D614148A24F0E8DCE5772B56BEC_CustomAttributesCacheGenerator_m_AnchorPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ButtonOrder_t245C2F5F9D03DCFED8D1322B301B24E0251CB8A4_CustomAttributesCacheGenerator_ButtonOrder_ObjectGlow_mFFACD508BE48F738C0B6F6670043CCD24E8D9A28(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CObjectGlowU3Ed__4_t3296F14F1E3002A882E4B18A0F910BCD3D8654E8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CObjectGlowU3Ed__4_t3296F14F1E3002A882E4B18A0F910BCD3D8654E8_0_0_0_var), NULL);
	}
}
static void U3CObjectGlowU3Ed__4_t3296F14F1E3002A882E4B18A0F910BCD3D8654E8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CObjectGlowU3Ed__4_t3296F14F1E3002A882E4B18A0F910BCD3D8654E8_CustomAttributesCacheGenerator_U3CObjectGlowU3Ed__4__ctor_mFFF411C1EF1D8807296A811F1F11ED26AD45D30D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CObjectGlowU3Ed__4_t3296F14F1E3002A882E4B18A0F910BCD3D8654E8_CustomAttributesCacheGenerator_U3CObjectGlowU3Ed__4_System_IDisposable_Dispose_mDDAB1BA6F77D08AA6F33C428CC442A4F6DDD67C0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CObjectGlowU3Ed__4_t3296F14F1E3002A882E4B18A0F910BCD3D8654E8_CustomAttributesCacheGenerator_U3CObjectGlowU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE1361ED0C0996CF5D8FFF29168BDB0CC68A62A85(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CObjectGlowU3Ed__4_t3296F14F1E3002A882E4B18A0F910BCD3D8654E8_CustomAttributesCacheGenerator_U3CObjectGlowU3Ed__4_System_Collections_IEnumerator_Reset_m49133519AB880CF16DE2DB819F3D2D174E7F9FBE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CObjectGlowU3Ed__4_t3296F14F1E3002A882E4B18A0F910BCD3D8654E8_CustomAttributesCacheGenerator_U3CObjectGlowU3Ed__4_System_Collections_IEnumerator_get_Current_mCF9EF965D329EA8A4BA33843423711DF62E304D6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void nailPulling_t604FA20D1C5869E4D43986026F4777C92C3F4B1A_CustomAttributesCacheGenerator_nailPulling_ObjectGlow_m671AFA1FB9F8625C9ED1C3EC4F1DFF56D06486EE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CObjectGlowU3Ed__13_t2420E3BC6E90FDA3AE8047F3A262A9A0ECA0FC66_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CObjectGlowU3Ed__13_t2420E3BC6E90FDA3AE8047F3A262A9A0ECA0FC66_0_0_0_var), NULL);
	}
}
static void U3CObjectGlowU3Ed__13_t2420E3BC6E90FDA3AE8047F3A262A9A0ECA0FC66_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CObjectGlowU3Ed__13_t2420E3BC6E90FDA3AE8047F3A262A9A0ECA0FC66_CustomAttributesCacheGenerator_U3CObjectGlowU3Ed__13__ctor_m36705611765608E1AB5180A973D57E7E35844585(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CObjectGlowU3Ed__13_t2420E3BC6E90FDA3AE8047F3A262A9A0ECA0FC66_CustomAttributesCacheGenerator_U3CObjectGlowU3Ed__13_System_IDisposable_Dispose_m5B1BAE725BBB3C5F71C815456DA1E0DF40A6D6B3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CObjectGlowU3Ed__13_t2420E3BC6E90FDA3AE8047F3A262A9A0ECA0FC66_CustomAttributesCacheGenerator_U3CObjectGlowU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m33227A5F49FE9DCF93DC806DF8654EEAF52C2F4B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CObjectGlowU3Ed__13_t2420E3BC6E90FDA3AE8047F3A262A9A0ECA0FC66_CustomAttributesCacheGenerator_U3CObjectGlowU3Ed__13_System_Collections_IEnumerator_Reset_m49EB597CCB8F39CD4041F0C029EED9BBDA1EC76B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CObjectGlowU3Ed__13_t2420E3BC6E90FDA3AE8047F3A262A9A0ECA0FC66_CustomAttributesCacheGenerator_U3CObjectGlowU3Ed__13_System_Collections_IEnumerator_get_Current_mC162CD86B5EF34EF47ADE853F214878EF5101AAA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DropdownSample_t9D3509399763FFB709ED3C29FFA59286C780C5CD_CustomAttributesCacheGenerator_text(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DropdownSample_t9D3509399763FFB709ED3C29FFA59286C780C5CD_CustomAttributesCacheGenerator_dropdownWithoutPlaceholder(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DropdownSample_t9D3509399763FFB709ED3C29FFA59286C780C5CD_CustomAttributesCacheGenerator_dropdownWithPlaceholder(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EnvMapAnimator_tF07513776BE9808D0FD92E8A773D4CB71679DBF9_CustomAttributesCacheGenerator_EnvMapAnimator_Start_m3432946DE1A3B40667B9D4CE90384F765C9A1788(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__4_t6C5C20C50A666BDE5941872858E901994274B948_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__4_t6C5C20C50A666BDE5941872858E901994274B948_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__4_t6C5C20C50A666BDE5941872858E901994274B948_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t6C5C20C50A666BDE5941872858E901994274B948_CustomAttributesCacheGenerator_U3CStartU3Ed__4__ctor_m0450ADD262F7838968EDCEA567F1046D7AA157E2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t6C5C20C50A666BDE5941872858E901994274B948_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_IDisposable_Dispose_m0E6287744F66D21C42C549B857E1A4A3BECDBC8A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t6C5C20C50A666BDE5941872858E901994274B948_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1A4BE78025454A74E21F9730A5269AE2E2461258(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t6C5C20C50A666BDE5941872858E901994274B948_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m912B63F0B462B11EAC1AA20D7DF921990F53BF82(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t6C5C20C50A666BDE5941872858E901994274B948_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m4C9D59B07EECBC8BF4F62D136E6A833D1CF3DB8E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TMP_TextEventHandler_t7D87D28BFB2254325AB5CF4D21DBDEF591DD3277_CustomAttributesCacheGenerator_m_OnCharacterSelection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TMP_TextEventHandler_t7D87D28BFB2254325AB5CF4D21DBDEF591DD3277_CustomAttributesCacheGenerator_m_OnSpriteSelection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TMP_TextEventHandler_t7D87D28BFB2254325AB5CF4D21DBDEF591DD3277_CustomAttributesCacheGenerator_m_OnWordSelection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TMP_TextEventHandler_t7D87D28BFB2254325AB5CF4D21DBDEF591DD3277_CustomAttributesCacheGenerator_m_OnLineSelection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TMP_TextEventHandler_t7D87D28BFB2254325AB5CF4D21DBDEF591DD3277_CustomAttributesCacheGenerator_m_OnLinkSelection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Benchmark01_tD75224AB8C8270F9AB89CDC6406DC065AD37B1C8_CustomAttributesCacheGenerator_Benchmark01_Start_mB7AE105F2B9A718DA0034E36DCB2DD5D3CE08E79(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__10_t13C254D46B39AB253CE2B5B25479F038B54DBF01_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__10_t13C254D46B39AB253CE2B5B25479F038B54DBF01_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__10_t13C254D46B39AB253CE2B5B25479F038B54DBF01_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t13C254D46B39AB253CE2B5B25479F038B54DBF01_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_mF134B42BDB16CFE45959D2238A883F3782D96A6B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t13C254D46B39AB253CE2B5B25479F038B54DBF01_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_m73EE988CEE5D33849663BBF874B890EFDF4D6C2C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t13C254D46B39AB253CE2B5B25479F038B54DBF01_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m40C85CAAA5F91860372CE6EC7DE2BC0651008A47(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t13C254D46B39AB253CE2B5B25479F038B54DBF01_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m9B240EF4945F412941C11524E6ADD12C718C96CB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t13C254D46B39AB253CE2B5B25479F038B54DBF01_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m99360A58385EF846DA9324753FFD814763486E44(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Benchmark01_UGUI_t5E4288018FE0F6AE5524F187F2B622E5F47B75A5_CustomAttributesCacheGenerator_Benchmark01_UGUI_Start_m34BF3D36C4148DD9B6AFF435494132D134DB1FEB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__10_tF18344D372584F361358E78B7DB8CCC4BBC449D1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__10_tF18344D372584F361358E78B7DB8CCC4BBC449D1_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__10_tF18344D372584F361358E78B7DB8CCC4BBC449D1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_tF18344D372584F361358E78B7DB8CCC4BBC449D1_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_mBD96F83AB86F1DADD0205D48950AD9472C49BB3C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_tF18344D372584F361358E78B7DB8CCC4BBC449D1_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_mA9BCB5EB21EAD06A6B3CAA5F637E4E3E61690A15(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_tF18344D372584F361358E78B7DB8CCC4BBC449D1_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m73A9DBD9986792A9C873E94820A327719DAD13B2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_tF18344D372584F361358E78B7DB8CCC4BBC449D1_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m8673C491BA40498DA72FCF82D0C35CDB2B2C1302(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_tF18344D372584F361358E78B7DB8CCC4BBC449D1_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m1403238260593386B4796ABC7CE5DE15710C066E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ShaderPropAnimator_tCF2361E34C8D8382EAA332EA1148C452860CA47D_CustomAttributesCacheGenerator_ShaderPropAnimator_AnimateProperties_m9635CDE20D007A404C2D8F09E0E3D474F83404BD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimatePropertiesU3Ed__6_t15A2A4AE8295FA8A4B453E8A9EDFE3A92E52D517_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimatePropertiesU3Ed__6_t15A2A4AE8295FA8A4B453E8A9EDFE3A92E52D517_0_0_0_var), NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t15A2A4AE8295FA8A4B453E8A9EDFE3A92E52D517_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t15A2A4AE8295FA8A4B453E8A9EDFE3A92E52D517_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6__ctor_m1EECC5609C55D8ACE8A015D095176613DF1CC729(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t15A2A4AE8295FA8A4B453E8A9EDFE3A92E52D517_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m899F6FDE18D7A082B51E81253115D11211B9E306(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t15A2A4AE8295FA8A4B453E8A9EDFE3A92E52D517_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB8A061DA69769417CF2DB406083A6B9FEC9216BD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t15A2A4AE8295FA8A4B453E8A9EDFE3A92E52D517_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_m96C640E17307465DC89EC0216DEC638A1434FB7F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t15A2A4AE8295FA8A4B453E8A9EDFE3A92E52D517_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m896EEE48086AA67D02FFA51CE50AF7184343D25B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SkewTextExample_t38AD31CC73BE149ECF960D04D8EE25D9513C8F99_CustomAttributesCacheGenerator_SkewTextExample_WarpText_mE1969FED4C69A06BF296EB3C57D655E70067C268(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWarpTextU3Ed__7_t0A70FD8329D91B599DDB7EBCEAB30A1168404906_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWarpTextU3Ed__7_t0A70FD8329D91B599DDB7EBCEAB30A1168404906_0_0_0_var), NULL);
	}
}
static void U3CWarpTextU3Ed__7_t0A70FD8329D91B599DDB7EBCEAB30A1168404906_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__7_t0A70FD8329D91B599DDB7EBCEAB30A1168404906_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7__ctor_m0C10F1442F3AFF382988656986D6043B8E15A143(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__7_t0A70FD8329D91B599DDB7EBCEAB30A1168404906_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_IDisposable_Dispose_mB08192042C7064309C074E7E2A091ACB1F4DEFD9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__7_t0A70FD8329D91B599DDB7EBCEAB30A1168404906_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBB2D90533557FC705A99905EA0757D55CF5BFEFF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__7_t0A70FD8329D91B599DDB7EBCEAB30A1168404906_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_m98CD8E7319A8FBC6CAFF79C5CBA0A29C7325967E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__7_t0A70FD8329D91B599DDB7EBCEAB30A1168404906_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m8E6A1A9AD6B87208DFEB11A4769BB5A79E90E1D3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TeleType_t9472171E255BE0707D857C416C12F6F4A7D7A972_CustomAttributesCacheGenerator_TeleType_Start_m37AB7AE2F364CEC3F6181C53750D67E08B07B63B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__4_t488FF63DDCD818027484B14F0FF95DD298E72D62_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__4_t488FF63DDCD818027484B14F0FF95DD298E72D62_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__4_t488FF63DDCD818027484B14F0FF95DD298E72D62_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t488FF63DDCD818027484B14F0FF95DD298E72D62_CustomAttributesCacheGenerator_U3CStartU3Ed__4__ctor_mE761C315080CB9D9ECC5E13541382FD8F4CF6A70(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t488FF63DDCD818027484B14F0FF95DD298E72D62_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_IDisposable_Dispose_m32D3DFDFEBC3CB106D26E913802FD00E209C12FF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t488FF63DDCD818027484B14F0FF95DD298E72D62_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC97655E37D3DEC57375D73AAE9E98572E8F81E27(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t488FF63DDCD818027484B14F0FF95DD298E72D62_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m8CDA19A57B0E471F332E9C43B122FEC63FB14FB4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t488FF63DDCD818027484B14F0FF95DD298E72D62_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mA02188F69F7BC357C743F33D3A643A7ECD0DBF69(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TextConsoleSimulator_t5D67BCF6EE238EA70D9CE2544BCC74D079B496A5_CustomAttributesCacheGenerator_TextConsoleSimulator_RevealCharacters_m270C2B31ECEBC901F31CC780869FF0BF9ADBFFCF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRevealCharactersU3Ed__7_t14708E46E1A66B01F15CACFFEB7FB12A271C7B2C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRevealCharactersU3Ed__7_t14708E46E1A66B01F15CACFFEB7FB12A271C7B2C_0_0_0_var), NULL);
	}
}
static void TextConsoleSimulator_t5D67BCF6EE238EA70D9CE2544BCC74D079B496A5_CustomAttributesCacheGenerator_TextConsoleSimulator_RevealWords_m2831A10B9CBE63D3B6A96764F84F98860FDDEA87(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRevealWordsU3Ed__8_tC3EA2AB6154D5D909B175AE61A9A717D42A02ADA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRevealWordsU3Ed__8_tC3EA2AB6154D5D909B175AE61A9A717D42A02ADA_0_0_0_var), NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t14708E46E1A66B01F15CACFFEB7FB12A271C7B2C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t14708E46E1A66B01F15CACFFEB7FB12A271C7B2C_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7__ctor_mC622CB57F2C8C2C5E5D6592FA61D37D9B2DD2F4E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t14708E46E1A66B01F15CACFFEB7FB12A271C7B2C_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m8581BA87F52BA3D2EC25328353D57DCEF2A57932(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t14708E46E1A66B01F15CACFFEB7FB12A271C7B2C_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F82F9A02F414D7485FD3AC3003ED386F5329009(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t14708E46E1A66B01F15CACFFEB7FB12A271C7B2C_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_m0A85EE143D0136BDEF293624A8B419EDFBCAC183(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t14708E46E1A66B01F15CACFFEB7FB12A271C7B2C_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m52987689D9C37472E3A5BB72A11F676BDA9708C8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tC3EA2AB6154D5D909B175AE61A9A717D42A02ADA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tC3EA2AB6154D5D909B175AE61A9A717D42A02ADA_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8__ctor_mF28D666A0DA72F5541416C9A900D63FFF1826D3C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tC3EA2AB6154D5D909B175AE61A9A717D42A02ADA_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m1BB12C3BC2792B0BFDB4BAE14EA6C876A64FEED2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tC3EA2AB6154D5D909B175AE61A9A717D42A02ADA_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m78510D10295A5465DF7FA6CC3062988A0A8C21F0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tC3EA2AB6154D5D909B175AE61A9A717D42A02ADA_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_m5DBF478AE8BD7F6A592EEAA1D0955006E69E36F6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tC3EA2AB6154D5D909B175AE61A9A717D42A02ADA_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mA8E2269E365F70178BEEC3156303BAB8F7115642(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TextMeshProFloatingText_t1B042E0159910D71C74E88EC19A866D5861502B9_CustomAttributesCacheGenerator_TextMeshProFloatingText_DisplayTextMeshProFloatingText_m214F7E4C44FAE3A3635443BD41F2645BEF83D07C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDisplayTextMeshProFloatingTextU3Ed__15_t2C450C526125FE0890D8581F5A5E60EA4BEA4177_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDisplayTextMeshProFloatingTextU3Ed__15_t2C450C526125FE0890D8581F5A5E60EA4BEA4177_0_0_0_var), NULL);
	}
}
static void TextMeshProFloatingText_t1B042E0159910D71C74E88EC19A866D5861502B9_CustomAttributesCacheGenerator_TextMeshProFloatingText_DisplayTextMeshFloatingText_m634EC4DD456EB9596D9ACF27710D5F28095EEDA1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDisplayTextMeshFloatingTextU3Ed__16_t45DFD3865E7CF6058313A502EEE4823924DF0ADF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDisplayTextMeshFloatingTextU3Ed__16_t45DFD3865E7CF6058313A502EEE4823924DF0ADF_0_0_0_var), NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_t2C450C526125FE0890D8581F5A5E60EA4BEA4177_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_t2C450C526125FE0890D8581F5A5E60EA4BEA4177_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m328AB7CF0E2A3A56BC77FC55140903098CF87CFE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_t2C450C526125FE0890D8581F5A5E60EA4BEA4177_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m052F727B1032933B1DD2CABD4B44CFBA23662047(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_t2C450C526125FE0890D8581F5A5E60EA4BEA4177_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7C348F98BC1901EB30404695B7548067947121D8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_t2C450C526125FE0890D8581F5A5E60EA4BEA4177_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_m62DBBFD80A8879E56C9BAB8B279CADA3B0D5E95F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_t2C450C526125FE0890D8581F5A5E60EA4BEA4177_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mC6FA041905BD4EEE785DFA63B708CD601C8C0C2A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_t45DFD3865E7CF6058313A502EEE4823924DF0ADF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_t45DFD3865E7CF6058313A502EEE4823924DF0ADF_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m27613E638D919598A9C8C2DDCA2C157E69A7D399(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_t45DFD3865E7CF6058313A502EEE4823924DF0ADF_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_mBB9A19DD082FE5F45831A42F6ABFF7310B783653(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_t45DFD3865E7CF6058313A502EEE4823924DF0ADF_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m033D07D6C2A22DE4239E5B7F15C7535C1AE709F9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_t45DFD3865E7CF6058313A502EEE4823924DF0ADF_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mB883AE948B2D5B213E28C2C08FC4FAC846090596(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_t45DFD3865E7CF6058313A502EEE4823924DF0ADF_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_mDB3B40C77FDD1B4D3FCB62E34C424BCB9F61715D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VertexColorCycler_tE1208E27D8B0ED521EA4AB48577E80F47CC8391E_CustomAttributesCacheGenerator_VertexColorCycler_AnimateVertexColors_m4EE5EFAAA9D9F65C811848AE36551E8137B3FDF1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimateVertexColorsU3Ed__3_t9EE128818D22D10FB704C73831F62C8D89E37DE6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimateVertexColorsU3Ed__3_t9EE128818D22D10FB704C73831F62C8D89E37DE6_0_0_0_var), NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_t9EE128818D22D10FB704C73831F62C8D89E37DE6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_t9EE128818D22D10FB704C73831F62C8D89E37DE6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3__ctor_m69255FD61F9CB7F7AFD1468325DB6EDA90C3DD83(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_t9EE128818D22D10FB704C73831F62C8D89E37DE6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m9E8F17AF5EB46169BCFBA2152681B7A4A8B870D9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_t9EE128818D22D10FB704C73831F62C8D89E37DE6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF8FBC07125C99541937A71DE72A3348131A4FFD9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_t9EE128818D22D10FB704C73831F62C8D89E37DE6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m176E827425D6A6561CD6A941517F502A1AFD610E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_t9EE128818D22D10FB704C73831F62C8D89E37DE6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m25C8138AE53031CDDF90D50063B1286ED44F4266(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VertexJitter_t3EE0DD43F99BDEC17600BA011E1C15A90655ABBB_CustomAttributesCacheGenerator_VertexJitter_AnimateVertexColors_m76363C66669DF0DD2070901DEFFF39D24C242B90(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimateVertexColorsU3Ed__11_tAC0216BA39C3BA66767A320D95DA1C392389AADE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimateVertexColorsU3Ed__11_tAC0216BA39C3BA66767A320D95DA1C392389AADE_0_0_0_var), NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tAC0216BA39C3BA66767A320D95DA1C392389AADE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tAC0216BA39C3BA66767A320D95DA1C392389AADE_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11__ctor_m896A9095AFF2B1F7D823B4D14CFC2527EB3152CC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tAC0216BA39C3BA66767A320D95DA1C392389AADE_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_mD72B273D37EA2543C5C0B88211549EC0FA3E720D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tAC0216BA39C3BA66767A320D95DA1C392389AADE_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m36CA4792794DA1C9908A9C9FC4874ACF82804AB8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tAC0216BA39C3BA66767A320D95DA1C392389AADE_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m627446844CA02880583B92428889EA9D9CEC04CC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tAC0216BA39C3BA66767A320D95DA1C392389AADE_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m7669B30A4AC65F85F8EBA4BDC640F43B74C478EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VertexShakeA_t7B2BEE781CFAD3E36A50BF75015504A018C5F2E0_CustomAttributesCacheGenerator_VertexShakeA_AnimateVertexColors_mAEF171DDF17D8B42259C3C2F6486E550DE014BF1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimateVertexColorsU3Ed__11_tCF4677CE6128B93FB1DA8390DAA5B7BE85EB9B79_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimateVertexColorsU3Ed__11_tCF4677CE6128B93FB1DA8390DAA5B7BE85EB9B79_0_0_0_var), NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tCF4677CE6128B93FB1DA8390DAA5B7BE85EB9B79_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tCF4677CE6128B93FB1DA8390DAA5B7BE85EB9B79_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11__ctor_mCAC1B99791AE7DD5F3F96BC4CB7645898301C2CE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tCF4677CE6128B93FB1DA8390DAA5B7BE85EB9B79_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m4EEC65E3452BBDCB67C9ABC7409C73FEACF9BC2F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tCF4677CE6128B93FB1DA8390DAA5B7BE85EB9B79_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBC451FD1BCDDB66EAEAA07F399199A9DC63AFB23(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tCF4677CE6128B93FB1DA8390DAA5B7BE85EB9B79_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m74DE64545E3327D66FB6C069A5190BFC0FF683AC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tCF4677CE6128B93FB1DA8390DAA5B7BE85EB9B79_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_mD3DFEB94E1B0A0153083715008C670364D32C097(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VertexShakeB_t67B14E5277C675D533503A2D80A1633115299391_CustomAttributesCacheGenerator_VertexShakeB_AnimateVertexColors_m4C64F1552C5539AE431814B8DE4673B764D26576(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimateVertexColorsU3Ed__10_t096128B3F18D445CA2BC7915A490343F5CB40293_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimateVertexColorsU3Ed__10_t096128B3F18D445CA2BC7915A490343F5CB40293_0_0_0_var), NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t096128B3F18D445CA2BC7915A490343F5CB40293_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t096128B3F18D445CA2BC7915A490343F5CB40293_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10__ctor_mD5440310F221693B051892C32E5825ACF2910B0C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t096128B3F18D445CA2BC7915A490343F5CB40293_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m85345564110FD83A156AE3D768E78E41E8D464C0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t096128B3F18D445CA2BC7915A490343F5CB40293_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C64AB0762FDA81B311143C78B0B7314A05B40F7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t096128B3F18D445CA2BC7915A490343F5CB40293_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mD0C3021D6DEB76A541E9A6F93CF17691D62B417C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t096128B3F18D445CA2BC7915A490343F5CB40293_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mAD8B239C2AFA0DAE135EED3CFB49F0816D150048(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VertexZoom_t8A88A990BFB605774C93B0940F6A258FCD5710D2_CustomAttributesCacheGenerator_VertexZoom_AnimateVertexColors_m7373CF7A5AC35CDBC74CE80BD56B04123A91C0C7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimateVertexColorsU3Ed__10_t80A253D2D22F54FF4777892E88E3C49CF40C0283_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimateVertexColorsU3Ed__10_t80A253D2D22F54FF4777892E88E3C49CF40C0283_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass10_0_t45962AFEC260B93E3091482654F75F63398B1ADE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t80A253D2D22F54FF4777892E88E3C49CF40C0283_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t80A253D2D22F54FF4777892E88E3C49CF40C0283_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10__ctor_mA2C6461F1E88C3131B5C4E6FFD7C75771C73D09A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t80A253D2D22F54FF4777892E88E3C49CF40C0283_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mE5843C784E5D25537D577419267FAA093836BBA6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t80A253D2D22F54FF4777892E88E3C49CF40C0283_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m02F8B326B53D4BB33A54254ED664BA615DAC4E24(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t80A253D2D22F54FF4777892E88E3C49CF40C0283_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m06892291F75733C0A0525F44054E4CEA8D9BD58C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t80A253D2D22F54FF4777892E88E3C49CF40C0283_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m2F4836BAA267C17023363704B71B53454934A6E0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WarpTextExample_tC5297C8412EF87B266159FECD34E06BFB474CC74_CustomAttributesCacheGenerator_WarpTextExample_WarpText_m453D126F0994CE92A6A3D2F620B81BADD85F7DBF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWarpTextU3Ed__8_t5E5B765727F5E411B819B7A5855C31D921A4CD4F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWarpTextU3Ed__8_t5E5B765727F5E411B819B7A5855C31D921A4CD4F_0_0_0_var), NULL);
	}
}
static void U3CWarpTextU3Ed__8_t5E5B765727F5E411B819B7A5855C31D921A4CD4F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__8_t5E5B765727F5E411B819B7A5855C31D921A4CD4F_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8__ctor_m76DC34194319F4AF8CFB96066F3764402BEB5279(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__8_t5E5B765727F5E411B819B7A5855C31D921A4CD4F_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_IDisposable_Dispose_mE963A8C458C7424814637CF46BBDDEC46CAD7F9C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__8_t5E5B765727F5E411B819B7A5855C31D921A4CD4F_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m32CF5062AEB89B0ECAE1021AF873CD799923FDFF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__8_t5E5B765727F5E411B819B7A5855C31D921A4CD4F_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m46782447641AAFD07BA0D6D363B5FA2FC2282837(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__8_t5E5B765727F5E411B819B7A5855C31D921A4CD4F_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_mDB18401834B9740BFAECF2C624EF0C8ACED4E9D2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BaseDwellSample_t1D103FDA990DC40C609B6A4AA034D14B17F825E2_CustomAttributesCacheGenerator_dwellVisualImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BaseDwellSample_t1D103FDA990DC40C609B6A4AA034D14B17F825E2_CustomAttributesCacheGenerator_targetButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BaseDwellSample_t1D103FDA990DC40C609B6A4AA034D14B17F825E2_CustomAttributesCacheGenerator_U3CDwellHandlerU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void BaseDwellSample_t1D103FDA990DC40C609B6A4AA034D14B17F825E2_CustomAttributesCacheGenerator_U3CIsDwellingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[0];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[1];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseDwellSample_t1D103FDA990DC40C609B6A4AA034D14B17F825E2_CustomAttributesCacheGenerator_BaseDwellSample_get_DwellHandler_mC114D08153104E01605702BD59AF73888F1E6CE9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseDwellSample_t1D103FDA990DC40C609B6A4AA034D14B17F825E2_CustomAttributesCacheGenerator_BaseDwellSample_set_DwellHandler_m73E008459D501A72BB8C968B6BAF6575D5265348(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseDwellSample_t1D103FDA990DC40C609B6A4AA034D14B17F825E2_CustomAttributesCacheGenerator_BaseDwellSample_get_IsDwelling_m9E0E0CD2600EB5782A736E1C8EAC08773126F873(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseDwellSample_t1D103FDA990DC40C609B6A4AA034D14B17F825E2_CustomAttributesCacheGenerator_BaseDwellSample_set_IsDwelling_m18391541F4D24E4F07E99244BE58C23997F65E4A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InstantDwellSample_t906C3DC5D27F9829445E4EC0BF48F655FC0EE90E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x49\x6E\x73\x74\x61\x6E\x74\x44\x77\x65\x6C\x6C\x53\x61\x6D\x70\x6C\x65"), NULL);
	}
}
static void InstantDwellSample_t906C3DC5D27F9829445E4EC0BF48F655FC0EE90E_CustomAttributesCacheGenerator_listItems(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ListItemDwell_tD65C3DD6A4A9E19185BB0700A5D4D24265159866_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x4C\x69\x73\x74\x49\x74\x65\x6D\x44\x77\x65\x6C\x6C"), NULL);
	}
}
static void ListItemDwell_tD65C3DD6A4A9E19185BB0700A5D4D24265159866_CustomAttributesCacheGenerator_itemName(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ListItemDwell_tD65C3DD6A4A9E19185BB0700A5D4D24265159866_CustomAttributesCacheGenerator_displayLabel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ToggleDwellSample_t3A51ABF9023DAB4F055EBCB63AF8A943EBB42762_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x54\x6F\x67\x67\x6C\x65\x44\x77\x65\x6C\x6C\x53\x61\x6D\x70\x6C\x65"), NULL);
	}
}
static void ToggleDwellSample_t3A51ABF9023DAB4F055EBCB63AF8A943EBB42762_CustomAttributesCacheGenerator_dwellStatus(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ToggleDwellSample_t3A51ABF9023DAB4F055EBCB63AF8A943EBB42762_CustomAttributesCacheGenerator_buttonBackground(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ToggleDwellSample_t3A51ABF9023DAB4F055EBCB63AF8A943EBB42762_CustomAttributesCacheGenerator_dwellOnColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ToggleDwellSample_t3A51ABF9023DAB4F055EBCB63AF8A943EBB42762_CustomAttributesCacheGenerator_dwellOffColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ToggleDwellSample_t3A51ABF9023DAB4F055EBCB63AF8A943EBB42762_CustomAttributesCacheGenerator_dwellIntendedColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ToggleDwellSample_t3A51ABF9023DAB4F055EBCB63AF8A943EBB42762_CustomAttributesCacheGenerator_dwellVisualCancelDurationInFrames(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_SavedSceneNamePrefix(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_InstantiatePrefabs(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_InstantiatedPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_InstantiatedParent(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_autoUpdateToggle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49"), NULL);
	}
}
static void DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_quadsToggle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_inferRegionsToggle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_meshesToggle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_maskToggle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_platformToggle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_wallToggle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_floorToggle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_ceilingToggle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_worldToggle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_completelyInferred(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_backgroundToggle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void KeyboardTest_t3BB2F6627AE6806F38FC3BB29D9CF3DC1DCC99FC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TMP_InputField_tD50B4F3E6822EAC2720FAED56B86E98183F61D59_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(TMP_InputField_tD50B4F3E6822EAC2720FAED56B86E98183F61D59_0_0_0_var), NULL);
	}
}
static void KeyboardTest_t3BB2F6627AE6806F38FC3BB29D9CF3DC1DCC99FC_CustomAttributesCacheGenerator_keyboard(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		ExperimentalAttribute_t3BDEE74CA71C892C46D946384DF1580F65514882 * tmp = (ExperimentalAttribute_t3BDEE74CA71C892C46D946384DF1580F65514882 *)cache->attributes[1];
		ExperimentalAttribute__ctor_m4A28B36A1580F20C34A776EB137C53DF47A55F55(tmp, il2cpp_codegen_string_new_wrapper("\x3C\x62\x3E\x3C\x63\x6F\x6C\x6F\x72\x3D\x79\x65\x6C\x6C\x6F\x77\x3E\x54\x68\x69\x73\x20\x69\x73\x20\x61\x6E\x20\x65\x78\x70\x65\x72\x69\x6D\x65\x6E\x74\x61\x6C\x20\x66\x65\x61\x74\x75\x72\x65\x2E\x3C\x2F\x63\x6F\x6C\x6F\x72\x3E\x3C\x2F\x62\x3E\xA\x50\x61\x72\x74\x73\x20\x6F\x66\x20\x74\x68\x65\x20\x4D\x52\x54\x4B\x20\x61\x70\x70\x65\x61\x72\x20\x74\x6F\x20\x68\x61\x76\x65\x20\x61\x20\x6C\x6F\x74\x20\x6F\x66\x20\x76\x61\x6C\x75\x65\x20\x65\x76\x65\x6E\x20\x69\x66\x20\x74\x68\x65\x20\x64\x65\x74\x61\x69\x6C\x73\x20\x68\x61\x76\x65\x6E\xE2\x80\x99\x74\x20\x66\x75\x6C\x6C\x79\x20\x62\x65\x65\x6E\x20\x66\x6C\x65\x73\x68\x65\x64\x20\x6F\x75\x74\x2E\x20\x46\x6F\x72\x20\x74\x68\x65\x73\x65\x20\x74\x79\x70\x65\x73\x20\x6F\x66\x20\x66\x65\x61\x74\x75\x72\x65\x73\x2C\x20\x77\x65\x20\x77\x61\x6E\x74\x20\x74\x68\x65\x20\x63\x6F\x6D\x6D\x75\x6E\x69\x74\x79\x20\x74\x6F\x20\x73\x65\x65\x20\x74\x68\x65\x6D\x20\x61\x6E\x64\x20\x67\x65\x74\x20\x76\x61\x6C\x75\x65\x20\x6F\x75\x74\x20\x6F\x66\x20\x74\x68\x65\x6D\x20\x65\x61\x72\x6C\x79\x2E\x20\x42\x65\x63\x61\x75\x73\x65\x20\x74\x68\x65\x79\x20\x61\x72\x65\x20\x65\x61\x72\x6C\x79\x20\x69\x6E\x20\x74\x68\x65\x20\x63\x79\x63\x6C\x65\x2C\x20\x77\x65\x20\x6C\x61\x62\x65\x6C\x20\x74\x68\x65\x6D\x20\x61\x73\x20\x65\x78\x70\x65\x72\x69\x6D\x65\x6E\x74\x61\x6C\x20\x74\x6F\x20\x69\x6E\x64\x69\x63\x61\x74\x65\x20\x74\x68\x61\x74\x20\x74\x68\x65\x79\x20\x61\x72\x65\x20\x73\x74\x69\x6C\x6C\x20\x65\x76\x6F\x6C\x76\x69\x6E\x67\x2C\x20\x61\x6E\x64\x20\x73\x75\x62\x6A\x65\x63\x74\x20\x74\x6F\x20\x63\x68\x61\x6E\x67\x65\x20\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x2E"), NULL);
	}
}
static void MicrophoneAmplitudeDemo_t3B38ACC364A3D3FBAA5E1ED48E021994F7843353_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B_0_0_0_var), NULL);
	}
}
static void GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x47\x65\x73\x74\x75\x72\x65\x54\x65\x73\x74\x65\x72"), NULL);
	}
}
static void GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_holdIndicator(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x6C\x64\x49\x6E\x64\x69\x63\x61\x74\x6F\x72"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x47\x65\x73\x74\x75\x72\x65\x20\x69\x6E\x64\x69\x63\x61\x74\x6F\x72\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_manipulationIndicator(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x6E\x69\x70\x75\x6C\x61\x74\x69\x6F\x6E\x49\x6E\x64\x69\x63\x61\x74\x6F\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_navigationIndicator(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x76\x69\x67\x61\x74\x69\x6F\x6E\x49\x6E\x64\x69\x63\x61\x74\x6F\x72"), NULL);
	}
}
static void GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_selectIndicator(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x6C\x65\x63\x74\x49\x6E\x64\x69\x63\x61\x74\x6F\x72"), NULL);
	}
}
static void GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_defaultMaterial(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x47\x65\x73\x74\x75\x72\x65\x20\x6D\x61\x74\x65\x72\x69\x61\x6C\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[2];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x66\x61\x75\x6C\x74\x4D\x61\x74\x65\x72\x69\x61\x6C"), NULL);
	}
}
static void GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_holdMaterial(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x6C\x64\x4D\x61\x74\x65\x72\x69\x61\x6C"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_manipulationMaterial(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x6E\x69\x70\x75\x6C\x61\x74\x69\x6F\x6E\x4D\x61\x74\x65\x72\x69\x61\x6C"), NULL);
	}
}
static void GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_navigationMaterial(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x76\x69\x67\x61\x74\x69\x6F\x6E\x4D\x61\x74\x65\x72\x69\x61\x6C"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_selectMaterial(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x6C\x65\x63\x74\x4D\x61\x74\x65\x72\x69\x61\x6C"), NULL);
	}
}
static void GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_railsAxisX(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x69\x6C\x73\x41\x78\x69\x73\x58"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[2];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x69\x6C\x73\x20\x61\x78\x69\x73\x20\x76\x69\x73\x75\x61\x6C\x73"), NULL);
	}
}
static void GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_railsAxisY(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x69\x6C\x73\x41\x78\x69\x73\x59"), NULL);
	}
}
static void GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_railsAxisZ(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[1];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x69\x6C\x73\x41\x78\x69\x73\x5A"), NULL);
	}
}
static void GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_holdAction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x70\x70\x65\x64\x20\x67\x65\x73\x74\x75\x72\x65\x20\x69\x6E\x70\x75\x74\x20\x61\x63\x74\x69\x6F\x6E\x73"), NULL);
	}
}
static void GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_navigationAction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_manipulationAction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_tapAction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GrabTouchExample_t68D2275E78565CAEB49DEEEC97441C462D269826_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x69\x73\x20\x6E\x6F\x20\x6C\x6F\x6E\x67\x65\x72\x20\x73\x75\x70\x70\x6F\x72\x74\x65\x64"), true, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x4F\x62\x73\x6F\x6C\x65\x74\x65\x2F\x47\x72\x61\x62\x54\x6F\x75\x63\x68\x45\x78\x61\x6D\x70\x6C\x65"), NULL);
	}
}
static void GrabTouchExample_t68D2275E78565CAEB49DEEEC97441C462D269826_CustomAttributesCacheGenerator_grabAction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RotateWithPan_t87849F9AC083BCBE4EA3513254033E0309E2B1EF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x52\x6F\x74\x61\x74\x65\x57\x69\x74\x68\x50\x61\x6E"), NULL);
	}
}
static void RotateWithPan_t87849F9AC083BCBE4EA3513254033E0309E2B1EF_CustomAttributesCacheGenerator_panInputSource(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x70\x61\x6E\x20\x6F\x62\x6A\x65\x63\x74\x20\x74\x6F\x20\x6C\x69\x73\x74\x65\x6E\x20\x74\x6F\x20\x65\x76\x65\x6E\x74\x73\x20\x66\x72\x6F\x6D\x2E\x20\x49\x66\x20\x6E\x75\x6C\x6C\x2C\x20\x77\x69\x6C\x6C\x20\x6C\x69\x73\x74\x65\x6E\x20\x6F\x6E\x20\x74\x68\x69\x73\x20\x6F\x62\x6A\x65\x63\x74\x20\x6F\x72\x20\x6C\x6F\x6F\x6B\x20\x66\x6F\x72\x20\x66\x69\x72\x73\x74\x20\x64\x65\x73\x63\x65\x6E\x64\x61\x6E\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WidgetElasticDemo_tE623A71620B902B821CD05D322B2CFC29207C900_CustomAttributesCacheGenerator_WidgetElasticDemo_DeflateCoroutine_mAFCF1FE1810464E142CA6140D747438EE7CE4BC7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDeflateCoroutineU3Ed__17_t51DB69BD5458E3680AFD5FEB84AFF52D3EAEBEDA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDeflateCoroutineU3Ed__17_t51DB69BD5458E3680AFD5FEB84AFF52D3EAEBEDA_0_0_0_var), NULL);
	}
}
static void WidgetElasticDemo_tE623A71620B902B821CD05D322B2CFC29207C900_CustomAttributesCacheGenerator_WidgetElasticDemo_InflateCoroutine_m19FE65F593F306036730601D94D30F2307A02EAD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CInflateCoroutineU3Ed__18_tDEF4EE704B4C848D5E10E502151CC371CBFF0164_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CInflateCoroutineU3Ed__18_tDEF4EE704B4C848D5E10E502151CC371CBFF0164_0_0_0_var), NULL);
	}
}
static void U3CDeflateCoroutineU3Ed__17_t51DB69BD5458E3680AFD5FEB84AFF52D3EAEBEDA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDeflateCoroutineU3Ed__17_t51DB69BD5458E3680AFD5FEB84AFF52D3EAEBEDA_CustomAttributesCacheGenerator_U3CDeflateCoroutineU3Ed__17__ctor_mCE59BFC0F867BA58A8EBFA0509AE6353BCAFBF40(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDeflateCoroutineU3Ed__17_t51DB69BD5458E3680AFD5FEB84AFF52D3EAEBEDA_CustomAttributesCacheGenerator_U3CDeflateCoroutineU3Ed__17_System_IDisposable_Dispose_mFF3E272F263C9873B5972D43624F29AE8917EF6E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDeflateCoroutineU3Ed__17_t51DB69BD5458E3680AFD5FEB84AFF52D3EAEBEDA_CustomAttributesCacheGenerator_U3CDeflateCoroutineU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD3D31C0D3D48BF83F1024764C3BFBEEE1504B054(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDeflateCoroutineU3Ed__17_t51DB69BD5458E3680AFD5FEB84AFF52D3EAEBEDA_CustomAttributesCacheGenerator_U3CDeflateCoroutineU3Ed__17_System_Collections_IEnumerator_Reset_m6432C3B69E4C3EC48FD04166F910F93750C361C9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDeflateCoroutineU3Ed__17_t51DB69BD5458E3680AFD5FEB84AFF52D3EAEBEDA_CustomAttributesCacheGenerator_U3CDeflateCoroutineU3Ed__17_System_Collections_IEnumerator_get_Current_m85DEF7CC0E93830CB2218DB8E323FFDCA2B572A9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInflateCoroutineU3Ed__18_tDEF4EE704B4C848D5E10E502151CC371CBFF0164_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CInflateCoroutineU3Ed__18_tDEF4EE704B4C848D5E10E502151CC371CBFF0164_CustomAttributesCacheGenerator_U3CInflateCoroutineU3Ed__18__ctor_m004C3F0C3E72B5B25663F8B4AC86AB8570EBDE07(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInflateCoroutineU3Ed__18_tDEF4EE704B4C848D5E10E502151CC371CBFF0164_CustomAttributesCacheGenerator_U3CInflateCoroutineU3Ed__18_System_IDisposable_Dispose_m175FA6717CE8675FEC96F60413B3A08ED6C9B056(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInflateCoroutineU3Ed__18_tDEF4EE704B4C848D5E10E502151CC371CBFF0164_CustomAttributesCacheGenerator_U3CInflateCoroutineU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m440D830708196E0CE60BEC34E2B1F28D3CD40A2E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInflateCoroutineU3Ed__18_tDEF4EE704B4C848D5E10E502151CC371CBFF0164_CustomAttributesCacheGenerator_U3CInflateCoroutineU3Ed__18_System_Collections_IEnumerator_Reset_mB20C406EC02C71E87A765F0EC617CE421FA3FB5E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CInflateCoroutineU3Ed__18_tDEF4EE704B4C848D5E10E502151CC371CBFF0164_CustomAttributesCacheGenerator_U3CInflateCoroutineU3Ed__18_System_Collections_IEnumerator_get_Current_m5C619A4BDCB54466183141A0CD0E43E4BD23A4C7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void DialogExampleController_tC224B81392615531E8383FFFE7CC720C58C7500D_CustomAttributesCacheGenerator_dialogPrefabLarge(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x69\x67\x6E\x20\x44\x69\x61\x6C\x6F\x67\x4C\x61\x72\x67\x65\x5F\x31\x39\x32\x78\x31\x39\x32\x2E\x70\x72\x65\x66\x61\x62"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DialogExampleController_tC224B81392615531E8383FFFE7CC720C58C7500D_CustomAttributesCacheGenerator_dialogPrefabMedium(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x69\x67\x6E\x20\x44\x69\x61\x6C\x6F\x67\x4D\x65\x64\x69\x75\x6D\x65\x5F\x31\x39\x32\x78\x31\x32\x38\x2E\x70\x72\x65\x66\x61\x62"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DialogExampleController_tC224B81392615531E8383FFFE7CC720C58C7500D_CustomAttributesCacheGenerator_dialogPrefabSmall(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x69\x67\x6E\x20\x44\x69\x61\x6C\x6F\x67\x53\x6D\x61\x6C\x6C\x5F\x31\x39\x32\x78\x39\x36\x2E\x70\x72\x65\x66\x61\x62"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoFiFilterSelection_tE9D58D7964B6A6B2F4A4B53BC9E3F0538123279E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AudioLoFiEffect_tA5E7B3166B04B25ED8BEDBDC74161D90A979CADD_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x4C\x6F\x46\x69\x46\x69\x6C\x74\x65\x72\x53\x65\x6C\x65\x63\x74\x69\x6F\x6E"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(AudioLoFiEffect_tA5E7B3166B04B25ED8BEDBDC74161D90A979CADD_0_0_0_var), NULL);
	}
}
static void LoFiFilterSelection_tE9D58D7964B6A6B2F4A4B53BC9E3F0538123279E_CustomAttributesCacheGenerator_NarrowBandTelephony(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x65\x72\x69\x61\x6C\x20\x75\x73\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x65\x6D\x69\x74\x74\x65\x72\x20\x69\x73\x20\x73\x65\x74\x20\x74\x6F\x20\x4E\x61\x72\x72\x6F\x77\x20\x42\x61\x6E\x64\x20\x54\x65\x6C\x65\x70\x68\x6F\x6E\x79"), NULL);
	}
}
static void LoFiFilterSelection_tE9D58D7964B6A6B2F4A4B53BC9E3F0538123279E_CustomAttributesCacheGenerator_AmRadio(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x65\x72\x69\x61\x6C\x20\x75\x73\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x65\x6D\x69\x74\x74\x65\x72\x20\x69\x73\x20\x73\x65\x74\x20\x74\x6F\x20\x41\x4D\x20\x52\x61\x64\x69\x6F"), NULL);
	}
}
static void LoFiFilterSelection_tE9D58D7964B6A6B2F4A4B53BC9E3F0538123279E_CustomAttributesCacheGenerator_FullRange(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x65\x72\x69\x61\x6C\x20\x75\x73\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x65\x6D\x69\x74\x74\x65\x72\x20\x69\x73\x20\x73\x65\x74\x20\x74\x6F\x20\x46\x75\x6C\x6C\x20\x52\x61\x6E\x67\x65"), NULL);
	}
}
static void LoFiFilterSelection_tE9D58D7964B6A6B2F4A4B53BC9E3F0538123279E_CustomAttributesCacheGenerator_UnknownQuality(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x74\x65\x72\x69\x61\x6C\x20\x75\x73\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x65\x6D\x69\x74\x74\x65\x72\x20\x69\x73\x20\x73\x65\x74\x20\x74\x6F\x20\x61\x6E\x20\x75\x6E\x6B\x6E\x6F\x77\x6E\x20\x71\x75\x61\x6C\x69\x74\x79"), NULL);
	}
}
static void TextToSpeechSample_t074932D20F9E19BE32D083D57F2BE8B0A19AC745_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TextToSpeech_t918EA567E482DA071F2DE44C0AC7657FB8324DE9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(TextToSpeech_t918EA567E482DA071F2DE44C0AC7657FB8324DE9_0_0_0_var), NULL);
	}
}
static void BoundaryVisualizationDemo_tE6E398A0CDA93FD8387EC5C8B7236DF86E3B29AC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x42\x6F\x75\x6E\x64\x61\x72\x79\x56\x69\x73\x75\x61\x6C\x69\x7A\x61\x74\x69\x6F\x6E\x44\x65\x6D\x6F"), NULL);
	}
}
static void BoundaryVisualizationDemo_tE6E398A0CDA93FD8387EC5C8B7236DF86E3B29AC_CustomAttributesCacheGenerator_showFloor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BoundaryVisualizationDemo_tE6E398A0CDA93FD8387EC5C8B7236DF86E3B29AC_CustomAttributesCacheGenerator_showPlayArea(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BoundaryVisualizationDemo_tE6E398A0CDA93FD8387EC5C8B7236DF86E3B29AC_CustomAttributesCacheGenerator_showTrackedArea(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BoundaryVisualizationDemo_tE6E398A0CDA93FD8387EC5C8B7236DF86E3B29AC_CustomAttributesCacheGenerator_showBoundaryWalls(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BoundaryVisualizationDemo_tE6E398A0CDA93FD8387EC5C8B7236DF86E3B29AC_CustomAttributesCacheGenerator_showBoundaryCeiling(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DebugTextOutput_tD24FC6A02A33518D7142FA16B1159131977277FC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x44\x65\x62\x75\x67\x54\x65\x78\x74\x4F\x75\x74\x70\x75\x74"), NULL);
	}
}
static void DebugTextOutput_tD24FC6A02A33518D7142FA16B1159131977277FC_CustomAttributesCacheGenerator_textMesh(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DemoTouchButton_t4A0517F23180ABAF686A6F7A5D1170C9D2DB5A41_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x4F\x62\x73\x6F\x6C\x65\x74\x65\x2F\x44\x65\x6D\x6F\x54\x6F\x75\x63\x68\x42\x75\x74\x74\x6F\x6E"), NULL);
	}
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[1];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x69\x73\x20\x6E\x6F\x20\x6C\x6F\x6E\x67\x65\x72\x20\x73\x75\x70\x70\x6F\x72\x74\x65\x64"), true, NULL);
	}
}
static void DemoTouchButton_t4A0517F23180ABAF686A6F7A5D1170C9D2DB5A41_CustomAttributesCacheGenerator_debugMessage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HandInteractionTouch_tBD0410F82943DA6D9E05BC1BE521A121250B2220_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x48\x61\x6E\x64\x49\x6E\x74\x65\x72\x61\x63\x74\x69\x6F\x6E\x54\x6F\x75\x63\x68"), NULL);
	}
}
static void HandInteractionTouch_tBD0410F82943DA6D9E05BC1BE521A121250B2220_CustomAttributesCacheGenerator_debugMessage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HandInteractionTouch_tBD0410F82943DA6D9E05BC1BE521A121250B2220_CustomAttributesCacheGenerator_debugMessage2(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HandInteractionTouchRotate_t18A3A74DF6D3ADECF217F9A6FD840B55C6CE7D7C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x48\x61\x6E\x64\x49\x6E\x74\x65\x72\x61\x63\x74\x69\x6F\x6E\x54\x6F\x75\x63\x68\x52\x6F\x74\x61\x74\x65"), NULL);
	}
}
static void HandInteractionTouchRotate_t18A3A74DF6D3ADECF217F9A6FD840B55C6CE7D7C_CustomAttributesCacheGenerator_targetObjectTransform(CustomAttributesCache* cache)
{
	{
		FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 * tmp = (FormerlySerializedAsAttribute_t9505BD2243F1C81AB32EEAF3543A796C2D935210 *)cache->attributes[0];
		FormerlySerializedAsAttribute__ctor_m7A9FC6914FCBA79EE12567BEF3B482CAB7D5265D(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74\x4F\x62\x6A\x65\x63\x74\x54\x72\x61\x6E\x73\x66\x6F\x72\x6D"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HandInteractionTouchRotate_t18A3A74DF6D3ADECF217F9A6FD840B55C6CE7D7C_CustomAttributesCacheGenerator_rotateSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LaunchUri_t777F88E92C36FC506B6E02ED5857E27D01E6889A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x4C\x61\x75\x6E\x63\x68\x55\x72\x69"), NULL);
	}
}
static void SolverTrackedTargetType_tBBC449E2E26568DC90E58135B36D0DB12811F6CB_CustomAttributesCacheGenerator_solverHandler(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SystemKeyboardExample_t54325C6FC824F2691F1EDD471FBEBA326B3BE4F7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x53\x79\x73\x74\x65\x6D\x4B\x65\x79\x62\x6F\x61\x72\x64\x45\x78\x61\x6D\x70\x6C\x65"), NULL);
	}
}
static void SystemKeyboardExample_t54325C6FC824F2691F1EDD471FBEBA326B3BE4F7_CustomAttributesCacheGenerator_debugMessage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SystemKeyboardExample_t54325C6FC824F2691F1EDD471FBEBA326B3BE4F7_CustomAttributesCacheGenerator_mixedRealityKeyboardPreview(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SystemKeyboardExample_t54325C6FC824F2691F1EDD471FBEBA326B3BE4F7_CustomAttributesCacheGenerator_disableUIInteractionWhenTyping(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x57\x68\x65\x74\x68\x65\x72\x20\x64\x69\x73\x61\x62\x6C\x65\x20\x75\x73\x65\x72\x27\x73\x20\x69\x6E\x74\x65\x72\x61\x63\x74\x69\x6F\x6E\x20\x77\x69\x74\x68\x20\x6F\x74\x68\x65\x72\x20\x55\x49\x20\x65\x6C\x65\x6D\x65\x6E\x74\x73\x20\x77\x68\x69\x6C\x65\x20\x74\x79\x70\x69\x6E\x67\x2E\x20\x55\x73\x65\x20\x74\x68\x69\x73\x20\x6F\x70\x74\x69\x6F\x6E\x20\x74\x6F\x20\x64\x65\x63\x72\x65\x61\x73\x65\x20\x74\x68\x65\x20\x63\x68\x61\x6E\x63\x65\x20\x6F\x66\x20\x6B\x65\x79\x62\x6F\x61\x72\x64\x20\x67\x65\x74\x74\x69\x6E\x67\x20\x61\x63\x63\x69\x64\x65\x6E\x74\x61\x6C\x6C\x79\x20\x63\x6C\x6F\x73\x65\x64\x2E"), NULL);
	}
}
static void SystemKeyboardExample_t54325C6FC824F2691F1EDD471FBEBA326B3BE4F7_CustomAttributesCacheGenerator_SystemKeyboardExample_U3CStartU3Eb__5_0_m541CA735B73D753F9564EC8F92050CD1194DEE63(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SystemKeyboardExample_t54325C6FC824F2691F1EDD471FBEBA326B3BE4F7_CustomAttributesCacheGenerator_SystemKeyboardExample_U3CStartU3Eb__5_1_mD9B2785A96F31F655646DC94D36E0F22330A5CFD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TetheredPlacement_tF0ECFAE1E9964CD31FDF8225B43E88F73AFD5EEF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x54\x65\x74\x68\x65\x72\x65\x64\x50\x6C\x61\x63\x65\x6D\x65\x6E\x74"), NULL);
	}
}
static void TetheredPlacement_tF0ECFAE1E9964CD31FDF8225B43E88F73AFD5EEF_CustomAttributesCacheGenerator_DistanceThreshold(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x47\x61\x6D\x65\x4F\x62\x6A\x65\x63\x74\x27\x73\x20\x73\x70\x61\x77\x6E\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x20\x61\x74\x20\x77\x68\x69\x63\x68\x20\x77\x69\x6C\x6C\x20\x74\x72\x69\x67\x67\x65\x72\x20\x61\x20\x72\x65\x73\x70\x61\x77\x6E\x20"), NULL);
	}
}
static void ToggleBoundingBox_t22FA240E53038C2B8021E232F18303DE62A674B2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x4F\x62\x73\x6F\x6C\x65\x74\x65\x2F\x54\x6F\x67\x67\x6C\x65\x42\x6F\x75\x6E\x64\x69\x6E\x67\x42\x6F\x78"), NULL);
	}
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[1];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x69\x73\x20\x6E\x6F\x20\x6C\x6F\x6E\x67\x65\x72\x20\x73\x75\x70\x70\x6F\x72\x74\x65\x64"), true, NULL);
	}
}
static void DisablePointersExample_tCFF01316186515E9F8F96188A01E89D89BD3B04A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&PointerBehaviorControls_tD6A9DA05A72646CCCA2D127CFF34F006A1CB524F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(PointerBehaviorControls_tD6A9DA05A72646CCCA2D127CFF34F006A1CB524F_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x44\x69\x73\x61\x62\x6C\x65\x50\x6F\x69\x6E\x74\x65\x72\x73\x45\x78\x61\x6D\x70\x6C\x65"), NULL);
	}
}
static void Rotator_tCA9D5248AF1FACF74076F54BD0CD759018894849_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x52\x6F\x74\x61\x74\x6F\x72"), NULL);
	}
}
static void InputDataExample_t7D1AAA6071CB7CA1B6EF6D2F5BC2493035E6850D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x49\x6E\x70\x75\x74\x44\x61\x74\x61\x45\x78\x61\x6D\x70\x6C\x65"), NULL);
	}
}
static void InputDataExampleGizmo_t3BA0A8C3399807A32D90E7EF0431099B3937AC2B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x49\x6E\x70\x75\x74\x44\x61\x74\x61\x45\x78\x61\x6D\x70\x6C\x65\x47\x69\x7A\x6D\x6F"), NULL);
	}
}
static void SpawnOnPointerEvent_tC163DCFC36FCA9DDC03C9852D84FF8778614D546_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x53\x70\x61\x77\x6E\x4F\x6E\x50\x6F\x69\x6E\x74\x65\x72\x45\x76\x65\x6E\x74"), NULL);
	}
}
static void PrimaryPointerHandlerExample_tA4F25ED61FCEB8429C788E5918B0DD2BFF567F0F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x50\x72\x69\x6D\x61\x72\x79\x50\x6F\x69\x6E\x74\x65\x72\x48\x61\x6E\x64\x6C\x65\x72\x45\x78\x61\x6D\x70\x6C\x65"), NULL);
	}
}
static void ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x53\x63\x72\x6F\x6C\x6C\x61\x62\x6C\x65\x4C\x69\x73\x74\x50\x6F\x70\x75\x6C\x61\x74\x6F\x72"), NULL);
	}
}
static void ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_scrollView(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x53\x63\x72\x6F\x6C\x6C\x69\x6E\x67\x4F\x62\x6A\x65\x63\x74\x43\x6F\x6C\x6C\x65\x63\x74\x69\x6F\x6E\x20\x74\x6F\x20\x70\x6F\x70\x75\x6C\x61\x74\x65\x2C\x20\x69\x66\x20\x6C\x65\x66\x74\x20\x65\x6D\x70\x74\x79\x2E\x20\x74\x68\x65\x20\x70\x6F\x70\x75\x6C\x61\x74\x6F\x72\x20\x77\x69\x6C\x6C\x20\x63\x72\x65\x61\x74\x65\x20\x6F\x6E\x20\x79\x6F\x75\x72\x20\x62\x65\x68\x61\x6C\x66\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_dynamicItem(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x62\x6A\x65\x63\x74\x20\x74\x6F\x20\x64\x75\x70\x6C\x69\x63\x61\x74\x65\x20\x69\x6E\x20\x53\x63\x72\x6F\x6C\x6C\x43\x6F\x6C\x6C\x65\x63\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_numItems(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x69\x74\x65\x6D\x73\x20\x74\x6F\x20\x67\x65\x6E\x65\x72\x61\x74\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_lazyLoad(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x6D\x6F\x6E\x73\x74\x72\x61\x74\x65\x20\x6C\x61\x7A\x79\x20\x6C\x6F\x61\x64\x69\x6E\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_itemsPerFrame(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x69\x74\x65\x6D\x73\x20\x74\x6F\x20\x6C\x6F\x61\x64\x20\x65\x61\x63\x68\x20\x66\x72\x61\x6D\x65\x20\x64\x75\x72\x69\x6E\x67\x20\x6C\x61\x7A\x79\x20\x6C\x6F\x61\x64"), NULL);
	}
}
static void ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_loader(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x64\x65\x74\x65\x72\x6D\x69\x6E\x61\x74\x65\x20\x6C\x6F\x61\x64\x65\x72\x20\x74\x6F\x20\x68\x69\x64\x65\x20\x2F\x20\x73\x68\x6F\x77\x20\x66\x6F\x72\x20\x4C\x61\x7A\x79\x4C\x6F\x61\x64"), NULL);
	}
}
static void ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_cellWidth(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_cellHeight(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_cellDepth(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_cellsPerTier(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_tiersPerPage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_scrollPositionRef(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_ScrollableListPopulator_UpdateListOverTime_m9C3F25617584DDF0E49A4E5FE3F0434FC554F228(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUpdateListOverTimeU3Ed__33_t6F615CB841917C7A9D914665B70E25E5A2B78D61_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CUpdateListOverTimeU3Ed__33_t6F615CB841917C7A9D914665B70E25E5A2B78D61_0_0_0_var), NULL);
	}
}
static void U3CUpdateListOverTimeU3Ed__33_t6F615CB841917C7A9D914665B70E25E5A2B78D61_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUpdateListOverTimeU3Ed__33_t6F615CB841917C7A9D914665B70E25E5A2B78D61_CustomAttributesCacheGenerator_U3CUpdateListOverTimeU3Ed__33__ctor_m6EEEA2C4C3863DE786A4FB33D9FCD7DCBB83AC7A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateListOverTimeU3Ed__33_t6F615CB841917C7A9D914665B70E25E5A2B78D61_CustomAttributesCacheGenerator_U3CUpdateListOverTimeU3Ed__33_System_IDisposable_Dispose_mC45591ACB830F32EADCEB7990E91CE8347678FE7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateListOverTimeU3Ed__33_t6F615CB841917C7A9D914665B70E25E5A2B78D61_CustomAttributesCacheGenerator_U3CUpdateListOverTimeU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE4E49E62B8DCC9D5A741F6E62D8B43615027F87C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateListOverTimeU3Ed__33_t6F615CB841917C7A9D914665B70E25E5A2B78D61_CustomAttributesCacheGenerator_U3CUpdateListOverTimeU3Ed__33_System_Collections_IEnumerator_Reset_m9839718D6F2442EB8A25E4942DA9C744A69674F0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateListOverTimeU3Ed__33_t6F615CB841917C7A9D914665B70E25E5A2B78D61_CustomAttributesCacheGenerator_U3CUpdateListOverTimeU3Ed__33_System_Collections_IEnumerator_get_Current_m9CA3175850108F9ED839FB1B592FF078FAD74C6B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ScrollablePagination_tF85B373DE319CEBC1E29B196F08A47A80E23ADEE_CustomAttributesCacheGenerator_scrollView(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x53\x63\x72\x6F\x6C\x6C\x69\x6E\x67\x4F\x62\x6A\x65\x63\x74\x43\x6F\x6C\x6C\x65\x63\x74\x69\x6F\x6E\x20\x74\x6F\x20\x6E\x61\x76\x69\x67\x61\x74\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HideTapToPlaceLabel_t0E6D5DD452C4AA4C3BD5D87804F3DEF779665DA9_CustomAttributesCacheGenerator_HideTapToPlaceLabel_U3CAddTapToPlaceListenersU3Eb__3_0_m336168FCCF071E355A2405EF347038D93A48E42D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HideTapToPlaceLabel_t0E6D5DD452C4AA4C3BD5D87804F3DEF779665DA9_CustomAttributesCacheGenerator_HideTapToPlaceLabel_U3CAddTapToPlaceListenersU3Eb__3_1_m1AA8691520D7DF4DE9A4D8AD73CC0CC243564E0B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SolverExampleManager_tCC766F11426471D9B07E155705CFF3378D5228CE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x53\x6F\x6C\x76\x65\x72\x45\x78\x61\x6D\x70\x6C\x65\x4D\x61\x6E\x61\x67\x65\x72"), NULL);
	}
}
static void SolverExampleManager_tCC766F11426471D9B07E155705CFF3378D5228CE_CustomAttributesCacheGenerator_CustomTrackedObject(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ClearSpatialObservations_t3567617E559B0D55730BF91310C23024AAF2531C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x43\x6C\x65\x61\x72\x53\x70\x61\x74\x69\x61\x6C\x4F\x62\x73\x65\x72\x76\x61\x74\x69\x6F\x6E\x73"), NULL);
	}
}
static void ShowPlaneFindingInstructions_t3BAC31E90BD8DA80DA6FD6A704D5C51597D192DA_CustomAttributesCacheGenerator_planeFindingPanel(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x70\x61\x6E\x65\x6C\x20\x74\x6F\x20\x64\x69\x73\x70\x6C\x61\x79\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x70\x6C\x61\x6E\x65\x20\x66\x69\x6E\x64\x69\x6E\x67\x20\x70\x61\x63\x6B\x61\x67\x65\x20\x69\x73\x20\x69\x6D\x70\x6F\x72\x74\x65\x64\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BoundingBoxExampleTest_t920EC66E68751C735AB5FB7F83F532C46DA2BEDB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x42\x6F\x75\x6E\x64\x69\x6E\x67\x42\x6F\x78\x45\x78\x61\x6D\x70\x6C\x65\x54\x65\x73\x74"), NULL);
	}
}
static void BoundingBoxExampleTest_t920EC66E68751C735AB5FB7F83F532C46DA2BEDB_CustomAttributesCacheGenerator_BoundingBoxExampleTest_Sequence_m0A42C1BD0F1E8F21CD3D8C3008AEB03AE3AE2240(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSequenceU3Ed__12_t056AE53EBAF2B9CF286C48D22F0C486DD67863C0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSequenceU3Ed__12_t056AE53EBAF2B9CF286C48D22F0C486DD67863C0_0_0_0_var), NULL);
	}
}
static void BoundingBoxExampleTest_t920EC66E68751C735AB5FB7F83F532C46DA2BEDB_CustomAttributesCacheGenerator_BoundingBoxExampleTest_WaitForSpeechCommand_mBC53747078DBA387C49DB0E4848A9329E34CDC0D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForSpeechCommandU3Ed__14_t02FCE23E8825096EA938587649A5E60F44836910_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForSpeechCommandU3Ed__14_t02FCE23E8825096EA938587649A5E60F44836910_0_0_0_var), NULL);
	}
}
static void BoundingBoxExampleTest_t920EC66E68751C735AB5FB7F83F532C46DA2BEDB_CustomAttributesCacheGenerator_BoundingBoxExampleTest_U3CSequenceU3Eb__12_0_m4F95AD13CBBA6BB3ED84AB44126AD26BC933DB67(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundingBoxExampleTest_t920EC66E68751C735AB5FB7F83F532C46DA2BEDB_CustomAttributesCacheGenerator_BoundingBoxExampleTest_U3CSequenceU3Eb__12_1_mD27CA529D7D5FD14AA5EAE422601F8CC5E583AD2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSequenceU3Ed__12_t056AE53EBAF2B9CF286C48D22F0C486DD67863C0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSequenceU3Ed__12_t056AE53EBAF2B9CF286C48D22F0C486DD67863C0_CustomAttributesCacheGenerator_U3CSequenceU3Ed__12__ctor_m34250F7FFD94CF51D0BA8F66FAA696A74EBED421(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSequenceU3Ed__12_t056AE53EBAF2B9CF286C48D22F0C486DD67863C0_CustomAttributesCacheGenerator_U3CSequenceU3Ed__12_System_IDisposable_Dispose_m1EA4A1FA54F48C6410C00ECF692E454555F19E2A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSequenceU3Ed__12_t056AE53EBAF2B9CF286C48D22F0C486DD67863C0_CustomAttributesCacheGenerator_U3CSequenceU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m966AE4CFB2CB37D234767C012EC7095857E00B82(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSequenceU3Ed__12_t056AE53EBAF2B9CF286C48D22F0C486DD67863C0_CustomAttributesCacheGenerator_U3CSequenceU3Ed__12_System_Collections_IEnumerator_Reset_m847F3DF0C15BDD6E2D270512B111204A7A1028CC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSequenceU3Ed__12_t056AE53EBAF2B9CF286C48D22F0C486DD67863C0_CustomAttributesCacheGenerator_U3CSequenceU3Ed__12_System_Collections_IEnumerator_get_Current_mC6C5FB9C66089B57910B291CC5B9B396A2B6D0B5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForSpeechCommandU3Ed__14_t02FCE23E8825096EA938587649A5E60F44836910_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForSpeechCommandU3Ed__14_t02FCE23E8825096EA938587649A5E60F44836910_CustomAttributesCacheGenerator_U3CWaitForSpeechCommandU3Ed__14__ctor_mB5943136E45C53386CF88C696988515A51E4A9A4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForSpeechCommandU3Ed__14_t02FCE23E8825096EA938587649A5E60F44836910_CustomAttributesCacheGenerator_U3CWaitForSpeechCommandU3Ed__14_System_IDisposable_Dispose_m241B40EFB3E896C86BD1572454BFDB2D695FEE84(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForSpeechCommandU3Ed__14_t02FCE23E8825096EA938587649A5E60F44836910_CustomAttributesCacheGenerator_U3CWaitForSpeechCommandU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m10D6B994F0EF6EDF684CC5C0A0AC7B33EA6316EB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForSpeechCommandU3Ed__14_t02FCE23E8825096EA938587649A5E60F44836910_CustomAttributesCacheGenerator_U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_Reset_m3292A30EC9B1AEAAB25EEF38748317038D3881FC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForSpeechCommandU3Ed__14_t02FCE23E8825096EA938587649A5E60F44836910_CustomAttributesCacheGenerator_U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_get_Current_m2DBE575A1FE49BAEB66D584B50F2C0A91772C4B2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BoundsControlRuntimeExample_t2DF9FD9A869E0569B21F7E491EE942CAAE6C6E3B_CustomAttributesCacheGenerator_BoundsControlRuntimeExample_Sequence_mADF6858AFE4D2FAB573185743D611BD3C7C26CCF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSequenceU3Ed__12_t1CC8D60F8F8D94D9A497136AC0611C33C7913A73_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CSequenceU3Ed__12_t1CC8D60F8F8D94D9A497136AC0611C33C7913A73_0_0_0_var), NULL);
	}
}
static void BoundsControlRuntimeExample_t2DF9FD9A869E0569B21F7E491EE942CAAE6C6E3B_CustomAttributesCacheGenerator_BoundsControlRuntimeExample_WaitForSpeechCommand_mBB8AF5BD87EFE0B9311F1327DB13626525442F3C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForSpeechCommandU3Ed__14_tF24D07507705A5DC8FE5B96BC8EF0EF1223BA8B9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForSpeechCommandU3Ed__14_tF24D07507705A5DC8FE5B96BC8EF0EF1223BA8B9_0_0_0_var), NULL);
	}
}
static void BoundsControlRuntimeExample_t2DF9FD9A869E0569B21F7E491EE942CAAE6C6E3B_CustomAttributesCacheGenerator_BoundsControlRuntimeExample_U3CSequenceU3Eb__12_0_m36BA64048BC4CFEE4DA52E5D2146DA1F21C6B826(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BoundsControlRuntimeExample_t2DF9FD9A869E0569B21F7E491EE942CAAE6C6E3B_CustomAttributesCacheGenerator_BoundsControlRuntimeExample_U3CSequenceU3Eb__12_1_mC4E12695D5E3AF83B72379DE584C30395A510730(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSequenceU3Ed__12_t1CC8D60F8F8D94D9A497136AC0611C33C7913A73_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSequenceU3Ed__12_t1CC8D60F8F8D94D9A497136AC0611C33C7913A73_CustomAttributesCacheGenerator_U3CSequenceU3Ed__12__ctor_mB596D0726FB14CE0546E56694FB5B2CACB0CF45C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSequenceU3Ed__12_t1CC8D60F8F8D94D9A497136AC0611C33C7913A73_CustomAttributesCacheGenerator_U3CSequenceU3Ed__12_System_IDisposable_Dispose_mF11A785D0A2418CAA49D61132D659F18559AF866(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSequenceU3Ed__12_t1CC8D60F8F8D94D9A497136AC0611C33C7913A73_CustomAttributesCacheGenerator_U3CSequenceU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5F0E02B2B5C6D0F72716E6F36D0C6639ECD72379(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSequenceU3Ed__12_t1CC8D60F8F8D94D9A497136AC0611C33C7913A73_CustomAttributesCacheGenerator_U3CSequenceU3Ed__12_System_Collections_IEnumerator_Reset_m43D114449892836ED049B28D22F2E047E47C3D6B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSequenceU3Ed__12_t1CC8D60F8F8D94D9A497136AC0611C33C7913A73_CustomAttributesCacheGenerator_U3CSequenceU3Ed__12_System_Collections_IEnumerator_get_Current_mE027BFD6EE3F6E9724BAC26E0B59C0F8C2C35A7E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForSpeechCommandU3Ed__14_tF24D07507705A5DC8FE5B96BC8EF0EF1223BA8B9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForSpeechCommandU3Ed__14_tF24D07507705A5DC8FE5B96BC8EF0EF1223BA8B9_CustomAttributesCacheGenerator_U3CWaitForSpeechCommandU3Ed__14__ctor_mC11BB974A74E2A51DC40095516466073D190FF36(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForSpeechCommandU3Ed__14_tF24D07507705A5DC8FE5B96BC8EF0EF1223BA8B9_CustomAttributesCacheGenerator_U3CWaitForSpeechCommandU3Ed__14_System_IDisposable_Dispose_mF17BD015850AF82D586F2E502A0F98DFDF1CF681(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForSpeechCommandU3Ed__14_tF24D07507705A5DC8FE5B96BC8EF0EF1223BA8B9_CustomAttributesCacheGenerator_U3CWaitForSpeechCommandU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C339347BA3254B9C3D4ED05B7EC66545A274319(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForSpeechCommandU3Ed__14_tF24D07507705A5DC8FE5B96BC8EF0EF1223BA8B9_CustomAttributesCacheGenerator_U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_Reset_m546A0C9B8314A5766CE101397F9E7DC39CB10DE6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForSpeechCommandU3Ed__14_tF24D07507705A5DC8FE5B96BC8EF0EF1223BA8B9_CustomAttributesCacheGenerator_U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_get_Current_m6DE668CFDD0D0BCCFAE54D3D6F21AFAE511AFB38(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void GridObjectLayoutControl_tC085893CB2C961AC9CBF6B6DC8F379776FA97A35_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x47\x72\x69\x64\x4F\x62\x6A\x65\x63\x74\x4C\x61\x79\x6F\x75\x74\x43\x6F\x6E\x74\x72\x6F\x6C"), NULL);
	}
}
static void GridObjectLayoutControl_tC085893CB2C961AC9CBF6B6DC8F379776FA97A35_CustomAttributesCacheGenerator_grid(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6F\x69\x6E\x74\x20\x74\x68\x69\x73\x20\x61\x74\x20\x74\x68\x65\x20\x47\x72\x69\x64\x4F\x62\x6A\x65\x63\x74\x43\x6F\x6C\x6C\x65\x63\x74\x69\x6F\x6E\x20\x74\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x2E"), NULL);
	}
}
static void GridObjectLayoutControl_tC085893CB2C961AC9CBF6B6DC8F379776FA97A35_CustomAttributesCacheGenerator_text(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x70\x74\x69\x6F\x6E\x61\x6C\x20\x74\x65\x78\x74\x20\x66\x69\x65\x6C\x64\x20\x74\x6F\x20\x6F\x75\x74\x70\x75\x74\x20\x74\x68\x65\x20\x6C\x61\x79\x6F\x75\x74\x20\x6F\x66\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x2E"), NULL);
	}
}
static void GridObjectLayoutControl_tC085893CB2C961AC9CBF6B6DC8F379776FA97A35_CustomAttributesCacheGenerator_GridObjectLayoutControl_TestAnchors_mE3AA69D08ED0CF4432BFBE26768657FA5755555E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CTestAnchorsU3Ed__7_t60EB0100F609816FA418017838B859C42D501123_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CTestAnchorsU3Ed__7_t60EB0100F609816FA418017838B859C42D501123_0_0_0_var), NULL);
	}
}
static void U3CTestAnchorsU3Ed__7_t60EB0100F609816FA418017838B859C42D501123_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CTestAnchorsU3Ed__7_t60EB0100F609816FA418017838B859C42D501123_CustomAttributesCacheGenerator_U3CTestAnchorsU3Ed__7__ctor_m431EC69B94B62747171BF91FA9EBA93C74F1AD56(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTestAnchorsU3Ed__7_t60EB0100F609816FA418017838B859C42D501123_CustomAttributesCacheGenerator_U3CTestAnchorsU3Ed__7_System_IDisposable_Dispose_m8C7D9B0F8F63A73C58000A6C42F7E017F5A01BAA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTestAnchorsU3Ed__7_t60EB0100F609816FA418017838B859C42D501123_CustomAttributesCacheGenerator_U3CTestAnchorsU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C0EB33A01381805945D1A8E56E4D2794FABEFE4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTestAnchorsU3Ed__7_t60EB0100F609816FA418017838B859C42D501123_CustomAttributesCacheGenerator_U3CTestAnchorsU3Ed__7_System_Collections_IEnumerator_Reset_mAC0260AC48A259E59C1D6B027E9A1461C84A50AA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CTestAnchorsU3Ed__7_t60EB0100F609816FA418017838B859C42D501123_CustomAttributesCacheGenerator_U3CTestAnchorsU3Ed__7_System_Collections_IEnumerator_get_Current_m8537D1C2951BE0CF00056F85E74785D071BE37BC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ChangeManipulation_t5D314B3B583DB2F36B1C50A731AE557C824829E6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x43\x68\x61\x6E\x67\x65\x4D\x61\x6E\x69\x70\x75\x6C\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void ReturnToBounds_tF30BD3E23A4AD52A99E97CFF36271E520A3BBF32_CustomAttributesCacheGenerator_frontBounds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ReturnToBounds_tF30BD3E23A4AD52A99E97CFF36271E520A3BBF32_CustomAttributesCacheGenerator_backBounds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ReturnToBounds_tF30BD3E23A4AD52A99E97CFF36271E520A3BBF32_CustomAttributesCacheGenerator_leftBounds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ReturnToBounds_tF30BD3E23A4AD52A99E97CFF36271E520A3BBF32_CustomAttributesCacheGenerator_rightBounds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ReturnToBounds_tF30BD3E23A4AD52A99E97CFF36271E520A3BBF32_CustomAttributesCacheGenerator_bottomBounds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ReturnToBounds_tF30BD3E23A4AD52A99E97CFF36271E520A3BBF32_CustomAttributesCacheGenerator_topBounds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ProgressIndicatorDemo_tD9BD7DCF1298C882786983D4268E77A3E9380474_CustomAttributesCacheGenerator_progressIndicatorLoadingBarGo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x64\x69\x63\x61\x74\x6F\x72\x73"), NULL);
	}
}
static void ProgressIndicatorDemo_tD9BD7DCF1298C882786983D4268E77A3E9380474_CustomAttributesCacheGenerator_progressIndicatorRotatingObjectGo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ProgressIndicatorDemo_tD9BD7DCF1298C882786983D4268E77A3E9380474_CustomAttributesCacheGenerator_progressIndicatorRotatingOrbsGo(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ProgressIndicatorDemo_tD9BD7DCF1298C882786983D4268E77A3E9380474_CustomAttributesCacheGenerator_toggleBarKey(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x64\x69\x74\x6F\x72\x20\x4B\x65\x79\x62\x6F\x61\x72\x64\x20\x43\x6F\x6E\x74\x72\x6F\x6C\x73"), NULL);
	}
}
static void ProgressIndicatorDemo_tD9BD7DCF1298C882786983D4268E77A3E9380474_CustomAttributesCacheGenerator_toggleRotatingKey(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ProgressIndicatorDemo_tD9BD7DCF1298C882786983D4268E77A3E9380474_CustomAttributesCacheGenerator_toggleOrbsKey(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ProgressIndicatorDemo_tD9BD7DCF1298C882786983D4268E77A3E9380474_CustomAttributesCacheGenerator_loadingMessages(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x74\x69\x6E\x67\x73"), NULL);
	}
}
static void ProgressIndicatorDemo_tD9BD7DCF1298C882786983D4268E77A3E9380474_CustomAttributesCacheGenerator_loadingTime(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 10.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ProgressIndicatorDemo_tD9BD7DCF1298C882786983D4268E77A3E9380474_CustomAttributesCacheGenerator_ProgressIndicatorDemo_HandleButtonClick_m83BDE61656ADAE9FA660C25015AF0ED3CE1489A2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CHandleButtonClickU3Ed__14_t6DC14582F351A525C8BBBA55020C98E0EC7871E7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CHandleButtonClickU3Ed__14_t6DC14582F351A525C8BBBA55020C98E0EC7871E7_0_0_0_var), NULL);
	}
	{
		DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F * tmp = (DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F *)cache->attributes[1];
		DebuggerStepThroughAttribute__ctor_m2B40F019B0DF22CF7A815AAB3D2D027225D59D85(tmp, NULL);
	}
}
static void ProgressIndicatorDemo_tD9BD7DCF1298C882786983D4268E77A3E9380474_CustomAttributesCacheGenerator_ProgressIndicatorDemo_OpenProgressIndicator_m21F6477AF93FE257DF3D39A22301C610B7AA2EC3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3COpenProgressIndicatorU3Ed__17_tCA040FFC07AD3E4D55D008542D434CD6D80B4388_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3COpenProgressIndicatorU3Ed__17_tCA040FFC07AD3E4D55D008542D434CD6D80B4388_0_0_0_var), NULL);
	}
	{
		DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F * tmp = (DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F *)cache->attributes[1];
		DebuggerStepThroughAttribute__ctor_m2B40F019B0DF22CF7A815AAB3D2D027225D59D85(tmp, NULL);
	}
}
static void U3CHandleButtonClickU3Ed__14_t6DC14582F351A525C8BBBA55020C98E0EC7871E7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CHandleButtonClickU3Ed__14_t6DC14582F351A525C8BBBA55020C98E0EC7871E7_CustomAttributesCacheGenerator_U3CHandleButtonClickU3Ed__14_SetStateMachine_m6763E06BEC587E1ABE97242AC32B2C93DA60BB30(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3COpenProgressIndicatorU3Ed__17_tCA040FFC07AD3E4D55D008542D434CD6D80B4388_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3COpenProgressIndicatorU3Ed__17_tCA040FFC07AD3E4D55D008542D434CD6D80B4388_CustomAttributesCacheGenerator_U3COpenProgressIndicatorU3Ed__17_SetStateMachine_m8402426A9BB1212956FF5CDA08A962A3FA14D274(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SliderLunarLander_tCC3911180680DDC07A7DF357A5039DCC83338A1D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x53\x6C\x69\x64\x65\x72\x4C\x75\x6E\x61\x72\x4C\x61\x6E\x64\x65\x72"), NULL);
	}
}
static void SliderLunarLander_tCC3911180680DDC07A7DF357A5039DCC83338A1D_CustomAttributesCacheGenerator_transformLandingGear(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MixedRealityCapabilityDemo_tB168C0284BB55A7C2DC7269E6ABF652A8464CDEC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x4D\x69\x78\x65\x64\x52\x65\x61\x6C\x69\x74\x79\x43\x61\x70\x61\x62\x69\x6C\x69\x74\x79\x44\x65\x6D\x6F"), NULL);
	}
}
static void MixedRealityCapabilityDemo_tB168C0284BB55A7C2DC7269E6ABF652A8464CDEC_CustomAttributesCacheGenerator_articulatedHandResult(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MixedRealityCapabilityDemo_tB168C0284BB55A7C2DC7269E6ABF652A8464CDEC_CustomAttributesCacheGenerator_ggvHandResult(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MixedRealityCapabilityDemo_tB168C0284BB55A7C2DC7269E6ABF652A8464CDEC_CustomAttributesCacheGenerator_motionControllerResult(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MixedRealityCapabilityDemo_tB168C0284BB55A7C2DC7269E6ABF652A8464CDEC_CustomAttributesCacheGenerator_eyeTrackingResult(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MixedRealityCapabilityDemo_tB168C0284BB55A7C2DC7269E6ABF652A8464CDEC_CustomAttributesCacheGenerator_voiceCommandResult(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MixedRealityCapabilityDemo_tB168C0284BB55A7C2DC7269E6ABF652A8464CDEC_CustomAttributesCacheGenerator_voiceDictationResult(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MixedRealityCapabilityDemo_tB168C0284BB55A7C2DC7269E6ABF652A8464CDEC_CustomAttributesCacheGenerator_spatialMeshResult(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MixedRealityCapabilityDemo_tB168C0284BB55A7C2DC7269E6ABF652A8464CDEC_CustomAttributesCacheGenerator_spatialPlaneResult(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MixedRealityCapabilityDemo_tB168C0284BB55A7C2DC7269E6ABF652A8464CDEC_CustomAttributesCacheGenerator_spatialPointResult(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ReadingModeSceneBehavior_tF8E4C305C89C94C3FE039C6D08D41D98A11E08D7_CustomAttributesCacheGenerator_renderViewportScaleSlider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ColorTap_t3968DEA6867F20CE61064E8D670CE3A4757B2FC3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x43\x6F\x6C\x6F\x72\x54\x61\x70"), NULL);
	}
}
static void ColorTap_t3968DEA6867F20CE61064E8D670CE3A4757B2FC3_CustomAttributesCacheGenerator_tapAction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ColorTap_t3968DEA6867F20CE61064E8D670CE3A4757B2FC3_CustomAttributesCacheGenerator_color_IdleState(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ColorTap_t3968DEA6867F20CE61064E8D670CE3A4757B2FC3_CustomAttributesCacheGenerator_color_OnHover(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ColorTap_t3968DEA6867F20CE61064E8D670CE3A4757B2FC3_CustomAttributesCacheGenerator_color_OnSelect(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FollowEyeGazeGazeProvider_tCF30FF9F5815081DCD446A5EB5FD32D9ACC2F3C9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x46\x6F\x6C\x6C\x6F\x77\x45\x79\x65\x47\x61\x7A\x65\x47\x61\x7A\x65\x50\x72\x6F\x76\x69\x64\x65\x72"), NULL);
	}
}
static void FollowEyeGazeGazeProvider_tCF30FF9F5815081DCD446A5EB5FD32D9ACC2F3C9_CustomAttributesCacheGenerator_defaultDistanceInMeters(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x73\x70\x6C\x61\x79\x20\x74\x68\x65\x20\x67\x61\x6D\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x61\x6C\x6F\x6E\x67\x20\x74\x68\x65\x20\x65\x79\x65\x20\x67\x61\x7A\x65\x20\x72\x61\x79\x20\x61\x74\x20\x61\x20\x64\x65\x66\x61\x75\x6C\x74\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x28\x69\x6E\x20\x6D\x65\x74\x65\x72\x73\x29\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanZoomBase_t202BE6839273DFACBD9236A4C11306A4E4DF09B0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EyeTrackingTarget_tC638599E70EEE5CDC5E0A3EAEE31E158C221C0FA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(EyeTrackingTarget_tC638599E70EEE5CDC5E0A3EAEE31E158C221C0FA_0_0_0_var), NULL);
	}
}
static void PanZoomBase_t202BE6839273DFACBD9236A4C11306A4E4DF09B0_CustomAttributesCacheGenerator_ZoomSpeedMax(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x20\x73\x70\x65\x65\x64\x20\x77\x68\x65\x6E\x20\x7A\x6F\x6F\x6D\x69\x6E\x67\x2E"), NULL);
	}
}
static void PanZoomBase_t202BE6839273DFACBD9236A4C11306A4E4DF09B0_CustomAttributesCacheGenerator_PanZoomBase_ZoomAndStop_m70C71BF56D1145AB3CDF2E00A8EB01D238FBCECD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CZoomAndStopU3Ed__78_t5DD82D6ECCE58A5515F5D95AABB14F522D219E46_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CZoomAndStopU3Ed__78_t5DD82D6ECCE58A5515F5D95AABB14F522D219E46_0_0_0_var), NULL);
	}
}
static void U3CZoomAndStopU3Ed__78_t5DD82D6ECCE58A5515F5D95AABB14F522D219E46_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CZoomAndStopU3Ed__78_t5DD82D6ECCE58A5515F5D95AABB14F522D219E46_CustomAttributesCacheGenerator_U3CZoomAndStopU3Ed__78__ctor_m46258496853C97A3C755A49B27F1D14CBCDA8AC4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CZoomAndStopU3Ed__78_t5DD82D6ECCE58A5515F5D95AABB14F522D219E46_CustomAttributesCacheGenerator_U3CZoomAndStopU3Ed__78_System_IDisposable_Dispose_m3125E7B1AF6696673562538B404794532B5C6581(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CZoomAndStopU3Ed__78_t5DD82D6ECCE58A5515F5D95AABB14F522D219E46_CustomAttributesCacheGenerator_U3CZoomAndStopU3Ed__78_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5007A408115112E9A302CE1D2D2D6C8912A0B878(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CZoomAndStopU3Ed__78_t5DD82D6ECCE58A5515F5D95AABB14F522D219E46_CustomAttributesCacheGenerator_U3CZoomAndStopU3Ed__78_System_Collections_IEnumerator_Reset_m377FFA4C86BEBD8E6385A94CAFD88AC4F7389826(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CZoomAndStopU3Ed__78_t5DD82D6ECCE58A5515F5D95AABB14F522D219E46_CustomAttributesCacheGenerator_U3CZoomAndStopU3Ed__78_System_Collections_IEnumerator_get_Current_m40F497BEDB0DA316498CD3F4F0964E601517371A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void PanZoomBaseRectTransf_tD2B01CF8F2DC8FAD0B548F6B6B82064CADB979E5_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x50\x61\x6E\x5A\x6F\x6F\x6D\x42\x61\x73\x65\x52\x65\x63\x74\x54\x72\x61\x6E\x73\x66"), NULL);
	}
}
static void PanZoomBaseTexture_t42FA11BC2834FC26BB348A44DC0CA4E4B422AD3D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x50\x61\x6E\x5A\x6F\x6F\x6D\x42\x61\x73\x65\x54\x65\x78\x74\x75\x72\x65"), NULL);
	}
}
static void PanZoomBaseTexture_t42FA11BC2834FC26BB348A44DC0CA4E4B422AD3D_CustomAttributesCacheGenerator_defaultAspectRatio(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x6E\x64\x65\x72\x6C\x79\x69\x6E\x67\x20\x61\x73\x70\x65\x63\x74\x20\x72\x61\x74\x69\x6F\x20\x6F\x66\x20\x74\x68\x65\x20\x6C\x6F\x61\x64\x65\x64\x20\x74\x65\x78\x74\x75\x72\x65\x20\x74\x6F\x20\x63\x6F\x72\x72\x65\x63\x74\x6C\x79\x20\x64\x65\x74\x65\x72\x6D\x69\x6E\x65\x20\x73\x63\x61\x6C\x69\x6E\x67\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x50\x61\x6E\x5A\x6F\x6F\x6D\x52\x65\x63\x74\x54\x72\x61\x6E\x73\x66"), NULL);
	}
}
static void PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_rectTransfToNavigate(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x63\x74\x54\x72\x61\x6E\x73\x66\x6F\x72\x6D\x20\x66\x72\x6F\x6D\x2C\x20\x66\x6F\x72\x20\x65\x78\x61\x6D\x70\x6C\x65\x2C\x20\x79\x6F\x75\x72\x20\x54\x65\x78\x74\x4D\x65\x73\x68\x50\x72\x6F\x20\x67\x61\x6D\x65\x20\x6F\x62\x6A\x65\x63\x74\x2E"), NULL);
	}
}
static void PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_refToViewPort(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x66\x65\x72\x65\x6E\x63\x65\x20\x74\x6F\x20\x74\x68\x65\x20\x76\x69\x65\x77\x70\x6F\x72\x74\x20\x72\x65\x73\x74\x72\x69\x63\x74\x69\x6E\x67\x20\x74\x68\x65\x20\x76\x69\x65\x77\x62\x6F\x78\x2E\x20\x54\x68\x69\x73\x20\x69\x73\x20\x69\x6D\x70\x6F\x72\x74\x61\x6E\x74\x20\x66\x6F\x72\x20\x69\x64\x65\x6E\x74\x69\x66\x79\x69\x6E\x67\x20\x74\x68\x65\x20\x6D\x61\x78\x20\x63\x6F\x6E\x73\x74\x72\x61\x69\x6E\x73\x20\x66\x6F\x72\x20\x70\x61\x6E\x6E\x69\x6E\x67\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_zoomAcceleration(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x5A\x6F\x6F\x6D\x20\x61\x63\x63\x65\x6C\x65\x72\x61\x74\x69\x6F\x6E\x20\x64\x65\x66\x69\x6E\x69\x6E\x67\x20\x74\x68\x65\x20\x73\x74\x65\x65\x70\x6E\x65\x73\x73\x20\x6F\x66\x20\x6C\x6F\x67\x69\x73\x74\x69\x63\x20\x73\x70\x65\x65\x64\x20\x66\x75\x6E\x63\x74\x69\x6F\x6E\x20\x6D\x61\x70\x70\x69\x6E\x67\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_zoomSpeedMax(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x20\x7A\x6F\x6F\x6D\x20\x73\x70\x65\x65\x64\x2E"), NULL);
	}
}
static void PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_zoomMinScale(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x75\x6D\x20\x73\x63\x61\x6C\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x65\x78\x74\x75\x72\x65\x20\x66\x6F\x72\x20\x7A\x6F\x6F\x6D\x20\x69\x6E\x20\x2D\x20\x65\x2E\x67\x2E\x2C\x20\x30\x2E\x35\x66\x20\x28\x68\x61\x6C\x66\x20\x74\x68\x65\x20\x6F\x72\x69\x67\x69\x6E\x61\x6C\x20\x73\x69\x7A\x65\x29"), NULL);
	}
}
static void PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_zoomMaxScale(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x20\x73\x63\x61\x6C\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x65\x78\x74\x75\x72\x65\x20\x66\x6F\x72\x20\x7A\x6F\x6F\x6D\x20\x6F\x75\x74\x20\x2D\x20\x65\x2E\x67\x2E\x2C\x20\x31\x66\x20\x28\x74\x68\x65\x20\x6F\x72\x69\x67\x69\x6E\x61\x6C\x20\x73\x69\x7A\x65\x29\x20\x6F\x72\x20\x32\x2E\x30\x66\x20\x28\x64\x6F\x75\x62\x6C\x65\x20\x74\x68\x65\x20\x6F\x72\x69\x67\x69\x6E\x61\x6C\x20\x73\x69\x7A\x65\x29\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_zoomTimeInSecToZoom(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x69\x6D\x65\x64\x20\x7A\x6F\x6F\x6D\x3A\x20\x4F\x6E\x63\x65\x20\x74\x72\x69\x67\x67\x65\x72\x65\x64\x2C\x20\x61\x20\x7A\x6F\x6F\x6D\x20\x69\x6E\x2F\x6F\x75\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x70\x65\x72\x66\x6F\x72\x6D\x65\x64\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x67\x69\x76\x65\x6E\x20\x61\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x74\x69\x6D\x65\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_zoomGestureEnabledOnStartup(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x6F\x72\x20\x64\x69\x73\x61\x62\x6C\x65\x20\x68\x61\x6E\x64\x20\x67\x65\x73\x74\x75\x72\x65\x73\x20\x66\x6F\x72\x20\x7A\x6F\x6F\x6D\x69\x6E\x67\x20\x6F\x6E\x20\x73\x74\x61\x72\x74\x75\x70\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_panAutoScrollIsActive(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x62\x69\x6C\x69\x74\x79\x20\x74\x6F\x20\x73\x63\x72\x6F\x6C\x6C\x20\x75\x73\x69\x6E\x67\x20\x79\x6F\x75\x72\x20\x65\x79\x65\x20\x67\x61\x7A\x65\x20\x77\x69\x74\x68\x6F\x75\x74\x20\x61\x6E\x79\x20\x61\x64\x64\x69\x74\x69\x6F\x6E\x61\x6C\x20\x69\x6E\x70\x75\x74\x20\x28\x65\x2E\x67\x2E\x2C\x20\x61\x69\x72\x20\x74\x61\x70\x20\x6F\x72\x20\x62\x75\x74\x74\x6F\x6E\x20\x70\x72\x65\x73\x73\x65\x73\x29\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_panSpeedHorizontal(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x72\x69\x7A\x6F\x6E\x74\x61\x6C\x20\x73\x63\x72\x6F\x6C\x6C\x20\x73\x70\x65\x65\x64\x2E\x20\x46\x6F\x72\x20\x65\x78\x61\x6D\x70\x6C\x65\x3A\x20\x30\x2E\x31\x66\x20\x66\x6F\x72\x20\x73\x6C\x6F\x77\x20\x70\x61\x6E\x6E\x69\x6E\x67\x2E\x20\x30\x2E\x36\x66\x20\x66\x6F\x72\x20\x66\x61\x73\x74\x20\x70\x61\x6E\x6E\x69\x6E\x67\x2E"), NULL);
	}
}
static void PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_panSpeedVertical(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x56\x65\x72\x74\x69\x63\x61\x6C\x20\x73\x63\x72\x6F\x6C\x6C\x20\x73\x70\x65\x65\x64\x2E\x20\x46\x6F\x72\x20\x65\x78\x61\x6D\x70\x6C\x65\x3A\x20\x30\x2E\x31\x66\x20\x66\x6F\x72\x20\x73\x6C\x6F\x77\x20\x70\x61\x6E\x6E\x69\x6E\x67\x2E\x20\x30\x2E\x36\x66\x20\x66\x6F\x72\x20\x66\x61\x73\x74\x20\x70\x61\x6E\x6E\x69\x6E\x67\x2E"), NULL);
	}
}
static void PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_panMinDistFromCenter(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x61\x6C\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x69\x6E\x20\x78\x20\x61\x6E\x64\x20\x79\x20\x66\x72\x6F\x6D\x20\x63\x65\x6E\x74\x65\x72\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x27\x73\x20\x68\x69\x74\x20\x62\x6F\x78\x20\x28\x30\x2C\x20\x30\x29\x20\x74\x6F\x20\x73\x63\x72\x6F\x6C\x6C\x2E\x20\x54\x68\x75\x73\x2C\x20\x76\x61\x6C\x75\x65\x73\x20\x6D\x75\x73\x74\x20\x72\x61\x6E\x67\x65\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x30\x20\x28\x61\x6C\x77\x61\x79\x73\x20\x73\x63\x72\x6F\x6C\x6C\x29\x20\x61\x6E\x64\x20\x30\x2E\x35\x20\x28\x6E\x6F\x20\x73\x63\x72\x6F\x6C\x6C\x29\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_useSkimProofing(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x20\x74\x6F\x20\x74\x72\x75\x65\x20\x74\x6F\x20\x70\x72\x65\x76\x65\x6E\x74\x20\x73\x75\x64\x64\x65\x6E\x20\x73\x63\x72\x6F\x6C\x6C\x69\x6E\x67\x20\x77\x68\x65\x6E\x20\x71\x75\x69\x63\x6B\x6C\x79\x20\x6C\x6F\x6F\x6B\x69\x6E\x67\x20\x61\x72\x6F\x75\x6E\x64\x2E\x20\x54\x68\x69\x73\x20\x6D\x61\x79\x20\x6D\x61\x6B\x65\x20\x73\x63\x72\x6F\x6C\x6C\x69\x6E\x67\x20\x66\x65\x65\x6C\x20\x6C\x65\x73\x73\x20\x72\x65\x73\x70\x6F\x6E\x73\x69\x76\x65\x20\x74\x68\x6F\x75\x67\x68\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_skimProofUpdateSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6C\x6F\x77\x65\x72\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x2C\x20\x74\x68\x65\x20\x73\x6C\x6F\x77\x65\x72\x20\x74\x68\x65\x20\x73\x63\x72\x6F\x6C\x6C\x69\x6E\x67\x20\x77\x69\x6C\x6C\x20\x73\x70\x65\x65\x64\x20\x75\x70\x20\x61\x66\x74\x65\x72\x20\x73\x6B\x69\x6D\x6D\x69\x6E\x67\x2E\x20\x52\x65\x63\x6F\x6D\x6D\x65\x6E\x64\x65\x64\x20\x76\x61\x6C\x75\x65\x3A\x20\x35\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[2];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 10.0f, NULL);
	}
}
static void PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x50\x61\x6E\x5A\x6F\x6F\x6D\x54\x65\x78\x74\x75\x72\x65"), NULL);
	}
}
static void PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_rendererOfTextureToBeNavigated(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x66\x65\x72\x65\x6E\x63\x65\x64\x20\x72\x65\x6E\x64\x65\x72\x65\x72\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x65\x78\x74\x75\x72\x65\x20\x74\x6F\x20\x62\x65\x20\x6E\x61\x76\x69\x67\x61\x74\x65\x64\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_zoomAcceleration(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x5A\x6F\x6F\x6D\x20\x61\x63\x63\x65\x6C\x65\x72\x61\x74\x69\x6F\x6E\x20\x64\x65\x66\x69\x6E\x69\x6E\x67\x20\x74\x68\x65\x20\x73\x74\x65\x65\x70\x6E\x65\x73\x73\x20\x6F\x66\x20\x6C\x6F\x67\x69\x73\x74\x69\x63\x20\x73\x70\x65\x65\x64\x20\x66\x75\x6E\x63\x74\x69\x6F\x6E\x20\x6D\x61\x70\x70\x69\x6E\x67\x2E"), NULL);
	}
}
static void PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_zoomSpeedMax(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x20\x7A\x6F\x6F\x6D\x20\x73\x70\x65\x65\x64\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_zoomMinScale(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x75\x6D\x20\x73\x63\x61\x6C\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x65\x78\x74\x75\x72\x65\x20\x66\x6F\x72\x20\x7A\x6F\x6F\x6D\x20\x69\x6E\x20\x2D\x20\x65\x2E\x67\x2E\x2C\x20\x30\x2E\x35\x66\x20\x28\x68\x61\x6C\x66\x20\x74\x68\x65\x20\x6F\x72\x69\x67\x69\x6E\x61\x6C\x20\x73\x69\x7A\x65\x29"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_zoomMaxScale(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x20\x73\x63\x61\x6C\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x65\x78\x74\x75\x72\x65\x20\x66\x6F\x72\x20\x7A\x6F\x6F\x6D\x20\x6F\x75\x74\x20\x2D\x20\x65\x2E\x67\x2E\x2C\x20\x31\x66\x20\x28\x74\x68\x65\x20\x6F\x72\x69\x67\x69\x6E\x61\x6C\x20\x73\x69\x7A\x65\x29\x20\x6F\x72\x20\x32\x2E\x30\x66\x20\x28\x64\x6F\x75\x62\x6C\x65\x20\x74\x68\x65\x20\x6F\x72\x69\x67\x69\x6E\x61\x6C\x20\x73\x69\x7A\x65\x29\x2E"), NULL);
	}
}
static void PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_zoomTimeInSecToZoom(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x69\x6D\x65\x64\x20\x7A\x6F\x6F\x6D\x3A\x20\x4F\x6E\x63\x65\x20\x74\x72\x69\x67\x67\x65\x72\x65\x64\x2C\x20\x61\x20\x7A\x6F\x6F\x6D\x20\x69\x6E\x2F\x6F\x75\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x70\x65\x72\x66\x6F\x72\x6D\x65\x64\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x67\x69\x76\x65\x6E\x20\x61\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x74\x69\x6D\x65\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x2E"), NULL);
	}
}
static void PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_zoomGestureEnabledOnStartup(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x61\x62\x6C\x65\x20\x6F\x72\x20\x64\x69\x73\x61\x62\x6C\x65\x20\x68\x61\x6E\x64\x20\x67\x65\x73\x74\x75\x72\x65\x73\x20\x66\x6F\x72\x20\x7A\x6F\x6F\x6D\x69\x6E\x67\x20\x6F\x6E\x20\x73\x74\x61\x72\x74\x75\x70\x2E"), NULL);
	}
}
static void PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_panAutoScrollIsActive(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x62\x69\x6C\x69\x74\x79\x20\x74\x6F\x20\x73\x63\x72\x6F\x6C\x6C\x20\x75\x73\x69\x6E\x67\x20\x79\x6F\x75\x72\x20\x65\x79\x65\x20\x67\x61\x7A\x65\x20\x77\x69\x74\x68\x6F\x75\x74\x20\x61\x6E\x79\x20\x61\x64\x64\x69\x74\x69\x6F\x6E\x61\x6C\x20\x69\x6E\x70\x75\x74\x20\x28\x65\x2E\x67\x2E\x2C\x20\x61\x69\x72\x20\x74\x61\x70\x20\x6F\x72\x20\x62\x75\x74\x74\x6F\x6E\x20\x70\x72\x65\x73\x73\x65\x73\x29\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_panSpeedHorizontal(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x72\x69\x7A\x6F\x6E\x74\x61\x6C\x20\x73\x63\x72\x6F\x6C\x6C\x20\x73\x70\x65\x65\x64\x2E\x20\x46\x6F\x72\x20\x65\x78\x61\x6D\x70\x6C\x65\x3A\x20\x30\x2E\x31\x66\x20\x66\x6F\x72\x20\x73\x6C\x6F\x77\x20\x70\x61\x6E\x6E\x69\x6E\x67\x2E\x20\x30\x2E\x36\x66\x20\x66\x6F\x72\x20\x66\x61\x73\x74\x20\x70\x61\x6E\x6E\x69\x6E\x67\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_panSpeedVertical(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x56\x65\x72\x74\x69\x63\x61\x6C\x20\x73\x63\x72\x6F\x6C\x6C\x20\x73\x70\x65\x65\x64\x2E\x20\x46\x6F\x72\x20\x65\x78\x61\x6D\x70\x6C\x65\x3A\x20\x30\x2E\x31\x66\x20\x66\x6F\x72\x20\x73\x6C\x6F\x77\x20\x70\x61\x6E\x6E\x69\x6E\x67\x2E\x20\x30\x2E\x36\x66\x20\x66\x6F\x72\x20\x66\x61\x73\x74\x20\x70\x61\x6E\x6E\x69\x6E\x67\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_panMinDistFromCenter(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x61\x6C\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x69\x6E\x20\x78\x20\x61\x6E\x64\x20\x79\x20\x66\x72\x6F\x6D\x20\x63\x65\x6E\x74\x65\x72\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x27\x73\x20\x68\x69\x74\x20\x62\x6F\x78\x20\x28\x30\x2C\x20\x30\x29\x20\x74\x6F\x20\x73\x63\x72\x6F\x6C\x6C\x2E\x20\x54\x68\x75\x73\x2C\x20\x76\x61\x6C\x75\x65\x73\x20\x6D\x75\x73\x74\x20\x72\x61\x6E\x67\x65\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x30\x20\x28\x61\x6C\x77\x61\x79\x73\x20\x73\x63\x72\x6F\x6C\x6C\x29\x20\x61\x6E\x64\x20\x30\x2E\x35\x20\x28\x6E\x6F\x20\x73\x63\x72\x6F\x6C\x6C\x29\x2E"), NULL);
	}
}
static void PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_useSkimProofing(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x20\x74\x6F\x20\x74\x72\x75\x65\x20\x74\x6F\x20\x70\x72\x65\x76\x65\x6E\x74\x20\x73\x75\x64\x64\x65\x6E\x20\x73\x63\x72\x6F\x6C\x6C\x69\x6E\x67\x20\x77\x68\x65\x6E\x20\x71\x75\x69\x63\x6B\x6C\x79\x20\x6C\x6F\x6F\x6B\x69\x6E\x67\x20\x61\x72\x6F\x75\x6E\x64\x2E\x20\x54\x68\x69\x73\x20\x6D\x61\x79\x20\x6D\x61\x6B\x65\x20\x73\x63\x72\x6F\x6C\x6C\x69\x6E\x67\x20\x66\x65\x65\x6C\x20\x6C\x65\x73\x73\x20\x72\x65\x73\x70\x6F\x6E\x73\x69\x76\x65\x20\x74\x68\x6F\x75\x67\x68\x2E"), NULL);
	}
}
static void PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_skimProofUpdateSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 10.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6C\x6F\x77\x65\x72\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x2C\x20\x74\x68\x65\x20\x73\x6C\x6F\x77\x65\x72\x20\x74\x68\x65\x20\x73\x63\x72\x6F\x6C\x6C\x69\x6E\x67\x20\x77\x69\x6C\x6C\x20\x73\x70\x65\x65\x64\x20\x75\x70\x20\x61\x66\x74\x65\x72\x20\x73\x6B\x69\x6D\x6D\x69\x6E\x67\x2E\x20\x52\x65\x63\x6F\x6D\x6D\x65\x6E\x64\x65\x64\x20\x76\x61\x6C\x75\x65\x3A\x20\x35\x2E"), NULL);
	}
}
static void ScrollRectTransf_tAC616AEB395D752AF7834443D37E44BCADE1406F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x53\x63\x72\x6F\x6C\x6C\x52\x65\x63\x74\x54\x72\x61\x6E\x73\x66"), NULL);
	}
}
static void ScrollRectTransf_tAC616AEB395D752AF7834443D37E44BCADE1406F_CustomAttributesCacheGenerator_rectTransfToNavigate(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x63\x74\x54\x72\x61\x6E\x73\x66\x6F\x72\x6D\x20\x66\x72\x6F\x6D\x2C\x20\x66\x6F\x72\x20\x65\x78\x61\x6D\x70\x6C\x65\x2C\x20\x79\x6F\x75\x72\x20\x54\x65\x78\x74\x4D\x65\x73\x68\x50\x72\x6F\x20\x67\x61\x6D\x65\x20\x6F\x62\x6A\x65\x63\x74\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollRectTransf_tAC616AEB395D752AF7834443D37E44BCADE1406F_CustomAttributesCacheGenerator_refToViewPort(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x66\x65\x72\x65\x6E\x63\x65\x20\x74\x6F\x20\x74\x68\x65\x20\x76\x69\x65\x77\x70\x6F\x72\x74\x20\x72\x65\x73\x74\x72\x69\x63\x74\x69\x6E\x67\x20\x74\x68\x65\x20\x76\x69\x65\x77\x62\x6F\x78\x2E\x20\x54\x68\x69\x73\x20\x69\x73\x20\x69\x6D\x70\x6F\x72\x74\x61\x6E\x74\x20\x66\x6F\x72\x20\x69\x64\x65\x6E\x74\x69\x66\x79\x69\x6E\x67\x20\x74\x68\x65\x20\x6D\x61\x78\x20\x63\x6F\x6E\x73\x74\x72\x61\x69\x6E\x73\x20\x66\x6F\x72\x20\x70\x61\x6E\x6E\x69\x6E\x67\x2E"), NULL);
	}
}
static void ScrollRectTransf_tAC616AEB395D752AF7834443D37E44BCADE1406F_CustomAttributesCacheGenerator_autoGazeScrollIsActive(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x62\x69\x6C\x69\x74\x79\x20\x74\x6F\x20\x73\x63\x72\x6F\x6C\x6C\x20\x75\x73\x69\x6E\x67\x20\x79\x6F\x75\x72\x20\x65\x79\x65\x20\x67\x61\x7A\x65\x20\x77\x69\x74\x68\x6F\x75\x74\x20\x61\x6E\x79\x20\x61\x64\x64\x69\x74\x69\x6F\x6E\x61\x6C\x20\x69\x6E\x70\x75\x74\x20\x28\x65\x2E\x67\x2E\x2C\x20\x61\x69\x72\x20\x74\x61\x70\x20\x6F\x72\x20\x62\x75\x74\x74\x6F\x6E\x20\x70\x72\x65\x73\x73\x65\x73\x29\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollRectTransf_tAC616AEB395D752AF7834443D37E44BCADE1406F_CustomAttributesCacheGenerator_scrollSpeedHorizontal(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x72\x69\x7A\x6F\x6E\x74\x61\x6C\x20\x73\x63\x72\x6F\x6C\x6C\x20\x73\x70\x65\x65\x64\x2E\x20\x46\x6F\x72\x20\x65\x78\x61\x6D\x70\x6C\x65\x3A\x20\x30\x2E\x31\x66\x20\x66\x6F\x72\x20\x73\x6C\x6F\x77\x20\x70\x61\x6E\x6E\x69\x6E\x67\x2E\x20\x30\x2E\x36\x66\x20\x66\x6F\x72\x20\x66\x61\x73\x74\x20\x70\x61\x6E\x6E\x69\x6E\x67\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollRectTransf_tAC616AEB395D752AF7834443D37E44BCADE1406F_CustomAttributesCacheGenerator_scrollSpeedVertical(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x56\x65\x72\x74\x69\x63\x61\x6C\x20\x73\x63\x72\x6F\x6C\x6C\x20\x73\x70\x65\x65\x64\x2E\x20\x46\x6F\x72\x20\x65\x78\x61\x6D\x70\x6C\x65\x3A\x20\x30\x2E\x31\x66\x20\x66\x6F\x72\x20\x73\x6C\x6F\x77\x20\x70\x61\x6E\x6E\x69\x6E\x67\x2E\x20\x30\x2E\x36\x66\x20\x66\x6F\x72\x20\x66\x61\x73\x74\x20\x70\x61\x6E\x6E\x69\x6E\x67\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollRectTransf_tAC616AEB395D752AF7834443D37E44BCADE1406F_CustomAttributesCacheGenerator_minDistFromCenterForAutoScroll(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x61\x6C\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x69\x6E\x20\x78\x20\x61\x6E\x64\x20\x79\x20\x66\x72\x6F\x6D\x20\x63\x65\x6E\x74\x65\x72\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x27\x73\x20\x68\x69\x74\x20\x62\x6F\x78\x20\x28\x30\x2C\x20\x30\x29\x20\x74\x6F\x20\x73\x63\x72\x6F\x6C\x6C\x2E\x20\x54\x68\x75\x73\x2C\x20\x76\x61\x6C\x75\x65\x73\x20\x6D\x75\x73\x74\x20\x72\x61\x6E\x67\x65\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x30\x20\x28\x61\x6C\x77\x61\x79\x73\x20\x73\x63\x72\x6F\x6C\x6C\x29\x20\x61\x6E\x64\x20\x30\x2E\x35\x20\x28\x6E\x6F\x20\x73\x63\x72\x6F\x6C\x6C\x29\x2E"), NULL);
	}
}
static void ScrollRectTransf_tAC616AEB395D752AF7834443D37E44BCADE1406F_CustomAttributesCacheGenerator_useSkimProofing(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x20\x74\x6F\x20\x74\x72\x75\x65\x20\x74\x6F\x20\x70\x72\x65\x76\x65\x6E\x74\x20\x73\x75\x64\x64\x65\x6E\x20\x73\x63\x72\x6F\x6C\x6C\x69\x6E\x67\x20\x77\x68\x65\x6E\x20\x71\x75\x69\x63\x6B\x6C\x79\x20\x6C\x6F\x6F\x6B\x69\x6E\x67\x20\x61\x72\x6F\x75\x6E\x64\x2E\x20\x54\x68\x69\x73\x20\x6D\x61\x79\x20\x6D\x61\x6B\x65\x20\x73\x63\x72\x6F\x6C\x6C\x69\x6E\x67\x20\x66\x65\x65\x6C\x20\x6C\x65\x73\x73\x20\x72\x65\x73\x70\x6F\x6E\x73\x69\x76\x65\x20\x74\x68\x6F\x75\x67\x68\x2E"), NULL);
	}
}
static void ScrollRectTransf_tAC616AEB395D752AF7834443D37E44BCADE1406F_CustomAttributesCacheGenerator_skimProofUpdateSpeed(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 10.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6C\x6F\x77\x65\x72\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x2C\x20\x74\x68\x65\x20\x73\x6C\x6F\x77\x65\x72\x20\x74\x68\x65\x20\x73\x63\x72\x6F\x6C\x6C\x69\x6E\x67\x20\x77\x69\x6C\x6C\x20\x73\x70\x65\x65\x64\x20\x75\x70\x20\x61\x66\x74\x65\x72\x20\x73\x6B\x69\x6D\x6D\x69\x6E\x67\x2E\x20\x52\x65\x63\x6F\x6D\x6D\x65\x6E\x64\x65\x64\x20\x76\x61\x6C\x75\x65\x3A\x20\x35\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollRectTransf_tAC616AEB395D752AF7834443D37E44BCADE1406F_CustomAttributesCacheGenerator_customStartPos(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x75\x73\x74\x6F\x6D\x20\x61\x6E\x63\x68\x6F\x72\x20\x73\x74\x61\x72\x74\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollTexture_tAE1FDD084678A7B8C5B780057A93630AFD47560C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x53\x63\x72\x6F\x6C\x6C\x54\x65\x78\x74\x75\x72\x65"), NULL);
	}
}
static void ScrollTexture_tAE1FDD084678A7B8C5B780057A93630AFD47560C_CustomAttributesCacheGenerator_textureRendererToBeScrolled(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x6E\x64\x65\x72\x65\x72\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x65\x78\x74\x75\x72\x65\x20\x74\x6F\x20\x62\x65\x20\x73\x63\x72\x6F\x6C\x6C\x65\x64\x2E"), NULL);
	}
}
static void ScrollTexture_tAE1FDD084678A7B8C5B780057A93630AFD47560C_CustomAttributesCacheGenerator_autoGazeScrollIsActive(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x62\x69\x6C\x69\x74\x79\x20\x74\x6F\x20\x73\x63\x72\x6F\x6C\x6C\x20\x75\x73\x69\x6E\x67\x20\x79\x6F\x75\x72\x20\x65\x79\x65\x20\x67\x61\x7A\x65\x20\x77\x69\x74\x68\x6F\x75\x74\x20\x61\x6E\x79\x20\x61\x64\x64\x69\x74\x69\x6F\x6E\x61\x6C\x20\x69\x6E\x70\x75\x74\x20\x28\x65\x2E\x67\x2E\x2C\x20\x61\x69\x72\x20\x74\x61\x70\x20\x6F\x72\x20\x62\x75\x74\x74\x6F\x6E\x20\x70\x72\x65\x73\x73\x65\x73\x29\x2E"), NULL);
	}
}
static void ScrollTexture_tAE1FDD084678A7B8C5B780057A93630AFD47560C_CustomAttributesCacheGenerator_scrollSpeedHorizontal(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x72\x69\x7A\x6F\x6E\x74\x61\x6C\x20\x73\x63\x72\x6F\x6C\x6C\x20\x73\x70\x65\x65\x64\x2E\x20\x46\x6F\x72\x20\x65\x78\x61\x6D\x70\x6C\x65\x3A\x20\x30\x2E\x31\x66\x20\x66\x6F\x72\x20\x73\x6C\x6F\x77\x20\x70\x61\x6E\x6E\x69\x6E\x67\x2E\x20\x30\x2E\x36\x66\x20\x66\x6F\x72\x20\x66\x61\x73\x74\x20\x70\x61\x6E\x6E\x69\x6E\x67\x2E"), NULL);
	}
}
static void ScrollTexture_tAE1FDD084678A7B8C5B780057A93630AFD47560C_CustomAttributesCacheGenerator_scrollSpeedVertical(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x56\x65\x72\x74\x69\x63\x61\x6C\x20\x73\x63\x72\x6F\x6C\x6C\x20\x73\x70\x65\x65\x64\x2E\x20\x46\x6F\x72\x20\x65\x78\x61\x6D\x70\x6C\x65\x3A\x20\x30\x2E\x31\x66\x20\x66\x6F\x72\x20\x73\x6C\x6F\x77\x20\x70\x61\x6E\x6E\x69\x6E\x67\x2E\x20\x30\x2E\x36\x66\x20\x66\x6F\x72\x20\x66\x61\x73\x74\x20\x70\x61\x6E\x6E\x69\x6E\x67\x2E"), NULL);
	}
}
static void ScrollTexture_tAE1FDD084678A7B8C5B780057A93630AFD47560C_CustomAttributesCacheGenerator_minDistFromCenterForAutoScroll(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x61\x6C\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x69\x6E\x20\x78\x20\x61\x6E\x64\x20\x79\x20\x66\x72\x6F\x6D\x20\x63\x65\x6E\x74\x65\x72\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x27\x73\x20\x68\x69\x74\x20\x62\x6F\x78\x20\x28\x30\x2C\x20\x30\x29\x20\x74\x6F\x20\x73\x63\x72\x6F\x6C\x6C\x2E\x20\x54\x68\x75\x73\x2C\x20\x76\x61\x6C\x75\x65\x73\x20\x6D\x75\x73\x74\x20\x72\x61\x6E\x67\x65\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x30\x20\x28\x61\x6C\x77\x61\x79\x73\x20\x73\x63\x72\x6F\x6C\x6C\x29\x20\x61\x6E\x64\x20\x30\x2E\x35\x20\x28\x6E\x6F\x20\x73\x63\x72\x6F\x6C\x6C\x29\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ScrollTexture_tAE1FDD084678A7B8C5B780057A93630AFD47560C_CustomAttributesCacheGenerator_onLookAtColliderSize(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x69\x7A\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x47\x61\x6D\x65\x4F\x62\x6A\x65\x63\x74\x27\x73\x20\x63\x6F\x6C\x6C\x69\x64\x65\x72\x20\x77\x68\x65\x6E\x20\x62\x65\x69\x6E\x67\x20\x6C\x6F\x6F\x6B\x65\x64\x20\x61\x74\x2E"), NULL);
	}
}
static void ScrollTexture_tAE1FDD084678A7B8C5B780057A93630AFD47560C_CustomAttributesCacheGenerator_useSkimProofing(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x20\x74\x6F\x20\x74\x72\x75\x65\x20\x74\x6F\x20\x70\x72\x65\x76\x65\x6E\x74\x20\x73\x75\x64\x64\x65\x6E\x20\x73\x63\x72\x6F\x6C\x6C\x69\x6E\x67\x20\x77\x68\x65\x6E\x20\x71\x75\x69\x63\x6B\x6C\x79\x20\x6C\x6F\x6F\x6B\x69\x6E\x67\x20\x61\x72\x6F\x75\x6E\x64\x2E\x20\x54\x68\x69\x73\x20\x6D\x61\x79\x20\x6D\x61\x6B\x65\x20\x73\x63\x72\x6F\x6C\x6C\x69\x6E\x67\x20\x66\x65\x65\x6C\x20\x6C\x65\x73\x73\x20\x72\x65\x73\x70\x6F\x6E\x73\x69\x76\x65\x20\x74\x68\x6F\x75\x67\x68\x2E"), NULL);
	}
}
static void ScrollTexture_tAE1FDD084678A7B8C5B780057A93630AFD47560C_CustomAttributesCacheGenerator_skimProofUpdateSpeed(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 10.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x6C\x6F\x77\x65\x72\x20\x74\x68\x65\x20\x76\x61\x6C\x75\x65\x2C\x20\x74\x68\x65\x20\x73\x6C\x6F\x77\x65\x72\x20\x74\x68\x65\x20\x73\x63\x72\x6F\x6C\x6C\x69\x6E\x67\x20\x77\x69\x6C\x6C\x20\x73\x70\x65\x65\x64\x20\x75\x70\x20\x61\x66\x74\x65\x72\x20\x73\x6B\x69\x6D\x6D\x69\x6E\x67\x2E\x20\x52\x65\x63\x6F\x6D\x6D\x65\x6E\x64\x65\x64\x20\x76\x61\x6C\x75\x65\x3A\x20\x35\x2E"), NULL);
	}
}
static void TargetMoveToCamera_t0ABE76E6AC5EFA94348D8CD457C65D8DBA2EAB26_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EyeTrackingTarget_tC638599E70EEE5CDC5E0A3EAEE31E158C221C0FA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x54\x61\x72\x67\x65\x74\x4D\x6F\x76\x65\x54\x6F\x43\x61\x6D\x65\x72\x61"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(EyeTrackingTarget_tC638599E70EEE5CDC5E0A3EAEE31E158C221C0FA_0_0_0_var), NULL);
	}
}
static void TargetMoveToCamera_t0ABE76E6AC5EFA94348D8CD457C65D8DBA2EAB26_CustomAttributesCacheGenerator_speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TargetMoveToCamera_t0ABE76E6AC5EFA94348D8CD457C65D8DBA2EAB26_CustomAttributesCacheGenerator_isEnabled(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TargetMoveToCamera_t0ABE76E6AC5EFA94348D8CD457C65D8DBA2EAB26_CustomAttributesCacheGenerator_minDistToStopTransition(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TargetMoveToCamera_t0ABE76E6AC5EFA94348D8CD457C65D8DBA2EAB26_CustomAttributesCacheGenerator_setToAutoRotateIfFocused(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GrabReleaseDetector_tAE4C6AF4642DCC8AB2117205BE34A69EB5FAB642_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x4F\x62\x73\x6F\x6C\x65\x74\x65\x2F\x47\x72\x61\x62\x52\x65\x6C\x65\x61\x73\x65\x44\x65\x74\x65\x63\x74\x6F\x72"), NULL);
	}
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[1];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x69\x73\x20\x6E\x6F\x20\x6C\x6F\x6E\x67\x65\x72\x20\x73\x75\x70\x70\x6F\x72\x74\x65\x64"), true, NULL);
	}
}
static void GrabReleaseDetector_tAE4C6AF4642DCC8AB2117205BE34A69EB5FAB642_CustomAttributesCacheGenerator_OnGrab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GrabReleaseDetector_tAE4C6AF4642DCC8AB2117205BE34A69EB5FAB642_CustomAttributesCacheGenerator_OnRelease(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x4D\x6F\x76\x65\x4F\x62\x6A\x42\x79\x45\x79\x65\x47\x61\x7A\x65"), NULL);
	}
}
static void MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_useEyeSupportedTargetPlacement(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x79\x65\x73"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_minLookAwayDistToEnableEyeWarp(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 1.0f, 10.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x75\x73\x65\x72\x20\x68\x61\x73\x20\x74\x6F\x20\x6C\x6F\x6F\x6B\x20\x61\x77\x61\x79\x20\x61\x74\x20\x6C\x65\x61\x73\x74\x20\x74\x68\x69\x73\x20\x66\x61\x72\x20\x74\x6F\x20\x65\x6E\x61\x62\x6C\x65\x20\x74\x68\x65\x20\x65\x79\x65\x2D\x73\x75\x70\x70\x6F\x72\x74\x65\x64\x20\x74\x61\x72\x67\x65\x74\x20\x70\x6C\x61\x63\x65\x6D\x65\x6E\x74\x2E\x20\x54\x68\x69\x73\x20\x69\x73\x20\x74\x6F\x20\x61\x6C\x6C\x6F\x77\x20\x66\x6F\x72\x20\x6C\x6F\x63\x61\x6C\x20\x6D\x61\x6E\x75\x61\x6C\x20\x70\x6F\x73\x69\x74\x69\x6F\x6E\x69\x6E\x67\x20\x75\x73\x69\x6E\x67\x20\x68\x61\x6E\x64\x20\x69\x6E\x70\x75\x74\x2E"), NULL);
	}
}
static void MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_handInputEnabled(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x48\x61\x6E\x64\x73"), NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x55\x73\x65\x20\x74\x68\x69\x73\x20\x74\x6F\x20\x65\x6E\x66\x6F\x72\x63\x65\x20\x6F\x6E\x6C\x79\x20\x76\x6F\x69\x63\x65\x20\x63\x6F\x6D\x6D\x61\x6E\x64\x73\x20\x74\x6F\x20\x6D\x6F\x76\x65\x20\x74\x61\x72\x67\x65\x74\x73\x2E"), NULL);
	}
}
static void MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_handmapping(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x6F\x20\x63\x6F\x6E\x74\x72\x6F\x6C\x20\x77\x68\x65\x74\x68\x65\x72\x20\x74\x68\x65\x20\x68\x61\x6E\x64\x20\x6D\x6F\x74\x69\x6F\x6E\x20\x69\x73\x20\x75\x73\x65\x64\x20\x31\x3A\x31\x20\x74\x6F\x20\x6D\x6F\x76\x65\x20\x61\x20\x74\x61\x72\x67\x65\x74\x20\x6F\x72\x20\x74\x6F\x20\x75\x73\x65\x20\x64\x69\x66\x66\x65\x72\x65\x6E\x74\x20\x67\x61\x69\x6E\x73\x20\x74\x6F\x20\x61\x6C\x6C\x6F\x77\x20\x66\x6F\x72\x20\x73\x6D\x61\x6C\x6C\x65\x72\x20\x68\x61\x6E\x64\x20\x6D\x6F\x74\x69\x6F\x6E\x73\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_deltaHandMovemThresh(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x61\x6C\x20\x61\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x68\x61\x6E\x64\x20\x6D\x6F\x76\x65\x6D\x65\x6E\x74\x20\x74\x6F\x20\x74\x72\x69\x67\x67\x65\x72\x20\x74\x61\x72\x67\x65\x74\x20\x72\x65\x70\x6F\x73\x69\x74\x69\x6F\x6E\x69\x6E\x67\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_transparency_inTransition(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x61\x6E\x73\x70\x61\x72\x65\x6E\x63\x79\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x20\x69\x74\x73\x65\x6C\x66\x20\x77\x68\x69\x6C\x65\x20\x64\x72\x61\x67\x67\x69\x6E\x67\x20\x69\x73\x20\x61\x63\x74\x69\x76\x65\x2E"), NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[2];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x61\x6E\x73\x69\x74\x69\x6F\x6E\x69\x6E\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[3];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_transparency_preview(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x61\x6E\x73\x70\x61\x72\x65\x6E\x63\x79\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x20\x70\x72\x65\x76\x69\x65\x77\x20\x77\x68\x69\x6C\x65\x20\x64\x72\x61\x67\x67\x69\x6E\x67\x20\x69\x74\x20\x61\x72\x6F\x75\x6E\x64\x2E"), NULL);
	}
}
static void MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_previewPlacemDistThresh(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x61\x6C\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x74\x68\x65\x20\x6F\x6C\x64\x20\x61\x6E\x64\x20\x6E\x65\x77\x20\x70\x72\x65\x76\x69\x65\x77\x2E\x20\x54\x68\x69\x73\x20\x69\x73\x20\x74\x6F\x20\x70\x72\x65\x76\x65\x6E\x74\x20\x74\x68\x65\x20\x70\x72\x65\x76\x69\x65\x77\x20\x74\x6F\x20\x61\x6C\x77\x61\x79\x73\x20\x66\x6F\x6C\x6C\x6F\x77\x20\x74\x68\x65\x20\x65\x79\x65\x20\x67\x61\x7A\x65\x20\x69\x6D\x6D\x65\x64\x69\x61\x74\x65\x6C\x79\x2E\x20\x54\x68\x65\x20\x76\x61\x6C\x75\x65\x20\x73\x68\x6F\x75\x6C\x64\x20\x64\x65\x70\x65\x6E\x64\x20\x6F\x6E\x20\x74\x68\x65\x20\x73\x69\x7A\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_freezeX(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6E\x73\x74\x72\x61\x69\x6E\x65\x64\x20\x4D\x6F\x76\x65\x6D\x65\x6E\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_freezeY(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_freezeZ(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_audio_OnDragStart(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F\x20\x46\x65\x65\x64\x62\x61\x63\x6B"), NULL);
	}
}
static void MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_audio_OnDragStop(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_voiceAction_PutThis(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[1];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x76\x65\x6E\x74\x20\x48\x61\x6E\x64\x6C\x65\x72\x73"), NULL);
	}
}
static void MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_voiceAction_OverHere(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_OnDragStart(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_OnDrop(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_useAsSlider(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6C\x69\x64\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_txtOutput_sliderValue(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_slider_snapToNearestDecimal(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SnapTo_t4330BD61FEF351200A4FF00AABAA8B2FB888A2A6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x53\x6E\x61\x70\x54\x6F"), NULL);
	}
}
static void TransportToRespawnLocation_tFCBB88202A163EC84A401ADCBD723189884EDD87_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Collider_t5E81E43C2ECA0209A7C4528E84A632712D192B02_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x54\x72\x61\x6E\x73\x70\x6F\x72\x74\x54\x6F\x52\x65\x73\x70\x61\x77\x6E\x4C\x6F\x63\x61\x74\x69\x6F\x6E"), NULL);
	}
}
static void TransportToRespawnLocation_tFCBB88202A163EC84A401ADCBD723189884EDD87_CustomAttributesCacheGenerator_RespawnReference(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x72\x65\x66\x65\x72\x65\x6E\x63\x65\x64\x20\x67\x61\x6D\x65\x20\x6F\x62\x6A\x65\x63\x74\x73\x20\x61\x63\x74\x73\x20\x61\x73\x20\x61\x20\x70\x6C\x61\x63\x65\x68\x6F\x6C\x64\x65\x72\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x72\x65\x73\x70\x61\x77\x6E\x20\x6C\x6F\x63\x61\x74\x69\x6F\x6E\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TransportToRespawnLocation_tFCBB88202A163EC84A401ADCBD723189884EDD87_CustomAttributesCacheGenerator_AudioFX_OnRespawn(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x70\x74\x69\x6F\x6E\x61\x6C\x20\x61\x75\x64\x69\x6F\x20\x63\x6C\x69\x70\x20\x77\x68\x69\x63\x68\x20\x69\x73\x20\x70\x6C\x61\x79\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x20\x69\x73\x20\x72\x65\x73\x70\x61\x77\x6E\x65\x64\x2E"), NULL);
	}
}
static void TriggerZonePlaceObjsWithin_t33B1B0C9F2E911BCBD31114F6574B16AD7219047_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x54\x72\x69\x67\x67\x65\x72\x5A\x6F\x6E\x65\x50\x6C\x61\x63\x65\x4F\x62\x6A\x73\x57\x69\x74\x68\x69\x6E"), NULL);
	}
}
static void TriggerZonePlaceObjsWithin_t33B1B0C9F2E911BCBD31114F6574B16AD7219047_CustomAttributesCacheGenerator_ObjsToPlaceHere(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x72\x72\x61\x79\x20\x6F\x66\x20\x72\x65\x66\x65\x72\x65\x6E\x63\x65\x64\x20\x67\x61\x6D\x65\x20\x6F\x62\x6A\x65\x63\x74\x73\x20\x74\x68\x61\x74\x20\x61\x72\x65\x20\x73\x75\x70\x70\x6F\x73\x65\x64\x20\x74\x6F\x20\x62\x65\x20\x70\x6C\x61\x63\x65\x64\x20\x77\x69\x74\x68\x69\x6E\x20\x74\x68\x65\x20\x63\x6F\x6C\x6C\x69\x64\x65\x72\x20\x6F\x66\x20\x74\x68\x69\x73\x20\x74\x61\x72\x67\x65\x74\x2E\x29\x2E"), NULL);
	}
}
static void TriggerZonePlaceObjsWithin_t33B1B0C9F2E911BCBD31114F6574B16AD7219047_CustomAttributesCacheGenerator_statusColor_idle(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x6F\x72\x20\x6F\x66\x20\x74\x68\x69\x73\x20\x6F\x62\x6A\x65\x63\x74\x20\x77\x68\x65\x6E\x20\x27\x69\x64\x6C\x65\x27\x20\x2D\x20\x77\x61\x69\x74\x69\x6E\x67\x20\x6F\x6E\x20\x74\x68\x65\x20\x63\x6F\x72\x72\x65\x63\x74\x20\x74\x61\x72\x67\x65\x74\x73\x20\x74\x6F\x20\x62\x65\x20\x70\x6C\x61\x63\x65\x64\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TriggerZonePlaceObjsWithin_t33B1B0C9F2E911BCBD31114F6574B16AD7219047_CustomAttributesCacheGenerator_statusColor_achieved(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x6F\x72\x20\x6F\x66\x20\x74\x68\x69\x73\x20\x6F\x62\x6A\x65\x63\x74\x20\x6F\x6E\x63\x65\x20\x61\x6C\x6C\x20\x72\x65\x71\x75\x65\x73\x74\x65\x64\x20\x74\x61\x72\x67\x65\x74\x73\x20\x68\x61\x76\x65\x20\x62\x65\x65\x6E\x20\x70\x6C\x61\x63\x65\x64\x20\x77\x69\x74\x68\x69\x6E\x20\x74\x68\x65\x20\x74\x72\x69\x67\x67\x65\x72\x20\x7A\x6F\x6E\x65\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TriggerZonePlaceObjsWithin_t33B1B0C9F2E911BCBD31114F6574B16AD7219047_CustomAttributesCacheGenerator_AudioFx_Success(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x70\x74\x69\x6F\x6E\x61\x6C\x20\x61\x75\x64\x69\x6F\x20\x63\x6C\x69\x70\x20\x74\x6F\x20\x62\x65\x20\x70\x6C\x61\x79\x65\x64\x20\x6F\x6E\x63\x65\x20\x61\x6C\x6C\x20\x72\x65\x71\x75\x65\x73\x74\x65\x64\x20\x6F\x62\x6A\x65\x63\x74\x73\x20\x68\x61\x76\x65\x20\x62\x65\x65\x6E\x20\x63\x6F\x72\x72\x65\x63\x74\x6C\x79\x20\x70\x6C\x61\x63\x65\x64\x2E"), NULL);
	}
}
static void HitBehaviorDestroyOnSelect_t95899B83013C85EC186992EE1AABC94DE2311CF4_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EyeTrackingTarget_tC638599E70EEE5CDC5E0A3EAEE31E158C221C0FA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x48\x69\x74\x42\x65\x68\x61\x76\x69\x6F\x72\x44\x65\x73\x74\x72\x6F\x79\x4F\x6E\x53\x65\x6C\x65\x63\x74"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(EyeTrackingTarget_tC638599E70EEE5CDC5E0A3EAEE31E158C221C0FA_0_0_0_var), NULL);
	}
}
static void HitBehaviorDestroyOnSelect_t95899B83013C85EC186992EE1AABC94DE2311CF4_CustomAttributesCacheGenerator_visualFxTemplate_OnHit(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x56\x69\x73\x75\x61\x6C\x20\x65\x66\x66\x65\x63\x74\x20\x28\x65\x2E\x67\x2E\x2C\x20\x70\x61\x72\x74\x69\x63\x6C\x65\x20\x65\x78\x70\x6C\x6F\x73\x69\x6F\x6E\x20\x6F\x72\x20\x61\x6E\x69\x6D\x61\x74\x69\x6F\x6E\x29\x20\x74\x68\x61\x74\x20\x69\x73\x20\x70\x6C\x61\x79\x65\x64\x20\x77\x68\x65\x6E\x20\x61\x20\x74\x61\x72\x67\x65\x74\x20\x69\x73\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HitBehaviorDestroyOnSelect_t95899B83013C85EC186992EE1AABC94DE2311CF4_CustomAttributesCacheGenerator_audioFx_CorrectTarget(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F\x20\x63\x6C\x69\x70\x20\x74\x68\x61\x74\x20\x69\x73\x20\x70\x6C\x61\x79\x65\x64\x20\x77\x68\x65\x6E\x20\x61\x20\x74\x61\x72\x67\x65\x74\x20\x69\x73\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x2E"), NULL);
	}
}
static void HitBehaviorDestroyOnSelect_t95899B83013C85EC186992EE1AABC94DE2311CF4_CustomAttributesCacheGenerator_audioFx_IncorrectTarget(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x75\x64\x69\x6F\x20\x63\x6C\x69\x70\x20\x74\x68\x61\x74\x20\x69\x73\x20\x70\x6C\x61\x79\x65\x64\x20\x77\x68\x65\x6E\x20\x61\x20\x77\x72\x6F\x6E\x67\x20\x74\x61\x72\x67\x65\x74\x20\x69\x73\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x2E"), NULL);
	}
}
static void HitBehaviorDestroyOnSelect_t95899B83013C85EC186992EE1AABC94DE2311CF4_CustomAttributesCacheGenerator_is_a_valid_target(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x6E\x75\x61\x6C\x6C\x79\x20\x69\x6E\x64\x69\x63\x61\x74\x65\x20\x77\x68\x65\x74\x68\x65\x72\x20\x74\x68\x69\x73\x20\x69\x73\x20\x61\x6E\x20\x69\x6E\x63\x6F\x72\x72\x65\x63\x74\x20\x74\x61\x72\x67\x65\x74\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HitBehaviorDestroyOnSelect_t95899B83013C85EC186992EE1AABC94DE2311CF4_CustomAttributesCacheGenerator_targetIterator(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x73\x73\x6F\x63\x69\x61\x74\x65\x64\x20\x54\x61\x72\x67\x65\x74\x47\x72\x69\x64\x49\x74\x65\x72\x61\x74\x6F\x72\x20\x74\x6F\x20\x63\x68\x65\x63\x6B\x20\x77\x68\x65\x74\x68\x65\x72\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x6C\x79\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x20\x74\x61\x72\x67\x65\x74\x20\x69\x73\x20\x74\x68\x65\x20\x63\x6F\x72\x72\x65\x63\x74\x20\x6F\x6E\x65\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RotateWithConstSpeedDir_t30194E08C6AB4143A2038469E3B5D2D6CAE3608A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x52\x6F\x74\x61\x74\x65\x57\x69\x74\x68\x43\x6F\x6E\x73\x74\x53\x70\x65\x65\x64\x44\x69\x72"), NULL);
	}
}
static void RotateWithConstSpeedDir_t30194E08C6AB4143A2038469E3B5D2D6CAE3608A_CustomAttributesCacheGenerator_RotateByEulerAngles(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x75\x6C\x65\x72\x20\x61\x6E\x67\x6C\x65\x73\x20\x62\x79\x20\x77\x68\x69\x63\x68\x20\x74\x68\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x73\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x72\x6F\x74\x61\x74\x65\x64\x20\x62\x79\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RotateWithConstSpeedDir_t30194E08C6AB4143A2038469E3B5D2D6CAE3608A_CustomAttributesCacheGenerator_speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x6F\x74\x61\x74\x69\x6F\x6E\x20\x73\x70\x65\x65\x64\x20\x66\x61\x63\x74\x6F\x72\x2E"), NULL);
	}
}
static void TargetGroupCreatorRadial_t980AFA83ECED3B9654D24511FEB07DAC3118D9F1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x54\x61\x72\x67\x65\x74\x47\x72\x6F\x75\x70\x43\x72\x65\x61\x74\x6F\x72\x52\x61\x64\x69\x61\x6C"), NULL);
	}
}
static void TargetGroupCreatorRadial_t980AFA83ECED3B9654D24511FEB07DAC3118D9F1_CustomAttributesCacheGenerator_templates(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x61\x72\x67\x65\x74\x20\x74\x65\x6D\x70\x6C\x61\x74\x65\x73\x20\x66\x72\x6F\x6D\x20\x77\x68\x69\x63\x68\x20\x74\x68\x65\x20\x67\x72\x6F\x75\x70\x20\x6F\x66\x20\x74\x61\x72\x67\x65\x74\x73\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x63\x72\x65\x61\x74\x65\x64\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TargetGroupCreatorRadial_t980AFA83ECED3B9654D24511FEB07DAC3118D9F1_CustomAttributesCacheGenerator_targetSizeInVisAngle(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x73\x69\x7A\x65\x20\x6F\x66\x20\x74\x61\x72\x67\x65\x74\x73\x20\x69\x6E\x20\x76\x69\x73\x75\x61\x6C\x20\x61\x6E\x67\x6C\x65\x2E"), NULL);
	}
}
static void TargetGroupCreatorRadial_t980AFA83ECED3B9654D24511FEB07DAC3118D9F1_CustomAttributesCacheGenerator_keepVisAngleSizeConstant(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x2C\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x20\x73\x69\x7A\x65\x73\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x63\x6F\x6E\x74\x69\x6E\x75\x6F\x75\x73\x6C\x79\x20\x61\x64\x6A\x75\x73\x74\x65\x64\x20\x74\x6F\x20\x6B\x65\x65\x70\x20\x74\x68\x65\x20\x73\x69\x7A\x65\x20\x69\x6E\x20\x76\x69\x73\x75\x61\x6C\x20\x61\x6E\x67\x6C\x65\x20\x63\x6F\x6E\x73\x74\x61\x6E\x74\x2E"), NULL);
	}
}
static void TargetGroupCreatorRadial_t980AFA83ECED3B9654D24511FEB07DAC3118D9F1_CustomAttributesCacheGenerator_keepTargetsFacingTheCam(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x2C\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x73\x20\x77\x69\x6C\x6C\x20\x6B\x65\x65\x70\x20\x66\x61\x63\x69\x6E\x67\x20\x74\x68\x65\x20\x75\x73\x65\x72\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TargetGroupCreatorRadial_t980AFA83ECED3B9654D24511FEB07DAC3118D9F1_CustomAttributesCacheGenerator_hideTemplatesOnStartup(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x2C\x20\x74\x68\x65\x20\x74\x65\x6D\x70\x6C\x61\x74\x65\x73\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x68\x69\x64\x64\x65\x6E\x20\x6F\x6E\x20\x73\x74\x61\x72\x74\x75\x70\x2E"), NULL);
	}
}
static void TargetGroupCreatorRadial_t980AFA83ECED3B9654D24511FEB07DAC3118D9F1_CustomAttributesCacheGenerator_radialLayout_nTargets(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x74\x61\x72\x67\x65\x74\x73\x20\x70\x65\x72\x20\x72\x69\x6E\x67\x2E"), NULL);
	}
}
static void TargetGroupCreatorRadial_t980AFA83ECED3B9654D24511FEB07DAC3118D9F1_CustomAttributesCacheGenerator_radialLayout_radiusInVisAngle(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x6E\x20\x61\x72\x72\x61\x79\x20\x6F\x66\x20\x72\x61\x64\x69\x69\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x63\x6F\x6E\x63\x65\x6E\x74\x72\x69\x63\x20\x72\x69\x6E\x67\x73\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TargetGroupCreatorRadial_t980AFA83ECED3B9654D24511FEB07DAC3118D9F1_CustomAttributesCacheGenerator_showTargetAtGroupCenter(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x2C\x20\x73\x68\x6F\x77\x20\x61\x20\x74\x61\x72\x67\x65\x74\x20\x61\x6C\x73\x6F\x20\x61\x74\x20\x74\x68\x65\x20\x63\x65\x6E\x74\x65\x72\x20\x6F\x66\x20\x74\x68\x65\x20\x72\x69\x6E\x67\x73\x2E"), NULL);
	}
}
static void TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&TargetGroupCreatorRadial_t980AFA83ECED3B9654D24511FEB07DAC3118D9F1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x54\x61\x72\x67\x65\x74\x47\x72\x6F\x75\x70\x49\x74\x65\x72\x61\x74\x6F\x72"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(TargetGroupCreatorRadial_t980AFA83ECED3B9654D24511FEB07DAC3118D9F1_0_0_0_var), NULL);
	}
}
static void TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_nrOfTargetsToSelect(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x61\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x74\x61\x72\x67\x65\x74\x73\x20\x74\x6F\x20\x73\x65\x6C\x65\x63\x74\x20\x28\x74\x68\x65\x20\x61\x6D\x6F\x75\x6E\x74\x20\x6F\x66\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x20\x74\x61\x72\x67\x65\x74\x73\x20\x6D\x61\x79\x20\x62\x65\x20\x68\x69\x67\x68\x65\x72\x29\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_Randomize(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x6E\x64\x6F\x6D\x69\x7A\x65\x20\x74\x68\x65\x20\x6F\x72\x64\x65\x72\x20\x6F\x66\x20\x74\x61\x72\x67\x65\x74\x73\x20\x74\x6F\x20\x73\x65\x6C\x65\x63\x74\x2E"), NULL);
	}
}
static void TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_highlightColor(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x68\x69\x67\x68\x6C\x69\x67\x68\x74\x20\x63\x6F\x6C\x6F\x72\x20\x74\x6F\x20\x69\x6E\x64\x69\x63\x61\x74\x65\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x71\x75\x65\x72\x79\x20\x74\x61\x72\x67\x65\x74\x2E"), NULL);
	}
}
static void TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_maxNumberOfTries(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x20\x6E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x74\x72\x69\x65\x73\x20\x66\x6F\x72\x20\x73\x65\x6C\x65\x63\x74\x69\x6E\x67\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x71\x75\x65\x72\x79\x20\x74\x61\x72\x67\x65\x74\x20\x62\x65\x66\x6F\x72\x65\x20\x61\x75\x74\x6F\x6D\x61\x74\x69\x63\x61\x6C\x6C\x79\x20\x70\x72\x6F\x63\x65\x65\x64\x69\x6E\x67\x2E"), NULL);
	}
}
static void TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_DeactiveDistractors(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x5B\x4F\x70\x74\x69\x6F\x6E\x61\x6C\x5D\x20\x44\x65\x61\x63\x74\x69\x76\x61\x74\x65\x20\x74\x68\x65\x20\x71\x75\x65\x72\x79\x20\x74\x61\x72\x67\x65\x74\x20\x61\x66\x74\x65\x72\x20\x69\x74\x27\x73\x20\x62\x65\x65\x6E\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_template_VisualMarkerForCurrTarget(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x5B\x4F\x70\x74\x69\x6F\x6E\x61\x6C\x5D\x20\x54\x65\x6D\x70\x6C\x61\x74\x65\x20\x47\x61\x6D\x65\x4F\x62\x6A\x65\x63\x74\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x20\x61\x74\x20\x74\x68\x65\x20\x63\x65\x6E\x74\x65\x72\x20\x6F\x66\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x71\x75\x65\x72\x79\x20\x74\x61\x72\x67\x65\x74\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_SceneToLoadOnFinish(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x5B\x4F\x70\x74\x69\x6F\x6E\x61\x6C\x5D\x20\x4E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x65\x6E\x65\x20\x74\x6F\x20\x6C\x6F\x61\x64\x20\x61\x66\x74\x65\x72\x20\x66\x69\x6E\x69\x73\x68\x69\x6E\x67\x20\x73\x65\x6C\x65\x63\x74\x69\x6E\x67\x20\x74\x68\x65\x20\x73\x70\x65\x63\x69\x66\x69\x65\x64\x20\x6E\x75\x6D\x62\x65\x72\x20\x6F\x66\x20\x74\x61\x72\x67\x65\x74\x73\x2E"), NULL);
	}
}
static void TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_AudioApplauseOnFinish(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x5B\x4F\x70\x74\x69\x6F\x6E\x61\x6C\x5D\x20\x41\x75\x64\x69\x6F\x20\x63\x6C\x69\x70\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x70\x6C\x61\x79\x65\x64\x20\x61\x66\x74\x65\x72\x20\x73\x65\x6C\x65\x63\x74\x69\x6E\x67\x20\x61\x6C\x6C\x20\x72\x65\x71\x75\x69\x72\x65\x64\x20\x74\x61\x72\x67\x65\x74\x73\x2E"), NULL);
	}
}
static void TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_OnAllTargetsSelected(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_OnTargetSelected(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_selectAction(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_TargetGroupIterator_add_OnAllTargetsSelected_m6064AFDC7FD0A4F707C0C0B66CD34F2AA7BF872F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_TargetGroupIterator_remove_OnAllTargetsSelected_m5AF09D842ECCE703EF50F6A8731AB37D8533AE62(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_TargetGroupIterator_add_OnTargetSelected_mBBDB4D90C8A878BAF5BFCF91388D545305DA2BD9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_TargetGroupIterator_remove_OnTargetSelected_m18F381E83CBF9A077ABBAE7A92638A6AB32DED47(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ToggleGameObject_t9969D493884F94B1CF86F1F834BD6E7271E047C0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x54\x6F\x67\x67\x6C\x65\x47\x61\x6D\x65\x4F\x62\x6A\x65\x63\x74"), NULL);
	}
}
static void ToggleGameObject_t9969D493884F94B1CF86F1F834BD6E7271E047C0_CustomAttributesCacheGenerator_objToShowHide(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DrawOnTexture_t99BDA5310DF2AC6F885B874496A2537ED869005C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x44\x72\x61\x77\x4F\x6E\x54\x65\x78\x74\x75\x72\x65"), NULL);
	}
}
static void DrawOnTexture_t99BDA5310DF2AC6F885B874496A2537ED869005C_CustomAttributesCacheGenerator_drawBrushSize(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DrawOnTexture_t99BDA5310DF2AC6F885B874496A2537ED869005C_CustomAttributesCacheGenerator_drawIntensity(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DrawOnTexture_t99BDA5310DF2AC6F885B874496A2537ED869005C_CustomAttributesCacheGenerator_minThreshDeltaHeatMap(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DrawOnTexture_t99BDA5310DF2AC6F885B874496A2537ED869005C_CustomAttributesCacheGenerator_DrawOnTexture_DrawAt_m5CCF53A676F5EF6B8E413C510A70B1ABD8EB42DD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDrawAtU3Ed__19_tF9694B76A7C548907397EF887A0AF5273444C7A1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDrawAtU3Ed__19_tF9694B76A7C548907397EF887A0AF5273444C7A1_0_0_0_var), NULL);
	}
}
static void DrawOnTexture_t99BDA5310DF2AC6F885B874496A2537ED869005C_CustomAttributesCacheGenerator_DrawOnTexture_ComputeHeatmapAt_m4234A80A7ECCD004A0549561897E1FDCBF25898E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CComputeHeatmapAtU3Ed__20_t57778DEFEFEA9FE64A0801F0D2B0B59F4398058F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CComputeHeatmapAtU3Ed__20_t57778DEFEFEA9FE64A0801F0D2B0B59F4398058F_0_0_0_var), NULL);
	}
}
static void U3CDrawAtU3Ed__19_tF9694B76A7C548907397EF887A0AF5273444C7A1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDrawAtU3Ed__19_tF9694B76A7C548907397EF887A0AF5273444C7A1_CustomAttributesCacheGenerator_U3CDrawAtU3Ed__19__ctor_m0669E9BEA6952B3F3DD94D06CBB8ABE617E39B24(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDrawAtU3Ed__19_tF9694B76A7C548907397EF887A0AF5273444C7A1_CustomAttributesCacheGenerator_U3CDrawAtU3Ed__19_System_IDisposable_Dispose_m6E4D0EBCBAE804E9BCE39A65E328D04E34213B52(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDrawAtU3Ed__19_tF9694B76A7C548907397EF887A0AF5273444C7A1_CustomAttributesCacheGenerator_U3CDrawAtU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB8FE5A3C65743AF09977A975BCC7D237891A637(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDrawAtU3Ed__19_tF9694B76A7C548907397EF887A0AF5273444C7A1_CustomAttributesCacheGenerator_U3CDrawAtU3Ed__19_System_Collections_IEnumerator_Reset_m928AB931B01F942ECA5D6B39A97E2DDB4B316A66(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDrawAtU3Ed__19_tF9694B76A7C548907397EF887A0AF5273444C7A1_CustomAttributesCacheGenerator_U3CDrawAtU3Ed__19_System_Collections_IEnumerator_get_Current_mCF5A234BE92720D5D3143BAA4F6FA6A2E7C5C7C2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CComputeHeatmapAtU3Ed__20_t57778DEFEFEA9FE64A0801F0D2B0B59F4398058F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CComputeHeatmapAtU3Ed__20_t57778DEFEFEA9FE64A0801F0D2B0B59F4398058F_CustomAttributesCacheGenerator_U3CComputeHeatmapAtU3Ed__20__ctor_mB446BCBDF6A86972E2CCA7C4D41D2BC187574100(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CComputeHeatmapAtU3Ed__20_t57778DEFEFEA9FE64A0801F0D2B0B59F4398058F_CustomAttributesCacheGenerator_U3CComputeHeatmapAtU3Ed__20_System_IDisposable_Dispose_m037E1BB2B14C2C41D8DC547D74349C2DB0BE76FA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CComputeHeatmapAtU3Ed__20_t57778DEFEFEA9FE64A0801F0D2B0B59F4398058F_CustomAttributesCacheGenerator_U3CComputeHeatmapAtU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01119328E1F744FF691CE5485CC2BADFC8766CC2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CComputeHeatmapAtU3Ed__20_t57778DEFEFEA9FE64A0801F0D2B0B59F4398058F_CustomAttributesCacheGenerator_U3CComputeHeatmapAtU3Ed__20_System_Collections_IEnumerator_Reset_mAF888F7D9CB6C49243C9B4D75E73BBB14A77D906(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CComputeHeatmapAtU3Ed__20_t57778DEFEFEA9FE64A0801F0D2B0B59F4398058F_CustomAttributesCacheGenerator_U3CComputeHeatmapAtU3Ed__20_System_Collections_IEnumerator_get_Current_mA0A6E63023754E4F80F25FB667CE8E119770D05A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void OnSelectVisualizerInputController_tB9FBC5958B594373F7A2B42AAE300CA8F961B226_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EyeTrackingTarget_tC638599E70EEE5CDC5E0A3EAEE31E158C221C0FA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(EyeTrackingTarget_tC638599E70EEE5CDC5E0A3EAEE31E158C221C0FA_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x4F\x62\x73\x6F\x6C\x65\x74\x65\x2F\x4F\x6E\x53\x65\x6C\x65\x63\x74\x56\x69\x73\x75\x61\x6C\x69\x7A\x65\x72\x49\x6E\x70\x75\x74\x43\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72"), NULL);
	}
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[2];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x69\x73\x20\x6E\x6F\x20\x6C\x6F\x6E\x67\x65\x72\x20\x73\x75\x70\x70\x6F\x72\x74\x65\x64"), true, NULL);
	}
}
static void OnSelectVisualizerInputController_tB9FBC5958B594373F7A2B42AAE300CA8F961B226_CustomAttributesCacheGenerator_EventToTrigger(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ParticleHeatmap_t6206EFF67123720F3FEE9DE9521D92DBA361DB9F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x50\x61\x72\x74\x69\x63\x6C\x65\x48\x65\x61\x74\x6D\x61\x70"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(ParticleSystem_t2F526CCDBD3512879B3FCBE04BCAB20D7B4F391E_0_0_0_var), NULL);
	}
}
static void ParticleHeatmap_t6206EFF67123720F3FEE9DE9521D92DBA361DB9F_CustomAttributesCacheGenerator_colorGradient(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ParticleHeatmap_t6206EFF67123720F3FEE9DE9521D92DBA361DB9F_CustomAttributesCacheGenerator_maxNumberOfParticles(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ParticleHeatmap_t6206EFF67123720F3FEE9DE9521D92DBA361DB9F_CustomAttributesCacheGenerator_minParticleSize(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ParticleHeatmap_t6206EFF67123720F3FEE9DE9521D92DBA361DB9F_CustomAttributesCacheGenerator_maxParticleSize(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AudioFeedbackPlayer_t5B3ACA94A40A08A9873B04A9F76A8B50AAA362A7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x41\x75\x64\x69\x6F\x46\x65\x65\x64\x62\x61\x63\x6B\x50\x6C\x61\x79\x65\x72"), NULL);
	}
}
static void AudioFeedbackPlayer_t5B3ACA94A40A08A9873B04A9F76A8B50AAA362A7_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void AudioFeedbackPlayer_t5B3ACA94A40A08A9873B04A9F76A8B50AAA362A7_CustomAttributesCacheGenerator_AudioFeedbackPlayer_get_Instance_m714F2501DD250566ABEF9269938D4F9FEFDBF577(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AudioFeedbackPlayer_t5B3ACA94A40A08A9873B04A9F76A8B50AAA362A7_CustomAttributesCacheGenerator_AudioFeedbackPlayer_set_Instance_m3C0D461B9B45905B85975D30EFB5DCAACACAC802(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FollowEyeGaze_t99F955746D639DF98AB3AB29C03B3853BF54FB0C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x46\x6F\x6C\x6C\x6F\x77\x45\x79\x65\x47\x61\x7A\x65"), NULL);
	}
}
static void FollowEyeGaze_t99F955746D639DF98AB3AB29C03B3853BF54FB0C_CustomAttributesCacheGenerator_defaultDistanceInMeters(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x73\x70\x6C\x61\x79\x20\x74\x68\x65\x20\x67\x61\x6D\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x61\x6C\x6F\x6E\x67\x20\x74\x68\x65\x20\x65\x79\x65\x20\x67\x61\x7A\x65\x20\x72\x61\x79\x20\x61\x74\x20\x61\x20\x64\x65\x66\x61\x75\x6C\x74\x20\x64\x69\x73\x74\x61\x6E\x63\x65\x20\x28\x69\x6E\x20\x6D\x65\x74\x65\x72\x73\x29\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SpeechVisualFeedback_t37EFE8BCEA67D376CC90165094981C0182F11D4C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x53\x70\x65\x65\x63\x68\x56\x69\x73\x75\x61\x6C\x46\x65\x65\x64\x62\x61\x63\x6B"), NULL);
	}
}
static void SpeechVisualFeedback_t37EFE8BCEA67D376CC90165094981C0182F11D4C_CustomAttributesCacheGenerator_visualFeedbackTemplate(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x63\x74\x73\x20\x61\x73\x20\x74\x68\x65\x20\x74\x65\x6D\x70\x6C\x61\x74\x65\x20\x77\x68\x69\x63\x68\x20\x77\x69\x6C\x6C\x20\x73\x68\x6F\x77\x20\x74\x68\x65\x20\x73\x70\x65\x65\x63\x68\x20\x63\x6F\x6D\x6D\x61\x6E\x64\x20\x74\x68\x61\x74\x20\x74\x68\x65\x20\x73\x79\x73\x74\x65\x6D\x20\x75\x6E\x64\x65\x72\x73\x74\x6F\x6F\x64\x2E"), NULL);
	}
}
static void SpeechVisualFeedback_t37EFE8BCEA67D376CC90165094981C0182F11D4C_CustomAttributesCacheGenerator_maxShowtimeInSeconds(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x64\x75\x72\x61\x74\x69\x6F\x6E\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x66\x6F\x72\x20\x77\x68\x69\x63\x68\x20\x74\x68\x65\x20\x76\x69\x73\x75\x61\x6C\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x69\x73\x20\x73\x68\x6F\x77\x6E\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BlendOut_tB7E293CB36E83E19E2870E11705BE16E5DF0900C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x42\x6C\x65\x6E\x64\x4F\x75\x74"), NULL);
	}
}
static void BlendOut_tB7E293CB36E83E19E2870E11705BE16E5DF0900C_CustomAttributesCacheGenerator_BlendOutSpeed(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x65\x65\x64\x20\x66\x6F\x72\x20\x62\x6C\x65\x6E\x64\x69\x6E\x67\x20\x6F\x75\x74\x20\x74\x61\x72\x67\x65\x74\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BlendOut_tB7E293CB36E83E19E2870E11705BE16E5DF0900C_CustomAttributesCacheGenerator_MinTransparency(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x61\x6C\x20\x74\x72\x61\x6E\x73\x70\x61\x72\x65\x6E\x63\x79\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x30\x20\x61\x6E\x64\x20\x31\x2E"), NULL);
	}
}
static void BlendOut_tB7E293CB36E83E19E2870E11705BE16E5DF0900C_CustomAttributesCacheGenerator_LookAtTransparency(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x72\x61\x6E\x73\x70\x61\x72\x65\x6E\x63\x79\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x30\x20\x61\x6E\x64\x20\x31\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x73\x65\x74\x20\x6F\x6E\x63\x65\x20\x74\x68\x65\x20\x75\x73\x65\x72\x20\x6C\x6F\x6F\x6B\x73\x20\x61\x74\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BlendOut_tB7E293CB36E83E19E2870E11705BE16E5DF0900C_CustomAttributesCacheGenerator_IdleTransparency(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x64\x6C\x65\x20\x74\x72\x61\x6E\x73\x70\x61\x72\x65\x6E\x63\x79\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x30\x20\x61\x6E\x64\x20\x31\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x73\x65\x74\x20\x69\x66\x20\x74\x68\x65\x20\x75\x73\x65\x72\x20\x6C\x6F\x6F\x6B\x73\x20\x61\x77\x61\x79\x20\x66\x72\x6F\x6D\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x2C\x20\x62\x75\x74\x20\x64\x69\x64\x6E\xE2\x80\x99\x74\x20\x6C\x6F\x6F\x6B\x20\x61\x74\x20\x69\x74\x20\x6C\x6F\x6E\x67\x20\x65\x6E\x6F\x75\x67\x68\x20\x74\x6F\x20\x62\x65\x20\x63\x6F\x6E\x73\x69\x64\x65\x72\x65\x64\x20\xE2\x80\x9C\x66\x75\x6C\x6C\x79\x20\x65\x6E\x67\x61\x67\x65\x64\xE2\x80\x9D\x2E\x20\x49\x6E\x20\x74\x68\x69\x73\x20\x63\x61\x73\x65\x2C\x20\x69\x74\x20\x73\x69\x6D\x70\x6C\x79\x20\x72\x65\x74\x75\x72\x6E\x73\x20\x74\x6F\x20\x61\x6E\x20\x69\x64\x6C\x65\x20\x73\x74\x61\x74\x65\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BlendOut_tB7E293CB36E83E19E2870E11705BE16E5DF0900C_CustomAttributesCacheGenerator_DestroyAfterBlendOut(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x42\x6F\x6F\x6C\x65\x61\x6E\x20\x74\x6F\x20\x64\x65\x63\x69\x64\x65\x20\x77\x68\x65\x74\x68\x65\x72\x20\x74\x6F\x20\x64\x65\x73\x74\x72\x6F\x79\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x20\x6F\x6E\x63\x65\x20\x74\x68\x65\x20\x62\x6C\x65\x6E\x64\x20\x6F\x75\x74\x20\x69\x73\x20\x63\x6F\x6D\x70\x6C\x65\x74\x65\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BlendOut_tB7E293CB36E83E19E2870E11705BE16E5DF0900C_CustomAttributesCacheGenerator_DisableAfterBlendOut(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x42\x6F\x6F\x6C\x65\x61\x6E\x20\x74\x6F\x20\x64\x65\x63\x69\x64\x65\x20\x77\x68\x65\x74\x68\x65\x72\x20\x74\x6F\x20\x64\x69\x73\x61\x62\x6C\x65\x20\x74\x68\x65\x20\x67\x61\x6D\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x6F\x6E\x63\x65\x20\x74\x68\x65\x20\x62\x6C\x65\x6E\x64\x20\x6F\x75\x74\x20\x69\x73\x20\x63\x6F\x6D\x70\x6C\x65\x74\x65\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BlendOut_tB7E293CB36E83E19E2870E11705BE16E5DF0900C_CustomAttributesCacheGenerator_DwellRequired(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x42\x6F\x6F\x6C\x65\x61\x6E\x20\x74\x6F\x20\x64\x65\x63\x69\x64\x65\x20\x77\x68\x65\x74\x68\x65\x72\x20\x61\x20\x64\x77\x65\x6C\x6C\x20\x69\x73\x20\x72\x65\x71\x75\x69\x72\x65\x64\x20\x74\x6F\x20\x64\x69\x73\x65\x6E\x67\x61\x67\x65\x2E"), NULL);
	}
}
static void BlendOut_tB7E293CB36E83E19E2870E11705BE16E5DF0900C_CustomAttributesCacheGenerator_shaderPropsToCheckForBlending(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x70\x65\x6E\x64\x69\x6E\x67\x20\x6F\x6E\x20\x74\x68\x65\x20\x6D\x61\x74\x65\x72\x69\x61\x6C\x73\x20\x61\x70\x70\x6C\x69\x65\x64\x20\x74\x6F\x20\x74\x68\x65\x20\x6E\x6F\x74\x69\x66\x69\x63\x61\x74\x69\x6F\x6E\x2C\x20\x77\x65\x20\x6E\x65\x65\x64\x20\x74\x6F\x20\x63\x68\x65\x63\x6B\x20\x73\x70\x65\x63\x69\x66\x69\x63\x20\x73\x75\x70\x70\x6F\x72\x74\x65\x64\x20\x70\x72\x6F\x70\x65\x72\x74\x69\x65\x73\x20\x66\x6F\x72\x20\x62\x6C\x65\x6E\x64\x69\x6E\x67\x2E"), NULL);
	}
}
static void ChangeSize_t49BFDAE8C90A63E56A2579AC04FA12E909FE89C0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x43\x68\x61\x6E\x67\x65\x53\x69\x7A\x65"), NULL);
	}
}
static void ChangeSize_t49BFDAE8C90A63E56A2579AC04FA12E909FE89C0_CustomAttributesCacheGenerator_Size_OnLookAt(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x69\x6E\x61\x6C\x20\x73\x69\x7A\x65\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x20\x69\x73\x20\x6C\x6F\x6F\x6B\x65\x64\x20\x61\x74\x2E"), NULL);
	}
}
static void ChangeSize_t49BFDAE8C90A63E56A2579AC04FA12E909FE89C0_CustomAttributesCacheGenerator_SizeChangeSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x70\x65\x65\x64\x20\x66\x61\x63\x74\x6F\x72\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x6D\x75\x6C\x74\x69\x70\x6C\x69\x65\x64\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x64\x65\x6C\x74\x61\x20\x74\x69\x6D\x65\x20\x74\x6F\x20\x61\x64\x61\x70\x74\x20\x73\x70\x65\x65\x64\x20\x6F\x66\x20\x63\x68\x61\x6E\x67\x65\x2E"), NULL);
	}
}
static void ChangeSize_t49BFDAE8C90A63E56A2579AC04FA12E909FE89C0_CustomAttributesCacheGenerator_SizeDeltaThresh(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x68\x65\x20\x64\x65\x6C\x74\x61\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x74\x68\x65\x20\x63\x75\x72\x72\x65\x6E\x74\x20\x73\x63\x61\x6C\x65\x20\x61\x6E\x64\x20\x74\x68\x65\x20\x66\x69\x6E\x61\x6C\x20\x73\x63\x61\x6C\x65\x20\x69\x73\x20\x6C\x6F\x77\x65\x72\x20\x74\x68\x61\x6E\x20\x74\x68\x69\x73\x20\x74\x68\x72\x65\x73\x68\x6F\x6C\x64\x2C\x20\x73\x74\x6F\x70\x20\x72\x65\x73\x69\x7A\x69\x6E\x67\x2E\x20\x54\x68\x69\x73\x20\x68\x65\x6C\x70\x73\x20\x6B\x65\x65\x70\x69\x6E\x67\x20\x70\x65\x72\x66\x6F\x72\x6D\x61\x6E\x63\x65\x20\x68\x69\x67\x68\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FaceUser_t4BEBFC0407D7824C7B033116ABCC900A04E1DCCB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x46\x61\x63\x65\x55\x73\x65\x72"), NULL);
	}
}
static void FaceUser_t4BEBFC0407D7824C7B033116ABCC900A04E1DCCB_CustomAttributesCacheGenerator_Speed(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x6F\x74\x61\x74\x69\x6F\x6E\x20\x73\x70\x65\x65\x64\x20\x66\x61\x63\x74\x6F\x72\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x6D\x75\x6C\x74\x69\x70\x6C\x69\x65\x64\x20\x77\x69\x74\x68\x20\x74\x68\x65\x20\x64\x65\x6C\x74\x61\x20\x74\x69\x6D\x65\x2E\x20\x52\x65\x63\x6F\x6D\x6D\x65\x6E\x64\x65\x64\x20\x76\x61\x6C\x75\x65\x73\x3A\x20\x31\x20\x6F\x72\x20\x32\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FaceUser_t4BEBFC0407D7824C7B033116ABCC900A04E1DCCB_CustomAttributesCacheGenerator_RotationThreshInDegrees(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x68\x65\x20\x61\x6E\x67\x6C\x65\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x27\x47\x61\x7A\x65\x20\x74\x6F\x20\x54\x61\x72\x67\x65\x74\x27\x20\x61\x6E\x64\x20\x27\x43\x61\x6D\x65\x72\x61\x20\x74\x6F\x20\x54\x61\x72\x67\x65\x74\x27\x20\x69\x73\x20\x6C\x65\x73\x73\x20\x74\x68\x61\x6E\x20\x74\x68\x69\x73\x20\x76\x61\x6C\x75\x65\x2C\x20\x64\x6F\x20\x6E\x6F\x74\x68\x69\x6E\x67\x2E\x20\x54\x68\x69\x73\x20\x69\x73\x20\x74\x6F\x20\x70\x72\x65\x76\x65\x6E\x74\x20\x73\x6D\x61\x6C\x6C\x20\x6A\x69\x74\x74\x65\x72\x79\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x73\x2E"), NULL);
	}
}
static void KeepFacingCamera_t0E11683B24D4C5D1DEA905F103AA587F4304DC6E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x69\x73\x20\x6E\x6F\x20\x6C\x6F\x6E\x67\x65\x72\x20\x73\x75\x70\x70\x6F\x72\x74\x65\x64"), true, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x4F\x62\x73\x6F\x6C\x65\x74\x65\x2F\x4B\x65\x65\x70\x46\x61\x63\x69\x6E\x67\x43\x61\x6D\x65\x72\x61"), NULL);
	}
}
static void LoadAdditiveScene_tBAB7527A2C0FC63B97C9B9A863BC813612A4691C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EyeTrackingTarget_tC638599E70EEE5CDC5E0A3EAEE31E158C221C0FA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x4C\x6F\x61\x64\x41\x64\x64\x69\x74\x69\x76\x65\x53\x63\x65\x6E\x65"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(EyeTrackingTarget_tC638599E70EEE5CDC5E0A3EAEE31E158C221C0FA_0_0_0_var), NULL);
	}
}
static void LoadAdditiveScene_tBAB7527A2C0FC63B97C9B9A863BC813612A4691C_CustomAttributesCacheGenerator_SceneToBeLoaded(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x65\x6E\x65\x20\x74\x6F\x20\x62\x65\x20\x6C\x6F\x61\x64\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x62\x75\x74\x74\x6F\x6E\x20\x69\x73\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadAdditiveScene_tBAB7527A2C0FC63B97C9B9A863BC813612A4691C_CustomAttributesCacheGenerator_audio_OnSelect(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x70\x74\x69\x6F\x6E\x61\x6C\x20\x41\x75\x64\x69\x6F\x43\x6C\x69\x70\x20\x77\x68\x69\x63\x68\x20\x69\x73\x20\x70\x6C\x61\x79\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x62\x75\x74\x74\x6F\x6E\x20\x69\x73\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LoadAdditiveScene_tBAB7527A2C0FC63B97C9B9A863BC813612A4691C_CustomAttributesCacheGenerator_waitTimeInSecBeforeLoading(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x69\x6D\x65\x6F\x75\x74\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x62\x65\x66\x6F\x72\x65\x20\x6E\x65\x77\x20\x73\x63\x65\x6E\x65\x20\x69\x73\x20\x6C\x6F\x61\x64\x65\x64\x2E"), NULL);
	}
}
static void LoadAdditiveScene_tBAB7527A2C0FC63B97C9B9A863BC813612A4691C_CustomAttributesCacheGenerator_LoadAdditiveScene_LoadNewScene_m8369E583996CD92D5D211F38E219CDAA014E43FE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadNewSceneU3Ed__6_t0E26EB5F61490AF9926F108D5E2AF8BFD3A41D9B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadNewSceneU3Ed__6_t0E26EB5F61490AF9926F108D5E2AF8BFD3A41D9B_0_0_0_var), NULL);
	}
}
static void U3CLoadNewSceneU3Ed__6_t0E26EB5F61490AF9926F108D5E2AF8BFD3A41D9B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadNewSceneU3Ed__6_t0E26EB5F61490AF9926F108D5E2AF8BFD3A41D9B_CustomAttributesCacheGenerator_U3CLoadNewSceneU3Ed__6__ctor_mAAA7D7B2D2A941AB339DDE43B676DA5EB7C3F25A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadNewSceneU3Ed__6_t0E26EB5F61490AF9926F108D5E2AF8BFD3A41D9B_CustomAttributesCacheGenerator_U3CLoadNewSceneU3Ed__6_System_IDisposable_Dispose_mAC49FC02DB74158443E728F2B937460842721A3B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadNewSceneU3Ed__6_t0E26EB5F61490AF9926F108D5E2AF8BFD3A41D9B_CustomAttributesCacheGenerator_U3CLoadNewSceneU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9CF41C95C56DA29A06DC641A6B10B958B14D43D3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadNewSceneU3Ed__6_t0E26EB5F61490AF9926F108D5E2AF8BFD3A41D9B_CustomAttributesCacheGenerator_U3CLoadNewSceneU3Ed__6_System_Collections_IEnumerator_Reset_m1F4F5F43E8C334FD2178951548219825F2978BA6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadNewSceneU3Ed__6_t0E26EB5F61490AF9926F108D5E2AF8BFD3A41D9B_CustomAttributesCacheGenerator_U3CLoadNewSceneU3Ed__6_System_Collections_IEnumerator_get_Current_m7D090F16DFB2D3B4CFB78A558428242744FEF519(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MoveWithCamera_t713CAB32E98200C6F64B9013A82132212BE4CE70_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x4D\x6F\x76\x65\x57\x69\x74\x68\x43\x61\x6D\x65\x72\x61"), NULL);
	}
}
static void MoveWithCamera_t713CAB32E98200C6F64B9013A82132212BE4CE70_CustomAttributesCacheGenerator_offsetToCamera(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OnLoadStartScene_tB348169740631E1E1F7DF598FA0DA6A2FD91DB87_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x4F\x6E\x4C\x6F\x61\x64\x53\x74\x61\x72\x74\x53\x63\x65\x6E\x65"), NULL);
	}
}
static void OnLoadStartScene_tB348169740631E1E1F7DF598FA0DA6A2FD91DB87_CustomAttributesCacheGenerator_SceneToBeLoaded(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x61\x6D\x65\x20\x6F\x66\x20\x74\x68\x65\x20\x73\x63\x65\x6E\x65\x20\x74\x6F\x20\x62\x65\x20\x6C\x6F\x61\x64\x65\x64\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x62\x75\x74\x74\x6F\x6E\x20\x69\x73\x20\x73\x65\x6C\x65\x63\x74\x65\x64\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OnLoadStartScene_tB348169740631E1E1F7DF598FA0DA6A2FD91DB87_CustomAttributesCacheGenerator_LoadOption(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4F\x70\x74\x69\x6F\x6E\x20\x74\x6F\x20\x6F\x6E\x6C\x79\x20\x6C\x6F\x61\x64\x20\x74\x68\x65\x20\x73\x63\x65\x6E\x65\x20\x69\x66\x20\x72\x75\x6E\x6E\x69\x6E\x67\x20\x6F\x6E\x20\x74\x68\x65\x20\x48\x6F\x6C\x6F\x4C\x65\x6E\x73\x20\x64\x65\x76\x69\x63\x65\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OnLookAtRotateByEyeGaze_tC6DB484A3A2B0C7792840A6E70B82CE54B54A3A2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EyeTrackingTarget_tC638599E70EEE5CDC5E0A3EAEE31E158C221C0FA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x4F\x6E\x4C\x6F\x6F\x6B\x41\x74\x52\x6F\x74\x61\x74\x65\x42\x79\x45\x79\x65\x47\x61\x7A\x65"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(EyeTrackingTarget_tC638599E70EEE5CDC5E0A3EAEE31E158C221C0FA_0_0_0_var), NULL);
	}
}
static void OnLookAtRotateByEyeGaze_tC6DB484A3A2B0C7792840A6E70B82CE54B54A3A2_CustomAttributesCacheGenerator_speedX(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x72\x69\x7A\x6F\x6E\x74\x61\x6C\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x73\x70\x65\x65\x64\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OnLookAtRotateByEyeGaze_tC6DB484A3A2B0C7792840A6E70B82CE54B54A3A2_CustomAttributesCacheGenerator_speedY(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x56\x65\x72\x74\x69\x63\x61\x6C\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x73\x70\x65\x65\x64\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OnLookAtRotateByEyeGaze_tC6DB484A3A2B0C7792840A6E70B82CE54B54A3A2_CustomAttributesCacheGenerator_inverseX(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x6F\x20\x69\x6E\x76\x65\x72\x73\x65\x20\x74\x68\x65\x20\x68\x6F\x72\x69\x7A\x6F\x6E\x74\x61\x6C\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x64\x69\x72\x65\x63\x74\x69\x6F\x6E\x2E"), NULL);
	}
}
static void OnLookAtRotateByEyeGaze_tC6DB484A3A2B0C7792840A6E70B82CE54B54A3A2_CustomAttributesCacheGenerator_inverseY(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x6F\x20\x69\x6E\x76\x65\x72\x73\x65\x20\x74\x68\x65\x20\x76\x65\x72\x74\x69\x63\x61\x6C\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x64\x69\x72\x65\x63\x74\x69\x6F\x6E\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OnLookAtRotateByEyeGaze_tC6DB484A3A2B0C7792840A6E70B82CE54B54A3A2_CustomAttributesCacheGenerator_rotationThreshInDegrees(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x68\x65\x20\x61\x6E\x67\x6C\x65\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x27\x47\x61\x7A\x65\x20\x74\x6F\x20\x54\x61\x72\x67\x65\x74\x27\x20\x61\x6E\x64\x20\x27\x43\x61\x6D\x65\x72\x61\x20\x74\x6F\x20\x54\x61\x72\x67\x65\x74\x27\x20\x69\x73\x20\x6C\x65\x73\x73\x20\x74\x68\x61\x6E\x20\x74\x68\x69\x73\x20\x76\x61\x6C\x75\x65\x2C\x20\x64\x6F\x20\x6E\x6F\x74\x68\x69\x6E\x67\x2E\x20\x54\x68\x69\x73\x20\x69\x73\x20\x74\x6F\x20\x70\x72\x65\x76\x65\x6E\x74\x20\x73\x6D\x61\x6C\x6C\x20\x6A\x69\x74\x74\x65\x72\x79\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x73\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OnLookAtRotateByEyeGaze_tC6DB484A3A2B0C7792840A6E70B82CE54B54A3A2_CustomAttributesCacheGenerator_minRotX(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x75\x6D\x20\x68\x6F\x72\x69\x7A\x6F\x6E\x74\x61\x6C\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x61\x6E\x67\x6C\x65\x2E\x20\x54\x68\x69\x73\x20\x69\x73\x20\x74\x6F\x20\x6C\x69\x6D\x69\x74\x20\x74\x68\x65\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x69\x6E\x20\x64\x69\x66\x66\x65\x72\x65\x6E\x74\x20\x64\x69\x72\x65\x63\x74\x69\x6F\x6E\x73\x2E"), NULL);
	}
}
static void OnLookAtRotateByEyeGaze_tC6DB484A3A2B0C7792840A6E70B82CE54B54A3A2_CustomAttributesCacheGenerator_maxRotX(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x20\x68\x6F\x72\x69\x7A\x6F\x6E\x74\x61\x6C\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x61\x6E\x67\x6C\x65\x2E\x20\x54\x68\x69\x73\x20\x69\x73\x20\x74\x6F\x20\x6C\x69\x6D\x69\x74\x20\x74\x68\x65\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x69\x6E\x20\x64\x69\x66\x66\x65\x72\x65\x6E\x74\x20\x64\x69\x72\x65\x63\x74\x69\x6F\x6E\x73\x2E"), NULL);
	}
}
static void OnLookAtRotateByEyeGaze_tC6DB484A3A2B0C7792840A6E70B82CE54B54A3A2_CustomAttributesCacheGenerator_minRotY(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x69\x6E\x69\x6D\x61\x6C\x20\x76\x65\x72\x74\x69\x63\x61\x6C\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x61\x6E\x67\x6C\x65\x2E\x20\x54\x68\x69\x73\x20\x69\x73\x20\x74\x6F\x20\x6C\x69\x6D\x69\x74\x20\x74\x68\x65\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x69\x6E\x20\x64\x69\x66\x66\x65\x72\x65\x6E\x74\x20\x64\x69\x72\x65\x63\x74\x69\x6F\x6E\x73\x2E"), NULL);
	}
}
static void OnLookAtRotateByEyeGaze_tC6DB484A3A2B0C7792840A6E70B82CE54B54A3A2_CustomAttributesCacheGenerator_maxRotY(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x78\x69\x6D\x75\x6D\x20\x76\x65\x72\x74\x69\x63\x61\x6C\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x61\x6E\x67\x6C\x65\x2E\x20\x54\x68\x69\x73\x20\x69\x73\x20\x74\x6F\x20\x6C\x69\x6D\x69\x74\x20\x74\x68\x65\x20\x72\x6F\x74\x61\x74\x69\x6F\x6E\x20\x69\x6E\x20\x64\x69\x66\x66\x65\x72\x65\x6E\x74\x20\x64\x69\x72\x65\x63\x74\x69\x6F\x6E\x73\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Interactable_tA1E638AA4938DBD6D4E2CCC082762389FC37F6E2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x44\x77\x65\x6C\x6C\x53\x65\x6C\x65\x63\x74\x69\x6F\x6E"), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(Interactable_tA1E638AA4938DBD6D4E2CCC082762389FC37F6E2_0_0_0_var), NULL);
	}
}
static void DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator_startEnabledOnStartup(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x74\x72\x75\x65\x2C\x20\x74\x68\x65\x20\x61\x74\x74\x61\x63\x68\x65\x64\x20\x74\x61\x72\x67\x65\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x73\x65\x6C\x65\x63\x74\x61\x62\x6C\x65\x20\x76\x69\x61\x20\x64\x77\x65\x6C\x6C\x20\x6F\x6E\x20\x73\x74\x61\x72\x74\x75\x70\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator_rootFeedbackToEnable(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x52\x6F\x6F\x74\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x67\x61\x6D\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x74\x68\x61\x74\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x65\x6E\x61\x62\x6C\x65\x64\x20\x6F\x6E\x63\x65\x20\x64\x77\x65\x6C\x6C\x20\x69\x73\x20\x69\x6E\x69\x74\x69\x61\x74\x65\x64\x2E"), NULL);
	}
}
static void DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator_feedbackToChangeInSize(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x74\x6F\x20\x6D\x61\x6E\x69\x70\x75\x6C\x61\x74\x65\x20\x66\x6F\x72\x20\x70\x72\x6F\x76\x69\x64\x69\x6E\x67\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x61\x62\x6F\x75\x74\x20\x74\x68\x65\x20\x64\x77\x65\x6C\x6C\x20\x73\x74\x61\x74\x65\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator_feedbackStartSize(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x74\x6F\x20\x72\x65\x70\x72\x65\x73\x65\x6E\x74\x20\x74\x68\x65\x20\x6D\x61\x78\x20\x28\x69\x6E\x69\x74\x69\x61\x6C\x29\x20\x65\x78\x74\x65\x6E\x73\x69\x6F\x6E\x2E"), NULL);
	}
}
static void DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator_feedbackEndSize(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x20\x6F\x62\x6A\x65\x63\x74\x20\x74\x6F\x20\x72\x65\x70\x72\x65\x73\x65\x6E\x74\x20\x74\x68\x65\x20\x6D\x69\x6E\x20\x28\x69\x6E\x69\x74\x69\x61\x6C\x29\x20\x65\x78\x74\x65\x6E\x73\x69\x6F\x6E\x2E"), NULL);
	}
}
static void DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator_feedbackDelayInSeconds(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x65\x6C\x61\x79\x20\x69\x6E\x20\x73\x65\x63\x6F\x6E\x64\x73\x20\x75\x6E\x74\x69\x6C\x20\x64\x77\x65\x6C\x6C\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x69\x73\x20\x73\x74\x61\x72\x74\x65\x64\x20\x74\x6F\x20\x62\x65\x20\x73\x68\x6F\x77\x6E\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 5.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[2];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator_dwellTimeInSecondsToSelect(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x41\x64\x64\x69\x74\x69\x6F\x6E\x61\x6C\x20\x74\x69\x6D\x65\x20\x28\x6E\x6F\x74\x20\x69\x6E\x63\x6C\x75\x64\x69\x6E\x67\x20\x74\x68\x65\x20\x69\x6E\x69\x74\x69\x61\x6C\x20\x64\x65\x6C\x61\x79\x20\x74\x69\x6D\x65\x29\x20\x74\x68\x65\x20\x75\x73\x65\x72\x20\x6E\x65\x65\x64\x73\x20\x74\x6F\x20\x6B\x65\x65\x70\x20\x6C\x6F\x6F\x6B\x69\x6E\x67\x20\x61\x74\x20\x74\x68\x65\x20\x55\x49\x20\x74\x6F\x20\x61\x63\x74\x69\x76\x61\x74\x65\x20\x69\x74\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[2];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 5.0f, NULL);
	}
}
static void DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator_startScale(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x72\x74\x20\x74\x61\x72\x67\x65\x74\x20\x73\x69\x7A\x65"), NULL);
	}
}
static void DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator_endScale(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x64\x20\x74\x61\x72\x67\x65\x74\x20\x73\x69\x7A\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator_startTransp(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x69\x74\x69\x61\x6C\x20\x74\x72\x61\x6E\x73\x70\x61\x72\x65\x6E\x63\x79\x20\x74\x6F\x20\x73\x74\x61\x72\x74\x20\x77\x69\x74\x68\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[2];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator_endTransp(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x69\x6E\x61\x6C\x20\x74\x72\x61\x6E\x73\x70\x61\x72\x65\x6E\x63\x79\x20\x74\x6F\x20\x65\x6E\x64\x20\x77\x69\x74\x68\x2E"), NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[2];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void TargetEventArgs_t7713E8CD48F3A9DC5C9036F0CDD4C44DDA493350_CustomAttributesCacheGenerator_U3CHitTargetU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void TargetEventArgs_t7713E8CD48F3A9DC5C9036F0CDD4C44DDA493350_CustomAttributesCacheGenerator_TargetEventArgs_get_HitTarget_m8388656A38F227CCD16F8BCF83A22047ECFDA41B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TargetEventArgs_t7713E8CD48F3A9DC5C9036F0CDD4C44DDA493350_CustomAttributesCacheGenerator_TargetEventArgs_set_HitTarget_mC1AE547E25147BE85A735C663443ABEFDE11B4F0(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DoNotRender_t5C652699B6FF291B6AF41FA936CEF2F9243315CB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x44\x6F\x4E\x6F\x74\x52\x65\x6E\x64\x65\x72"), NULL);
	}
}
static void EyeCalibrationChecker_tC19835DAC8153DBEE0A3E08F367C3578A0E3B214_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x45\x79\x65\x43\x61\x6C\x69\x62\x72\x61\x74\x69\x6F\x6E\x43\x68\x65\x63\x6B\x65\x72"), NULL);
	}
}
static void EyeCalibrationChecker_tC19835DAC8153DBEE0A3E08F367C3578A0E3B214_CustomAttributesCacheGenerator_editorTestUserIsCalibrated(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x6F\x72\x20\x74\x65\x73\x74\x69\x6E\x67\x20\x70\x75\x72\x70\x6F\x73\x65\x73\x2C\x20\x79\x6F\x75\x20\x63\x61\x6E\x20\x6D\x61\x6E\x75\x61\x6C\x6C\x79\x20\x61\x73\x73\x69\x67\x6E\x20\x77\x68\x65\x74\x68\x65\x72\x20\x74\x68\x65\x20\x75\x73\x65\x72\x20\x69\x73\x20\x65\x79\x65\x20\x63\x61\x6C\x69\x62\x72\x61\x74\x65\x64\x20\x6F\x72\x20\x6E\x6F\x74\x2E"), NULL);
	}
}
static void KeepThisAlive_t6373FC2BC670FA2D10DF2EA75A5D40325044C99B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[0];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x69\x73\x20\x6E\x6F\x20\x6C\x6F\x6E\x67\x65\x72\x20\x73\x75\x70\x70\x6F\x72\x74\x65\x64"), true, NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x4F\x62\x73\x6F\x6C\x65\x74\x65\x2F\x4B\x65\x65\x70\x54\x68\x69\x73\x41\x6C\x69\x76\x65"), NULL);
	}
}
static void KeepThisAlive_t6373FC2BC670FA2D10DF2EA75A5D40325044C99B_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void KeepThisAlive_t6373FC2BC670FA2D10DF2EA75A5D40325044C99B_CustomAttributesCacheGenerator_KeepThisAlive_get_Instance_m6E3E6968684933E52E1A47F41083AC48FF464465(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void KeepThisAlive_t6373FC2BC670FA2D10DF2EA75A5D40325044C99B_CustomAttributesCacheGenerator_KeepThisAlive_set_Instance_mA2A10EE2DD42672547E38F84945744FB35AF8640(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void StatusText_t6175A4B456E90A4490D283864EFEA6CF6DD274EB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x4F\x62\x73\x6F\x6C\x65\x74\x65\x2F\x53\x74\x61\x74\x75\x73\x54\x65\x78\x74"), NULL);
	}
	{
		ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 * tmp = (ObsoleteAttribute_t14BAC1669C0409EB9F28D72D664FFA6764ACD671 *)cache->attributes[1];
		ObsoleteAttribute__ctor_m058D2B798E9AB7E7778A6DB04C3FE7011C660F4C(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x69\x73\x20\x63\x6F\x6D\x70\x6F\x6E\x65\x6E\x74\x20\x69\x73\x20\x6E\x6F\x20\x6C\x6F\x6E\x67\x65\x72\x20\x73\x75\x70\x70\x6F\x72\x74\x65\x64"), true, NULL);
	}
}
static void StatusText_t6175A4B456E90A4490D283864EFEA6CF6DD274EB_CustomAttributesCacheGenerator_status(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EyeTrackingDemoUtils_tEADE14372E8745738081ECE2AF930C7AB369754B_CustomAttributesCacheGenerator_EyeTrackingDemoUtils_LoadNewScene_mDDC18CE3E1DB98A62D89FB6C89E485D1B68BF4F2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadNewSceneU3Ed__8_t6CC37138968FD381723B0793506AC695ED553EF9_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadNewSceneU3Ed__8_t6CC37138968FD381723B0793506AC695ED553EF9_0_0_0_var), NULL);
	}
}
static void U3CLoadNewSceneU3Ed__8_t6CC37138968FD381723B0793506AC695ED553EF9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadNewSceneU3Ed__8_t6CC37138968FD381723B0793506AC695ED553EF9_CustomAttributesCacheGenerator_U3CLoadNewSceneU3Ed__8__ctor_m401003DDD285C5623BEB280831A31A466D16A964(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadNewSceneU3Ed__8_t6CC37138968FD381723B0793506AC695ED553EF9_CustomAttributesCacheGenerator_U3CLoadNewSceneU3Ed__8_System_IDisposable_Dispose_mD06F424BF364A4726F11674C65C0CA9EEEAFE2CC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadNewSceneU3Ed__8_t6CC37138968FD381723B0793506AC695ED553EF9_CustomAttributesCacheGenerator_U3CLoadNewSceneU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6D49FF64BD8C8DE4A6EA2EC600BD12FF3390B71E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadNewSceneU3Ed__8_t6CC37138968FD381723B0793506AC695ED553EF9_CustomAttributesCacheGenerator_U3CLoadNewSceneU3Ed__8_System_Collections_IEnumerator_Reset_mC163ABF19EDAEDE1B729059AFF32EC00D9760CED(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadNewSceneU3Ed__8_t6CC37138968FD381723B0793506AC695ED553EF9_CustomAttributesCacheGenerator_U3CLoadNewSceneU3Ed__8_System_Collections_IEnumerator_get_Current_m4E0C826DFFCFD5E5854D74C1DDB3B2F680731F1C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&EyeTrackingTarget_tC638599E70EEE5CDC5E0A3EAEE31E158C221C0FA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(EyeTrackingTarget_tC638599E70EEE5CDC5E0A3EAEE31E158C221C0FA_0_0_0_var), NULL);
	}
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[1];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x4F\x6E\x4C\x6F\x6F\x6B\x41\x74\x53\x68\x6F\x77\x48\x6F\x76\x65\x72\x46\x65\x65\x64\x62\x61\x63\x6B"), NULL);
	}
}
static void OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator_Overlay_UseIt(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x54\x52\x55\x45\x3A\x20\x53\x68\x6F\x77\x20\x61\x20\x76\x69\x73\x75\x61\x6C\x20\x69\x6E\x64\x69\x63\x61\x74\x6F\x72\x20\x61\x74\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x20\x63\x65\x6E\x74\x65\x72\x20\x77\x68\x65\x6E\x20\x68\x6F\x76\x65\x72\x65\x64\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator_Overlay_GameObj(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x48\x6F\x76\x65\x72\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x3A\x20\x41\x6E\x20\x69\x6E\x73\x74\x61\x6E\x63\x65\x20\x6F\x66\x20\x74\x68\x69\x73\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x64\x69\x73\x70\x6C\x61\x79\x65\x64\x20\x61\x74\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x20\x63\x65\x6E\x74\x65\x72\x20\x61\x73\x20\x76\x69\x73\x75\x61\x6C\x20\x69\x6E\x64\x69\x63\x61\x74\x6F\x72\x20\x74\x68\x61\x74\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x20\x69\x73\x20\x68\x6F\x76\x65\x72\x65\x64\x2E"), NULL);
	}
}
static void OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator_startTransparency(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x53\x6C\x6F\x77\x6C\x79\x20\x66\x61\x64\x65\x20\x6F\x76\x65\x72\x6C\x61\x79\x20\x69\x6E\x20\x73\x74\x61\x72\x74\x69\x6E\x67\x20\x66\x72\x6F\x6D\x20\x74\x68\x69\x73\x20\x69\x6E\x69\x74\x69\x61\x6C\x20\x74\x72\x61\x6E\x73\x70\x61\x72\x65\x6E\x63\x79\x2E"), NULL);
	}
}
static void OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator_endTransparency(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[2];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x69\x6E\x69\x73\x68\x20\x66\x61\x64\x65\x20\x69\x6E\x20\x6F\x66\x20\x74\x68\x65\x20\x6F\x76\x65\x72\x6C\x61\x79\x20\x61\x74\x20\x74\x68\x69\x73\x20\x67\x69\x76\x65\x6E\x20\x74\x72\x61\x6E\x73\x70\x61\x72\x65\x6E\x63\x79\x2E"), NULL);
	}
}
static void OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator_Highlight_UseIt(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x49\x66\x20\x54\x52\x55\x45\x3A\x20\x48\x69\x67\x68\x6C\x69\x67\x68\x74\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x20\x77\x68\x65\x6E\x20\x68\x6F\x76\x65\x72\x65\x64\x2E"), NULL);
	}
}
static void OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator_Highlight_Color(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x6F\x72\x20\x74\x6F\x20\x75\x73\x65\x20\x66\x6F\x72\x20\x74\x69\x6E\x74\x69\x6E\x67\x20\x74\x68\x65\x20\x74\x61\x72\x67\x65\x74\x20\x77\x68\x65\x6E\x20\x68\x6F\x76\x65\x72\x65\x64\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator_MinDwellTimeInMs(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x61\x64\x65\x20\x69\x6E\x20\x73\x74\x61\x72\x74\x3A\x20\x4D\x69\x6E\x69\x6D\x75\x6D\x20\x68\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x20\x62\x65\x66\x6F\x72\x65\x20\x74\x68\x65\x20\x76\x69\x73\x75\x61\x6C\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x20\x73\x74\x61\x72\x74\x73\x20\x66\x61\x64\x69\x6E\x67\x20\x69\x6E\x2E"), NULL);
	}
}
static void OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator_MaxDwellTimeInMs(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x61\x64\x65\x20\x69\x6E\x20\x65\x6E\x64\x3A\x20\x4D\x61\x78\x20\x68\x6F\x76\x65\x72\x20\x74\x69\x6D\x65\x20\x69\x6E\x64\x69\x63\x61\x74\x69\x6E\x67\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x66\x61\x64\x65\x2D\x69\x6E\x20\x69\x73\x20\x66\x69\x6E\x69\x73\x68\x65\x64\x2E\x20\x54\x68\x69\x73\x20\x63\x61\x6E\x20\x62\x65\x20\x75\x73\x65\x64\x20\x66\x6F\x72\x20\x73\x6C\x6F\x77\x20\x66\x61\x64\x65\x20\x69\x6E\x73\x2E"), NULL);
	}
}
static void OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator_MinLookAwayTimeInMs(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x61\x64\x65\x20\x6F\x75\x74\x20\x73\x74\x61\x72\x74\x3A\x20\x4D\x69\x6E\x20\x6C\x6F\x6F\x6B\x20\x61\x77\x61\x79\x20\x74\x69\x6D\x65\x20\x69\x6E\x64\x69\x63\x61\x74\x69\x6E\x67\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x66\x61\x64\x65\x2D\x6F\x75\x74\x20\x73\x74\x61\x72\x74\x73\x2E\x20\x54\x68\x69\x73\x20\x69\x73\x20\x75\x73\x65\x66\x75\x6C\x20\x74\x6F\x20\x70\x72\x65\x76\x65\x6E\x74\x20\x66\x6C\x69\x63\x6B\x65\x72\x69\x6E\x67\x20\x66\x65\x65\x64\x62\x61\x63\x6B\x2E"), NULL);
	}
}
static void OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator_MaxLookAwayTimeInMs(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x46\x61\x64\x65\x20\x6F\x75\x74\x20\x65\x6E\x64\x3A\x20\x4D\x61\x78\x20\x6C\x6F\x6F\x6B\x20\x61\x77\x61\x79\x20\x74\x69\x6D\x65\x20\x69\x6E\x64\x69\x63\x61\x74\x69\x6E\x67\x20\x77\x68\x65\x6E\x20\x74\x68\x65\x20\x66\x61\x64\x65\x2D\x6F\x75\x74\x20\x69\x73\x20\x66\x69\x6E\x69\x73\x68\x65\x64\x2E"), NULL);
	}
}
static void OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator_BlendType(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x64\x65\x73\x20\x66\x6F\x72\x20\x74\x68\x65\x20\x74\x79\x70\x65\x20\x6F\x66\x20\x66\x61\x64\x65\x20\x69\x6E\x2F\x6F\x75\x74\x3A\x20\x4F\x6E\x2F\x6F\x66\x66\x20\x28\x62\x6F\x6F\x6C\x65\x61\x6E\x29\x2C\x20\x6C\x69\x6E\x65\x61\x72\x20\x62\x6C\x65\x6E\x64\x2C\x20\x65\x74\x63\x2E"), NULL);
	}
}
static void ExclusiveSynchronizationContext_tF7C7F5ABCA05A45DB107864767F66F827CC903B8_CustomAttributesCacheGenerator_U3CInnerExceptionU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void ExclusiveSynchronizationContext_tF7C7F5ABCA05A45DB107864767F66F827CC903B8_CustomAttributesCacheGenerator_ExclusiveSynchronizationContext_get_InnerException_m4409BF574355A708E7AD7B501A23394DC8194BDB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExclusiveSynchronizationContext_tF7C7F5ABCA05A45DB107864767F66F827CC903B8_CustomAttributesCacheGenerator_ExclusiveSynchronizationContext_set_InnerException_m8E8500331FAEEC67FA3687E1AB92B8B6AFCCE153(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ExclusiveSynchronizationContext_tF7C7F5ABCA05A45DB107864767F66F827CC903B8_CustomAttributesCacheGenerator_ExclusiveSynchronizationContext_U3CEndMessageLoopU3Eb__9_0_m2269F5C62E97D5684D03C180C278A730562819C4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_t2B7F40848D5A9FFE0BF064D1B1E9AA3221E5971C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass0_0_t2B7F40848D5A9FFE0BF064D1B1E9AA3221E5971C_CustomAttributesCacheGenerator_U3CU3Ec__DisplayClass0_0_U3CRunSyncU3Eb__0_mE0B82500FB231395120B4EA8482680957CFEE75A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3CRunSyncU3Eb__0U3Ed_tA0DB1D14F5469B811AB9737E394CE355699216C8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CU3CRunSyncU3Eb__0U3Ed_tA0DB1D14F5469B811AB9737E394CE355699216C8_0_0_0_var), NULL);
	}
	{
		DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F * tmp = (DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F *)cache->attributes[1];
		DebuggerStepThroughAttribute__ctor_m2B40F019B0DF22CF7A815AAB3D2D027225D59D85(tmp, NULL);
	}
}
static void U3CU3CRunSyncU3Eb__0U3Ed_tA0DB1D14F5469B811AB9737E394CE355699216C8_CustomAttributesCacheGenerator_U3CU3CRunSyncU3Eb__0U3Ed_SetStateMachine_m9CD2295E60C2CDDFF44C6A2CA4ACF079D62B4BE0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_1_t25BE1789EC1C3AC5447D39D361E55C23E82224EE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass1_0_1_t25BE1789EC1C3AC5447D39D361E55C23E82224EE_CustomAttributesCacheGenerator_U3CU3Ec__DisplayClass1_0_1_U3CRunSyncU3Eb__0_mBC5BADE1F00FF150D1011C6AB8CFD0C8B7701110(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3CRunSyncU3Eb__0U3Ed_t274F9BB76D6EF41A733C34C50B5394D62F080C76_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CU3CRunSyncU3Eb__0U3Ed_t274F9BB76D6EF41A733C34C50B5394D62F080C76_0_0_0_var), NULL);
	}
	{
		DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F * tmp = (DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F *)cache->attributes[1];
		DebuggerStepThroughAttribute__ctor_m2B40F019B0DF22CF7A815AAB3D2D027225D59D85(tmp, NULL);
	}
}
static void U3CU3CRunSyncU3Eb__0U3Ed_t274F9BB76D6EF41A733C34C50B5394D62F080C76_CustomAttributesCacheGenerator_U3CU3CRunSyncU3Eb__0U3Ed_SetStateMachine_m7AF5C7352D1CB51F349862D24C352799EC366C81(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BasicInputLogger_tF39D6623A3849E400F84E3E8ADB2CBF4FC2F9D4F_CustomAttributesCacheGenerator_sessionDescr(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BasicInputLogger_tF39D6623A3849E400F84E3E8ADB2CBF4FC2F9D4F_CustomAttributesCacheGenerator_BasicInputLogger_CreateNewLogFile_mA8AB2F74DDF248A632C2C07FED6A9F121C5F24AC(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCreateNewLogFileU3Ed__14_tE9FFFDE66A83EFB7925BBCF9E4D461E949829D7A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CCreateNewLogFileU3Ed__14_tE9FFFDE66A83EFB7925BBCF9E4D461E949829D7A_0_0_0_var), NULL);
	}
	{
		DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F * tmp = (DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F *)cache->attributes[1];
		DebuggerStepThroughAttribute__ctor_m2B40F019B0DF22CF7A815AAB3D2D027225D59D85(tmp, NULL);
	}
}
static void BasicInputLogger_tF39D6623A3849E400F84E3E8ADB2CBF4FC2F9D4F_CustomAttributesCacheGenerator_BasicInputLogger_LoadLogs_m7F0D6AA20D8D240CD2A26D43CB0604DD4A59B853(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadLogsU3Ed__21_t71C6912789A78D48E47827228CE2FB8377D5655F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F * tmp = (DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F *)cache->attributes[0];
		DebuggerStepThroughAttribute__ctor_m2B40F019B0DF22CF7A815AAB3D2D027225D59D85(tmp, NULL);
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[1];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CLoadLogsU3Ed__21_t71C6912789A78D48E47827228CE2FB8377D5655F_0_0_0_var), NULL);
	}
}
static void BasicInputLogger_tF39D6623A3849E400F84E3E8ADB2CBF4FC2F9D4F_CustomAttributesCacheGenerator_BasicInputLogger_SaveLogs_m8E427165D7149A23C324257EC8E8F0C2637FF3B1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CSaveLogsU3Ed__22_tA5C05FED470C577465EB69A3BAA3AE8EA4ADF0EE_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CSaveLogsU3Ed__22_tA5C05FED470C577465EB69A3BAA3AE8EA4ADF0EE_0_0_0_var), NULL);
	}
	{
		DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F * tmp = (DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F *)cache->attributes[1];
		DebuggerStepThroughAttribute__ctor_m2B40F019B0DF22CF7A815AAB3D2D027225D59D85(tmp, NULL);
	}
}
static void U3CCreateNewLogFileU3Ed__14_tE9FFFDE66A83EFB7925BBCF9E4D461E949829D7A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCreateNewLogFileU3Ed__14_tE9FFFDE66A83EFB7925BBCF9E4D461E949829D7A_CustomAttributesCacheGenerator_U3CCreateNewLogFileU3Ed__14_SetStateMachine_m6F238B4288F208E800147D5C1CEABA0527113273(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadLogsU3Ed__21_t71C6912789A78D48E47827228CE2FB8377D5655F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadLogsU3Ed__21_t71C6912789A78D48E47827228CE2FB8377D5655F_CustomAttributesCacheGenerator_U3CLoadLogsU3Ed__21_SetStateMachine_m2E9C43D81C1E46818BC390C7402EB5F366A44F4D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CSaveLogsU3Ed__22_tA5C05FED470C577465EB69A3BAA3AE8EA4ADF0EE_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CSaveLogsU3Ed__22_tA5C05FED470C577465EB69A3BAA3AE8EA4ADF0EE_CustomAttributesCacheGenerator_U3CSaveLogsU3Ed__22_SetStateMachine_m61A95B0BF84898DFF17E30CC4BDF6756503A7F54(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void CustomInputLogger_tCD760BEF5586405526D75D8209961F56AB53A484_CustomAttributesCacheGenerator_filename(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x68\x65\x20\x64\x61\x74\x61\x20\x77\x69\x6C\x6C\x20\x62\x65\x20\x73\x61\x76\x65\x64\x20\x61\x73\x20\x43\x53\x56\x20\x66\x69\x6C\x65\x73\x2E"), NULL);
	}
}
static void InputPointerVisualizer_tD1C9426AA2AD4546530800E8206B5B09C7A804AD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x49\x6E\x70\x75\x74\x50\x6F\x69\x6E\x74\x65\x72\x56\x69\x73\x75\x61\x6C\x69\x7A\x65\x72"), NULL);
	}
}
static void InputPointerVisualizer_tD1C9426AA2AD4546530800E8206B5B09C7A804AD_CustomAttributesCacheGenerator_useLiveInputStream(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InputPointerVisualizer_tD1C9426AA2AD4546530800E8206B5B09C7A804AD_CustomAttributesCacheGenerator_onlyShowForHitTargets(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InputPointerVisualizer_tD1C9426AA2AD4546530800E8206B5B09C7A804AD_CustomAttributesCacheGenerator_tmplt_Origins(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x6D\x70\x6C\x61\x74\x65\x20\x66\x6F\x72\x20\x76\x69\x73\x75\x61\x6C\x69\x7A\x69\x6E\x67\x20\x76\x65\x63\x74\x6F\x72\x20\x6F\x72\x69\x67\x69\x6E\x2C\x20\x65\x2E\x67\x2E\x2C\x20\x61\x20\x63\x6F\x6C\x6F\x72\x65\x64\x20\x73\x70\x68\x65\x72\x65\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InputPointerVisualizer_tD1C9426AA2AD4546530800E8206B5B09C7A804AD_CustomAttributesCacheGenerator_tmplt_Destinations(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x6D\x70\x6C\x61\x74\x65\x20\x66\x6F\x72\x20\x76\x69\x73\x75\x61\x6C\x69\x7A\x69\x6E\x67\x20\x68\x69\x74\x20\x70\x6F\x73\x2C\x20\x65\x2E\x67\x2E\x2C\x20\x61\x20\x63\x6F\x6C\x6F\x72\x65\x64\x20\x73\x70\x68\x65\x72\x65\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InputPointerVisualizer_tD1C9426AA2AD4546530800E8206B5B09C7A804AD_CustomAttributesCacheGenerator_tmplt_LinkOrigToOrig(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x6D\x70\x6C\x61\x74\x65\x20\x66\x6F\x72\x20\x76\x69\x73\x75\x61\x6C\x69\x7A\x69\x6E\x67\x20\x63\x6F\x6E\x6E\x65\x63\x74\x69\x6E\x67\x20\x6C\x69\x6E\x65\x73\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x76\x65\x63\x74\x6F\x72\x20\x6F\x72\x69\x67\x69\x6E\x73\x20\x2D\x20\x53\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x61\x20\x6C\x69\x6E\x65\x20\x72\x65\x6E\x64\x65\x72\x65\x72\x2E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InputPointerVisualizer_tD1C9426AA2AD4546530800E8206B5B09C7A804AD_CustomAttributesCacheGenerator_tmplt_LinkDestToDest(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x6D\x70\x6C\x61\x74\x65\x20\x66\x6F\x72\x20\x76\x69\x73\x75\x61\x6C\x69\x7A\x69\x6E\x67\x20\x63\x6F\x6E\x6E\x65\x63\x74\x69\x6E\x67\x20\x6C\x69\x6E\x65\x73\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x76\x65\x63\x74\x6F\x72\x20\x64\x65\x73\x74\x69\x6E\x61\x74\x69\x6F\x6E\x73\x20\x2D\x20\x53\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x61\x20\x6C\x69\x6E\x65\x20\x72\x65\x6E\x64\x65\x72\x65\x72\x2E"), NULL);
	}
}
static void InputPointerVisualizer_tD1C9426AA2AD4546530800E8206B5B09C7A804AD_CustomAttributesCacheGenerator_tmplt_LinkOrigToDest(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[1];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x6D\x70\x6C\x61\x74\x65\x20\x66\x6F\x72\x20\x76\x69\x73\x75\x61\x6C\x69\x7A\x69\x6E\x67\x20\x74\x68\x65\x20\x76\x65\x63\x74\x6F\x72\x20\x62\x65\x74\x77\x65\x65\x6E\x20\x76\x65\x63\x74\x6F\x72\x20\x6F\x72\x69\x67\x69\x6E\x20\x61\x6E\x64\x20\x64\x65\x73\x74\x69\x6E\x61\x74\x69\x6F\x6E\x20\x2D\x20\x53\x68\x6F\x75\x6C\x64\x20\x62\x65\x20\x61\x20\x6C\x69\x6E\x65\x20\x72\x65\x6E\x64\x65\x72\x65\x72\x2E"), NULL);
	}
}
static void InputPointerVisualizer_tD1C9426AA2AD4546530800E8206B5B09C7A804AD_CustomAttributesCacheGenerator_cursorDist(CustomAttributesCache* cache)
{
	{
		TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B * tmp = (TooltipAttribute_t503A1598A4E68E91673758F50447D0EDFB95149B *)cache->attributes[0];
		TooltipAttribute__ctor_m1839ACEC1560968A6D0EA55D7EB4535546588042(tmp, il2cpp_codegen_string_new_wrapper("\x44\x69\x73\x74\x61\x6E\x63\x65\x20\x74\x6F\x20\x64\x65\x66\x61\x75\x6C\x74\x20\x74\x6F\x20\x69\x6E\x20\x63\x61\x73\x65\x20\x6F\x66\x20\x6E\x6F\x20\x68\x69\x74\x20\x74\x61\x72\x67\x65\x74\x2E"), NULL);
	}
}
static void LogStructure_t65E0934AE48BFC188EAF8E1D7498F7BC7A714D5D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x4C\x6F\x67\x53\x74\x72\x75\x63\x74\x75\x72\x65"), NULL);
	}
}
static void LogStructureEyeGaze_t72EF741E9EAAB8FE5DAD7CC4533D5C1B8DB8BECD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x4C\x6F\x67\x53\x74\x72\x75\x63\x74\x75\x72\x65\x45\x79\x65\x47\x61\x7A\x65"), NULL);
	}
}
static void UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x55\x73\x65\x72\x49\x6E\x70\x75\x74\x50\x6C\x61\x79\x62\x61\x63\x6B"), NULL);
	}
}
static void UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_customFilename(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_heatmapRefs(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_U3CIsPlayingU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
	{
		DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 * tmp = (DebuggerBrowsableAttribute_t2FA4793AD1982F5150E07D26822ED5953CD90F53 *)cache->attributes[1];
		DebuggerBrowsableAttribute__ctor_mAA8BCC1E418754685F320B14A08AC226E76346E5(tmp, 0LL, NULL);
	}
}
static void UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_replaySpeed(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 10.0f, NULL);
	}
}
static void UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_UserInputPlayback_UWP_Load_m5788A70494218FE373050086B3654F96B15AAA46(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUWP_LoadU3Ed__12_t160E61830FAF366112986A0A34F22E9EE073E64D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F * tmp = (DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F *)cache->attributes[0];
		DebuggerStepThroughAttribute__ctor_m2B40F019B0DF22CF7A815AAB3D2D027225D59D85(tmp, NULL);
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[1];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CUWP_LoadU3Ed__12_t160E61830FAF366112986A0A34F22E9EE073E64D_0_0_0_var), NULL);
	}
}
static void UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_UserInputPlayback_UWP_LoadNewFile_mF348B60237E4F7DCEA10BC345BAEC56B9513FDAE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUWP_LoadNewFileU3Ed__13_t77C4C264E32F46DFAE7A7B03CF7068C5D031CB71_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CUWP_LoadNewFileU3Ed__13_t77C4C264E32F46DFAE7A7B03CF7068C5D031CB71_0_0_0_var), NULL);
	}
	{
		DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F * tmp = (DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F *)cache->attributes[1];
		DebuggerStepThroughAttribute__ctor_m2B40F019B0DF22CF7A815AAB3D2D027225D59D85(tmp, NULL);
	}
}
static void UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_UserInputPlayback_UWP_FileExists_m418F422637A601D32632A7EDD1A33DEB9FB624E0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUWP_FileExistsU3Ed__14_t39C80B99CD2D1B2F41522AEED4CFC6EEF2436CC8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CUWP_FileExistsU3Ed__14_t39C80B99CD2D1B2F41522AEED4CFC6EEF2436CC8_0_0_0_var), NULL);
	}
	{
		DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F * tmp = (DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F *)cache->attributes[1];
		DebuggerStepThroughAttribute__ctor_m2B40F019B0DF22CF7A815AAB3D2D027225D59D85(tmp, NULL);
	}
}
static void UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_UserInputPlayback_UWP_ReadData_m67177F6AB330EE92B655AF4C87F16D7AC8A85E50(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUWP_ReadDataU3Ed__15_t2AB9B79A3BFC04423D02231A2D501DDCFF3FB0D8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[0];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CUWP_ReadDataU3Ed__15_t2AB9B79A3BFC04423D02231A2D501DDCFF3FB0D8_0_0_0_var), NULL);
	}
	{
		DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F * tmp = (DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F *)cache->attributes[1];
		DebuggerStepThroughAttribute__ctor_m2B40F019B0DF22CF7A815AAB3D2D027225D59D85(tmp, NULL);
	}
}
static void UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_UserInputPlayback_LoadInUWP_m64594642768FF5FE2AB13AA978790AEF6420CB65(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadInUWPU3Ed__21_t24C5FD3475E2D7A787B3874D4713F17B3B33C400_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F * tmp = (DebuggerStepThroughAttribute_t4058F4B4E5E1DF6883627F75165741AF154B781F *)cache->attributes[0];
		DebuggerStepThroughAttribute__ctor_m2B40F019B0DF22CF7A815AAB3D2D027225D59D85(tmp, NULL);
	}
	{
		AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 * tmp = (AsyncStateMachineAttribute_tBDB4B958CFB5CD3BEE1427711FFC8C358C9BA6E6 *)cache->attributes[1];
		AsyncStateMachineAttribute__ctor_m9530B59D9722DE383A1703C52EBC1ED1FEFB100B(tmp, il2cpp_codegen_type_get_object(U3CLoadInUWPU3Ed__21_t24C5FD3475E2D7A787B3874D4713F17B3B33C400_0_0_0_var), NULL);
	}
}
static void UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_UserInputPlayback_set_IsPlaying_mCBC869EE68F90686CAD123225EEA43AD43205C64(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_UserInputPlayback_get_IsPlaying_mEC33D4DE5699A80B1228F6515DEBB43DE45C7A91(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_UserInputPlayback_PopulateHeatmap_m12AFD4C59AE8898EFB84E27AA69D1C526324ECB8(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CPopulateHeatmapU3Ed__42_tEE23C9F4ED699F6431644212A126E0B0D685DA84_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CPopulateHeatmapU3Ed__42_tEE23C9F4ED699F6431644212A126E0B0D685DA84_0_0_0_var), NULL);
	}
}
static void UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_UserInputPlayback_UpdateStatus_mD6B38FDE0F453849A87799311B028E23B7DB175B(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUpdateStatusU3Ed__43_t04A5CD192106000D7D3513F48FA80B8D35403EBB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CUpdateStatusU3Ed__43_t04A5CD192106000D7D3513F48FA80B8D35403EBB_0_0_0_var), NULL);
	}
}
static void UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_UserInputPlayback_AddToCounter_m41084542E3C5DCC962EB9929A40BF5E61922298D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAddToCounterU3Ed__44_t9030FB67DA45E920F753C3098D5B310AB504A561_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAddToCounterU3Ed__44_t9030FB67DA45E920F753C3098D5B310AB504A561_0_0_0_var), NULL);
	}
}
static void UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_UserInputPlayback_U3CShowAllAndFreezeU3Eb__34_0_mDF45375F98D731AB4729A0CA2E208E5B88EE62C2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUWP_LoadU3Ed__12_t160E61830FAF366112986A0A34F22E9EE073E64D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUWP_LoadU3Ed__12_t160E61830FAF366112986A0A34F22E9EE073E64D_CustomAttributesCacheGenerator_U3CUWP_LoadU3Ed__12_SetStateMachine_m7ED88E33A30408B900C9A7A3C35A4E02BFC5683E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUWP_LoadNewFileU3Ed__13_t77C4C264E32F46DFAE7A7B03CF7068C5D031CB71_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUWP_LoadNewFileU3Ed__13_t77C4C264E32F46DFAE7A7B03CF7068C5D031CB71_CustomAttributesCacheGenerator_U3CUWP_LoadNewFileU3Ed__13_SetStateMachine_m22C14D0803818A1A528BDA6D6179361CE22C7233(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUWP_FileExistsU3Ed__14_t39C80B99CD2D1B2F41522AEED4CFC6EEF2436CC8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUWP_FileExistsU3Ed__14_t39C80B99CD2D1B2F41522AEED4CFC6EEF2436CC8_CustomAttributesCacheGenerator_U3CUWP_FileExistsU3Ed__14_SetStateMachine_m3A2B9C0F394A24619019FE3C65461944E89E98DD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUWP_ReadDataU3Ed__15_t2AB9B79A3BFC04423D02231A2D501DDCFF3FB0D8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUWP_ReadDataU3Ed__15_t2AB9B79A3BFC04423D02231A2D501DDCFF3FB0D8_CustomAttributesCacheGenerator_U3CUWP_ReadDataU3Ed__15_SetStateMachine_mADF140A5B17E52067F652E349087443412A6D68C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadInUWPU3Ed__21_t24C5FD3475E2D7A787B3874D4713F17B3B33C400_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadInUWPU3Ed__21_t24C5FD3475E2D7A787B3874D4713F17B3B33C400_CustomAttributesCacheGenerator_U3CLoadInUWPU3Ed__21_SetStateMachine_m8FFD898942DB58C8CC1D604F4286C93A5FF9E428(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPopulateHeatmapU3Ed__42_tEE23C9F4ED699F6431644212A126E0B0D685DA84_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CPopulateHeatmapU3Ed__42_tEE23C9F4ED699F6431644212A126E0B0D685DA84_CustomAttributesCacheGenerator_U3CPopulateHeatmapU3Ed__42__ctor_m102DF160804926625E30794716CD29865EC82DD9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPopulateHeatmapU3Ed__42_tEE23C9F4ED699F6431644212A126E0B0D685DA84_CustomAttributesCacheGenerator_U3CPopulateHeatmapU3Ed__42_System_IDisposable_Dispose_m1097B7B94EB08BA3EF350FE660AD7B9956863392(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPopulateHeatmapU3Ed__42_tEE23C9F4ED699F6431644212A126E0B0D685DA84_CustomAttributesCacheGenerator_U3CPopulateHeatmapU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0976BC78E7B504C287CB571F0DE0B71DF8413CC6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPopulateHeatmapU3Ed__42_tEE23C9F4ED699F6431644212A126E0B0D685DA84_CustomAttributesCacheGenerator_U3CPopulateHeatmapU3Ed__42_System_Collections_IEnumerator_Reset_m83A7B4025FE7ECAD0E352634B658870920480462(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPopulateHeatmapU3Ed__42_tEE23C9F4ED699F6431644212A126E0B0D685DA84_CustomAttributesCacheGenerator_U3CPopulateHeatmapU3Ed__42_System_Collections_IEnumerator_get_Current_m661273649CF08D4B0CE3715EA1ACE2A6F8530DD3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateStatusU3Ed__43_t04A5CD192106000D7D3513F48FA80B8D35403EBB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUpdateStatusU3Ed__43_t04A5CD192106000D7D3513F48FA80B8D35403EBB_CustomAttributesCacheGenerator_U3CUpdateStatusU3Ed__43__ctor_m970C1666C59016AEFE7E3CB5257A6E95BA8A2B91(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateStatusU3Ed__43_t04A5CD192106000D7D3513F48FA80B8D35403EBB_CustomAttributesCacheGenerator_U3CUpdateStatusU3Ed__43_System_IDisposable_Dispose_m2BF8311A1FA147B50BAA705D688E487697456AFB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateStatusU3Ed__43_t04A5CD192106000D7D3513F48FA80B8D35403EBB_CustomAttributesCacheGenerator_U3CUpdateStatusU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE88D22C380772911600CEDC6B6C20DECF74174DC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateStatusU3Ed__43_t04A5CD192106000D7D3513F48FA80B8D35403EBB_CustomAttributesCacheGenerator_U3CUpdateStatusU3Ed__43_System_Collections_IEnumerator_Reset_mA92CE780FB948B861106556A7D441579F0E52951(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateStatusU3Ed__43_t04A5CD192106000D7D3513F48FA80B8D35403EBB_CustomAttributesCacheGenerator_U3CUpdateStatusU3Ed__43_System_Collections_IEnumerator_get_Current_m4E0F683876005E30B881179D5A5255E3BC229525(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAddToCounterU3Ed__44_t9030FB67DA45E920F753C3098D5B310AB504A561_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAddToCounterU3Ed__44_t9030FB67DA45E920F753C3098D5B310AB504A561_CustomAttributesCacheGenerator_U3CAddToCounterU3Ed__44__ctor_m6FB296DA9BFA0BA393DAD6F0B6CC62FE117DEA6B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAddToCounterU3Ed__44_t9030FB67DA45E920F753C3098D5B310AB504A561_CustomAttributesCacheGenerator_U3CAddToCounterU3Ed__44_System_IDisposable_Dispose_mDCE82C6860FA2A2FF993B0E52EFD549B701BBAE9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAddToCounterU3Ed__44_t9030FB67DA45E920F753C3098D5B310AB504A561_CustomAttributesCacheGenerator_U3CAddToCounterU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA8B93ED764B147113D8B736B485AE53DFD39DB52(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAddToCounterU3Ed__44_t9030FB67DA45E920F753C3098D5B310AB504A561_CustomAttributesCacheGenerator_U3CAddToCounterU3Ed__44_System_Collections_IEnumerator_Reset_m331A758A780FF4767AF4D233962A09D5E9D11954(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAddToCounterU3Ed__44_t9030FB67DA45E920F753C3098D5B310AB504A561_CustomAttributesCacheGenerator_U3CAddToCounterU3Ed__44_System_Collections_IEnumerator_get_Current_mA947986D577DD4062D7BFC7DBA9A0D6608A0CADC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void UserInputRecorder_t62C9F229C236C209AF8F3A4E2F4A50203B2ADE44_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x55\x73\x65\x72\x49\x6E\x70\x75\x74\x52\x65\x63\x6F\x72\x64\x65\x72"), NULL);
	}
}
static void UserInputRecorder_t62C9F229C236C209AF8F3A4E2F4A50203B2ADE44_CustomAttributesCacheGenerator_logStructure(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UserInputRecorderFeedback_tDDC174B81D39018027F583D0750618525B46B0DC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x55\x73\x65\x72\x49\x6E\x70\x75\x74\x52\x65\x63\x6F\x72\x64\x65\x72\x46\x65\x65\x64\x62\x61\x63\x6B"), NULL);
	}
}
static void UserInputRecorderFeedback_tDDC174B81D39018027F583D0750618525B46B0DC_CustomAttributesCacheGenerator_statusText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UserInputRecorderFeedback_tDDC174B81D39018027F583D0750618525B46B0DC_CustomAttributesCacheGenerator_maxShowDurationInSeconds(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UserInputRecorderFeedback_tDDC174B81D39018027F583D0750618525B46B0DC_CustomAttributesCacheGenerator_audio_StartRecording(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UserInputRecorderFeedback_tDDC174B81D39018027F583D0750618525B46B0DC_CustomAttributesCacheGenerator_audio_StopRecording(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UserInputRecorderFeedback_tDDC174B81D39018027F583D0750618525B46B0DC_CustomAttributesCacheGenerator_audio_StartPlayback(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UserInputRecorderFeedback_tDDC174B81D39018027F583D0750618525B46B0DC_CustomAttributesCacheGenerator_audio_PausePlayback(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UserInputRecorderFeedback_tDDC174B81D39018027F583D0750618525B46B0DC_CustomAttributesCacheGenerator_audio_LoadRecordedData(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UserInputRecorderUIController_tAD19FD678DA0CC11F1D6E0B479C6BC3716885E1F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 * tmp = (AddComponentMenu_t3477A931DC56E9A4F67FFA5745D657ADD2931100 *)cache->attributes[0];
		AddComponentMenu__ctor_m34CE7BDF93FA607429964AEF1D23436963EE8549(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x72\x69\x70\x74\x73\x2F\x4D\x52\x54\x4B\x2F\x45\x78\x61\x6D\x70\x6C\x65\x73\x2F\x55\x73\x65\x72\x49\x6E\x70\x75\x74\x52\x65\x63\x6F\x72\x64\x65\x72\x55\x49\x43\x6F\x6E\x74\x72\x6F\x6C\x6C\x65\x72"), NULL);
	}
}
static void UserInputRecorderUIController_tAD19FD678DA0CC11F1D6E0B479C6BC3716885E1F_CustomAttributesCacheGenerator_btn_StartRecording(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UserInputRecorderUIController_tAD19FD678DA0CC11F1D6E0B479C6BC3716885E1F_CustomAttributesCacheGenerator_btn_StopRecording(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UserInputRecorderUIController_tAD19FD678DA0CC11F1D6E0B479C6BC3716885E1F_CustomAttributesCacheGenerator_btn_StartPlayback_Inactive(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UserInputRecorderUIController_tAD19FD678DA0CC11F1D6E0B479C6BC3716885E1F_CustomAttributesCacheGenerator_btn_StartPlayback(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UserInputRecorderUIController_tAD19FD678DA0CC11F1D6E0B479C6BC3716885E1F_CustomAttributesCacheGenerator_btn_PausePlayback(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t7BC783FF47E68A2ADD462F1B81630FDD4FF30110_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[714] = 
{
	ARFeatheredPlaneMeshVisualizer_t1327CCD0B254624AEE8A13935E9BDB1BECA4F304_CustomAttributesCacheGenerator,
	AnchorCreator_t07D98CC500EA5D614148A24F0E8DCE5772B56BEC_CustomAttributesCacheGenerator,
	U3CObjectGlowU3Ed__4_t3296F14F1E3002A882E4B18A0F910BCD3D8654E8_CustomAttributesCacheGenerator,
	U3CObjectGlowU3Ed__13_t2420E3BC6E90FDA3AE8047F3A262A9A0ECA0FC66_CustomAttributesCacheGenerator,
	U3CStartU3Ed__4_t6C5C20C50A666BDE5941872858E901994274B948_CustomAttributesCacheGenerator,
	U3CStartU3Ed__10_t13C254D46B39AB253CE2B5B25479F038B54DBF01_CustomAttributesCacheGenerator,
	U3CStartU3Ed__10_tF18344D372584F361358E78B7DB8CCC4BBC449D1_CustomAttributesCacheGenerator,
	U3CAnimatePropertiesU3Ed__6_t15A2A4AE8295FA8A4B453E8A9EDFE3A92E52D517_CustomAttributesCacheGenerator,
	U3CWarpTextU3Ed__7_t0A70FD8329D91B599DDB7EBCEAB30A1168404906_CustomAttributesCacheGenerator,
	U3CStartU3Ed__4_t488FF63DDCD818027484B14F0FF95DD298E72D62_CustomAttributesCacheGenerator,
	U3CRevealCharactersU3Ed__7_t14708E46E1A66B01F15CACFFEB7FB12A271C7B2C_CustomAttributesCacheGenerator,
	U3CRevealWordsU3Ed__8_tC3EA2AB6154D5D909B175AE61A9A717D42A02ADA_CustomAttributesCacheGenerator,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_t2C450C526125FE0890D8581F5A5E60EA4BEA4177_CustomAttributesCacheGenerator,
	U3CDisplayTextMeshFloatingTextU3Ed__16_t45DFD3865E7CF6058313A502EEE4823924DF0ADF_CustomAttributesCacheGenerator,
	U3CAnimateVertexColorsU3Ed__3_t9EE128818D22D10FB704C73831F62C8D89E37DE6_CustomAttributesCacheGenerator,
	U3CAnimateVertexColorsU3Ed__11_tAC0216BA39C3BA66767A320D95DA1C392389AADE_CustomAttributesCacheGenerator,
	U3CAnimateVertexColorsU3Ed__11_tCF4677CE6128B93FB1DA8390DAA5B7BE85EB9B79_CustomAttributesCacheGenerator,
	U3CAnimateVertexColorsU3Ed__10_t096128B3F18D445CA2BC7915A490343F5CB40293_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_0_t45962AFEC260B93E3091482654F75F63398B1ADE_CustomAttributesCacheGenerator,
	U3CAnimateVertexColorsU3Ed__10_t80A253D2D22F54FF4777892E88E3C49CF40C0283_CustomAttributesCacheGenerator,
	U3CWarpTextU3Ed__8_t5E5B765727F5E411B819B7A5855C31D921A4CD4F_CustomAttributesCacheGenerator,
	InstantDwellSample_t906C3DC5D27F9829445E4EC0BF48F655FC0EE90E_CustomAttributesCacheGenerator,
	ListItemDwell_tD65C3DD6A4A9E19185BB0700A5D4D24265159866_CustomAttributesCacheGenerator,
	ToggleDwellSample_t3A51ABF9023DAB4F055EBCB63AF8A943EBB42762_CustomAttributesCacheGenerator,
	KeyboardTest_t3BB2F6627AE6806F38FC3BB29D9CF3DC1DCC99FC_CustomAttributesCacheGenerator,
	MicrophoneAmplitudeDemo_t3B38ACC364A3D3FBAA5E1ED48E021994F7843353_CustomAttributesCacheGenerator,
	GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator,
	GrabTouchExample_t68D2275E78565CAEB49DEEEC97441C462D269826_CustomAttributesCacheGenerator,
	RotateWithPan_t87849F9AC083BCBE4EA3513254033E0309E2B1EF_CustomAttributesCacheGenerator,
	U3CDeflateCoroutineU3Ed__17_t51DB69BD5458E3680AFD5FEB84AFF52D3EAEBEDA_CustomAttributesCacheGenerator,
	U3CInflateCoroutineU3Ed__18_tDEF4EE704B4C848D5E10E502151CC371CBFF0164_CustomAttributesCacheGenerator,
	LoFiFilterSelection_tE9D58D7964B6A6B2F4A4B53BC9E3F0538123279E_CustomAttributesCacheGenerator,
	TextToSpeechSample_t074932D20F9E19BE32D083D57F2BE8B0A19AC745_CustomAttributesCacheGenerator,
	BoundaryVisualizationDemo_tE6E398A0CDA93FD8387EC5C8B7236DF86E3B29AC_CustomAttributesCacheGenerator,
	DebugTextOutput_tD24FC6A02A33518D7142FA16B1159131977277FC_CustomAttributesCacheGenerator,
	DemoTouchButton_t4A0517F23180ABAF686A6F7A5D1170C9D2DB5A41_CustomAttributesCacheGenerator,
	HandInteractionTouch_tBD0410F82943DA6D9E05BC1BE521A121250B2220_CustomAttributesCacheGenerator,
	HandInteractionTouchRotate_t18A3A74DF6D3ADECF217F9A6FD840B55C6CE7D7C_CustomAttributesCacheGenerator,
	LaunchUri_t777F88E92C36FC506B6E02ED5857E27D01E6889A_CustomAttributesCacheGenerator,
	SystemKeyboardExample_t54325C6FC824F2691F1EDD471FBEBA326B3BE4F7_CustomAttributesCacheGenerator,
	TetheredPlacement_tF0ECFAE1E9964CD31FDF8225B43E88F73AFD5EEF_CustomAttributesCacheGenerator,
	ToggleBoundingBox_t22FA240E53038C2B8021E232F18303DE62A674B2_CustomAttributesCacheGenerator,
	DisablePointersExample_tCFF01316186515E9F8F96188A01E89D89BD3B04A_CustomAttributesCacheGenerator,
	Rotator_tCA9D5248AF1FACF74076F54BD0CD759018894849_CustomAttributesCacheGenerator,
	InputDataExample_t7D1AAA6071CB7CA1B6EF6D2F5BC2493035E6850D_CustomAttributesCacheGenerator,
	InputDataExampleGizmo_t3BA0A8C3399807A32D90E7EF0431099B3937AC2B_CustomAttributesCacheGenerator,
	SpawnOnPointerEvent_tC163DCFC36FCA9DDC03C9852D84FF8778614D546_CustomAttributesCacheGenerator,
	PrimaryPointerHandlerExample_tA4F25ED61FCEB8429C788E5918B0DD2BFF567F0F_CustomAttributesCacheGenerator,
	ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator,
	U3CUpdateListOverTimeU3Ed__33_t6F615CB841917C7A9D914665B70E25E5A2B78D61_CustomAttributesCacheGenerator,
	SolverExampleManager_tCC766F11426471D9B07E155705CFF3378D5228CE_CustomAttributesCacheGenerator,
	ClearSpatialObservations_t3567617E559B0D55730BF91310C23024AAF2531C_CustomAttributesCacheGenerator,
	BoundingBoxExampleTest_t920EC66E68751C735AB5FB7F83F532C46DA2BEDB_CustomAttributesCacheGenerator,
	U3CSequenceU3Ed__12_t056AE53EBAF2B9CF286C48D22F0C486DD67863C0_CustomAttributesCacheGenerator,
	U3CWaitForSpeechCommandU3Ed__14_t02FCE23E8825096EA938587649A5E60F44836910_CustomAttributesCacheGenerator,
	U3CSequenceU3Ed__12_t1CC8D60F8F8D94D9A497136AC0611C33C7913A73_CustomAttributesCacheGenerator,
	U3CWaitForSpeechCommandU3Ed__14_tF24D07507705A5DC8FE5B96BC8EF0EF1223BA8B9_CustomAttributesCacheGenerator,
	GridObjectLayoutControl_tC085893CB2C961AC9CBF6B6DC8F379776FA97A35_CustomAttributesCacheGenerator,
	U3CTestAnchorsU3Ed__7_t60EB0100F609816FA418017838B859C42D501123_CustomAttributesCacheGenerator,
	ChangeManipulation_t5D314B3B583DB2F36B1C50A731AE557C824829E6_CustomAttributesCacheGenerator,
	U3CHandleButtonClickU3Ed__14_t6DC14582F351A525C8BBBA55020C98E0EC7871E7_CustomAttributesCacheGenerator,
	U3COpenProgressIndicatorU3Ed__17_tCA040FFC07AD3E4D55D008542D434CD6D80B4388_CustomAttributesCacheGenerator,
	SliderLunarLander_tCC3911180680DDC07A7DF357A5039DCC83338A1D_CustomAttributesCacheGenerator,
	MixedRealityCapabilityDemo_tB168C0284BB55A7C2DC7269E6ABF652A8464CDEC_CustomAttributesCacheGenerator,
	ColorTap_t3968DEA6867F20CE61064E8D670CE3A4757B2FC3_CustomAttributesCacheGenerator,
	FollowEyeGazeGazeProvider_tCF30FF9F5815081DCD446A5EB5FD32D9ACC2F3C9_CustomAttributesCacheGenerator,
	PanZoomBase_t202BE6839273DFACBD9236A4C11306A4E4DF09B0_CustomAttributesCacheGenerator,
	U3CZoomAndStopU3Ed__78_t5DD82D6ECCE58A5515F5D95AABB14F522D219E46_CustomAttributesCacheGenerator,
	PanZoomBaseRectTransf_tD2B01CF8F2DC8FAD0B548F6B6B82064CADB979E5_CustomAttributesCacheGenerator,
	PanZoomBaseTexture_t42FA11BC2834FC26BB348A44DC0CA4E4B422AD3D_CustomAttributesCacheGenerator,
	PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator,
	PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator,
	ScrollRectTransf_tAC616AEB395D752AF7834443D37E44BCADE1406F_CustomAttributesCacheGenerator,
	ScrollTexture_tAE1FDD084678A7B8C5B780057A93630AFD47560C_CustomAttributesCacheGenerator,
	TargetMoveToCamera_t0ABE76E6AC5EFA94348D8CD457C65D8DBA2EAB26_CustomAttributesCacheGenerator,
	GrabReleaseDetector_tAE4C6AF4642DCC8AB2117205BE34A69EB5FAB642_CustomAttributesCacheGenerator,
	MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator,
	SnapTo_t4330BD61FEF351200A4FF00AABAA8B2FB888A2A6_CustomAttributesCacheGenerator,
	TransportToRespawnLocation_tFCBB88202A163EC84A401ADCBD723189884EDD87_CustomAttributesCacheGenerator,
	TriggerZonePlaceObjsWithin_t33B1B0C9F2E911BCBD31114F6574B16AD7219047_CustomAttributesCacheGenerator,
	HitBehaviorDestroyOnSelect_t95899B83013C85EC186992EE1AABC94DE2311CF4_CustomAttributesCacheGenerator,
	RotateWithConstSpeedDir_t30194E08C6AB4143A2038469E3B5D2D6CAE3608A_CustomAttributesCacheGenerator,
	TargetGroupCreatorRadial_t980AFA83ECED3B9654D24511FEB07DAC3118D9F1_CustomAttributesCacheGenerator,
	TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator,
	ToggleGameObject_t9969D493884F94B1CF86F1F834BD6E7271E047C0_CustomAttributesCacheGenerator,
	DrawOnTexture_t99BDA5310DF2AC6F885B874496A2537ED869005C_CustomAttributesCacheGenerator,
	U3CDrawAtU3Ed__19_tF9694B76A7C548907397EF887A0AF5273444C7A1_CustomAttributesCacheGenerator,
	U3CComputeHeatmapAtU3Ed__20_t57778DEFEFEA9FE64A0801F0D2B0B59F4398058F_CustomAttributesCacheGenerator,
	OnSelectVisualizerInputController_tB9FBC5958B594373F7A2B42AAE300CA8F961B226_CustomAttributesCacheGenerator,
	ParticleHeatmap_t6206EFF67123720F3FEE9DE9521D92DBA361DB9F_CustomAttributesCacheGenerator,
	AudioFeedbackPlayer_t5B3ACA94A40A08A9873B04A9F76A8B50AAA362A7_CustomAttributesCacheGenerator,
	FollowEyeGaze_t99F955746D639DF98AB3AB29C03B3853BF54FB0C_CustomAttributesCacheGenerator,
	SpeechVisualFeedback_t37EFE8BCEA67D376CC90165094981C0182F11D4C_CustomAttributesCacheGenerator,
	BlendOut_tB7E293CB36E83E19E2870E11705BE16E5DF0900C_CustomAttributesCacheGenerator,
	ChangeSize_t49BFDAE8C90A63E56A2579AC04FA12E909FE89C0_CustomAttributesCacheGenerator,
	FaceUser_t4BEBFC0407D7824C7B033116ABCC900A04E1DCCB_CustomAttributesCacheGenerator,
	KeepFacingCamera_t0E11683B24D4C5D1DEA905F103AA587F4304DC6E_CustomAttributesCacheGenerator,
	LoadAdditiveScene_tBAB7527A2C0FC63B97C9B9A863BC813612A4691C_CustomAttributesCacheGenerator,
	U3CLoadNewSceneU3Ed__6_t0E26EB5F61490AF9926F108D5E2AF8BFD3A41D9B_CustomAttributesCacheGenerator,
	MoveWithCamera_t713CAB32E98200C6F64B9013A82132212BE4CE70_CustomAttributesCacheGenerator,
	OnLoadStartScene_tB348169740631E1E1F7DF598FA0DA6A2FD91DB87_CustomAttributesCacheGenerator,
	OnLookAtRotateByEyeGaze_tC6DB484A3A2B0C7792840A6E70B82CE54B54A3A2_CustomAttributesCacheGenerator,
	DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator,
	DoNotRender_t5C652699B6FF291B6AF41FA936CEF2F9243315CB_CustomAttributesCacheGenerator,
	EyeCalibrationChecker_tC19835DAC8153DBEE0A3E08F367C3578A0E3B214_CustomAttributesCacheGenerator,
	KeepThisAlive_t6373FC2BC670FA2D10DF2EA75A5D40325044C99B_CustomAttributesCacheGenerator,
	StatusText_t6175A4B456E90A4490D283864EFEA6CF6DD274EB_CustomAttributesCacheGenerator,
	U3CLoadNewSceneU3Ed__8_t6CC37138968FD381723B0793506AC695ED553EF9_CustomAttributesCacheGenerator,
	OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass0_0_t2B7F40848D5A9FFE0BF064D1B1E9AA3221E5971C_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass1_0_1_t25BE1789EC1C3AC5447D39D361E55C23E82224EE_CustomAttributesCacheGenerator,
	U3CCreateNewLogFileU3Ed__14_tE9FFFDE66A83EFB7925BBCF9E4D461E949829D7A_CustomAttributesCacheGenerator,
	U3CLoadLogsU3Ed__21_t71C6912789A78D48E47827228CE2FB8377D5655F_CustomAttributesCacheGenerator,
	U3CSaveLogsU3Ed__22_tA5C05FED470C577465EB69A3BAA3AE8EA4ADF0EE_CustomAttributesCacheGenerator,
	InputPointerVisualizer_tD1C9426AA2AD4546530800E8206B5B09C7A804AD_CustomAttributesCacheGenerator,
	LogStructure_t65E0934AE48BFC188EAF8E1D7498F7BC7A714D5D_CustomAttributesCacheGenerator,
	LogStructureEyeGaze_t72EF741E9EAAB8FE5DAD7CC4533D5C1B8DB8BECD_CustomAttributesCacheGenerator,
	UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator,
	U3CUWP_LoadU3Ed__12_t160E61830FAF366112986A0A34F22E9EE073E64D_CustomAttributesCacheGenerator,
	U3CUWP_LoadNewFileU3Ed__13_t77C4C264E32F46DFAE7A7B03CF7068C5D031CB71_CustomAttributesCacheGenerator,
	U3CUWP_FileExistsU3Ed__14_t39C80B99CD2D1B2F41522AEED4CFC6EEF2436CC8_CustomAttributesCacheGenerator,
	U3CUWP_ReadDataU3Ed__15_t2AB9B79A3BFC04423D02231A2D501DDCFF3FB0D8_CustomAttributesCacheGenerator,
	U3CLoadInUWPU3Ed__21_t24C5FD3475E2D7A787B3874D4713F17B3B33C400_CustomAttributesCacheGenerator,
	U3CPopulateHeatmapU3Ed__42_tEE23C9F4ED699F6431644212A126E0B0D685DA84_CustomAttributesCacheGenerator,
	U3CUpdateStatusU3Ed__43_t04A5CD192106000D7D3513F48FA80B8D35403EBB_CustomAttributesCacheGenerator,
	U3CAddToCounterU3Ed__44_t9030FB67DA45E920F753C3098D5B310AB504A561_CustomAttributesCacheGenerator,
	UserInputRecorder_t62C9F229C236C209AF8F3A4E2F4A50203B2ADE44_CustomAttributesCacheGenerator,
	UserInputRecorderFeedback_tDDC174B81D39018027F583D0750618525B46B0DC_CustomAttributesCacheGenerator,
	UserInputRecorderUIController_tAD19FD678DA0CC11F1D6E0B479C6BC3716885E1F_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t7BC783FF47E68A2ADD462F1B81630FDD4FF30110_CustomAttributesCacheGenerator,
	ARFeatheredPlaneMeshVisualizer_t1327CCD0B254624AEE8A13935E9BDB1BECA4F304_CustomAttributesCacheGenerator_m_FeatheringWidth,
	AnchorCreator_t07D98CC500EA5D614148A24F0E8DCE5772B56BEC_CustomAttributesCacheGenerator_m_AnchorPrefab,
	DropdownSample_t9D3509399763FFB709ED3C29FFA59286C780C5CD_CustomAttributesCacheGenerator_text,
	DropdownSample_t9D3509399763FFB709ED3C29FFA59286C780C5CD_CustomAttributesCacheGenerator_dropdownWithoutPlaceholder,
	DropdownSample_t9D3509399763FFB709ED3C29FFA59286C780C5CD_CustomAttributesCacheGenerator_dropdownWithPlaceholder,
	TMP_TextEventHandler_t7D87D28BFB2254325AB5CF4D21DBDEF591DD3277_CustomAttributesCacheGenerator_m_OnCharacterSelection,
	TMP_TextEventHandler_t7D87D28BFB2254325AB5CF4D21DBDEF591DD3277_CustomAttributesCacheGenerator_m_OnSpriteSelection,
	TMP_TextEventHandler_t7D87D28BFB2254325AB5CF4D21DBDEF591DD3277_CustomAttributesCacheGenerator_m_OnWordSelection,
	TMP_TextEventHandler_t7D87D28BFB2254325AB5CF4D21DBDEF591DD3277_CustomAttributesCacheGenerator_m_OnLineSelection,
	TMP_TextEventHandler_t7D87D28BFB2254325AB5CF4D21DBDEF591DD3277_CustomAttributesCacheGenerator_m_OnLinkSelection,
	BaseDwellSample_t1D103FDA990DC40C609B6A4AA034D14B17F825E2_CustomAttributesCacheGenerator_dwellVisualImage,
	BaseDwellSample_t1D103FDA990DC40C609B6A4AA034D14B17F825E2_CustomAttributesCacheGenerator_targetButton,
	BaseDwellSample_t1D103FDA990DC40C609B6A4AA034D14B17F825E2_CustomAttributesCacheGenerator_U3CDwellHandlerU3Ek__BackingField,
	BaseDwellSample_t1D103FDA990DC40C609B6A4AA034D14B17F825E2_CustomAttributesCacheGenerator_U3CIsDwellingU3Ek__BackingField,
	InstantDwellSample_t906C3DC5D27F9829445E4EC0BF48F655FC0EE90E_CustomAttributesCacheGenerator_listItems,
	ListItemDwell_tD65C3DD6A4A9E19185BB0700A5D4D24265159866_CustomAttributesCacheGenerator_itemName,
	ListItemDwell_tD65C3DD6A4A9E19185BB0700A5D4D24265159866_CustomAttributesCacheGenerator_displayLabel,
	ToggleDwellSample_t3A51ABF9023DAB4F055EBCB63AF8A943EBB42762_CustomAttributesCacheGenerator_dwellStatus,
	ToggleDwellSample_t3A51ABF9023DAB4F055EBCB63AF8A943EBB42762_CustomAttributesCacheGenerator_buttonBackground,
	ToggleDwellSample_t3A51ABF9023DAB4F055EBCB63AF8A943EBB42762_CustomAttributesCacheGenerator_dwellOnColor,
	ToggleDwellSample_t3A51ABF9023DAB4F055EBCB63AF8A943EBB42762_CustomAttributesCacheGenerator_dwellOffColor,
	ToggleDwellSample_t3A51ABF9023DAB4F055EBCB63AF8A943EBB42762_CustomAttributesCacheGenerator_dwellIntendedColor,
	ToggleDwellSample_t3A51ABF9023DAB4F055EBCB63AF8A943EBB42762_CustomAttributesCacheGenerator_dwellVisualCancelDurationInFrames,
	DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_SavedSceneNamePrefix,
	DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_InstantiatePrefabs,
	DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_InstantiatedPrefab,
	DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_InstantiatedParent,
	DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_autoUpdateToggle,
	DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_quadsToggle,
	DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_inferRegionsToggle,
	DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_meshesToggle,
	DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_maskToggle,
	DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_platformToggle,
	DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_wallToggle,
	DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_floorToggle,
	DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_ceilingToggle,
	DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_worldToggle,
	DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_completelyInferred,
	DemoSceneUnderstandingController_t9B06B745069CD8C12928C3AAC6E492952908E51F_CustomAttributesCacheGenerator_backgroundToggle,
	KeyboardTest_t3BB2F6627AE6806F38FC3BB29D9CF3DC1DCC99FC_CustomAttributesCacheGenerator_keyboard,
	GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_holdIndicator,
	GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_manipulationIndicator,
	GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_navigationIndicator,
	GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_selectIndicator,
	GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_defaultMaterial,
	GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_holdMaterial,
	GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_manipulationMaterial,
	GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_navigationMaterial,
	GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_selectMaterial,
	GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_railsAxisX,
	GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_railsAxisY,
	GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_railsAxisZ,
	GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_holdAction,
	GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_navigationAction,
	GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_manipulationAction,
	GestureTester_tEA4D80AD235E811E79613DFBB92A43EC28EE7ACC_CustomAttributesCacheGenerator_tapAction,
	GrabTouchExample_t68D2275E78565CAEB49DEEEC97441C462D269826_CustomAttributesCacheGenerator_grabAction,
	RotateWithPan_t87849F9AC083BCBE4EA3513254033E0309E2B1EF_CustomAttributesCacheGenerator_panInputSource,
	DialogExampleController_tC224B81392615531E8383FFFE7CC720C58C7500D_CustomAttributesCacheGenerator_dialogPrefabLarge,
	DialogExampleController_tC224B81392615531E8383FFFE7CC720C58C7500D_CustomAttributesCacheGenerator_dialogPrefabMedium,
	DialogExampleController_tC224B81392615531E8383FFFE7CC720C58C7500D_CustomAttributesCacheGenerator_dialogPrefabSmall,
	LoFiFilterSelection_tE9D58D7964B6A6B2F4A4B53BC9E3F0538123279E_CustomAttributesCacheGenerator_NarrowBandTelephony,
	LoFiFilterSelection_tE9D58D7964B6A6B2F4A4B53BC9E3F0538123279E_CustomAttributesCacheGenerator_AmRadio,
	LoFiFilterSelection_tE9D58D7964B6A6B2F4A4B53BC9E3F0538123279E_CustomAttributesCacheGenerator_FullRange,
	LoFiFilterSelection_tE9D58D7964B6A6B2F4A4B53BC9E3F0538123279E_CustomAttributesCacheGenerator_UnknownQuality,
	BoundaryVisualizationDemo_tE6E398A0CDA93FD8387EC5C8B7236DF86E3B29AC_CustomAttributesCacheGenerator_showFloor,
	BoundaryVisualizationDemo_tE6E398A0CDA93FD8387EC5C8B7236DF86E3B29AC_CustomAttributesCacheGenerator_showPlayArea,
	BoundaryVisualizationDemo_tE6E398A0CDA93FD8387EC5C8B7236DF86E3B29AC_CustomAttributesCacheGenerator_showTrackedArea,
	BoundaryVisualizationDemo_tE6E398A0CDA93FD8387EC5C8B7236DF86E3B29AC_CustomAttributesCacheGenerator_showBoundaryWalls,
	BoundaryVisualizationDemo_tE6E398A0CDA93FD8387EC5C8B7236DF86E3B29AC_CustomAttributesCacheGenerator_showBoundaryCeiling,
	DebugTextOutput_tD24FC6A02A33518D7142FA16B1159131977277FC_CustomAttributesCacheGenerator_textMesh,
	DemoTouchButton_t4A0517F23180ABAF686A6F7A5D1170C9D2DB5A41_CustomAttributesCacheGenerator_debugMessage,
	HandInteractionTouch_tBD0410F82943DA6D9E05BC1BE521A121250B2220_CustomAttributesCacheGenerator_debugMessage,
	HandInteractionTouch_tBD0410F82943DA6D9E05BC1BE521A121250B2220_CustomAttributesCacheGenerator_debugMessage2,
	HandInteractionTouchRotate_t18A3A74DF6D3ADECF217F9A6FD840B55C6CE7D7C_CustomAttributesCacheGenerator_targetObjectTransform,
	HandInteractionTouchRotate_t18A3A74DF6D3ADECF217F9A6FD840B55C6CE7D7C_CustomAttributesCacheGenerator_rotateSpeed,
	SolverTrackedTargetType_tBBC449E2E26568DC90E58135B36D0DB12811F6CB_CustomAttributesCacheGenerator_solverHandler,
	SystemKeyboardExample_t54325C6FC824F2691F1EDD471FBEBA326B3BE4F7_CustomAttributesCacheGenerator_debugMessage,
	SystemKeyboardExample_t54325C6FC824F2691F1EDD471FBEBA326B3BE4F7_CustomAttributesCacheGenerator_mixedRealityKeyboardPreview,
	SystemKeyboardExample_t54325C6FC824F2691F1EDD471FBEBA326B3BE4F7_CustomAttributesCacheGenerator_disableUIInteractionWhenTyping,
	TetheredPlacement_tF0ECFAE1E9964CD31FDF8225B43E88F73AFD5EEF_CustomAttributesCacheGenerator_DistanceThreshold,
	ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_scrollView,
	ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_dynamicItem,
	ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_numItems,
	ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_lazyLoad,
	ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_itemsPerFrame,
	ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_loader,
	ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_cellWidth,
	ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_cellHeight,
	ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_cellDepth,
	ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_cellsPerTier,
	ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_tiersPerPage,
	ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_scrollPositionRef,
	ScrollablePagination_tF85B373DE319CEBC1E29B196F08A47A80E23ADEE_CustomAttributesCacheGenerator_scrollView,
	SolverExampleManager_tCC766F11426471D9B07E155705CFF3378D5228CE_CustomAttributesCacheGenerator_CustomTrackedObject,
	ShowPlaneFindingInstructions_t3BAC31E90BD8DA80DA6FD6A704D5C51597D192DA_CustomAttributesCacheGenerator_planeFindingPanel,
	GridObjectLayoutControl_tC085893CB2C961AC9CBF6B6DC8F379776FA97A35_CustomAttributesCacheGenerator_grid,
	GridObjectLayoutControl_tC085893CB2C961AC9CBF6B6DC8F379776FA97A35_CustomAttributesCacheGenerator_text,
	ReturnToBounds_tF30BD3E23A4AD52A99E97CFF36271E520A3BBF32_CustomAttributesCacheGenerator_frontBounds,
	ReturnToBounds_tF30BD3E23A4AD52A99E97CFF36271E520A3BBF32_CustomAttributesCacheGenerator_backBounds,
	ReturnToBounds_tF30BD3E23A4AD52A99E97CFF36271E520A3BBF32_CustomAttributesCacheGenerator_leftBounds,
	ReturnToBounds_tF30BD3E23A4AD52A99E97CFF36271E520A3BBF32_CustomAttributesCacheGenerator_rightBounds,
	ReturnToBounds_tF30BD3E23A4AD52A99E97CFF36271E520A3BBF32_CustomAttributesCacheGenerator_bottomBounds,
	ReturnToBounds_tF30BD3E23A4AD52A99E97CFF36271E520A3BBF32_CustomAttributesCacheGenerator_topBounds,
	ProgressIndicatorDemo_tD9BD7DCF1298C882786983D4268E77A3E9380474_CustomAttributesCacheGenerator_progressIndicatorLoadingBarGo,
	ProgressIndicatorDemo_tD9BD7DCF1298C882786983D4268E77A3E9380474_CustomAttributesCacheGenerator_progressIndicatorRotatingObjectGo,
	ProgressIndicatorDemo_tD9BD7DCF1298C882786983D4268E77A3E9380474_CustomAttributesCacheGenerator_progressIndicatorRotatingOrbsGo,
	ProgressIndicatorDemo_tD9BD7DCF1298C882786983D4268E77A3E9380474_CustomAttributesCacheGenerator_toggleBarKey,
	ProgressIndicatorDemo_tD9BD7DCF1298C882786983D4268E77A3E9380474_CustomAttributesCacheGenerator_toggleRotatingKey,
	ProgressIndicatorDemo_tD9BD7DCF1298C882786983D4268E77A3E9380474_CustomAttributesCacheGenerator_toggleOrbsKey,
	ProgressIndicatorDemo_tD9BD7DCF1298C882786983D4268E77A3E9380474_CustomAttributesCacheGenerator_loadingMessages,
	ProgressIndicatorDemo_tD9BD7DCF1298C882786983D4268E77A3E9380474_CustomAttributesCacheGenerator_loadingTime,
	SliderLunarLander_tCC3911180680DDC07A7DF357A5039DCC83338A1D_CustomAttributesCacheGenerator_transformLandingGear,
	MixedRealityCapabilityDemo_tB168C0284BB55A7C2DC7269E6ABF652A8464CDEC_CustomAttributesCacheGenerator_articulatedHandResult,
	MixedRealityCapabilityDemo_tB168C0284BB55A7C2DC7269E6ABF652A8464CDEC_CustomAttributesCacheGenerator_ggvHandResult,
	MixedRealityCapabilityDemo_tB168C0284BB55A7C2DC7269E6ABF652A8464CDEC_CustomAttributesCacheGenerator_motionControllerResult,
	MixedRealityCapabilityDemo_tB168C0284BB55A7C2DC7269E6ABF652A8464CDEC_CustomAttributesCacheGenerator_eyeTrackingResult,
	MixedRealityCapabilityDemo_tB168C0284BB55A7C2DC7269E6ABF652A8464CDEC_CustomAttributesCacheGenerator_voiceCommandResult,
	MixedRealityCapabilityDemo_tB168C0284BB55A7C2DC7269E6ABF652A8464CDEC_CustomAttributesCacheGenerator_voiceDictationResult,
	MixedRealityCapabilityDemo_tB168C0284BB55A7C2DC7269E6ABF652A8464CDEC_CustomAttributesCacheGenerator_spatialMeshResult,
	MixedRealityCapabilityDemo_tB168C0284BB55A7C2DC7269E6ABF652A8464CDEC_CustomAttributesCacheGenerator_spatialPlaneResult,
	MixedRealityCapabilityDemo_tB168C0284BB55A7C2DC7269E6ABF652A8464CDEC_CustomAttributesCacheGenerator_spatialPointResult,
	ReadingModeSceneBehavior_tF8E4C305C89C94C3FE039C6D08D41D98A11E08D7_CustomAttributesCacheGenerator_renderViewportScaleSlider,
	ColorTap_t3968DEA6867F20CE61064E8D670CE3A4757B2FC3_CustomAttributesCacheGenerator_tapAction,
	ColorTap_t3968DEA6867F20CE61064E8D670CE3A4757B2FC3_CustomAttributesCacheGenerator_color_IdleState,
	ColorTap_t3968DEA6867F20CE61064E8D670CE3A4757B2FC3_CustomAttributesCacheGenerator_color_OnHover,
	ColorTap_t3968DEA6867F20CE61064E8D670CE3A4757B2FC3_CustomAttributesCacheGenerator_color_OnSelect,
	FollowEyeGazeGazeProvider_tCF30FF9F5815081DCD446A5EB5FD32D9ACC2F3C9_CustomAttributesCacheGenerator_defaultDistanceInMeters,
	PanZoomBase_t202BE6839273DFACBD9236A4C11306A4E4DF09B0_CustomAttributesCacheGenerator_ZoomSpeedMax,
	PanZoomBaseTexture_t42FA11BC2834FC26BB348A44DC0CA4E4B422AD3D_CustomAttributesCacheGenerator_defaultAspectRatio,
	PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_rectTransfToNavigate,
	PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_refToViewPort,
	PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_zoomAcceleration,
	PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_zoomSpeedMax,
	PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_zoomMinScale,
	PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_zoomMaxScale,
	PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_zoomTimeInSecToZoom,
	PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_zoomGestureEnabledOnStartup,
	PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_panAutoScrollIsActive,
	PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_panSpeedHorizontal,
	PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_panSpeedVertical,
	PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_panMinDistFromCenter,
	PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_useSkimProofing,
	PanZoomRectTransf_t823FB3A5C0140F5DD2712B7ACFABBD0C52ED063B_CustomAttributesCacheGenerator_skimProofUpdateSpeed,
	PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_rendererOfTextureToBeNavigated,
	PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_zoomAcceleration,
	PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_zoomSpeedMax,
	PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_zoomMinScale,
	PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_zoomMaxScale,
	PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_zoomTimeInSecToZoom,
	PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_zoomGestureEnabledOnStartup,
	PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_panAutoScrollIsActive,
	PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_panSpeedHorizontal,
	PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_panSpeedVertical,
	PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_panMinDistFromCenter,
	PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_useSkimProofing,
	PanZoomTexture_t2E872A8EF459C3B02E9AF9A8D57736C1C0115EBC_CustomAttributesCacheGenerator_skimProofUpdateSpeed,
	ScrollRectTransf_tAC616AEB395D752AF7834443D37E44BCADE1406F_CustomAttributesCacheGenerator_rectTransfToNavigate,
	ScrollRectTransf_tAC616AEB395D752AF7834443D37E44BCADE1406F_CustomAttributesCacheGenerator_refToViewPort,
	ScrollRectTransf_tAC616AEB395D752AF7834443D37E44BCADE1406F_CustomAttributesCacheGenerator_autoGazeScrollIsActive,
	ScrollRectTransf_tAC616AEB395D752AF7834443D37E44BCADE1406F_CustomAttributesCacheGenerator_scrollSpeedHorizontal,
	ScrollRectTransf_tAC616AEB395D752AF7834443D37E44BCADE1406F_CustomAttributesCacheGenerator_scrollSpeedVertical,
	ScrollRectTransf_tAC616AEB395D752AF7834443D37E44BCADE1406F_CustomAttributesCacheGenerator_minDistFromCenterForAutoScroll,
	ScrollRectTransf_tAC616AEB395D752AF7834443D37E44BCADE1406F_CustomAttributesCacheGenerator_useSkimProofing,
	ScrollRectTransf_tAC616AEB395D752AF7834443D37E44BCADE1406F_CustomAttributesCacheGenerator_skimProofUpdateSpeed,
	ScrollRectTransf_tAC616AEB395D752AF7834443D37E44BCADE1406F_CustomAttributesCacheGenerator_customStartPos,
	ScrollTexture_tAE1FDD084678A7B8C5B780057A93630AFD47560C_CustomAttributesCacheGenerator_textureRendererToBeScrolled,
	ScrollTexture_tAE1FDD084678A7B8C5B780057A93630AFD47560C_CustomAttributesCacheGenerator_autoGazeScrollIsActive,
	ScrollTexture_tAE1FDD084678A7B8C5B780057A93630AFD47560C_CustomAttributesCacheGenerator_scrollSpeedHorizontal,
	ScrollTexture_tAE1FDD084678A7B8C5B780057A93630AFD47560C_CustomAttributesCacheGenerator_scrollSpeedVertical,
	ScrollTexture_tAE1FDD084678A7B8C5B780057A93630AFD47560C_CustomAttributesCacheGenerator_minDistFromCenterForAutoScroll,
	ScrollTexture_tAE1FDD084678A7B8C5B780057A93630AFD47560C_CustomAttributesCacheGenerator_onLookAtColliderSize,
	ScrollTexture_tAE1FDD084678A7B8C5B780057A93630AFD47560C_CustomAttributesCacheGenerator_useSkimProofing,
	ScrollTexture_tAE1FDD084678A7B8C5B780057A93630AFD47560C_CustomAttributesCacheGenerator_skimProofUpdateSpeed,
	TargetMoveToCamera_t0ABE76E6AC5EFA94348D8CD457C65D8DBA2EAB26_CustomAttributesCacheGenerator_speed,
	TargetMoveToCamera_t0ABE76E6AC5EFA94348D8CD457C65D8DBA2EAB26_CustomAttributesCacheGenerator_isEnabled,
	TargetMoveToCamera_t0ABE76E6AC5EFA94348D8CD457C65D8DBA2EAB26_CustomAttributesCacheGenerator_minDistToStopTransition,
	TargetMoveToCamera_t0ABE76E6AC5EFA94348D8CD457C65D8DBA2EAB26_CustomAttributesCacheGenerator_setToAutoRotateIfFocused,
	GrabReleaseDetector_tAE4C6AF4642DCC8AB2117205BE34A69EB5FAB642_CustomAttributesCacheGenerator_OnGrab,
	GrabReleaseDetector_tAE4C6AF4642DCC8AB2117205BE34A69EB5FAB642_CustomAttributesCacheGenerator_OnRelease,
	MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_useEyeSupportedTargetPlacement,
	MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_minLookAwayDistToEnableEyeWarp,
	MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_handInputEnabled,
	MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_handmapping,
	MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_deltaHandMovemThresh,
	MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_transparency_inTransition,
	MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_transparency_preview,
	MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_previewPlacemDistThresh,
	MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_freezeX,
	MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_freezeY,
	MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_freezeZ,
	MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_audio_OnDragStart,
	MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_audio_OnDragStop,
	MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_voiceAction_PutThis,
	MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_voiceAction_OverHere,
	MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_OnDragStart,
	MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_OnDrop,
	MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_useAsSlider,
	MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_txtOutput_sliderValue,
	MoveObjByEyeGaze_t3C78C9B3EBD5804E6B40185AC3FB800EC83D288F_CustomAttributesCacheGenerator_slider_snapToNearestDecimal,
	TransportToRespawnLocation_tFCBB88202A163EC84A401ADCBD723189884EDD87_CustomAttributesCacheGenerator_RespawnReference,
	TransportToRespawnLocation_tFCBB88202A163EC84A401ADCBD723189884EDD87_CustomAttributesCacheGenerator_AudioFX_OnRespawn,
	TriggerZonePlaceObjsWithin_t33B1B0C9F2E911BCBD31114F6574B16AD7219047_CustomAttributesCacheGenerator_ObjsToPlaceHere,
	TriggerZonePlaceObjsWithin_t33B1B0C9F2E911BCBD31114F6574B16AD7219047_CustomAttributesCacheGenerator_statusColor_idle,
	TriggerZonePlaceObjsWithin_t33B1B0C9F2E911BCBD31114F6574B16AD7219047_CustomAttributesCacheGenerator_statusColor_achieved,
	TriggerZonePlaceObjsWithin_t33B1B0C9F2E911BCBD31114F6574B16AD7219047_CustomAttributesCacheGenerator_AudioFx_Success,
	HitBehaviorDestroyOnSelect_t95899B83013C85EC186992EE1AABC94DE2311CF4_CustomAttributesCacheGenerator_visualFxTemplate_OnHit,
	HitBehaviorDestroyOnSelect_t95899B83013C85EC186992EE1AABC94DE2311CF4_CustomAttributesCacheGenerator_audioFx_CorrectTarget,
	HitBehaviorDestroyOnSelect_t95899B83013C85EC186992EE1AABC94DE2311CF4_CustomAttributesCacheGenerator_audioFx_IncorrectTarget,
	HitBehaviorDestroyOnSelect_t95899B83013C85EC186992EE1AABC94DE2311CF4_CustomAttributesCacheGenerator_is_a_valid_target,
	HitBehaviorDestroyOnSelect_t95899B83013C85EC186992EE1AABC94DE2311CF4_CustomAttributesCacheGenerator_targetIterator,
	RotateWithConstSpeedDir_t30194E08C6AB4143A2038469E3B5D2D6CAE3608A_CustomAttributesCacheGenerator_RotateByEulerAngles,
	RotateWithConstSpeedDir_t30194E08C6AB4143A2038469E3B5D2D6CAE3608A_CustomAttributesCacheGenerator_speed,
	TargetGroupCreatorRadial_t980AFA83ECED3B9654D24511FEB07DAC3118D9F1_CustomAttributesCacheGenerator_templates,
	TargetGroupCreatorRadial_t980AFA83ECED3B9654D24511FEB07DAC3118D9F1_CustomAttributesCacheGenerator_targetSizeInVisAngle,
	TargetGroupCreatorRadial_t980AFA83ECED3B9654D24511FEB07DAC3118D9F1_CustomAttributesCacheGenerator_keepVisAngleSizeConstant,
	TargetGroupCreatorRadial_t980AFA83ECED3B9654D24511FEB07DAC3118D9F1_CustomAttributesCacheGenerator_keepTargetsFacingTheCam,
	TargetGroupCreatorRadial_t980AFA83ECED3B9654D24511FEB07DAC3118D9F1_CustomAttributesCacheGenerator_hideTemplatesOnStartup,
	TargetGroupCreatorRadial_t980AFA83ECED3B9654D24511FEB07DAC3118D9F1_CustomAttributesCacheGenerator_radialLayout_nTargets,
	TargetGroupCreatorRadial_t980AFA83ECED3B9654D24511FEB07DAC3118D9F1_CustomAttributesCacheGenerator_radialLayout_radiusInVisAngle,
	TargetGroupCreatorRadial_t980AFA83ECED3B9654D24511FEB07DAC3118D9F1_CustomAttributesCacheGenerator_showTargetAtGroupCenter,
	TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_nrOfTargetsToSelect,
	TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_Randomize,
	TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_highlightColor,
	TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_maxNumberOfTries,
	TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_DeactiveDistractors,
	TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_template_VisualMarkerForCurrTarget,
	TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_SceneToLoadOnFinish,
	TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_AudioApplauseOnFinish,
	TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_OnAllTargetsSelected,
	TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_OnTargetSelected,
	TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_selectAction,
	ToggleGameObject_t9969D493884F94B1CF86F1F834BD6E7271E047C0_CustomAttributesCacheGenerator_objToShowHide,
	DrawOnTexture_t99BDA5310DF2AC6F885B874496A2537ED869005C_CustomAttributesCacheGenerator_drawBrushSize,
	DrawOnTexture_t99BDA5310DF2AC6F885B874496A2537ED869005C_CustomAttributesCacheGenerator_drawIntensity,
	DrawOnTexture_t99BDA5310DF2AC6F885B874496A2537ED869005C_CustomAttributesCacheGenerator_minThreshDeltaHeatMap,
	OnSelectVisualizerInputController_tB9FBC5958B594373F7A2B42AAE300CA8F961B226_CustomAttributesCacheGenerator_EventToTrigger,
	ParticleHeatmap_t6206EFF67123720F3FEE9DE9521D92DBA361DB9F_CustomAttributesCacheGenerator_colorGradient,
	ParticleHeatmap_t6206EFF67123720F3FEE9DE9521D92DBA361DB9F_CustomAttributesCacheGenerator_maxNumberOfParticles,
	ParticleHeatmap_t6206EFF67123720F3FEE9DE9521D92DBA361DB9F_CustomAttributesCacheGenerator_minParticleSize,
	ParticleHeatmap_t6206EFF67123720F3FEE9DE9521D92DBA361DB9F_CustomAttributesCacheGenerator_maxParticleSize,
	AudioFeedbackPlayer_t5B3ACA94A40A08A9873B04A9F76A8B50AAA362A7_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	FollowEyeGaze_t99F955746D639DF98AB3AB29C03B3853BF54FB0C_CustomAttributesCacheGenerator_defaultDistanceInMeters,
	SpeechVisualFeedback_t37EFE8BCEA67D376CC90165094981C0182F11D4C_CustomAttributesCacheGenerator_visualFeedbackTemplate,
	SpeechVisualFeedback_t37EFE8BCEA67D376CC90165094981C0182F11D4C_CustomAttributesCacheGenerator_maxShowtimeInSeconds,
	BlendOut_tB7E293CB36E83E19E2870E11705BE16E5DF0900C_CustomAttributesCacheGenerator_BlendOutSpeed,
	BlendOut_tB7E293CB36E83E19E2870E11705BE16E5DF0900C_CustomAttributesCacheGenerator_MinTransparency,
	BlendOut_tB7E293CB36E83E19E2870E11705BE16E5DF0900C_CustomAttributesCacheGenerator_LookAtTransparency,
	BlendOut_tB7E293CB36E83E19E2870E11705BE16E5DF0900C_CustomAttributesCacheGenerator_IdleTransparency,
	BlendOut_tB7E293CB36E83E19E2870E11705BE16E5DF0900C_CustomAttributesCacheGenerator_DestroyAfterBlendOut,
	BlendOut_tB7E293CB36E83E19E2870E11705BE16E5DF0900C_CustomAttributesCacheGenerator_DisableAfterBlendOut,
	BlendOut_tB7E293CB36E83E19E2870E11705BE16E5DF0900C_CustomAttributesCacheGenerator_DwellRequired,
	BlendOut_tB7E293CB36E83E19E2870E11705BE16E5DF0900C_CustomAttributesCacheGenerator_shaderPropsToCheckForBlending,
	ChangeSize_t49BFDAE8C90A63E56A2579AC04FA12E909FE89C0_CustomAttributesCacheGenerator_Size_OnLookAt,
	ChangeSize_t49BFDAE8C90A63E56A2579AC04FA12E909FE89C0_CustomAttributesCacheGenerator_SizeChangeSpeed,
	ChangeSize_t49BFDAE8C90A63E56A2579AC04FA12E909FE89C0_CustomAttributesCacheGenerator_SizeDeltaThresh,
	FaceUser_t4BEBFC0407D7824C7B033116ABCC900A04E1DCCB_CustomAttributesCacheGenerator_Speed,
	FaceUser_t4BEBFC0407D7824C7B033116ABCC900A04E1DCCB_CustomAttributesCacheGenerator_RotationThreshInDegrees,
	LoadAdditiveScene_tBAB7527A2C0FC63B97C9B9A863BC813612A4691C_CustomAttributesCacheGenerator_SceneToBeLoaded,
	LoadAdditiveScene_tBAB7527A2C0FC63B97C9B9A863BC813612A4691C_CustomAttributesCacheGenerator_audio_OnSelect,
	LoadAdditiveScene_tBAB7527A2C0FC63B97C9B9A863BC813612A4691C_CustomAttributesCacheGenerator_waitTimeInSecBeforeLoading,
	MoveWithCamera_t713CAB32E98200C6F64B9013A82132212BE4CE70_CustomAttributesCacheGenerator_offsetToCamera,
	OnLoadStartScene_tB348169740631E1E1F7DF598FA0DA6A2FD91DB87_CustomAttributesCacheGenerator_SceneToBeLoaded,
	OnLoadStartScene_tB348169740631E1E1F7DF598FA0DA6A2FD91DB87_CustomAttributesCacheGenerator_LoadOption,
	OnLookAtRotateByEyeGaze_tC6DB484A3A2B0C7792840A6E70B82CE54B54A3A2_CustomAttributesCacheGenerator_speedX,
	OnLookAtRotateByEyeGaze_tC6DB484A3A2B0C7792840A6E70B82CE54B54A3A2_CustomAttributesCacheGenerator_speedY,
	OnLookAtRotateByEyeGaze_tC6DB484A3A2B0C7792840A6E70B82CE54B54A3A2_CustomAttributesCacheGenerator_inverseX,
	OnLookAtRotateByEyeGaze_tC6DB484A3A2B0C7792840A6E70B82CE54B54A3A2_CustomAttributesCacheGenerator_inverseY,
	OnLookAtRotateByEyeGaze_tC6DB484A3A2B0C7792840A6E70B82CE54B54A3A2_CustomAttributesCacheGenerator_rotationThreshInDegrees,
	OnLookAtRotateByEyeGaze_tC6DB484A3A2B0C7792840A6E70B82CE54B54A3A2_CustomAttributesCacheGenerator_minRotX,
	OnLookAtRotateByEyeGaze_tC6DB484A3A2B0C7792840A6E70B82CE54B54A3A2_CustomAttributesCacheGenerator_maxRotX,
	OnLookAtRotateByEyeGaze_tC6DB484A3A2B0C7792840A6E70B82CE54B54A3A2_CustomAttributesCacheGenerator_minRotY,
	OnLookAtRotateByEyeGaze_tC6DB484A3A2B0C7792840A6E70B82CE54B54A3A2_CustomAttributesCacheGenerator_maxRotY,
	DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator_startEnabledOnStartup,
	DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator_rootFeedbackToEnable,
	DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator_feedbackToChangeInSize,
	DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator_feedbackStartSize,
	DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator_feedbackEndSize,
	DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator_feedbackDelayInSeconds,
	DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator_dwellTimeInSecondsToSelect,
	DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator_startScale,
	DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator_endScale,
	DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator_startTransp,
	DwellSelection_tA2182E3A86AA9BDB9E8DD2686471C5A9EDEDC894_CustomAttributesCacheGenerator_endTransp,
	TargetEventArgs_t7713E8CD48F3A9DC5C9036F0CDD4C44DDA493350_CustomAttributesCacheGenerator_U3CHitTargetU3Ek__BackingField,
	EyeCalibrationChecker_tC19835DAC8153DBEE0A3E08F367C3578A0E3B214_CustomAttributesCacheGenerator_editorTestUserIsCalibrated,
	KeepThisAlive_t6373FC2BC670FA2D10DF2EA75A5D40325044C99B_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	StatusText_t6175A4B456E90A4490D283864EFEA6CF6DD274EB_CustomAttributesCacheGenerator_status,
	OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator_Overlay_UseIt,
	OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator_Overlay_GameObj,
	OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator_startTransparency,
	OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator_endTransparency,
	OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator_Highlight_UseIt,
	OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator_Highlight_Color,
	OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator_MinDwellTimeInMs,
	OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator_MaxDwellTimeInMs,
	OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator_MinLookAwayTimeInMs,
	OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator_MaxLookAwayTimeInMs,
	OnLookAtShowHoverFeedback_t6C41EF4A6B84FABA1AD4826275611DFAD148C57A_CustomAttributesCacheGenerator_BlendType,
	ExclusiveSynchronizationContext_tF7C7F5ABCA05A45DB107864767F66F827CC903B8_CustomAttributesCacheGenerator_U3CInnerExceptionU3Ek__BackingField,
	BasicInputLogger_tF39D6623A3849E400F84E3E8ADB2CBF4FC2F9D4F_CustomAttributesCacheGenerator_sessionDescr,
	CustomInputLogger_tCD760BEF5586405526D75D8209961F56AB53A484_CustomAttributesCacheGenerator_filename,
	InputPointerVisualizer_tD1C9426AA2AD4546530800E8206B5B09C7A804AD_CustomAttributesCacheGenerator_useLiveInputStream,
	InputPointerVisualizer_tD1C9426AA2AD4546530800E8206B5B09C7A804AD_CustomAttributesCacheGenerator_onlyShowForHitTargets,
	InputPointerVisualizer_tD1C9426AA2AD4546530800E8206B5B09C7A804AD_CustomAttributesCacheGenerator_tmplt_Origins,
	InputPointerVisualizer_tD1C9426AA2AD4546530800E8206B5B09C7A804AD_CustomAttributesCacheGenerator_tmplt_Destinations,
	InputPointerVisualizer_tD1C9426AA2AD4546530800E8206B5B09C7A804AD_CustomAttributesCacheGenerator_tmplt_LinkOrigToOrig,
	InputPointerVisualizer_tD1C9426AA2AD4546530800E8206B5B09C7A804AD_CustomAttributesCacheGenerator_tmplt_LinkDestToDest,
	InputPointerVisualizer_tD1C9426AA2AD4546530800E8206B5B09C7A804AD_CustomAttributesCacheGenerator_tmplt_LinkOrigToDest,
	InputPointerVisualizer_tD1C9426AA2AD4546530800E8206B5B09C7A804AD_CustomAttributesCacheGenerator_cursorDist,
	UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_customFilename,
	UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_heatmapRefs,
	UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_U3CIsPlayingU3Ek__BackingField,
	UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_replaySpeed,
	UserInputRecorder_t62C9F229C236C209AF8F3A4E2F4A50203B2ADE44_CustomAttributesCacheGenerator_logStructure,
	UserInputRecorderFeedback_tDDC174B81D39018027F583D0750618525B46B0DC_CustomAttributesCacheGenerator_statusText,
	UserInputRecorderFeedback_tDDC174B81D39018027F583D0750618525B46B0DC_CustomAttributesCacheGenerator_maxShowDurationInSeconds,
	UserInputRecorderFeedback_tDDC174B81D39018027F583D0750618525B46B0DC_CustomAttributesCacheGenerator_audio_StartRecording,
	UserInputRecorderFeedback_tDDC174B81D39018027F583D0750618525B46B0DC_CustomAttributesCacheGenerator_audio_StopRecording,
	UserInputRecorderFeedback_tDDC174B81D39018027F583D0750618525B46B0DC_CustomAttributesCacheGenerator_audio_StartPlayback,
	UserInputRecorderFeedback_tDDC174B81D39018027F583D0750618525B46B0DC_CustomAttributesCacheGenerator_audio_PausePlayback,
	UserInputRecorderFeedback_tDDC174B81D39018027F583D0750618525B46B0DC_CustomAttributesCacheGenerator_audio_LoadRecordedData,
	UserInputRecorderUIController_tAD19FD678DA0CC11F1D6E0B479C6BC3716885E1F_CustomAttributesCacheGenerator_btn_StartRecording,
	UserInputRecorderUIController_tAD19FD678DA0CC11F1D6E0B479C6BC3716885E1F_CustomAttributesCacheGenerator_btn_StopRecording,
	UserInputRecorderUIController_tAD19FD678DA0CC11F1D6E0B479C6BC3716885E1F_CustomAttributesCacheGenerator_btn_StartPlayback_Inactive,
	UserInputRecorderUIController_tAD19FD678DA0CC11F1D6E0B479C6BC3716885E1F_CustomAttributesCacheGenerator_btn_StartPlayback,
	UserInputRecorderUIController_tAD19FD678DA0CC11F1D6E0B479C6BC3716885E1F_CustomAttributesCacheGenerator_btn_PausePlayback,
	ButtonOrder_t245C2F5F9D03DCFED8D1322B301B24E0251CB8A4_CustomAttributesCacheGenerator_ButtonOrder_ObjectGlow_mFFACD508BE48F738C0B6F6670043CCD24E8D9A28,
	U3CObjectGlowU3Ed__4_t3296F14F1E3002A882E4B18A0F910BCD3D8654E8_CustomAttributesCacheGenerator_U3CObjectGlowU3Ed__4__ctor_mFFF411C1EF1D8807296A811F1F11ED26AD45D30D,
	U3CObjectGlowU3Ed__4_t3296F14F1E3002A882E4B18A0F910BCD3D8654E8_CustomAttributesCacheGenerator_U3CObjectGlowU3Ed__4_System_IDisposable_Dispose_mDDAB1BA6F77D08AA6F33C428CC442A4F6DDD67C0,
	U3CObjectGlowU3Ed__4_t3296F14F1E3002A882E4B18A0F910BCD3D8654E8_CustomAttributesCacheGenerator_U3CObjectGlowU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE1361ED0C0996CF5D8FFF29168BDB0CC68A62A85,
	U3CObjectGlowU3Ed__4_t3296F14F1E3002A882E4B18A0F910BCD3D8654E8_CustomAttributesCacheGenerator_U3CObjectGlowU3Ed__4_System_Collections_IEnumerator_Reset_m49133519AB880CF16DE2DB819F3D2D174E7F9FBE,
	U3CObjectGlowU3Ed__4_t3296F14F1E3002A882E4B18A0F910BCD3D8654E8_CustomAttributesCacheGenerator_U3CObjectGlowU3Ed__4_System_Collections_IEnumerator_get_Current_mCF9EF965D329EA8A4BA33843423711DF62E304D6,
	nailPulling_t604FA20D1C5869E4D43986026F4777C92C3F4B1A_CustomAttributesCacheGenerator_nailPulling_ObjectGlow_m671AFA1FB9F8625C9ED1C3EC4F1DFF56D06486EE,
	U3CObjectGlowU3Ed__13_t2420E3BC6E90FDA3AE8047F3A262A9A0ECA0FC66_CustomAttributesCacheGenerator_U3CObjectGlowU3Ed__13__ctor_m36705611765608E1AB5180A973D57E7E35844585,
	U3CObjectGlowU3Ed__13_t2420E3BC6E90FDA3AE8047F3A262A9A0ECA0FC66_CustomAttributesCacheGenerator_U3CObjectGlowU3Ed__13_System_IDisposable_Dispose_m5B1BAE725BBB3C5F71C815456DA1E0DF40A6D6B3,
	U3CObjectGlowU3Ed__13_t2420E3BC6E90FDA3AE8047F3A262A9A0ECA0FC66_CustomAttributesCacheGenerator_U3CObjectGlowU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m33227A5F49FE9DCF93DC806DF8654EEAF52C2F4B,
	U3CObjectGlowU3Ed__13_t2420E3BC6E90FDA3AE8047F3A262A9A0ECA0FC66_CustomAttributesCacheGenerator_U3CObjectGlowU3Ed__13_System_Collections_IEnumerator_Reset_m49EB597CCB8F39CD4041F0C029EED9BBDA1EC76B,
	U3CObjectGlowU3Ed__13_t2420E3BC6E90FDA3AE8047F3A262A9A0ECA0FC66_CustomAttributesCacheGenerator_U3CObjectGlowU3Ed__13_System_Collections_IEnumerator_get_Current_mC162CD86B5EF34EF47ADE853F214878EF5101AAA,
	EnvMapAnimator_tF07513776BE9808D0FD92E8A773D4CB71679DBF9_CustomAttributesCacheGenerator_EnvMapAnimator_Start_m3432946DE1A3B40667B9D4CE90384F765C9A1788,
	U3CStartU3Ed__4_t6C5C20C50A666BDE5941872858E901994274B948_CustomAttributesCacheGenerator_U3CStartU3Ed__4__ctor_m0450ADD262F7838968EDCEA567F1046D7AA157E2,
	U3CStartU3Ed__4_t6C5C20C50A666BDE5941872858E901994274B948_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_IDisposable_Dispose_m0E6287744F66D21C42C549B857E1A4A3BECDBC8A,
	U3CStartU3Ed__4_t6C5C20C50A666BDE5941872858E901994274B948_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1A4BE78025454A74E21F9730A5269AE2E2461258,
	U3CStartU3Ed__4_t6C5C20C50A666BDE5941872858E901994274B948_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m912B63F0B462B11EAC1AA20D7DF921990F53BF82,
	U3CStartU3Ed__4_t6C5C20C50A666BDE5941872858E901994274B948_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m4C9D59B07EECBC8BF4F62D136E6A833D1CF3DB8E,
	Benchmark01_tD75224AB8C8270F9AB89CDC6406DC065AD37B1C8_CustomAttributesCacheGenerator_Benchmark01_Start_mB7AE105F2B9A718DA0034E36DCB2DD5D3CE08E79,
	U3CStartU3Ed__10_t13C254D46B39AB253CE2B5B25479F038B54DBF01_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_mF134B42BDB16CFE45959D2238A883F3782D96A6B,
	U3CStartU3Ed__10_t13C254D46B39AB253CE2B5B25479F038B54DBF01_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_m73EE988CEE5D33849663BBF874B890EFDF4D6C2C,
	U3CStartU3Ed__10_t13C254D46B39AB253CE2B5B25479F038B54DBF01_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m40C85CAAA5F91860372CE6EC7DE2BC0651008A47,
	U3CStartU3Ed__10_t13C254D46B39AB253CE2B5B25479F038B54DBF01_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m9B240EF4945F412941C11524E6ADD12C718C96CB,
	U3CStartU3Ed__10_t13C254D46B39AB253CE2B5B25479F038B54DBF01_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m99360A58385EF846DA9324753FFD814763486E44,
	Benchmark01_UGUI_t5E4288018FE0F6AE5524F187F2B622E5F47B75A5_CustomAttributesCacheGenerator_Benchmark01_UGUI_Start_m34BF3D36C4148DD9B6AFF435494132D134DB1FEB,
	U3CStartU3Ed__10_tF18344D372584F361358E78B7DB8CCC4BBC449D1_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_mBD96F83AB86F1DADD0205D48950AD9472C49BB3C,
	U3CStartU3Ed__10_tF18344D372584F361358E78B7DB8CCC4BBC449D1_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_mA9BCB5EB21EAD06A6B3CAA5F637E4E3E61690A15,
	U3CStartU3Ed__10_tF18344D372584F361358E78B7DB8CCC4BBC449D1_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m73A9DBD9986792A9C873E94820A327719DAD13B2,
	U3CStartU3Ed__10_tF18344D372584F361358E78B7DB8CCC4BBC449D1_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m8673C491BA40498DA72FCF82D0C35CDB2B2C1302,
	U3CStartU3Ed__10_tF18344D372584F361358E78B7DB8CCC4BBC449D1_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m1403238260593386B4796ABC7CE5DE15710C066E,
	ShaderPropAnimator_tCF2361E34C8D8382EAA332EA1148C452860CA47D_CustomAttributesCacheGenerator_ShaderPropAnimator_AnimateProperties_m9635CDE20D007A404C2D8F09E0E3D474F83404BD,
	U3CAnimatePropertiesU3Ed__6_t15A2A4AE8295FA8A4B453E8A9EDFE3A92E52D517_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6__ctor_m1EECC5609C55D8ACE8A015D095176613DF1CC729,
	U3CAnimatePropertiesU3Ed__6_t15A2A4AE8295FA8A4B453E8A9EDFE3A92E52D517_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m899F6FDE18D7A082B51E81253115D11211B9E306,
	U3CAnimatePropertiesU3Ed__6_t15A2A4AE8295FA8A4B453E8A9EDFE3A92E52D517_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB8A061DA69769417CF2DB406083A6B9FEC9216BD,
	U3CAnimatePropertiesU3Ed__6_t15A2A4AE8295FA8A4B453E8A9EDFE3A92E52D517_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_m96C640E17307465DC89EC0216DEC638A1434FB7F,
	U3CAnimatePropertiesU3Ed__6_t15A2A4AE8295FA8A4B453E8A9EDFE3A92E52D517_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m896EEE48086AA67D02FFA51CE50AF7184343D25B,
	SkewTextExample_t38AD31CC73BE149ECF960D04D8EE25D9513C8F99_CustomAttributesCacheGenerator_SkewTextExample_WarpText_mE1969FED4C69A06BF296EB3C57D655E70067C268,
	U3CWarpTextU3Ed__7_t0A70FD8329D91B599DDB7EBCEAB30A1168404906_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7__ctor_m0C10F1442F3AFF382988656986D6043B8E15A143,
	U3CWarpTextU3Ed__7_t0A70FD8329D91B599DDB7EBCEAB30A1168404906_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_IDisposable_Dispose_mB08192042C7064309C074E7E2A091ACB1F4DEFD9,
	U3CWarpTextU3Ed__7_t0A70FD8329D91B599DDB7EBCEAB30A1168404906_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBB2D90533557FC705A99905EA0757D55CF5BFEFF,
	U3CWarpTextU3Ed__7_t0A70FD8329D91B599DDB7EBCEAB30A1168404906_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_m98CD8E7319A8FBC6CAFF79C5CBA0A29C7325967E,
	U3CWarpTextU3Ed__7_t0A70FD8329D91B599DDB7EBCEAB30A1168404906_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m8E6A1A9AD6B87208DFEB11A4769BB5A79E90E1D3,
	TeleType_t9472171E255BE0707D857C416C12F6F4A7D7A972_CustomAttributesCacheGenerator_TeleType_Start_m37AB7AE2F364CEC3F6181C53750D67E08B07B63B,
	U3CStartU3Ed__4_t488FF63DDCD818027484B14F0FF95DD298E72D62_CustomAttributesCacheGenerator_U3CStartU3Ed__4__ctor_mE761C315080CB9D9ECC5E13541382FD8F4CF6A70,
	U3CStartU3Ed__4_t488FF63DDCD818027484B14F0FF95DD298E72D62_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_IDisposable_Dispose_m32D3DFDFEBC3CB106D26E913802FD00E209C12FF,
	U3CStartU3Ed__4_t488FF63DDCD818027484B14F0FF95DD298E72D62_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC97655E37D3DEC57375D73AAE9E98572E8F81E27,
	U3CStartU3Ed__4_t488FF63DDCD818027484B14F0FF95DD298E72D62_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m8CDA19A57B0E471F332E9C43B122FEC63FB14FB4,
	U3CStartU3Ed__4_t488FF63DDCD818027484B14F0FF95DD298E72D62_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mA02188F69F7BC357C743F33D3A643A7ECD0DBF69,
	TextConsoleSimulator_t5D67BCF6EE238EA70D9CE2544BCC74D079B496A5_CustomAttributesCacheGenerator_TextConsoleSimulator_RevealCharacters_m270C2B31ECEBC901F31CC780869FF0BF9ADBFFCF,
	TextConsoleSimulator_t5D67BCF6EE238EA70D9CE2544BCC74D079B496A5_CustomAttributesCacheGenerator_TextConsoleSimulator_RevealWords_m2831A10B9CBE63D3B6A96764F84F98860FDDEA87,
	U3CRevealCharactersU3Ed__7_t14708E46E1A66B01F15CACFFEB7FB12A271C7B2C_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7__ctor_mC622CB57F2C8C2C5E5D6592FA61D37D9B2DD2F4E,
	U3CRevealCharactersU3Ed__7_t14708E46E1A66B01F15CACFFEB7FB12A271C7B2C_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m8581BA87F52BA3D2EC25328353D57DCEF2A57932,
	U3CRevealCharactersU3Ed__7_t14708E46E1A66B01F15CACFFEB7FB12A271C7B2C_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F82F9A02F414D7485FD3AC3003ED386F5329009,
	U3CRevealCharactersU3Ed__7_t14708E46E1A66B01F15CACFFEB7FB12A271C7B2C_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_m0A85EE143D0136BDEF293624A8B419EDFBCAC183,
	U3CRevealCharactersU3Ed__7_t14708E46E1A66B01F15CACFFEB7FB12A271C7B2C_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m52987689D9C37472E3A5BB72A11F676BDA9708C8,
	U3CRevealWordsU3Ed__8_tC3EA2AB6154D5D909B175AE61A9A717D42A02ADA_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8__ctor_mF28D666A0DA72F5541416C9A900D63FFF1826D3C,
	U3CRevealWordsU3Ed__8_tC3EA2AB6154D5D909B175AE61A9A717D42A02ADA_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m1BB12C3BC2792B0BFDB4BAE14EA6C876A64FEED2,
	U3CRevealWordsU3Ed__8_tC3EA2AB6154D5D909B175AE61A9A717D42A02ADA_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m78510D10295A5465DF7FA6CC3062988A0A8C21F0,
	U3CRevealWordsU3Ed__8_tC3EA2AB6154D5D909B175AE61A9A717D42A02ADA_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_m5DBF478AE8BD7F6A592EEAA1D0955006E69E36F6,
	U3CRevealWordsU3Ed__8_tC3EA2AB6154D5D909B175AE61A9A717D42A02ADA_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mA8E2269E365F70178BEEC3156303BAB8F7115642,
	TextMeshProFloatingText_t1B042E0159910D71C74E88EC19A866D5861502B9_CustomAttributesCacheGenerator_TextMeshProFloatingText_DisplayTextMeshProFloatingText_m214F7E4C44FAE3A3635443BD41F2645BEF83D07C,
	TextMeshProFloatingText_t1B042E0159910D71C74E88EC19A866D5861502B9_CustomAttributesCacheGenerator_TextMeshProFloatingText_DisplayTextMeshFloatingText_m634EC4DD456EB9596D9ACF27710D5F28095EEDA1,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_t2C450C526125FE0890D8581F5A5E60EA4BEA4177_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m328AB7CF0E2A3A56BC77FC55140903098CF87CFE,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_t2C450C526125FE0890D8581F5A5E60EA4BEA4177_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m052F727B1032933B1DD2CABD4B44CFBA23662047,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_t2C450C526125FE0890D8581F5A5E60EA4BEA4177_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7C348F98BC1901EB30404695B7548067947121D8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_t2C450C526125FE0890D8581F5A5E60EA4BEA4177_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_m62DBBFD80A8879E56C9BAB8B279CADA3B0D5E95F,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_t2C450C526125FE0890D8581F5A5E60EA4BEA4177_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mC6FA041905BD4EEE785DFA63B708CD601C8C0C2A,
	U3CDisplayTextMeshFloatingTextU3Ed__16_t45DFD3865E7CF6058313A502EEE4823924DF0ADF_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m27613E638D919598A9C8C2DDCA2C157E69A7D399,
	U3CDisplayTextMeshFloatingTextU3Ed__16_t45DFD3865E7CF6058313A502EEE4823924DF0ADF_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_mBB9A19DD082FE5F45831A42F6ABFF7310B783653,
	U3CDisplayTextMeshFloatingTextU3Ed__16_t45DFD3865E7CF6058313A502EEE4823924DF0ADF_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m033D07D6C2A22DE4239E5B7F15C7535C1AE709F9,
	U3CDisplayTextMeshFloatingTextU3Ed__16_t45DFD3865E7CF6058313A502EEE4823924DF0ADF_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mB883AE948B2D5B213E28C2C08FC4FAC846090596,
	U3CDisplayTextMeshFloatingTextU3Ed__16_t45DFD3865E7CF6058313A502EEE4823924DF0ADF_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_mDB3B40C77FDD1B4D3FCB62E34C424BCB9F61715D,
	VertexColorCycler_tE1208E27D8B0ED521EA4AB48577E80F47CC8391E_CustomAttributesCacheGenerator_VertexColorCycler_AnimateVertexColors_m4EE5EFAAA9D9F65C811848AE36551E8137B3FDF1,
	U3CAnimateVertexColorsU3Ed__3_t9EE128818D22D10FB704C73831F62C8D89E37DE6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3__ctor_m69255FD61F9CB7F7AFD1468325DB6EDA90C3DD83,
	U3CAnimateVertexColorsU3Ed__3_t9EE128818D22D10FB704C73831F62C8D89E37DE6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m9E8F17AF5EB46169BCFBA2152681B7A4A8B870D9,
	U3CAnimateVertexColorsU3Ed__3_t9EE128818D22D10FB704C73831F62C8D89E37DE6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF8FBC07125C99541937A71DE72A3348131A4FFD9,
	U3CAnimateVertexColorsU3Ed__3_t9EE128818D22D10FB704C73831F62C8D89E37DE6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m176E827425D6A6561CD6A941517F502A1AFD610E,
	U3CAnimateVertexColorsU3Ed__3_t9EE128818D22D10FB704C73831F62C8D89E37DE6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m25C8138AE53031CDDF90D50063B1286ED44F4266,
	VertexJitter_t3EE0DD43F99BDEC17600BA011E1C15A90655ABBB_CustomAttributesCacheGenerator_VertexJitter_AnimateVertexColors_m76363C66669DF0DD2070901DEFFF39D24C242B90,
	U3CAnimateVertexColorsU3Ed__11_tAC0216BA39C3BA66767A320D95DA1C392389AADE_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11__ctor_m896A9095AFF2B1F7D823B4D14CFC2527EB3152CC,
	U3CAnimateVertexColorsU3Ed__11_tAC0216BA39C3BA66767A320D95DA1C392389AADE_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_mD72B273D37EA2543C5C0B88211549EC0FA3E720D,
	U3CAnimateVertexColorsU3Ed__11_tAC0216BA39C3BA66767A320D95DA1C392389AADE_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m36CA4792794DA1C9908A9C9FC4874ACF82804AB8,
	U3CAnimateVertexColorsU3Ed__11_tAC0216BA39C3BA66767A320D95DA1C392389AADE_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m627446844CA02880583B92428889EA9D9CEC04CC,
	U3CAnimateVertexColorsU3Ed__11_tAC0216BA39C3BA66767A320D95DA1C392389AADE_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m7669B30A4AC65F85F8EBA4BDC640F43B74C478EC,
	VertexShakeA_t7B2BEE781CFAD3E36A50BF75015504A018C5F2E0_CustomAttributesCacheGenerator_VertexShakeA_AnimateVertexColors_mAEF171DDF17D8B42259C3C2F6486E550DE014BF1,
	U3CAnimateVertexColorsU3Ed__11_tCF4677CE6128B93FB1DA8390DAA5B7BE85EB9B79_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11__ctor_mCAC1B99791AE7DD5F3F96BC4CB7645898301C2CE,
	U3CAnimateVertexColorsU3Ed__11_tCF4677CE6128B93FB1DA8390DAA5B7BE85EB9B79_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m4EEC65E3452BBDCB67C9ABC7409C73FEACF9BC2F,
	U3CAnimateVertexColorsU3Ed__11_tCF4677CE6128B93FB1DA8390DAA5B7BE85EB9B79_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBC451FD1BCDDB66EAEAA07F399199A9DC63AFB23,
	U3CAnimateVertexColorsU3Ed__11_tCF4677CE6128B93FB1DA8390DAA5B7BE85EB9B79_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m74DE64545E3327D66FB6C069A5190BFC0FF683AC,
	U3CAnimateVertexColorsU3Ed__11_tCF4677CE6128B93FB1DA8390DAA5B7BE85EB9B79_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_mD3DFEB94E1B0A0153083715008C670364D32C097,
	VertexShakeB_t67B14E5277C675D533503A2D80A1633115299391_CustomAttributesCacheGenerator_VertexShakeB_AnimateVertexColors_m4C64F1552C5539AE431814B8DE4673B764D26576,
	U3CAnimateVertexColorsU3Ed__10_t096128B3F18D445CA2BC7915A490343F5CB40293_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10__ctor_mD5440310F221693B051892C32E5825ACF2910B0C,
	U3CAnimateVertexColorsU3Ed__10_t096128B3F18D445CA2BC7915A490343F5CB40293_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m85345564110FD83A156AE3D768E78E41E8D464C0,
	U3CAnimateVertexColorsU3Ed__10_t096128B3F18D445CA2BC7915A490343F5CB40293_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C64AB0762FDA81B311143C78B0B7314A05B40F7,
	U3CAnimateVertexColorsU3Ed__10_t096128B3F18D445CA2BC7915A490343F5CB40293_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mD0C3021D6DEB76A541E9A6F93CF17691D62B417C,
	U3CAnimateVertexColorsU3Ed__10_t096128B3F18D445CA2BC7915A490343F5CB40293_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mAD8B239C2AFA0DAE135EED3CFB49F0816D150048,
	VertexZoom_t8A88A990BFB605774C93B0940F6A258FCD5710D2_CustomAttributesCacheGenerator_VertexZoom_AnimateVertexColors_m7373CF7A5AC35CDBC74CE80BD56B04123A91C0C7,
	U3CAnimateVertexColorsU3Ed__10_t80A253D2D22F54FF4777892E88E3C49CF40C0283_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10__ctor_mA2C6461F1E88C3131B5C4E6FFD7C75771C73D09A,
	U3CAnimateVertexColorsU3Ed__10_t80A253D2D22F54FF4777892E88E3C49CF40C0283_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mE5843C784E5D25537D577419267FAA093836BBA6,
	U3CAnimateVertexColorsU3Ed__10_t80A253D2D22F54FF4777892E88E3C49CF40C0283_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m02F8B326B53D4BB33A54254ED664BA615DAC4E24,
	U3CAnimateVertexColorsU3Ed__10_t80A253D2D22F54FF4777892E88E3C49CF40C0283_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m06892291F75733C0A0525F44054E4CEA8D9BD58C,
	U3CAnimateVertexColorsU3Ed__10_t80A253D2D22F54FF4777892E88E3C49CF40C0283_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m2F4836BAA267C17023363704B71B53454934A6E0,
	WarpTextExample_tC5297C8412EF87B266159FECD34E06BFB474CC74_CustomAttributesCacheGenerator_WarpTextExample_WarpText_m453D126F0994CE92A6A3D2F620B81BADD85F7DBF,
	U3CWarpTextU3Ed__8_t5E5B765727F5E411B819B7A5855C31D921A4CD4F_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8__ctor_m76DC34194319F4AF8CFB96066F3764402BEB5279,
	U3CWarpTextU3Ed__8_t5E5B765727F5E411B819B7A5855C31D921A4CD4F_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_IDisposable_Dispose_mE963A8C458C7424814637CF46BBDDEC46CAD7F9C,
	U3CWarpTextU3Ed__8_t5E5B765727F5E411B819B7A5855C31D921A4CD4F_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m32CF5062AEB89B0ECAE1021AF873CD799923FDFF,
	U3CWarpTextU3Ed__8_t5E5B765727F5E411B819B7A5855C31D921A4CD4F_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m46782447641AAFD07BA0D6D363B5FA2FC2282837,
	U3CWarpTextU3Ed__8_t5E5B765727F5E411B819B7A5855C31D921A4CD4F_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_mDB18401834B9740BFAECF2C624EF0C8ACED4E9D2,
	BaseDwellSample_t1D103FDA990DC40C609B6A4AA034D14B17F825E2_CustomAttributesCacheGenerator_BaseDwellSample_get_DwellHandler_mC114D08153104E01605702BD59AF73888F1E6CE9,
	BaseDwellSample_t1D103FDA990DC40C609B6A4AA034D14B17F825E2_CustomAttributesCacheGenerator_BaseDwellSample_set_DwellHandler_m73E008459D501A72BB8C968B6BAF6575D5265348,
	BaseDwellSample_t1D103FDA990DC40C609B6A4AA034D14B17F825E2_CustomAttributesCacheGenerator_BaseDwellSample_get_IsDwelling_m9E0E0CD2600EB5782A736E1C8EAC08773126F873,
	BaseDwellSample_t1D103FDA990DC40C609B6A4AA034D14B17F825E2_CustomAttributesCacheGenerator_BaseDwellSample_set_IsDwelling_m18391541F4D24E4F07E99244BE58C23997F65E4A,
	WidgetElasticDemo_tE623A71620B902B821CD05D322B2CFC29207C900_CustomAttributesCacheGenerator_WidgetElasticDemo_DeflateCoroutine_mAFCF1FE1810464E142CA6140D747438EE7CE4BC7,
	WidgetElasticDemo_tE623A71620B902B821CD05D322B2CFC29207C900_CustomAttributesCacheGenerator_WidgetElasticDemo_InflateCoroutine_m19FE65F593F306036730601D94D30F2307A02EAD,
	U3CDeflateCoroutineU3Ed__17_t51DB69BD5458E3680AFD5FEB84AFF52D3EAEBEDA_CustomAttributesCacheGenerator_U3CDeflateCoroutineU3Ed__17__ctor_mCE59BFC0F867BA58A8EBFA0509AE6353BCAFBF40,
	U3CDeflateCoroutineU3Ed__17_t51DB69BD5458E3680AFD5FEB84AFF52D3EAEBEDA_CustomAttributesCacheGenerator_U3CDeflateCoroutineU3Ed__17_System_IDisposable_Dispose_mFF3E272F263C9873B5972D43624F29AE8917EF6E,
	U3CDeflateCoroutineU3Ed__17_t51DB69BD5458E3680AFD5FEB84AFF52D3EAEBEDA_CustomAttributesCacheGenerator_U3CDeflateCoroutineU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD3D31C0D3D48BF83F1024764C3BFBEEE1504B054,
	U3CDeflateCoroutineU3Ed__17_t51DB69BD5458E3680AFD5FEB84AFF52D3EAEBEDA_CustomAttributesCacheGenerator_U3CDeflateCoroutineU3Ed__17_System_Collections_IEnumerator_Reset_m6432C3B69E4C3EC48FD04166F910F93750C361C9,
	U3CDeflateCoroutineU3Ed__17_t51DB69BD5458E3680AFD5FEB84AFF52D3EAEBEDA_CustomAttributesCacheGenerator_U3CDeflateCoroutineU3Ed__17_System_Collections_IEnumerator_get_Current_m85DEF7CC0E93830CB2218DB8E323FFDCA2B572A9,
	U3CInflateCoroutineU3Ed__18_tDEF4EE704B4C848D5E10E502151CC371CBFF0164_CustomAttributesCacheGenerator_U3CInflateCoroutineU3Ed__18__ctor_m004C3F0C3E72B5B25663F8B4AC86AB8570EBDE07,
	U3CInflateCoroutineU3Ed__18_tDEF4EE704B4C848D5E10E502151CC371CBFF0164_CustomAttributesCacheGenerator_U3CInflateCoroutineU3Ed__18_System_IDisposable_Dispose_m175FA6717CE8675FEC96F60413B3A08ED6C9B056,
	U3CInflateCoroutineU3Ed__18_tDEF4EE704B4C848D5E10E502151CC371CBFF0164_CustomAttributesCacheGenerator_U3CInflateCoroutineU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m440D830708196E0CE60BEC34E2B1F28D3CD40A2E,
	U3CInflateCoroutineU3Ed__18_tDEF4EE704B4C848D5E10E502151CC371CBFF0164_CustomAttributesCacheGenerator_U3CInflateCoroutineU3Ed__18_System_Collections_IEnumerator_Reset_mB20C406EC02C71E87A765F0EC617CE421FA3FB5E,
	U3CInflateCoroutineU3Ed__18_tDEF4EE704B4C848D5E10E502151CC371CBFF0164_CustomAttributesCacheGenerator_U3CInflateCoroutineU3Ed__18_System_Collections_IEnumerator_get_Current_m5C619A4BDCB54466183141A0CD0E43E4BD23A4C7,
	SystemKeyboardExample_t54325C6FC824F2691F1EDD471FBEBA326B3BE4F7_CustomAttributesCacheGenerator_SystemKeyboardExample_U3CStartU3Eb__5_0_m541CA735B73D753F9564EC8F92050CD1194DEE63,
	SystemKeyboardExample_t54325C6FC824F2691F1EDD471FBEBA326B3BE4F7_CustomAttributesCacheGenerator_SystemKeyboardExample_U3CStartU3Eb__5_1_mD9B2785A96F31F655646DC94D36E0F22330A5CFD,
	ScrollableListPopulator_t11F4DD96C0681CF111BA9B35F6A84DB7DDF15E32_CustomAttributesCacheGenerator_ScrollableListPopulator_UpdateListOverTime_m9C3F25617584DDF0E49A4E5FE3F0434FC554F228,
	U3CUpdateListOverTimeU3Ed__33_t6F615CB841917C7A9D914665B70E25E5A2B78D61_CustomAttributesCacheGenerator_U3CUpdateListOverTimeU3Ed__33__ctor_m6EEEA2C4C3863DE786A4FB33D9FCD7DCBB83AC7A,
	U3CUpdateListOverTimeU3Ed__33_t6F615CB841917C7A9D914665B70E25E5A2B78D61_CustomAttributesCacheGenerator_U3CUpdateListOverTimeU3Ed__33_System_IDisposable_Dispose_mC45591ACB830F32EADCEB7990E91CE8347678FE7,
	U3CUpdateListOverTimeU3Ed__33_t6F615CB841917C7A9D914665B70E25E5A2B78D61_CustomAttributesCacheGenerator_U3CUpdateListOverTimeU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE4E49E62B8DCC9D5A741F6E62D8B43615027F87C,
	U3CUpdateListOverTimeU3Ed__33_t6F615CB841917C7A9D914665B70E25E5A2B78D61_CustomAttributesCacheGenerator_U3CUpdateListOverTimeU3Ed__33_System_Collections_IEnumerator_Reset_m9839718D6F2442EB8A25E4942DA9C744A69674F0,
	U3CUpdateListOverTimeU3Ed__33_t6F615CB841917C7A9D914665B70E25E5A2B78D61_CustomAttributesCacheGenerator_U3CUpdateListOverTimeU3Ed__33_System_Collections_IEnumerator_get_Current_m9CA3175850108F9ED839FB1B592FF078FAD74C6B,
	HideTapToPlaceLabel_t0E6D5DD452C4AA4C3BD5D87804F3DEF779665DA9_CustomAttributesCacheGenerator_HideTapToPlaceLabel_U3CAddTapToPlaceListenersU3Eb__3_0_m336168FCCF071E355A2405EF347038D93A48E42D,
	HideTapToPlaceLabel_t0E6D5DD452C4AA4C3BD5D87804F3DEF779665DA9_CustomAttributesCacheGenerator_HideTapToPlaceLabel_U3CAddTapToPlaceListenersU3Eb__3_1_m1AA8691520D7DF4DE9A4D8AD73CC0CC243564E0B,
	BoundingBoxExampleTest_t920EC66E68751C735AB5FB7F83F532C46DA2BEDB_CustomAttributesCacheGenerator_BoundingBoxExampleTest_Sequence_m0A42C1BD0F1E8F21CD3D8C3008AEB03AE3AE2240,
	BoundingBoxExampleTest_t920EC66E68751C735AB5FB7F83F532C46DA2BEDB_CustomAttributesCacheGenerator_BoundingBoxExampleTest_WaitForSpeechCommand_mBC53747078DBA387C49DB0E4848A9329E34CDC0D,
	BoundingBoxExampleTest_t920EC66E68751C735AB5FB7F83F532C46DA2BEDB_CustomAttributesCacheGenerator_BoundingBoxExampleTest_U3CSequenceU3Eb__12_0_m4F95AD13CBBA6BB3ED84AB44126AD26BC933DB67,
	BoundingBoxExampleTest_t920EC66E68751C735AB5FB7F83F532C46DA2BEDB_CustomAttributesCacheGenerator_BoundingBoxExampleTest_U3CSequenceU3Eb__12_1_mD27CA529D7D5FD14AA5EAE422601F8CC5E583AD2,
	U3CSequenceU3Ed__12_t056AE53EBAF2B9CF286C48D22F0C486DD67863C0_CustomAttributesCacheGenerator_U3CSequenceU3Ed__12__ctor_m34250F7FFD94CF51D0BA8F66FAA696A74EBED421,
	U3CSequenceU3Ed__12_t056AE53EBAF2B9CF286C48D22F0C486DD67863C0_CustomAttributesCacheGenerator_U3CSequenceU3Ed__12_System_IDisposable_Dispose_m1EA4A1FA54F48C6410C00ECF692E454555F19E2A,
	U3CSequenceU3Ed__12_t056AE53EBAF2B9CF286C48D22F0C486DD67863C0_CustomAttributesCacheGenerator_U3CSequenceU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m966AE4CFB2CB37D234767C012EC7095857E00B82,
	U3CSequenceU3Ed__12_t056AE53EBAF2B9CF286C48D22F0C486DD67863C0_CustomAttributesCacheGenerator_U3CSequenceU3Ed__12_System_Collections_IEnumerator_Reset_m847F3DF0C15BDD6E2D270512B111204A7A1028CC,
	U3CSequenceU3Ed__12_t056AE53EBAF2B9CF286C48D22F0C486DD67863C0_CustomAttributesCacheGenerator_U3CSequenceU3Ed__12_System_Collections_IEnumerator_get_Current_mC6C5FB9C66089B57910B291CC5B9B396A2B6D0B5,
	U3CWaitForSpeechCommandU3Ed__14_t02FCE23E8825096EA938587649A5E60F44836910_CustomAttributesCacheGenerator_U3CWaitForSpeechCommandU3Ed__14__ctor_mB5943136E45C53386CF88C696988515A51E4A9A4,
	U3CWaitForSpeechCommandU3Ed__14_t02FCE23E8825096EA938587649A5E60F44836910_CustomAttributesCacheGenerator_U3CWaitForSpeechCommandU3Ed__14_System_IDisposable_Dispose_m241B40EFB3E896C86BD1572454BFDB2D695FEE84,
	U3CWaitForSpeechCommandU3Ed__14_t02FCE23E8825096EA938587649A5E60F44836910_CustomAttributesCacheGenerator_U3CWaitForSpeechCommandU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m10D6B994F0EF6EDF684CC5C0A0AC7B33EA6316EB,
	U3CWaitForSpeechCommandU3Ed__14_t02FCE23E8825096EA938587649A5E60F44836910_CustomAttributesCacheGenerator_U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_Reset_m3292A30EC9B1AEAAB25EEF38748317038D3881FC,
	U3CWaitForSpeechCommandU3Ed__14_t02FCE23E8825096EA938587649A5E60F44836910_CustomAttributesCacheGenerator_U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_get_Current_m2DBE575A1FE49BAEB66D584B50F2C0A91772C4B2,
	BoundsControlRuntimeExample_t2DF9FD9A869E0569B21F7E491EE942CAAE6C6E3B_CustomAttributesCacheGenerator_BoundsControlRuntimeExample_Sequence_mADF6858AFE4D2FAB573185743D611BD3C7C26CCF,
	BoundsControlRuntimeExample_t2DF9FD9A869E0569B21F7E491EE942CAAE6C6E3B_CustomAttributesCacheGenerator_BoundsControlRuntimeExample_WaitForSpeechCommand_mBB8AF5BD87EFE0B9311F1327DB13626525442F3C,
	BoundsControlRuntimeExample_t2DF9FD9A869E0569B21F7E491EE942CAAE6C6E3B_CustomAttributesCacheGenerator_BoundsControlRuntimeExample_U3CSequenceU3Eb__12_0_m36BA64048BC4CFEE4DA52E5D2146DA1F21C6B826,
	BoundsControlRuntimeExample_t2DF9FD9A869E0569B21F7E491EE942CAAE6C6E3B_CustomAttributesCacheGenerator_BoundsControlRuntimeExample_U3CSequenceU3Eb__12_1_mC4E12695D5E3AF83B72379DE584C30395A510730,
	U3CSequenceU3Ed__12_t1CC8D60F8F8D94D9A497136AC0611C33C7913A73_CustomAttributesCacheGenerator_U3CSequenceU3Ed__12__ctor_mB596D0726FB14CE0546E56694FB5B2CACB0CF45C,
	U3CSequenceU3Ed__12_t1CC8D60F8F8D94D9A497136AC0611C33C7913A73_CustomAttributesCacheGenerator_U3CSequenceU3Ed__12_System_IDisposable_Dispose_mF11A785D0A2418CAA49D61132D659F18559AF866,
	U3CSequenceU3Ed__12_t1CC8D60F8F8D94D9A497136AC0611C33C7913A73_CustomAttributesCacheGenerator_U3CSequenceU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5F0E02B2B5C6D0F72716E6F36D0C6639ECD72379,
	U3CSequenceU3Ed__12_t1CC8D60F8F8D94D9A497136AC0611C33C7913A73_CustomAttributesCacheGenerator_U3CSequenceU3Ed__12_System_Collections_IEnumerator_Reset_m43D114449892836ED049B28D22F2E047E47C3D6B,
	U3CSequenceU3Ed__12_t1CC8D60F8F8D94D9A497136AC0611C33C7913A73_CustomAttributesCacheGenerator_U3CSequenceU3Ed__12_System_Collections_IEnumerator_get_Current_mE027BFD6EE3F6E9724BAC26E0B59C0F8C2C35A7E,
	U3CWaitForSpeechCommandU3Ed__14_tF24D07507705A5DC8FE5B96BC8EF0EF1223BA8B9_CustomAttributesCacheGenerator_U3CWaitForSpeechCommandU3Ed__14__ctor_mC11BB974A74E2A51DC40095516466073D190FF36,
	U3CWaitForSpeechCommandU3Ed__14_tF24D07507705A5DC8FE5B96BC8EF0EF1223BA8B9_CustomAttributesCacheGenerator_U3CWaitForSpeechCommandU3Ed__14_System_IDisposable_Dispose_mF17BD015850AF82D586F2E502A0F98DFDF1CF681,
	U3CWaitForSpeechCommandU3Ed__14_tF24D07507705A5DC8FE5B96BC8EF0EF1223BA8B9_CustomAttributesCacheGenerator_U3CWaitForSpeechCommandU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C339347BA3254B9C3D4ED05B7EC66545A274319,
	U3CWaitForSpeechCommandU3Ed__14_tF24D07507705A5DC8FE5B96BC8EF0EF1223BA8B9_CustomAttributesCacheGenerator_U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_Reset_m546A0C9B8314A5766CE101397F9E7DC39CB10DE6,
	U3CWaitForSpeechCommandU3Ed__14_tF24D07507705A5DC8FE5B96BC8EF0EF1223BA8B9_CustomAttributesCacheGenerator_U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_get_Current_m6DE668CFDD0D0BCCFAE54D3D6F21AFAE511AFB38,
	GridObjectLayoutControl_tC085893CB2C961AC9CBF6B6DC8F379776FA97A35_CustomAttributesCacheGenerator_GridObjectLayoutControl_TestAnchors_mE3AA69D08ED0CF4432BFBE26768657FA5755555E,
	U3CTestAnchorsU3Ed__7_t60EB0100F609816FA418017838B859C42D501123_CustomAttributesCacheGenerator_U3CTestAnchorsU3Ed__7__ctor_m431EC69B94B62747171BF91FA9EBA93C74F1AD56,
	U3CTestAnchorsU3Ed__7_t60EB0100F609816FA418017838B859C42D501123_CustomAttributesCacheGenerator_U3CTestAnchorsU3Ed__7_System_IDisposable_Dispose_m8C7D9B0F8F63A73C58000A6C42F7E017F5A01BAA,
	U3CTestAnchorsU3Ed__7_t60EB0100F609816FA418017838B859C42D501123_CustomAttributesCacheGenerator_U3CTestAnchorsU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C0EB33A01381805945D1A8E56E4D2794FABEFE4,
	U3CTestAnchorsU3Ed__7_t60EB0100F609816FA418017838B859C42D501123_CustomAttributesCacheGenerator_U3CTestAnchorsU3Ed__7_System_Collections_IEnumerator_Reset_mAC0260AC48A259E59C1D6B027E9A1461C84A50AA,
	U3CTestAnchorsU3Ed__7_t60EB0100F609816FA418017838B859C42D501123_CustomAttributesCacheGenerator_U3CTestAnchorsU3Ed__7_System_Collections_IEnumerator_get_Current_m8537D1C2951BE0CF00056F85E74785D071BE37BC,
	ProgressIndicatorDemo_tD9BD7DCF1298C882786983D4268E77A3E9380474_CustomAttributesCacheGenerator_ProgressIndicatorDemo_HandleButtonClick_m83BDE61656ADAE9FA660C25015AF0ED3CE1489A2,
	ProgressIndicatorDemo_tD9BD7DCF1298C882786983D4268E77A3E9380474_CustomAttributesCacheGenerator_ProgressIndicatorDemo_OpenProgressIndicator_m21F6477AF93FE257DF3D39A22301C610B7AA2EC3,
	U3CHandleButtonClickU3Ed__14_t6DC14582F351A525C8BBBA55020C98E0EC7871E7_CustomAttributesCacheGenerator_U3CHandleButtonClickU3Ed__14_SetStateMachine_m6763E06BEC587E1ABE97242AC32B2C93DA60BB30,
	U3COpenProgressIndicatorU3Ed__17_tCA040FFC07AD3E4D55D008542D434CD6D80B4388_CustomAttributesCacheGenerator_U3COpenProgressIndicatorU3Ed__17_SetStateMachine_m8402426A9BB1212956FF5CDA08A962A3FA14D274,
	PanZoomBase_t202BE6839273DFACBD9236A4C11306A4E4DF09B0_CustomAttributesCacheGenerator_PanZoomBase_ZoomAndStop_m70C71BF56D1145AB3CDF2E00A8EB01D238FBCECD,
	U3CZoomAndStopU3Ed__78_t5DD82D6ECCE58A5515F5D95AABB14F522D219E46_CustomAttributesCacheGenerator_U3CZoomAndStopU3Ed__78__ctor_m46258496853C97A3C755A49B27F1D14CBCDA8AC4,
	U3CZoomAndStopU3Ed__78_t5DD82D6ECCE58A5515F5D95AABB14F522D219E46_CustomAttributesCacheGenerator_U3CZoomAndStopU3Ed__78_System_IDisposable_Dispose_m3125E7B1AF6696673562538B404794532B5C6581,
	U3CZoomAndStopU3Ed__78_t5DD82D6ECCE58A5515F5D95AABB14F522D219E46_CustomAttributesCacheGenerator_U3CZoomAndStopU3Ed__78_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5007A408115112E9A302CE1D2D2D6C8912A0B878,
	U3CZoomAndStopU3Ed__78_t5DD82D6ECCE58A5515F5D95AABB14F522D219E46_CustomAttributesCacheGenerator_U3CZoomAndStopU3Ed__78_System_Collections_IEnumerator_Reset_m377FFA4C86BEBD8E6385A94CAFD88AC4F7389826,
	U3CZoomAndStopU3Ed__78_t5DD82D6ECCE58A5515F5D95AABB14F522D219E46_CustomAttributesCacheGenerator_U3CZoomAndStopU3Ed__78_System_Collections_IEnumerator_get_Current_m40F497BEDB0DA316498CD3F4F0964E601517371A,
	TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_TargetGroupIterator_add_OnAllTargetsSelected_m6064AFDC7FD0A4F707C0C0B66CD34F2AA7BF872F,
	TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_TargetGroupIterator_remove_OnAllTargetsSelected_m5AF09D842ECCE703EF50F6A8731AB37D8533AE62,
	TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_TargetGroupIterator_add_OnTargetSelected_mBBDB4D90C8A878BAF5BFCF91388D545305DA2BD9,
	TargetGroupIterator_t37237C2A8F9B113B48BA2FC154BB6F21C5069A7A_CustomAttributesCacheGenerator_TargetGroupIterator_remove_OnTargetSelected_m18F381E83CBF9A077ABBAE7A92638A6AB32DED47,
	DrawOnTexture_t99BDA5310DF2AC6F885B874496A2537ED869005C_CustomAttributesCacheGenerator_DrawOnTexture_DrawAt_m5CCF53A676F5EF6B8E413C510A70B1ABD8EB42DD,
	DrawOnTexture_t99BDA5310DF2AC6F885B874496A2537ED869005C_CustomAttributesCacheGenerator_DrawOnTexture_ComputeHeatmapAt_m4234A80A7ECCD004A0549561897E1FDCBF25898E,
	U3CDrawAtU3Ed__19_tF9694B76A7C548907397EF887A0AF5273444C7A1_CustomAttributesCacheGenerator_U3CDrawAtU3Ed__19__ctor_m0669E9BEA6952B3F3DD94D06CBB8ABE617E39B24,
	U3CDrawAtU3Ed__19_tF9694B76A7C548907397EF887A0AF5273444C7A1_CustomAttributesCacheGenerator_U3CDrawAtU3Ed__19_System_IDisposable_Dispose_m6E4D0EBCBAE804E9BCE39A65E328D04E34213B52,
	U3CDrawAtU3Ed__19_tF9694B76A7C548907397EF887A0AF5273444C7A1_CustomAttributesCacheGenerator_U3CDrawAtU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB8FE5A3C65743AF09977A975BCC7D237891A637,
	U3CDrawAtU3Ed__19_tF9694B76A7C548907397EF887A0AF5273444C7A1_CustomAttributesCacheGenerator_U3CDrawAtU3Ed__19_System_Collections_IEnumerator_Reset_m928AB931B01F942ECA5D6B39A97E2DDB4B316A66,
	U3CDrawAtU3Ed__19_tF9694B76A7C548907397EF887A0AF5273444C7A1_CustomAttributesCacheGenerator_U3CDrawAtU3Ed__19_System_Collections_IEnumerator_get_Current_mCF5A234BE92720D5D3143BAA4F6FA6A2E7C5C7C2,
	U3CComputeHeatmapAtU3Ed__20_t57778DEFEFEA9FE64A0801F0D2B0B59F4398058F_CustomAttributesCacheGenerator_U3CComputeHeatmapAtU3Ed__20__ctor_mB446BCBDF6A86972E2CCA7C4D41D2BC187574100,
	U3CComputeHeatmapAtU3Ed__20_t57778DEFEFEA9FE64A0801F0D2B0B59F4398058F_CustomAttributesCacheGenerator_U3CComputeHeatmapAtU3Ed__20_System_IDisposable_Dispose_m037E1BB2B14C2C41D8DC547D74349C2DB0BE76FA,
	U3CComputeHeatmapAtU3Ed__20_t57778DEFEFEA9FE64A0801F0D2B0B59F4398058F_CustomAttributesCacheGenerator_U3CComputeHeatmapAtU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01119328E1F744FF691CE5485CC2BADFC8766CC2,
	U3CComputeHeatmapAtU3Ed__20_t57778DEFEFEA9FE64A0801F0D2B0B59F4398058F_CustomAttributesCacheGenerator_U3CComputeHeatmapAtU3Ed__20_System_Collections_IEnumerator_Reset_mAF888F7D9CB6C49243C9B4D75E73BBB14A77D906,
	U3CComputeHeatmapAtU3Ed__20_t57778DEFEFEA9FE64A0801F0D2B0B59F4398058F_CustomAttributesCacheGenerator_U3CComputeHeatmapAtU3Ed__20_System_Collections_IEnumerator_get_Current_mA0A6E63023754E4F80F25FB667CE8E119770D05A,
	AudioFeedbackPlayer_t5B3ACA94A40A08A9873B04A9F76A8B50AAA362A7_CustomAttributesCacheGenerator_AudioFeedbackPlayer_get_Instance_m714F2501DD250566ABEF9269938D4F9FEFDBF577,
	AudioFeedbackPlayer_t5B3ACA94A40A08A9873B04A9F76A8B50AAA362A7_CustomAttributesCacheGenerator_AudioFeedbackPlayer_set_Instance_m3C0D461B9B45905B85975D30EFB5DCAACACAC802,
	LoadAdditiveScene_tBAB7527A2C0FC63B97C9B9A863BC813612A4691C_CustomAttributesCacheGenerator_LoadAdditiveScene_LoadNewScene_m8369E583996CD92D5D211F38E219CDAA014E43FE,
	U3CLoadNewSceneU3Ed__6_t0E26EB5F61490AF9926F108D5E2AF8BFD3A41D9B_CustomAttributesCacheGenerator_U3CLoadNewSceneU3Ed__6__ctor_mAAA7D7B2D2A941AB339DDE43B676DA5EB7C3F25A,
	U3CLoadNewSceneU3Ed__6_t0E26EB5F61490AF9926F108D5E2AF8BFD3A41D9B_CustomAttributesCacheGenerator_U3CLoadNewSceneU3Ed__6_System_IDisposable_Dispose_mAC49FC02DB74158443E728F2B937460842721A3B,
	U3CLoadNewSceneU3Ed__6_t0E26EB5F61490AF9926F108D5E2AF8BFD3A41D9B_CustomAttributesCacheGenerator_U3CLoadNewSceneU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9CF41C95C56DA29A06DC641A6B10B958B14D43D3,
	U3CLoadNewSceneU3Ed__6_t0E26EB5F61490AF9926F108D5E2AF8BFD3A41D9B_CustomAttributesCacheGenerator_U3CLoadNewSceneU3Ed__6_System_Collections_IEnumerator_Reset_m1F4F5F43E8C334FD2178951548219825F2978BA6,
	U3CLoadNewSceneU3Ed__6_t0E26EB5F61490AF9926F108D5E2AF8BFD3A41D9B_CustomAttributesCacheGenerator_U3CLoadNewSceneU3Ed__6_System_Collections_IEnumerator_get_Current_m7D090F16DFB2D3B4CFB78A558428242744FEF519,
	TargetEventArgs_t7713E8CD48F3A9DC5C9036F0CDD4C44DDA493350_CustomAttributesCacheGenerator_TargetEventArgs_get_HitTarget_m8388656A38F227CCD16F8BCF83A22047ECFDA41B,
	TargetEventArgs_t7713E8CD48F3A9DC5C9036F0CDD4C44DDA493350_CustomAttributesCacheGenerator_TargetEventArgs_set_HitTarget_mC1AE547E25147BE85A735C663443ABEFDE11B4F0,
	KeepThisAlive_t6373FC2BC670FA2D10DF2EA75A5D40325044C99B_CustomAttributesCacheGenerator_KeepThisAlive_get_Instance_m6E3E6968684933E52E1A47F41083AC48FF464465,
	KeepThisAlive_t6373FC2BC670FA2D10DF2EA75A5D40325044C99B_CustomAttributesCacheGenerator_KeepThisAlive_set_Instance_mA2A10EE2DD42672547E38F84945744FB35AF8640,
	EyeTrackingDemoUtils_tEADE14372E8745738081ECE2AF930C7AB369754B_CustomAttributesCacheGenerator_EyeTrackingDemoUtils_LoadNewScene_mDDC18CE3E1DB98A62D89FB6C89E485D1B68BF4F2,
	U3CLoadNewSceneU3Ed__8_t6CC37138968FD381723B0793506AC695ED553EF9_CustomAttributesCacheGenerator_U3CLoadNewSceneU3Ed__8__ctor_m401003DDD285C5623BEB280831A31A466D16A964,
	U3CLoadNewSceneU3Ed__8_t6CC37138968FD381723B0793506AC695ED553EF9_CustomAttributesCacheGenerator_U3CLoadNewSceneU3Ed__8_System_IDisposable_Dispose_mD06F424BF364A4726F11674C65C0CA9EEEAFE2CC,
	U3CLoadNewSceneU3Ed__8_t6CC37138968FD381723B0793506AC695ED553EF9_CustomAttributesCacheGenerator_U3CLoadNewSceneU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6D49FF64BD8C8DE4A6EA2EC600BD12FF3390B71E,
	U3CLoadNewSceneU3Ed__8_t6CC37138968FD381723B0793506AC695ED553EF9_CustomAttributesCacheGenerator_U3CLoadNewSceneU3Ed__8_System_Collections_IEnumerator_Reset_mC163ABF19EDAEDE1B729059AFF32EC00D9760CED,
	U3CLoadNewSceneU3Ed__8_t6CC37138968FD381723B0793506AC695ED553EF9_CustomAttributesCacheGenerator_U3CLoadNewSceneU3Ed__8_System_Collections_IEnumerator_get_Current_m4E0C826DFFCFD5E5854D74C1DDB3B2F680731F1C,
	ExclusiveSynchronizationContext_tF7C7F5ABCA05A45DB107864767F66F827CC903B8_CustomAttributesCacheGenerator_ExclusiveSynchronizationContext_get_InnerException_m4409BF574355A708E7AD7B501A23394DC8194BDB,
	ExclusiveSynchronizationContext_tF7C7F5ABCA05A45DB107864767F66F827CC903B8_CustomAttributesCacheGenerator_ExclusiveSynchronizationContext_set_InnerException_m8E8500331FAEEC67FA3687E1AB92B8B6AFCCE153,
	ExclusiveSynchronizationContext_tF7C7F5ABCA05A45DB107864767F66F827CC903B8_CustomAttributesCacheGenerator_ExclusiveSynchronizationContext_U3CEndMessageLoopU3Eb__9_0_m2269F5C62E97D5684D03C180C278A730562819C4,
	U3CU3Ec__DisplayClass0_0_t2B7F40848D5A9FFE0BF064D1B1E9AA3221E5971C_CustomAttributesCacheGenerator_U3CU3Ec__DisplayClass0_0_U3CRunSyncU3Eb__0_mE0B82500FB231395120B4EA8482680957CFEE75A,
	U3CU3CRunSyncU3Eb__0U3Ed_tA0DB1D14F5469B811AB9737E394CE355699216C8_CustomAttributesCacheGenerator_U3CU3CRunSyncU3Eb__0U3Ed_SetStateMachine_m9CD2295E60C2CDDFF44C6A2CA4ACF079D62B4BE0,
	U3CU3Ec__DisplayClass1_0_1_t25BE1789EC1C3AC5447D39D361E55C23E82224EE_CustomAttributesCacheGenerator_U3CU3Ec__DisplayClass1_0_1_U3CRunSyncU3Eb__0_mBC5BADE1F00FF150D1011C6AB8CFD0C8B7701110,
	U3CU3CRunSyncU3Eb__0U3Ed_t274F9BB76D6EF41A733C34C50B5394D62F080C76_CustomAttributesCacheGenerator_U3CU3CRunSyncU3Eb__0U3Ed_SetStateMachine_m7AF5C7352D1CB51F349862D24C352799EC366C81,
	BasicInputLogger_tF39D6623A3849E400F84E3E8ADB2CBF4FC2F9D4F_CustomAttributesCacheGenerator_BasicInputLogger_CreateNewLogFile_mA8AB2F74DDF248A632C2C07FED6A9F121C5F24AC,
	BasicInputLogger_tF39D6623A3849E400F84E3E8ADB2CBF4FC2F9D4F_CustomAttributesCacheGenerator_BasicInputLogger_LoadLogs_m7F0D6AA20D8D240CD2A26D43CB0604DD4A59B853,
	BasicInputLogger_tF39D6623A3849E400F84E3E8ADB2CBF4FC2F9D4F_CustomAttributesCacheGenerator_BasicInputLogger_SaveLogs_m8E427165D7149A23C324257EC8E8F0C2637FF3B1,
	U3CCreateNewLogFileU3Ed__14_tE9FFFDE66A83EFB7925BBCF9E4D461E949829D7A_CustomAttributesCacheGenerator_U3CCreateNewLogFileU3Ed__14_SetStateMachine_m6F238B4288F208E800147D5C1CEABA0527113273,
	U3CLoadLogsU3Ed__21_t71C6912789A78D48E47827228CE2FB8377D5655F_CustomAttributesCacheGenerator_U3CLoadLogsU3Ed__21_SetStateMachine_m2E9C43D81C1E46818BC390C7402EB5F366A44F4D,
	U3CSaveLogsU3Ed__22_tA5C05FED470C577465EB69A3BAA3AE8EA4ADF0EE_CustomAttributesCacheGenerator_U3CSaveLogsU3Ed__22_SetStateMachine_m61A95B0BF84898DFF17E30CC4BDF6756503A7F54,
	UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_UserInputPlayback_UWP_Load_m5788A70494218FE373050086B3654F96B15AAA46,
	UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_UserInputPlayback_UWP_LoadNewFile_mF348B60237E4F7DCEA10BC345BAEC56B9513FDAE,
	UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_UserInputPlayback_UWP_FileExists_m418F422637A601D32632A7EDD1A33DEB9FB624E0,
	UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_UserInputPlayback_UWP_ReadData_m67177F6AB330EE92B655AF4C87F16D7AC8A85E50,
	UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_UserInputPlayback_LoadInUWP_m64594642768FF5FE2AB13AA978790AEF6420CB65,
	UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_UserInputPlayback_set_IsPlaying_mCBC869EE68F90686CAD123225EEA43AD43205C64,
	UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_UserInputPlayback_get_IsPlaying_mEC33D4DE5699A80B1228F6515DEBB43DE45C7A91,
	UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_UserInputPlayback_PopulateHeatmap_m12AFD4C59AE8898EFB84E27AA69D1C526324ECB8,
	UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_UserInputPlayback_UpdateStatus_mD6B38FDE0F453849A87799311B028E23B7DB175B,
	UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_UserInputPlayback_AddToCounter_m41084542E3C5DCC962EB9929A40BF5E61922298D,
	UserInputPlayback_tC1B946BACCDA3152F4FE085814B2C65779F28F2E_CustomAttributesCacheGenerator_UserInputPlayback_U3CShowAllAndFreezeU3Eb__34_0_mDF45375F98D731AB4729A0CA2E208E5B88EE62C2,
	U3CUWP_LoadU3Ed__12_t160E61830FAF366112986A0A34F22E9EE073E64D_CustomAttributesCacheGenerator_U3CUWP_LoadU3Ed__12_SetStateMachine_m7ED88E33A30408B900C9A7A3C35A4E02BFC5683E,
	U3CUWP_LoadNewFileU3Ed__13_t77C4C264E32F46DFAE7A7B03CF7068C5D031CB71_CustomAttributesCacheGenerator_U3CUWP_LoadNewFileU3Ed__13_SetStateMachine_m22C14D0803818A1A528BDA6D6179361CE22C7233,
	U3CUWP_FileExistsU3Ed__14_t39C80B99CD2D1B2F41522AEED4CFC6EEF2436CC8_CustomAttributesCacheGenerator_U3CUWP_FileExistsU3Ed__14_SetStateMachine_m3A2B9C0F394A24619019FE3C65461944E89E98DD,
	U3CUWP_ReadDataU3Ed__15_t2AB9B79A3BFC04423D02231A2D501DDCFF3FB0D8_CustomAttributesCacheGenerator_U3CUWP_ReadDataU3Ed__15_SetStateMachine_mADF140A5B17E52067F652E349087443412A6D68C,
	U3CLoadInUWPU3Ed__21_t24C5FD3475E2D7A787B3874D4713F17B3B33C400_CustomAttributesCacheGenerator_U3CLoadInUWPU3Ed__21_SetStateMachine_m8FFD898942DB58C8CC1D604F4286C93A5FF9E428,
	U3CPopulateHeatmapU3Ed__42_tEE23C9F4ED699F6431644212A126E0B0D685DA84_CustomAttributesCacheGenerator_U3CPopulateHeatmapU3Ed__42__ctor_m102DF160804926625E30794716CD29865EC82DD9,
	U3CPopulateHeatmapU3Ed__42_tEE23C9F4ED699F6431644212A126E0B0D685DA84_CustomAttributesCacheGenerator_U3CPopulateHeatmapU3Ed__42_System_IDisposable_Dispose_m1097B7B94EB08BA3EF350FE660AD7B9956863392,
	U3CPopulateHeatmapU3Ed__42_tEE23C9F4ED699F6431644212A126E0B0D685DA84_CustomAttributesCacheGenerator_U3CPopulateHeatmapU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0976BC78E7B504C287CB571F0DE0B71DF8413CC6,
	U3CPopulateHeatmapU3Ed__42_tEE23C9F4ED699F6431644212A126E0B0D685DA84_CustomAttributesCacheGenerator_U3CPopulateHeatmapU3Ed__42_System_Collections_IEnumerator_Reset_m83A7B4025FE7ECAD0E352634B658870920480462,
	U3CPopulateHeatmapU3Ed__42_tEE23C9F4ED699F6431644212A126E0B0D685DA84_CustomAttributesCacheGenerator_U3CPopulateHeatmapU3Ed__42_System_Collections_IEnumerator_get_Current_m661273649CF08D4B0CE3715EA1ACE2A6F8530DD3,
	U3CUpdateStatusU3Ed__43_t04A5CD192106000D7D3513F48FA80B8D35403EBB_CustomAttributesCacheGenerator_U3CUpdateStatusU3Ed__43__ctor_m970C1666C59016AEFE7E3CB5257A6E95BA8A2B91,
	U3CUpdateStatusU3Ed__43_t04A5CD192106000D7D3513F48FA80B8D35403EBB_CustomAttributesCacheGenerator_U3CUpdateStatusU3Ed__43_System_IDisposable_Dispose_m2BF8311A1FA147B50BAA705D688E487697456AFB,
	U3CUpdateStatusU3Ed__43_t04A5CD192106000D7D3513F48FA80B8D35403EBB_CustomAttributesCacheGenerator_U3CUpdateStatusU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE88D22C380772911600CEDC6B6C20DECF74174DC,
	U3CUpdateStatusU3Ed__43_t04A5CD192106000D7D3513F48FA80B8D35403EBB_CustomAttributesCacheGenerator_U3CUpdateStatusU3Ed__43_System_Collections_IEnumerator_Reset_mA92CE780FB948B861106556A7D441579F0E52951,
	U3CUpdateStatusU3Ed__43_t04A5CD192106000D7D3513F48FA80B8D35403EBB_CustomAttributesCacheGenerator_U3CUpdateStatusU3Ed__43_System_Collections_IEnumerator_get_Current_m4E0F683876005E30B881179D5A5255E3BC229525,
	U3CAddToCounterU3Ed__44_t9030FB67DA45E920F753C3098D5B310AB504A561_CustomAttributesCacheGenerator_U3CAddToCounterU3Ed__44__ctor_m6FB296DA9BFA0BA393DAD6F0B6CC62FE117DEA6B,
	U3CAddToCounterU3Ed__44_t9030FB67DA45E920F753C3098D5B310AB504A561_CustomAttributesCacheGenerator_U3CAddToCounterU3Ed__44_System_IDisposable_Dispose_mDCE82C6860FA2A2FF993B0E52EFD549B701BBAE9,
	U3CAddToCounterU3Ed__44_t9030FB67DA45E920F753C3098D5B310AB504A561_CustomAttributesCacheGenerator_U3CAddToCounterU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA8B93ED764B147113D8B736B485AE53DFD39DB52,
	U3CAddToCounterU3Ed__44_t9030FB67DA45E920F753C3098D5B310AB504A561_CustomAttributesCacheGenerator_U3CAddToCounterU3Ed__44_System_Collections_IEnumerator_Reset_m331A758A780FF4767AF4D233962A09D5E9D11954,
	U3CAddToCounterU3Ed__44_t9030FB67DA45E920F753C3098D5B310AB504A561_CustomAttributesCacheGenerator_U3CAddToCounterU3Ed__44_System_Collections_IEnumerator_get_Current_mA947986D577DD4062D7BFC7DBA9A0D6608A0CADC,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
