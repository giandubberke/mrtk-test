﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.UInt64 Microsoft.MixedReality.OpenXR.AnchorConverter::ToOpenXRHandle(System.IntPtr)
extern void AnchorConverter_ToOpenXRHandle_m57977A85F0D37A59886DF7893B987DD1254A49E5 (void);
// 0x00000002 System.Object Microsoft.MixedReality.OpenXR.AnchorConverter::ToPerceptionSpatialAnchor(System.IntPtr)
extern void AnchorConverter_ToPerceptionSpatialAnchor_mD4AAECBDD16942CA9D2854F0558664B3A9D4F649 (void);
// 0x00000003 System.Object Microsoft.MixedReality.OpenXR.AnchorConverter::ToPerceptionSpatialAnchor(UnityEngine.XR.ARSubsystems.TrackableId)
extern void AnchorConverter_ToPerceptionSpatialAnchor_mDA9208F94F7E57458A45B7ECE5A18B4FEECB1B85 (void);
// 0x00000004 UnityEngine.XR.ARSubsystems.TrackableId Microsoft.MixedReality.OpenXR.AnchorConverter::FromPerceptionSpatialAnchor(System.Object)
extern void AnchorConverter_FromPerceptionSpatialAnchor_m0637FE1467FBBE7683CCD3B85D29C0D4AE934594 (void);
// 0x00000005 UnityEngine.XR.ARSubsystems.TrackableId Microsoft.MixedReality.OpenXR.AnchorConverter::ReplaceSpatialAnchor(System.Object,UnityEngine.XR.ARSubsystems.TrackableId)
extern void AnchorConverter_ReplaceSpatialAnchor_mF0D803DBBEF3803A3061DBA12422F1B0F3275076 (void);
// 0x00000006 Microsoft.MixedReality.OpenXR.ControllerModel Microsoft.MixedReality.OpenXR.ControllerModel::get_Left()
extern void ControllerModel_get_Left_m2A3E61DAC18EE9F9E1828A053A4028437B93C248 (void);
// 0x00000007 Microsoft.MixedReality.OpenXR.ControllerModel Microsoft.MixedReality.OpenXR.ControllerModel::get_Right()
extern void ControllerModel_get_Right_m1894F74F10AD4AECDA206CEDF56290B1C2F8B62F (void);
// 0x00000008 System.Void Microsoft.MixedReality.OpenXR.ControllerModel::.ctor(Microsoft.MixedReality.OpenXR.Handedness)
extern void ControllerModel__ctor_mEE62B39AC6CB6F90AFE871970E93BEB51A1AFF9A (void);
// 0x00000009 System.Boolean Microsoft.MixedReality.OpenXR.ControllerModel::TryGetControllerModelKey(System.UInt64&)
extern void ControllerModel_TryGetControllerModelKey_m3F85931A47F56FF8AAEF420C38B802E31B08EAE3 (void);
// 0x0000000A System.Threading.Tasks.Task`1<System.Byte[]> Microsoft.MixedReality.OpenXR.ControllerModel::TryGetControllerModel(System.UInt64)
extern void ControllerModel_TryGetControllerModel_m8BD6F01D3DE95E2F02E42EB2A04C885722404971 (void);
// 0x0000000B System.Void Microsoft.MixedReality.OpenXR.ControllerModel::.cctor()
extern void ControllerModel__cctor_m4BC8AE2D60E49E5C222928E9CDCB90457003B57B (void);
// 0x0000000C System.Void Microsoft.MixedReality.OpenXR.ControllerModel/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m6E8D0FC9C3C011B46B4ABE02FC955075F8F4B7CC (void);
// 0x0000000D System.Byte[] Microsoft.MixedReality.OpenXR.ControllerModel/<>c__DisplayClass13_0::<TryGetControllerModel>b__0()
extern void U3CU3Ec__DisplayClass13_0_U3CTryGetControllerModelU3Eb__0_m490119CF2A3CE5379B03DC5E9CC21988E9D1B843 (void);
// 0x0000000E System.Void Microsoft.MixedReality.OpenXR.EyeLevelSceneOrigin::OnEnable()
extern void EyeLevelSceneOrigin_OnEnable_m466D14A8F8D036F5499B3BE921C022C4E7CBD872 (void);
// 0x0000000F System.Void Microsoft.MixedReality.OpenXR.EyeLevelSceneOrigin::OnDisable()
extern void EyeLevelSceneOrigin_OnDisable_m272DBAC8618818B98EE030DFE4693C2FF2192A33 (void);
// 0x00000010 System.Void Microsoft.MixedReality.OpenXR.EyeLevelSceneOrigin::XrInput_trackingOriginUpdated(UnityEngine.XR.XRInputSubsystem)
extern void EyeLevelSceneOrigin_XrInput_trackingOriginUpdated_m1671E7925D1390710E23A2ABCEF08755D6C3E996 (void);
// 0x00000011 System.Void Microsoft.MixedReality.OpenXR.EyeLevelSceneOrigin::EnsureSceneOriginAtEyeLevel()
extern void EyeLevelSceneOrigin_EnsureSceneOriginAtEyeLevel_m012D81B67DF28B89A065DF84EE470C5C03AF272C (void);
// 0x00000012 System.Void Microsoft.MixedReality.OpenXR.EyeLevelSceneOrigin::SetEyeLevelTrackingOriginMode(UnityEngine.XR.XRInputSubsystem)
extern void EyeLevelSceneOrigin_SetEyeLevelTrackingOriginMode_mE7BA1DB16D8E8D7515D63357A3D6C2EE61CEFC79 (void);
// 0x00000013 System.Void Microsoft.MixedReality.OpenXR.EyeLevelSceneOrigin::.ctor()
extern void EyeLevelSceneOrigin__ctor_m8905D7EA3C7D43C7F3942610ACFF58E252DF2E11 (void);
// 0x00000014 Microsoft.MixedReality.OpenXR.GestureEventType Microsoft.MixedReality.OpenXR.GestureEventData::get_EventType()
extern void GestureEventData_get_EventType_m88A55F7183D10CFDCC7E76F9177EEB0093252824 (void);
// 0x00000015 Microsoft.MixedReality.OpenXR.GestureHandedness Microsoft.MixedReality.OpenXR.GestureEventData::get_Handedness()
extern void GestureEventData_get_Handedness_m98BC78EB77B9209D843E5A0A670926EA1CD6BBD0 (void);
// 0x00000016 System.Nullable`1<Microsoft.MixedReality.OpenXR.TappedEventData> Microsoft.MixedReality.OpenXR.GestureEventData::get_TappedData()
extern void GestureEventData_get_TappedData_mF12907AF8B7A81683ABBB5ED3D482FCDC87D6AE1 (void);
// 0x00000017 System.Nullable`1<Microsoft.MixedReality.OpenXR.ManipulationEventData> Microsoft.MixedReality.OpenXR.GestureEventData::get_ManipulationData()
extern void GestureEventData_get_ManipulationData_m10D386734CCEA909E0FF3E3EB046790B38E6791E (void);
// 0x00000018 System.Nullable`1<Microsoft.MixedReality.OpenXR.NavigationEventData> Microsoft.MixedReality.OpenXR.GestureEventData::get_NavigationData()
extern void GestureEventData_get_NavigationData_m18D58022F182D64E1BAF6A1D37C10188EB4BBCF8 (void);
// 0x00000019 System.Boolean Microsoft.MixedReality.OpenXR.NavigationEventData::get_IsNavigatingX()
extern void NavigationEventData_get_IsNavigatingX_m20EFA7621A684A1145CA1B769B386425021E1C2F (void);
// 0x0000001A System.Boolean Microsoft.MixedReality.OpenXR.NavigationEventData::get_IsNavigatingY()
extern void NavigationEventData_get_IsNavigatingY_m760BEDA5ED16D459CFECA95D90EFB10A5129F807 (void);
// 0x0000001B System.Boolean Microsoft.MixedReality.OpenXR.NavigationEventData::get_IsNavigatingZ()
extern void NavigationEventData_get_IsNavigatingZ_mAB4BB500BB8EA730BD60A31565D58859E900895A (void);
// 0x0000001C System.Void Microsoft.MixedReality.OpenXR.GestureRecognizer::.ctor(Microsoft.MixedReality.OpenXR.GestureSettings)
extern void GestureRecognizer__ctor_m2AC06A178237E25789FBDC0DC19EC6D906862D95 (void);
// 0x0000001D Microsoft.MixedReality.OpenXR.GestureSettings Microsoft.MixedReality.OpenXR.GestureRecognizer::get_GestureSettings()
extern void GestureRecognizer_get_GestureSettings_m5919DEAA3DE549B0C614AC242B5E2F620263DEE8 (void);
// 0x0000001E System.Void Microsoft.MixedReality.OpenXR.GestureRecognizer::set_GestureSettings(Microsoft.MixedReality.OpenXR.GestureSettings)
extern void GestureRecognizer_set_GestureSettings_m006ED56582BD067E23D2814F378D8B19391D4836 (void);
// 0x0000001F System.Void Microsoft.MixedReality.OpenXR.GestureRecognizer::Start()
extern void GestureRecognizer_Start_m5DAC513272A992DCCD96134ED7C2F95071DC46F4 (void);
// 0x00000020 System.Void Microsoft.MixedReality.OpenXR.GestureRecognizer::Stop()
extern void GestureRecognizer_Stop_m1905298F98D20231566B92F4D11924DA1E5D3781 (void);
// 0x00000021 System.Boolean Microsoft.MixedReality.OpenXR.GestureRecognizer::TryGetNextEvent(Microsoft.MixedReality.OpenXR.GestureEventData&)
extern void GestureRecognizer_TryGetNextEvent_m1259E5C44C8E1F78B03017BECBD37AD4E3E1582A (void);
// 0x00000022 System.Void Microsoft.MixedReality.OpenXR.GestureRecognizer::CancelPendingGestures()
extern void GestureRecognizer_CancelPendingGestures_m454E85BCA6F1DC02C7E9004F1802F6B07607454F (void);
// 0x00000023 System.Void Microsoft.MixedReality.OpenXR.GestureRecognizer::DisposeManagedResources()
extern void GestureRecognizer_DisposeManagedResources_mC51CBD87F8A6EA8C28CAEE26712BF6FBEA1E4517 (void);
// 0x00000024 Microsoft.MixedReality.OpenXR.HandMeshTracker Microsoft.MixedReality.OpenXR.HandMeshTracker::get_Left()
extern void HandMeshTracker_get_Left_m7F701DFA8E6967020927A919F70DC3CBC3AD0A30 (void);
// 0x00000025 Microsoft.MixedReality.OpenXR.HandMeshTracker Microsoft.MixedReality.OpenXR.HandMeshTracker::get_Right()
extern void HandMeshTracker_get_Right_mF2125AE765BF5540B6F8A455EE3773BF90C602A0 (void);
// 0x00000026 System.Void Microsoft.MixedReality.OpenXR.HandMeshTracker::.ctor(Microsoft.MixedReality.OpenXR.Handedness)
extern void HandMeshTracker__ctor_mBA6A4D33C1D7670D565D14AFC1E84DA4C9363E5D (void);
// 0x00000027 System.Boolean Microsoft.MixedReality.OpenXR.HandMeshTracker::TryLocateHandMesh(Microsoft.MixedReality.OpenXR.FrameTime,UnityEngine.Pose&,Microsoft.MixedReality.OpenXR.HandPoseType)
extern void HandMeshTracker_TryLocateHandMesh_m4C066C46D449B6D3E379BAE5B67EF1D951110976 (void);
// 0x00000028 System.Boolean Microsoft.MixedReality.OpenXR.HandMeshTracker::TryGetHandMesh(Microsoft.MixedReality.OpenXR.FrameTime,UnityEngine.Mesh,Microsoft.MixedReality.OpenXR.HandPoseType)
extern void HandMeshTracker_TryGetHandMesh_m12D7875710CA0D6E84E18A903313E331D60D50C8 (void);
// 0x00000029 System.Void Microsoft.MixedReality.OpenXR.HandMeshTracker::.cctor()
extern void HandMeshTracker__cctor_mDF10E2B657BDFA932979BF0A25994E5AB816C80A (void);
// 0x0000002A Microsoft.MixedReality.OpenXR.HandTracker Microsoft.MixedReality.OpenXR.HandTracker::get_Left()
extern void HandTracker_get_Left_m6ABBCEB6043192B964B6BCD295222BF4B837868F (void);
// 0x0000002B Microsoft.MixedReality.OpenXR.HandTracker Microsoft.MixedReality.OpenXR.HandTracker::get_Right()
extern void HandTracker_get_Right_mABE9DD4B81AC8FA6EBC72D3228DEC7B3AD9D7863 (void);
// 0x0000002C System.Void Microsoft.MixedReality.OpenXR.HandTracker::.ctor(Microsoft.MixedReality.OpenXR.Handedness)
extern void HandTracker__ctor_m3D77154AE26E5E6D27EA6DD95F62E01E20D5A3D7 (void);
// 0x0000002D System.Boolean Microsoft.MixedReality.OpenXR.HandTracker::TryLocateHandJoints(Microsoft.MixedReality.OpenXR.FrameTime,Microsoft.MixedReality.OpenXR.HandJointLocation[])
extern void HandTracker_TryLocateHandJoints_mB54C6CB63F2513A0EC2FCE33694C6989F2BB64EC (void);
// 0x0000002E System.Void Microsoft.MixedReality.OpenXR.HandTracker::.cctor()
extern void HandTracker__cctor_mE2018841AEA9DFC8F367AF896D979D1CDAB0A99C (void);
// 0x0000002F System.Boolean Microsoft.MixedReality.OpenXR.HandJointLocation::get_IsTracked()
extern void HandJointLocation_get_IsTracked_m21F8ECE32FD2B263058F0ED2A74C0BE4A4F2F7F7 (void);
// 0x00000030 UnityEngine.Pose Microsoft.MixedReality.OpenXR.HandJointLocation::get_Pose()
extern void HandJointLocation_get_Pose_m4DE68CD498791989C2F1B3B77BE4095DEA4A45F6 (void);
// 0x00000031 System.Single Microsoft.MixedReality.OpenXR.HandJointLocation::get_Radius()
extern void HandJointLocation_get_Radius_m64C5A593D416786073BB54194A760A355A637AA9 (void);
// 0x00000032 Microsoft.MixedReality.OpenXR.MeshType Microsoft.MixedReality.OpenXR.MeshComputeSettings::get_MeshType()
extern void MeshComputeSettings_get_MeshType_m9E560045929876C478863613388EB00DDFDF530E (void);
// 0x00000033 System.Void Microsoft.MixedReality.OpenXR.MeshComputeSettings::set_MeshType(Microsoft.MixedReality.OpenXR.MeshType)
extern void MeshComputeSettings_set_MeshType_m34356D59AC315E911007B0170A2244584B02A1E3 (void);
// 0x00000034 Microsoft.MixedReality.OpenXR.VisualMeshLevelOfDetail Microsoft.MixedReality.OpenXR.MeshComputeSettings::get_VisualMeshLevelOfDetail()
extern void MeshComputeSettings_get_VisualMeshLevelOfDetail_m9C4A5CE475233BF6C80EF15A170C728C532AD3CC (void);
// 0x00000035 System.Void Microsoft.MixedReality.OpenXR.MeshComputeSettings::set_VisualMeshLevelOfDetail(Microsoft.MixedReality.OpenXR.VisualMeshLevelOfDetail)
extern void MeshComputeSettings_set_VisualMeshLevelOfDetail_m306CDDDE899A58AC03EC954FB2B223F45AE71157 (void);
// 0x00000036 Microsoft.MixedReality.OpenXR.MeshComputeConsistency Microsoft.MixedReality.OpenXR.MeshComputeSettings::get_MeshComputeConsistency()
extern void MeshComputeSettings_get_MeshComputeConsistency_mED95E2D4FCAF5A1DE8F0E2C82B5B56E11D7BA878 (void);
// 0x00000037 System.Void Microsoft.MixedReality.OpenXR.MeshComputeSettings::set_MeshComputeConsistency(Microsoft.MixedReality.OpenXR.MeshComputeConsistency)
extern void MeshComputeSettings_set_MeshComputeConsistency_m2D0A6C5A790746581EF229D15944E0B8B78FBDFA (void);
// 0x00000038 System.Boolean Microsoft.MixedReality.OpenXR.MeshSettings::TrySetMeshComputeSettings(Microsoft.MixedReality.OpenXR.MeshComputeSettings)
extern void MeshSettings_TrySetMeshComputeSettings_m3C3A855F978DA3D525DEAD342C1FE2667120C3EF (void);
// 0x00000039 Microsoft.MixedReality.OpenXR.OpenXRContext Microsoft.MixedReality.OpenXR.OpenXRContext::get_Current()
extern void OpenXRContext_get_Current_m9D565CE153060E1C4A7A29752AEED8DCC8ACE293 (void);
// 0x0000003A System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRContext::get_Instance()
extern void OpenXRContext_get_Instance_m175F38DD43376A21CDC502AF984D9FA57D1A9A05 (void);
// 0x0000003B System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRContext::get_SystemId()
extern void OpenXRContext_get_SystemId_m4F8FB8858DB630ECE72D7988CD80E9F396B7018D (void);
// 0x0000003C System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRContext::get_Session()
extern void OpenXRContext_get_Session_m05C3CAE3B99D93180963F002BC41F14947358AA5 (void);
// 0x0000003D System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRContext::get_SceneOriginSpace()
extern void OpenXRContext_get_SceneOriginSpace_mAB9662C8329EFFED26193106C0D2D1F6C783488D (void);
// 0x0000003E System.IntPtr Microsoft.MixedReality.OpenXR.OpenXRContext::GetInstanceProcAddr(System.String)
extern void OpenXRContext_GetInstanceProcAddr_mF32DA1231AE49E1757544E1C0996D4273FE27BE6 (void);
// 0x0000003F System.Void Microsoft.MixedReality.OpenXR.OpenXRContext::.ctor()
extern void OpenXRContext__ctor_mE2E62F2BE7C68614D9C1BE48478F099563BA1FC0 (void);
// 0x00000040 System.Void Microsoft.MixedReality.OpenXR.OpenXRContext::.cctor()
extern void OpenXRContext__cctor_m5C68198775E43BD5A7425FCE12B532809E3FA62E (void);
// 0x00000041 System.Object Microsoft.MixedReality.OpenXR.PerceptionInterop::GetSceneCoordinateSystem(UnityEngine.Pose)
extern void PerceptionInterop_GetSceneCoordinateSystem_m814779B4D45B7613A6FF8C8F544692A42378C6E0 (void);
// 0x00000042 System.Void Microsoft.MixedReality.OpenXR.PerceptionInterop::.cctor()
extern void PerceptionInterop__cctor_mDC04F9A390B39BB8902FEB6C58B8C9525E060645 (void);
// 0x00000043 Microsoft.MixedReality.OpenXR.SpatialGraphNode Microsoft.MixedReality.OpenXR.SpatialGraphNode::FromStaticNodeId(System.Guid)
extern void SpatialGraphNode_FromStaticNodeId_m36EF16C1ED4C1DE619443C09A88BFAD855A7F118 (void);
// 0x00000044 System.Guid Microsoft.MixedReality.OpenXR.SpatialGraphNode::get_Id()
extern void SpatialGraphNode_get_Id_m0ADD258414D6FB14EF117EA022FF1D1C9EF43DAA (void);
// 0x00000045 System.Void Microsoft.MixedReality.OpenXR.SpatialGraphNode::set_Id(System.Guid)
extern void SpatialGraphNode_set_Id_mF565F99FC4FB8CC47BAC58EFEE820E8CBBE2B2C9 (void);
// 0x00000046 System.Boolean Microsoft.MixedReality.OpenXR.SpatialGraphNode::TryLocate(Microsoft.MixedReality.OpenXR.FrameTime,UnityEngine.Pose&)
extern void SpatialGraphNode_TryLocate_m1303DC576313BDB89974B56C3152F577ACB8A881 (void);
// 0x00000047 System.Void Microsoft.MixedReality.OpenXR.SpatialGraphNode::.ctor()
extern void SpatialGraphNode__ctor_mFF6542891FB04E66B9C3613E4237CDEC2E49A0B3 (void);
// 0x00000048 System.Collections.Generic.IReadOnlyList`1<Microsoft.MixedReality.OpenXR.ViewConfiguration> Microsoft.MixedReality.OpenXR.ViewConfiguration::get_EnabledViewConfigurations()
extern void ViewConfiguration_get_EnabledViewConfigurations_mDD45DF9719FA89E5B2898D4C5D93E44E7FE46DC7 (void);
// 0x00000049 System.Void Microsoft.MixedReality.OpenXR.ViewConfiguration::.ctor(Microsoft.MixedReality.OpenXR.OpenXRViewConfiguration)
extern void ViewConfiguration__ctor_mC98910BB64A18EF97C707339C732D86B2FD5305B (void);
// 0x0000004A Microsoft.MixedReality.OpenXR.ViewConfigurationType Microsoft.MixedReality.OpenXR.ViewConfiguration::get_ViewConfigurationType()
extern void ViewConfiguration_get_ViewConfigurationType_m2CCCF7ABF9EBE176E5E839CE040E251E51B394B3 (void);
// 0x0000004B System.Boolean Microsoft.MixedReality.OpenXR.ViewConfiguration::get_IsActive()
extern void ViewConfiguration_get_IsActive_m36150FA9963CEBF6B1628AEEA61E75716C8EBDE8 (void);
// 0x0000004C System.Collections.Generic.IReadOnlyList`1<Microsoft.MixedReality.OpenXR.ReprojectionMode> Microsoft.MixedReality.OpenXR.ViewConfiguration::get_SupportedReprojectionModes()
extern void ViewConfiguration_get_SupportedReprojectionModes_m4A049171510E53D849274DDA6E4F1F70066F93D2 (void);
// 0x0000004D System.Void Microsoft.MixedReality.OpenXR.ViewConfiguration::SetReprojectionSettings(Microsoft.MixedReality.OpenXR.ReprojectionSettings)
extern void ViewConfiguration_SetReprojectionSettings_mB3CA9C8F4836C92F4DF7148F560511D43C81E567 (void);
// 0x0000004E System.Boolean Microsoft.MixedReality.OpenXR.ViewConfiguration::get_IsTracked()
extern void ViewConfiguration_get_IsTracked_m807FD4FDEAB78B3D0A83FF6C7163A934159DAFA6 (void);
// 0x0000004F System.Void Microsoft.MixedReality.OpenXR.ViewConfiguration::.cctor()
extern void ViewConfiguration__cctor_mDBFDE3E0BFF7234BF307F6006281A0EB141949B4 (void);
// 0x00000050 Microsoft.MixedReality.OpenXR.ReprojectionMode Microsoft.MixedReality.OpenXR.ReprojectionSettings::get_ReprojectionMode()
extern void ReprojectionSettings_get_ReprojectionMode_m7F36D9AAEDD1924B4F24EA0EAF0028455B00E577 (void);
// 0x00000051 System.Void Microsoft.MixedReality.OpenXR.ReprojectionSettings::set_ReprojectionMode(Microsoft.MixedReality.OpenXR.ReprojectionMode)
extern void ReprojectionSettings_set_ReprojectionMode_m9FD37CBAA6EF0FF3C5A5C5D58558E24862542292 (void);
// 0x00000052 System.Collections.Generic.IReadOnlyList`1<System.String> Microsoft.MixedReality.OpenXR.XRAnchorStore::get_PersistedAnchorNames()
extern void XRAnchorStore_get_PersistedAnchorNames_m5BC2407ED032BEAAE255D39C1776F525C74B027E (void);
// 0x00000053 UnityEngine.XR.ARSubsystems.TrackableId Microsoft.MixedReality.OpenXR.XRAnchorStore::LoadAnchor(System.String)
extern void XRAnchorStore_LoadAnchor_m35F16E71D5D1CD79354EDAEC1C56F22FE124EF41 (void);
// 0x00000054 System.Boolean Microsoft.MixedReality.OpenXR.XRAnchorStore::TryPersistAnchor(UnityEngine.XR.ARSubsystems.TrackableId,System.String)
extern void XRAnchorStore_TryPersistAnchor_mE007CD067D0FD325C93B9B4B29921ACF12786C1C (void);
// 0x00000055 System.Void Microsoft.MixedReality.OpenXR.XRAnchorStore::UnpersistAnchor(System.String)
extern void XRAnchorStore_UnpersistAnchor_m2CDD559F8056F908A67F73844838AFC9DEF52BFB (void);
// 0x00000056 System.Void Microsoft.MixedReality.OpenXR.XRAnchorStore::Clear()
extern void XRAnchorStore_Clear_m1EC7F985022EF59EF217B6D083ECA8510EE733D7 (void);
// 0x00000057 System.Threading.Tasks.Task`1<Microsoft.MixedReality.OpenXR.XRAnchorStore> Microsoft.MixedReality.OpenXR.XRAnchorStore::LoadAsync(UnityEngine.XR.ARSubsystems.XRAnchorSubsystem)
extern void XRAnchorStore_LoadAsync_m140C720CCE0F64BB4692226F25BC74943FDEBF9B (void);
// 0x00000058 System.Void Microsoft.MixedReality.OpenXR.XRAnchorStore::.ctor(Microsoft.MixedReality.OpenXR.OpenXRAnchorStore)
extern void XRAnchorStore__ctor_m0CE578DC335F8682183E64C52DE75CFC09173EF7 (void);
// 0x00000059 System.Void Microsoft.MixedReality.OpenXR.XRAnchorStore/<LoadAsync>d__6::.ctor()
extern void U3CLoadAsyncU3Ed__6__ctor_m93FB7DBA5A043173929E4FA78F9EDE305B0D49B2 (void);
// 0x0000005A System.Void Microsoft.MixedReality.OpenXR.XRAnchorStore/<LoadAsync>d__6::MoveNext()
extern void U3CLoadAsyncU3Ed__6_MoveNext_mE273186E26622B88F0AE268D843B6BFF9452BAB9 (void);
// 0x0000005B System.Void Microsoft.MixedReality.OpenXR.XRAnchorStore/<LoadAsync>d__6::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CLoadAsyncU3Ed__6_SetStateMachine_mFD7FD11F07375E809C056D42937AFCB1C9C0CEE9 (void);
// 0x0000005C System.Void Microsoft.MixedReality.OpenXR.XRAnchorTransferBatch::.ctor()
extern void XRAnchorTransferBatch__ctor_m7F04A6B9B5BA215663DA0E657CF1E7C06536411F (void);
// 0x0000005D System.Void Microsoft.MixedReality.OpenXR.XRAnchorTransferBatch::.ctor(Microsoft.MixedReality.OpenXR.AnchorTransferBatch)
extern void XRAnchorTransferBatch__ctor_m8615FB163C8584AF726FAC9B7A7C9C3B63E37E77 (void);
// 0x0000005E System.Collections.Generic.IReadOnlyList`1<System.String> Microsoft.MixedReality.OpenXR.XRAnchorTransferBatch::get_AnchorNames()
extern void XRAnchorTransferBatch_get_AnchorNames_m8FEA9792C34E303694DC32AAD8CE51CC17479F8E (void);
// 0x0000005F System.Boolean Microsoft.MixedReality.OpenXR.XRAnchorTransferBatch::AddAnchor(UnityEngine.XR.ARSubsystems.TrackableId,System.String)
extern void XRAnchorTransferBatch_AddAnchor_mCFDC30C9C3839837378293B50C6FA4B0CF8C5ADE (void);
// 0x00000060 System.Void Microsoft.MixedReality.OpenXR.XRAnchorTransferBatch::RemoveAnchor(System.String)
extern void XRAnchorTransferBatch_RemoveAnchor_m48E9574BE7CCF7EECC610331D7E06A26DD2BBC47 (void);
// 0x00000061 System.Void Microsoft.MixedReality.OpenXR.XRAnchorTransferBatch::Clear()
extern void XRAnchorTransferBatch_Clear_m53EDCD2D497B1D8C247998B882B5D234BFC977F1 (void);
// 0x00000062 UnityEngine.XR.ARSubsystems.TrackableId Microsoft.MixedReality.OpenXR.XRAnchorTransferBatch::LoadAnchor(System.String)
extern void XRAnchorTransferBatch_LoadAnchor_m5EDDDB48090153419966523B75C4D511386750B4 (void);
// 0x00000063 UnityEngine.XR.ARSubsystems.TrackableId Microsoft.MixedReality.OpenXR.XRAnchorTransferBatch::LoadAndReplaceAnchor(System.String,UnityEngine.XR.ARSubsystems.TrackableId)
extern void XRAnchorTransferBatch_LoadAndReplaceAnchor_mB04292E78E85053BEEB0C5A408D671A7E9DCAD98 (void);
// 0x00000064 System.Threading.Tasks.Task`1<System.IO.Stream> Microsoft.MixedReality.OpenXR.XRAnchorTransferBatch::ExportAsync(Microsoft.MixedReality.OpenXR.XRAnchorTransferBatch)
extern void XRAnchorTransferBatch_ExportAsync_m8778FE35361BBA2382A0C77E59EAABB6490CC345 (void);
// 0x00000065 System.Threading.Tasks.Task`1<Microsoft.MixedReality.OpenXR.XRAnchorTransferBatch> Microsoft.MixedReality.OpenXR.XRAnchorTransferBatch::ImportAsync(System.IO.Stream)
extern void XRAnchorTransferBatch_ImportAsync_m9D90925BC9D1A90EAC7F353F0162C2FFBD2A94A1 (void);
// 0x00000066 System.Void Microsoft.MixedReality.OpenXR.XRAnchorTransferBatch/<ExportAsync>d__10::.ctor()
extern void U3CExportAsyncU3Ed__10__ctor_mE813862D495E5E8ED943B4977C5B3B7BB226B0E6 (void);
// 0x00000067 System.Void Microsoft.MixedReality.OpenXR.XRAnchorTransferBatch/<ExportAsync>d__10::MoveNext()
extern void U3CExportAsyncU3Ed__10_MoveNext_mE6C1EEEC49BB5D0626BE76D1DA0D4EA7B77B7134 (void);
// 0x00000068 System.Void Microsoft.MixedReality.OpenXR.XRAnchorTransferBatch/<ExportAsync>d__10::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CExportAsyncU3Ed__10_SetStateMachine_m4039FDD3FA60D54D84A6891420CDCF6E185DF63C (void);
// 0x00000069 System.Void Microsoft.MixedReality.OpenXR.XRAnchorTransferBatch/<ImportAsync>d__11::.ctor()
extern void U3CImportAsyncU3Ed__11__ctor_mECD0015D303AD5591566D8C1D4F121D1674A4AA4 (void);
// 0x0000006A System.Void Microsoft.MixedReality.OpenXR.XRAnchorTransferBatch/<ImportAsync>d__11::MoveNext()
extern void U3CImportAsyncU3Ed__11_MoveNext_m519F289E3957FF1B1D1646697D9B4E3C8CFD095C (void);
// 0x0000006B System.Void Microsoft.MixedReality.OpenXR.XRAnchorTransferBatch/<ImportAsync>d__11::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CImportAsyncU3Ed__11_SetStateMachine_m908CF6DE008204CAD11D883A71E3B6E99D90943E (void);
// 0x0000006C System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile::RegisterDeviceLayout()
extern void HPMixedRealityControllerProfile_RegisterDeviceLayout_m9451A9C2712715503B980648516413572AC10508 (void);
// 0x0000006D System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile::UnregisterDeviceLayout()
extern void HPMixedRealityControllerProfile_UnregisterDeviceLayout_m744E74FD5D70711D8766E18D40A4EDC79FCFCA12 (void);
// 0x0000006E System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile::RegisterActionMapsWithRuntime()
extern void HPMixedRealityControllerProfile_RegisterActionMapsWithRuntime_mC53E75CB3BC74030D17997A8868B76C8E0B8B709 (void);
// 0x0000006F System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile::.ctor()
extern void HPMixedRealityControllerProfile__ctor_mADA37F7C274FE24CF4E95295FA3B205E80AB5568 (void);
// 0x00000070 UnityEngine.InputSystem.Controls.Vector2Control Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_thumbstick()
extern void HPMixedRealityController_get_thumbstick_m2C0F38FC01ABFCB7AD18CAEFB497E1A58B5E0F3D (void);
// 0x00000071 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_thumbstick(UnityEngine.InputSystem.Controls.Vector2Control)
extern void HPMixedRealityController_set_thumbstick_mF15870EA7CF9586CF3B2EE1AEFB9AC194CE0DD9A (void);
// 0x00000072 UnityEngine.InputSystem.Controls.AxisControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_grip()
extern void HPMixedRealityController_get_grip_mB724A251071F8959D7A757B8123942D158B6679B (void);
// 0x00000073 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_grip(UnityEngine.InputSystem.Controls.AxisControl)
extern void HPMixedRealityController_set_grip_m201A1C05844AE3ED591E773C49767CD11CDEEC2E (void);
// 0x00000074 UnityEngine.InputSystem.Controls.ButtonControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_gripPressed()
extern void HPMixedRealityController_get_gripPressed_m3375FC51DD0439A9C716C58384A1BF4FD6DFFE62 (void);
// 0x00000075 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_gripPressed(UnityEngine.InputSystem.Controls.ButtonControl)
extern void HPMixedRealityController_set_gripPressed_mFB640265D90068D64206196B3B0920A57A2D5792 (void);
// 0x00000076 UnityEngine.InputSystem.Controls.ButtonControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_menu()
extern void HPMixedRealityController_get_menu_m635552E39C1978802F4B595C0C4C49DCF0D15DD1 (void);
// 0x00000077 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_menu(UnityEngine.InputSystem.Controls.ButtonControl)
extern void HPMixedRealityController_set_menu_m3CB5A5047D523E787E8A0A8E350F52D6659AB81A (void);
// 0x00000078 UnityEngine.InputSystem.Controls.ButtonControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_primaryButton()
extern void HPMixedRealityController_get_primaryButton_m40DFE0108AA2AB547B384EB54F461ACDDFE928DD (void);
// 0x00000079 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_primaryButton(UnityEngine.InputSystem.Controls.ButtonControl)
extern void HPMixedRealityController_set_primaryButton_mA99639AADA78F70833EF8A547ED27E9788A46C15 (void);
// 0x0000007A UnityEngine.InputSystem.Controls.ButtonControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_secondaryButton()
extern void HPMixedRealityController_get_secondaryButton_m0CEC92E2A3DA6284CF2BA3A47F4B042FB2A1691F (void);
// 0x0000007B System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_secondaryButton(UnityEngine.InputSystem.Controls.ButtonControl)
extern void HPMixedRealityController_set_secondaryButton_mFE5C25FC73EE85331F631AD4B5FAE21B046B022D (void);
// 0x0000007C UnityEngine.InputSystem.Controls.AxisControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_trigger()
extern void HPMixedRealityController_get_trigger_mE54BDE93DD5580AFBC805D435D9D2D0229EB3F30 (void);
// 0x0000007D System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_trigger(UnityEngine.InputSystem.Controls.AxisControl)
extern void HPMixedRealityController_set_trigger_m6D3E29454B07C891F15C12F0EF18DDB459B80767 (void);
// 0x0000007E UnityEngine.InputSystem.Controls.ButtonControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_triggerPressed()
extern void HPMixedRealityController_get_triggerPressed_mE16F7D0426B5000F3A1CAA459036DADC3F0A2E26 (void);
// 0x0000007F System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_triggerPressed(UnityEngine.InputSystem.Controls.ButtonControl)
extern void HPMixedRealityController_set_triggerPressed_m5DF391FD4FED57840E6CEE38EA236FF3A2CEFF46 (void);
// 0x00000080 UnityEngine.InputSystem.Controls.ButtonControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_thumbstickClicked()
extern void HPMixedRealityController_get_thumbstickClicked_m4737036F9F85F7D4ECB0F3FED80E571824247224 (void);
// 0x00000081 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_thumbstickClicked(UnityEngine.InputSystem.Controls.ButtonControl)
extern void HPMixedRealityController_set_thumbstickClicked_mBD23695ADC31BE90C06BB4FBB3D6312B5AE1C111 (void);
// 0x00000082 UnityEngine.XR.OpenXR.Input.PoseControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_devicePose()
extern void HPMixedRealityController_get_devicePose_mB9812EAC9113309C1BB170281F204F650A9BB3D7 (void);
// 0x00000083 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_devicePose(UnityEngine.XR.OpenXR.Input.PoseControl)
extern void HPMixedRealityController_set_devicePose_m5AAD04EA80EB8AA9D7A3BA153100E4FE27CF398C (void);
// 0x00000084 UnityEngine.XR.OpenXR.Input.PoseControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_pointer()
extern void HPMixedRealityController_get_pointer_mB15F58738938B664E857DEAFAFB56F9F7107AE37 (void);
// 0x00000085 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_pointer(UnityEngine.XR.OpenXR.Input.PoseControl)
extern void HPMixedRealityController_set_pointer_m5DF533F4E9DE388EFFD1685DD20C62E3E2B2A788 (void);
// 0x00000086 UnityEngine.InputSystem.Controls.ButtonControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_isTracked()
extern void HPMixedRealityController_get_isTracked_mE4596D65C40CA4DC301A42F4542171D2AB5125BB (void);
// 0x00000087 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_isTracked(UnityEngine.InputSystem.Controls.ButtonControl)
extern void HPMixedRealityController_set_isTracked_mFC54CA8FCF0D4BAA79FC9B35B1657D5BDD3A0CEE (void);
// 0x00000088 UnityEngine.InputSystem.Controls.IntegerControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_trackingState()
extern void HPMixedRealityController_get_trackingState_mD0F91C0E747D2E5AB641158EDBA649EDD7B165F6 (void);
// 0x00000089 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_trackingState(UnityEngine.InputSystem.Controls.IntegerControl)
extern void HPMixedRealityController_set_trackingState_m68C28FEEE06A670F84E48915BBE6399288666BC6 (void);
// 0x0000008A UnityEngine.InputSystem.Controls.Vector3Control Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_devicePosition()
extern void HPMixedRealityController_get_devicePosition_m86F1D3626022C8E39AA93FA2E887213D1610E60A (void);
// 0x0000008B System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_devicePosition(UnityEngine.InputSystem.Controls.Vector3Control)
extern void HPMixedRealityController_set_devicePosition_m96EEEC1FE1C9AE8AE6BA997D5001D1D2339EB966 (void);
// 0x0000008C UnityEngine.InputSystem.Controls.QuaternionControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_deviceRotation()
extern void HPMixedRealityController_get_deviceRotation_m8F8FEA316AB79509C3BF0D02F8C93FEF79DF8739 (void);
// 0x0000008D System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_deviceRotation(UnityEngine.InputSystem.Controls.QuaternionControl)
extern void HPMixedRealityController_set_deviceRotation_mB5D6EF4EB14701A354460EA333EEC4BD5F3EFD3D (void);
// 0x0000008E UnityEngine.InputSystem.Controls.Vector3Control Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_pointerPosition()
extern void HPMixedRealityController_get_pointerPosition_m614DA404A0C3E3CFDB4243C498DAC07675983A7C (void);
// 0x0000008F System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_pointerPosition(UnityEngine.InputSystem.Controls.Vector3Control)
extern void HPMixedRealityController_set_pointerPosition_m609BC5B0A9AF67C9DEE2D964281DA46B24B68DF1 (void);
// 0x00000090 UnityEngine.InputSystem.Controls.QuaternionControl Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::get_pointerRotation()
extern void HPMixedRealityController_get_pointerRotation_mB741026CAF043D08566B333B1D027DC9DE384288 (void);
// 0x00000091 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::set_pointerRotation(UnityEngine.InputSystem.Controls.QuaternionControl)
extern void HPMixedRealityController_set_pointerRotation_m7B88ADB027DE71D240A1632C512689161382581A (void);
// 0x00000092 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::FinishSetup()
extern void HPMixedRealityController_FinishSetup_m3A7DD449BAA4ABB9B6C5C7F7E1AA1F750C454A87 (void);
// 0x00000093 System.Void Microsoft.MixedReality.OpenXR.HPMixedRealityControllerProfile/HPMixedRealityController::.ctor()
extern void HPMixedRealityController__ctor_m8B3EA47CB8DECAF5792C33FE4A48A684165DFE30 (void);
// 0x00000094 System.Void Microsoft.MixedReality.OpenXR.HandTrackingFeaturePlugin::.ctor()
extern void HandTrackingFeaturePlugin__ctor_mC990448A6B21FA41DF1ABF7307CEF5E2BCC61FFC (void);
// 0x00000095 System.Void Microsoft.MixedReality.OpenXR.MixedRealityFeaturePlugin::.ctor()
extern void MixedRealityFeaturePlugin__ctor_mE4DF397019D5D701EFC80676D5EAFEDCB8416868 (void);
// 0x00000096 System.IntPtr Microsoft.MixedReality.OpenXR.MixedRealityFeaturePlugin::TryAcquireSceneCoordinateSystem(UnityEngine.Pose)
extern void MixedRealityFeaturePlugin_TryAcquireSceneCoordinateSystem_m526B46BC5E2562411E74667BB4101E392641E3FB (void);
// 0x00000097 System.IntPtr Microsoft.MixedReality.OpenXR.MixedRealityFeaturePlugin::TryAcquirePerceptionSpatialAnchor(System.UInt64)
extern void MixedRealityFeaturePlugin_TryAcquirePerceptionSpatialAnchor_mD3BA702EA07920B6B22B51C6A38BBCD558D8FC54 (void);
// 0x00000098 System.IntPtr Microsoft.MixedReality.OpenXR.MixedRealityFeaturePlugin::TryAcquirePerceptionSpatialAnchor(System.Guid)
extern void MixedRealityFeaturePlugin_TryAcquirePerceptionSpatialAnchor_m425C589E4377409A8370BA2DCC8C1092364AD892 (void);
// 0x00000099 System.Guid Microsoft.MixedReality.OpenXR.MixedRealityFeaturePlugin::TryAcquireXrSpatialAnchor(System.Object)
extern void MixedRealityFeaturePlugin_TryAcquireXrSpatialAnchor_mF906CC3C60A7350DEEE125D6021F5652BC758066 (void);
// 0x0000009A System.Guid Microsoft.MixedReality.OpenXR.MixedRealityFeaturePlugin::TryAcquireAndReplaceXrSpatialAnchor(System.Object,System.Guid)
extern void MixedRealityFeaturePlugin_TryAcquireAndReplaceXrSpatialAnchor_mFD605654389498F2146ECB999D5C1AEB15A64043 (void);
// 0x0000009B System.Collections.Generic.IReadOnlyList`1<Microsoft.MixedReality.OpenXR.ViewConfiguration> Microsoft.MixedReality.OpenXR.MixedRealityFeaturePlugin::get_EnabledViewConfigurations()
extern void MixedRealityFeaturePlugin_get_EnabledViewConfigurations_mBA9E11DE0C241C898BB90B4D94EC55366D75EDF2 (void);
// 0x0000009C System.IntPtr Microsoft.MixedReality.OpenXR.MixedRealityFeaturePlugin::TryGetPerceptionDeviceFactory()
extern void MixedRealityFeaturePlugin_TryGetPerceptionDeviceFactory_m48E6123F96AA107372D904488D5B17B31DC80C81 (void);
// 0x0000009D System.Void Microsoft.MixedReality.OpenXR.MotionControllerFeaturePlugin::.ctor()
extern void MotionControllerFeaturePlugin__ctor_mE2FD8AC16CC77D825B49C03FF591BFE0C4E29C49 (void);
// 0x0000009E System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::get_Instance()
// 0x0000009F System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::set_Instance(System.UInt64)
// 0x000000A0 System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::get_SystemId()
// 0x000000A1 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::set_SystemId(System.UInt64)
// 0x000000A2 System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::get_Session()
// 0x000000A3 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::set_Session(System.UInt64)
// 0x000000A4 System.Boolean Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::get_IsSessionRunning()
// 0x000000A5 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::set_IsSessionRunning(System.Boolean)
// 0x000000A6 Microsoft.MixedReality.OpenXR.XrSessionState Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::get_SessionState()
// 0x000000A7 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::set_SessionState(Microsoft.MixedReality.OpenXR.XrSessionState)
// 0x000000A8 System.UInt64 Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::get_SceneOriginSpace()
// 0x000000A9 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::set_SceneOriginSpace(System.UInt64)
// 0x000000AA System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::add_InstanceCreated(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x000000AB System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::remove_InstanceCreated(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x000000AC System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::add_InstanceDestroying(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x000000AD System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::remove_InstanceDestroying(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x000000AE System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::add_SessionCreated(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x000000AF System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::remove_SessionCreated(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x000000B0 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::add_SessionDestroying(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x000000B1 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::remove_SessionDestroying(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x000000B2 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::add_SessionBegun(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x000000B3 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::remove_SessionBegun(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x000000B4 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::add_SessionEnding(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x000000B5 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::remove_SessionEnding(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x000000B6 System.Boolean Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::get_IsAnchorExtensionSupported()
// 0x000000B7 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::set_IsAnchorExtensionSupported(System.Boolean)
// 0x000000B8 System.IntPtr Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::GetInstanceProcAddr(System.String)
// 0x000000B9 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::.cctor()
// 0x000000BA System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::.ctor()
// 0x000000BB System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::AddSubsystemController(Microsoft.MixedReality.OpenXR.SubsystemController)
// 0x000000BC System.Boolean Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::IsExtensionEnabled(System.String,System.UInt32)
// 0x000000BD System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSubsystemCreate()
// 0x000000BE System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSubsystemStart()
// 0x000000BF System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSubsystemStop()
// 0x000000C0 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSubsystemDestroy()
// 0x000000C1 System.IntPtr Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::HookGetInstanceProcAddr(System.IntPtr)
// 0x000000C2 System.Boolean Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnInstanceCreate(System.UInt64)
// 0x000000C3 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnInstanceDestroy(System.UInt64)
// 0x000000C4 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSystemChange(System.UInt64)
// 0x000000C5 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSessionCreate(System.UInt64)
// 0x000000C6 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSessionBegin(System.UInt64)
// 0x000000C7 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSessionStateChange(System.Int32,System.Int32)
// 0x000000C8 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSessionEnd(System.UInt64)
// 0x000000C9 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnSessionDestroy(System.UInt64)
// 0x000000CA System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::OnAppSpaceChange(System.UInt64)
// 0x000000CB System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::Microsoft.MixedReality.OpenXR.ISubsystemPlugin.CreateSubsystem(System.Collections.Generic.List`1<TDescriptor>,System.String)
// 0x000000CC System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::Microsoft.MixedReality.OpenXR.ISubsystemPlugin.StartSubsystem()
// 0x000000CD System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::Microsoft.MixedReality.OpenXR.ISubsystemPlugin.StopSubsystem()
// 0x000000CE System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::Microsoft.MixedReality.OpenXR.ISubsystemPlugin.DestroySubsystem()
// 0x000000CF System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::<OnSubsystemCreate>b__53_0(Microsoft.MixedReality.OpenXR.SubsystemController)
// 0x000000D0 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::<OnSubsystemStart>b__54_0(Microsoft.MixedReality.OpenXR.SubsystemController)
// 0x000000D1 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::<OnSubsystemStop>b__55_0(Microsoft.MixedReality.OpenXR.SubsystemController)
// 0x000000D2 System.Void Microsoft.MixedReality.OpenXR.OpenXRFeaturePlugin`1::<OnSubsystemDestroy>b__56_0(Microsoft.MixedReality.OpenXR.SubsystemController)
// 0x000000D3 Microsoft.MixedReality.OpenXR.NativeLibToken Microsoft.MixedReality.OpenXR.NativeLibTokenAttribute::get_NativeLibToken()
extern void NativeLibTokenAttribute_get_NativeLibToken_m57CE08A7213A75DA6C4B47539F4F1E736CA615A0 (void);
// 0x000000D4 System.Void Microsoft.MixedReality.OpenXR.NativeLibTokenAttribute::set_NativeLibToken(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLibTokenAttribute_set_NativeLibToken_m501CDEDBA23097ED0E9E37A1B9F03CFA17327D4D (void);
// 0x000000D5 System.Void Microsoft.MixedReality.OpenXR.NativeLibTokenAttribute::.ctor()
extern void NativeLibTokenAttribute__ctor_m5CA35C7559CEC05476ADB1902160ECCD598A1179 (void);
// 0x000000D6 System.Void Microsoft.MixedReality.OpenXR.NativeLib::InitializeNativeLibToken(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_InitializeNativeLibToken_m95B64CD49FCA504971E565AFA8607A952B17D5A8 (void);
// 0x000000D7 System.IntPtr Microsoft.MixedReality.OpenXR.NativeLib::HookGetInstanceProcAddr(Microsoft.MixedReality.OpenXR.NativeLibToken,System.IntPtr)
extern void NativeLib_HookGetInstanceProcAddr_m014050781B3C6AECFA36B7AF58708816287A4D00 (void);
// 0x000000D8 System.IntPtr Microsoft.MixedReality.OpenXR.NativeLib::GetInstanceProcAddr(System.UInt64,System.IntPtr,System.String)
extern void NativeLib_GetInstanceProcAddr_mC6AF6F1B3B0041F53101EF7E364AEF2961A76ED3 (void);
// 0x000000D9 System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetXrInstance(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64)
extern void NativeLib_SetXrInstance_m56F296BEFBD06C4ECFC7DFEE5B9A0DE839278F4E (void);
// 0x000000DA System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetXrSystemId(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64)
extern void NativeLib_SetXrSystemId_m4D98B22D73D67FC92E83D03A2E764A1781307E6D (void);
// 0x000000DB System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetXrSession(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64)
extern void NativeLib_SetXrSession_mB354DBA9CBDDC2595E09E646B7C905764DAB5E6F (void);
// 0x000000DC System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetXrSessionRunning(Microsoft.MixedReality.OpenXR.NativeLibToken,System.Boolean)
extern void NativeLib_SetXrSessionRunning_mAC69C6AD0CC1C6EE07EE9409D7C3DD94F95634A4 (void);
// 0x000000DD System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetSessionState(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt32)
extern void NativeLib_SetSessionState_m36CE73AA3F81102626F0F708279F408BCDE79892 (void);
// 0x000000DE Microsoft.MixedReality.OpenXR.NativeSpaceLocationFlags Microsoft.MixedReality.OpenXR.NativeLib::GetViewTrackingFlags(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.ViewConfigurationType)
extern void NativeLib_GetViewTrackingFlags_m9F1D5723CFC404926837064EA45EADACECF3F981 (void);
// 0x000000DF System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetSceneOriginSpace(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64)
extern void NativeLib_SetSceneOriginSpace_m6055267107ECF8E22C86CB06BF98942F3048D58D (void);
// 0x000000E0 System.UInt32 Microsoft.MixedReality.OpenXR.NativeLib::GetEnabledViewConfigurationTypesCount(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_GetEnabledViewConfigurationTypesCount_m3A48C9628B673F01D1593026D795ACF997857E25 (void);
// 0x000000E1 System.Void Microsoft.MixedReality.OpenXR.NativeLib::GetEnabledViewConfigurationTypes(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.ViewConfigurationType[],System.UInt32)
extern void NativeLib_GetEnabledViewConfigurationTypes_m4D8E7F38590AC51748F845198790973FF02F8793 (void);
// 0x000000E2 System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::GetViewConfigurationIsActive(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.ViewConfigurationType)
extern void NativeLib_GetViewConfigurationIsActive_m98B6E3510F7B4DEB6306E4AF92BC1067FC2A14BA (void);
// 0x000000E3 System.UInt32 Microsoft.MixedReality.OpenXR.NativeLib::GetSupportedReprojectionModesCount(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.ViewConfigurationType)
extern void NativeLib_GetSupportedReprojectionModesCount_m437C6CB32CAB2F5EE27E8252466E3D4D7831A1B0 (void);
// 0x000000E4 System.Void Microsoft.MixedReality.OpenXR.NativeLib::GetSupportedReprojectionModes(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.ViewConfigurationType,Microsoft.MixedReality.OpenXR.ReprojectionMode[],System.UInt32)
extern void NativeLib_GetSupportedReprojectionModes_mA8E34E75C57627A4D960CC74328F64188997C1CC (void);
// 0x000000E5 System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetReprojectionSettings(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.ViewConfigurationType,Microsoft.MixedReality.OpenXR.NativeReprojectionSettings)
extern void NativeLib_SetReprojectionSettings_mFE8D8BD2A766BA6542F7394548FB043A5E323ED6 (void);
// 0x000000E6 System.Void Microsoft.MixedReality.OpenXR.NativeLib::UpdateTrackingStateFlags(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.FrameTime,Microsoft.MixedReality.OpenXR.NativeSpaceLocationFlags&)
extern void NativeLib_UpdateTrackingStateFlags_m10E936F1136FBAD04A16A827D20B2F2A447B5FFB (void);
// 0x000000E7 System.Void Microsoft.MixedReality.OpenXR.NativeLib::CreatePlaneProvider(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_CreatePlaneProvider_mE02C92A670997B74A2708DC0B88B8F1932A1CC32 (void);
// 0x000000E8 System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetPlanefindingActive(Microsoft.MixedReality.OpenXR.NativeLibToken,System.Boolean)
extern void NativeLib_SetPlanefindingActive_mFCE919BC9BF2B674507AD70F909C631D722D104D (void);
// 0x000000E9 System.Void Microsoft.MixedReality.OpenXR.NativeLib::SetPlaneDetectionMode(Microsoft.MixedReality.OpenXR.NativeLibToken,UnityEngine.XR.ARSubsystems.PlaneDetectionMode)
extern void NativeLib_SetPlaneDetectionMode_m1EE3FF20CCE788A5FA0F352C0567EF24D34BE5D7 (void);
// 0x000000EA System.Void Microsoft.MixedReality.OpenXR.NativeLib::GetNumPlaneChanges(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.FrameTime,System.UInt32&,System.UInt32&,System.UInt32&)
extern void NativeLib_GetNumPlaneChanges_m4DAF6C1A7F372CE0C2C9916CE92146B6DA186804 (void);
// 0x000000EB System.Void Microsoft.MixedReality.OpenXR.NativeLib::GetPlaneChanges(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt32,System.Void*,System.UInt32,System.Void*,System.UInt32,System.Void*)
extern void NativeLib_GetPlaneChanges_mBEFF78A01A640B7545F435DE5B5371B8A84ABAC1 (void);
// 0x000000EC System.Void Microsoft.MixedReality.OpenXR.NativeLib::CreateAnchorProvider(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_CreateAnchorProvider_m65713355CF40FDE1A9CEB7C1A0F9463BD8FB0A80 (void);
// 0x000000ED System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryAddAnchor(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.FrameTime,UnityEngine.Quaternion,UnityEngine.Vector3,System.Void*)
extern void NativeLib_TryAddAnchor_m7A02EAB918571B2EBCBF1AF2F08AECEE0086D249 (void);
// 0x000000EE System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryRemoveAnchor(Microsoft.MixedReality.OpenXR.NativeLibToken,System.Guid)
extern void NativeLib_TryRemoveAnchor_m64BC414FAE58334CCAE01449F007E0105C364687 (void);
// 0x000000EF System.Void Microsoft.MixedReality.OpenXR.NativeLib::GetNumAnchorChanges(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.FrameTime,System.UInt32&,System.UInt32&,System.UInt32&)
extern void NativeLib_GetNumAnchorChanges_mF8642CFB18ABFE72C914FC89A03862F32B6049FF (void);
// 0x000000F0 System.Void Microsoft.MixedReality.OpenXR.NativeLib::GetAnchorChanges(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt32,System.Void*,System.UInt32,System.Void*,System.UInt32,System.Void*)
extern void NativeLib_GetAnchorChanges_m46EC2948279185E0A462757D70331F0C05DEE4AD (void);
// 0x000000F1 System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::LoadAnchorStore(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_LoadAnchorStore_mE809A75C2097FF2C51E8F78EDDDD8063346CE896 (void);
// 0x000000F2 System.UInt32 Microsoft.MixedReality.OpenXR.NativeLib::GetNumPersistedAnchorNames(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_GetNumPersistedAnchorNames_m7B6DDE494EEDBB4AE84B592F36C19CE82D16963B (void);
// 0x000000F3 System.Void Microsoft.MixedReality.OpenXR.NativeLib::ClearAnchorChanges(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_ClearAnchorChanges_mECC65A85FAC927E5C4F4E279EAE2E669975DD32E (void);
// 0x000000F4 System.Void Microsoft.MixedReality.OpenXR.NativeLib::GetPersistedAnchorName(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt32,System.Text.StringBuilder,System.UInt32)
extern void NativeLib_GetPersistedAnchorName_m7941515895E8BB47DE6C09DBE4A6E759872EBADB (void);
// 0x000000F5 System.Guid Microsoft.MixedReality.OpenXR.NativeLib::LoadPersistedAnchor(Microsoft.MixedReality.OpenXR.NativeLibToken,System.String)
extern void NativeLib_LoadPersistedAnchor_mA033E41B34E4D277B6C5966FFA10AC6163C24A3E (void);
// 0x000000F6 System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryPersistAnchor(Microsoft.MixedReality.OpenXR.NativeLibToken,System.String,System.Guid)
extern void NativeLib_TryPersistAnchor_m7DCF7CD8558EF839F02A8B8F689041D79DBD9EF4 (void);
// 0x000000F7 System.Void Microsoft.MixedReality.OpenXR.NativeLib::UnpersistAnchor(Microsoft.MixedReality.OpenXR.NativeLibToken,System.String)
extern void NativeLib_UnpersistAnchor_m836793A43DC8645F9988EDFE62D52B8E18E1D675 (void);
// 0x000000F8 System.Void Microsoft.MixedReality.OpenXR.NativeLib::ClearPersistedAnchors(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_ClearPersistedAnchors_m9D96CAC561CA17209DBF8347426E71DC6AD8AAE6 (void);
// 0x000000F9 System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::GetHandJointData(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.Handedness,Microsoft.MixedReality.OpenXR.FrameTime,Microsoft.MixedReality.OpenXR.HandJointLocation[])
extern void NativeLib_GetHandJointData_m354808DAB133B95A99C8D40C637B4CBACDB96976 (void);
// 0x000000FA System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryLocateHandMesh(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.Handedness,Microsoft.MixedReality.OpenXR.FrameTime,Microsoft.MixedReality.OpenXR.HandPoseType,UnityEngine.Pose&)
extern void NativeLib_TryLocateHandMesh_mC4A15EEBF22A87AC6C9DD4C88186CFB66836F76F (void);
// 0x000000FB System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryGetHandMesh(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.Handedness,Microsoft.MixedReality.OpenXR.FrameTime,Microsoft.MixedReality.OpenXR.HandPoseType,System.UInt64&,System.UInt32&,UnityEngine.Vector3[],UnityEngine.Vector3[],System.UInt32&,System.UInt32&,System.Int32[])
extern void NativeLib_TryGetHandMesh_mE9F7909CC84728B14791155940B7DF915048FE77 (void);
// 0x000000FC System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryGetHandMeshBufferSizes(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt32&,System.UInt32&)
extern void NativeLib_TryGetHandMeshBufferSizes_m218EFF700CFF52137D054F4ACB524375307CB51A (void);
// 0x000000FD System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryGetControllerModelKey(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.Handedness,System.UInt64&)
extern void NativeLib_TryGetControllerModelKey_mB6AADB687417EDEE8BE43B006F56912D87988B2C (void);
// 0x000000FE System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryGetControllerModel(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64,System.UInt32,System.UInt32&,System.Byte[])
extern void NativeLib_TryGetControllerModel_mB2E26B63A21A651C4C76D7624E049C2B34920CE1 (void);
// 0x000000FF System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryEnableRemotingOverride(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_TryEnableRemotingOverride_m65CB80AF9275C6166E7CF957FF43E267D7874FAC (void);
// 0x00000100 System.Void Microsoft.MixedReality.OpenXR.NativeLib::ResetRemotingOverride(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_ResetRemotingOverride_mE24F338B6D5880C8657FD8777D190D5F82877A63 (void);
// 0x00000101 System.Void Microsoft.MixedReality.OpenXR.NativeLib::ConnectRemoting(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.Remoting.RemotingConfiguration)
extern void NativeLib_ConnectRemoting_m39937C886A80C20ECA5324C1FB3AFF79EE6E78B3 (void);
// 0x00000102 System.Void Microsoft.MixedReality.OpenXR.NativeLib::DisconnectRemoting(Microsoft.MixedReality.OpenXR.NativeLibToken)
extern void NativeLib_DisconnectRemoting_mC2777EFEA63CF351A913108D3E1180F08293AA8F (void);
// 0x00000103 System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryGetRemotingConnectionState(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.Remoting.ConnectionState&,Microsoft.MixedReality.OpenXR.Remoting.DisconnectReason&)
extern void NativeLib_TryGetRemotingConnectionState_mD80730C9C023457CDB6001CA85D3CF09EC73063E (void);
// 0x00000104 System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryCreateSpaceFromStaticNodeId(Microsoft.MixedReality.OpenXR.NativeLibToken,System.Guid,System.UInt64&)
extern void NativeLib_TryCreateSpaceFromStaticNodeId_mF611876071D20F71BC84C32340519E87B2666AC4 (void);
// 0x00000105 System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryLocateSpace(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64,Microsoft.MixedReality.OpenXR.FrameTime,UnityEngine.Pose&)
extern void NativeLib_TryLocateSpace_mE49EE1781877295070B02C6B682819762FEEAD23 (void);
// 0x00000106 System.IntPtr Microsoft.MixedReality.OpenXR.NativeLib::TryAcquireSceneCoordinateSystem(Microsoft.MixedReality.OpenXR.NativeLibToken,UnityEngine.Pose)
extern void NativeLib_TryAcquireSceneCoordinateSystem_m0BDE1BC81BABA47CEAF7687F27141A7D79255536 (void);
// 0x00000107 System.IntPtr Microsoft.MixedReality.OpenXR.NativeLib::TryAcquirePerceptionSpatialAnchor(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64)
extern void NativeLib_TryAcquirePerceptionSpatialAnchor_m5F1F63754F455B839E4D9B8391A60557D8FED57F (void);
// 0x00000108 System.IntPtr Microsoft.MixedReality.OpenXR.NativeLib::TryAcquirePerceptionSpatialAnchor(Microsoft.MixedReality.OpenXR.NativeLibToken,System.Guid)
extern void NativeLib_TryAcquirePerceptionSpatialAnchor_m1540155A34690080F9D3B1078A4F05D619622AA0 (void);
// 0x00000109 System.Guid Microsoft.MixedReality.OpenXR.NativeLib::TryAcquireXrSpatialAnchor(Microsoft.MixedReality.OpenXR.NativeLibToken,System.Object)
extern void NativeLib_TryAcquireXrSpatialAnchor_m3C4C9E7DAEB85690632A664533C8B84CEAB9612A (void);
// 0x0000010A System.Guid Microsoft.MixedReality.OpenXR.NativeLib::TryAcquireAndReplaceXrSpatialAnchor(Microsoft.MixedReality.OpenXR.NativeLibToken,System.Object,System.Guid)
extern void NativeLib_TryAcquireAndReplaceXrSpatialAnchor_mF5380602FFE451AF6056A7743AEFB273464328D2 (void);
// 0x0000010B System.IntPtr Microsoft.MixedReality.OpenXR.NativeLib::TryGetPerceptionDeviceFactory(Microsoft.MixedReality.OpenXR.NativeLibToken,System.IntPtr)
extern void NativeLib_TryGetPerceptionDeviceFactory_m01DF05249BBB3EBF7B9B85251A1858C4BFADA935 (void);
// 0x0000010C System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::SetMeshComputeSettings(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.MeshComputeSettings)
extern void NativeLib_SetMeshComputeSettings_mAF0952F81CEB81B77216F400DD62B0120C1B1C7A (void);
// 0x0000010D System.UInt64 Microsoft.MixedReality.OpenXR.NativeLib::TryCreateGestureRecognizer(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.GestureSettings)
extern void NativeLib_TryCreateGestureRecognizer_mDDFAF30B62007B11B0D926B738DEE258B046B897 (void);
// 0x0000010E System.Void Microsoft.MixedReality.OpenXR.NativeLib::DestroyGestureRecognizer(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64)
extern void NativeLib_DestroyGestureRecognizer_m937070B52A899C530C779EEC06FBA1014C321AA6 (void);
// 0x0000010F System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TrySetGestureSettings(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64,Microsoft.MixedReality.OpenXR.GestureSettings)
extern void NativeLib_TrySetGestureSettings_m87BB2AD07848D0DE2A73066AD56620D84E5CEA7D (void);
// 0x00000110 System.Void Microsoft.MixedReality.OpenXR.NativeLib::CancelPendingGesture(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64)
extern void NativeLib_CancelPendingGesture_m5A4A29141DDEE66CE1E782CEFBBF42AF113AF279 (void);
// 0x00000111 System.Boolean Microsoft.MixedReality.OpenXR.NativeLib::TryGetNextEventData(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64,Microsoft.MixedReality.OpenXR.GestureEventData&)
extern void NativeLib_TryGetNextEventData_m3F50391FAAC5022C9C51461FFAB0E7D10350888E (void);
// 0x00000112 System.Void Microsoft.MixedReality.OpenXR.NativeLib::StartGestureRecognizer(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64)
extern void NativeLib_StartGestureRecognizer_mFB59EDB0434246BF5E56EB0DCA80DA3C2AB6EED7 (void);
// 0x00000113 System.Void Microsoft.MixedReality.OpenXR.NativeLib::StopGestureRecognizer(Microsoft.MixedReality.OpenXR.NativeLibToken,System.UInt64)
extern void NativeLib_StopGestureRecognizer_m5B5529FB025D8C8A614C905A0B6E501CB5D5F3E9 (void);
// 0x00000114 System.Void Microsoft.MixedReality.OpenXR.NativeLib::.ctor()
extern void NativeLib__ctor_mD2E629D2C2360D0D308C76724D18BFF62F81B1E0 (void);
// 0x00000115 System.Threading.Tasks.Task`1<Microsoft.MixedReality.OpenXR.OpenXRAnchorStore> Microsoft.MixedReality.OpenXR.OpenXRAnchorStoreFactory::LoadAnchorStoreAsync(UnityEngine.XR.ARSubsystems.XRAnchorSubsystem)
extern void OpenXRAnchorStoreFactory_LoadAnchorStoreAsync_m13432ADA482D2B6EFC83F651F7F55068C7607B17 (void);
// 0x00000116 System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStoreFactory::.cctor()
extern void OpenXRAnchorStoreFactory__cctor_m727C58754B753EF8936C91AB48D378421D9A7582 (void);
// 0x00000117 System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStoreFactory/<>c::.cctor()
extern void U3CU3Ec__cctor_m03FFA4DF41CC27D826322AF7C85531C64E419063 (void);
// 0x00000118 System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStoreFactory/<>c::.ctor()
extern void U3CU3Ec__ctor_mDB7C14C9EE6DE58538B7E62D6E3D9FD1FF804249 (void);
// 0x00000119 Microsoft.MixedReality.OpenXR.OpenXRAnchorStore Microsoft.MixedReality.OpenXR.OpenXRAnchorStoreFactory/<>c::<LoadAnchorStoreAsync>b__1_0()
extern void U3CU3Ec_U3CLoadAnchorStoreAsyncU3Eb__1_0_mBEBBC4D37605E2622E7833DBEDA3677E94853F37 (void);
// 0x0000011A System.Collections.Generic.IReadOnlyList`1<System.String> Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::get_PersistedAnchorNames()
extern void OpenXRAnchorStore_get_PersistedAnchorNames_mC62F94AA2B2481F95F6B12337BF917CC48AC03AE (void);
// 0x0000011B System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::UpdatePersistedAnchorNames()
extern void OpenXRAnchorStore_UpdatePersistedAnchorNames_m675B5BA25A86275BAE549499C820D80D4DE2EC38 (void);
// 0x0000011C UnityEngine.XR.ARSubsystems.TrackableId Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::LoadAnchor(System.String)
extern void OpenXRAnchorStore_LoadAnchor_m11C63B944247868E332C8808AA4083044DF44A73 (void);
// 0x0000011D System.Boolean Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::TryPersistAnchor(System.String,UnityEngine.XR.ARSubsystems.TrackableId)
extern void OpenXRAnchorStore_TryPersistAnchor_m9B37D9AB24960E360F25DB579EF374BE9B5883BB (void);
// 0x0000011E System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::UnpersistAnchor(System.String)
extern void OpenXRAnchorStore_UnpersistAnchor_m3B9973616B71B2D7C3F4CDCD6EA6249188C2AB07 (void);
// 0x0000011F System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::Clear()
extern void OpenXRAnchorStore_Clear_mB4736A71C7201D5006B1B2F5362F7E2F20131D83 (void);
// 0x00000120 System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::.ctor()
extern void OpenXRAnchorStore__ctor_mF98180F589A55557643ECB2DDCB251E121A3D0D1 (void);
// 0x00000121 System.Void Microsoft.MixedReality.OpenXR.OpenXRAnchorStore::.cctor()
extern void OpenXRAnchorStore__cctor_mD08A110B96708BD31842AA4F8DECEDA14686C74F (void);
// 0x00000122 System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystem::RegisterDescriptor()
extern void AnchorSubsystem_RegisterDescriptor_mBD65BB68EAA4E6AE82802E23824D6F9E1EBE4E10 (void);
// 0x00000123 System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystem::.ctor()
extern void AnchorSubsystem__ctor_m79C1A8B9480CF7B2046BC0C42D0E2E60BC2C6C4A (void);
// 0x00000124 System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::.ctor()
extern void OpenXRProvider__ctor_m320E40A117BB2F4F74F64A78FA6C33DB4C8C1F69 (void);
// 0x00000125 System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::Start()
extern void OpenXRProvider_Start_m0906764494E97630228556CE04C7FDD468F60803 (void);
// 0x00000126 System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::Stop()
extern void OpenXRProvider_Stop_m8388CAD96D82F90A2309BB79DE78D4F612802D6D (void);
// 0x00000127 System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::Destroy()
extern void OpenXRProvider_Destroy_m5D33DF97FD911419FA58FA8C946404553AFE3E81 (void);
// 0x00000128 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRAnchor> Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::GetChanges(UnityEngine.XR.ARSubsystems.XRAnchor,Unity.Collections.Allocator)
extern void OpenXRProvider_GetChanges_mEB9FD5DDEFF416E4BE71FAC4A4D7325C32A988B2 (void);
// 0x00000129 UnityEngine.XR.ARSubsystems.XRAnchor Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::ToXRAnchor(Microsoft.MixedReality.OpenXR.NativeAnchor)
extern void OpenXRProvider_ToXRAnchor_m7D2B52E756D80AC0EB6227EEB70220200EFCE20A (void);
// 0x0000012A System.Boolean Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::TryAddAnchor(UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRAnchor&)
extern void OpenXRProvider_TryAddAnchor_m5638EB804FBBA5BB8CA0B159FE33BC463A649C33 (void);
// 0x0000012B System.Boolean Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::TryAttachAnchor(UnityEngine.XR.ARSubsystems.TrackableId,UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRAnchor&)
extern void OpenXRProvider_TryAttachAnchor_m8E174CF1958AD094521DF3F108C7D9F0A45A52F6 (void);
// 0x0000012C System.Boolean Microsoft.MixedReality.OpenXR.AnchorSubsystem/OpenXRProvider::TryRemoveAnchor(UnityEngine.XR.ARSubsystems.TrackableId)
extern void OpenXRProvider_TryRemoveAnchor_m91BAA699EF37A5336810BF6BB76A443CE7209006 (void);
// 0x0000012D System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystemController::.ctor(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.IOpenXRContext)
extern void AnchorSubsystemController__ctor_m856D19077A4132CB65B1D88EBAC844A2EB109F6F (void);
// 0x0000012E System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystemController::OnSubsystemCreate(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void AnchorSubsystemController_OnSubsystemCreate_m9DCA74481236B3FE604889E5617C6B14C21D96F1 (void);
// 0x0000012F System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystemController::OnSubsystemDestroy(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void AnchorSubsystemController_OnSubsystemDestroy_m1817C17AA33EC818F5A0FA2D7F7540258F41417E (void);
// 0x00000130 System.Void Microsoft.MixedReality.OpenXR.AnchorSubsystemController::.cctor()
extern void AnchorSubsystemController__cctor_m47E779CA5D295D21F6B4201547E3B7BBB7E2BA36 (void);
// 0x00000131 System.Collections.Generic.IReadOnlyList`1<System.String> Microsoft.MixedReality.OpenXR.AnchorTransferBatch::get_AnchorNames()
extern void AnchorTransferBatch_get_AnchorNames_mC58CB054FDB0EFCEEF62885EAD959A1AF42EA51E (void);
// 0x00000132 System.Boolean Microsoft.MixedReality.OpenXR.AnchorTransferBatch::AddAnchor(UnityEngine.XR.ARSubsystems.TrackableId,System.String)
extern void AnchorTransferBatch_AddAnchor_m5619A047080CFFBBF7C6B3BD7E0E5BA2E464E21B (void);
// 0x00000133 System.Void Microsoft.MixedReality.OpenXR.AnchorTransferBatch::RemoveAnchor(System.String)
extern void AnchorTransferBatch_RemoveAnchor_m2578CA368DDD8577CFDABB436D2F1786CF87A2B4 (void);
// 0x00000134 System.Void Microsoft.MixedReality.OpenXR.AnchorTransferBatch::Clear()
extern void AnchorTransferBatch_Clear_m69C667A04558F95A48D1ECC8C504D812B74C7D1D (void);
// 0x00000135 UnityEngine.XR.ARSubsystems.TrackableId Microsoft.MixedReality.OpenXR.AnchorTransferBatch::LoadAnchor(System.String)
extern void AnchorTransferBatch_LoadAnchor_m58BD2A49557F04DA571B704775F4B7DFB217D91B (void);
// 0x00000136 UnityEngine.XR.ARSubsystems.TrackableId Microsoft.MixedReality.OpenXR.AnchorTransferBatch::LoadAndReplaceAnchor(System.String,UnityEngine.XR.ARSubsystems.TrackableId)
extern void AnchorTransferBatch_LoadAndReplaceAnchor_mADED6E11BAE9CD97A6B57E893A1D309D661CB16C (void);
// 0x00000137 System.Threading.Tasks.Task`1<Microsoft.MixedReality.OpenXR.SerializationCompletionReason> Microsoft.MixedReality.OpenXR.AnchorTransferBatch::ExportAsync(System.IO.Stream)
extern void AnchorTransferBatch_ExportAsync_m17C11B0470DD1B50FE5E7C788F1AF8DD0E13928C (void);
// 0x00000138 System.Threading.Tasks.Task`1<Microsoft.MixedReality.OpenXR.SerializationCompletionReason> Microsoft.MixedReality.OpenXR.AnchorTransferBatch::ImportAsync(System.IO.Stream)
extern void AnchorTransferBatch_ImportAsync_m7DD8A6D54C89F7A5185353FF12752B0750AFDDA4 (void);
// 0x00000139 System.Void Microsoft.MixedReality.OpenXR.AnchorTransferBatch::.ctor()
extern void AnchorTransferBatch__ctor_mEBA6563C7821F3C146ED2DEF445ED633B1B6BECC (void);
// 0x0000013A System.Void Microsoft.MixedReality.OpenXR.AnchorTransferBatch/<ExportAsync>d__9::.ctor()
extern void U3CExportAsyncU3Ed__9__ctor_m8BC11F1A3B7AB99A6FD6B66CEA938A7BEB954F00 (void);
// 0x0000013B System.Void Microsoft.MixedReality.OpenXR.AnchorTransferBatch/<ExportAsync>d__9::MoveNext()
extern void U3CExportAsyncU3Ed__9_MoveNext_mCA5AEFE18FBF7240A00B819803E07D80BF551827 (void);
// 0x0000013C System.Void Microsoft.MixedReality.OpenXR.AnchorTransferBatch/<ExportAsync>d__9::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CExportAsyncU3Ed__9_SetStateMachine_m8FC94203F324281C70E023FC6E3316FBFE894997 (void);
// 0x0000013D System.Void Microsoft.MixedReality.OpenXR.AnchorTransferBatch/<ImportAsync>d__10::.ctor()
extern void U3CImportAsyncU3Ed__10__ctor_m982830AC3328E3954BFAD2053233A2E0C7C0B6F8 (void);
// 0x0000013E System.Void Microsoft.MixedReality.OpenXR.AnchorTransferBatch/<ImportAsync>d__10::MoveNext()
extern void U3CImportAsyncU3Ed__10_MoveNext_m0EEFFB14E3A5909F1EEBBA8314745FCD7ECEC612 (void);
// 0x0000013F System.Void Microsoft.MixedReality.OpenXR.AnchorTransferBatch/<ImportAsync>d__10::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CImportAsyncU3Ed__10_SetStateMachine_m9982859FB6DB2B117D72269D855BD735610EFE80 (void);
// 0x00000140 System.Boolean Microsoft.MixedReality.OpenXR.GestureSubsystemExtensions::IsValid(Microsoft.MixedReality.OpenXR.NativeSpaceLocationFlags)
extern void GestureSubsystemExtensions_IsValid_m9691161206FED2DA19B29BF375E0E016BC951935 (void);
// 0x00000141 System.Boolean Microsoft.MixedReality.OpenXR.GestureSubsystemExtensions::IsTracked(Microsoft.MixedReality.OpenXR.NativeSpaceLocationFlags)
extern void GestureSubsystemExtensions_IsTracked_mC1758A43809D63186D0807BD83BCA5E682D71356 (void);
// 0x00000142 System.Boolean Microsoft.MixedReality.OpenXR.GestureSubsystemExtensions::IsTappedEvent(Microsoft.MixedReality.OpenXR.NativeGestureEventData)
extern void GestureSubsystemExtensions_IsTappedEvent_mFEF294AF50F36CB9D1D0DB8928C18F237C1C40AC (void);
// 0x00000143 System.Boolean Microsoft.MixedReality.OpenXR.GestureSubsystemExtensions::IsManipulationEvent(Microsoft.MixedReality.OpenXR.NativeGestureEventData)
extern void GestureSubsystemExtensions_IsManipulationEvent_m4AC32D8B5C269FB14885D44015E3C128790088BD (void);
// 0x00000144 System.Boolean Microsoft.MixedReality.OpenXR.GestureSubsystemExtensions::IsNavigationEvent(Microsoft.MixedReality.OpenXR.NativeGestureEventData)
extern void GestureSubsystemExtensions_IsNavigationEvent_mAF0396B90643EC0883A804EA9C61944BC819C7C4 (void);
// 0x00000145 System.Nullable`1<T> Microsoft.MixedReality.OpenXR.GestureSubsystemExtensions::Get(Microsoft.MixedReality.OpenXR.NativeGestureEventData,T,System.Boolean)
// 0x00000146 Microsoft.MixedReality.OpenXR.GestureSubsystem Microsoft.MixedReality.OpenXR.GestureSubsystem::TryCreateGestureSubsystem(Microsoft.MixedReality.OpenXR.GestureSettings)
extern void GestureSubsystem_TryCreateGestureSubsystem_mD46A81BCF2C5E0B4187B0B563E5FD0674975E9F0 (void);
// 0x00000147 System.Void Microsoft.MixedReality.OpenXR.GestureSubsystem::.ctor(Microsoft.MixedReality.OpenXR.GestureSettings,System.UInt64)
extern void GestureSubsystem__ctor_m7295623E04942797B82CB778954CF2103DE65D5A (void);
// 0x00000148 Microsoft.MixedReality.OpenXR.GestureSettings Microsoft.MixedReality.OpenXR.GestureSubsystem::get_GestureSettings()
extern void GestureSubsystem_get_GestureSettings_m716EFFBC7BFDEEFCE1DE3B8F6F59C95693908969 (void);
// 0x00000149 System.Void Microsoft.MixedReality.OpenXR.GestureSubsystem::set_GestureSettings(Microsoft.MixedReality.OpenXR.GestureSettings)
extern void GestureSubsystem_set_GestureSettings_mF8D3E1701DB945E7C14EE3B0ACCDCCA34974DEA7 (void);
// 0x0000014A System.Boolean Microsoft.MixedReality.OpenXR.GestureSubsystem::TryGetNextEvent(Microsoft.MixedReality.OpenXR.GestureEventData&)
extern void GestureSubsystem_TryGetNextEvent_mA2EE17448AAA90179CBBC29A87AAB62FDBA85BDF (void);
// 0x0000014B System.Void Microsoft.MixedReality.OpenXR.GestureSubsystem::CancelPendingGestures()
extern void GestureSubsystem_CancelPendingGestures_m0FCD4AC92DFF672FECDC932478E4E5FF2ACC7F1C (void);
// 0x0000014C System.Void Microsoft.MixedReality.OpenXR.GestureSubsystem::DisposeNativeResources()
extern void GestureSubsystem_DisposeNativeResources_m0B0180B54BD91DD8B1A87BC5B4E603B26EE18D2E (void);
// 0x0000014D System.Void Microsoft.MixedReality.OpenXR.GestureSubsystem::Start()
extern void GestureSubsystem_Start_m0F3C3AA6BA33E5AB79B5B5135AB20DF8509C46F5 (void);
// 0x0000014E System.Void Microsoft.MixedReality.OpenXR.GestureSubsystem::Stop()
extern void GestureSubsystem_Stop_m2100BEB137FE278A7C0BAD1CCA8141043966A7BF (void);
// 0x0000014F System.Void Microsoft.MixedReality.OpenXR.GestureSubsystem::.cctor()
extern void GestureSubsystem__cctor_m41722869154081E5AE5A22E21BE87534C362FCE2 (void);
// 0x00000150 System.Void Microsoft.MixedReality.OpenXR.HandTrackingSubsystemController::.ctor(Microsoft.MixedReality.OpenXR.IOpenXRContext)
extern void HandTrackingSubsystemController__ctor_m4E1D8582021D757A9B1B33E28D7E1934E60E6EA1 (void);
// 0x00000151 System.Void Microsoft.MixedReality.OpenXR.HandTrackingSubsystemController::OnSubsystemCreate(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void HandTrackingSubsystemController_OnSubsystemCreate_m2868153E799AFE27F32637748B25BEEEBE319A24 (void);
// 0x00000152 System.Void Microsoft.MixedReality.OpenXR.HandTrackingSubsystemController::OnSubsystemStart(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void HandTrackingSubsystemController_OnSubsystemStart_mB402D47ADD7A83EEB861A4AE279556FB442A0DCC (void);
// 0x00000153 System.Void Microsoft.MixedReality.OpenXR.HandTrackingSubsystemController::OnSubsystemStop(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void HandTrackingSubsystemController_OnSubsystemStop_m61BE5A307DF628476CA119F572BD13C7A9C4B054 (void);
// 0x00000154 System.Void Microsoft.MixedReality.OpenXR.HandTrackingSubsystemController::OnSubsystemDestroy(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void HandTrackingSubsystemController_OnSubsystemDestroy_m84596CA219304A30E12D815FEFB5E5C14E05BE11 (void);
// 0x00000155 System.Boolean Microsoft.MixedReality.OpenXR.InternalMeshSettings::TrySetMeshComputeSettings(Microsoft.MixedReality.OpenXR.MeshComputeSettings)
extern void InternalMeshSettings_TrySetMeshComputeSettings_m70B6E59537B68E04B0D28421A2E714E2F3BA7B15 (void);
// 0x00000156 System.Void Microsoft.MixedReality.OpenXR.InternalMeshSettings::.cctor()
extern void InternalMeshSettings__cctor_m95F2C27D054867D581852D393DBA630BA852FB81 (void);
// 0x00000157 System.Void Microsoft.MixedReality.OpenXR.MeshSubsystemController::.ctor(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.IOpenXRContext)
extern void MeshSubsystemController__ctor_m0906F209708A61DE1562146663F8DCD1472A9F22 (void);
// 0x00000158 System.Void Microsoft.MixedReality.OpenXR.MeshSubsystemController::OnSubsystemCreate(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void MeshSubsystemController_OnSubsystemCreate_m0B93DAA6EAECCAA8C8EF3A4136F3A38ECF36CE35 (void);
// 0x00000159 System.Void Microsoft.MixedReality.OpenXR.MeshSubsystemController::OnSubsystemDestroy(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void MeshSubsystemController_OnSubsystemDestroy_mEDE001ECC4622EF144C28EF54D6886E0B961540F (void);
// 0x0000015A System.Void Microsoft.MixedReality.OpenXR.MeshSubsystemController::.cctor()
extern void MeshSubsystemController__cctor_m981E8B4D281F3C91F542960911C43056955F6F11 (void);
// 0x0000015B System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystem::RegisterDescriptor()
extern void PlaneSubsystem_RegisterDescriptor_m3314F823AA4B70D235AFF6CB146B5B955546C1B6 (void);
// 0x0000015C System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystem::.ctor()
extern void PlaneSubsystem__ctor_mF33005FD8256FAE7D7363EC18E522AC5803548B0 (void);
// 0x0000015D System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::.ctor()
extern void OpenXRProvider__ctor_m3508CC09D8F1A89A5370AF760EAD8B7BDCAEEE60 (void);
// 0x0000015E System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::Start()
extern void OpenXRProvider_Start_mC5CBA89048B971AEF3F0B0885B9330EA2365ADFF (void);
// 0x0000015F System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::Stop()
extern void OpenXRProvider_Stop_m414097D6423A7C4F3A0781F58DB7564D02FA1695 (void);
// 0x00000160 System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::Destroy()
extern void OpenXRProvider_Destroy_m918E7D7479468A3FA170CFC8F3EC96F7CF81F461 (void);
// 0x00000161 UnityEngine.XR.ARSubsystems.PlaneDetectionMode Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::get_currentPlaneDetectionMode()
extern void OpenXRProvider_get_currentPlaneDetectionMode_mFA24D594A1C95C9694E8497FDFA4C5BC72844A65 (void);
// 0x00000162 UnityEngine.XR.ARSubsystems.PlaneDetectionMode Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::get_requestedPlaneDetectionMode()
extern void OpenXRProvider_get_requestedPlaneDetectionMode_m29CD10B27F009A3DE07DD97D869A72AB5AA92DCE (void);
// 0x00000163 System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::set_requestedPlaneDetectionMode(UnityEngine.XR.ARSubsystems.PlaneDetectionMode)
extern void OpenXRProvider_set_requestedPlaneDetectionMode_m6C323400A80DBACBDA9A6396AE047AB8D29CC489 (void);
// 0x00000164 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.BoundedPlane> Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::GetChanges(UnityEngine.XR.ARSubsystems.BoundedPlane,Unity.Collections.Allocator)
extern void OpenXRProvider_GetChanges_m76E59D4759F116A69F936E1229D37137B556D47E (void);
// 0x00000165 UnityEngine.XR.ARSubsystems.PlaneClassification Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::ToPlaneClassification(Microsoft.MixedReality.OpenXR.XrSceneObjectTypeMSFT)
extern void OpenXRProvider_ToPlaneClassification_m06CA5A3DA65A0FF77C1316189DC3A552C32D4194 (void);
// 0x00000166 UnityEngine.XR.ARSubsystems.BoundedPlane Microsoft.MixedReality.OpenXR.PlaneSubsystem/OpenXRProvider::ToBoundedPlane(Microsoft.MixedReality.OpenXR.NativePlane,UnityEngine.XR.ARSubsystems.BoundedPlane)
extern void OpenXRProvider_ToBoundedPlane_mCC1FA51A768874220FF9A69DF459455463B32038 (void);
// 0x00000167 System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystemController::.ctor(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.IOpenXRContext)
extern void PlaneSubsystemController__ctor_mAC08A8AB104522E7E02C1E28CF5835A02DC1D012 (void);
// 0x00000168 System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystemController::OnSubsystemCreate(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void PlaneSubsystemController_OnSubsystemCreate_m2D2F0ACF33CBF9279A64D8CA42F52EE8F2E10EAD (void);
// 0x00000169 System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystemController::OnSubsystemDestroy(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void PlaneSubsystemController_OnSubsystemDestroy_mB714353472B9007B5952679461699F1D2A783137 (void);
// 0x0000016A System.Void Microsoft.MixedReality.OpenXR.PlaneSubsystemController::.cctor()
extern void PlaneSubsystemController__cctor_mAE65BE24BB2AB09E7E4FA404873D22FDDCCEE8E4 (void);
// 0x0000016B System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystem::RegisterDescriptor()
extern void RaycastSubsystem_RegisterDescriptor_mAB1B2445FDC9A24C2A02707A0541E5163A9F49BE (void);
// 0x0000016C System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystem::.ctor()
extern void RaycastSubsystem__ctor_mD38BE05D2798FAD12E567ED93FB1CD218F30143B (void);
// 0x0000016D System.Boolean Microsoft.MixedReality.OpenXR.RaycastSubsystem/OpenXRProvider::TryAddRaycast(UnityEngine.Vector2,System.Single,UnityEngine.XR.ARSubsystems.XRRaycast&)
extern void OpenXRProvider_TryAddRaycast_m476C9DA92D24B0CF42DCB0C279BD31E0C01370EE (void);
// 0x0000016E System.Boolean Microsoft.MixedReality.OpenXR.RaycastSubsystem/OpenXRProvider::TryAddRaycast(UnityEngine.Ray,System.Single,UnityEngine.XR.ARSubsystems.XRRaycast&)
extern void OpenXRProvider_TryAddRaycast_m4E836F2983588BB94219C713578BA3B5938F6701 (void);
// 0x0000016F System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystem/OpenXRProvider::RemoveRaycast(UnityEngine.XR.ARSubsystems.TrackableId)
extern void OpenXRProvider_RemoveRaycast_m1310AF4F07056AE265931FE27CA0E75D836428C7 (void);
// 0x00000170 System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystem/OpenXRProvider::.ctor()
extern void OpenXRProvider__ctor_mB67400E1AC09938D193E6CD23A0DAB9AF96EE3C1 (void);
// 0x00000171 System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystemController::.ctor(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.IOpenXRContext)
extern void RaycastSubsystemController__ctor_mF5E6072607F68136EF8B6E4405C72C2BB8788DA0 (void);
// 0x00000172 System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystemController::OnSubsystemCreate(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void RaycastSubsystemController_OnSubsystemCreate_m59946BE36679390B13E52DE27E54E7BADF669021 (void);
// 0x00000173 System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystemController::OnSubsystemDestroy(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void RaycastSubsystemController_OnSubsystemDestroy_m0EAAA158BBF2A0A606A6C70CBE6B3AADB0CEDB12 (void);
// 0x00000174 System.Void Microsoft.MixedReality.OpenXR.RaycastSubsystemController::.cctor()
extern void RaycastSubsystemController__cctor_m7269BFE6EA4B284E3F8BD47E48AC726D26852E91 (void);
// 0x00000175 System.Void Microsoft.MixedReality.OpenXR.SessionSubsystem::RegisterDescriptor()
extern void SessionSubsystem_RegisterDescriptor_mF8034E8963BB613C84F75F7C01499EE24E911A6D (void);
// 0x00000176 System.Void Microsoft.MixedReality.OpenXR.SessionSubsystem::.ctor()
extern void SessionSubsystem__ctor_m64A29F3A53D7F79826697290063F230071A4074D (void);
// 0x00000177 System.Void Microsoft.MixedReality.OpenXR.SessionSubsystem/OpenXRProvider::.ctor()
extern void OpenXRProvider__ctor_m2E52876AFC959D10AD3676FC6E66044A45932673 (void);
// 0x00000178 System.Void Microsoft.MixedReality.OpenXR.SessionSubsystem/OpenXRProvider::Start()
extern void OpenXRProvider_Start_m5BBC9BDE6316699686B116595DFBDB379CC7C498 (void);
// 0x00000179 System.Void Microsoft.MixedReality.OpenXR.SessionSubsystem/OpenXRProvider::Stop()
extern void OpenXRProvider_Stop_mD8B83AE92DD2A2E0F91015D230185C0E1139BABC (void);
// 0x0000017A System.Void Microsoft.MixedReality.OpenXR.SessionSubsystem/OpenXRProvider::Destroy()
extern void OpenXRProvider_Destroy_mE013EBAD1145783675B53F91D0638E5EE5745FCE (void);
// 0x0000017B UnityEngine.XR.ARSubsystems.Feature Microsoft.MixedReality.OpenXR.SessionSubsystem/OpenXRProvider::get_currentTrackingMode()
extern void OpenXRProvider_get_currentTrackingMode_m042296646D0444F103BA1E45B95345E9C73D33F7 (void);
// 0x0000017C UnityEngine.XR.ARSubsystems.Feature Microsoft.MixedReality.OpenXR.SessionSubsystem/OpenXRProvider::get_requestedTrackingMode()
extern void OpenXRProvider_get_requestedTrackingMode_mDA80308F27A4E5C646CD694030A50D5AC3AD8FCB (void);
// 0x0000017D System.Void Microsoft.MixedReality.OpenXR.SessionSubsystem/OpenXRProvider::set_requestedTrackingMode(UnityEngine.XR.ARSubsystems.Feature)
extern void OpenXRProvider_set_requestedTrackingMode_mE922C1FA3A93B0F12B13172B362898F850C89DAA (void);
// 0x0000017E UnityEngine.XR.ARSubsystems.TrackingState Microsoft.MixedReality.OpenXR.SessionSubsystem/OpenXRProvider::get_trackingState()
extern void OpenXRProvider_get_trackingState_mB8CEA029EC47280B780D4384BA2C63D56C467922 (void);
// 0x0000017F UnityEngine.XR.ARSubsystems.NotTrackingReason Microsoft.MixedReality.OpenXR.SessionSubsystem/OpenXRProvider::get_notTrackingReason()
extern void OpenXRProvider_get_notTrackingReason_mCEFE9C48FED09EF8D37635BB9D0712A118C4FD73 (void);
// 0x00000180 System.Boolean Microsoft.MixedReality.OpenXR.SessionSubsystem/OpenXRProvider::get_matchFrameRateEnabled()
extern void OpenXRProvider_get_matchFrameRateEnabled_m1587845114307FF03F57F454D49E090079FA8C37 (void);
// 0x00000181 System.Boolean Microsoft.MixedReality.OpenXR.SessionSubsystem/OpenXRProvider::get_matchFrameRateRequested()
extern void OpenXRProvider_get_matchFrameRateRequested_m1A2AAEE4629B98B0BDFA5B6CCDBA31BA74652918 (void);
// 0x00000182 System.Int32 Microsoft.MixedReality.OpenXR.SessionSubsystem/OpenXRProvider::get_frameRate()
extern void OpenXRProvider_get_frameRate_m900E1FF91C217440AD4BF071AD17E1092D0E5D09 (void);
// 0x00000183 UnityEngine.XR.ARSubsystems.Feature Microsoft.MixedReality.OpenXR.SessionSubsystem/OpenXRProvider::get_requestedFeatures()
extern void OpenXRProvider_get_requestedFeatures_mB0B6D477EDA42351D0B323D16DED0878A6C26752 (void);
// 0x00000184 System.IntPtr Microsoft.MixedReality.OpenXR.SessionSubsystem/OpenXRProvider::get_nativePtr()
extern void OpenXRProvider_get_nativePtr_mE47AEDD62ED6F6DCC7F449BB511B58695F13764F (void);
// 0x00000185 System.Guid Microsoft.MixedReality.OpenXR.SessionSubsystem/OpenXRProvider::get_sessionId()
extern void OpenXRProvider_get_sessionId_m2F03F67AD664F5B7582BC6B396498A2F56C7F7C5 (void);
// 0x00000186 UnityEngine.XR.ARSubsystems.Promise`1<UnityEngine.XR.ARSubsystems.SessionAvailability> Microsoft.MixedReality.OpenXR.SessionSubsystem/OpenXRProvider::GetAvailabilityAsync()
extern void OpenXRProvider_GetAvailabilityAsync_m3B218DFB5872EB3A9A1E45AA6051806CE8F63EA0 (void);
// 0x00000187 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.ConfigurationDescriptor> Microsoft.MixedReality.OpenXR.SessionSubsystem/OpenXRProvider::GetConfigurationDescriptors(Unity.Collections.Allocator)
extern void OpenXRProvider_GetConfigurationDescriptors_mABB9FA128F0DAE72E6462B070C8E75490D1DD1DA (void);
// 0x00000188 System.Void Microsoft.MixedReality.OpenXR.SessionSubsystem/OpenXRProvider::OnApplicationPause()
extern void OpenXRProvider_OnApplicationPause_m524070BF7FED4BF26539546303E86F170F7455EE (void);
// 0x00000189 System.Void Microsoft.MixedReality.OpenXR.SessionSubsystem/OpenXRProvider::OnApplicationResume()
extern void OpenXRProvider_OnApplicationResume_m5BC429823C3CD09BBF52E11F7310835CEF0C3E7A (void);
// 0x0000018A System.Void Microsoft.MixedReality.OpenXR.SessionSubsystem/OpenXRProvider::Update(UnityEngine.XR.ARSubsystems.XRSessionUpdateParams,UnityEngine.XR.ARSubsystems.Configuration)
extern void OpenXRProvider_Update_mFC980F55AE6F241600903A4C0A3006243A034093 (void);
// 0x0000018B System.Void Microsoft.MixedReality.OpenXR.SessionSubsystem/OpenXRProvider::Update(UnityEngine.XR.ARSubsystems.XRSessionUpdateParams)
extern void OpenXRProvider_Update_mE2EA18EF60A27E9639668C775E04C13DE86E8E17 (void);
// 0x0000018C System.Void Microsoft.MixedReality.OpenXR.SessionSubsystemController::.ctor(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.IOpenXRContext)
extern void SessionSubsystemController__ctor_mB3F6EE731BC858CB76EBDAF49EB7660495224E89 (void);
// 0x0000018D System.Void Microsoft.MixedReality.OpenXR.SessionSubsystemController::OnSubsystemCreate(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void SessionSubsystemController_OnSubsystemCreate_mA37B5ED9317830B739E2CB4BFB79B3EF21F2F9FA (void);
// 0x0000018E System.Void Microsoft.MixedReality.OpenXR.SessionSubsystemController::OnSubsystemDestroy(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void SessionSubsystemController_OnSubsystemDestroy_mDC4A794482776A59EB505E2DE11720215E7D8FC1 (void);
// 0x0000018F System.Void Microsoft.MixedReality.OpenXR.SessionSubsystemController::.cctor()
extern void SessionSubsystemController__cctor_m0A77687155BEE1FBBF63019ECE25E53AE05CEE56 (void);
// 0x00000190 System.Void Microsoft.MixedReality.OpenXR.OpenXRContextEvent::.ctor(System.Object,System.IntPtr)
extern void OpenXRContextEvent__ctor_mC1C02B7E7E7EB9C9805614BED3BE44C5663873B1 (void);
// 0x00000191 System.Void Microsoft.MixedReality.OpenXR.OpenXRContextEvent::Invoke(Microsoft.MixedReality.OpenXR.IOpenXRContext,System.EventArgs)
extern void OpenXRContextEvent_Invoke_mC45C0D5318BC60D400A19259BE5E241BC6CBDF63 (void);
// 0x00000192 System.IAsyncResult Microsoft.MixedReality.OpenXR.OpenXRContextEvent::BeginInvoke(Microsoft.MixedReality.OpenXR.IOpenXRContext,System.EventArgs,System.AsyncCallback,System.Object)
extern void OpenXRContextEvent_BeginInvoke_m662B3692E51183FD8AC53A7670C8AAD6429295E8 (void);
// 0x00000193 System.Void Microsoft.MixedReality.OpenXR.OpenXRContextEvent::EndInvoke(System.IAsyncResult)
extern void OpenXRContextEvent_EndInvoke_mC8997B60F1B6AC9D7F7A3E7DA9094C22023E2805 (void);
// 0x00000194 System.Void Microsoft.MixedReality.OpenXR.OpenXRContextEvent`1::.ctor(System.Object,System.IntPtr)
// 0x00000195 System.Void Microsoft.MixedReality.OpenXR.OpenXRContextEvent`1::Invoke(Microsoft.MixedReality.OpenXR.IOpenXRContext,TEventArgs)
// 0x00000196 System.IAsyncResult Microsoft.MixedReality.OpenXR.OpenXRContextEvent`1::BeginInvoke(Microsoft.MixedReality.OpenXR.IOpenXRContext,TEventArgs,System.AsyncCallback,System.Object)
// 0x00000197 System.Void Microsoft.MixedReality.OpenXR.OpenXRContextEvent`1::EndInvoke(System.IAsyncResult)
// 0x00000198 System.UInt64 Microsoft.MixedReality.OpenXR.IOpenXRContext::get_Instance()
// 0x00000199 System.UInt64 Microsoft.MixedReality.OpenXR.IOpenXRContext::get_SystemId()
// 0x0000019A System.UInt64 Microsoft.MixedReality.OpenXR.IOpenXRContext::get_Session()
// 0x0000019B System.Boolean Microsoft.MixedReality.OpenXR.IOpenXRContext::get_IsSessionRunning()
// 0x0000019C Microsoft.MixedReality.OpenXR.XrSessionState Microsoft.MixedReality.OpenXR.IOpenXRContext::get_SessionState()
// 0x0000019D System.UInt64 Microsoft.MixedReality.OpenXR.IOpenXRContext::get_SceneOriginSpace()
// 0x0000019E System.Void Microsoft.MixedReality.OpenXR.IOpenXRContext::add_InstanceCreated(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x0000019F System.Void Microsoft.MixedReality.OpenXR.IOpenXRContext::remove_InstanceCreated(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x000001A0 System.Void Microsoft.MixedReality.OpenXR.IOpenXRContext::add_InstanceDestroying(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x000001A1 System.Void Microsoft.MixedReality.OpenXR.IOpenXRContext::remove_InstanceDestroying(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x000001A2 System.Void Microsoft.MixedReality.OpenXR.IOpenXRContext::add_SessionCreated(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x000001A3 System.Void Microsoft.MixedReality.OpenXR.IOpenXRContext::remove_SessionCreated(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x000001A4 System.Void Microsoft.MixedReality.OpenXR.IOpenXRContext::add_SessionDestroying(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x000001A5 System.Void Microsoft.MixedReality.OpenXR.IOpenXRContext::remove_SessionDestroying(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x000001A6 System.Void Microsoft.MixedReality.OpenXR.IOpenXRContext::add_SessionBegun(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x000001A7 System.Void Microsoft.MixedReality.OpenXR.IOpenXRContext::remove_SessionBegun(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x000001A8 System.Void Microsoft.MixedReality.OpenXR.IOpenXRContext::add_SessionEnding(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x000001A9 System.Void Microsoft.MixedReality.OpenXR.IOpenXRContext::remove_SessionEnding(Microsoft.MixedReality.OpenXR.OpenXRContextEvent)
// 0x000001AA System.Boolean Microsoft.MixedReality.OpenXR.IOpenXRContext::get_IsAnchorExtensionSupported()
// 0x000001AB System.Void Microsoft.MixedReality.OpenXR.ISubsystemPlugin::CreateSubsystem(System.Collections.Generic.List`1<TDescriptor>,System.String)
// 0x000001AC System.Void Microsoft.MixedReality.OpenXR.ISubsystemPlugin::StartSubsystem()
// 0x000001AD System.Void Microsoft.MixedReality.OpenXR.ISubsystemPlugin::StopSubsystem()
// 0x000001AE System.Void Microsoft.MixedReality.OpenXR.ISubsystemPlugin::DestroySubsystem()
// 0x000001AF System.Void Microsoft.MixedReality.OpenXR.SubsystemController::.ctor(Microsoft.MixedReality.OpenXR.IOpenXRContext)
extern void SubsystemController__ctor_m8BF7D0090E9AEC0FE52AAC4518975DB1BEA7320D (void);
// 0x000001B0 System.Void Microsoft.MixedReality.OpenXR.SubsystemController::OnSubsystemCreate(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void SubsystemController_OnSubsystemCreate_m8FEAE45E0E2800DE900D9563307F38FBAFBCFC49 (void);
// 0x000001B1 System.Void Microsoft.MixedReality.OpenXR.SubsystemController::OnSubsystemStart(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void SubsystemController_OnSubsystemStart_mA7484E965CF101ABFB63B9FD12CE67C4AB8A713E (void);
// 0x000001B2 System.Void Microsoft.MixedReality.OpenXR.SubsystemController::OnSubsystemStop(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void SubsystemController_OnSubsystemStop_mE74E6F0F270B8AD20625AD22F75AFCB389F09997 (void);
// 0x000001B3 System.Void Microsoft.MixedReality.OpenXR.SubsystemController::OnSubsystemDestroy(Microsoft.MixedReality.OpenXR.ISubsystemPlugin)
extern void SubsystemController_OnSubsystemDestroy_m5E4A963D244F45C4DF1AE4F98C98F9FE9BC6101C (void);
// 0x000001B4 System.Void Microsoft.MixedReality.OpenXR.NativeReprojectionSettings::.ctor(Microsoft.MixedReality.OpenXR.ReprojectionSettings)
extern void NativeReprojectionSettings__ctor_mCA8BC7291C9A2C39AF6206C8EC6FFBEA9EFF09BB (void);
// 0x000001B5 Microsoft.MixedReality.OpenXR.ViewConfigurationType Microsoft.MixedReality.OpenXR.OpenXRViewConfiguration::get_ViewConfigurationType()
extern void OpenXRViewConfiguration_get_ViewConfigurationType_m42A4C2195819676513065AE2996984098F46BA96 (void);
// 0x000001B6 System.Boolean Microsoft.MixedReality.OpenXR.OpenXRViewConfiguration::HasTrackingFlags(Microsoft.MixedReality.OpenXR.NativeSpaceLocationFlags)
extern void OpenXRViewConfiguration_HasTrackingFlags_m847D81E3251B08B4AA1B79A380344E60524A41C5 (void);
// 0x000001B7 System.Boolean Microsoft.MixedReality.OpenXR.OpenXRViewConfiguration::get_IsActive()
extern void OpenXRViewConfiguration_get_IsActive_m09CEB1BACB76BECB6456DF44DB580AA400C860A1 (void);
// 0x000001B8 System.Collections.Generic.IReadOnlyList`1<Microsoft.MixedReality.OpenXR.ReprojectionMode> Microsoft.MixedReality.OpenXR.OpenXRViewConfiguration::get_SupportedReprojectionModes()
extern void OpenXRViewConfiguration_get_SupportedReprojectionModes_m710DF687727B7A9F65EC768D5C13FE01A2CA8001 (void);
// 0x000001B9 System.Void Microsoft.MixedReality.OpenXR.OpenXRViewConfiguration::.ctor(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.ViewConfigurationType)
extern void OpenXRViewConfiguration__ctor_m1595B1EBD9CE30C08D9E447009CD18B34509A1B7 (void);
// 0x000001BA System.Void Microsoft.MixedReality.OpenXR.OpenXRViewConfiguration::SetReprojectionSettings(Microsoft.MixedReality.OpenXR.ReprojectionSettings)
extern void OpenXRViewConfiguration_SetReprojectionSettings_m07A4F916B270A6DDBB55A1666A265ACDA1E9D608 (void);
// 0x000001BB System.Collections.Generic.IReadOnlyList`1<Microsoft.MixedReality.OpenXR.ViewConfiguration> Microsoft.MixedReality.OpenXR.OpenXRViewConfigurationSettings::get_EnabledViewConfigurations()
extern void OpenXRViewConfigurationSettings_get_EnabledViewConfigurations_m8FF2FBC3A21345AAE95BC37AFC4EF88C13F51199 (void);
// 0x000001BC System.Void Microsoft.MixedReality.OpenXR.OpenXRViewConfigurationSettings::.ctor(Microsoft.MixedReality.OpenXR.NativeLibToken,Microsoft.MixedReality.OpenXR.IOpenXRContext)
extern void OpenXRViewConfigurationSettings__ctor_m0FA11B908B422A6BE587A1B84B08872A7CEDFD30 (void);
// 0x000001BD System.Void Microsoft.MixedReality.OpenXR.OpenXRViewConfigurationSettings::Context_SessionBegun(Microsoft.MixedReality.OpenXR.IOpenXRContext,System.EventArgs)
extern void OpenXRViewConfigurationSettings_Context_SessionBegun_m3CCE3FDE4247DC7FC616B6A0B79348946029C6E7 (void);
// 0x000001BE System.Void Microsoft.MixedReality.OpenXR.OpenXRViewConfigurationSettings::Context_SessionEnding(Microsoft.MixedReality.OpenXR.IOpenXRContext,System.EventArgs)
extern void OpenXRViewConfigurationSettings_Context_SessionEnding_m58A86088800E6094C8FA0F809E1BBBC5C07572AD (void);
// 0x000001BF System.Boolean Microsoft.MixedReality.OpenXR.Disposable::get_disposedValue()
extern void Disposable_get_disposedValue_m37F9FE23B5F9C1C2F671F4CC0269513A6269162B (void);
// 0x000001C0 System.Void Microsoft.MixedReality.OpenXR.Disposable::set_disposedValue(System.Boolean)
extern void Disposable_set_disposedValue_mE989CA7460AF95CB60DF9987B70456A9E26C73C0 (void);
// 0x000001C1 System.Void Microsoft.MixedReality.OpenXR.Disposable::DisposeManagedResources()
extern void Disposable_DisposeManagedResources_m2514AC3BFE9FEA58F50B1CD59B9A9BA3E6C89BBD (void);
// 0x000001C2 System.Void Microsoft.MixedReality.OpenXR.Disposable::DisposeNativeResources()
extern void Disposable_DisposeNativeResources_mB94CC9934C3BA9F2A8EDEB96F563A1BB0A219B68 (void);
// 0x000001C3 System.Void Microsoft.MixedReality.OpenXR.Disposable::Dispose(System.Boolean)
extern void Disposable_Dispose_mCB8C81203700712721C670C72F34E8B72AE8B2CE (void);
// 0x000001C4 System.Void Microsoft.MixedReality.OpenXR.Disposable::Finalize()
extern void Disposable_Finalize_m63AFD6EC7F4EA73D7582AEABF5D46855AD2A46B0 (void);
// 0x000001C5 System.Void Microsoft.MixedReality.OpenXR.Disposable::Dispose()
extern void Disposable_Dispose_mF8B1F9E40AAA18072384E7D1C835BA8FC42E025D (void);
// 0x000001C6 System.Void Microsoft.MixedReality.OpenXR.Disposable::.ctor()
extern void Disposable__ctor_m2E997B3A6F6074D0E5D249D9D193D574A1F534C3 (void);
// 0x000001C7 UnityEngine.XR.ARSubsystems.TrackableId Microsoft.MixedReality.OpenXR.FeatureUtils::ToTrackableId(System.Guid)
extern void FeatureUtils_ToTrackableId_mD2613E700B947506164B78B23DE90A37E364C5AC (void);
// 0x000001C8 System.Guid Microsoft.MixedReality.OpenXR.FeatureUtils::ToGuid(UnityEngine.XR.ARSubsystems.TrackableId)
extern void FeatureUtils_ToGuid_mB6CBCCBFA13A358696DC536EC26CD926C4623A83 (void);
// 0x000001C9 Microsoft.MixedReality.OpenXR.Remoting.RemotingConfiguration Microsoft.MixedReality.OpenXR.Remoting.AppRemoting::get_Configuration()
extern void AppRemoting_get_Configuration_m59240B30012EADE83022C081DD4774BB533DAF6F (void);
// 0x000001CA System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemoting::set_Configuration(Microsoft.MixedReality.OpenXR.Remoting.RemotingConfiguration)
extern void AppRemoting_set_Configuration_mAB11BB7D4EFDB579EF386ADE450CC5BF5626B3AE (void);
// 0x000001CB System.Collections.IEnumerator Microsoft.MixedReality.OpenXR.Remoting.AppRemoting::Connect(Microsoft.MixedReality.OpenXR.Remoting.RemotingConfiguration)
extern void AppRemoting_Connect_m9ACC1D3D6279EE7418ABDB078808DDFF6DF4E7B5 (void);
// 0x000001CC System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemoting::Disconnect()
extern void AppRemoting_Disconnect_mD7E39E02BA208785AC0660A3D91D556E8AB01B2F (void);
// 0x000001CD System.Boolean Microsoft.MixedReality.OpenXR.Remoting.AppRemoting::TryGetConnectionState(Microsoft.MixedReality.OpenXR.Remoting.ConnectionState&,Microsoft.MixedReality.OpenXR.Remoting.DisconnectReason&)
extern void AppRemoting_TryGetConnectionState_m8BC3E5D2DBA821AE32FB42C19F889170F9AF3FD2 (void);
// 0x000001CE System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemoting/<Connect>d__4::.ctor(System.Int32)
extern void U3CConnectU3Ed__4__ctor_m16D0308BF5417A2EA916D4AB2EE78D90CD9B583C (void);
// 0x000001CF System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemoting/<Connect>d__4::System.IDisposable.Dispose()
extern void U3CConnectU3Ed__4_System_IDisposable_Dispose_mB273067A679C24652E766178067E64C011BC7AC7 (void);
// 0x000001D0 System.Boolean Microsoft.MixedReality.OpenXR.Remoting.AppRemoting/<Connect>d__4::MoveNext()
extern void U3CConnectU3Ed__4_MoveNext_mAFC7E644350828464269F84F1971A3C9C6692DA9 (void);
// 0x000001D1 System.Object Microsoft.MixedReality.OpenXR.Remoting.AppRemoting/<Connect>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CConnectU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m09B7F3861324A2F76E20EB4729EE0BBF44DDCFFB (void);
// 0x000001D2 System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemoting/<Connect>d__4::System.Collections.IEnumerator.Reset()
extern void U3CConnectU3Ed__4_System_Collections_IEnumerator_Reset_m521571FB27D480324916995DB19DBBF80569E601 (void);
// 0x000001D3 System.Object Microsoft.MixedReality.OpenXR.Remoting.AppRemoting/<Connect>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CConnectU3Ed__4_System_Collections_IEnumerator_get_Current_m6F556A2B18CB3FE77F0B1A165389882B3E7E746F (void);
// 0x000001D4 System.IntPtr Microsoft.MixedReality.OpenXR.Remoting.AppRemotingPlugin::HookGetInstanceProcAddr(System.IntPtr)
extern void AppRemotingPlugin_HookGetInstanceProcAddr_mB6E12E8BC5C841554F50CCBEA7DC27882C94FBA9 (void);
// 0x000001D5 System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemotingPlugin::OnInstanceDestroy(System.UInt64)
extern void AppRemotingPlugin_OnInstanceDestroy_m52D6674FA37CBC0C8D6B959B7D905C5EFD48E92B (void);
// 0x000001D6 System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemotingPlugin::OnSystemChange(System.UInt64)
extern void AppRemotingPlugin_OnSystemChange_m6B1BE3DD7548C1F132EA9AD08C30093B53DD81BE (void);
// 0x000001D7 System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemotingPlugin::OnSessionStateChange(System.Int32,System.Int32)
extern void AppRemotingPlugin_OnSessionStateChange_m60420AAE568EBBB8DFF1915C871DD9470BC3FC9F (void);
// 0x000001D8 System.Void Microsoft.MixedReality.OpenXR.Remoting.AppRemotingPlugin::.ctor()
extern void AppRemotingPlugin__ctor_mB1C6A73D50769B1B08B703FCB291C680250DEBFA (void);
// 0x000001D9 System.String Microsoft.MixedReality.OpenXR.Remoting.PlayModeRemotingPlugin::get_UserSettingsFolder()
extern void PlayModeRemotingPlugin_get_UserSettingsFolder_m4136476D4D190B2E5608DA63BD4D93B34CDFC605 (void);
// 0x000001DA System.String Microsoft.MixedReality.OpenXR.Remoting.PlayModeRemotingPlugin::get_SettingsAssetPath()
extern void PlayModeRemotingPlugin_get_SettingsAssetPath_mF436EE3130A88990D7F6C136FC7DBAEC732333BC (void);
// 0x000001DB System.IntPtr Microsoft.MixedReality.OpenXR.Remoting.PlayModeRemotingPlugin::HookGetInstanceProcAddr(System.IntPtr)
extern void PlayModeRemotingPlugin_HookGetInstanceProcAddr_mC2127730DA1905F9AF282B85CAD1A478C11FB3C4 (void);
// 0x000001DC System.Void Microsoft.MixedReality.OpenXR.Remoting.PlayModeRemotingPlugin::OnInstanceDestroy(System.UInt64)
extern void PlayModeRemotingPlugin_OnInstanceDestroy_mBA0E2B0CEA1ABE102237571A81CBF30DFB9EC3D1 (void);
// 0x000001DD System.Void Microsoft.MixedReality.OpenXR.Remoting.PlayModeRemotingPlugin::OnSystemChange(System.UInt64)
extern void PlayModeRemotingPlugin_OnSystemChange_mDE9CCBA0727E16DEADF94585757270FD286A7957 (void);
// 0x000001DE System.Void Microsoft.MixedReality.OpenXR.Remoting.PlayModeRemotingPlugin::OnSessionStateChange(System.Int32,System.Int32)
extern void PlayModeRemotingPlugin_OnSessionStateChange_m71DFBFD4EAFE416118A2168DEDAB7EAA069BBC3F (void);
// 0x000001DF System.Boolean Microsoft.MixedReality.OpenXR.Remoting.PlayModeRemotingPlugin::HasValidSettings()
extern void PlayModeRemotingPlugin_HasValidSettings_m0B684D9C3ADA0325A882745A9D74517928C1B6D1 (void);
// 0x000001E0 Microsoft.MixedReality.OpenXR.Remoting.RemotingSettings Microsoft.MixedReality.OpenXR.Remoting.PlayModeRemotingPlugin::get_RemotingSettings()
extern void PlayModeRemotingPlugin_get_RemotingSettings_m00C86AAAA9CC9108F80A5064201E17FC17B08C66 (void);
// 0x000001E1 System.Void Microsoft.MixedReality.OpenXR.Remoting.PlayModeRemotingPlugin::set_RemotingSettings(Microsoft.MixedReality.OpenXR.Remoting.RemotingSettings)
extern void PlayModeRemotingPlugin_set_RemotingSettings_mB071C68697DF94C234F5A0F5A922C828A880E1CE (void);
// 0x000001E2 System.Void Microsoft.MixedReality.OpenXR.Remoting.PlayModeRemotingPlugin::EnsureSettingsLoaded()
extern void PlayModeRemotingPlugin_EnsureSettingsLoaded_m45657D3C784DB188BBC35CEEDBF4764EDCA1E541 (void);
// 0x000001E3 System.Void Microsoft.MixedReality.OpenXR.Remoting.PlayModeRemotingPlugin::SaveSettings()
extern void PlayModeRemotingPlugin_SaveSettings_m4E5D8F27CC0009714BA40598BC8C95BA32EC0B11 (void);
// 0x000001E4 System.Void Microsoft.MixedReality.OpenXR.Remoting.PlayModeRemotingPlugin::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void PlayModeRemotingPlugin_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mDEA5C57DECA4274A4ADB6E2EA6DC3A0CEF220354 (void);
// 0x000001E5 System.Void Microsoft.MixedReality.OpenXR.Remoting.PlayModeRemotingPlugin::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void PlayModeRemotingPlugin_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_mE073C13A5D49E3B99714C0EF973ABE9E97C2742F (void);
// 0x000001E6 System.Void Microsoft.MixedReality.OpenXR.Remoting.PlayModeRemotingPlugin::.ctor()
extern void PlayModeRemotingPlugin__ctor_m4604CF9C2CCBFAF8E3B08F70AC437F440B072338 (void);
// 0x000001E7 System.String Microsoft.MixedReality.OpenXR.Remoting.RemotingSettings::get_RemoteHostName()
extern void RemotingSettings_get_RemoteHostName_m3FDF27FE05412EE4F2FF1548EF8D87F6A10053A3 (void);
// 0x000001E8 System.Void Microsoft.MixedReality.OpenXR.Remoting.RemotingSettings::set_RemoteHostName(System.String)
extern void RemotingSettings_set_RemoteHostName_m02696AA58795790D967DC14D3F5F6670AA276371 (void);
// 0x000001E9 System.UInt16 Microsoft.MixedReality.OpenXR.Remoting.RemotingSettings::get_RemoteHostPort()
extern void RemotingSettings_get_RemoteHostPort_m55353E3AE8CB30462EAA9E01C2FA266A000E8BF1 (void);
// 0x000001EA System.Void Microsoft.MixedReality.OpenXR.Remoting.RemotingSettings::set_RemoteHostPort(System.UInt16)
extern void RemotingSettings_set_RemoteHostPort_m095753ECC58C774D62EE79EDD0319638D36227D5 (void);
// 0x000001EB System.UInt32 Microsoft.MixedReality.OpenXR.Remoting.RemotingSettings::get_MaxBitrate()
extern void RemotingSettings_get_MaxBitrate_m29259A022F5E3E55A397EF977C3D4BEFD91D6843 (void);
// 0x000001EC System.Void Microsoft.MixedReality.OpenXR.Remoting.RemotingSettings::set_MaxBitrate(System.UInt32)
extern void RemotingSettings_set_MaxBitrate_m176B6B6A311CD4266F15772FBD6D514C441639D3 (void);
// 0x000001ED Microsoft.MixedReality.OpenXR.Remoting.RemotingVideoCodec Microsoft.MixedReality.OpenXR.Remoting.RemotingSettings::get_VideoCodec()
extern void RemotingSettings_get_VideoCodec_m15DF5F27E9D83A008F30BADACC90F05AD5AA55EE (void);
// 0x000001EE System.Void Microsoft.MixedReality.OpenXR.Remoting.RemotingSettings::set_VideoCodec(Microsoft.MixedReality.OpenXR.Remoting.RemotingVideoCodec)
extern void RemotingSettings_set_VideoCodec_mEE7FD91D12D6B0D04490B3F00B6D79C25BBF5817 (void);
// 0x000001EF System.Boolean Microsoft.MixedReality.OpenXR.Remoting.RemotingSettings::get_EnableAudio()
extern void RemotingSettings_get_EnableAudio_m10AF5B2CEB0F30D5E363C144431750F6112EB557 (void);
// 0x000001F0 System.Void Microsoft.MixedReality.OpenXR.Remoting.RemotingSettings::set_EnableAudio(System.Boolean)
extern void RemotingSettings_set_EnableAudio_mE213EA8D4047C6E82CA05D81143AE5661358A266 (void);
// 0x000001F1 System.Void Microsoft.MixedReality.OpenXR.Remoting.RemotingSettings::.ctor()
extern void RemotingSettings__ctor_mDBF434040E2D7E7F25EBA85028D51E2008594F3B (void);
// 0x000001F2 System.UInt64 Microsoft.MixedReality.OpenXR.ARFoundation.ARAnchorExtensions::GetOpenXRHandle(UnityEngine.XR.ARFoundation.ARAnchor)
extern void ARAnchorExtensions_GetOpenXRHandle_mCE7CE8E9D6C4051A6848790FCFB7282B835EACD2 (void);
// 0x000001F3 System.Threading.Tasks.Task`1<Microsoft.MixedReality.OpenXR.XRAnchorStore> Microsoft.MixedReality.OpenXR.ARFoundation.AnchorManagerExtensions::LoadAnchorStoreAsync(UnityEngine.XR.ARFoundation.ARAnchorManager)
extern void AnchorManagerExtensions_LoadAnchorStoreAsync_mFF975059F3FE5F0C08F5AA121784ED5BC775AE55 (void);
// 0x000001F4 System.UInt64 Microsoft.MixedReality.OpenXR.ARSubsystems.XRAnchorExtensions::GetOpenXRHandle(UnityEngine.XR.ARSubsystems.XRAnchor)
extern void XRAnchorExtensions_GetOpenXRHandle_m469AFFF1ABD3B6EB50E395DB616919937FDF7143 (void);
// 0x000001F5 System.Boolean Microsoft.MixedReality.OpenXR.ARSubsystems.MeshSubsystemExtensions::TrySetMeshComputeSettings(UnityEngine.XR.XRMeshSubsystem,Microsoft.MixedReality.OpenXR.MeshComputeSettings)
extern void MeshSubsystemExtensions_TrySetMeshComputeSettings_mAC55F5D74E3B6B768A5B9AE34995E2434F9EF834 (void);
// 0x000001F6 System.Threading.Tasks.Task`1<Microsoft.MixedReality.OpenXR.XRAnchorStore> Microsoft.MixedReality.OpenXR.ARSubsystems.AnchorSubsystemExtensions::LoadAnchorStoreAsync(UnityEngine.XR.ARSubsystems.XRAnchorSubsystem)
extern void AnchorSubsystemExtensions_LoadAnchorStoreAsync_mAD2B5E2AF9EB381AE2926E801BEBAC33D4BD5A48 (void);
static Il2CppMethodPointer s_methodPointers[502] = 
{
	AnchorConverter_ToOpenXRHandle_m57977A85F0D37A59886DF7893B987DD1254A49E5,
	AnchorConverter_ToPerceptionSpatialAnchor_mD4AAECBDD16942CA9D2854F0558664B3A9D4F649,
	AnchorConverter_ToPerceptionSpatialAnchor_mDA9208F94F7E57458A45B7ECE5A18B4FEECB1B85,
	AnchorConverter_FromPerceptionSpatialAnchor_m0637FE1467FBBE7683CCD3B85D29C0D4AE934594,
	AnchorConverter_ReplaceSpatialAnchor_mF0D803DBBEF3803A3061DBA12422F1B0F3275076,
	ControllerModel_get_Left_m2A3E61DAC18EE9F9E1828A053A4028437B93C248,
	ControllerModel_get_Right_m1894F74F10AD4AECDA206CEDF56290B1C2F8B62F,
	ControllerModel__ctor_mEE62B39AC6CB6F90AFE871970E93BEB51A1AFF9A,
	ControllerModel_TryGetControllerModelKey_m3F85931A47F56FF8AAEF420C38B802E31B08EAE3,
	ControllerModel_TryGetControllerModel_m8BD6F01D3DE95E2F02E42EB2A04C885722404971,
	ControllerModel__cctor_m4BC8AE2D60E49E5C222928E9CDCB90457003B57B,
	U3CU3Ec__DisplayClass13_0__ctor_m6E8D0FC9C3C011B46B4ABE02FC955075F8F4B7CC,
	U3CU3Ec__DisplayClass13_0_U3CTryGetControllerModelU3Eb__0_m490119CF2A3CE5379B03DC5E9CC21988E9D1B843,
	EyeLevelSceneOrigin_OnEnable_m466D14A8F8D036F5499B3BE921C022C4E7CBD872,
	EyeLevelSceneOrigin_OnDisable_m272DBAC8618818B98EE030DFE4693C2FF2192A33,
	EyeLevelSceneOrigin_XrInput_trackingOriginUpdated_m1671E7925D1390710E23A2ABCEF08755D6C3E996,
	EyeLevelSceneOrigin_EnsureSceneOriginAtEyeLevel_m012D81B67DF28B89A065DF84EE470C5C03AF272C,
	EyeLevelSceneOrigin_SetEyeLevelTrackingOriginMode_mE7BA1DB16D8E8D7515D63357A3D6C2EE61CEFC79,
	EyeLevelSceneOrigin__ctor_m8905D7EA3C7D43C7F3942610ACFF58E252DF2E11,
	GestureEventData_get_EventType_m88A55F7183D10CFDCC7E76F9177EEB0093252824,
	GestureEventData_get_Handedness_m98BC78EB77B9209D843E5A0A670926EA1CD6BBD0,
	GestureEventData_get_TappedData_mF12907AF8B7A81683ABBB5ED3D482FCDC87D6AE1,
	GestureEventData_get_ManipulationData_m10D386734CCEA909E0FF3E3EB046790B38E6791E,
	GestureEventData_get_NavigationData_m18D58022F182D64E1BAF6A1D37C10188EB4BBCF8,
	NavigationEventData_get_IsNavigatingX_m20EFA7621A684A1145CA1B769B386425021E1C2F,
	NavigationEventData_get_IsNavigatingY_m760BEDA5ED16D459CFECA95D90EFB10A5129F807,
	NavigationEventData_get_IsNavigatingZ_mAB4BB500BB8EA730BD60A31565D58859E900895A,
	GestureRecognizer__ctor_m2AC06A178237E25789FBDC0DC19EC6D906862D95,
	GestureRecognizer_get_GestureSettings_m5919DEAA3DE549B0C614AC242B5E2F620263DEE8,
	GestureRecognizer_set_GestureSettings_m006ED56582BD067E23D2814F378D8B19391D4836,
	GestureRecognizer_Start_m5DAC513272A992DCCD96134ED7C2F95071DC46F4,
	GestureRecognizer_Stop_m1905298F98D20231566B92F4D11924DA1E5D3781,
	GestureRecognizer_TryGetNextEvent_m1259E5C44C8E1F78B03017BECBD37AD4E3E1582A,
	GestureRecognizer_CancelPendingGestures_m454E85BCA6F1DC02C7E9004F1802F6B07607454F,
	GestureRecognizer_DisposeManagedResources_mC51CBD87F8A6EA8C28CAEE26712BF6FBEA1E4517,
	HandMeshTracker_get_Left_m7F701DFA8E6967020927A919F70DC3CBC3AD0A30,
	HandMeshTracker_get_Right_mF2125AE765BF5540B6F8A455EE3773BF90C602A0,
	HandMeshTracker__ctor_mBA6A4D33C1D7670D565D14AFC1E84DA4C9363E5D,
	HandMeshTracker_TryLocateHandMesh_m4C066C46D449B6D3E379BAE5B67EF1D951110976,
	HandMeshTracker_TryGetHandMesh_m12D7875710CA0D6E84E18A903313E331D60D50C8,
	HandMeshTracker__cctor_mDF10E2B657BDFA932979BF0A25994E5AB816C80A,
	HandTracker_get_Left_m6ABBCEB6043192B964B6BCD295222BF4B837868F,
	HandTracker_get_Right_mABE9DD4B81AC8FA6EBC72D3228DEC7B3AD9D7863,
	HandTracker__ctor_m3D77154AE26E5E6D27EA6DD95F62E01E20D5A3D7,
	HandTracker_TryLocateHandJoints_mB54C6CB63F2513A0EC2FCE33694C6989F2BB64EC,
	HandTracker__cctor_mE2018841AEA9DFC8F367AF896D979D1CDAB0A99C,
	HandJointLocation_get_IsTracked_m21F8ECE32FD2B263058F0ED2A74C0BE4A4F2F7F7,
	HandJointLocation_get_Pose_m4DE68CD498791989C2F1B3B77BE4095DEA4A45F6,
	HandJointLocation_get_Radius_m64C5A593D416786073BB54194A760A355A637AA9,
	MeshComputeSettings_get_MeshType_m9E560045929876C478863613388EB00DDFDF530E,
	MeshComputeSettings_set_MeshType_m34356D59AC315E911007B0170A2244584B02A1E3,
	MeshComputeSettings_get_VisualMeshLevelOfDetail_m9C4A5CE475233BF6C80EF15A170C728C532AD3CC,
	MeshComputeSettings_set_VisualMeshLevelOfDetail_m306CDDDE899A58AC03EC954FB2B223F45AE71157,
	MeshComputeSettings_get_MeshComputeConsistency_mED95E2D4FCAF5A1DE8F0E2C82B5B56E11D7BA878,
	MeshComputeSettings_set_MeshComputeConsistency_m2D0A6C5A790746581EF229D15944E0B8B78FBDFA,
	MeshSettings_TrySetMeshComputeSettings_m3C3A855F978DA3D525DEAD342C1FE2667120C3EF,
	OpenXRContext_get_Current_m9D565CE153060E1C4A7A29752AEED8DCC8ACE293,
	OpenXRContext_get_Instance_m175F38DD43376A21CDC502AF984D9FA57D1A9A05,
	OpenXRContext_get_SystemId_m4F8FB8858DB630ECE72D7988CD80E9F396B7018D,
	OpenXRContext_get_Session_m05C3CAE3B99D93180963F002BC41F14947358AA5,
	OpenXRContext_get_SceneOriginSpace_mAB9662C8329EFFED26193106C0D2D1F6C783488D,
	OpenXRContext_GetInstanceProcAddr_mF32DA1231AE49E1757544E1C0996D4273FE27BE6,
	OpenXRContext__ctor_mE2E62F2BE7C68614D9C1BE48478F099563BA1FC0,
	OpenXRContext__cctor_m5C68198775E43BD5A7425FCE12B532809E3FA62E,
	PerceptionInterop_GetSceneCoordinateSystem_m814779B4D45B7613A6FF8C8F544692A42378C6E0,
	PerceptionInterop__cctor_mDC04F9A390B39BB8902FEB6C58B8C9525E060645,
	SpatialGraphNode_FromStaticNodeId_m36EF16C1ED4C1DE619443C09A88BFAD855A7F118,
	SpatialGraphNode_get_Id_m0ADD258414D6FB14EF117EA022FF1D1C9EF43DAA,
	SpatialGraphNode_set_Id_mF565F99FC4FB8CC47BAC58EFEE820E8CBBE2B2C9,
	SpatialGraphNode_TryLocate_m1303DC576313BDB89974B56C3152F577ACB8A881,
	SpatialGraphNode__ctor_mFF6542891FB04E66B9C3613E4237CDEC2E49A0B3,
	ViewConfiguration_get_EnabledViewConfigurations_mDD45DF9719FA89E5B2898D4C5D93E44E7FE46DC7,
	ViewConfiguration__ctor_mC98910BB64A18EF97C707339C732D86B2FD5305B,
	ViewConfiguration_get_ViewConfigurationType_m2CCCF7ABF9EBE176E5E839CE040E251E51B394B3,
	ViewConfiguration_get_IsActive_m36150FA9963CEBF6B1628AEEA61E75716C8EBDE8,
	ViewConfiguration_get_SupportedReprojectionModes_m4A049171510E53D849274DDA6E4F1F70066F93D2,
	ViewConfiguration_SetReprojectionSettings_mB3CA9C8F4836C92F4DF7148F560511D43C81E567,
	ViewConfiguration_get_IsTracked_m807FD4FDEAB78B3D0A83FF6C7163A934159DAFA6,
	ViewConfiguration__cctor_mDBFDE3E0BFF7234BF307F6006281A0EB141949B4,
	ReprojectionSettings_get_ReprojectionMode_m7F36D9AAEDD1924B4F24EA0EAF0028455B00E577,
	ReprojectionSettings_set_ReprojectionMode_m9FD37CBAA6EF0FF3C5A5C5D58558E24862542292,
	XRAnchorStore_get_PersistedAnchorNames_m5BC2407ED032BEAAE255D39C1776F525C74B027E,
	XRAnchorStore_LoadAnchor_m35F16E71D5D1CD79354EDAEC1C56F22FE124EF41,
	XRAnchorStore_TryPersistAnchor_mE007CD067D0FD325C93B9B4B29921ACF12786C1C,
	XRAnchorStore_UnpersistAnchor_m2CDD559F8056F908A67F73844838AFC9DEF52BFB,
	XRAnchorStore_Clear_m1EC7F985022EF59EF217B6D083ECA8510EE733D7,
	XRAnchorStore_LoadAsync_m140C720CCE0F64BB4692226F25BC74943FDEBF9B,
	XRAnchorStore__ctor_m0CE578DC335F8682183E64C52DE75CFC09173EF7,
	U3CLoadAsyncU3Ed__6__ctor_m93FB7DBA5A043173929E4FA78F9EDE305B0D49B2,
	U3CLoadAsyncU3Ed__6_MoveNext_mE273186E26622B88F0AE268D843B6BFF9452BAB9,
	U3CLoadAsyncU3Ed__6_SetStateMachine_mFD7FD11F07375E809C056D42937AFCB1C9C0CEE9,
	XRAnchorTransferBatch__ctor_m7F04A6B9B5BA215663DA0E657CF1E7C06536411F,
	XRAnchorTransferBatch__ctor_m8615FB163C8584AF726FAC9B7A7C9C3B63E37E77,
	XRAnchorTransferBatch_get_AnchorNames_m8FEA9792C34E303694DC32AAD8CE51CC17479F8E,
	XRAnchorTransferBatch_AddAnchor_mCFDC30C9C3839837378293B50C6FA4B0CF8C5ADE,
	XRAnchorTransferBatch_RemoveAnchor_m48E9574BE7CCF7EECC610331D7E06A26DD2BBC47,
	XRAnchorTransferBatch_Clear_m53EDCD2D497B1D8C247998B882B5D234BFC977F1,
	XRAnchorTransferBatch_LoadAnchor_m5EDDDB48090153419966523B75C4D511386750B4,
	XRAnchorTransferBatch_LoadAndReplaceAnchor_mB04292E78E85053BEEB0C5A408D671A7E9DCAD98,
	XRAnchorTransferBatch_ExportAsync_m8778FE35361BBA2382A0C77E59EAABB6490CC345,
	XRAnchorTransferBatch_ImportAsync_m9D90925BC9D1A90EAC7F353F0162C2FFBD2A94A1,
	U3CExportAsyncU3Ed__10__ctor_mE813862D495E5E8ED943B4977C5B3B7BB226B0E6,
	U3CExportAsyncU3Ed__10_MoveNext_mE6C1EEEC49BB5D0626BE76D1DA0D4EA7B77B7134,
	U3CExportAsyncU3Ed__10_SetStateMachine_m4039FDD3FA60D54D84A6891420CDCF6E185DF63C,
	U3CImportAsyncU3Ed__11__ctor_mECD0015D303AD5591566D8C1D4F121D1674A4AA4,
	U3CImportAsyncU3Ed__11_MoveNext_m519F289E3957FF1B1D1646697D9B4E3C8CFD095C,
	U3CImportAsyncU3Ed__11_SetStateMachine_m908CF6DE008204CAD11D883A71E3B6E99D90943E,
	HPMixedRealityControllerProfile_RegisterDeviceLayout_m9451A9C2712715503B980648516413572AC10508,
	HPMixedRealityControllerProfile_UnregisterDeviceLayout_m744E74FD5D70711D8766E18D40A4EDC79FCFCA12,
	HPMixedRealityControllerProfile_RegisterActionMapsWithRuntime_mC53E75CB3BC74030D17997A8868B76C8E0B8B709,
	HPMixedRealityControllerProfile__ctor_mADA37F7C274FE24CF4E95295FA3B205E80AB5568,
	HPMixedRealityController_get_thumbstick_m2C0F38FC01ABFCB7AD18CAEFB497E1A58B5E0F3D,
	HPMixedRealityController_set_thumbstick_mF15870EA7CF9586CF3B2EE1AEFB9AC194CE0DD9A,
	HPMixedRealityController_get_grip_mB724A251071F8959D7A757B8123942D158B6679B,
	HPMixedRealityController_set_grip_m201A1C05844AE3ED591E773C49767CD11CDEEC2E,
	HPMixedRealityController_get_gripPressed_m3375FC51DD0439A9C716C58384A1BF4FD6DFFE62,
	HPMixedRealityController_set_gripPressed_mFB640265D90068D64206196B3B0920A57A2D5792,
	HPMixedRealityController_get_menu_m635552E39C1978802F4B595C0C4C49DCF0D15DD1,
	HPMixedRealityController_set_menu_m3CB5A5047D523E787E8A0A8E350F52D6659AB81A,
	HPMixedRealityController_get_primaryButton_m40DFE0108AA2AB547B384EB54F461ACDDFE928DD,
	HPMixedRealityController_set_primaryButton_mA99639AADA78F70833EF8A547ED27E9788A46C15,
	HPMixedRealityController_get_secondaryButton_m0CEC92E2A3DA6284CF2BA3A47F4B042FB2A1691F,
	HPMixedRealityController_set_secondaryButton_mFE5C25FC73EE85331F631AD4B5FAE21B046B022D,
	HPMixedRealityController_get_trigger_mE54BDE93DD5580AFBC805D435D9D2D0229EB3F30,
	HPMixedRealityController_set_trigger_m6D3E29454B07C891F15C12F0EF18DDB459B80767,
	HPMixedRealityController_get_triggerPressed_mE16F7D0426B5000F3A1CAA459036DADC3F0A2E26,
	HPMixedRealityController_set_triggerPressed_m5DF391FD4FED57840E6CEE38EA236FF3A2CEFF46,
	HPMixedRealityController_get_thumbstickClicked_m4737036F9F85F7D4ECB0F3FED80E571824247224,
	HPMixedRealityController_set_thumbstickClicked_mBD23695ADC31BE90C06BB4FBB3D6312B5AE1C111,
	HPMixedRealityController_get_devicePose_mB9812EAC9113309C1BB170281F204F650A9BB3D7,
	HPMixedRealityController_set_devicePose_m5AAD04EA80EB8AA9D7A3BA153100E4FE27CF398C,
	HPMixedRealityController_get_pointer_mB15F58738938B664E857DEAFAFB56F9F7107AE37,
	HPMixedRealityController_set_pointer_m5DF533F4E9DE388EFFD1685DD20C62E3E2B2A788,
	HPMixedRealityController_get_isTracked_mE4596D65C40CA4DC301A42F4542171D2AB5125BB,
	HPMixedRealityController_set_isTracked_mFC54CA8FCF0D4BAA79FC9B35B1657D5BDD3A0CEE,
	HPMixedRealityController_get_trackingState_mD0F91C0E747D2E5AB641158EDBA649EDD7B165F6,
	HPMixedRealityController_set_trackingState_m68C28FEEE06A670F84E48915BBE6399288666BC6,
	HPMixedRealityController_get_devicePosition_m86F1D3626022C8E39AA93FA2E887213D1610E60A,
	HPMixedRealityController_set_devicePosition_m96EEEC1FE1C9AE8AE6BA997D5001D1D2339EB966,
	HPMixedRealityController_get_deviceRotation_m8F8FEA316AB79509C3BF0D02F8C93FEF79DF8739,
	HPMixedRealityController_set_deviceRotation_mB5D6EF4EB14701A354460EA333EEC4BD5F3EFD3D,
	HPMixedRealityController_get_pointerPosition_m614DA404A0C3E3CFDB4243C498DAC07675983A7C,
	HPMixedRealityController_set_pointerPosition_m609BC5B0A9AF67C9DEE2D964281DA46B24B68DF1,
	HPMixedRealityController_get_pointerRotation_mB741026CAF043D08566B333B1D027DC9DE384288,
	HPMixedRealityController_set_pointerRotation_m7B88ADB027DE71D240A1632C512689161382581A,
	HPMixedRealityController_FinishSetup_m3A7DD449BAA4ABB9B6C5C7F7E1AA1F750C454A87,
	HPMixedRealityController__ctor_m8B3EA47CB8DECAF5792C33FE4A48A684165DFE30,
	HandTrackingFeaturePlugin__ctor_mC990448A6B21FA41DF1ABF7307CEF5E2BCC61FFC,
	MixedRealityFeaturePlugin__ctor_mE4DF397019D5D701EFC80676D5EAFEDCB8416868,
	MixedRealityFeaturePlugin_TryAcquireSceneCoordinateSystem_m526B46BC5E2562411E74667BB4101E392641E3FB,
	MixedRealityFeaturePlugin_TryAcquirePerceptionSpatialAnchor_mD3BA702EA07920B6B22B51C6A38BBCD558D8FC54,
	MixedRealityFeaturePlugin_TryAcquirePerceptionSpatialAnchor_m425C589E4377409A8370BA2DCC8C1092364AD892,
	MixedRealityFeaturePlugin_TryAcquireXrSpatialAnchor_mF906CC3C60A7350DEEE125D6021F5652BC758066,
	MixedRealityFeaturePlugin_TryAcquireAndReplaceXrSpatialAnchor_mFD605654389498F2146ECB999D5C1AEB15A64043,
	MixedRealityFeaturePlugin_get_EnabledViewConfigurations_mBA9E11DE0C241C898BB90B4D94EC55366D75EDF2,
	MixedRealityFeaturePlugin_TryGetPerceptionDeviceFactory_m48E6123F96AA107372D904488D5B17B31DC80C81,
	MotionControllerFeaturePlugin__ctor_mE2FD8AC16CC77D825B49C03FF591BFE0C4E29C49,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NativeLibTokenAttribute_get_NativeLibToken_m57CE08A7213A75DA6C4B47539F4F1E736CA615A0,
	NativeLibTokenAttribute_set_NativeLibToken_m501CDEDBA23097ED0E9E37A1B9F03CFA17327D4D,
	NativeLibTokenAttribute__ctor_m5CA35C7559CEC05476ADB1902160ECCD598A1179,
	NativeLib_InitializeNativeLibToken_m95B64CD49FCA504971E565AFA8607A952B17D5A8,
	NativeLib_HookGetInstanceProcAddr_m014050781B3C6AECFA36B7AF58708816287A4D00,
	NativeLib_GetInstanceProcAddr_mC6AF6F1B3B0041F53101EF7E364AEF2961A76ED3,
	NativeLib_SetXrInstance_m56F296BEFBD06C4ECFC7DFEE5B9A0DE839278F4E,
	NativeLib_SetXrSystemId_m4D98B22D73D67FC92E83D03A2E764A1781307E6D,
	NativeLib_SetXrSession_mB354DBA9CBDDC2595E09E646B7C905764DAB5E6F,
	NativeLib_SetXrSessionRunning_mAC69C6AD0CC1C6EE07EE9409D7C3DD94F95634A4,
	NativeLib_SetSessionState_m36CE73AA3F81102626F0F708279F408BCDE79892,
	NativeLib_GetViewTrackingFlags_m9F1D5723CFC404926837064EA45EADACECF3F981,
	NativeLib_SetSceneOriginSpace_m6055267107ECF8E22C86CB06BF98942F3048D58D,
	NativeLib_GetEnabledViewConfigurationTypesCount_m3A48C9628B673F01D1593026D795ACF997857E25,
	NativeLib_GetEnabledViewConfigurationTypes_m4D8E7F38590AC51748F845198790973FF02F8793,
	NativeLib_GetViewConfigurationIsActive_m98B6E3510F7B4DEB6306E4AF92BC1067FC2A14BA,
	NativeLib_GetSupportedReprojectionModesCount_m437C6CB32CAB2F5EE27E8252466E3D4D7831A1B0,
	NativeLib_GetSupportedReprojectionModes_mA8E34E75C57627A4D960CC74328F64188997C1CC,
	NativeLib_SetReprojectionSettings_mFE8D8BD2A766BA6542F7394548FB043A5E323ED6,
	NativeLib_UpdateTrackingStateFlags_m10E936F1136FBAD04A16A827D20B2F2A447B5FFB,
	NativeLib_CreatePlaneProvider_mE02C92A670997B74A2708DC0B88B8F1932A1CC32,
	NativeLib_SetPlanefindingActive_mFCE919BC9BF2B674507AD70F909C631D722D104D,
	NativeLib_SetPlaneDetectionMode_m1EE3FF20CCE788A5FA0F352C0567EF24D34BE5D7,
	NativeLib_GetNumPlaneChanges_m4DAF6C1A7F372CE0C2C9916CE92146B6DA186804,
	NativeLib_GetPlaneChanges_mBEFF78A01A640B7545F435DE5B5371B8A84ABAC1,
	NativeLib_CreateAnchorProvider_m65713355CF40FDE1A9CEB7C1A0F9463BD8FB0A80,
	NativeLib_TryAddAnchor_m7A02EAB918571B2EBCBF1AF2F08AECEE0086D249,
	NativeLib_TryRemoveAnchor_m64BC414FAE58334CCAE01449F007E0105C364687,
	NativeLib_GetNumAnchorChanges_mF8642CFB18ABFE72C914FC89A03862F32B6049FF,
	NativeLib_GetAnchorChanges_m46EC2948279185E0A462757D70331F0C05DEE4AD,
	NativeLib_LoadAnchorStore_mE809A75C2097FF2C51E8F78EDDDD8063346CE896,
	NativeLib_GetNumPersistedAnchorNames_m7B6DDE494EEDBB4AE84B592F36C19CE82D16963B,
	NativeLib_ClearAnchorChanges_mECC65A85FAC927E5C4F4E279EAE2E669975DD32E,
	NativeLib_GetPersistedAnchorName_m7941515895E8BB47DE6C09DBE4A6E759872EBADB,
	NativeLib_LoadPersistedAnchor_mA033E41B34E4D277B6C5966FFA10AC6163C24A3E,
	NativeLib_TryPersistAnchor_m7DCF7CD8558EF839F02A8B8F689041D79DBD9EF4,
	NativeLib_UnpersistAnchor_m836793A43DC8645F9988EDFE62D52B8E18E1D675,
	NativeLib_ClearPersistedAnchors_m9D96CAC561CA17209DBF8347426E71DC6AD8AAE6,
	NativeLib_GetHandJointData_m354808DAB133B95A99C8D40C637B4CBACDB96976,
	NativeLib_TryLocateHandMesh_mC4A15EEBF22A87AC6C9DD4C88186CFB66836F76F,
	NativeLib_TryGetHandMesh_mE9F7909CC84728B14791155940B7DF915048FE77,
	NativeLib_TryGetHandMeshBufferSizes_m218EFF700CFF52137D054F4ACB524375307CB51A,
	NativeLib_TryGetControllerModelKey_mB6AADB687417EDEE8BE43B006F56912D87988B2C,
	NativeLib_TryGetControllerModel_mB2E26B63A21A651C4C76D7624E049C2B34920CE1,
	NativeLib_TryEnableRemotingOverride_m65CB80AF9275C6166E7CF957FF43E267D7874FAC,
	NativeLib_ResetRemotingOverride_mE24F338B6D5880C8657FD8777D190D5F82877A63,
	NativeLib_ConnectRemoting_m39937C886A80C20ECA5324C1FB3AFF79EE6E78B3,
	NativeLib_DisconnectRemoting_mC2777EFEA63CF351A913108D3E1180F08293AA8F,
	NativeLib_TryGetRemotingConnectionState_mD80730C9C023457CDB6001CA85D3CF09EC73063E,
	NativeLib_TryCreateSpaceFromStaticNodeId_mF611876071D20F71BC84C32340519E87B2666AC4,
	NativeLib_TryLocateSpace_mE49EE1781877295070B02C6B682819762FEEAD23,
	NativeLib_TryAcquireSceneCoordinateSystem_m0BDE1BC81BABA47CEAF7687F27141A7D79255536,
	NativeLib_TryAcquirePerceptionSpatialAnchor_m5F1F63754F455B839E4D9B8391A60557D8FED57F,
	NativeLib_TryAcquirePerceptionSpatialAnchor_m1540155A34690080F9D3B1078A4F05D619622AA0,
	NativeLib_TryAcquireXrSpatialAnchor_m3C4C9E7DAEB85690632A664533C8B84CEAB9612A,
	NativeLib_TryAcquireAndReplaceXrSpatialAnchor_mF5380602FFE451AF6056A7743AEFB273464328D2,
	NativeLib_TryGetPerceptionDeviceFactory_m01DF05249BBB3EBF7B9B85251A1858C4BFADA935,
	NativeLib_SetMeshComputeSettings_mAF0952F81CEB81B77216F400DD62B0120C1B1C7A,
	NativeLib_TryCreateGestureRecognizer_mDDFAF30B62007B11B0D926B738DEE258B046B897,
	NativeLib_DestroyGestureRecognizer_m937070B52A899C530C779EEC06FBA1014C321AA6,
	NativeLib_TrySetGestureSettings_m87BB2AD07848D0DE2A73066AD56620D84E5CEA7D,
	NativeLib_CancelPendingGesture_m5A4A29141DDEE66CE1E782CEFBBF42AF113AF279,
	NativeLib_TryGetNextEventData_m3F50391FAAC5022C9C51461FFAB0E7D10350888E,
	NativeLib_StartGestureRecognizer_mFB59EDB0434246BF5E56EB0DCA80DA3C2AB6EED7,
	NativeLib_StopGestureRecognizer_m5B5529FB025D8C8A614C905A0B6E501CB5D5F3E9,
	NativeLib__ctor_mD2E629D2C2360D0D308C76724D18BFF62F81B1E0,
	OpenXRAnchorStoreFactory_LoadAnchorStoreAsync_m13432ADA482D2B6EFC83F651F7F55068C7607B17,
	OpenXRAnchorStoreFactory__cctor_m727C58754B753EF8936C91AB48D378421D9A7582,
	U3CU3Ec__cctor_m03FFA4DF41CC27D826322AF7C85531C64E419063,
	U3CU3Ec__ctor_mDB7C14C9EE6DE58538B7E62D6E3D9FD1FF804249,
	U3CU3Ec_U3CLoadAnchorStoreAsyncU3Eb__1_0_mBEBBC4D37605E2622E7833DBEDA3677E94853F37,
	OpenXRAnchorStore_get_PersistedAnchorNames_mC62F94AA2B2481F95F6B12337BF917CC48AC03AE,
	OpenXRAnchorStore_UpdatePersistedAnchorNames_m675B5BA25A86275BAE549499C820D80D4DE2EC38,
	OpenXRAnchorStore_LoadAnchor_m11C63B944247868E332C8808AA4083044DF44A73,
	OpenXRAnchorStore_TryPersistAnchor_m9B37D9AB24960E360F25DB579EF374BE9B5883BB,
	OpenXRAnchorStore_UnpersistAnchor_m3B9973616B71B2D7C3F4CDCD6EA6249188C2AB07,
	OpenXRAnchorStore_Clear_mB4736A71C7201D5006B1B2F5362F7E2F20131D83,
	OpenXRAnchorStore__ctor_mF98180F589A55557643ECB2DDCB251E121A3D0D1,
	OpenXRAnchorStore__cctor_mD08A110B96708BD31842AA4F8DECEDA14686C74F,
	AnchorSubsystem_RegisterDescriptor_mBD65BB68EAA4E6AE82802E23824D6F9E1EBE4E10,
	AnchorSubsystem__ctor_m79C1A8B9480CF7B2046BC0C42D0E2E60BC2C6C4A,
	OpenXRProvider__ctor_m320E40A117BB2F4F74F64A78FA6C33DB4C8C1F69,
	OpenXRProvider_Start_m0906764494E97630228556CE04C7FDD468F60803,
	OpenXRProvider_Stop_m8388CAD96D82F90A2309BB79DE78D4F612802D6D,
	OpenXRProvider_Destroy_m5D33DF97FD911419FA58FA8C946404553AFE3E81,
	OpenXRProvider_GetChanges_mEB9FD5DDEFF416E4BE71FAC4A4D7325C32A988B2,
	OpenXRProvider_ToXRAnchor_m7D2B52E756D80AC0EB6227EEB70220200EFCE20A,
	OpenXRProvider_TryAddAnchor_m5638EB804FBBA5BB8CA0B159FE33BC463A649C33,
	OpenXRProvider_TryAttachAnchor_m8E174CF1958AD094521DF3F108C7D9F0A45A52F6,
	OpenXRProvider_TryRemoveAnchor_m91BAA699EF37A5336810BF6BB76A443CE7209006,
	AnchorSubsystemController__ctor_m856D19077A4132CB65B1D88EBAC844A2EB109F6F,
	AnchorSubsystemController_OnSubsystemCreate_m9DCA74481236B3FE604889E5617C6B14C21D96F1,
	AnchorSubsystemController_OnSubsystemDestroy_m1817C17AA33EC818F5A0FA2D7F7540258F41417E,
	AnchorSubsystemController__cctor_m47E779CA5D295D21F6B4201547E3B7BBB7E2BA36,
	AnchorTransferBatch_get_AnchorNames_mC58CB054FDB0EFCEEF62885EAD959A1AF42EA51E,
	AnchorTransferBatch_AddAnchor_m5619A047080CFFBBF7C6B3BD7E0E5BA2E464E21B,
	AnchorTransferBatch_RemoveAnchor_m2578CA368DDD8577CFDABB436D2F1786CF87A2B4,
	AnchorTransferBatch_Clear_m69C667A04558F95A48D1ECC8C504D812B74C7D1D,
	AnchorTransferBatch_LoadAnchor_m58BD2A49557F04DA571B704775F4B7DFB217D91B,
	AnchorTransferBatch_LoadAndReplaceAnchor_mADED6E11BAE9CD97A6B57E893A1D309D661CB16C,
	AnchorTransferBatch_ExportAsync_m17C11B0470DD1B50FE5E7C788F1AF8DD0E13928C,
	AnchorTransferBatch_ImportAsync_m7DD8A6D54C89F7A5185353FF12752B0750AFDDA4,
	AnchorTransferBatch__ctor_mEBA6563C7821F3C146ED2DEF445ED633B1B6BECC,
	U3CExportAsyncU3Ed__9__ctor_m8BC11F1A3B7AB99A6FD6B66CEA938A7BEB954F00,
	U3CExportAsyncU3Ed__9_MoveNext_mCA5AEFE18FBF7240A00B819803E07D80BF551827,
	U3CExportAsyncU3Ed__9_SetStateMachine_m8FC94203F324281C70E023FC6E3316FBFE894997,
	U3CImportAsyncU3Ed__10__ctor_m982830AC3328E3954BFAD2053233A2E0C7C0B6F8,
	U3CImportAsyncU3Ed__10_MoveNext_m0EEFFB14E3A5909F1EEBBA8314745FCD7ECEC612,
	U3CImportAsyncU3Ed__10_SetStateMachine_m9982859FB6DB2B117D72269D855BD735610EFE80,
	GestureSubsystemExtensions_IsValid_m9691161206FED2DA19B29BF375E0E016BC951935,
	GestureSubsystemExtensions_IsTracked_mC1758A43809D63186D0807BD83BCA5E682D71356,
	GestureSubsystemExtensions_IsTappedEvent_mFEF294AF50F36CB9D1D0DB8928C18F237C1C40AC,
	GestureSubsystemExtensions_IsManipulationEvent_m4AC32D8B5C269FB14885D44015E3C128790088BD,
	GestureSubsystemExtensions_IsNavigationEvent_mAF0396B90643EC0883A804EA9C61944BC819C7C4,
	NULL,
	GestureSubsystem_TryCreateGestureSubsystem_mD46A81BCF2C5E0B4187B0B563E5FD0674975E9F0,
	GestureSubsystem__ctor_m7295623E04942797B82CB778954CF2103DE65D5A,
	GestureSubsystem_get_GestureSettings_m716EFFBC7BFDEEFCE1DE3B8F6F59C95693908969,
	GestureSubsystem_set_GestureSettings_mF8D3E1701DB945E7C14EE3B0ACCDCCA34974DEA7,
	GestureSubsystem_TryGetNextEvent_mA2EE17448AAA90179CBBC29A87AAB62FDBA85BDF,
	GestureSubsystem_CancelPendingGestures_m0FCD4AC92DFF672FECDC932478E4E5FF2ACC7F1C,
	GestureSubsystem_DisposeNativeResources_m0B0180B54BD91DD8B1A87BC5B4E603B26EE18D2E,
	GestureSubsystem_Start_m0F3C3AA6BA33E5AB79B5B5135AB20DF8509C46F5,
	GestureSubsystem_Stop_m2100BEB137FE278A7C0BAD1CCA8141043966A7BF,
	GestureSubsystem__cctor_m41722869154081E5AE5A22E21BE87534C362FCE2,
	HandTrackingSubsystemController__ctor_m4E1D8582021D757A9B1B33E28D7E1934E60E6EA1,
	HandTrackingSubsystemController_OnSubsystemCreate_m2868153E799AFE27F32637748B25BEEEBE319A24,
	HandTrackingSubsystemController_OnSubsystemStart_mB402D47ADD7A83EEB861A4AE279556FB442A0DCC,
	HandTrackingSubsystemController_OnSubsystemStop_m61BE5A307DF628476CA119F572BD13C7A9C4B054,
	HandTrackingSubsystemController_OnSubsystemDestroy_m84596CA219304A30E12D815FEFB5E5C14E05BE11,
	InternalMeshSettings_TrySetMeshComputeSettings_m70B6E59537B68E04B0D28421A2E714E2F3BA7B15,
	InternalMeshSettings__cctor_m95F2C27D054867D581852D393DBA630BA852FB81,
	MeshSubsystemController__ctor_m0906F209708A61DE1562146663F8DCD1472A9F22,
	MeshSubsystemController_OnSubsystemCreate_m0B93DAA6EAECCAA8C8EF3A4136F3A38ECF36CE35,
	MeshSubsystemController_OnSubsystemDestroy_mEDE001ECC4622EF144C28EF54D6886E0B961540F,
	MeshSubsystemController__cctor_m981E8B4D281F3C91F542960911C43056955F6F11,
	PlaneSubsystem_RegisterDescriptor_m3314F823AA4B70D235AFF6CB146B5B955546C1B6,
	PlaneSubsystem__ctor_mF33005FD8256FAE7D7363EC18E522AC5803548B0,
	OpenXRProvider__ctor_m3508CC09D8F1A89A5370AF760EAD8B7BDCAEEE60,
	OpenXRProvider_Start_mC5CBA89048B971AEF3F0B0885B9330EA2365ADFF,
	OpenXRProvider_Stop_m414097D6423A7C4F3A0781F58DB7564D02FA1695,
	OpenXRProvider_Destroy_m918E7D7479468A3FA170CFC8F3EC96F7CF81F461,
	OpenXRProvider_get_currentPlaneDetectionMode_mFA24D594A1C95C9694E8497FDFA4C5BC72844A65,
	OpenXRProvider_get_requestedPlaneDetectionMode_m29CD10B27F009A3DE07DD97D869A72AB5AA92DCE,
	OpenXRProvider_set_requestedPlaneDetectionMode_m6C323400A80DBACBDA9A6396AE047AB8D29CC489,
	OpenXRProvider_GetChanges_m76E59D4759F116A69F936E1229D37137B556D47E,
	OpenXRProvider_ToPlaneClassification_m06CA5A3DA65A0FF77C1316189DC3A552C32D4194,
	OpenXRProvider_ToBoundedPlane_mCC1FA51A768874220FF9A69DF459455463B32038,
	PlaneSubsystemController__ctor_mAC08A8AB104522E7E02C1E28CF5835A02DC1D012,
	PlaneSubsystemController_OnSubsystemCreate_m2D2F0ACF33CBF9279A64D8CA42F52EE8F2E10EAD,
	PlaneSubsystemController_OnSubsystemDestroy_mB714353472B9007B5952679461699F1D2A783137,
	PlaneSubsystemController__cctor_mAE65BE24BB2AB09E7E4FA404873D22FDDCCEE8E4,
	RaycastSubsystem_RegisterDescriptor_mAB1B2445FDC9A24C2A02707A0541E5163A9F49BE,
	RaycastSubsystem__ctor_mD38BE05D2798FAD12E567ED93FB1CD218F30143B,
	OpenXRProvider_TryAddRaycast_m476C9DA92D24B0CF42DCB0C279BD31E0C01370EE,
	OpenXRProvider_TryAddRaycast_m4E836F2983588BB94219C713578BA3B5938F6701,
	OpenXRProvider_RemoveRaycast_m1310AF4F07056AE265931FE27CA0E75D836428C7,
	OpenXRProvider__ctor_mB67400E1AC09938D193E6CD23A0DAB9AF96EE3C1,
	RaycastSubsystemController__ctor_mF5E6072607F68136EF8B6E4405C72C2BB8788DA0,
	RaycastSubsystemController_OnSubsystemCreate_m59946BE36679390B13E52DE27E54E7BADF669021,
	RaycastSubsystemController_OnSubsystemDestroy_m0EAAA158BBF2A0A606A6C70CBE6B3AADB0CEDB12,
	RaycastSubsystemController__cctor_m7269BFE6EA4B284E3F8BD47E48AC726D26852E91,
	SessionSubsystem_RegisterDescriptor_mF8034E8963BB613C84F75F7C01499EE24E911A6D,
	SessionSubsystem__ctor_m64A29F3A53D7F79826697290063F230071A4074D,
	OpenXRProvider__ctor_m2E52876AFC959D10AD3676FC6E66044A45932673,
	OpenXRProvider_Start_m5BBC9BDE6316699686B116595DFBDB379CC7C498,
	OpenXRProvider_Stop_mD8B83AE92DD2A2E0F91015D230185C0E1139BABC,
	OpenXRProvider_Destroy_mE013EBAD1145783675B53F91D0638E5EE5745FCE,
	OpenXRProvider_get_currentTrackingMode_m042296646D0444F103BA1E45B95345E9C73D33F7,
	OpenXRProvider_get_requestedTrackingMode_mDA80308F27A4E5C646CD694030A50D5AC3AD8FCB,
	OpenXRProvider_set_requestedTrackingMode_mE922C1FA3A93B0F12B13172B362898F850C89DAA,
	OpenXRProvider_get_trackingState_mB8CEA029EC47280B780D4384BA2C63D56C467922,
	OpenXRProvider_get_notTrackingReason_mCEFE9C48FED09EF8D37635BB9D0712A118C4FD73,
	OpenXRProvider_get_matchFrameRateEnabled_m1587845114307FF03F57F454D49E090079FA8C37,
	OpenXRProvider_get_matchFrameRateRequested_m1A2AAEE4629B98B0BDFA5B6CCDBA31BA74652918,
	OpenXRProvider_get_frameRate_m900E1FF91C217440AD4BF071AD17E1092D0E5D09,
	OpenXRProvider_get_requestedFeatures_mB0B6D477EDA42351D0B323D16DED0878A6C26752,
	OpenXRProvider_get_nativePtr_mE47AEDD62ED6F6DCC7F449BB511B58695F13764F,
	OpenXRProvider_get_sessionId_m2F03F67AD664F5B7582BC6B396498A2F56C7F7C5,
	OpenXRProvider_GetAvailabilityAsync_m3B218DFB5872EB3A9A1E45AA6051806CE8F63EA0,
	OpenXRProvider_GetConfigurationDescriptors_mABB9FA128F0DAE72E6462B070C8E75490D1DD1DA,
	OpenXRProvider_OnApplicationPause_m524070BF7FED4BF26539546303E86F170F7455EE,
	OpenXRProvider_OnApplicationResume_m5BC429823C3CD09BBF52E11F7310835CEF0C3E7A,
	OpenXRProvider_Update_mFC980F55AE6F241600903A4C0A3006243A034093,
	OpenXRProvider_Update_mE2EA18EF60A27E9639668C775E04C13DE86E8E17,
	SessionSubsystemController__ctor_mB3F6EE731BC858CB76EBDAF49EB7660495224E89,
	SessionSubsystemController_OnSubsystemCreate_mA37B5ED9317830B739E2CB4BFB79B3EF21F2F9FA,
	SessionSubsystemController_OnSubsystemDestroy_mDC4A794482776A59EB505E2DE11720215E7D8FC1,
	SessionSubsystemController__cctor_m0A77687155BEE1FBBF63019ECE25E53AE05CEE56,
	OpenXRContextEvent__ctor_mC1C02B7E7E7EB9C9805614BED3BE44C5663873B1,
	OpenXRContextEvent_Invoke_mC45C0D5318BC60D400A19259BE5E241BC6CBDF63,
	OpenXRContextEvent_BeginInvoke_m662B3692E51183FD8AC53A7670C8AAD6429295E8,
	OpenXRContextEvent_EndInvoke_mC8997B60F1B6AC9D7F7A3E7DA9094C22023E2805,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SubsystemController__ctor_m8BF7D0090E9AEC0FE52AAC4518975DB1BEA7320D,
	SubsystemController_OnSubsystemCreate_m8FEAE45E0E2800DE900D9563307F38FBAFBCFC49,
	SubsystemController_OnSubsystemStart_mA7484E965CF101ABFB63B9FD12CE67C4AB8A713E,
	SubsystemController_OnSubsystemStop_mE74E6F0F270B8AD20625AD22F75AFCB389F09997,
	SubsystemController_OnSubsystemDestroy_m5E4A963D244F45C4DF1AE4F98C98F9FE9BC6101C,
	NativeReprojectionSettings__ctor_mCA8BC7291C9A2C39AF6206C8EC6FFBEA9EFF09BB,
	OpenXRViewConfiguration_get_ViewConfigurationType_m42A4C2195819676513065AE2996984098F46BA96,
	OpenXRViewConfiguration_HasTrackingFlags_m847D81E3251B08B4AA1B79A380344E60524A41C5,
	OpenXRViewConfiguration_get_IsActive_m09CEB1BACB76BECB6456DF44DB580AA400C860A1,
	OpenXRViewConfiguration_get_SupportedReprojectionModes_m710DF687727B7A9F65EC768D5C13FE01A2CA8001,
	OpenXRViewConfiguration__ctor_m1595B1EBD9CE30C08D9E447009CD18B34509A1B7,
	OpenXRViewConfiguration_SetReprojectionSettings_m07A4F916B270A6DDBB55A1666A265ACDA1E9D608,
	OpenXRViewConfigurationSettings_get_EnabledViewConfigurations_m8FF2FBC3A21345AAE95BC37AFC4EF88C13F51199,
	OpenXRViewConfigurationSettings__ctor_m0FA11B908B422A6BE587A1B84B08872A7CEDFD30,
	OpenXRViewConfigurationSettings_Context_SessionBegun_m3CCE3FDE4247DC7FC616B6A0B79348946029C6E7,
	OpenXRViewConfigurationSettings_Context_SessionEnding_m58A86088800E6094C8FA0F809E1BBBC5C07572AD,
	Disposable_get_disposedValue_m37F9FE23B5F9C1C2F671F4CC0269513A6269162B,
	Disposable_set_disposedValue_mE989CA7460AF95CB60DF9987B70456A9E26C73C0,
	Disposable_DisposeManagedResources_m2514AC3BFE9FEA58F50B1CD59B9A9BA3E6C89BBD,
	Disposable_DisposeNativeResources_mB94CC9934C3BA9F2A8EDEB96F563A1BB0A219B68,
	Disposable_Dispose_mCB8C81203700712721C670C72F34E8B72AE8B2CE,
	Disposable_Finalize_m63AFD6EC7F4EA73D7582AEABF5D46855AD2A46B0,
	Disposable_Dispose_mF8B1F9E40AAA18072384E7D1C835BA8FC42E025D,
	Disposable__ctor_m2E997B3A6F6074D0E5D249D9D193D574A1F534C3,
	FeatureUtils_ToTrackableId_mD2613E700B947506164B78B23DE90A37E364C5AC,
	FeatureUtils_ToGuid_mB6CBCCBFA13A358696DC536EC26CD926C4623A83,
	AppRemoting_get_Configuration_m59240B30012EADE83022C081DD4774BB533DAF6F,
	AppRemoting_set_Configuration_mAB11BB7D4EFDB579EF386ADE450CC5BF5626B3AE,
	AppRemoting_Connect_m9ACC1D3D6279EE7418ABDB078808DDFF6DF4E7B5,
	AppRemoting_Disconnect_mD7E39E02BA208785AC0660A3D91D556E8AB01B2F,
	AppRemoting_TryGetConnectionState_m8BC3E5D2DBA821AE32FB42C19F889170F9AF3FD2,
	U3CConnectU3Ed__4__ctor_m16D0308BF5417A2EA916D4AB2EE78D90CD9B583C,
	U3CConnectU3Ed__4_System_IDisposable_Dispose_mB273067A679C24652E766178067E64C011BC7AC7,
	U3CConnectU3Ed__4_MoveNext_mAFC7E644350828464269F84F1971A3C9C6692DA9,
	U3CConnectU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m09B7F3861324A2F76E20EB4729EE0BBF44DDCFFB,
	U3CConnectU3Ed__4_System_Collections_IEnumerator_Reset_m521571FB27D480324916995DB19DBBF80569E601,
	U3CConnectU3Ed__4_System_Collections_IEnumerator_get_Current_m6F556A2B18CB3FE77F0B1A165389882B3E7E746F,
	AppRemotingPlugin_HookGetInstanceProcAddr_mB6E12E8BC5C841554F50CCBEA7DC27882C94FBA9,
	AppRemotingPlugin_OnInstanceDestroy_m52D6674FA37CBC0C8D6B959B7D905C5EFD48E92B,
	AppRemotingPlugin_OnSystemChange_m6B1BE3DD7548C1F132EA9AD08C30093B53DD81BE,
	AppRemotingPlugin_OnSessionStateChange_m60420AAE568EBBB8DFF1915C871DD9470BC3FC9F,
	AppRemotingPlugin__ctor_mB1C6A73D50769B1B08B703FCB291C680250DEBFA,
	PlayModeRemotingPlugin_get_UserSettingsFolder_m4136476D4D190B2E5608DA63BD4D93B34CDFC605,
	PlayModeRemotingPlugin_get_SettingsAssetPath_mF436EE3130A88990D7F6C136FC7DBAEC732333BC,
	PlayModeRemotingPlugin_HookGetInstanceProcAddr_mC2127730DA1905F9AF282B85CAD1A478C11FB3C4,
	PlayModeRemotingPlugin_OnInstanceDestroy_mBA0E2B0CEA1ABE102237571A81CBF30DFB9EC3D1,
	PlayModeRemotingPlugin_OnSystemChange_mDE9CCBA0727E16DEADF94585757270FD286A7957,
	PlayModeRemotingPlugin_OnSessionStateChange_m71DFBFD4EAFE416118A2168DEDAB7EAA069BBC3F,
	PlayModeRemotingPlugin_HasValidSettings_m0B684D9C3ADA0325A882745A9D74517928C1B6D1,
	PlayModeRemotingPlugin_get_RemotingSettings_m00C86AAAA9CC9108F80A5064201E17FC17B08C66,
	PlayModeRemotingPlugin_set_RemotingSettings_mB071C68697DF94C234F5A0F5A922C828A880E1CE,
	PlayModeRemotingPlugin_EnsureSettingsLoaded_m45657D3C784DB188BBC35CEEDBF4764EDCA1E541,
	PlayModeRemotingPlugin_SaveSettings_m4E5D8F27CC0009714BA40598BC8C95BA32EC0B11,
	PlayModeRemotingPlugin_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mDEA5C57DECA4274A4ADB6E2EA6DC3A0CEF220354,
	PlayModeRemotingPlugin_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_mE073C13A5D49E3B99714C0EF973ABE9E97C2742F,
	PlayModeRemotingPlugin__ctor_m4604CF9C2CCBFAF8E3B08F70AC437F440B072338,
	RemotingSettings_get_RemoteHostName_m3FDF27FE05412EE4F2FF1548EF8D87F6A10053A3,
	RemotingSettings_set_RemoteHostName_m02696AA58795790D967DC14D3F5F6670AA276371,
	RemotingSettings_get_RemoteHostPort_m55353E3AE8CB30462EAA9E01C2FA266A000E8BF1,
	RemotingSettings_set_RemoteHostPort_m095753ECC58C774D62EE79EDD0319638D36227D5,
	RemotingSettings_get_MaxBitrate_m29259A022F5E3E55A397EF977C3D4BEFD91D6843,
	RemotingSettings_set_MaxBitrate_m176B6B6A311CD4266F15772FBD6D514C441639D3,
	RemotingSettings_get_VideoCodec_m15DF5F27E9D83A008F30BADACC90F05AD5AA55EE,
	RemotingSettings_set_VideoCodec_mEE7FD91D12D6B0D04490B3F00B6D79C25BBF5817,
	RemotingSettings_get_EnableAudio_m10AF5B2CEB0F30D5E363C144431750F6112EB557,
	RemotingSettings_set_EnableAudio_mE213EA8D4047C6E82CA05D81143AE5661358A266,
	RemotingSettings__ctor_mDBF434040E2D7E7F25EBA85028D51E2008594F3B,
	ARAnchorExtensions_GetOpenXRHandle_mCE7CE8E9D6C4051A6848790FCFB7282B835EACD2,
	AnchorManagerExtensions_LoadAnchorStoreAsync_mFF975059F3FE5F0C08F5AA121784ED5BC775AE55,
	XRAnchorExtensions_GetOpenXRHandle_m469AFFF1ABD3B6EB50E395DB616919937FDF7143,
	MeshSubsystemExtensions_TrySetMeshComputeSettings_mAC55F5D74E3B6B768A5B9AE34995E2434F9EF834,
	AnchorSubsystemExtensions_LoadAnchorStoreAsync_mAD2B5E2AF9EB381AE2926E801BEBAC33D4BD5A48,
};
extern void GestureEventData_get_EventType_m88A55F7183D10CFDCC7E76F9177EEB0093252824_AdjustorThunk (void);
extern void GestureEventData_get_Handedness_m98BC78EB77B9209D843E5A0A670926EA1CD6BBD0_AdjustorThunk (void);
extern void GestureEventData_get_TappedData_mF12907AF8B7A81683ABBB5ED3D482FCDC87D6AE1_AdjustorThunk (void);
extern void GestureEventData_get_ManipulationData_m10D386734CCEA909E0FF3E3EB046790B38E6791E_AdjustorThunk (void);
extern void GestureEventData_get_NavigationData_m18D58022F182D64E1BAF6A1D37C10188EB4BBCF8_AdjustorThunk (void);
extern void NavigationEventData_get_IsNavigatingX_m20EFA7621A684A1145CA1B769B386425021E1C2F_AdjustorThunk (void);
extern void NavigationEventData_get_IsNavigatingY_m760BEDA5ED16D459CFECA95D90EFB10A5129F807_AdjustorThunk (void);
extern void NavigationEventData_get_IsNavigatingZ_mAB4BB500BB8EA730BD60A31565D58859E900895A_AdjustorThunk (void);
extern void HandJointLocation_get_IsTracked_m21F8ECE32FD2B263058F0ED2A74C0BE4A4F2F7F7_AdjustorThunk (void);
extern void HandJointLocation_get_Pose_m4DE68CD498791989C2F1B3B77BE4095DEA4A45F6_AdjustorThunk (void);
extern void HandJointLocation_get_Radius_m64C5A593D416786073BB54194A760A355A637AA9_AdjustorThunk (void);
extern void MeshComputeSettings_get_MeshType_m9E560045929876C478863613388EB00DDFDF530E_AdjustorThunk (void);
extern void MeshComputeSettings_set_MeshType_m34356D59AC315E911007B0170A2244584B02A1E3_AdjustorThunk (void);
extern void MeshComputeSettings_get_VisualMeshLevelOfDetail_m9C4A5CE475233BF6C80EF15A170C728C532AD3CC_AdjustorThunk (void);
extern void MeshComputeSettings_set_VisualMeshLevelOfDetail_m306CDDDE899A58AC03EC954FB2B223F45AE71157_AdjustorThunk (void);
extern void MeshComputeSettings_get_MeshComputeConsistency_mED95E2D4FCAF5A1DE8F0E2C82B5B56E11D7BA878_AdjustorThunk (void);
extern void MeshComputeSettings_set_MeshComputeConsistency_m2D0A6C5A790746581EF229D15944E0B8B78FBDFA_AdjustorThunk (void);
extern void ViewConfiguration__ctor_mC98910BB64A18EF97C707339C732D86B2FD5305B_AdjustorThunk (void);
extern void ViewConfiguration_get_ViewConfigurationType_m2CCCF7ABF9EBE176E5E839CE040E251E51B394B3_AdjustorThunk (void);
extern void ViewConfiguration_get_IsActive_m36150FA9963CEBF6B1628AEEA61E75716C8EBDE8_AdjustorThunk (void);
extern void ViewConfiguration_get_SupportedReprojectionModes_m4A049171510E53D849274DDA6E4F1F70066F93D2_AdjustorThunk (void);
extern void ViewConfiguration_SetReprojectionSettings_mB3CA9C8F4836C92F4DF7148F560511D43C81E567_AdjustorThunk (void);
extern void ViewConfiguration_get_IsTracked_m807FD4FDEAB78B3D0A83FF6C7163A934159DAFA6_AdjustorThunk (void);
extern void ReprojectionSettings_get_ReprojectionMode_m7F36D9AAEDD1924B4F24EA0EAF0028455B00E577_AdjustorThunk (void);
extern void ReprojectionSettings_set_ReprojectionMode_m9FD37CBAA6EF0FF3C5A5C5D58558E24862542292_AdjustorThunk (void);
extern void NativeReprojectionSettings__ctor_mCA8BC7291C9A2C39AF6206C8EC6FFBEA9EFF09BB_AdjustorThunk (void);
extern void OpenXRViewConfiguration_get_ViewConfigurationType_m42A4C2195819676513065AE2996984098F46BA96_AdjustorThunk (void);
extern void OpenXRViewConfiguration_HasTrackingFlags_m847D81E3251B08B4AA1B79A380344E60524A41C5_AdjustorThunk (void);
extern void OpenXRViewConfiguration_get_IsActive_m09CEB1BACB76BECB6456DF44DB580AA400C860A1_AdjustorThunk (void);
extern void OpenXRViewConfiguration_get_SupportedReprojectionModes_m710DF687727B7A9F65EC768D5C13FE01A2CA8001_AdjustorThunk (void);
extern void OpenXRViewConfiguration__ctor_m1595B1EBD9CE30C08D9E447009CD18B34509A1B7_AdjustorThunk (void);
extern void OpenXRViewConfiguration_SetReprojectionSettings_m07A4F916B270A6DDBB55A1666A265ACDA1E9D608_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[32] = 
{
	{ 0x06000014, GestureEventData_get_EventType_m88A55F7183D10CFDCC7E76F9177EEB0093252824_AdjustorThunk },
	{ 0x06000015, GestureEventData_get_Handedness_m98BC78EB77B9209D843E5A0A670926EA1CD6BBD0_AdjustorThunk },
	{ 0x06000016, GestureEventData_get_TappedData_mF12907AF8B7A81683ABBB5ED3D482FCDC87D6AE1_AdjustorThunk },
	{ 0x06000017, GestureEventData_get_ManipulationData_m10D386734CCEA909E0FF3E3EB046790B38E6791E_AdjustorThunk },
	{ 0x06000018, GestureEventData_get_NavigationData_m18D58022F182D64E1BAF6A1D37C10188EB4BBCF8_AdjustorThunk },
	{ 0x06000019, NavigationEventData_get_IsNavigatingX_m20EFA7621A684A1145CA1B769B386425021E1C2F_AdjustorThunk },
	{ 0x0600001A, NavigationEventData_get_IsNavigatingY_m760BEDA5ED16D459CFECA95D90EFB10A5129F807_AdjustorThunk },
	{ 0x0600001B, NavigationEventData_get_IsNavigatingZ_mAB4BB500BB8EA730BD60A31565D58859E900895A_AdjustorThunk },
	{ 0x0600002F, HandJointLocation_get_IsTracked_m21F8ECE32FD2B263058F0ED2A74C0BE4A4F2F7F7_AdjustorThunk },
	{ 0x06000030, HandJointLocation_get_Pose_m4DE68CD498791989C2F1B3B77BE4095DEA4A45F6_AdjustorThunk },
	{ 0x06000031, HandJointLocation_get_Radius_m64C5A593D416786073BB54194A760A355A637AA9_AdjustorThunk },
	{ 0x06000032, MeshComputeSettings_get_MeshType_m9E560045929876C478863613388EB00DDFDF530E_AdjustorThunk },
	{ 0x06000033, MeshComputeSettings_set_MeshType_m34356D59AC315E911007B0170A2244584B02A1E3_AdjustorThunk },
	{ 0x06000034, MeshComputeSettings_get_VisualMeshLevelOfDetail_m9C4A5CE475233BF6C80EF15A170C728C532AD3CC_AdjustorThunk },
	{ 0x06000035, MeshComputeSettings_set_VisualMeshLevelOfDetail_m306CDDDE899A58AC03EC954FB2B223F45AE71157_AdjustorThunk },
	{ 0x06000036, MeshComputeSettings_get_MeshComputeConsistency_mED95E2D4FCAF5A1DE8F0E2C82B5B56E11D7BA878_AdjustorThunk },
	{ 0x06000037, MeshComputeSettings_set_MeshComputeConsistency_m2D0A6C5A790746581EF229D15944E0B8B78FBDFA_AdjustorThunk },
	{ 0x06000049, ViewConfiguration__ctor_mC98910BB64A18EF97C707339C732D86B2FD5305B_AdjustorThunk },
	{ 0x0600004A, ViewConfiguration_get_ViewConfigurationType_m2CCCF7ABF9EBE176E5E839CE040E251E51B394B3_AdjustorThunk },
	{ 0x0600004B, ViewConfiguration_get_IsActive_m36150FA9963CEBF6B1628AEEA61E75716C8EBDE8_AdjustorThunk },
	{ 0x0600004C, ViewConfiguration_get_SupportedReprojectionModes_m4A049171510E53D849274DDA6E4F1F70066F93D2_AdjustorThunk },
	{ 0x0600004D, ViewConfiguration_SetReprojectionSettings_mB3CA9C8F4836C92F4DF7148F560511D43C81E567_AdjustorThunk },
	{ 0x0600004E, ViewConfiguration_get_IsTracked_m807FD4FDEAB78B3D0A83FF6C7163A934159DAFA6_AdjustorThunk },
	{ 0x06000050, ReprojectionSettings_get_ReprojectionMode_m7F36D9AAEDD1924B4F24EA0EAF0028455B00E577_AdjustorThunk },
	{ 0x06000051, ReprojectionSettings_set_ReprojectionMode_m9FD37CBAA6EF0FF3C5A5C5D58558E24862542292_AdjustorThunk },
	{ 0x060001B4, NativeReprojectionSettings__ctor_mCA8BC7291C9A2C39AF6206C8EC6FFBEA9EFF09BB_AdjustorThunk },
	{ 0x060001B5, OpenXRViewConfiguration_get_ViewConfigurationType_m42A4C2195819676513065AE2996984098F46BA96_AdjustorThunk },
	{ 0x060001B6, OpenXRViewConfiguration_HasTrackingFlags_m847D81E3251B08B4AA1B79A380344E60524A41C5_AdjustorThunk },
	{ 0x060001B7, OpenXRViewConfiguration_get_IsActive_m09CEB1BACB76BECB6456DF44DB580AA400C860A1_AdjustorThunk },
	{ 0x060001B8, OpenXRViewConfiguration_get_SupportedReprojectionModes_m710DF687727B7A9F65EC768D5C13FE01A2CA8001_AdjustorThunk },
	{ 0x060001B9, OpenXRViewConfiguration__ctor_m1595B1EBD9CE30C08D9E447009CD18B34509A1B7_AdjustorThunk },
	{ 0x060001BA, OpenXRViewConfiguration_SetReprojectionSettings_m07A4F916B270A6DDBB55A1666A265ACDA1E9D608_AdjustorThunk },
};
static const int32_t s_InvokerIndices[502] = 
{
	6714,
	6793,
	6806,
	6890,
	6377,
	7026,
	7026,
	3850,
	3224,
	2988,
	7059,
	4797,
	4712,
	4797,
	4797,
	3886,
	4797,
	6921,
	4797,
	4676,
	4676,
	4548,
	4545,
	4546,
	4748,
	4748,
	4748,
	3850,
	4676,
	3850,
	4797,
	4797,
	3224,
	4797,
	4797,
	7026,
	7026,
	3850,
	1048,
	1058,
	7059,
	7026,
	7026,
	3850,
	1692,
	7059,
	4748,
	4723,
	4755,
	4676,
	3850,
	4676,
	3850,
	4676,
	3850,
	6850,
	7026,
	4677,
	4677,
	4677,
	4677,
	2918,
	4797,
	7059,
	6796,
	7059,
	6788,
	4647,
	3822,
	1686,
	4797,
	7026,
	3887,
	4676,
	4748,
	4712,
	3916,
	4748,
	7059,
	4676,
	3850,
	4712,
	3570,
	1743,
	3886,
	4797,
	6795,
	3886,
	4797,
	4797,
	3886,
	4797,
	3886,
	4712,
	1743,
	3886,
	4797,
	3570,
	1809,
	6795,
	6795,
	4797,
	4797,
	3886,
	4797,
	4797,
	3886,
	4797,
	4797,
	4797,
	4797,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4797,
	4797,
	4797,
	4797,
	2919,
	2916,
	2914,
	2518,
	1305,
	4712,
	4678,
	4797,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4677,
	3851,
	4797,
	6917,
	6161,
	5742,
	6454,
	6454,
	6454,
	6457,
	6453,
	6134,
	6454,
	6693,
	5988,
	6293,
	6134,
	5628,
	5986,
	5985,
	6917,
	6457,
	6453,
	5308,
	4978,
	6917,
	5228,
	6292,
	5308,
	4978,
	6847,
	6693,
	6917,
	5628,
	6096,
	5833,
	6455,
	6917,
	5555,
	5227,
	4914,
	5827,
	5829,
	5229,
	6847,
	6917,
	6456,
	6917,
	5827,
	5828,
	5557,
	6162,
	6160,
	6159,
	6096,
	5699,
	6161,
	6295,
	6152,
	6454,
	5831,
	6454,
	5830,
	6454,
	6454,
	4797,
	6795,
	7059,
	7059,
	4797,
	4712,
	4712,
	4797,
	3570,
	1718,
	3886,
	4797,
	4797,
	7059,
	7059,
	4797,
	4797,
	4797,
	4797,
	4797,
	1288,
	4079,
	1721,
	1110,
	3415,
	2192,
	3886,
	3886,
	7059,
	4712,
	1743,
	3886,
	4797,
	3570,
	1809,
	2993,
	2993,
	4797,
	4797,
	4797,
	3886,
	4797,
	4797,
	3886,
	6846,
	6846,
	6851,
	6851,
	6851,
	-1,
	6791,
	2021,
	4676,
	3850,
	3224,
	4797,
	4797,
	4797,
	4797,
	7059,
	3886,
	3886,
	3886,
	3886,
	3886,
	6850,
	7059,
	2192,
	3886,
	3886,
	7059,
	7059,
	4797,
	4797,
	4797,
	4797,
	4797,
	4676,
	4676,
	3850,
	1287,
	2728,
	1292,
	2192,
	3886,
	3886,
	7059,
	7059,
	4797,
	1112,
	1100,
	3951,
	4797,
	2192,
	3886,
	3886,
	7059,
	7059,
	4797,
	4797,
	4797,
	4797,
	4797,
	4677,
	4677,
	3851,
	4676,
	4676,
	4748,
	4748,
	4676,
	4677,
	4678,
	4647,
	4712,
	2406,
	4797,
	4797,
	2285,
	3982,
	2192,
	3886,
	3886,
	7059,
	2219,
	2224,
	632,
	3886,
	-1,
	-1,
	-1,
	-1,
	4677,
	4677,
	4677,
	4748,
	4676,
	4677,
	3886,
	3886,
	3886,
	3886,
	3886,
	3886,
	3886,
	3886,
	3886,
	3886,
	3886,
	3886,
	4748,
	-1,
	-1,
	-1,
	-1,
	3886,
	3886,
	3886,
	3886,
	3886,
	3916,
	4676,
	3317,
	4748,
	4712,
	2190,
	3916,
	4712,
	2192,
	2224,
	2224,
	4748,
	3920,
	4797,
	4797,
	3920,
	4797,
	4797,
	4797,
	6889,
	6659,
	7044,
	6923,
	6798,
	7059,
	6250,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	2917,
	3851,
	3851,
	2020,
	4797,
	7026,
	7026,
	2917,
	3851,
	3851,
	2020,
	4748,
	4712,
	3886,
	4797,
	4797,
	4797,
	4797,
	4797,
	4712,
	3886,
	4675,
	3849,
	4676,
	3850,
	4676,
	3850,
	4748,
	3920,
	4797,
	6715,
	6795,
	6720,
	6312,
	6795,
};
static const Il2CppTokenRangePair s_rgctxIndices[6] = 
{
	{ 0x0200002B, { 0, 15 } },
	{ 0x060000CB, { 15, 1 } },
	{ 0x060000CC, { 16, 1 } },
	{ 0x060000CD, { 17, 1 } },
	{ 0x060000CE, { 18, 1 } },
	{ 0x06000145, { 19, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[21] = 
{
	{ (Il2CppRGCTXDataType)3, 35495 },
	{ (Il2CppRGCTXDataType)1, 763 },
	{ (Il2CppRGCTXDataType)2, 8201 },
	{ (Il2CppRGCTXDataType)3, 35490 },
	{ (Il2CppRGCTXDataType)3, 35492 },
	{ (Il2CppRGCTXDataType)3, 35493 },
	{ (Il2CppRGCTXDataType)3, 35491 },
	{ (Il2CppRGCTXDataType)3, 35496 },
	{ (Il2CppRGCTXDataType)3, 35494 },
	{ (Il2CppRGCTXDataType)3, 35497 },
	{ (Il2CppRGCTXDataType)3, 35502 },
	{ (Il2CppRGCTXDataType)3, 35500 },
	{ (Il2CppRGCTXDataType)3, 35498 },
	{ (Il2CppRGCTXDataType)3, 35501 },
	{ (Il2CppRGCTXDataType)3, 35499 },
	{ (Il2CppRGCTXDataType)3, 51708 },
	{ (Il2CppRGCTXDataType)3, 51712 },
	{ (Il2CppRGCTXDataType)3, 51714 },
	{ (Il2CppRGCTXDataType)3, 51710 },
	{ (Il2CppRGCTXDataType)2, 8124 },
	{ (Il2CppRGCTXDataType)3, 33629 },
};
extern const CustomAttributesCacheGenerator g_Microsoft_MixedReality_OpenXR_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Microsoft_MixedReality_OpenXR_CodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_OpenXR_CodeGenModule = 
{
	"Microsoft.MixedReality.OpenXR.dll",
	502,
	s_methodPointers,
	32,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	6,
	s_rgctxIndices,
	21,
	s_rgctxValues,
	NULL,
	g_Microsoft_MixedReality_OpenXR_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
