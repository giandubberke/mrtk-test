﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.IntPtr UnityEngine.XR.ARFoundation.ARAnchor::get_nativePtr()
extern void ARAnchor_get_nativePtr_mAFCE600DF5E1231E8349DDD3441EE0DC05304BD7 (void);
// 0x00000002 System.Guid UnityEngine.XR.ARFoundation.ARAnchor::get_sessionId()
extern void ARAnchor_get_sessionId_m2932E088EB59231C0D76273DEAD10D56C8631560 (void);
// 0x00000003 System.Void UnityEngine.XR.ARFoundation.ARAnchor::OnEnable()
extern void ARAnchor_OnEnable_m9706E493783C175B28DE168C67B54AD3DDE3F3A9 (void);
// 0x00000004 System.Void UnityEngine.XR.ARFoundation.ARAnchor::Update()
extern void ARAnchor_Update_m471443F726BC7814024765A4ADCEE55667AAA6A5 (void);
// 0x00000005 System.Void UnityEngine.XR.ARFoundation.ARAnchor::OnDisable()
extern void ARAnchor_OnDisable_mA724EEFA3739BD8A09A61F02D3DDEAA6DD6E0872 (void);
// 0x00000006 System.Void UnityEngine.XR.ARFoundation.ARAnchor::.ctor()
extern void ARAnchor__ctor_m8AC4C705DEC2C70EEF810D9C4EABC0C01B3C2225 (void);
// 0x00000007 UnityEngine.GameObject UnityEngine.XR.ARFoundation.ARAnchorManager::get_anchorPrefab()
extern void ARAnchorManager_get_anchorPrefab_m83A6325983B087F873F4A2D77A9959DA3DE396F8 (void);
// 0x00000008 System.Void UnityEngine.XR.ARFoundation.ARAnchorManager::set_anchorPrefab(UnityEngine.GameObject)
extern void ARAnchorManager_set_anchorPrefab_m42038AAD8D7B70F498EA0429CDFBF1E18DA38756 (void);
// 0x00000009 System.Void UnityEngine.XR.ARFoundation.ARAnchorManager::add_anchorsChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARAnchorsChangedEventArgs>)
extern void ARAnchorManager_add_anchorsChanged_m32E33C78B20F072111FEBBDB2AFEF1F8B25EA367 (void);
// 0x0000000A System.Void UnityEngine.XR.ARFoundation.ARAnchorManager::remove_anchorsChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARAnchorsChangedEventArgs>)
extern void ARAnchorManager_remove_anchorsChanged_m36F2B944753B78D564BDFD3D7118934AD836AB6B (void);
// 0x0000000B UnityEngine.XR.ARFoundation.ARAnchor UnityEngine.XR.ARFoundation.ARAnchorManager::AddAnchor(UnityEngine.Pose)
extern void ARAnchorManager_AddAnchor_mBE78F22E6E4BE754EF0210E24E63E17EBEC5A3DC (void);
// 0x0000000C System.Boolean UnityEngine.XR.ARFoundation.ARAnchorManager::TryAddAnchor(UnityEngine.XR.ARFoundation.ARAnchor)
extern void ARAnchorManager_TryAddAnchor_mC5522D55076F7D56771AD3334996162B4AF3FCAF (void);
// 0x0000000D UnityEngine.XR.ARFoundation.ARAnchor UnityEngine.XR.ARFoundation.ARAnchorManager::AttachAnchor(UnityEngine.XR.ARFoundation.ARPlane,UnityEngine.Pose)
extern void ARAnchorManager_AttachAnchor_m09F3BB1DC64599205719BE1F28AE2610DECF5825 (void);
// 0x0000000E System.Boolean UnityEngine.XR.ARFoundation.ARAnchorManager::RemoveAnchor(UnityEngine.XR.ARFoundation.ARAnchor)
extern void ARAnchorManager_RemoveAnchor_m1D2D061879D97BCCCFF3267A44A7D7E43F8799A0 (void);
// 0x0000000F System.Boolean UnityEngine.XR.ARFoundation.ARAnchorManager::TryRemoveAnchor(UnityEngine.XR.ARFoundation.ARAnchor)
extern void ARAnchorManager_TryRemoveAnchor_mEA631AE447C8251AC439E9D9383DA9ABD0A7975E (void);
// 0x00000010 UnityEngine.XR.ARFoundation.ARAnchor UnityEngine.XR.ARFoundation.ARAnchorManager::GetAnchor(UnityEngine.XR.ARSubsystems.TrackableId)
extern void ARAnchorManager_GetAnchor_mDA50990B270097F8544840C0DFC14FF7419742FA (void);
// 0x00000011 UnityEngine.GameObject UnityEngine.XR.ARFoundation.ARAnchorManager::GetPrefab()
extern void ARAnchorManager_GetPrefab_m854DECEB66697A6104D8D5F22DAFC927A7AFE510 (void);
// 0x00000012 System.String UnityEngine.XR.ARFoundation.ARAnchorManager::get_gameObjectName()
extern void ARAnchorManager_get_gameObjectName_m46C6FAA9D14AADDF62B2ECA7B8BB3094A55F00B1 (void);
// 0x00000013 System.Void UnityEngine.XR.ARFoundation.ARAnchorManager::OnTrackablesChanged(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARAnchor>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARAnchor>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARAnchor>)
extern void ARAnchorManager_OnTrackablesChanged_m4D4B7E050451FB393240E94551B80C055CEE3A63 (void);
// 0x00000014 System.Void UnityEngine.XR.ARFoundation.ARAnchorManager::.ctor()
extern void ARAnchorManager__ctor_m6A7DB89B5F9DE5A22FD7FE1E85D35A5000D06F3E (void);
// 0x00000015 System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARAnchor> UnityEngine.XR.ARFoundation.ARAnchorsChangedEventArgs::get_added()
extern void ARAnchorsChangedEventArgs_get_added_m4D07DC13F8D06201C220964F8CCE1CD99F4342E8 (void);
// 0x00000016 System.Void UnityEngine.XR.ARFoundation.ARAnchorsChangedEventArgs::set_added(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARAnchor>)
extern void ARAnchorsChangedEventArgs_set_added_m250070DF629DAAF526CE631D008D1320DE449A40 (void);
// 0x00000017 System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARAnchor> UnityEngine.XR.ARFoundation.ARAnchorsChangedEventArgs::get_updated()
extern void ARAnchorsChangedEventArgs_get_updated_m87B120B079D1B9FF8C9F1E305672BD6F741AC7DF (void);
// 0x00000018 System.Void UnityEngine.XR.ARFoundation.ARAnchorsChangedEventArgs::set_updated(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARAnchor>)
extern void ARAnchorsChangedEventArgs_set_updated_m3A0ED32E6B56174861DD6431E0C50AE016FC9826 (void);
// 0x00000019 System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARAnchor> UnityEngine.XR.ARFoundation.ARAnchorsChangedEventArgs::get_removed()
extern void ARAnchorsChangedEventArgs_get_removed_m3DA7093629E8D772C2732299D5F612B1357F6D08 (void);
// 0x0000001A System.Void UnityEngine.XR.ARFoundation.ARAnchorsChangedEventArgs::set_removed(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARAnchor>)
extern void ARAnchorsChangedEventArgs_set_removed_m96CA8069483206C90B983B2BCF00F8AB60FC6985 (void);
// 0x0000001B System.Void UnityEngine.XR.ARFoundation.ARAnchorsChangedEventArgs::.ctor(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARAnchor>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARAnchor>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARAnchor>)
extern void ARAnchorsChangedEventArgs__ctor_m51231F5A1B806EDD5E2A37C514A81BA87AC29DB5 (void);
// 0x0000001C System.Int32 UnityEngine.XR.ARFoundation.ARAnchorsChangedEventArgs::GetHashCode()
extern void ARAnchorsChangedEventArgs_GetHashCode_m9F8FD840B1A6562E4B3CF4A3B5A7175A49D136D0 (void);
// 0x0000001D System.Boolean UnityEngine.XR.ARFoundation.ARAnchorsChangedEventArgs::Equals(System.Object)
extern void ARAnchorsChangedEventArgs_Equals_mBB1A87EB69C9AED17A688CAF715718D87FD027D6 (void);
// 0x0000001E System.String UnityEngine.XR.ARFoundation.ARAnchorsChangedEventArgs::ToString()
extern void ARAnchorsChangedEventArgs_ToString_m2C6B93BD6C868C7243C104B916EC06E871AABC5A (void);
// 0x0000001F System.Boolean UnityEngine.XR.ARFoundation.ARAnchorsChangedEventArgs::Equals(UnityEngine.XR.ARFoundation.ARAnchorsChangedEventArgs)
extern void ARAnchorsChangedEventArgs_Equals_m007C8B067E9DF6AEE162E8BF8889419E0E183961 (void);
// 0x00000020 System.Single UnityEngine.XR.ARFoundation.ARPlane::get_vertexChangedThreshold()
extern void ARPlane_get_vertexChangedThreshold_mAA60F671E3E504C8728555CA7CE9EE1622EF9288 (void);
// 0x00000021 System.Void UnityEngine.XR.ARFoundation.ARPlane::set_vertexChangedThreshold(System.Single)
extern void ARPlane_set_vertexChangedThreshold_m04C245B86A30F5D34F9D470FB04AC8B8BBEA9A2D (void);
// 0x00000022 System.Void UnityEngine.XR.ARFoundation.ARPlane::add_boundaryChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs>)
extern void ARPlane_add_boundaryChanged_m2C6F18F6A876A0CDC182820725A38A534B988BEE (void);
// 0x00000023 System.Void UnityEngine.XR.ARFoundation.ARPlane::remove_boundaryChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs>)
extern void ARPlane_remove_boundaryChanged_m806691C95E05CDBCB7637F6070F30860D9BF403B (void);
// 0x00000024 UnityEngine.Vector3 UnityEngine.XR.ARFoundation.ARPlane::get_normal()
extern void ARPlane_get_normal_m5D251614E45E0F89C3525A1EA8BF0A23B1FED8DB (void);
// 0x00000025 UnityEngine.XR.ARFoundation.ARPlane UnityEngine.XR.ARFoundation.ARPlane::get_subsumedBy()
extern void ARPlane_get_subsumedBy_m4A7A81ED11E4EF5CE7A82E9E3E1BF33620B8C4C3 (void);
// 0x00000026 System.Void UnityEngine.XR.ARFoundation.ARPlane::set_subsumedBy(UnityEngine.XR.ARFoundation.ARPlane)
extern void ARPlane_set_subsumedBy_mF2FA42EB5816B76672003101C8F4E56698B6126A (void);
// 0x00000027 UnityEngine.XR.ARSubsystems.PlaneAlignment UnityEngine.XR.ARFoundation.ARPlane::get_alignment()
extern void ARPlane_get_alignment_mE98AB13AA23A17118D57E28213955B32F96B7046 (void);
// 0x00000028 UnityEngine.XR.ARSubsystems.PlaneClassification UnityEngine.XR.ARFoundation.ARPlane::get_classification()
extern void ARPlane_get_classification_mE63F0D4D9B17E53D3F58F7A4BEDFF1DDE28C829E (void);
// 0x00000029 UnityEngine.Vector2 UnityEngine.XR.ARFoundation.ARPlane::get_centerInPlaneSpace()
extern void ARPlane_get_centerInPlaneSpace_m243BBB28377DBE0039805A0E271B6BF0879FB437 (void);
// 0x0000002A UnityEngine.Vector3 UnityEngine.XR.ARFoundation.ARPlane::get_center()
extern void ARPlane_get_center_mEDC87E2E4C5029D34515F17E9B2D80B9125B5484 (void);
// 0x0000002B UnityEngine.Vector2 UnityEngine.XR.ARFoundation.ARPlane::get_extents()
extern void ARPlane_get_extents_m15E7D52F5BBB68F8EF0871A573894309B1D9B623 (void);
// 0x0000002C UnityEngine.Vector2 UnityEngine.XR.ARFoundation.ARPlane::get_size()
extern void ARPlane_get_size_m3848DFA43414D59474E29C8F4ED3DAB848A27D18 (void);
// 0x0000002D UnityEngine.Plane UnityEngine.XR.ARFoundation.ARPlane::get_infinitePlane()
extern void ARPlane_get_infinitePlane_mF31BA660A9854B85EBE1079763B6C3F702425963 (void);
// 0x0000002E System.IntPtr UnityEngine.XR.ARFoundation.ARPlane::get_nativePtr()
extern void ARPlane_get_nativePtr_m83BE2C3F5EADF8CE66FCF7045A4AB318F91F97A9 (void);
// 0x0000002F Unity.Collections.NativeArray`1<UnityEngine.Vector2> UnityEngine.XR.ARFoundation.ARPlane::get_boundary()
extern void ARPlane_get_boundary_m36CBBE48CE6D88237DF3394250A83A69C4E7F391 (void);
// 0x00000030 System.Void UnityEngine.XR.ARFoundation.ARPlane::UpdateBoundary(UnityEngine.XR.ARSubsystems.XRPlaneSubsystem)
extern void ARPlane_UpdateBoundary_m2108B0805369F86A19E689A5FC3A403D78AB0396 (void);
// 0x00000031 System.Void UnityEngine.XR.ARFoundation.ARPlane::OnValidate()
extern void ARPlane_OnValidate_mA4A9679E135EC8A45942CF35E2E5E90544D5383D (void);
// 0x00000032 System.Void UnityEngine.XR.ARFoundation.ARPlane::OnDestroy()
extern void ARPlane_OnDestroy_mD8358BCB71AFF7210A0612DB75707D8F0B3A5684 (void);
// 0x00000033 System.Void UnityEngine.XR.ARFoundation.ARPlane::CheckForBoundaryChanges()
extern void ARPlane_CheckForBoundaryChanges_m9C3F3B40C724A8F16196F81D8858AB67F33799F1 (void);
// 0x00000034 System.Void UnityEngine.XR.ARFoundation.ARPlane::CopyBoundaryAndSetChangedFlag()
extern void ARPlane_CopyBoundaryAndSetChangedFlag_m782C386656526707A27FFAA2AA43586205601346 (void);
// 0x00000035 System.Void UnityEngine.XR.ARFoundation.ARPlane::Update()
extern void ARPlane_Update_m11B70239AFD5ED715614EAAB6BF71C25D67ACE27 (void);
// 0x00000036 System.Void UnityEngine.XR.ARFoundation.ARPlane::.ctor()
extern void ARPlane__ctor_m0408A53F8E2054DB1A48292C0F80EC228711399A (void);
// 0x00000037 UnityEngine.XR.ARFoundation.ARPlane UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs::get_plane()
extern void ARPlaneBoundaryChangedEventArgs_get_plane_mC91E64413E8593C0137A7C47485A1238180932A5 (void);
// 0x00000038 System.Void UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs::set_plane(UnityEngine.XR.ARFoundation.ARPlane)
extern void ARPlaneBoundaryChangedEventArgs_set_plane_mEDFCCE4100F4480E53F3EE12F4F89DB7054AE6D4 (void);
// 0x00000039 System.Void UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs::.ctor(UnityEngine.XR.ARFoundation.ARPlane)
extern void ARPlaneBoundaryChangedEventArgs__ctor_mCD138E5FFD74678A29B6A494550C428AE7344678 (void);
// 0x0000003A System.Int32 UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs::GetHashCode()
extern void ARPlaneBoundaryChangedEventArgs_GetHashCode_m01011EEB2FAD823382934CDF5710CE259A212EB4 (void);
// 0x0000003B System.Boolean UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs::Equals(System.Object)
extern void ARPlaneBoundaryChangedEventArgs_Equals_m9BBA8DDF067E8DC0BB0956C0BD4FF95B78C9DA1B (void);
// 0x0000003C System.String UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs::ToString()
extern void ARPlaneBoundaryChangedEventArgs_ToString_mB9D8B3B3DD1965A75E8CBE0F1133DA0C4310D28F (void);
// 0x0000003D System.Boolean UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs::Equals(UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs)
extern void ARPlaneBoundaryChangedEventArgs_Equals_m8FCE506D26E362A259FD1E260648E9FA5C07B91D (void);
// 0x0000003E UnityEngine.GameObject UnityEngine.XR.ARFoundation.ARPlaneManager::get_planePrefab()
extern void ARPlaneManager_get_planePrefab_m3C00DB153DAE1813C0174CE0668913D2D2F74AE6 (void);
// 0x0000003F System.Void UnityEngine.XR.ARFoundation.ARPlaneManager::set_planePrefab(UnityEngine.GameObject)
extern void ARPlaneManager_set_planePrefab_m411D7254A8FF9891E6DCB7E1BE9C64052D4565FC (void);
// 0x00000040 UnityEngine.XR.ARSubsystems.PlaneDetectionMode UnityEngine.XR.ARFoundation.ARPlaneManager::get_detectionMode()
extern void ARPlaneManager_get_detectionMode_mFA03DF070244B75712F5EB76B01C56B6ECD9C92D (void);
// 0x00000041 System.Void UnityEngine.XR.ARFoundation.ARPlaneManager::set_detectionMode(UnityEngine.XR.ARSubsystems.PlaneDetectionMode)
extern void ARPlaneManager_set_detectionMode_m9751E405615DF5749E5CABFF3AD5178A907AB540 (void);
// 0x00000042 UnityEngine.XR.ARSubsystems.PlaneDetectionMode UnityEngine.XR.ARFoundation.ARPlaneManager::get_requestedDetectionMode()
extern void ARPlaneManager_get_requestedDetectionMode_m7AB0F69B8A0EC9A8A76C1303621BF3932FF1B614 (void);
// 0x00000043 System.Void UnityEngine.XR.ARFoundation.ARPlaneManager::set_requestedDetectionMode(UnityEngine.XR.ARSubsystems.PlaneDetectionMode)
extern void ARPlaneManager_set_requestedDetectionMode_mBAE613D44F9110CF01D898BC68AD02914AB23A6E (void);
// 0x00000044 UnityEngine.XR.ARSubsystems.PlaneDetectionMode UnityEngine.XR.ARFoundation.ARPlaneManager::get_currentDetectionMode()
extern void ARPlaneManager_get_currentDetectionMode_m12AFC03E86949D9D354D68F340332ADDBD79E5DE (void);
// 0x00000045 System.Void UnityEngine.XR.ARFoundation.ARPlaneManager::add_planesChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs>)
extern void ARPlaneManager_add_planesChanged_m785106C77BC4F503A9AB904E9BEF3EDCCF49FD27 (void);
// 0x00000046 System.Void UnityEngine.XR.ARFoundation.ARPlaneManager::remove_planesChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs>)
extern void ARPlaneManager_remove_planesChanged_mF9F02F508C2B19FFB3F6B5738B00C2C618B4C722 (void);
// 0x00000047 UnityEngine.XR.ARFoundation.ARPlane UnityEngine.XR.ARFoundation.ARPlaneManager::GetPlane(UnityEngine.XR.ARSubsystems.TrackableId)
extern void ARPlaneManager_GetPlane_m2863C99ED9ACEE22F720067A1E0AC6C55E743442 (void);
// 0x00000048 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARFoundation.ARPlaneManager::Raycast(UnityEngine.Ray,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void ARPlaneManager_Raycast_mAE3382E17ADFF506CF71B72705CF8C25AA264C79 (void);
// 0x00000049 System.Single UnityEngine.XR.ARFoundation.ARPlaneManager::GetCrossDirection(UnityEngine.Vector2,UnityEngine.Vector2)
extern void ARPlaneManager_GetCrossDirection_m6929DD8D3045AC8AE5F9A816F94F7EBDB0470916 (void);
// 0x0000004A System.Int32 UnityEngine.XR.ARFoundation.ARPlaneManager::WindingNumber(UnityEngine.Vector2,Unity.Collections.NativeArray`1<UnityEngine.Vector2>)
extern void ARPlaneManager_WindingNumber_m37E0C9DBCC45AAC1CE645B13ED72C0A467886279 (void);
// 0x0000004B UnityEngine.GameObject UnityEngine.XR.ARFoundation.ARPlaneManager::GetPrefab()
extern void ARPlaneManager_GetPrefab_mAC046B2B1BF7CF3D5D0812982CD8697D9EEE15E6 (void);
// 0x0000004C System.Void UnityEngine.XR.ARFoundation.ARPlaneManager::OnBeforeStart()
extern void ARPlaneManager_OnBeforeStart_m08A048DDE25E6E58DAB14D5A98FA967D335281FE (void);
// 0x0000004D System.Void UnityEngine.XR.ARFoundation.ARPlaneManager::OnAfterSetSessionRelativeData(UnityEngine.XR.ARFoundation.ARPlane,UnityEngine.XR.ARSubsystems.BoundedPlane)
extern void ARPlaneManager_OnAfterSetSessionRelativeData_m29742CEC4DFD4C38531850D5BB294D187BBEE86B (void);
// 0x0000004E System.Void UnityEngine.XR.ARFoundation.ARPlaneManager::OnTrackablesChanged(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>)
extern void ARPlaneManager_OnTrackablesChanged_m35BB527E9A876CD709358974F404DF783B220C10 (void);
// 0x0000004F System.String UnityEngine.XR.ARFoundation.ARPlaneManager::get_gameObjectName()
extern void ARPlaneManager_get_gameObjectName_mA9589CB27F850045D5C9403B124C18E592E73D61 (void);
// 0x00000050 System.Void UnityEngine.XR.ARFoundation.ARPlaneManager::OnEnable()
extern void ARPlaneManager_OnEnable_m02B2D3772BF6CC084FA6BFA31B6939CB5199CF97 (void);
// 0x00000051 System.Void UnityEngine.XR.ARFoundation.ARPlaneManager::OnDisable()
extern void ARPlaneManager_OnDisable_mD54AB766B9285D5C673E54A2A3AEA7F37CC54882 (void);
// 0x00000052 System.Void UnityEngine.XR.ARFoundation.ARPlaneManager::.ctor()
extern void ARPlaneManager__ctor_m620BF313F00F9BCAEB75096C558463FF98DD5FEF (void);
// 0x00000053 System.Boolean UnityEngine.XR.ARFoundation.ARPlaneMeshGenerators::GenerateMesh(UnityEngine.Mesh,UnityEngine.Pose,Unity.Collections.NativeArray`1<UnityEngine.Vector2>,System.Single)
extern void ARPlaneMeshGenerators_GenerateMesh_mC65957DD09D5DF63DD09B6067B5F9CEEEC60A100 (void);
// 0x00000054 System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshGenerators::GenerateUvs(System.Collections.Generic.List`1<UnityEngine.Vector2>,UnityEngine.Pose,System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void ARPlaneMeshGenerators_GenerateUvs_m63AD50B08D0846EB2FE7D189E817B7CCE1D1CA2A (void);
// 0x00000055 System.Boolean UnityEngine.XR.ARFoundation.ARPlaneMeshGenerators::GenerateIndices(System.Collections.Generic.List`1<System.Int32>,System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Single)
extern void ARPlaneMeshGenerators_GenerateIndices_m0884F6FE12A5E246AE457EE06D1614E9D35C40A7 (void);
// 0x00000056 System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshGenerators::.cctor()
extern void ARPlaneMeshGenerators__cctor_mA2AD482799CBD56AC165BBAC69142C0EE97096B4 (void);
// 0x00000057 UnityEngine.Mesh UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer::get_mesh()
extern void ARPlaneMeshVisualizer_get_mesh_m7E5CE302636B7C6F9578DBB9D0161CF96F5D7AB4 (void);
// 0x00000058 System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer::set_mesh(UnityEngine.Mesh)
extern void ARPlaneMeshVisualizer_set_mesh_mE140F46F7B4124CEABB9F1B8D146844437FE5C41 (void);
// 0x00000059 System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer::OnBoundaryChanged(UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs)
extern void ARPlaneMeshVisualizer_OnBoundaryChanged_mD8351E73EB50EA25B76AE4B34AF2F4D6EC6DAFF3 (void);
// 0x0000005A System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer::DisableComponents()
extern void ARPlaneMeshVisualizer_DisableComponents_m9FD32662E3F810874B66E074B06C2A9995567387 (void);
// 0x0000005B System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer::SetVisible(System.Boolean)
extern void ARPlaneMeshVisualizer_SetVisible_mCFE4013BD17B4922538374DFD89C5717FE8C1B1F (void);
// 0x0000005C System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer::UpdateVisibility()
extern void ARPlaneMeshVisualizer_UpdateVisibility_m1F81FBFD5A513C75A9AFB8426782B76178672121 (void);
// 0x0000005D System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer::Awake()
extern void ARPlaneMeshVisualizer_Awake_m38B47CDFA66CAC55E120597A2C745424C78492B0 (void);
// 0x0000005E System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer::OnEnable()
extern void ARPlaneMeshVisualizer_OnEnable_mA7F75A39674737AEC66313CF4F3F4A9210034840 (void);
// 0x0000005F System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer::OnDisable()
extern void ARPlaneMeshVisualizer_OnDisable_mE542AD4800BA0FE7AE2F2E368AF2F46C426C5533 (void);
// 0x00000060 System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer::Update()
extern void ARPlaneMeshVisualizer_Update_m98C87020EC822B160D3B62FB67EC169E4931CC8D (void);
// 0x00000061 System.Void UnityEngine.XR.ARFoundation.ARPlaneMeshVisualizer::.ctor()
extern void ARPlaneMeshVisualizer__ctor_mC9399C82657A81411A5D6CECD94F27D1D6B8DDF5 (void);
// 0x00000062 System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane> UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::get_added()
extern void ARPlanesChangedEventArgs_get_added_mD0CA0FB88C9F669580ED4BE0C9A3DA6E1333DA07 (void);
// 0x00000063 System.Void UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::set_added(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>)
extern void ARPlanesChangedEventArgs_set_added_mADE2BA0CDD14711F3576C51A04BC0A06CC4F0C37 (void);
// 0x00000064 System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane> UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::get_updated()
extern void ARPlanesChangedEventArgs_get_updated_m1EB6B86313D75F78C287B67BF2EA7A03468BB48E (void);
// 0x00000065 System.Void UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::set_updated(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>)
extern void ARPlanesChangedEventArgs_set_updated_m186CC3DF36E1B28FE2B7CFB2F9C56E638C4A4F26 (void);
// 0x00000066 System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane> UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::get_removed()
extern void ARPlanesChangedEventArgs_get_removed_m771596586A99FE5593EF16233E7F3BA3603E0927 (void);
// 0x00000067 System.Void UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::set_removed(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>)
extern void ARPlanesChangedEventArgs_set_removed_mE2BE624B870142F492E01AB6328B4D74E575228A (void);
// 0x00000068 System.Void UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::.ctor(System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARPlane>)
extern void ARPlanesChangedEventArgs__ctor_mE0D81BC682849CEF4267F2466246A42A451D7D3A (void);
// 0x00000069 System.Int32 UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::GetHashCode()
extern void ARPlanesChangedEventArgs_GetHashCode_m80CECC92973BF7AC233A66E338DFE888901E8C7F (void);
// 0x0000006A System.Boolean UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::Equals(System.Object)
extern void ARPlanesChangedEventArgs_Equals_m6EE7772274DE47C25AB57E32C1B4C0FAC78B48BF (void);
// 0x0000006B System.String UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::ToString()
extern void ARPlanesChangedEventArgs_ToString_m0175F0496B86842A452179E194FEAB0FC61A5D3E (void);
// 0x0000006C System.Boolean UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs::Equals(UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs)
extern void ARPlanesChangedEventArgs_Equals_mCB86F0483BA241F16D592B63E2DA15F7DA74778C (void);
// 0x0000006D System.Void UnityEngine.XR.ARFoundation.ARPoseDriver::OnEnable()
extern void ARPoseDriver_OnEnable_m4CF363539DE8829C4C61AE09CD2AF6709A402818 (void);
// 0x0000006E System.Void UnityEngine.XR.ARFoundation.ARPoseDriver::OnDisable()
extern void ARPoseDriver_OnDisable_m1607040D5924045B7775017AFFF5C7AF04BCDFBA (void);
// 0x0000006F System.Void UnityEngine.XR.ARFoundation.ARPoseDriver::Update()
extern void ARPoseDriver_Update_m5AB64BF6DD47D8219284D3355127F4B981A70586 (void);
// 0x00000070 System.Void UnityEngine.XR.ARFoundation.ARPoseDriver::OnBeforeRender()
extern void ARPoseDriver_OnBeforeRender_mFE069B31B36607003CC185498D5E39A6F4227838 (void);
// 0x00000071 System.Void UnityEngine.XR.ARFoundation.ARPoseDriver::PerformUpdate()
extern void ARPoseDriver_PerformUpdate_m1622A744D6B9B8B8407DC9ECB4B3C1C0EB120C63 (void);
// 0x00000072 System.Void UnityEngine.XR.ARFoundation.ARPoseDriver::OnInputDeviceConnected(UnityEngine.XR.InputDevice)
extern void ARPoseDriver_OnInputDeviceConnected_m9FC7B979E01897C30CE6A29F1A568F59C4A15068 (void);
// 0x00000073 System.Void UnityEngine.XR.ARFoundation.ARPoseDriver::CheckConnectedDevice(UnityEngine.XR.InputDevice,System.Boolean)
extern void ARPoseDriver_CheckConnectedDevice_m79F0F1BA26D8A444E26CC2A66718C048FD64F096 (void);
// 0x00000074 UnityEngine.XR.ARFoundation.ARPoseDriver/NullablePose UnityEngine.XR.ARFoundation.ARPoseDriver::GetPoseData()
extern void ARPoseDriver_GetPoseData_mD73767B4A41C576A343C4F09E3B7C700EF25AE9D (void);
// 0x00000075 System.Void UnityEngine.XR.ARFoundation.ARPoseDriver::.ctor()
extern void ARPoseDriver__ctor_mDC919E8D461DA698AA8CFE0A2DF8E18E65990EBA (void);
// 0x00000076 System.Void UnityEngine.XR.ARFoundation.ARPoseDriver::.cctor()
extern void ARPoseDriver__cctor_m0F6DD1D538D8F6B58E83EF69963A791CCBC08147 (void);
// 0x00000077 System.IntPtr UnityEngine.XR.ARFoundation.ARRaycast::get_nativePtr()
extern void ARRaycast_get_nativePtr_mBFBD4341016CEE43EF6D2BEC4A00233C71653583 (void);
// 0x00000078 System.Single UnityEngine.XR.ARFoundation.ARRaycast::get_distance()
extern void ARRaycast_get_distance_mB010898D359A5494CE80DE8373D7690B1B5BB06D (void);
// 0x00000079 UnityEngine.XR.ARFoundation.ARPlane UnityEngine.XR.ARFoundation.ARRaycast::get_plane()
extern void ARRaycast_get_plane_mD994AB92D48D9E69C94A70473E23B04725B43663 (void);
// 0x0000007A System.Void UnityEngine.XR.ARFoundation.ARRaycast::set_plane(UnityEngine.XR.ARFoundation.ARPlane)
extern void ARRaycast_set_plane_mCA9E660D5C3168183D2DB52EAF433D1DB69EC5AF (void);
// 0x0000007B System.Void UnityEngine.XR.ARFoundation.ARRaycast::add_updated(System.Action`1<UnityEngine.XR.ARFoundation.ARRaycastUpdatedEventArgs>)
extern void ARRaycast_add_updated_mA58AF75CDF5D15B24D56E79FBD17BFBBC9B76131 (void);
// 0x0000007C System.Void UnityEngine.XR.ARFoundation.ARRaycast::remove_updated(System.Action`1<UnityEngine.XR.ARFoundation.ARRaycastUpdatedEventArgs>)
extern void ARRaycast_remove_updated_mB5AE0EC23EA14B7331E507D9F4752A056E0AC2EA (void);
// 0x0000007D System.Void UnityEngine.XR.ARFoundation.ARRaycast::OnAfterSetSessionRelativeData()
extern void ARRaycast_OnAfterSetSessionRelativeData_m509CCE0B3BBDCDAA969901835228CC31C8DCB61D (void);
// 0x0000007E System.Void UnityEngine.XR.ARFoundation.ARRaycast::Update()
extern void ARRaycast_Update_m28E30EED38FB607393D4B1524B8BFBADFCD55658 (void);
// 0x0000007F System.Void UnityEngine.XR.ARFoundation.ARRaycast::.ctor()
extern void ARRaycast__ctor_m69DF5EC95EB5F88FF46324B6FAD7B97FE77CF80A (void);
// 0x00000080 System.Void UnityEngine.XR.ARFoundation.ARRaycastHit::.ctor(UnityEngine.XR.ARSubsystems.XRRaycastHit,System.Single,UnityEngine.Transform,UnityEngine.XR.ARFoundation.ARTrackable)
extern void ARRaycastHit__ctor_mC7110E7B31FCC610B0D6AF62CBF4BDCD33751C09 (void);
// 0x00000081 System.Single UnityEngine.XR.ARFoundation.ARRaycastHit::get_distance()
extern void ARRaycastHit_get_distance_mED96EC43E8D177E5B912822DD5566A543BF995AB (void);
// 0x00000082 UnityEngine.Pose UnityEngine.XR.ARFoundation.ARRaycastHit::get_pose()
extern void ARRaycastHit_get_pose_mB4D8BC45F23D9F2C2C8DCAFA88DB1221D76EF02B (void);
// 0x00000083 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARFoundation.ARRaycastHit::get_trackableId()
extern void ARRaycastHit_get_trackableId_mCC2447AD8425B92F2E25E74812D6F50DE588D16A (void);
// 0x00000084 UnityEngine.Pose UnityEngine.XR.ARFoundation.ARRaycastHit::get_sessionRelativePose()
extern void ARRaycastHit_get_sessionRelativePose_m496222B38CDAA9D976ECAFA7CE7ED77CA9E37DBA (void);
// 0x00000085 UnityEngine.XR.ARFoundation.ARTrackable UnityEngine.XR.ARFoundation.ARRaycastHit::get_trackable()
extern void ARRaycastHit_get_trackable_m0A2E4A67B074ACA0021869D6D8930D34A85F1D79 (void);
// 0x00000086 System.Int32 UnityEngine.XR.ARFoundation.ARRaycastHit::GetHashCode()
extern void ARRaycastHit_GetHashCode_m659BE91F4F0039B4C81562344E13B9BB4454BE85 (void);
// 0x00000087 System.Boolean UnityEngine.XR.ARFoundation.ARRaycastHit::Equals(System.Object)
extern void ARRaycastHit_Equals_m10EB8CE2C048C4C1FF0254C69C376B16553577E6 (void);
// 0x00000088 System.Boolean UnityEngine.XR.ARFoundation.ARRaycastHit::Equals(UnityEngine.XR.ARFoundation.ARRaycastHit)
extern void ARRaycastHit_Equals_m75D93284E7D73EDEF82038FA2C0BA7543DBF088D (void);
// 0x00000089 System.Int32 UnityEngine.XR.ARFoundation.ARRaycastHit::CompareTo(UnityEngine.XR.ARFoundation.ARRaycastHit)
extern void ARRaycastHit_CompareTo_m3B3E66B95BFC5CF7099F910BAAC6823403755514 (void);
// 0x0000008A UnityEngine.GameObject UnityEngine.XR.ARFoundation.ARRaycastManager::get_raycastPrefab()
extern void ARRaycastManager_get_raycastPrefab_m1C25B3E344582D3D581C31EB92239E5D9D387AEC (void);
// 0x0000008B System.Void UnityEngine.XR.ARFoundation.ARRaycastManager::set_raycastPrefab(UnityEngine.GameObject)
extern void ARRaycastManager_set_raycastPrefab_m9F40ED082F528AF756FF04DD028DF40A78512DE4 (void);
// 0x0000008C System.Boolean UnityEngine.XR.ARFoundation.ARRaycastManager::Raycast(UnityEngine.Vector2,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit>,UnityEngine.XR.ARSubsystems.TrackableType)
extern void ARRaycastManager_Raycast_mCC2851DAC2542C59528FCE21242231DFAF024650 (void);
// 0x0000008D System.Boolean UnityEngine.XR.ARFoundation.ARRaycastManager::Raycast(UnityEngine.Ray,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit>,UnityEngine.XR.ARSubsystems.TrackableType)
extern void ARRaycastManager_Raycast_m08DA1BF9999699DFC094F91B281B84A44F243E2C (void);
// 0x0000008E UnityEngine.XR.ARFoundation.ARRaycast UnityEngine.XR.ARFoundation.ARRaycastManager::AddRaycast(UnityEngine.Vector2,System.Single)
extern void ARRaycastManager_AddRaycast_mC7DABC7FC09CF7622669790B22D97C08C787B11D (void);
// 0x0000008F System.Void UnityEngine.XR.ARFoundation.ARRaycastManager::RemoveRaycast(UnityEngine.XR.ARFoundation.ARRaycast)
extern void ARRaycastManager_RemoveRaycast_m9171455192ECE274C5D19C15BB86C29AD9321A77 (void);
// 0x00000090 UnityEngine.GameObject UnityEngine.XR.ARFoundation.ARRaycastManager::GetPrefab()
extern void ARRaycastManager_GetPrefab_m74C14C1DF9446B86945F4D0419506B95B16F1903 (void);
// 0x00000091 System.Void UnityEngine.XR.ARFoundation.ARRaycastManager::RegisterRaycaster(UnityEngine.XR.ARFoundation.IRaycaster)
extern void ARRaycastManager_RegisterRaycaster_m483341201AF04DE1DAF54B223E45B3CD75ED4AFE (void);
// 0x00000092 System.Void UnityEngine.XR.ARFoundation.ARRaycastManager::UnregisterRaycaster(UnityEngine.XR.ARFoundation.IRaycaster)
extern void ARRaycastManager_UnregisterRaycaster_mBC2C92DC026598D8F2B6835A97A725525326F0DB (void);
// 0x00000093 System.Void UnityEngine.XR.ARFoundation.ARRaycastManager::OnAfterStart()
extern void ARRaycastManager_OnAfterStart_mB047CA21B4220498B1583A84D1DA41995D7E6370 (void);
// 0x00000094 UnityEngine.Ray UnityEngine.XR.ARFoundation.ARRaycastManager::ScreenPointToSessionSpaceRay(UnityEngine.Vector2)
extern void ARRaycastManager_ScreenPointToSessionSpaceRay_m7D68FA43BDD2972DA070E7DE05935AB6F7586240 (void);
// 0x00000095 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARFoundation.ARRaycastManager::RaycastViewportAsRay(UnityEngine.Vector2,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void ARRaycastManager_RaycastViewportAsRay_m15C127E7D6ED10D456B2C4C8571BB86CE1973C81 (void);
// 0x00000096 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARFoundation.ARRaycastManager::RaycastViewport(UnityEngine.Vector2,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void ARRaycastManager_RaycastViewport_mE0B2B4A53A3CAD20F00501F715DB4D266D3B55BF (void);
// 0x00000097 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARFoundation.ARRaycastManager::RaycastRay(UnityEngine.Ray,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void ARRaycastManager_RaycastRay_m4AE8E0B028B4126C87280B1CEE0A479D71EEF3B9 (void);
// 0x00000098 System.Int32 UnityEngine.XR.ARFoundation.ARRaycastManager::RaycastHitComparer(UnityEngine.XR.ARFoundation.ARRaycastHit,UnityEngine.XR.ARFoundation.ARRaycastHit)
extern void ARRaycastManager_RaycastHitComparer_mC2A9D36BC37C468BBE51196980393FC8916FB64F (void);
// 0x00000099 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARFoundation.ARRaycastManager::RaycastFallback(UnityEngine.Ray,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
extern void ARRaycastManager_RaycastFallback_m67FBA4A718FC08126B64E4895447FD4AF5C5B9CF (void);
// 0x0000009A System.Boolean UnityEngine.XR.ARFoundation.ARRaycastManager::TransformAndDisposeNativeHitResults(Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit>,System.Collections.Generic.List`1<UnityEngine.XR.ARFoundation.ARRaycastHit>,UnityEngine.Vector3)
extern void ARRaycastManager_TransformAndDisposeNativeHitResults_m8A369C106CD89AD3BDEFBA51AC220D6E631E5438 (void);
// 0x0000009B System.Void UnityEngine.XR.ARFoundation.ARRaycastManager::OnAfterSetSessionRelativeData(UnityEngine.XR.ARFoundation.ARRaycast,UnityEngine.XR.ARSubsystems.XRRaycast)
extern void ARRaycastManager_OnAfterSetSessionRelativeData_m7D3DADC477AC0DBDFFF3EB29E537B19EF8EBD763 (void);
// 0x0000009C System.String UnityEngine.XR.ARFoundation.ARRaycastManager::get_gameObjectName()
extern void ARRaycastManager_get_gameObjectName_mDC542442BB59C94E84269051FE9F6D02176470EF (void);
// 0x0000009D System.Void UnityEngine.XR.ARFoundation.ARRaycastManager::.ctor()
extern void ARRaycastManager__ctor_m5318CB3E2A40E89FBB333ED540805A8C155FACEB (void);
// 0x0000009E System.Void UnityEngine.XR.ARFoundation.ARRaycastManager::.cctor()
extern void ARRaycastManager__cctor_m4F7D8AFBCFB8EE07B9E991352AC74B843133A0DA (void);
// 0x0000009F UnityEngine.XR.ARFoundation.ARRaycast UnityEngine.XR.ARFoundation.ARRaycastUpdatedEventArgs::get_raycast()
extern void ARRaycastUpdatedEventArgs_get_raycast_m0609B624800A5388048A79B439B4B38F5F2A885F (void);
// 0x000000A0 System.Void UnityEngine.XR.ARFoundation.ARRaycastUpdatedEventArgs::set_raycast(UnityEngine.XR.ARFoundation.ARRaycast)
extern void ARRaycastUpdatedEventArgs_set_raycast_m7BE3386FAFCA2682E4B8ADDAAD3B0861CF7B6199 (void);
// 0x000000A1 System.Boolean UnityEngine.XR.ARFoundation.ARRaycastUpdatedEventArgs::Equals(UnityEngine.XR.ARFoundation.ARRaycastUpdatedEventArgs)
extern void ARRaycastUpdatedEventArgs_Equals_m99C427C07FE659083820EF17D021D78BBE7E0B86 (void);
// 0x000000A2 System.Boolean UnityEngine.XR.ARFoundation.ARRaycastUpdatedEventArgs::Equals(System.Object)
extern void ARRaycastUpdatedEventArgs_Equals_mAD79322F0D5530018B97925A99B19352DEB5A2F8 (void);
// 0x000000A3 System.Int32 UnityEngine.XR.ARFoundation.ARRaycastUpdatedEventArgs::GetHashCode()
extern void ARRaycastUpdatedEventArgs_GetHashCode_mE8AEACE0EA29276661BD1CF7E212E3652833044F (void);
// 0x000000A4 System.Boolean UnityEngine.XR.ARFoundation.ARSession::get_attemptUpdate()
extern void ARSession_get_attemptUpdate_m8A07979329A9676158CE1122FFFC573F9114A68A (void);
// 0x000000A5 System.Void UnityEngine.XR.ARFoundation.ARSession::set_attemptUpdate(System.Boolean)
extern void ARSession_set_attemptUpdate_mB93F0042D4CA826181527CA915D7022A8607C515 (void);
// 0x000000A6 System.Boolean UnityEngine.XR.ARFoundation.ARSession::get_matchFrameRate()
extern void ARSession_get_matchFrameRate_m52F8C79BE303168DC19A61E08957AC5E93367DC5 (void);
// 0x000000A7 System.Void UnityEngine.XR.ARFoundation.ARSession::set_matchFrameRate(System.Boolean)
extern void ARSession_set_matchFrameRate_mC6625E4B0C2EC397C541722463C6FA8385FF9B26 (void);
// 0x000000A8 System.Boolean UnityEngine.XR.ARFoundation.ARSession::get_matchFrameRateEnabled()
extern void ARSession_get_matchFrameRateEnabled_m54F5C93DC7746648A7535FF28E14F49B3ADF92CD (void);
// 0x000000A9 System.Boolean UnityEngine.XR.ARFoundation.ARSession::get_matchFrameRateRequested()
extern void ARSession_get_matchFrameRateRequested_m87736EDEA3ADB050FCBB44A2E55A71FEF9364548 (void);
// 0x000000AA System.Void UnityEngine.XR.ARFoundation.ARSession::set_matchFrameRateRequested(System.Boolean)
extern void ARSession_set_matchFrameRateRequested_mDC93048F4E690E8DED853E885C8DFF3CE8346623 (void);
// 0x000000AB UnityEngine.XR.ARFoundation.TrackingMode UnityEngine.XR.ARFoundation.ARSession::get_requestedTrackingMode()
extern void ARSession_get_requestedTrackingMode_m4F66072D778CF6008CAF3A8A0AC0B491435008AE (void);
// 0x000000AC System.Void UnityEngine.XR.ARFoundation.ARSession::set_requestedTrackingMode(UnityEngine.XR.ARFoundation.TrackingMode)
extern void ARSession_set_requestedTrackingMode_m88C66125B39BBD446153EF90805B9D68665EBECF (void);
// 0x000000AD UnityEngine.XR.ARFoundation.TrackingMode UnityEngine.XR.ARFoundation.ARSession::get_currentTrackingMode()
extern void ARSession_get_currentTrackingMode_m1AE17402BEB3E163983463AD5691FFA0EF47FFC3 (void);
// 0x000000AE System.Nullable`1<System.Int32> UnityEngine.XR.ARFoundation.ARSession::get_frameRate()
extern void ARSession_get_frameRate_m8C253E2EB41BDEE414614EA4C31001F74343BD4F (void);
// 0x000000AF System.Void UnityEngine.XR.ARFoundation.ARSession::add_stateChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs>)
extern void ARSession_add_stateChanged_m0B11F34533799F3DB6B188D44AB356574954D3DF (void);
// 0x000000B0 System.Void UnityEngine.XR.ARFoundation.ARSession::remove_stateChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs>)
extern void ARSession_remove_stateChanged_m2A25BD04AD425F21552A6057A35701600BEB9451 (void);
// 0x000000B1 UnityEngine.XR.ARFoundation.ARSessionState UnityEngine.XR.ARFoundation.ARSession::get_state()
extern void ARSession_get_state_m623161F1E2E5BA2752C821DD409880E6647CA130 (void);
// 0x000000B2 System.Void UnityEngine.XR.ARFoundation.ARSession::set_state(UnityEngine.XR.ARFoundation.ARSessionState)
extern void ARSession_set_state_m74178050EEB5D0A2C787009344AB9C917C0AEEFB (void);
// 0x000000B3 UnityEngine.XR.ARSubsystems.NotTrackingReason UnityEngine.XR.ARFoundation.ARSession::get_notTrackingReason()
extern void ARSession_get_notTrackingReason_m346D5BD4415CD34223970E3FAF019A78E86B606F (void);
// 0x000000B4 System.Void UnityEngine.XR.ARFoundation.ARSession::Reset()
extern void ARSession_Reset_m3C78CB4465887888EE1A5EEBCCD47E8D462A1116 (void);
// 0x000000B5 System.Void UnityEngine.XR.ARFoundation.ARSession::SetMatchFrameRateRequested(System.Boolean)
extern void ARSession_SetMatchFrameRateRequested_mABECAD724B851AD673474DB26FDF256292EEBA3F (void);
// 0x000000B6 System.Void UnityEngine.XR.ARFoundation.ARSession::WarnIfMultipleARSessions()
extern void ARSession_WarnIfMultipleARSessions_m193EF1C5A32379298AFA36D6637E924FD0E161A9 (void);
// 0x000000B7 UnityEngine.XR.ARSubsystems.XRSessionSubsystem UnityEngine.XR.ARFoundation.ARSession::GetSubsystem()
extern void ARSession_GetSubsystem_m0721DA6041E5FDD2E29D741C678C16CD84F26B79 (void);
// 0x000000B8 System.Collections.IEnumerator UnityEngine.XR.ARFoundation.ARSession::CheckAvailability()
extern void ARSession_CheckAvailability_mCC906561CCCE269C11B69D2216C39120563F00FD (void);
// 0x000000B9 System.Collections.IEnumerator UnityEngine.XR.ARFoundation.ARSession::Install()
extern void ARSession_Install_mF45AE40F0F8A22E4EEF960BEBD434A7F7591CA67 (void);
// 0x000000BA System.Void UnityEngine.XR.ARFoundation.ARSession::OnEnable()
extern void ARSession_OnEnable_mA368B8911A5ABF1EF0C44B636723D6981ABF4714 (void);
// 0x000000BB System.Collections.IEnumerator UnityEngine.XR.ARFoundation.ARSession::Initialize()
extern void ARSession_Initialize_m0FE9C0B5D3007D3D127FFF52674255F4CCAFF808 (void);
// 0x000000BC System.Void UnityEngine.XR.ARFoundation.ARSession::StartSubsystem()
extern void ARSession_StartSubsystem_m4A470169DE740C623C2C770AEA105859493C0DA8 (void);
// 0x000000BD System.Void UnityEngine.XR.ARFoundation.ARSession::Awake()
extern void ARSession_Awake_m9024E982F0CD73C9C679A3DA6FAACE0FFCD39A34 (void);
// 0x000000BE System.Void UnityEngine.XR.ARFoundation.ARSession::Update()
extern void ARSession_Update_mF2C8C33579C2D606200802FE4F7668268DAD1828 (void);
// 0x000000BF System.Void UnityEngine.XR.ARFoundation.ARSession::OnApplicationPause(System.Boolean)
extern void ARSession_OnApplicationPause_mEA4B2C6D69B1D3CB247CC36C219298F38FC42EBE (void);
// 0x000000C0 System.Void UnityEngine.XR.ARFoundation.ARSession::OnDisable()
extern void ARSession_OnDisable_m2C35032CBD09545363AB5877C9CFDD89226931AD (void);
// 0x000000C1 System.Void UnityEngine.XR.ARFoundation.ARSession::OnDestroy()
extern void ARSession_OnDestroy_m494EC9644E21D53BCFB5BF1FB000477AA60116C9 (void);
// 0x000000C2 System.Void UnityEngine.XR.ARFoundation.ARSession::UpdateNotTrackingReason()
extern void ARSession_UpdateNotTrackingReason_m48F2669C645017758983AF46A48444849373849A (void);
// 0x000000C3 System.Void UnityEngine.XR.ARFoundation.ARSession::.ctor()
extern void ARSession__ctor_mA9A989B079A758DBA381FEECE8F3377F45F96E9C (void);
// 0x000000C4 System.Void UnityEngine.XR.ARFoundation.ARSession/<CheckAvailability>d__33::.ctor(System.Int32)
extern void U3CCheckAvailabilityU3Ed__33__ctor_mCA3C75BF2A10DAEF6340EEE3AA96787CE9D43AD6 (void);
// 0x000000C5 System.Void UnityEngine.XR.ARFoundation.ARSession/<CheckAvailability>d__33::System.IDisposable.Dispose()
extern void U3CCheckAvailabilityU3Ed__33_System_IDisposable_Dispose_mFC4943930186516D05B109AF2FA2F9F3941BB406 (void);
// 0x000000C6 System.Boolean UnityEngine.XR.ARFoundation.ARSession/<CheckAvailability>d__33::MoveNext()
extern void U3CCheckAvailabilityU3Ed__33_MoveNext_mE7DDB20C3E68ED1E3C5C9AD093583E8634671D23 (void);
// 0x000000C7 System.Object UnityEngine.XR.ARFoundation.ARSession/<CheckAvailability>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckAvailabilityU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m04253DF005179DA04D14FAAF9B88492B0B249A36 (void);
// 0x000000C8 System.Void UnityEngine.XR.ARFoundation.ARSession/<CheckAvailability>d__33::System.Collections.IEnumerator.Reset()
extern void U3CCheckAvailabilityU3Ed__33_System_Collections_IEnumerator_Reset_mB34A16CDDAD5F80D161C66EBA84D985634D02F84 (void);
// 0x000000C9 System.Object UnityEngine.XR.ARFoundation.ARSession/<CheckAvailability>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CCheckAvailabilityU3Ed__33_System_Collections_IEnumerator_get_Current_m5F68AE4C034BA873619CA65A51B15247D937A534 (void);
// 0x000000CA System.Void UnityEngine.XR.ARFoundation.ARSession/<Install>d__34::.ctor(System.Int32)
extern void U3CInstallU3Ed__34__ctor_mB10A644A399CAFF7AB219EB64D3FDAD2E5FB8603 (void);
// 0x000000CB System.Void UnityEngine.XR.ARFoundation.ARSession/<Install>d__34::System.IDisposable.Dispose()
extern void U3CInstallU3Ed__34_System_IDisposable_Dispose_mB443F32C58F5487A5014937CC18849C8D060F98D (void);
// 0x000000CC System.Boolean UnityEngine.XR.ARFoundation.ARSession/<Install>d__34::MoveNext()
extern void U3CInstallU3Ed__34_MoveNext_m1C1F4EF15039976EDE584C6F71E1A7A808549767 (void);
// 0x000000CD System.Object UnityEngine.XR.ARFoundation.ARSession/<Install>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInstallU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6C0A95CD160AECECA459DBA361BFE015BDF06783 (void);
// 0x000000CE System.Void UnityEngine.XR.ARFoundation.ARSession/<Install>d__34::System.Collections.IEnumerator.Reset()
extern void U3CInstallU3Ed__34_System_Collections_IEnumerator_Reset_m48F87F38059EF06A00F6EB1CE1F4E3BA76CE953A (void);
// 0x000000CF System.Object UnityEngine.XR.ARFoundation.ARSession/<Install>d__34::System.Collections.IEnumerator.get_Current()
extern void U3CInstallU3Ed__34_System_Collections_IEnumerator_get_Current_m4ACDB1ECA51128D6DD766DD902377440DF4E8A29 (void);
// 0x000000D0 System.Void UnityEngine.XR.ARFoundation.ARSession/<Initialize>d__36::.ctor(System.Int32)
extern void U3CInitializeU3Ed__36__ctor_m50BF8F1AEBF98425C1898C6BC8FE52FED8274DE8 (void);
// 0x000000D1 System.Void UnityEngine.XR.ARFoundation.ARSession/<Initialize>d__36::System.IDisposable.Dispose()
extern void U3CInitializeU3Ed__36_System_IDisposable_Dispose_m223C7A21303665BF07B1F54D823BB2A620660EF8 (void);
// 0x000000D2 System.Boolean UnityEngine.XR.ARFoundation.ARSession/<Initialize>d__36::MoveNext()
extern void U3CInitializeU3Ed__36_MoveNext_mA09F10F396058FD8ECEAC71BFC432715C32B9FB3 (void);
// 0x000000D3 System.Object UnityEngine.XR.ARFoundation.ARSession/<Initialize>d__36::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitializeU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFC5A2125CEA50D8FD28766B4C38C4257CE59CDF7 (void);
// 0x000000D4 System.Void UnityEngine.XR.ARFoundation.ARSession/<Initialize>d__36::System.Collections.IEnumerator.Reset()
extern void U3CInitializeU3Ed__36_System_Collections_IEnumerator_Reset_m01FA7C362A9D4A6A2BE1C3694B777867F492171F (void);
// 0x000000D5 System.Object UnityEngine.XR.ARFoundation.ARSession/<Initialize>d__36::System.Collections.IEnumerator.get_Current()
extern void U3CInitializeU3Ed__36_System_Collections_IEnumerator_get_Current_m054DDB2DE2181B115B6CE16D08BB8953178F4659 (void);
// 0x000000D6 UnityEngine.Camera UnityEngine.XR.ARFoundation.ARSessionOrigin::get_camera()
extern void ARSessionOrigin_get_camera_m06DE26C86E4FD1149361159F5609734F989C23D3 (void);
// 0x000000D7 System.Void UnityEngine.XR.ARFoundation.ARSessionOrigin::set_camera(UnityEngine.Camera)
extern void ARSessionOrigin_set_camera_m5280D9B057050AEB45F58F542F6CC81930D8D908 (void);
// 0x000000D8 UnityEngine.Transform UnityEngine.XR.ARFoundation.ARSessionOrigin::get_trackablesParent()
extern void ARSessionOrigin_get_trackablesParent_mC232717A3F6993690E5A68E1CD17B25F7843C634 (void);
// 0x000000D9 System.Void UnityEngine.XR.ARFoundation.ARSessionOrigin::set_trackablesParent(UnityEngine.Transform)
extern void ARSessionOrigin_set_trackablesParent_mEDC8CC36C64F3478FE5CB37D306D41FC2D0AB7B4 (void);
// 0x000000DA System.Void UnityEngine.XR.ARFoundation.ARSessionOrigin::add_trackablesParentTransformChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARTrackablesParentTransformChangedEventArgs>)
extern void ARSessionOrigin_add_trackablesParentTransformChanged_m9936C7B9DA33251D5DDF255C31CB9352F8F1A527 (void);
// 0x000000DB System.Void UnityEngine.XR.ARFoundation.ARSessionOrigin::remove_trackablesParentTransformChanged(System.Action`1<UnityEngine.XR.ARFoundation.ARTrackablesParentTransformChangedEventArgs>)
extern void ARSessionOrigin_remove_trackablesParentTransformChanged_mAA1A639505243A4AC78177CC703EDE41F76AAE90 (void);
// 0x000000DC UnityEngine.Transform UnityEngine.XR.ARFoundation.ARSessionOrigin::get_contentOffsetTransform()
extern void ARSessionOrigin_get_contentOffsetTransform_mA49736F93177F052B00CC6CEE06833F8D278CF59 (void);
// 0x000000DD System.Void UnityEngine.XR.ARFoundation.ARSessionOrigin::MakeContentAppearAt(UnityEngine.Transform,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void ARSessionOrigin_MakeContentAppearAt_m741B35243FFD56004B8FEC762A7FE9759B4D6603 (void);
// 0x000000DE System.Void UnityEngine.XR.ARFoundation.ARSessionOrigin::MakeContentAppearAt(UnityEngine.Transform,UnityEngine.Vector3)
extern void ARSessionOrigin_MakeContentAppearAt_m0BAF5FBD841FD67517431E8D81773968CE457608 (void);
// 0x000000DF System.Void UnityEngine.XR.ARFoundation.ARSessionOrigin::MakeContentAppearAt(UnityEngine.Transform,UnityEngine.Quaternion)
extern void ARSessionOrigin_MakeContentAppearAt_mE5B578D7C44877F95C526480F873124E60549CB0 (void);
// 0x000000E0 System.Void UnityEngine.XR.ARFoundation.ARSessionOrigin::Awake()
extern void ARSessionOrigin_Awake_m9EDF100DB525494EB21B07082DC5CACA63667DF6 (void);
// 0x000000E1 UnityEngine.Pose UnityEngine.XR.ARFoundation.ARSessionOrigin::GetCameraOriginPose()
extern void ARSessionOrigin_GetCameraOriginPose_m796F542B04BE57748CFB98A759A6204FD3AAA7CC (void);
// 0x000000E2 System.Void UnityEngine.XR.ARFoundation.ARSessionOrigin::OnEnable()
extern void ARSessionOrigin_OnEnable_m5B3860FC541DC44A3F186C4E2F1CABB74F074D3A (void);
// 0x000000E3 System.Void UnityEngine.XR.ARFoundation.ARSessionOrigin::OnDisable()
extern void ARSessionOrigin_OnDisable_m09E783B9894438CE2B2B28603EDA70F860C1EDB4 (void);
// 0x000000E4 System.Void UnityEngine.XR.ARFoundation.ARSessionOrigin::OnBeforeRender()
extern void ARSessionOrigin_OnBeforeRender_mB0B28F0BD8F27D7BD3499C8D55E19963C0D35547 (void);
// 0x000000E5 System.Void UnityEngine.XR.ARFoundation.ARSessionOrigin::.ctor()
extern void ARSessionOrigin__ctor_m0E3C505EA0D5537EC2DCFB047C67BF39B6E334CF (void);
// 0x000000E6 UnityEngine.XR.ARFoundation.ARSessionState UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs::get_state()
extern void ARSessionStateChangedEventArgs_get_state_mDFCCADECEFE9356E4246B9BF912157F3EDD88796 (void);
// 0x000000E7 System.Void UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs::set_state(UnityEngine.XR.ARFoundation.ARSessionState)
extern void ARSessionStateChangedEventArgs_set_state_mA9AB6CBFCAD476EC94E3642F5638BCC11B4A4B95 (void);
// 0x000000E8 System.Void UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs::.ctor(UnityEngine.XR.ARFoundation.ARSessionState)
extern void ARSessionStateChangedEventArgs__ctor_m6001FA2930CF6A8551451A20129E7827738E0AA9 (void);
// 0x000000E9 System.Int32 UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs::GetHashCode()
extern void ARSessionStateChangedEventArgs_GetHashCode_m00DDAFEED0CA796E059FBB5845B62DA16DA90BFF (void);
// 0x000000EA System.Boolean UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs::Equals(System.Object)
extern void ARSessionStateChangedEventArgs_Equals_m55B6A7D08E747098F36B59A6B3CAA8DB392E3454 (void);
// 0x000000EB System.String UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs::ToString()
extern void ARSessionStateChangedEventArgs_ToString_m415BD73E732AE375F1BDF0990604D5FA0DD87A44 (void);
// 0x000000EC System.Boolean UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs::Equals(UnityEngine.XR.ARFoundation.ARSessionStateChangedEventArgs)
extern void ARSessionStateChangedEventArgs_Equals_mC4C56CAF33303BA032420A206F358AE4AEF4181A (void);
// 0x000000ED System.Void UnityEngine.XR.ARFoundation.ARTrackable::.ctor()
extern void ARTrackable__ctor_m7430516DC3D8985F9C8E2C82DAA05C2DAF4B90B1 (void);
// 0x000000EE System.Boolean UnityEngine.XR.ARFoundation.ARTrackable`2::get_destroyOnRemoval()
// 0x000000EF System.Void UnityEngine.XR.ARFoundation.ARTrackable`2::set_destroyOnRemoval(System.Boolean)
// 0x000000F0 UnityEngine.XR.ARSubsystems.TrackableId UnityEngine.XR.ARFoundation.ARTrackable`2::get_trackableId()
// 0x000000F1 UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.ARFoundation.ARTrackable`2::get_trackingState()
// 0x000000F2 System.Boolean UnityEngine.XR.ARFoundation.ARTrackable`2::get_pending()
// 0x000000F3 System.Void UnityEngine.XR.ARFoundation.ARTrackable`2::set_pending(System.Boolean)
// 0x000000F4 TSessionRelativeData UnityEngine.XR.ARFoundation.ARTrackable`2::get_sessionRelativeData()
// 0x000000F5 System.Void UnityEngine.XR.ARFoundation.ARTrackable`2::set_sessionRelativeData(TSessionRelativeData)
// 0x000000F6 System.Void UnityEngine.XR.ARFoundation.ARTrackable`2::OnAfterSetSessionRelativeData()
// 0x000000F7 System.Void UnityEngine.XR.ARFoundation.ARTrackable`2::SetSessionRelativeData(TSessionRelativeData)
// 0x000000F8 UnityEngine.Pose UnityEngine.XR.ARFoundation.ARTrackable`2::get_sessionRelativePose()
// 0x000000F9 System.Void UnityEngine.XR.ARFoundation.ARTrackable`2::.ctor()
// 0x000000FA UnityEngine.XR.ARFoundation.ARTrackableManager`5<TSubsystem,TSubsystemDescriptor,TProvider,TSessionRelativeData,TTrackable> UnityEngine.XR.ARFoundation.ARTrackableManager`5::get_instance()
// 0x000000FB System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`5::set_instance(UnityEngine.XR.ARFoundation.ARTrackableManager`5<TSubsystem,TSubsystemDescriptor,TProvider,TSessionRelativeData,TTrackable>)
// 0x000000FC UnityEngine.XR.ARFoundation.TrackableCollection`1<TTrackable> UnityEngine.XR.ARFoundation.ARTrackableManager`5::get_trackables()
// 0x000000FD System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`5::SetTrackablesActive(System.Boolean)
// 0x000000FE UnityEngine.XR.ARFoundation.ARSessionOrigin UnityEngine.XR.ARFoundation.ARTrackableManager`5::get_sessionOrigin()
// 0x000000FF System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`5::set_sessionOrigin(UnityEngine.XR.ARFoundation.ARSessionOrigin)
// 0x00000100 System.String UnityEngine.XR.ARFoundation.ARTrackableManager`5::get_gameObjectName()
// 0x00000101 UnityEngine.GameObject UnityEngine.XR.ARFoundation.ARTrackableManager`5::GetPrefab()
// 0x00000102 System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`5::Awake()
// 0x00000103 System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`5::OnEnable()
// 0x00000104 System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`5::OnDisable()
// 0x00000105 System.Boolean UnityEngine.XR.ARFoundation.ARTrackableManager`5::CanBeAddedToSubsystem(TTrackable)
// 0x00000106 System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`5::OnTrackablesParentTransformChanged(UnityEngine.XR.ARFoundation.ARTrackablesParentTransformChangedEventArgs)
// 0x00000107 System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`5::Update()
// 0x00000108 System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`5::OnTrackablesChanged(System.Collections.Generic.List`1<TTrackable>,System.Collections.Generic.List`1<TTrackable>,System.Collections.Generic.List`1<TTrackable>)
// 0x00000109 System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`5::OnCreateTrackable(TTrackable)
// 0x0000010A System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`5::OnAfterSetSessionRelativeData(TTrackable,TSessionRelativeData)
// 0x0000010B TTrackable UnityEngine.XR.ARFoundation.ARTrackableManager`5::CreateTrackableImmediate(TSessionRelativeData)
// 0x0000010C System.Boolean UnityEngine.XR.ARFoundation.ARTrackableManager`5::DestroyPendingTrackable(UnityEngine.XR.ARSubsystems.TrackableId)
// 0x0000010D System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`5::ClearAndSetCapacity(System.Collections.Generic.List`1<TTrackable>,System.Int32)
// 0x0000010E System.String UnityEngine.XR.ARFoundation.ARTrackableManager`5::GetTrackableName(UnityEngine.XR.ARSubsystems.TrackableId)
// 0x0000010F System.ValueTuple`2<UnityEngine.GameObject,System.Boolean> UnityEngine.XR.ARFoundation.ARTrackableManager`5::CreateGameObjectDeactivated()
// 0x00000110 System.ValueTuple`2<UnityEngine.GameObject,System.Boolean> UnityEngine.XR.ARFoundation.ARTrackableManager`5::CreateGameObjectDeactivated(System.String)
// 0x00000111 System.ValueTuple`2<UnityEngine.GameObject,System.Boolean> UnityEngine.XR.ARFoundation.ARTrackableManager`5::CreateGameObjectDeactivated(UnityEngine.XR.ARSubsystems.TrackableId)
// 0x00000112 TTrackable UnityEngine.XR.ARFoundation.ARTrackableManager`5::CreateTrackable(TSessionRelativeData)
// 0x00000113 System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`5::SetSessionRelativeData(TTrackable,TSessionRelativeData)
// 0x00000114 System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`5::CreateTrackableFromExisting(TTrackable,TSessionRelativeData)
// 0x00000115 TTrackable UnityEngine.XR.ARFoundation.ARTrackableManager`5::CreateOrUpdateTrackable(TSessionRelativeData)
// 0x00000116 System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`5::DestroyTrackable(TTrackable)
// 0x00000117 System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`5::.ctor()
// 0x00000118 System.Void UnityEngine.XR.ARFoundation.ARTrackableManager`5::.cctor()
// 0x00000119 UnityEngine.XR.ARFoundation.ARSessionOrigin UnityEngine.XR.ARFoundation.ARTrackablesParentTransformChangedEventArgs::get_sessionOrigin()
extern void ARTrackablesParentTransformChangedEventArgs_get_sessionOrigin_m4A650B7FFE29FC40DA7781843DF2AE744108FBBA (void);
// 0x0000011A UnityEngine.Transform UnityEngine.XR.ARFoundation.ARTrackablesParentTransformChangedEventArgs::get_trackablesParent()
extern void ARTrackablesParentTransformChangedEventArgs_get_trackablesParent_m996D495C51CD1A26A40FB44EF841E824EF87711E (void);
// 0x0000011B System.Void UnityEngine.XR.ARFoundation.ARTrackablesParentTransformChangedEventArgs::.ctor(UnityEngine.XR.ARFoundation.ARSessionOrigin,UnityEngine.Transform)
extern void ARTrackablesParentTransformChangedEventArgs__ctor_m45E72BC55106F4DFE8E9DBC77C2D33213203B2B6 (void);
// 0x0000011C System.Boolean UnityEngine.XR.ARFoundation.ARTrackablesParentTransformChangedEventArgs::Equals(UnityEngine.XR.ARFoundation.ARTrackablesParentTransformChangedEventArgs)
extern void ARTrackablesParentTransformChangedEventArgs_Equals_mEFECB01DA6949C437802A4E65FFC803FD0BBA603 (void);
// 0x0000011D System.Boolean UnityEngine.XR.ARFoundation.ARTrackablesParentTransformChangedEventArgs::Equals(System.Object)
extern void ARTrackablesParentTransformChangedEventArgs_Equals_m31CF1D718EFE540CACA9C601CA7B9E2666AA2361 (void);
// 0x0000011E System.Int32 UnityEngine.XR.ARFoundation.ARTrackablesParentTransformChangedEventArgs::GetHashCode()
extern void ARTrackablesParentTransformChangedEventArgs_GetHashCode_mCC1795A3F585FA0C88849C76D602A0B7F6A0F620 (void);
// 0x0000011F System.Int32 UnityEngine.XR.ARFoundation.HashCode::Combine(System.Int32,System.Int32)
extern void HashCode_Combine_mB733DE32A13A557A4EBF6B13881920FACBCD7C8D (void);
// 0x00000120 System.Int32 UnityEngine.XR.ARFoundation.HashCode::ReferenceHash(System.Object)
extern void HashCode_ReferenceHash_m8E89AD422634F94074CADFA2642183BF353D150F (void);
// 0x00000121 System.Int32 UnityEngine.XR.ARFoundation.HashCode::Combine(System.Int32,System.Int32,System.Int32)
extern void HashCode_Combine_mE6B8F1B5170F1B7A7AC2B7EF8BF9739E96CAFB3D (void);
// 0x00000122 System.Int32 UnityEngine.XR.ARFoundation.HashCode::Combine(System.Int32,System.Int32,System.Int32,System.Int32)
extern void HashCode_Combine_mD76CDEE1565B93AFF8FDBE56E7C33C971BCB012F (void);
// 0x00000123 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRRaycastHit> UnityEngine.XR.ARFoundation.IRaycaster::Raycast(UnityEngine.Ray,UnityEngine.XR.ARSubsystems.TrackableType,Unity.Collections.Allocator)
// 0x00000124 System.Void UnityEngine.XR.ARFoundation.PlaneDetectionModeMaskAttribute::.ctor()
extern void PlaneDetectionModeMaskAttribute__ctor_m7D29BF65AB4C7AFAB629A57F74DE67ED29FFEB1C (void);
// 0x00000125 TSubsystem UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`3::get_subsystem()
// 0x00000126 System.Void UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`3::set_subsystem(TSubsystem)
// 0x00000127 TSubsystemDescriptor UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`3::get_descriptor()
// 0x00000128 TSubsystem UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`3::GetActiveSubsystemInstance()
// 0x00000129 System.Void UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`3::EnsureSubsystemInstanceSet()
// 0x0000012A System.Void UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`3::OnEnable()
// 0x0000012B System.Void UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`3::OnDisable()
// 0x0000012C System.Void UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`3::OnDestroy()
// 0x0000012D System.Void UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`3::OnBeforeStart()
// 0x0000012E System.Void UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`3::OnAfterStart()
// 0x0000012F System.Void UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`3::.ctor()
// 0x00000130 System.Void UnityEngine.XR.ARFoundation.SubsystemLifecycleManager`3::.cctor()
// 0x00000131 UnityEngine.XR.ARFoundation.TrackableCollection`1/Enumerator<TTrackable> UnityEngine.XR.ARFoundation.TrackableCollection`1::GetEnumerator()
// 0x00000132 System.Void UnityEngine.XR.ARFoundation.TrackableCollection`1::.ctor(System.Collections.Generic.Dictionary`2<UnityEngine.XR.ARSubsystems.TrackableId,TTrackable>)
// 0x00000133 System.Int32 UnityEngine.XR.ARFoundation.TrackableCollection`1::get_count()
// 0x00000134 System.Int32 UnityEngine.XR.ARFoundation.TrackableCollection`1::GetHashCode()
// 0x00000135 System.Boolean UnityEngine.XR.ARFoundation.TrackableCollection`1::Equals(System.Object)
// 0x00000136 System.Boolean UnityEngine.XR.ARFoundation.TrackableCollection`1::Equals(UnityEngine.XR.ARFoundation.TrackableCollection`1<TTrackable>)
// 0x00000137 System.Void UnityEngine.XR.ARFoundation.TrackableCollection`1/Enumerator::.ctor(System.Collections.Generic.Dictionary`2<UnityEngine.XR.ARSubsystems.TrackableId,TTrackable>)
// 0x00000138 System.Boolean UnityEngine.XR.ARFoundation.TrackableCollection`1/Enumerator::MoveNext()
// 0x00000139 TTrackable UnityEngine.XR.ARFoundation.TrackableCollection`1/Enumerator::get_Current()
// 0x0000013A UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.ARFoundation.TrackingModeExtensions::ToFeature(UnityEngine.XR.ARFoundation.TrackingMode)
extern void TrackingModeExtensions_ToFeature_mC09F4526E4C8B7459305E10902A0698F8AD3F1BD (void);
// 0x0000013B UnityEngine.XR.ARFoundation.TrackingMode UnityEngine.XR.ARFoundation.TrackingModeExtensions::ToTrackingMode(UnityEngine.XR.ARSubsystems.Feature)
extern void TrackingModeExtensions_ToTrackingMode_m1AD1AC6E4580B57E09536930C4A107E61CB8912E (void);
// 0x0000013C UnityEngine.Ray UnityEngine.XR.ARFoundation.TransformExtensions::InverseTransformRay(UnityEngine.Transform,UnityEngine.Ray)
extern void TransformExtensions_InverseTransformRay_m6C3083362DA474AF08321E73612E373771E5CA3A (void);
// 0x0000013D UnityEngine.Pose UnityEngine.XR.ARFoundation.TransformExtensions::TransformPose(UnityEngine.Transform,UnityEngine.Pose)
extern void TransformExtensions_TransformPose_mD57F5882BD08CAFB48396780B469DA938E816A4B (void);
// 0x0000013E UnityEngine.Pose UnityEngine.XR.ARFoundation.TransformExtensions::InverseTransformPose(UnityEngine.Transform,UnityEngine.Pose)
extern void TransformExtensions_InverseTransformPose_m6143382A59744E50CB53B9718F8F21B18C692E4C (void);
static Il2CppMethodPointer s_methodPointers[318] = 
{
	ARAnchor_get_nativePtr_mAFCE600DF5E1231E8349DDD3441EE0DC05304BD7,
	ARAnchor_get_sessionId_m2932E088EB59231C0D76273DEAD10D56C8631560,
	ARAnchor_OnEnable_m9706E493783C175B28DE168C67B54AD3DDE3F3A9,
	ARAnchor_Update_m471443F726BC7814024765A4ADCEE55667AAA6A5,
	ARAnchor_OnDisable_mA724EEFA3739BD8A09A61F02D3DDEAA6DD6E0872,
	ARAnchor__ctor_m8AC4C705DEC2C70EEF810D9C4EABC0C01B3C2225,
	ARAnchorManager_get_anchorPrefab_m83A6325983B087F873F4A2D77A9959DA3DE396F8,
	ARAnchorManager_set_anchorPrefab_m42038AAD8D7B70F498EA0429CDFBF1E18DA38756,
	ARAnchorManager_add_anchorsChanged_m32E33C78B20F072111FEBBDB2AFEF1F8B25EA367,
	ARAnchorManager_remove_anchorsChanged_m36F2B944753B78D564BDFD3D7118934AD836AB6B,
	ARAnchorManager_AddAnchor_mBE78F22E6E4BE754EF0210E24E63E17EBEC5A3DC,
	ARAnchorManager_TryAddAnchor_mC5522D55076F7D56771AD3334996162B4AF3FCAF,
	ARAnchorManager_AttachAnchor_m09F3BB1DC64599205719BE1F28AE2610DECF5825,
	ARAnchorManager_RemoveAnchor_m1D2D061879D97BCCCFF3267A44A7D7E43F8799A0,
	ARAnchorManager_TryRemoveAnchor_mEA631AE447C8251AC439E9D9383DA9ABD0A7975E,
	ARAnchorManager_GetAnchor_mDA50990B270097F8544840C0DFC14FF7419742FA,
	ARAnchorManager_GetPrefab_m854DECEB66697A6104D8D5F22DAFC927A7AFE510,
	ARAnchorManager_get_gameObjectName_m46C6FAA9D14AADDF62B2ECA7B8BB3094A55F00B1,
	ARAnchorManager_OnTrackablesChanged_m4D4B7E050451FB393240E94551B80C055CEE3A63,
	ARAnchorManager__ctor_m6A7DB89B5F9DE5A22FD7FE1E85D35A5000D06F3E,
	ARAnchorsChangedEventArgs_get_added_m4D07DC13F8D06201C220964F8CCE1CD99F4342E8,
	ARAnchorsChangedEventArgs_set_added_m250070DF629DAAF526CE631D008D1320DE449A40,
	ARAnchorsChangedEventArgs_get_updated_m87B120B079D1B9FF8C9F1E305672BD6F741AC7DF,
	ARAnchorsChangedEventArgs_set_updated_m3A0ED32E6B56174861DD6431E0C50AE016FC9826,
	ARAnchorsChangedEventArgs_get_removed_m3DA7093629E8D772C2732299D5F612B1357F6D08,
	ARAnchorsChangedEventArgs_set_removed_m96CA8069483206C90B983B2BCF00F8AB60FC6985,
	ARAnchorsChangedEventArgs__ctor_m51231F5A1B806EDD5E2A37C514A81BA87AC29DB5,
	ARAnchorsChangedEventArgs_GetHashCode_m9F8FD840B1A6562E4B3CF4A3B5A7175A49D136D0,
	ARAnchorsChangedEventArgs_Equals_mBB1A87EB69C9AED17A688CAF715718D87FD027D6,
	ARAnchorsChangedEventArgs_ToString_m2C6B93BD6C868C7243C104B916EC06E871AABC5A,
	ARAnchorsChangedEventArgs_Equals_m007C8B067E9DF6AEE162E8BF8889419E0E183961,
	ARPlane_get_vertexChangedThreshold_mAA60F671E3E504C8728555CA7CE9EE1622EF9288,
	ARPlane_set_vertexChangedThreshold_m04C245B86A30F5D34F9D470FB04AC8B8BBEA9A2D,
	ARPlane_add_boundaryChanged_m2C6F18F6A876A0CDC182820725A38A534B988BEE,
	ARPlane_remove_boundaryChanged_m806691C95E05CDBCB7637F6070F30860D9BF403B,
	ARPlane_get_normal_m5D251614E45E0F89C3525A1EA8BF0A23B1FED8DB,
	ARPlane_get_subsumedBy_m4A7A81ED11E4EF5CE7A82E9E3E1BF33620B8C4C3,
	ARPlane_set_subsumedBy_mF2FA42EB5816B76672003101C8F4E56698B6126A,
	ARPlane_get_alignment_mE98AB13AA23A17118D57E28213955B32F96B7046,
	ARPlane_get_classification_mE63F0D4D9B17E53D3F58F7A4BEDFF1DDE28C829E,
	ARPlane_get_centerInPlaneSpace_m243BBB28377DBE0039805A0E271B6BF0879FB437,
	ARPlane_get_center_mEDC87E2E4C5029D34515F17E9B2D80B9125B5484,
	ARPlane_get_extents_m15E7D52F5BBB68F8EF0871A573894309B1D9B623,
	ARPlane_get_size_m3848DFA43414D59474E29C8F4ED3DAB848A27D18,
	ARPlane_get_infinitePlane_mF31BA660A9854B85EBE1079763B6C3F702425963,
	ARPlane_get_nativePtr_m83BE2C3F5EADF8CE66FCF7045A4AB318F91F97A9,
	ARPlane_get_boundary_m36CBBE48CE6D88237DF3394250A83A69C4E7F391,
	ARPlane_UpdateBoundary_m2108B0805369F86A19E689A5FC3A403D78AB0396,
	ARPlane_OnValidate_mA4A9679E135EC8A45942CF35E2E5E90544D5383D,
	ARPlane_OnDestroy_mD8358BCB71AFF7210A0612DB75707D8F0B3A5684,
	ARPlane_CheckForBoundaryChanges_m9C3F3B40C724A8F16196F81D8858AB67F33799F1,
	ARPlane_CopyBoundaryAndSetChangedFlag_m782C386656526707A27FFAA2AA43586205601346,
	ARPlane_Update_m11B70239AFD5ED715614EAAB6BF71C25D67ACE27,
	ARPlane__ctor_m0408A53F8E2054DB1A48292C0F80EC228711399A,
	ARPlaneBoundaryChangedEventArgs_get_plane_mC91E64413E8593C0137A7C47485A1238180932A5,
	ARPlaneBoundaryChangedEventArgs_set_plane_mEDFCCE4100F4480E53F3EE12F4F89DB7054AE6D4,
	ARPlaneBoundaryChangedEventArgs__ctor_mCD138E5FFD74678A29B6A494550C428AE7344678,
	ARPlaneBoundaryChangedEventArgs_GetHashCode_m01011EEB2FAD823382934CDF5710CE259A212EB4,
	ARPlaneBoundaryChangedEventArgs_Equals_m9BBA8DDF067E8DC0BB0956C0BD4FF95B78C9DA1B,
	ARPlaneBoundaryChangedEventArgs_ToString_mB9D8B3B3DD1965A75E8CBE0F1133DA0C4310D28F,
	ARPlaneBoundaryChangedEventArgs_Equals_m8FCE506D26E362A259FD1E260648E9FA5C07B91D,
	ARPlaneManager_get_planePrefab_m3C00DB153DAE1813C0174CE0668913D2D2F74AE6,
	ARPlaneManager_set_planePrefab_m411D7254A8FF9891E6DCB7E1BE9C64052D4565FC,
	ARPlaneManager_get_detectionMode_mFA03DF070244B75712F5EB76B01C56B6ECD9C92D,
	ARPlaneManager_set_detectionMode_m9751E405615DF5749E5CABFF3AD5178A907AB540,
	ARPlaneManager_get_requestedDetectionMode_m7AB0F69B8A0EC9A8A76C1303621BF3932FF1B614,
	ARPlaneManager_set_requestedDetectionMode_mBAE613D44F9110CF01D898BC68AD02914AB23A6E,
	ARPlaneManager_get_currentDetectionMode_m12AFC03E86949D9D354D68F340332ADDBD79E5DE,
	ARPlaneManager_add_planesChanged_m785106C77BC4F503A9AB904E9BEF3EDCCF49FD27,
	ARPlaneManager_remove_planesChanged_mF9F02F508C2B19FFB3F6B5738B00C2C618B4C722,
	ARPlaneManager_GetPlane_m2863C99ED9ACEE22F720067A1E0AC6C55E743442,
	ARPlaneManager_Raycast_mAE3382E17ADFF506CF71B72705CF8C25AA264C79,
	ARPlaneManager_GetCrossDirection_m6929DD8D3045AC8AE5F9A816F94F7EBDB0470916,
	ARPlaneManager_WindingNumber_m37E0C9DBCC45AAC1CE645B13ED72C0A467886279,
	ARPlaneManager_GetPrefab_mAC046B2B1BF7CF3D5D0812982CD8697D9EEE15E6,
	ARPlaneManager_OnBeforeStart_m08A048DDE25E6E58DAB14D5A98FA967D335281FE,
	ARPlaneManager_OnAfterSetSessionRelativeData_m29742CEC4DFD4C38531850D5BB294D187BBEE86B,
	ARPlaneManager_OnTrackablesChanged_m35BB527E9A876CD709358974F404DF783B220C10,
	ARPlaneManager_get_gameObjectName_mA9589CB27F850045D5C9403B124C18E592E73D61,
	ARPlaneManager_OnEnable_m02B2D3772BF6CC084FA6BFA31B6939CB5199CF97,
	ARPlaneManager_OnDisable_mD54AB766B9285D5C673E54A2A3AEA7F37CC54882,
	ARPlaneManager__ctor_m620BF313F00F9BCAEB75096C558463FF98DD5FEF,
	ARPlaneMeshGenerators_GenerateMesh_mC65957DD09D5DF63DD09B6067B5F9CEEEC60A100,
	ARPlaneMeshGenerators_GenerateUvs_m63AD50B08D0846EB2FE7D189E817B7CCE1D1CA2A,
	ARPlaneMeshGenerators_GenerateIndices_m0884F6FE12A5E246AE457EE06D1614E9D35C40A7,
	ARPlaneMeshGenerators__cctor_mA2AD482799CBD56AC165BBAC69142C0EE97096B4,
	ARPlaneMeshVisualizer_get_mesh_m7E5CE302636B7C6F9578DBB9D0161CF96F5D7AB4,
	ARPlaneMeshVisualizer_set_mesh_mE140F46F7B4124CEABB9F1B8D146844437FE5C41,
	ARPlaneMeshVisualizer_OnBoundaryChanged_mD8351E73EB50EA25B76AE4B34AF2F4D6EC6DAFF3,
	ARPlaneMeshVisualizer_DisableComponents_m9FD32662E3F810874B66E074B06C2A9995567387,
	ARPlaneMeshVisualizer_SetVisible_mCFE4013BD17B4922538374DFD89C5717FE8C1B1F,
	ARPlaneMeshVisualizer_UpdateVisibility_m1F81FBFD5A513C75A9AFB8426782B76178672121,
	ARPlaneMeshVisualizer_Awake_m38B47CDFA66CAC55E120597A2C745424C78492B0,
	ARPlaneMeshVisualizer_OnEnable_mA7F75A39674737AEC66313CF4F3F4A9210034840,
	ARPlaneMeshVisualizer_OnDisable_mE542AD4800BA0FE7AE2F2E368AF2F46C426C5533,
	ARPlaneMeshVisualizer_Update_m98C87020EC822B160D3B62FB67EC169E4931CC8D,
	ARPlaneMeshVisualizer__ctor_mC9399C82657A81411A5D6CECD94F27D1D6B8DDF5,
	ARPlanesChangedEventArgs_get_added_mD0CA0FB88C9F669580ED4BE0C9A3DA6E1333DA07,
	ARPlanesChangedEventArgs_set_added_mADE2BA0CDD14711F3576C51A04BC0A06CC4F0C37,
	ARPlanesChangedEventArgs_get_updated_m1EB6B86313D75F78C287B67BF2EA7A03468BB48E,
	ARPlanesChangedEventArgs_set_updated_m186CC3DF36E1B28FE2B7CFB2F9C56E638C4A4F26,
	ARPlanesChangedEventArgs_get_removed_m771596586A99FE5593EF16233E7F3BA3603E0927,
	ARPlanesChangedEventArgs_set_removed_mE2BE624B870142F492E01AB6328B4D74E575228A,
	ARPlanesChangedEventArgs__ctor_mE0D81BC682849CEF4267F2466246A42A451D7D3A,
	ARPlanesChangedEventArgs_GetHashCode_m80CECC92973BF7AC233A66E338DFE888901E8C7F,
	ARPlanesChangedEventArgs_Equals_m6EE7772274DE47C25AB57E32C1B4C0FAC78B48BF,
	ARPlanesChangedEventArgs_ToString_m0175F0496B86842A452179E194FEAB0FC61A5D3E,
	ARPlanesChangedEventArgs_Equals_mCB86F0483BA241F16D592B63E2DA15F7DA74778C,
	ARPoseDriver_OnEnable_m4CF363539DE8829C4C61AE09CD2AF6709A402818,
	ARPoseDriver_OnDisable_m1607040D5924045B7775017AFFF5C7AF04BCDFBA,
	ARPoseDriver_Update_m5AB64BF6DD47D8219284D3355127F4B981A70586,
	ARPoseDriver_OnBeforeRender_mFE069B31B36607003CC185498D5E39A6F4227838,
	ARPoseDriver_PerformUpdate_m1622A744D6B9B8B8407DC9ECB4B3C1C0EB120C63,
	ARPoseDriver_OnInputDeviceConnected_m9FC7B979E01897C30CE6A29F1A568F59C4A15068,
	ARPoseDriver_CheckConnectedDevice_m79F0F1BA26D8A444E26CC2A66718C048FD64F096,
	ARPoseDriver_GetPoseData_mD73767B4A41C576A343C4F09E3B7C700EF25AE9D,
	ARPoseDriver__ctor_mDC919E8D461DA698AA8CFE0A2DF8E18E65990EBA,
	ARPoseDriver__cctor_m0F6DD1D538D8F6B58E83EF69963A791CCBC08147,
	ARRaycast_get_nativePtr_mBFBD4341016CEE43EF6D2BEC4A00233C71653583,
	ARRaycast_get_distance_mB010898D359A5494CE80DE8373D7690B1B5BB06D,
	ARRaycast_get_plane_mD994AB92D48D9E69C94A70473E23B04725B43663,
	ARRaycast_set_plane_mCA9E660D5C3168183D2DB52EAF433D1DB69EC5AF,
	ARRaycast_add_updated_mA58AF75CDF5D15B24D56E79FBD17BFBBC9B76131,
	ARRaycast_remove_updated_mB5AE0EC23EA14B7331E507D9F4752A056E0AC2EA,
	ARRaycast_OnAfterSetSessionRelativeData_m509CCE0B3BBDCDAA969901835228CC31C8DCB61D,
	ARRaycast_Update_m28E30EED38FB607393D4B1524B8BFBADFCD55658,
	ARRaycast__ctor_m69DF5EC95EB5F88FF46324B6FAD7B97FE77CF80A,
	ARRaycastHit__ctor_mC7110E7B31FCC610B0D6AF62CBF4BDCD33751C09,
	ARRaycastHit_get_distance_mED96EC43E8D177E5B912822DD5566A543BF995AB,
	ARRaycastHit_get_pose_mB4D8BC45F23D9F2C2C8DCAFA88DB1221D76EF02B,
	ARRaycastHit_get_trackableId_mCC2447AD8425B92F2E25E74812D6F50DE588D16A,
	ARRaycastHit_get_sessionRelativePose_m496222B38CDAA9D976ECAFA7CE7ED77CA9E37DBA,
	ARRaycastHit_get_trackable_m0A2E4A67B074ACA0021869D6D8930D34A85F1D79,
	ARRaycastHit_GetHashCode_m659BE91F4F0039B4C81562344E13B9BB4454BE85,
	ARRaycastHit_Equals_m10EB8CE2C048C4C1FF0254C69C376B16553577E6,
	ARRaycastHit_Equals_m75D93284E7D73EDEF82038FA2C0BA7543DBF088D,
	ARRaycastHit_CompareTo_m3B3E66B95BFC5CF7099F910BAAC6823403755514,
	ARRaycastManager_get_raycastPrefab_m1C25B3E344582D3D581C31EB92239E5D9D387AEC,
	ARRaycastManager_set_raycastPrefab_m9F40ED082F528AF756FF04DD028DF40A78512DE4,
	ARRaycastManager_Raycast_mCC2851DAC2542C59528FCE21242231DFAF024650,
	ARRaycastManager_Raycast_m08DA1BF9999699DFC094F91B281B84A44F243E2C,
	ARRaycastManager_AddRaycast_mC7DABC7FC09CF7622669790B22D97C08C787B11D,
	ARRaycastManager_RemoveRaycast_m9171455192ECE274C5D19C15BB86C29AD9321A77,
	ARRaycastManager_GetPrefab_m74C14C1DF9446B86945F4D0419506B95B16F1903,
	ARRaycastManager_RegisterRaycaster_m483341201AF04DE1DAF54B223E45B3CD75ED4AFE,
	ARRaycastManager_UnregisterRaycaster_mBC2C92DC026598D8F2B6835A97A725525326F0DB,
	ARRaycastManager_OnAfterStart_mB047CA21B4220498B1583A84D1DA41995D7E6370,
	ARRaycastManager_ScreenPointToSessionSpaceRay_m7D68FA43BDD2972DA070E7DE05935AB6F7586240,
	ARRaycastManager_RaycastViewportAsRay_m15C127E7D6ED10D456B2C4C8571BB86CE1973C81,
	ARRaycastManager_RaycastViewport_mE0B2B4A53A3CAD20F00501F715DB4D266D3B55BF,
	ARRaycastManager_RaycastRay_m4AE8E0B028B4126C87280B1CEE0A479D71EEF3B9,
	ARRaycastManager_RaycastHitComparer_mC2A9D36BC37C468BBE51196980393FC8916FB64F,
	ARRaycastManager_RaycastFallback_m67FBA4A718FC08126B64E4895447FD4AF5C5B9CF,
	ARRaycastManager_TransformAndDisposeNativeHitResults_m8A369C106CD89AD3BDEFBA51AC220D6E631E5438,
	ARRaycastManager_OnAfterSetSessionRelativeData_m7D3DADC477AC0DBDFFF3EB29E537B19EF8EBD763,
	ARRaycastManager_get_gameObjectName_mDC542442BB59C94E84269051FE9F6D02176470EF,
	ARRaycastManager__ctor_m5318CB3E2A40E89FBB333ED540805A8C155FACEB,
	ARRaycastManager__cctor_m4F7D8AFBCFB8EE07B9E991352AC74B843133A0DA,
	ARRaycastUpdatedEventArgs_get_raycast_m0609B624800A5388048A79B439B4B38F5F2A885F,
	ARRaycastUpdatedEventArgs_set_raycast_m7BE3386FAFCA2682E4B8ADDAAD3B0861CF7B6199,
	ARRaycastUpdatedEventArgs_Equals_m99C427C07FE659083820EF17D021D78BBE7E0B86,
	ARRaycastUpdatedEventArgs_Equals_mAD79322F0D5530018B97925A99B19352DEB5A2F8,
	ARRaycastUpdatedEventArgs_GetHashCode_mE8AEACE0EA29276661BD1CF7E212E3652833044F,
	ARSession_get_attemptUpdate_m8A07979329A9676158CE1122FFFC573F9114A68A,
	ARSession_set_attemptUpdate_mB93F0042D4CA826181527CA915D7022A8607C515,
	ARSession_get_matchFrameRate_m52F8C79BE303168DC19A61E08957AC5E93367DC5,
	ARSession_set_matchFrameRate_mC6625E4B0C2EC397C541722463C6FA8385FF9B26,
	ARSession_get_matchFrameRateEnabled_m54F5C93DC7746648A7535FF28E14F49B3ADF92CD,
	ARSession_get_matchFrameRateRequested_m87736EDEA3ADB050FCBB44A2E55A71FEF9364548,
	ARSession_set_matchFrameRateRequested_mDC93048F4E690E8DED853E885C8DFF3CE8346623,
	ARSession_get_requestedTrackingMode_m4F66072D778CF6008CAF3A8A0AC0B491435008AE,
	ARSession_set_requestedTrackingMode_m88C66125B39BBD446153EF90805B9D68665EBECF,
	ARSession_get_currentTrackingMode_m1AE17402BEB3E163983463AD5691FFA0EF47FFC3,
	ARSession_get_frameRate_m8C253E2EB41BDEE414614EA4C31001F74343BD4F,
	ARSession_add_stateChanged_m0B11F34533799F3DB6B188D44AB356574954D3DF,
	ARSession_remove_stateChanged_m2A25BD04AD425F21552A6057A35701600BEB9451,
	ARSession_get_state_m623161F1E2E5BA2752C821DD409880E6647CA130,
	ARSession_set_state_m74178050EEB5D0A2C787009344AB9C917C0AEEFB,
	ARSession_get_notTrackingReason_m346D5BD4415CD34223970E3FAF019A78E86B606F,
	ARSession_Reset_m3C78CB4465887888EE1A5EEBCCD47E8D462A1116,
	ARSession_SetMatchFrameRateRequested_mABECAD724B851AD673474DB26FDF256292EEBA3F,
	ARSession_WarnIfMultipleARSessions_m193EF1C5A32379298AFA36D6637E924FD0E161A9,
	ARSession_GetSubsystem_m0721DA6041E5FDD2E29D741C678C16CD84F26B79,
	ARSession_CheckAvailability_mCC906561CCCE269C11B69D2216C39120563F00FD,
	ARSession_Install_mF45AE40F0F8A22E4EEF960BEBD434A7F7591CA67,
	ARSession_OnEnable_mA368B8911A5ABF1EF0C44B636723D6981ABF4714,
	ARSession_Initialize_m0FE9C0B5D3007D3D127FFF52674255F4CCAFF808,
	ARSession_StartSubsystem_m4A470169DE740C623C2C770AEA105859493C0DA8,
	ARSession_Awake_m9024E982F0CD73C9C679A3DA6FAACE0FFCD39A34,
	ARSession_Update_mF2C8C33579C2D606200802FE4F7668268DAD1828,
	ARSession_OnApplicationPause_mEA4B2C6D69B1D3CB247CC36C219298F38FC42EBE,
	ARSession_OnDisable_m2C35032CBD09545363AB5877C9CFDD89226931AD,
	ARSession_OnDestroy_m494EC9644E21D53BCFB5BF1FB000477AA60116C9,
	ARSession_UpdateNotTrackingReason_m48F2669C645017758983AF46A48444849373849A,
	ARSession__ctor_mA9A989B079A758DBA381FEECE8F3377F45F96E9C,
	U3CCheckAvailabilityU3Ed__33__ctor_mCA3C75BF2A10DAEF6340EEE3AA96787CE9D43AD6,
	U3CCheckAvailabilityU3Ed__33_System_IDisposable_Dispose_mFC4943930186516D05B109AF2FA2F9F3941BB406,
	U3CCheckAvailabilityU3Ed__33_MoveNext_mE7DDB20C3E68ED1E3C5C9AD093583E8634671D23,
	U3CCheckAvailabilityU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m04253DF005179DA04D14FAAF9B88492B0B249A36,
	U3CCheckAvailabilityU3Ed__33_System_Collections_IEnumerator_Reset_mB34A16CDDAD5F80D161C66EBA84D985634D02F84,
	U3CCheckAvailabilityU3Ed__33_System_Collections_IEnumerator_get_Current_m5F68AE4C034BA873619CA65A51B15247D937A534,
	U3CInstallU3Ed__34__ctor_mB10A644A399CAFF7AB219EB64D3FDAD2E5FB8603,
	U3CInstallU3Ed__34_System_IDisposable_Dispose_mB443F32C58F5487A5014937CC18849C8D060F98D,
	U3CInstallU3Ed__34_MoveNext_m1C1F4EF15039976EDE584C6F71E1A7A808549767,
	U3CInstallU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6C0A95CD160AECECA459DBA361BFE015BDF06783,
	U3CInstallU3Ed__34_System_Collections_IEnumerator_Reset_m48F87F38059EF06A00F6EB1CE1F4E3BA76CE953A,
	U3CInstallU3Ed__34_System_Collections_IEnumerator_get_Current_m4ACDB1ECA51128D6DD766DD902377440DF4E8A29,
	U3CInitializeU3Ed__36__ctor_m50BF8F1AEBF98425C1898C6BC8FE52FED8274DE8,
	U3CInitializeU3Ed__36_System_IDisposable_Dispose_m223C7A21303665BF07B1F54D823BB2A620660EF8,
	U3CInitializeU3Ed__36_MoveNext_mA09F10F396058FD8ECEAC71BFC432715C32B9FB3,
	U3CInitializeU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFC5A2125CEA50D8FD28766B4C38C4257CE59CDF7,
	U3CInitializeU3Ed__36_System_Collections_IEnumerator_Reset_m01FA7C362A9D4A6A2BE1C3694B777867F492171F,
	U3CInitializeU3Ed__36_System_Collections_IEnumerator_get_Current_m054DDB2DE2181B115B6CE16D08BB8953178F4659,
	ARSessionOrigin_get_camera_m06DE26C86E4FD1149361159F5609734F989C23D3,
	ARSessionOrigin_set_camera_m5280D9B057050AEB45F58F542F6CC81930D8D908,
	ARSessionOrigin_get_trackablesParent_mC232717A3F6993690E5A68E1CD17B25F7843C634,
	ARSessionOrigin_set_trackablesParent_mEDC8CC36C64F3478FE5CB37D306D41FC2D0AB7B4,
	ARSessionOrigin_add_trackablesParentTransformChanged_m9936C7B9DA33251D5DDF255C31CB9352F8F1A527,
	ARSessionOrigin_remove_trackablesParentTransformChanged_mAA1A639505243A4AC78177CC703EDE41F76AAE90,
	ARSessionOrigin_get_contentOffsetTransform_mA49736F93177F052B00CC6CEE06833F8D278CF59,
	ARSessionOrigin_MakeContentAppearAt_m741B35243FFD56004B8FEC762A7FE9759B4D6603,
	ARSessionOrigin_MakeContentAppearAt_m0BAF5FBD841FD67517431E8D81773968CE457608,
	ARSessionOrigin_MakeContentAppearAt_mE5B578D7C44877F95C526480F873124E60549CB0,
	ARSessionOrigin_Awake_m9EDF100DB525494EB21B07082DC5CACA63667DF6,
	ARSessionOrigin_GetCameraOriginPose_m796F542B04BE57748CFB98A759A6204FD3AAA7CC,
	ARSessionOrigin_OnEnable_m5B3860FC541DC44A3F186C4E2F1CABB74F074D3A,
	ARSessionOrigin_OnDisable_m09E783B9894438CE2B2B28603EDA70F860C1EDB4,
	ARSessionOrigin_OnBeforeRender_mB0B28F0BD8F27D7BD3499C8D55E19963C0D35547,
	ARSessionOrigin__ctor_m0E3C505EA0D5537EC2DCFB047C67BF39B6E334CF,
	ARSessionStateChangedEventArgs_get_state_mDFCCADECEFE9356E4246B9BF912157F3EDD88796,
	ARSessionStateChangedEventArgs_set_state_mA9AB6CBFCAD476EC94E3642F5638BCC11B4A4B95,
	ARSessionStateChangedEventArgs__ctor_m6001FA2930CF6A8551451A20129E7827738E0AA9,
	ARSessionStateChangedEventArgs_GetHashCode_m00DDAFEED0CA796E059FBB5845B62DA16DA90BFF,
	ARSessionStateChangedEventArgs_Equals_m55B6A7D08E747098F36B59A6B3CAA8DB392E3454,
	ARSessionStateChangedEventArgs_ToString_m415BD73E732AE375F1BDF0990604D5FA0DD87A44,
	ARSessionStateChangedEventArgs_Equals_mC4C56CAF33303BA032420A206F358AE4AEF4181A,
	ARTrackable__ctor_m7430516DC3D8985F9C8E2C82DAA05C2DAF4B90B1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ARTrackablesParentTransformChangedEventArgs_get_sessionOrigin_m4A650B7FFE29FC40DA7781843DF2AE744108FBBA,
	ARTrackablesParentTransformChangedEventArgs_get_trackablesParent_m996D495C51CD1A26A40FB44EF841E824EF87711E,
	ARTrackablesParentTransformChangedEventArgs__ctor_m45E72BC55106F4DFE8E9DBC77C2D33213203B2B6,
	ARTrackablesParentTransformChangedEventArgs_Equals_mEFECB01DA6949C437802A4E65FFC803FD0BBA603,
	ARTrackablesParentTransformChangedEventArgs_Equals_m31CF1D718EFE540CACA9C601CA7B9E2666AA2361,
	ARTrackablesParentTransformChangedEventArgs_GetHashCode_mCC1795A3F585FA0C88849C76D602A0B7F6A0F620,
	HashCode_Combine_mB733DE32A13A557A4EBF6B13881920FACBCD7C8D,
	HashCode_ReferenceHash_m8E89AD422634F94074CADFA2642183BF353D150F,
	HashCode_Combine_mE6B8F1B5170F1B7A7AC2B7EF8BF9739E96CAFB3D,
	HashCode_Combine_mD76CDEE1565B93AFF8FDBE56E7C33C971BCB012F,
	NULL,
	PlaneDetectionModeMaskAttribute__ctor_m7D29BF65AB4C7AFAB629A57F74DE67ED29FFEB1C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TrackingModeExtensions_ToFeature_mC09F4526E4C8B7459305E10902A0698F8AD3F1BD,
	TrackingModeExtensions_ToTrackingMode_m1AD1AC6E4580B57E09536930C4A107E61CB8912E,
	TransformExtensions_InverseTransformRay_m6C3083362DA474AF08321E73612E373771E5CA3A,
	TransformExtensions_TransformPose_mD57F5882BD08CAFB48396780B469DA938E816A4B,
	TransformExtensions_InverseTransformPose_m6143382A59744E50CB53B9718F8F21B18C692E4C,
};
extern void ARAnchorsChangedEventArgs_get_added_m4D07DC13F8D06201C220964F8CCE1CD99F4342E8_AdjustorThunk (void);
extern void ARAnchorsChangedEventArgs_set_added_m250070DF629DAAF526CE631D008D1320DE449A40_AdjustorThunk (void);
extern void ARAnchorsChangedEventArgs_get_updated_m87B120B079D1B9FF8C9F1E305672BD6F741AC7DF_AdjustorThunk (void);
extern void ARAnchorsChangedEventArgs_set_updated_m3A0ED32E6B56174861DD6431E0C50AE016FC9826_AdjustorThunk (void);
extern void ARAnchorsChangedEventArgs_get_removed_m3DA7093629E8D772C2732299D5F612B1357F6D08_AdjustorThunk (void);
extern void ARAnchorsChangedEventArgs_set_removed_m96CA8069483206C90B983B2BCF00F8AB60FC6985_AdjustorThunk (void);
extern void ARAnchorsChangedEventArgs__ctor_m51231F5A1B806EDD5E2A37C514A81BA87AC29DB5_AdjustorThunk (void);
extern void ARAnchorsChangedEventArgs_GetHashCode_m9F8FD840B1A6562E4B3CF4A3B5A7175A49D136D0_AdjustorThunk (void);
extern void ARAnchorsChangedEventArgs_Equals_mBB1A87EB69C9AED17A688CAF715718D87FD027D6_AdjustorThunk (void);
extern void ARAnchorsChangedEventArgs_ToString_m2C6B93BD6C868C7243C104B916EC06E871AABC5A_AdjustorThunk (void);
extern void ARAnchorsChangedEventArgs_Equals_m007C8B067E9DF6AEE162E8BF8889419E0E183961_AdjustorThunk (void);
extern void ARPlaneBoundaryChangedEventArgs_get_plane_mC91E64413E8593C0137A7C47485A1238180932A5_AdjustorThunk (void);
extern void ARPlaneBoundaryChangedEventArgs_set_plane_mEDFCCE4100F4480E53F3EE12F4F89DB7054AE6D4_AdjustorThunk (void);
extern void ARPlaneBoundaryChangedEventArgs__ctor_mCD138E5FFD74678A29B6A494550C428AE7344678_AdjustorThunk (void);
extern void ARPlaneBoundaryChangedEventArgs_GetHashCode_m01011EEB2FAD823382934CDF5710CE259A212EB4_AdjustorThunk (void);
extern void ARPlaneBoundaryChangedEventArgs_Equals_m9BBA8DDF067E8DC0BB0956C0BD4FF95B78C9DA1B_AdjustorThunk (void);
extern void ARPlaneBoundaryChangedEventArgs_ToString_mB9D8B3B3DD1965A75E8CBE0F1133DA0C4310D28F_AdjustorThunk (void);
extern void ARPlaneBoundaryChangedEventArgs_Equals_m8FCE506D26E362A259FD1E260648E9FA5C07B91D_AdjustorThunk (void);
extern void ARPlanesChangedEventArgs_get_added_mD0CA0FB88C9F669580ED4BE0C9A3DA6E1333DA07_AdjustorThunk (void);
extern void ARPlanesChangedEventArgs_set_added_mADE2BA0CDD14711F3576C51A04BC0A06CC4F0C37_AdjustorThunk (void);
extern void ARPlanesChangedEventArgs_get_updated_m1EB6B86313D75F78C287B67BF2EA7A03468BB48E_AdjustorThunk (void);
extern void ARPlanesChangedEventArgs_set_updated_m186CC3DF36E1B28FE2B7CFB2F9C56E638C4A4F26_AdjustorThunk (void);
extern void ARPlanesChangedEventArgs_get_removed_m771596586A99FE5593EF16233E7F3BA3603E0927_AdjustorThunk (void);
extern void ARPlanesChangedEventArgs_set_removed_mE2BE624B870142F492E01AB6328B4D74E575228A_AdjustorThunk (void);
extern void ARPlanesChangedEventArgs__ctor_mE0D81BC682849CEF4267F2466246A42A451D7D3A_AdjustorThunk (void);
extern void ARPlanesChangedEventArgs_GetHashCode_m80CECC92973BF7AC233A66E338DFE888901E8C7F_AdjustorThunk (void);
extern void ARPlanesChangedEventArgs_Equals_m6EE7772274DE47C25AB57E32C1B4C0FAC78B48BF_AdjustorThunk (void);
extern void ARPlanesChangedEventArgs_ToString_m0175F0496B86842A452179E194FEAB0FC61A5D3E_AdjustorThunk (void);
extern void ARPlanesChangedEventArgs_Equals_mCB86F0483BA241F16D592B63E2DA15F7DA74778C_AdjustorThunk (void);
extern void ARRaycastHit__ctor_mC7110E7B31FCC610B0D6AF62CBF4BDCD33751C09_AdjustorThunk (void);
extern void ARRaycastHit_get_distance_mED96EC43E8D177E5B912822DD5566A543BF995AB_AdjustorThunk (void);
extern void ARRaycastHit_get_pose_mB4D8BC45F23D9F2C2C8DCAFA88DB1221D76EF02B_AdjustorThunk (void);
extern void ARRaycastHit_get_trackableId_mCC2447AD8425B92F2E25E74812D6F50DE588D16A_AdjustorThunk (void);
extern void ARRaycastHit_get_sessionRelativePose_m496222B38CDAA9D976ECAFA7CE7ED77CA9E37DBA_AdjustorThunk (void);
extern void ARRaycastHit_get_trackable_m0A2E4A67B074ACA0021869D6D8930D34A85F1D79_AdjustorThunk (void);
extern void ARRaycastHit_GetHashCode_m659BE91F4F0039B4C81562344E13B9BB4454BE85_AdjustorThunk (void);
extern void ARRaycastHit_Equals_m10EB8CE2C048C4C1FF0254C69C376B16553577E6_AdjustorThunk (void);
extern void ARRaycastHit_Equals_m75D93284E7D73EDEF82038FA2C0BA7543DBF088D_AdjustorThunk (void);
extern void ARRaycastHit_CompareTo_m3B3E66B95BFC5CF7099F910BAAC6823403755514_AdjustorThunk (void);
extern void ARRaycastUpdatedEventArgs_get_raycast_m0609B624800A5388048A79B439B4B38F5F2A885F_AdjustorThunk (void);
extern void ARRaycastUpdatedEventArgs_set_raycast_m7BE3386FAFCA2682E4B8ADDAAD3B0861CF7B6199_AdjustorThunk (void);
extern void ARRaycastUpdatedEventArgs_Equals_m99C427C07FE659083820EF17D021D78BBE7E0B86_AdjustorThunk (void);
extern void ARRaycastUpdatedEventArgs_Equals_mAD79322F0D5530018B97925A99B19352DEB5A2F8_AdjustorThunk (void);
extern void ARRaycastUpdatedEventArgs_GetHashCode_mE8AEACE0EA29276661BD1CF7E212E3652833044F_AdjustorThunk (void);
extern void ARSessionStateChangedEventArgs_get_state_mDFCCADECEFE9356E4246B9BF912157F3EDD88796_AdjustorThunk (void);
extern void ARSessionStateChangedEventArgs_set_state_mA9AB6CBFCAD476EC94E3642F5638BCC11B4A4B95_AdjustorThunk (void);
extern void ARSessionStateChangedEventArgs__ctor_m6001FA2930CF6A8551451A20129E7827738E0AA9_AdjustorThunk (void);
extern void ARSessionStateChangedEventArgs_GetHashCode_m00DDAFEED0CA796E059FBB5845B62DA16DA90BFF_AdjustorThunk (void);
extern void ARSessionStateChangedEventArgs_Equals_m55B6A7D08E747098F36B59A6B3CAA8DB392E3454_AdjustorThunk (void);
extern void ARSessionStateChangedEventArgs_ToString_m415BD73E732AE375F1BDF0990604D5FA0DD87A44_AdjustorThunk (void);
extern void ARSessionStateChangedEventArgs_Equals_mC4C56CAF33303BA032420A206F358AE4AEF4181A_AdjustorThunk (void);
extern void ARTrackablesParentTransformChangedEventArgs_get_sessionOrigin_m4A650B7FFE29FC40DA7781843DF2AE744108FBBA_AdjustorThunk (void);
extern void ARTrackablesParentTransformChangedEventArgs_get_trackablesParent_m996D495C51CD1A26A40FB44EF841E824EF87711E_AdjustorThunk (void);
extern void ARTrackablesParentTransformChangedEventArgs__ctor_m45E72BC55106F4DFE8E9DBC77C2D33213203B2B6_AdjustorThunk (void);
extern void ARTrackablesParentTransformChangedEventArgs_Equals_mEFECB01DA6949C437802A4E65FFC803FD0BBA603_AdjustorThunk (void);
extern void ARTrackablesParentTransformChangedEventArgs_Equals_m31CF1D718EFE540CACA9C601CA7B9E2666AA2361_AdjustorThunk (void);
extern void ARTrackablesParentTransformChangedEventArgs_GetHashCode_mCC1795A3F585FA0C88849C76D602A0B7F6A0F620_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[57] = 
{
	{ 0x06000015, ARAnchorsChangedEventArgs_get_added_m4D07DC13F8D06201C220964F8CCE1CD99F4342E8_AdjustorThunk },
	{ 0x06000016, ARAnchorsChangedEventArgs_set_added_m250070DF629DAAF526CE631D008D1320DE449A40_AdjustorThunk },
	{ 0x06000017, ARAnchorsChangedEventArgs_get_updated_m87B120B079D1B9FF8C9F1E305672BD6F741AC7DF_AdjustorThunk },
	{ 0x06000018, ARAnchorsChangedEventArgs_set_updated_m3A0ED32E6B56174861DD6431E0C50AE016FC9826_AdjustorThunk },
	{ 0x06000019, ARAnchorsChangedEventArgs_get_removed_m3DA7093629E8D772C2732299D5F612B1357F6D08_AdjustorThunk },
	{ 0x0600001A, ARAnchorsChangedEventArgs_set_removed_m96CA8069483206C90B983B2BCF00F8AB60FC6985_AdjustorThunk },
	{ 0x0600001B, ARAnchorsChangedEventArgs__ctor_m51231F5A1B806EDD5E2A37C514A81BA87AC29DB5_AdjustorThunk },
	{ 0x0600001C, ARAnchorsChangedEventArgs_GetHashCode_m9F8FD840B1A6562E4B3CF4A3B5A7175A49D136D0_AdjustorThunk },
	{ 0x0600001D, ARAnchorsChangedEventArgs_Equals_mBB1A87EB69C9AED17A688CAF715718D87FD027D6_AdjustorThunk },
	{ 0x0600001E, ARAnchorsChangedEventArgs_ToString_m2C6B93BD6C868C7243C104B916EC06E871AABC5A_AdjustorThunk },
	{ 0x0600001F, ARAnchorsChangedEventArgs_Equals_m007C8B067E9DF6AEE162E8BF8889419E0E183961_AdjustorThunk },
	{ 0x06000037, ARPlaneBoundaryChangedEventArgs_get_plane_mC91E64413E8593C0137A7C47485A1238180932A5_AdjustorThunk },
	{ 0x06000038, ARPlaneBoundaryChangedEventArgs_set_plane_mEDFCCE4100F4480E53F3EE12F4F89DB7054AE6D4_AdjustorThunk },
	{ 0x06000039, ARPlaneBoundaryChangedEventArgs__ctor_mCD138E5FFD74678A29B6A494550C428AE7344678_AdjustorThunk },
	{ 0x0600003A, ARPlaneBoundaryChangedEventArgs_GetHashCode_m01011EEB2FAD823382934CDF5710CE259A212EB4_AdjustorThunk },
	{ 0x0600003B, ARPlaneBoundaryChangedEventArgs_Equals_m9BBA8DDF067E8DC0BB0956C0BD4FF95B78C9DA1B_AdjustorThunk },
	{ 0x0600003C, ARPlaneBoundaryChangedEventArgs_ToString_mB9D8B3B3DD1965A75E8CBE0F1133DA0C4310D28F_AdjustorThunk },
	{ 0x0600003D, ARPlaneBoundaryChangedEventArgs_Equals_m8FCE506D26E362A259FD1E260648E9FA5C07B91D_AdjustorThunk },
	{ 0x06000062, ARPlanesChangedEventArgs_get_added_mD0CA0FB88C9F669580ED4BE0C9A3DA6E1333DA07_AdjustorThunk },
	{ 0x06000063, ARPlanesChangedEventArgs_set_added_mADE2BA0CDD14711F3576C51A04BC0A06CC4F0C37_AdjustorThunk },
	{ 0x06000064, ARPlanesChangedEventArgs_get_updated_m1EB6B86313D75F78C287B67BF2EA7A03468BB48E_AdjustorThunk },
	{ 0x06000065, ARPlanesChangedEventArgs_set_updated_m186CC3DF36E1B28FE2B7CFB2F9C56E638C4A4F26_AdjustorThunk },
	{ 0x06000066, ARPlanesChangedEventArgs_get_removed_m771596586A99FE5593EF16233E7F3BA3603E0927_AdjustorThunk },
	{ 0x06000067, ARPlanesChangedEventArgs_set_removed_mE2BE624B870142F492E01AB6328B4D74E575228A_AdjustorThunk },
	{ 0x06000068, ARPlanesChangedEventArgs__ctor_mE0D81BC682849CEF4267F2466246A42A451D7D3A_AdjustorThunk },
	{ 0x06000069, ARPlanesChangedEventArgs_GetHashCode_m80CECC92973BF7AC233A66E338DFE888901E8C7F_AdjustorThunk },
	{ 0x0600006A, ARPlanesChangedEventArgs_Equals_m6EE7772274DE47C25AB57E32C1B4C0FAC78B48BF_AdjustorThunk },
	{ 0x0600006B, ARPlanesChangedEventArgs_ToString_m0175F0496B86842A452179E194FEAB0FC61A5D3E_AdjustorThunk },
	{ 0x0600006C, ARPlanesChangedEventArgs_Equals_mCB86F0483BA241F16D592B63E2DA15F7DA74778C_AdjustorThunk },
	{ 0x06000080, ARRaycastHit__ctor_mC7110E7B31FCC610B0D6AF62CBF4BDCD33751C09_AdjustorThunk },
	{ 0x06000081, ARRaycastHit_get_distance_mED96EC43E8D177E5B912822DD5566A543BF995AB_AdjustorThunk },
	{ 0x06000082, ARRaycastHit_get_pose_mB4D8BC45F23D9F2C2C8DCAFA88DB1221D76EF02B_AdjustorThunk },
	{ 0x06000083, ARRaycastHit_get_trackableId_mCC2447AD8425B92F2E25E74812D6F50DE588D16A_AdjustorThunk },
	{ 0x06000084, ARRaycastHit_get_sessionRelativePose_m496222B38CDAA9D976ECAFA7CE7ED77CA9E37DBA_AdjustorThunk },
	{ 0x06000085, ARRaycastHit_get_trackable_m0A2E4A67B074ACA0021869D6D8930D34A85F1D79_AdjustorThunk },
	{ 0x06000086, ARRaycastHit_GetHashCode_m659BE91F4F0039B4C81562344E13B9BB4454BE85_AdjustorThunk },
	{ 0x06000087, ARRaycastHit_Equals_m10EB8CE2C048C4C1FF0254C69C376B16553577E6_AdjustorThunk },
	{ 0x06000088, ARRaycastHit_Equals_m75D93284E7D73EDEF82038FA2C0BA7543DBF088D_AdjustorThunk },
	{ 0x06000089, ARRaycastHit_CompareTo_m3B3E66B95BFC5CF7099F910BAAC6823403755514_AdjustorThunk },
	{ 0x0600009F, ARRaycastUpdatedEventArgs_get_raycast_m0609B624800A5388048A79B439B4B38F5F2A885F_AdjustorThunk },
	{ 0x060000A0, ARRaycastUpdatedEventArgs_set_raycast_m7BE3386FAFCA2682E4B8ADDAAD3B0861CF7B6199_AdjustorThunk },
	{ 0x060000A1, ARRaycastUpdatedEventArgs_Equals_m99C427C07FE659083820EF17D021D78BBE7E0B86_AdjustorThunk },
	{ 0x060000A2, ARRaycastUpdatedEventArgs_Equals_mAD79322F0D5530018B97925A99B19352DEB5A2F8_AdjustorThunk },
	{ 0x060000A3, ARRaycastUpdatedEventArgs_GetHashCode_mE8AEACE0EA29276661BD1CF7E212E3652833044F_AdjustorThunk },
	{ 0x060000E6, ARSessionStateChangedEventArgs_get_state_mDFCCADECEFE9356E4246B9BF912157F3EDD88796_AdjustorThunk },
	{ 0x060000E7, ARSessionStateChangedEventArgs_set_state_mA9AB6CBFCAD476EC94E3642F5638BCC11B4A4B95_AdjustorThunk },
	{ 0x060000E8, ARSessionStateChangedEventArgs__ctor_m6001FA2930CF6A8551451A20129E7827738E0AA9_AdjustorThunk },
	{ 0x060000E9, ARSessionStateChangedEventArgs_GetHashCode_m00DDAFEED0CA796E059FBB5845B62DA16DA90BFF_AdjustorThunk },
	{ 0x060000EA, ARSessionStateChangedEventArgs_Equals_m55B6A7D08E747098F36B59A6B3CAA8DB392E3454_AdjustorThunk },
	{ 0x060000EB, ARSessionStateChangedEventArgs_ToString_m415BD73E732AE375F1BDF0990604D5FA0DD87A44_AdjustorThunk },
	{ 0x060000EC, ARSessionStateChangedEventArgs_Equals_mC4C56CAF33303BA032420A206F358AE4AEF4181A_AdjustorThunk },
	{ 0x06000119, ARTrackablesParentTransformChangedEventArgs_get_sessionOrigin_m4A650B7FFE29FC40DA7781843DF2AE744108FBBA_AdjustorThunk },
	{ 0x0600011A, ARTrackablesParentTransformChangedEventArgs_get_trackablesParent_m996D495C51CD1A26A40FB44EF841E824EF87711E_AdjustorThunk },
	{ 0x0600011B, ARTrackablesParentTransformChangedEventArgs__ctor_m45E72BC55106F4DFE8E9DBC77C2D33213203B2B6_AdjustorThunk },
	{ 0x0600011C, ARTrackablesParentTransformChangedEventArgs_Equals_mEFECB01DA6949C437802A4E65FFC803FD0BBA603_AdjustorThunk },
	{ 0x0600011D, ARTrackablesParentTransformChangedEventArgs_Equals_m31CF1D718EFE540CACA9C601CA7B9E2666AA2361_AdjustorThunk },
	{ 0x0600011E, ARTrackablesParentTransformChangedEventArgs_GetHashCode_mCC1795A3F585FA0C88849C76D602A0B7F6A0F620_AdjustorThunk },
};
static const int32_t s_InvokerIndices[318] = 
{
	4678,
	4647,
	4797,
	4797,
	4797,
	4797,
	4712,
	3886,
	3886,
	3886,
	2994,
	3352,
	1543,
	3352,
	3352,
	3001,
	4712,
	4712,
	1223,
	4797,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	1223,
	4676,
	3352,
	4712,
	3225,
	4755,
	3926,
	3886,
	3886,
	4791,
	4712,
	3886,
	4676,
	4676,
	4788,
	4791,
	4788,
	4788,
	4714,
	4678,
	4530,
	3886,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4712,
	3886,
	3886,
	4676,
	3352,
	4712,
	3226,
	4712,
	3886,
	4676,
	3850,
	4676,
	3850,
	4676,
	3886,
	3886,
	3001,
	870,
	6363,
	6147,
	4712,
	4797,
	2205,
	1223,
	4712,
	4797,
	4797,
	4797,
	5582,
	6023,
	5851,
	7059,
	4712,
	3886,
	3762,
	4797,
	3920,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	1223,
	4676,
	3352,
	4712,
	3227,
	4797,
	4797,
	4797,
	4797,
	4797,
	3841,
	1846,
	7065,
	4797,
	7059,
	4678,
	4755,
	4712,
	3886,
	3886,
	3886,
	4797,
	4797,
	4797,
	866,
	4755,
	4723,
	4780,
	4723,
	4712,
	4676,
	3352,
	3228,
	2669,
	4712,
	3886,
	1111,
	1099,
	1553,
	3886,
	4712,
	3886,
	3886,
	4797,
	3036,
	871,
	871,
	870,
	6125,
	870,
	1037,
	2237,
	4712,
	4797,
	7059,
	4712,
	3886,
	3229,
	3352,
	4676,
	4748,
	3920,
	4748,
	3920,
	4748,
	4748,
	3920,
	4676,
	3850,
	4676,
	4544,
	6921,
	6921,
	7014,
	6916,
	7014,
	4797,
	3920,
	4797,
	7026,
	7026,
	7026,
	4797,
	4712,
	4797,
	4797,
	4797,
	3920,
	4797,
	4797,
	7059,
	4797,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4712,
	3886,
	4712,
	3886,
	3886,
	3886,
	4712,
	1247,
	2233,
	2225,
	4797,
	4723,
	4797,
	4797,
	4797,
	4797,
	4676,
	3850,
	3850,
	4676,
	3352,
	4712,
	3230,
	4797,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	4712,
	4712,
	2224,
	3231,
	3352,
	4676,
	6131,
	6696,
	5712,
	5394,
	870,
	4797,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	6712,
	6693,
	6238,
	6232,
	6232,
};
static const Il2CppTokenRangePair s_rgctxIndices[5] = 
{
	{ 0x02000019, { 0, 3 } },
	{ 0x0200001A, { 3, 68 } },
	{ 0x0200001F, { 71, 15 } },
	{ 0x02000020, { 86, 5 } },
	{ 0x02000021, { 91, 4 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[95] = 
{
	{ (Il2CppRGCTXDataType)3, 570 },
	{ (Il2CppRGCTXDataType)2, 587 },
	{ (Il2CppRGCTXDataType)3, 571 },
	{ (Il2CppRGCTXDataType)2, 1259 },
	{ (Il2CppRGCTXDataType)2, 9383 },
	{ (Il2CppRGCTXDataType)2, 9669 },
	{ (Il2CppRGCTXDataType)3, 42933 },
	{ (Il2CppRGCTXDataType)3, 448 },
	{ (Il2CppRGCTXDataType)3, 42934 },
	{ (Il2CppRGCTXDataType)3, 13044 },
	{ (Il2CppRGCTXDataType)2, 1173 },
	{ (Il2CppRGCTXDataType)3, 13043 },
	{ (Il2CppRGCTXDataType)3, 450 },
	{ (Il2CppRGCTXDataType)3, 41517 },
	{ (Il2CppRGCTXDataType)3, 449 },
	{ (Il2CppRGCTXDataType)3, 447 },
	{ (Il2CppRGCTXDataType)3, 444 },
	{ (Il2CppRGCTXDataType)3, 41516 },
	{ (Il2CppRGCTXDataType)3, 576 },
	{ (Il2CppRGCTXDataType)3, 8940 },
	{ (Il2CppRGCTXDataType)3, 41518 },
	{ (Il2CppRGCTXDataType)2, 586 },
	{ (Il2CppRGCTXDataType)3, 577 },
	{ (Il2CppRGCTXDataType)3, 575 },
	{ (Il2CppRGCTXDataType)3, 42943 },
	{ (Il2CppRGCTXDataType)3, 42905 },
	{ (Il2CppRGCTXDataType)3, 33035 },
	{ (Il2CppRGCTXDataType)3, 432 },
	{ (Il2CppRGCTXDataType)3, 33034 },
	{ (Il2CppRGCTXDataType)3, 13040 },
	{ (Il2CppRGCTXDataType)3, 436 },
	{ (Il2CppRGCTXDataType)3, 28345 },
	{ (Il2CppRGCTXDataType)3, 13039 },
	{ (Il2CppRGCTXDataType)2, 2912 },
	{ (Il2CppRGCTXDataType)3, 42907 },
	{ (Il2CppRGCTXDataType)3, 42906 },
	{ (Il2CppRGCTXDataType)3, 8942 },
	{ (Il2CppRGCTXDataType)3, 8941 },
	{ (Il2CppRGCTXDataType)2, 9664 },
	{ (Il2CppRGCTXDataType)3, 28349 },
	{ (Il2CppRGCTXDataType)3, 443 },
	{ (Il2CppRGCTXDataType)3, 28347 },
	{ (Il2CppRGCTXDataType)3, 13042 },
	{ (Il2CppRGCTXDataType)3, 438 },
	{ (Il2CppRGCTXDataType)3, 13041 },
	{ (Il2CppRGCTXDataType)2, 2913 },
	{ (Il2CppRGCTXDataType)3, 8939 },
	{ (Il2CppRGCTXDataType)3, 28346 },
	{ (Il2CppRGCTXDataType)3, 28348 },
	{ (Il2CppRGCTXDataType)3, 28350 },
	{ (Il2CppRGCTXDataType)3, 446 },
	{ (Il2CppRGCTXDataType)3, 439 },
	{ (Il2CppRGCTXDataType)3, 433 },
	{ (Il2CppRGCTXDataType)3, 440 },
	{ (Il2CppRGCTXDataType)3, 434 },
	{ (Il2CppRGCTXDataType)2, 1161 },
	{ (Il2CppRGCTXDataType)3, 435 },
	{ (Il2CppRGCTXDataType)3, 50951 },
	{ (Il2CppRGCTXDataType)3, 50896 },
	{ (Il2CppRGCTXDataType)3, 445 },
	{ (Il2CppRGCTXDataType)3, 573 },
	{ (Il2CppRGCTXDataType)3, 442 },
	{ (Il2CppRGCTXDataType)3, 441 },
	{ (Il2CppRGCTXDataType)3, 572 },
	{ (Il2CppRGCTXDataType)3, 437 },
	{ (Il2CppRGCTXDataType)3, 574 },
	{ (Il2CppRGCTXDataType)2, 2614 },
	{ (Il2CppRGCTXDataType)3, 8938 },
	{ (Il2CppRGCTXDataType)3, 41512 },
	{ (Il2CppRGCTXDataType)2, 7556 },
	{ (Il2CppRGCTXDataType)3, 28344 },
	{ (Il2CppRGCTXDataType)3, 41523 },
	{ (Il2CppRGCTXDataType)2, 796 },
	{ (Il2CppRGCTXDataType)3, 41572 },
	{ (Il2CppRGCTXDataType)3, 52433 },
	{ (Il2CppRGCTXDataType)1, 796 },
	{ (Il2CppRGCTXDataType)3, 41520 },
	{ (Il2CppRGCTXDataType)3, 41524 },
	{ (Il2CppRGCTXDataType)3, 41519 },
	{ (Il2CppRGCTXDataType)3, 41522 },
	{ (Il2CppRGCTXDataType)3, 41521 },
	{ (Il2CppRGCTXDataType)2, 7554 },
	{ (Il2CppRGCTXDataType)3, 28343 },
	{ (Il2CppRGCTXDataType)2, 9384 },
	{ (Il2CppRGCTXDataType)2, 7540 },
	{ (Il2CppRGCTXDataType)3, 28331 },
	{ (Il2CppRGCTXDataType)2, 2908 },
	{ (Il2CppRGCTXDataType)3, 13020 },
	{ (Il2CppRGCTXDataType)3, 8936 },
	{ (Il2CppRGCTXDataType)2, 9668 },
	{ (Il2CppRGCTXDataType)3, 42932 },
	{ (Il2CppRGCTXDataType)3, 8937 },
	{ (Il2CppRGCTXDataType)3, 14694 },
	{ (Il2CppRGCTXDataType)3, 14695 },
	{ (Il2CppRGCTXDataType)3, 27991 },
};
extern const CustomAttributesCacheGenerator g_Unity_XR_ARFoundation_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Unity_XR_ARFoundation_CodeGenModule;
const Il2CppCodeGenModule g_Unity_XR_ARFoundation_CodeGenModule = 
{
	"Unity.XR.ARFoundation.dll",
	318,
	s_methodPointers,
	57,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	5,
	s_rgctxIndices,
	95,
	s_rgctxValues,
	NULL,
	g_Unity_XR_ARFoundation_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
