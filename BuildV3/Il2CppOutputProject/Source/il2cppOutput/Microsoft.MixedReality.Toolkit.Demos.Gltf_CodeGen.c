﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.Gltf.TestGlbLoading::Start()
extern void TestGlbLoading_Start_m38C9512BEF3BF429CBA458AACBBAFFB40670BB35 (void);
// 0x00000002 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.Gltf.TestGlbLoading::.ctor()
extern void TestGlbLoading__ctor_m4DA87CBC3939A0FD6E83095AC9FE52A818202417 (void);
// 0x00000003 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.Gltf.TestGlbLoading/<Start>d__1::.ctor()
extern void U3CStartU3Ed__1__ctor_mD8DA86170FA33604B82D6D915B64ABBD2A37EA2B (void);
// 0x00000004 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.Gltf.TestGlbLoading/<Start>d__1::MoveNext()
extern void U3CStartU3Ed__1_MoveNext_m88F1AB7C0C0F897822A5FEF4575C24852B21AB3E (void);
// 0x00000005 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.Gltf.TestGlbLoading/<Start>d__1::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__1_SetStateMachine_m52E2A34A6D3B2748F523AC44555A07354EBD4A29 (void);
// 0x00000006 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.Gltf.TestGltfLoading::get_RelativePath()
extern void TestGltfLoading_get_RelativePath_m4ED0B6227B9F3DB4FB6ED2D674E7C69D9EC28B03 (void);
// 0x00000007 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.Gltf.TestGltfLoading::get_AbsolutePath()
extern void TestGltfLoading_get_AbsolutePath_m9AB7588290DB9610B5EE66AC20B25EC6BEF28C60 (void);
// 0x00000008 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.Gltf.TestGltfLoading::Start()
extern void TestGltfLoading_Start_m38B28A18F3EC3594670EACF8A4B13B114A93BFEE (void);
// 0x00000009 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.Gltf.TestGltfLoading::.ctor()
extern void TestGltfLoading__ctor_m273584C6793E4724E38D29B31202387E26C36D25 (void);
// 0x0000000A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.Gltf.TestGltfLoading/<Start>d__7::.ctor()
extern void U3CStartU3Ed__7__ctor_m01A75BE3D98592F84C1E327E2FB58B8366A74742 (void);
// 0x0000000B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.Gltf.TestGltfLoading/<Start>d__7::MoveNext()
extern void U3CStartU3Ed__7_MoveNext_m365EBB8E791A72EA06B3EAEAC9BBE2B9A404698A (void);
// 0x0000000C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.Gltf.TestGltfLoading/<Start>d__7::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__7_SetStateMachine_m76E981C0A120D8F92AAB01DBBA88A34DE3EA15AE (void);
static Il2CppMethodPointer s_methodPointers[12] = 
{
	TestGlbLoading_Start_m38C9512BEF3BF429CBA458AACBBAFFB40670BB35,
	TestGlbLoading__ctor_m4DA87CBC3939A0FD6E83095AC9FE52A818202417,
	U3CStartU3Ed__1__ctor_mD8DA86170FA33604B82D6D915B64ABBD2A37EA2B,
	U3CStartU3Ed__1_MoveNext_m88F1AB7C0C0F897822A5FEF4575C24852B21AB3E,
	U3CStartU3Ed__1_SetStateMachine_m52E2A34A6D3B2748F523AC44555A07354EBD4A29,
	TestGltfLoading_get_RelativePath_m4ED0B6227B9F3DB4FB6ED2D674E7C69D9EC28B03,
	TestGltfLoading_get_AbsolutePath_m9AB7588290DB9610B5EE66AC20B25EC6BEF28C60,
	TestGltfLoading_Start_m38B28A18F3EC3594670EACF8A4B13B114A93BFEE,
	TestGltfLoading__ctor_m273584C6793E4724E38D29B31202387E26C36D25,
	U3CStartU3Ed__7__ctor_m01A75BE3D98592F84C1E327E2FB58B8366A74742,
	U3CStartU3Ed__7_MoveNext_m365EBB8E791A72EA06B3EAEAC9BBE2B9A404698A,
	U3CStartU3Ed__7_SetStateMachine_m76E981C0A120D8F92AAB01DBBA88A34DE3EA15AE,
};
static const int32_t s_InvokerIndices[12] = 
{
	4797,
	4797,
	4797,
	4797,
	3886,
	4712,
	4712,
	4797,
	4797,
	4797,
	4797,
	3886,
};
extern const CustomAttributesCacheGenerator g_Microsoft_MixedReality_Toolkit_Demos_Gltf_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Demos_Gltf_CodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Demos_Gltf_CodeGenModule = 
{
	"Microsoft.MixedReality.Toolkit.Demos.Gltf.dll",
	12,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Microsoft_MixedReality_Toolkit_Demos_Gltf_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
