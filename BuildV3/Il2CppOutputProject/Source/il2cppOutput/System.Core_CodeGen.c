﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E (void);
// 0x00000002 System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_m2EFB999454161A6B48F8DAC3753FDC190538F0F2 (void);
// 0x00000003 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8 (void);
// 0x00000004 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_mB89E91246572F009281D79730950808F17C3F353 (void);
// 0x00000005 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000007 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000008 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000009 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Take(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000000A System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::TakeIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000000B System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000C System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderByDescending(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000D System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000000E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Union(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::UnionIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000010 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Intersect(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000011 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::IntersectIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000012 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000013 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000014 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000015 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000016 TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000017 TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000018 TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000019 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Empty()
// 0x0000001A System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001B System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001C System.Boolean System.Linq.Enumerable::All(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001D System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000001E System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x0000001F System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000020 TAccumulate System.Linq.Enumerable::Aggregate(System.Collections.Generic.IEnumerable`1<TSource>,TAccumulate,System.Func`3<TAccumulate,TSource,TAccumulate>)
// 0x00000021 System.Int32 System.Linq.Enumerable::Sum(System.Collections.Generic.IEnumerable`1<System.Int32>)
extern void Enumerable_Sum_m6CFC8CEAC70AE3C469A5D1993FAF8EEEC6A06FB5 (void);
// 0x00000022 System.Void System.Linq.Enumerable/Iterator`1::.ctor()
// 0x00000023 TSource System.Linq.Enumerable/Iterator`1::get_Current()
// 0x00000024 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/Iterator`1::Clone()
// 0x00000025 System.Void System.Linq.Enumerable/Iterator`1::Dispose()
// 0x00000026 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/Iterator`1::GetEnumerator()
// 0x00000027 System.Boolean System.Linq.Enumerable/Iterator`1::MoveNext()
// 0x00000028 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000029 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000002A System.Object System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x0000002B System.Collections.IEnumerator System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000002C System.Void System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000002D System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000002E System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Clone()
// 0x0000002F System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::Dispose()
// 0x00000030 System.Boolean System.Linq.Enumerable/WhereEnumerableIterator`1::MoveNext()
// 0x00000031 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000032 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000033 System.Void System.Linq.Enumerable/WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000034 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Clone()
// 0x00000035 System.Boolean System.Linq.Enumerable/WhereArrayIterator`1::MoveNext()
// 0x00000036 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000037 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000038 System.Void System.Linq.Enumerable/WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000039 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Clone()
// 0x0000003A System.Boolean System.Linq.Enumerable/WhereListIterator`1::MoveNext()
// 0x0000003B System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000003C System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000003D System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000003E System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Clone()
// 0x0000003F System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Dispose()
// 0x00000040 System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2::MoveNext()
// 0x00000041 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000042 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000043 System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000044 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Clone()
// 0x00000045 System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2::MoveNext()
// 0x00000046 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000047 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000048 System.Void System.Linq.Enumerable/WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000049 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Clone()
// 0x0000004A System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2::MoveNext()
// 0x0000004B System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000004C System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000004D System.Void System.Linq.Enumerable/<>c__DisplayClass6_0`1::.ctor()
// 0x0000004E System.Boolean System.Linq.Enumerable/<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x0000004F System.Void System.Linq.Enumerable/<>c__DisplayClass7_0`3::.ctor()
// 0x00000050 TResult System.Linq.Enumerable/<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x00000051 System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::.ctor(System.Int32)
// 0x00000052 System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::System.IDisposable.Dispose()
// 0x00000053 System.Boolean System.Linq.Enumerable/<TakeIterator>d__25`1::MoveNext()
// 0x00000054 System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::<>m__Finally1()
// 0x00000055 TSource System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000056 System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.IEnumerator.Reset()
// 0x00000057 System.Object System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.IEnumerator.get_Current()
// 0x00000058 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000059 System.Collections.IEnumerator System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000005A System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::.ctor(System.Int32)
// 0x0000005B System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::System.IDisposable.Dispose()
// 0x0000005C System.Boolean System.Linq.Enumerable/<UnionIterator>d__71`1::MoveNext()
// 0x0000005D System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::<>m__Finally1()
// 0x0000005E System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::<>m__Finally2()
// 0x0000005F TSource System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000060 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerator.Reset()
// 0x00000061 System.Object System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerator.get_Current()
// 0x00000062 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x00000063 System.Collections.IEnumerator System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000064 System.Void System.Linq.Enumerable/<IntersectIterator>d__74`1::.ctor(System.Int32)
// 0x00000065 System.Void System.Linq.Enumerable/<IntersectIterator>d__74`1::System.IDisposable.Dispose()
// 0x00000066 System.Boolean System.Linq.Enumerable/<IntersectIterator>d__74`1::MoveNext()
// 0x00000067 System.Void System.Linq.Enumerable/<IntersectIterator>d__74`1::<>m__Finally1()
// 0x00000068 TSource System.Linq.Enumerable/<IntersectIterator>d__74`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000069 System.Void System.Linq.Enumerable/<IntersectIterator>d__74`1::System.Collections.IEnumerator.Reset()
// 0x0000006A System.Object System.Linq.Enumerable/<IntersectIterator>d__74`1::System.Collections.IEnumerator.get_Current()
// 0x0000006B System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<IntersectIterator>d__74`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000006C System.Collections.IEnumerator System.Linq.Enumerable/<IntersectIterator>d__74`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000006D System.Void System.Linq.EmptyEnumerable`1::.cctor()
// 0x0000006E System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000006F System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x00000070 System.Boolean System.Linq.Set`1::Add(TElement)
// 0x00000071 System.Boolean System.Linq.Set`1::Remove(TElement)
// 0x00000072 System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x00000073 System.Void System.Linq.Set`1::Resize()
// 0x00000074 System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x00000075 System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x00000076 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000077 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000078 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000079 System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x0000007A System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::.ctor(System.Int32)
// 0x0000007B System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x0000007C System.Boolean System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::MoveNext()
// 0x0000007D TElement System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x0000007E System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x0000007F System.Object System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x00000080 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000081 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000082 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x00000083 System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x00000084 System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x00000085 System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x00000086 System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x00000087 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x00000088 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x00000089 System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x0000008A System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x0000008B TElement[] System.Linq.Buffer`1::ToArray()
// 0x0000008C System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x0000008D System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x0000008E System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000008F System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000090 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000091 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000092 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x00000093 System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x00000094 System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x00000095 System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x00000096 System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x00000097 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x00000098 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000099 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x0000009A System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x0000009B System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x0000009C System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x0000009D System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x0000009E System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x0000009F System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x000000A0 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x000000A1 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x000000A2 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x000000A3 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x000000A4 System.Void System.Collections.Generic.HashSet`1/Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000000A5 System.Void System.Collections.Generic.HashSet`1/Enumerator::Dispose()
// 0x000000A6 System.Boolean System.Collections.Generic.HashSet`1/Enumerator::MoveNext()
// 0x000000A7 T System.Collections.Generic.HashSet`1/Enumerator::get_Current()
// 0x000000A8 System.Object System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.get_Current()
// 0x000000A9 System.Void System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.Reset()
// 0x000000AA System.Void System.Collections.Generic.ICollectionDebugView`1::.ctor(System.Collections.Generic.ICollection`1<T>)
// 0x000000AB T[] System.Collections.Generic.ICollectionDebugView`1::get_Items()
static Il2CppMethodPointer s_methodPointers[171] = 
{
	Error_ArgumentNull_m0EDA0D46D72CA692518E3E2EB75B48044D8FD41E,
	Error_ArgumentOutOfRange_m2EFB999454161A6B48F8DAC3753FDC190538F0F2,
	Error_MoreThanOneMatch_m4C4756AF34A76EF12F3B2B6D8C78DE547F0FBCF8,
	Error_NoElements_mB89E91246572F009281D79730950808F17C3F353,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Sum_m6CFC8CEAC70AE3C469A5D1993FAF8EEEC6A06FB5,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[171] = 
{
	6795,
	6795,
	7026,
	7026,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	6696,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[58] = 
{
	{ 0x02000004, { 87, 4 } },
	{ 0x02000005, { 91, 9 } },
	{ 0x02000006, { 102, 7 } },
	{ 0x02000007, { 111, 10 } },
	{ 0x02000008, { 123, 11 } },
	{ 0x02000009, { 137, 9 } },
	{ 0x0200000A, { 149, 12 } },
	{ 0x0200000B, { 164, 1 } },
	{ 0x0200000C, { 165, 2 } },
	{ 0x0200000D, { 167, 8 } },
	{ 0x0200000E, { 175, 12 } },
	{ 0x0200000F, { 187, 12 } },
	{ 0x02000010, { 199, 2 } },
	{ 0x02000012, { 201, 8 } },
	{ 0x02000014, { 209, 3 } },
	{ 0x02000015, { 214, 5 } },
	{ 0x02000016, { 219, 7 } },
	{ 0x02000017, { 226, 3 } },
	{ 0x02000018, { 229, 7 } },
	{ 0x02000019, { 236, 4 } },
	{ 0x0200001A, { 240, 23 } },
	{ 0x0200001C, { 263, 2 } },
	{ 0x0200001D, { 265, 2 } },
	{ 0x06000005, { 0, 10 } },
	{ 0x06000006, { 10, 10 } },
	{ 0x06000007, { 20, 5 } },
	{ 0x06000008, { 25, 5 } },
	{ 0x06000009, { 30, 1 } },
	{ 0x0600000A, { 31, 2 } },
	{ 0x0600000B, { 33, 2 } },
	{ 0x0600000C, { 35, 2 } },
	{ 0x0600000D, { 37, 1 } },
	{ 0x0600000E, { 38, 1 } },
	{ 0x0600000F, { 39, 2 } },
	{ 0x06000010, { 41, 1 } },
	{ 0x06000011, { 42, 2 } },
	{ 0x06000012, { 44, 3 } },
	{ 0x06000013, { 47, 2 } },
	{ 0x06000014, { 49, 4 } },
	{ 0x06000015, { 53, 4 } },
	{ 0x06000016, { 57, 4 } },
	{ 0x06000017, { 61, 3 } },
	{ 0x06000018, { 64, 3 } },
	{ 0x06000019, { 67, 1 } },
	{ 0x0600001A, { 68, 1 } },
	{ 0x0600001B, { 69, 3 } },
	{ 0x0600001C, { 72, 3 } },
	{ 0x0600001D, { 75, 2 } },
	{ 0x0600001E, { 77, 2 } },
	{ 0x0600001F, { 79, 5 } },
	{ 0x06000020, { 84, 3 } },
	{ 0x06000031, { 100, 2 } },
	{ 0x06000036, { 109, 2 } },
	{ 0x0600003B, { 121, 2 } },
	{ 0x06000041, { 134, 3 } },
	{ 0x06000046, { 146, 3 } },
	{ 0x0600004B, { 161, 3 } },
	{ 0x06000078, { 212, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[267] = 
{
	{ (Il2CppRGCTXDataType)2, 6824 },
	{ (Il2CppRGCTXDataType)3, 26151 },
	{ (Il2CppRGCTXDataType)2, 10763 },
	{ (Il2CppRGCTXDataType)2, 10055 },
	{ (Il2CppRGCTXDataType)3, 45371 },
	{ (Il2CppRGCTXDataType)2, 7499 },
	{ (Il2CppRGCTXDataType)2, 10081 },
	{ (Il2CppRGCTXDataType)3, 45415 },
	{ (Il2CppRGCTXDataType)2, 10066 },
	{ (Il2CppRGCTXDataType)3, 45387 },
	{ (Il2CppRGCTXDataType)2, 6825 },
	{ (Il2CppRGCTXDataType)3, 26152 },
	{ (Il2CppRGCTXDataType)2, 10795 },
	{ (Il2CppRGCTXDataType)2, 10092 },
	{ (Il2CppRGCTXDataType)3, 45431 },
	{ (Il2CppRGCTXDataType)2, 7525 },
	{ (Il2CppRGCTXDataType)2, 10116 },
	{ (Il2CppRGCTXDataType)3, 45573 },
	{ (Il2CppRGCTXDataType)2, 10104 },
	{ (Il2CppRGCTXDataType)3, 45496 },
	{ (Il2CppRGCTXDataType)2, 1212 },
	{ (Il2CppRGCTXDataType)3, 205 },
	{ (Il2CppRGCTXDataType)3, 206 },
	{ (Il2CppRGCTXDataType)2, 3877 },
	{ (Il2CppRGCTXDataType)3, 16287 },
	{ (Il2CppRGCTXDataType)2, 1213 },
	{ (Il2CppRGCTXDataType)3, 215 },
	{ (Il2CppRGCTXDataType)3, 216 },
	{ (Il2CppRGCTXDataType)2, 3890 },
	{ (Il2CppRGCTXDataType)3, 16294 },
	{ (Il2CppRGCTXDataType)3, 50769 },
	{ (Il2CppRGCTXDataType)2, 1252 },
	{ (Il2CppRGCTXDataType)3, 384 },
	{ (Il2CppRGCTXDataType)2, 8215 },
	{ (Il2CppRGCTXDataType)3, 35785 },
	{ (Il2CppRGCTXDataType)2, 8216 },
	{ (Il2CppRGCTXDataType)3, 35786 },
	{ (Il2CppRGCTXDataType)3, 21442 },
	{ (Il2CppRGCTXDataType)3, 50805 },
	{ (Il2CppRGCTXDataType)2, 1256 },
	{ (Il2CppRGCTXDataType)3, 416 },
	{ (Il2CppRGCTXDataType)3, 50728 },
	{ (Il2CppRGCTXDataType)2, 1239 },
	{ (Il2CppRGCTXDataType)3, 336 },
	{ (Il2CppRGCTXDataType)2, 1612 },
	{ (Il2CppRGCTXDataType)3, 3046 },
	{ (Il2CppRGCTXDataType)3, 3047 },
	{ (Il2CppRGCTXDataType)2, 7500 },
	{ (Il2CppRGCTXDataType)3, 28271 },
	{ (Il2CppRGCTXDataType)2, 5888 },
	{ (Il2CppRGCTXDataType)2, 4169 },
	{ (Il2CppRGCTXDataType)2, 4430 },
	{ (Il2CppRGCTXDataType)2, 4751 },
	{ (Il2CppRGCTXDataType)2, 5889 },
	{ (Il2CppRGCTXDataType)2, 4170 },
	{ (Il2CppRGCTXDataType)2, 4431 },
	{ (Il2CppRGCTXDataType)2, 4752 },
	{ (Il2CppRGCTXDataType)2, 5890 },
	{ (Il2CppRGCTXDataType)2, 4171 },
	{ (Il2CppRGCTXDataType)2, 4432 },
	{ (Il2CppRGCTXDataType)2, 4753 },
	{ (Il2CppRGCTXDataType)2, 4433 },
	{ (Il2CppRGCTXDataType)2, 4754 },
	{ (Il2CppRGCTXDataType)3, 16288 },
	{ (Il2CppRGCTXDataType)2, 5887 },
	{ (Il2CppRGCTXDataType)2, 4429 },
	{ (Il2CppRGCTXDataType)2, 4750 },
	{ (Il2CppRGCTXDataType)2, 2709 },
	{ (Il2CppRGCTXDataType)2, 4415 },
	{ (Il2CppRGCTXDataType)2, 4416 },
	{ (Il2CppRGCTXDataType)2, 4748 },
	{ (Il2CppRGCTXDataType)3, 16286 },
	{ (Il2CppRGCTXDataType)2, 4414 },
	{ (Il2CppRGCTXDataType)2, 4747 },
	{ (Il2CppRGCTXDataType)3, 16285 },
	{ (Il2CppRGCTXDataType)2, 4168 },
	{ (Il2CppRGCTXDataType)2, 4428 },
	{ (Il2CppRGCTXDataType)2, 4167 },
	{ (Il2CppRGCTXDataType)3, 50693 },
	{ (Il2CppRGCTXDataType)3, 15068 },
	{ (Il2CppRGCTXDataType)2, 3675 },
	{ (Il2CppRGCTXDataType)2, 4418 },
	{ (Il2CppRGCTXDataType)2, 4749 },
	{ (Il2CppRGCTXDataType)2, 4998 },
	{ (Il2CppRGCTXDataType)2, 4458 },
	{ (Il2CppRGCTXDataType)2, 4761 },
	{ (Il2CppRGCTXDataType)3, 16509 },
	{ (Il2CppRGCTXDataType)3, 26153 },
	{ (Il2CppRGCTXDataType)3, 26155 },
	{ (Il2CppRGCTXDataType)2, 883 },
	{ (Il2CppRGCTXDataType)3, 26154 },
	{ (Il2CppRGCTXDataType)3, 26163 },
	{ (Il2CppRGCTXDataType)2, 6828 },
	{ (Il2CppRGCTXDataType)2, 10067 },
	{ (Il2CppRGCTXDataType)3, 45388 },
	{ (Il2CppRGCTXDataType)3, 26164 },
	{ (Il2CppRGCTXDataType)2, 4514 },
	{ (Il2CppRGCTXDataType)2, 4804 },
	{ (Il2CppRGCTXDataType)3, 16301 },
	{ (Il2CppRGCTXDataType)3, 50658 },
	{ (Il2CppRGCTXDataType)2, 10105 },
	{ (Il2CppRGCTXDataType)3, 45497 },
	{ (Il2CppRGCTXDataType)3, 26156 },
	{ (Il2CppRGCTXDataType)2, 6827 },
	{ (Il2CppRGCTXDataType)2, 10056 },
	{ (Il2CppRGCTXDataType)3, 45372 },
	{ (Il2CppRGCTXDataType)3, 16300 },
	{ (Il2CppRGCTXDataType)3, 26157 },
	{ (Il2CppRGCTXDataType)3, 50657 },
	{ (Il2CppRGCTXDataType)2, 10093 },
	{ (Il2CppRGCTXDataType)3, 45432 },
	{ (Il2CppRGCTXDataType)3, 26170 },
	{ (Il2CppRGCTXDataType)2, 6829 },
	{ (Il2CppRGCTXDataType)2, 10082 },
	{ (Il2CppRGCTXDataType)3, 45416 },
	{ (Il2CppRGCTXDataType)3, 28332 },
	{ (Il2CppRGCTXDataType)3, 13024 },
	{ (Il2CppRGCTXDataType)3, 16302 },
	{ (Il2CppRGCTXDataType)3, 13023 },
	{ (Il2CppRGCTXDataType)3, 26171 },
	{ (Il2CppRGCTXDataType)3, 50659 },
	{ (Il2CppRGCTXDataType)2, 10117 },
	{ (Il2CppRGCTXDataType)3, 45574 },
	{ (Il2CppRGCTXDataType)3, 26184 },
	{ (Il2CppRGCTXDataType)2, 6831 },
	{ (Il2CppRGCTXDataType)2, 10107 },
	{ (Il2CppRGCTXDataType)3, 45499 },
	{ (Il2CppRGCTXDataType)3, 26185 },
	{ (Il2CppRGCTXDataType)2, 4517 },
	{ (Il2CppRGCTXDataType)2, 4807 },
	{ (Il2CppRGCTXDataType)3, 16306 },
	{ (Il2CppRGCTXDataType)3, 16305 },
	{ (Il2CppRGCTXDataType)2, 10069 },
	{ (Il2CppRGCTXDataType)3, 45390 },
	{ (Il2CppRGCTXDataType)3, 50665 },
	{ (Il2CppRGCTXDataType)2, 10106 },
	{ (Il2CppRGCTXDataType)3, 45498 },
	{ (Il2CppRGCTXDataType)3, 26177 },
	{ (Il2CppRGCTXDataType)2, 6830 },
	{ (Il2CppRGCTXDataType)2, 10095 },
	{ (Il2CppRGCTXDataType)3, 45434 },
	{ (Il2CppRGCTXDataType)3, 16304 },
	{ (Il2CppRGCTXDataType)3, 16303 },
	{ (Il2CppRGCTXDataType)3, 26178 },
	{ (Il2CppRGCTXDataType)2, 10068 },
	{ (Il2CppRGCTXDataType)3, 45389 },
	{ (Il2CppRGCTXDataType)3, 50664 },
	{ (Il2CppRGCTXDataType)2, 10094 },
	{ (Il2CppRGCTXDataType)3, 45433 },
	{ (Il2CppRGCTXDataType)3, 26191 },
	{ (Il2CppRGCTXDataType)2, 6832 },
	{ (Il2CppRGCTXDataType)2, 10119 },
	{ (Il2CppRGCTXDataType)3, 45576 },
	{ (Il2CppRGCTXDataType)3, 28333 },
	{ (Il2CppRGCTXDataType)3, 13026 },
	{ (Il2CppRGCTXDataType)3, 16308 },
	{ (Il2CppRGCTXDataType)3, 16307 },
	{ (Il2CppRGCTXDataType)3, 13025 },
	{ (Il2CppRGCTXDataType)3, 26192 },
	{ (Il2CppRGCTXDataType)2, 10070 },
	{ (Il2CppRGCTXDataType)3, 45391 },
	{ (Il2CppRGCTXDataType)3, 50666 },
	{ (Il2CppRGCTXDataType)2, 10118 },
	{ (Il2CppRGCTXDataType)3, 45575 },
	{ (Il2CppRGCTXDataType)3, 16298 },
	{ (Il2CppRGCTXDataType)3, 16299 },
	{ (Il2CppRGCTXDataType)3, 16309 },
	{ (Il2CppRGCTXDataType)3, 386 },
	{ (Il2CppRGCTXDataType)2, 4506 },
	{ (Il2CppRGCTXDataType)2, 4798 },
	{ (Il2CppRGCTXDataType)3, 388 },
	{ (Il2CppRGCTXDataType)2, 879 },
	{ (Il2CppRGCTXDataType)2, 1253 },
	{ (Il2CppRGCTXDataType)3, 385 },
	{ (Il2CppRGCTXDataType)3, 387 },
	{ (Il2CppRGCTXDataType)3, 418 },
	{ (Il2CppRGCTXDataType)3, 419 },
	{ (Il2CppRGCTXDataType)2, 9286 },
	{ (Il2CppRGCTXDataType)3, 41332 },
	{ (Il2CppRGCTXDataType)2, 4509 },
	{ (Il2CppRGCTXDataType)2, 4800 },
	{ (Il2CppRGCTXDataType)3, 41333 },
	{ (Il2CppRGCTXDataType)3, 421 },
	{ (Il2CppRGCTXDataType)2, 881 },
	{ (Il2CppRGCTXDataType)2, 1257 },
	{ (Il2CppRGCTXDataType)3, 417 },
	{ (Il2CppRGCTXDataType)3, 420 },
	{ (Il2CppRGCTXDataType)3, 338 },
	{ (Il2CppRGCTXDataType)2, 9284 },
	{ (Il2CppRGCTXDataType)3, 41329 },
	{ (Il2CppRGCTXDataType)2, 4503 },
	{ (Il2CppRGCTXDataType)2, 4796 },
	{ (Il2CppRGCTXDataType)3, 41330 },
	{ (Il2CppRGCTXDataType)3, 41331 },
	{ (Il2CppRGCTXDataType)3, 340 },
	{ (Il2CppRGCTXDataType)2, 877 },
	{ (Il2CppRGCTXDataType)2, 1240 },
	{ (Il2CppRGCTXDataType)3, 337 },
	{ (Il2CppRGCTXDataType)3, 339 },
	{ (Il2CppRGCTXDataType)2, 10811 },
	{ (Il2CppRGCTXDataType)2, 2710 },
	{ (Il2CppRGCTXDataType)3, 15110 },
	{ (Il2CppRGCTXDataType)2, 3692 },
	{ (Il2CppRGCTXDataType)2, 11252 },
	{ (Il2CppRGCTXDataType)3, 41326 },
	{ (Il2CppRGCTXDataType)3, 41327 },
	{ (Il2CppRGCTXDataType)2, 5014 },
	{ (Il2CppRGCTXDataType)3, 41328 },
	{ (Il2CppRGCTXDataType)2, 783 },
	{ (Il2CppRGCTXDataType)2, 1216 },
	{ (Il2CppRGCTXDataType)3, 258 },
	{ (Il2CppRGCTXDataType)3, 35760 },
	{ (Il2CppRGCTXDataType)2, 8217 },
	{ (Il2CppRGCTXDataType)3, 35787 },
	{ (Il2CppRGCTXDataType)2, 1613 },
	{ (Il2CppRGCTXDataType)3, 3048 },
	{ (Il2CppRGCTXDataType)3, 35766 },
	{ (Il2CppRGCTXDataType)3, 12963 },
	{ (Il2CppRGCTXDataType)2, 914 },
	{ (Il2CppRGCTXDataType)3, 35761 },
	{ (Il2CppRGCTXDataType)2, 8212 },
	{ (Il2CppRGCTXDataType)3, 3477 },
	{ (Il2CppRGCTXDataType)2, 1635 },
	{ (Il2CppRGCTXDataType)2, 2894 },
	{ (Il2CppRGCTXDataType)3, 12981 },
	{ (Il2CppRGCTXDataType)3, 35762 },
	{ (Il2CppRGCTXDataType)3, 12958 },
	{ (Il2CppRGCTXDataType)3, 12959 },
	{ (Il2CppRGCTXDataType)3, 12957 },
	{ (Il2CppRGCTXDataType)3, 12960 },
	{ (Il2CppRGCTXDataType)2, 2890 },
	{ (Il2CppRGCTXDataType)2, 10881 },
	{ (Il2CppRGCTXDataType)3, 16296 },
	{ (Il2CppRGCTXDataType)3, 12962 },
	{ (Il2CppRGCTXDataType)2, 4339 },
	{ (Il2CppRGCTXDataType)3, 12961 },
	{ (Il2CppRGCTXDataType)2, 4176 },
	{ (Il2CppRGCTXDataType)2, 10804 },
	{ (Il2CppRGCTXDataType)2, 4461 },
	{ (Il2CppRGCTXDataType)2, 4763 },
	{ (Il2CppRGCTXDataType)3, 15089 },
	{ (Il2CppRGCTXDataType)2, 3685 },
	{ (Il2CppRGCTXDataType)3, 17203 },
	{ (Il2CppRGCTXDataType)3, 17204 },
	{ (Il2CppRGCTXDataType)3, 17209 },
	{ (Il2CppRGCTXDataType)2, 5008 },
	{ (Il2CppRGCTXDataType)3, 17206 },
	{ (Il2CppRGCTXDataType)3, 51853 },
	{ (Il2CppRGCTXDataType)2, 2898 },
	{ (Il2CppRGCTXDataType)3, 13011 },
	{ (Il2CppRGCTXDataType)1, 4330 },
	{ (Il2CppRGCTXDataType)2, 10817 },
	{ (Il2CppRGCTXDataType)3, 17205 },
	{ (Il2CppRGCTXDataType)1, 10817 },
	{ (Il2CppRGCTXDataType)1, 5008 },
	{ (Il2CppRGCTXDataType)2, 11250 },
	{ (Il2CppRGCTXDataType)2, 10817 },
	{ (Il2CppRGCTXDataType)2, 4467 },
	{ (Il2CppRGCTXDataType)2, 4767 },
	{ (Il2CppRGCTXDataType)3, 17210 },
	{ (Il2CppRGCTXDataType)3, 17208 },
	{ (Il2CppRGCTXDataType)3, 17207 },
	{ (Il2CppRGCTXDataType)2, 660 },
	{ (Il2CppRGCTXDataType)3, 13027 },
	{ (Il2CppRGCTXDataType)2, 892 },
	{ (Il2CppRGCTXDataType)2, 4185 },
	{ (Il2CppRGCTXDataType)2, 10819 },
};
extern const CustomAttributesCacheGenerator g_System_Core_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_System_Core_CodeGenModule;
const Il2CppCodeGenModule g_System_Core_CodeGenModule = 
{
	"System.Core.dll",
	171,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	58,
	s_rgctxIndices,
	267,
	s_rgctxValues,
	NULL,
	g_System_Core_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
