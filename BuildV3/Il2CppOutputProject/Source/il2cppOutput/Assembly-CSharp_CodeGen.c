﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Single ARFeatheredPlaneMeshVisualizer::get_featheringWidth()
extern void ARFeatheredPlaneMeshVisualizer_get_featheringWidth_m8BE519A22AD47F3A760464845108D0944511C87D (void);
// 0x00000002 System.Void ARFeatheredPlaneMeshVisualizer::set_featheringWidth(System.Single)
extern void ARFeatheredPlaneMeshVisualizer_set_featheringWidth_mD87ACEDD2FF4BC88F656401F29F1D1810A1104FF (void);
// 0x00000003 System.Void ARFeatheredPlaneMeshVisualizer::Awake()
extern void ARFeatheredPlaneMeshVisualizer_Awake_m993B679E7BFB88DC915BA81EFF12E0E87C15A7F2 (void);
// 0x00000004 System.Void ARFeatheredPlaneMeshVisualizer::OnEnable()
extern void ARFeatheredPlaneMeshVisualizer_OnEnable_m155B23B8A1DF0DD73F1162A0345C4A12C7F604EF (void);
// 0x00000005 System.Void ARFeatheredPlaneMeshVisualizer::OnDisable()
extern void ARFeatheredPlaneMeshVisualizer_OnDisable_mAE620C99AE5DE85F1C82B9E9A35C6780C8C7E67D (void);
// 0x00000006 System.Void ARFeatheredPlaneMeshVisualizer::ARPlane_boundaryUpdated(UnityEngine.XR.ARFoundation.ARPlaneBoundaryChangedEventArgs)
extern void ARFeatheredPlaneMeshVisualizer_ARPlane_boundaryUpdated_m40EC1ED3A7A6F667F0449A09A480089094B7A24E (void);
// 0x00000007 System.Void ARFeatheredPlaneMeshVisualizer::GenerateBoundaryUVs(UnityEngine.Mesh)
extern void ARFeatheredPlaneMeshVisualizer_GenerateBoundaryUVs_mB3462F5523511A902C34362EDD0516C249F88B75 (void);
// 0x00000008 System.Void ARFeatheredPlaneMeshVisualizer::.ctor()
extern void ARFeatheredPlaneMeshVisualizer__ctor_mB44A8435E0EC4636C44C3B033562170947741D68 (void);
// 0x00000009 System.Void ARFeatheredPlaneMeshVisualizer::.cctor()
extern void ARFeatheredPlaneMeshVisualizer__cctor_mEC2B60FD20D1136E61018EB6E9529C2E4A2050B2 (void);
// 0x0000000A UnityEngine.GameObject AnchorCreator::get_AnchorPrefab()
extern void AnchorCreator_get_AnchorPrefab_mCBC6DDFB725DC05F6F1610FE7D322B357A08536C (void);
// 0x0000000B System.Void AnchorCreator::set_AnchorPrefab(UnityEngine.GameObject)
extern void AnchorCreator_set_AnchorPrefab_m9F7395B86B99482963E7BFDDF264CD2CEBA143BA (void);
// 0x0000000C System.Void AnchorCreator::RemoveAllAnchors()
extern void AnchorCreator_RemoveAllAnchors_m34875F8B1A50A4C417E4C417D0B339A88A9AC92E (void);
// 0x0000000D System.Void AnchorCreator::Awake()
extern void AnchorCreator_Awake_m56775052F5AE0008240A8A71DDB205E8E9B8A7E6 (void);
// 0x0000000E System.Void AnchorCreator::Update()
extern void AnchorCreator_Update_mF7D8BE0D5E2E77762285F666F94095AA2A5FDED2 (void);
// 0x0000000F System.Void AnchorCreator::.ctor()
extern void AnchorCreator__ctor_m0081A533A02EB12D0C1D6243FB8CF360DB688F55 (void);
// 0x00000010 System.Void AnchorCreator::.cctor()
extern void AnchorCreator__cctor_m690F180F316103AF8B707D214C588406D5D9ED64 (void);
// 0x00000011 System.Void ButtonCodeChecker::Update()
extern void ButtonCodeChecker_Update_mB6C8E767CE3F4467E311422EC7C6CD9AFBF9037F (void);
// 0x00000012 System.Void ButtonCodeChecker::.ctor()
extern void ButtonCodeChecker__ctor_m8A91B4F891ADAE8DB48A87F76818C66D830A2C1F (void);
// 0x00000013 System.Void ButtonNumberChange::Start()
extern void ButtonNumberChange_Start_m3FD0A889466638FF628B7F63208186D5B242893F (void);
// 0x00000014 System.Void ButtonNumberChange::WriteInConsole()
extern void ButtonNumberChange_WriteInConsole_m7232E7FC3BD64B26556219FFF72C926C2F8E5AC4 (void);
// 0x00000015 System.Void ButtonNumberChange::AddToNumber()
extern void ButtonNumberChange_AddToNumber_m7F57F5D278DE5EF415EB77435E86F557B803A10B (void);
// 0x00000016 System.Void ButtonNumberChange::.ctor()
extern void ButtonNumberChange__ctor_mFB24C8BB0F772D7C2C9025101AFA70A046682574 (void);
// 0x00000017 System.Void ButtonOrder::Start()
extern void ButtonOrder_Start_m241D3CD2CDBDB2EF5BEFB8FC0B5DE00155A00F3A (void);
// 0x00000018 System.Void ButtonOrder::StartTheGlow()
extern void ButtonOrder_StartTheGlow_m3DCEF84F43779917F18F8EE982376F14BCC24C93 (void);
// 0x00000019 System.Collections.IEnumerator ButtonOrder::ObjectGlow()
extern void ButtonOrder_ObjectGlow_mFFACD508BE48F738C0B6F6670043CCD24E8D9A28 (void);
// 0x0000001A System.Void ButtonOrder::.ctor()
extern void ButtonOrder__ctor_mB6A5CCFB39A9AD500AAEBFE68994034176BF58E6 (void);
// 0x0000001B System.Void ButtonOrder/<ObjectGlow>d__4::.ctor(System.Int32)
extern void U3CObjectGlowU3Ed__4__ctor_mFFF411C1EF1D8807296A811F1F11ED26AD45D30D (void);
// 0x0000001C System.Void ButtonOrder/<ObjectGlow>d__4::System.IDisposable.Dispose()
extern void U3CObjectGlowU3Ed__4_System_IDisposable_Dispose_mDDAB1BA6F77D08AA6F33C428CC442A4F6DDD67C0 (void);
// 0x0000001D System.Boolean ButtonOrder/<ObjectGlow>d__4::MoveNext()
extern void U3CObjectGlowU3Ed__4_MoveNext_mB3E6AB0065460FD0B812E7812A886749DF2A7C04 (void);
// 0x0000001E System.Object ButtonOrder/<ObjectGlow>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CObjectGlowU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE1361ED0C0996CF5D8FFF29168BDB0CC68A62A85 (void);
// 0x0000001F System.Void ButtonOrder/<ObjectGlow>d__4::System.Collections.IEnumerator.Reset()
extern void U3CObjectGlowU3Ed__4_System_Collections_IEnumerator_Reset_m49133519AB880CF16DE2DB819F3D2D174E7F9FBE (void);
// 0x00000020 System.Object ButtonOrder/<ObjectGlow>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CObjectGlowU3Ed__4_System_Collections_IEnumerator_get_Current_mCF9EF965D329EA8A4BA33843423711DF62E304D6 (void);
// 0x00000021 System.Void GemStoneScript::Start()
extern void GemStoneScript_Start_m5CF5ED869E1C269767B4B8F63CA2131D99D5D020 (void);
// 0x00000022 System.Void GemStoneScript::Update()
extern void GemStoneScript_Update_m3FCD4A056EB88E5242C8BE34452200859712783C (void);
// 0x00000023 System.Void GemStoneScript::OnTriggerEnter(UnityEngine.Collider)
extern void GemStoneScript_OnTriggerEnter_mE61C6314D01C53FEBB7A41FEA8D216EB842BD0FD (void);
// 0x00000024 System.Void GemStoneScript::openBox()
extern void GemStoneScript_openBox_mAD9EB2014659890F7578637FF8C22151F0F8B18C (void);
// 0x00000025 System.Void GemStoneScript::.ctor()
extern void GemStoneScript__ctor_m18894504D13CD149B012E0480CDBAC973B07DD1D (void);
// 0x00000026 System.Void nailPulling::Start()
extern void nailPulling_Start_m7E1B7BD93F6F5A5BECC4F3C5353A7954D4F524A0 (void);
// 0x00000027 System.Void nailPulling::Update()
extern void nailPulling_Update_m4CF554D8635E89421C6048A6FD6148415BB056F3 (void);
// 0x00000028 System.Void nailPulling::ResetNails()
extern void nailPulling_ResetNails_m73FF2A2082B044FD519FA31A2A2626251B838891 (void);
// 0x00000029 System.Void nailPulling::destroyNails()
extern void nailPulling_destroyNails_m93290212E621256E37F808019B0DE23805119435 (void);
// 0x0000002A System.Void nailPulling::StartTheGlow()
extern void nailPulling_StartTheGlow_m7AEF03D14F55C66E411D6756CD4B7D01BAC69A50 (void);
// 0x0000002B System.Collections.IEnumerator nailPulling::ObjectGlow()
extern void nailPulling_ObjectGlow_m671AFA1FB9F8625C9ED1C3EC4F1DFF56D06486EE (void);
// 0x0000002C System.Void nailPulling::.ctor()
extern void nailPulling__ctor_mC80A4D1F829F11A75A0402DEBCD5B19F3E609813 (void);
// 0x0000002D System.Void nailPulling/<ObjectGlow>d__13::.ctor(System.Int32)
extern void U3CObjectGlowU3Ed__13__ctor_m36705611765608E1AB5180A973D57E7E35844585 (void);
// 0x0000002E System.Void nailPulling/<ObjectGlow>d__13::System.IDisposable.Dispose()
extern void U3CObjectGlowU3Ed__13_System_IDisposable_Dispose_m5B1BAE725BBB3C5F71C815456DA1E0DF40A6D6B3 (void);
// 0x0000002F System.Boolean nailPulling/<ObjectGlow>d__13::MoveNext()
extern void U3CObjectGlowU3Ed__13_MoveNext_m1CF0BF92BC16BC0B34E9091F28D64074C14BE9BC (void);
// 0x00000030 System.Object nailPulling/<ObjectGlow>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CObjectGlowU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m33227A5F49FE9DCF93DC806DF8654EEAF52C2F4B (void);
// 0x00000031 System.Void nailPulling/<ObjectGlow>d__13::System.Collections.IEnumerator.Reset()
extern void U3CObjectGlowU3Ed__13_System_Collections_IEnumerator_Reset_m49EB597CCB8F39CD4041F0C029EED9BBDA1EC76B (void);
// 0x00000032 System.Object nailPulling/<ObjectGlow>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CObjectGlowU3Ed__13_System_Collections_IEnumerator_get_Current_mC162CD86B5EF34EF47ADE853F214878EF5101AAA (void);
// 0x00000033 System.Void pipeCheck::Start()
extern void pipeCheck_Start_mB4C4F062F02C075A12348B3A20E2E47CEF31774C (void);
// 0x00000034 System.Void pipeCheck::Update()
extern void pipeCheck_Update_mEF602F1BD191510FF0390CB5C076E9FCD542CEC7 (void);
// 0x00000035 System.Boolean pipeCheck::allRight()
extern void pipeCheck_allRight_m3F664F6E4448F3ED79D5F1B0350AF543FFD71B39 (void);
// 0x00000036 System.Void pipeCheck::.ctor()
extern void pipeCheck__ctor_m0D33F7F47D62231E6C109CE2CE4A31C18E3BA62D (void);
// 0x00000037 System.Void test::Start()
extern void test_Start_m0E5D7128828095615C6BABB85F034A1FC262D37C (void);
// 0x00000038 System.Void test::Update()
extern void test_Update_m43D0AB83A5D3BC6F8ED9CAB780EDE28726FCE7D9 (void);
// 0x00000039 System.Void test::.ctor()
extern void test__ctor_mCF2B5CF07AD306E14B3243CF42DB5687A10B7D42 (void);
// 0x0000003A System.Void testZeiger::Start()
extern void testZeiger_Start_m788ACC30F8104D546645652663745C0EA999C7FA (void);
// 0x0000003B System.Void testZeiger::Update()
extern void testZeiger_Update_m64B6267F81C2D0B657A0D69D6E1215856ACA1AF4 (void);
// 0x0000003C System.Boolean testZeiger::klZeigerInPosition()
extern void testZeiger_klZeigerInPosition_m1AA09E1FE7558762DE7DF6C5CD56A69FCA04D946 (void);
// 0x0000003D System.Boolean testZeiger::grZeigerInPosition()
extern void testZeiger_grZeigerInPosition_m332ECB4F91C0DCD854055447C7AFCABB1A4C7AAD (void);
// 0x0000003E System.Void testZeiger::.ctor()
extern void testZeiger__ctor_m12B8F70EC344427B9A2AC2A432FFA8F382FD2265 (void);
// 0x0000003F System.Void PipeRotator::Update()
extern void PipeRotator_Update_m9B9900894A57953119A20D4020E78BEE65AB85EF (void);
// 0x00000040 System.Void PipeRotator::RotatePipe()
extern void PipeRotator_RotatePipe_m33AF82BF65142654308B03B4E8F752BCDFB8894C (void);
// 0x00000041 System.Void PipeRotator::Flag()
extern void PipeRotator_Flag_mF5D9CFD2ED9269D897690035EA2623B60A6A77DB (void);
// 0x00000042 System.Void PipeRotator::.ctor()
extern void PipeRotator__ctor_m4751A2A6758A6810473900C064CF153960167731 (void);
// 0x00000043 System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_mF20A5E1E9CD63D8800DD813ECDAD8EAEAF7192E1 (void);
// 0x00000044 System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_mDE9EDE3922A33C294F174FDF695AAA83750C0F5C (void);
// 0x00000045 System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m89A6C97B3E7BD66E156D8FD8CCD64541DB57F9B2 (void);
// 0x00000046 System.Void ChatController::.ctor()
extern void ChatController__ctor_m158B20E594A208D0C5F6333DD917D46FB62F4EC8 (void);
// 0x00000047 System.Void DropdownSample::OnButtonClick()
extern void DropdownSample_OnButtonClick_m79337F8A102B280E0E63457496D2F115D321D74E (void);
// 0x00000048 System.Void DropdownSample::.ctor()
extern void DropdownSample__ctor_m7E0CECA26A98DAA6F28A1B922C908C54730D1289 (void);
// 0x00000049 System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_mD5E7612C9869A91B6A0681B9D592C7D8D9445D1E (void);
// 0x0000004A System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_m3432946DE1A3B40667B9D4CE90384F765C9A1788 (void);
// 0x0000004B System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_mCF816FEE8C1CB473929754FF4C93B3CAB21EADEE (void);
// 0x0000004C System.Void EnvMapAnimator/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m0450ADD262F7838968EDCEA567F1046D7AA157E2 (void);
// 0x0000004D System.Void EnvMapAnimator/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m0E6287744F66D21C42C549B857E1A4A3BECDBC8A (void);
// 0x0000004E System.Boolean EnvMapAnimator/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m99BBB7693C8CB2F4660E3077F949F5A0AEED1489 (void);
// 0x0000004F System.Object EnvMapAnimator/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1A4BE78025454A74E21F9730A5269AE2E2461258 (void);
// 0x00000050 System.Void EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m912B63F0B462B11EAC1AA20D7DF921990F53BF82 (void);
// 0x00000051 System.Object EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m4C9D59B07EECBC8BF4F62D136E6A833D1CF3DB8E (void);
// 0x00000052 System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_m4EF3DF30BE68E1A21BE472A0A3BA1FC20EDFC347 (void);
// 0x00000053 System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m27DF8132999BAC27C2849CE216E6723BA8F35AB8 (void);
// 0x00000054 System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_m5A4A4EBF5A9028BB0FA786671211B9E213AD4B45 (void);
// 0x00000055 System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_m375CFE89D55D4F9090D220BF021B692285A430AB (void);
// 0x00000056 TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_m20150DBB2AA6AE1E0FF93C92F5A1C9829162D98B (void);
// 0x00000057 System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler/CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m93EE620535D6E26B880160D8B634385C25CAC811 (void);
// 0x00000058 TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m4488683A472FCDC75B48E6CBE3C93F95CE324F77 (void);
// 0x00000059 System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler/SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_m2B028D4954742D0438CA3F79FAC273A9EED03025 (void);
// 0x0000005A TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_mAD7565769E8437E05EAACDB21D7F5A3E2FED0937 (void);
// 0x0000005B System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler/WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_m4422C3D459E4BC0F143362CF55614EE0D1612C98 (void);
// 0x0000005C TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_mB62A6F4286161E57D306C87007163288DAC6CD9C (void);
// 0x0000005D System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler/LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_mFADF41D9509B00E31037ED15F4DE7E94F81DFC77 (void);
// 0x0000005E TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_mD1E4073940CC30686C8720AA612D865D3383F8A7 (void);
// 0x0000005F System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler/LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m2AF582259BC5F7361AF19BF7045B7D222F5876E7 (void);
// 0x00000060 System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_m68E0ED04A66A6C28DC19E2146D68E029F68B0550 (void);
// 0x00000061 System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_m3394ECD6DA959CB037A680CFBC5A628A51493C46 (void);
// 0x00000062 System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_m07E27AA290A22918830E5722A2BD42D9F6255351 (void);
// 0x00000063 System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_m532319C274AE6D05BE14F26487CED4043A3B9595 (void);
// 0x00000064 System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_mD31D487759CB8AA8C35E450DBB3CDE60E2D11BEF (void);
// 0x00000065 System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_m11A38EB9C3BDB99606E8CDDCD5EC4E3434DA2CEB (void);
// 0x00000066 System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_m56034E6F2C416674D8E47F4F5EC7DFE8ADF16EDD (void);
// 0x00000067 System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_m2276E25EE1D24547D63DDFE3FB3E47C74A7D9781 (void);
// 0x00000068 System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_mA21BD08A006D3D620A97629C395BE2693361A98B (void);
// 0x00000069 System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_m428CBE312988192C610700CA48B74C735E9FBDC6 (void);
// 0x0000006A System.Void TMPro.TMP_TextEventHandler/CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m687770B1255B5BF9631660AEAD91F79910478C0A (void);
// 0x0000006B System.Void TMPro.TMP_TextEventHandler/SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_mE314A5C368E728BAAEDED124763A20248037CF3A (void);
// 0x0000006C System.Void TMPro.TMP_TextEventHandler/WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_mDCF4270A199AC5D4479A161B45D9D11CD4B1BEF1 (void);
// 0x0000006D System.Void TMPro.TMP_TextEventHandler/LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m8F84A7A2C4818BE3F27D51986218B84BABDFC370 (void);
// 0x0000006E System.Void TMPro.TMP_TextEventHandler/LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m2EF8B73807E199B9C528AEB298E181822675F52F (void);
// 0x0000006F System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_mB7AE105F2B9A718DA0034E36DCB2DD5D3CE08E79 (void);
// 0x00000070 System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_m3B820F7C607C8C6572B857CABB8BCCBFA3106DB3 (void);
// 0x00000071 System.Void TMPro.Examples.Benchmark01/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_mF134B42BDB16CFE45959D2238A883F3782D96A6B (void);
// 0x00000072 System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m73EE988CEE5D33849663BBF874B890EFDF4D6C2C (void);
// 0x00000073 System.Boolean TMPro.Examples.Benchmark01/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_m1A2DA78BE014DF35ABE2623FF7E4DE3E6CE73FC7 (void);
// 0x00000074 System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m40C85CAAA5F91860372CE6EC7DE2BC0651008A47 (void);
// 0x00000075 System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m9B240EF4945F412941C11524E6ADD12C718C96CB (void);
// 0x00000076 System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m99360A58385EF846DA9324753FFD814763486E44 (void);
// 0x00000077 System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m34BF3D36C4148DD9B6AFF435494132D134DB1FEB (void);
// 0x00000078 System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_mB0EEBE63C2DA47C794890B995079A8D56A8EB8FC (void);
// 0x00000079 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_mBD96F83AB86F1DADD0205D48950AD9472C49BB3C (void);
// 0x0000007A System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mA9BCB5EB21EAD06A6B3CAA5F637E4E3E61690A15 (void);
// 0x0000007B System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_m67056499AB7657EA01F01A2244439C4AB723305F (void);
// 0x0000007C System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m73A9DBD9986792A9C873E94820A327719DAD13B2 (void);
// 0x0000007D System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m8673C491BA40498DA72FCF82D0C35CDB2B2C1302 (void);
// 0x0000007E System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m1403238260593386B4796ABC7CE5DE15710C066E (void);
// 0x0000007F System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_m859244102E662AABC6EADDF3FDDBCB70BF2B5FB9 (void);
// 0x00000080 System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_mDC57831288611477556FBDF923656A7C8749E13A (void);
// 0x00000081 System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_mE99D6A79FC839F0D59743FAD941491C7C8E547F4 (void);
// 0x00000082 System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_mB1E011A054E5B0EA0B63D5962F0998A1C7581553 (void);
// 0x00000083 System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_mA5799BD9758B18722EC7CE1A24BE236AE20CB44E (void);
// 0x00000084 System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mA03959F172F3569D71A1343425D27BAD52FEA3BD (void);
// 0x00000085 System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_m8032BF50C54C71AA79A81686361E19F47264D3E7 (void);
// 0x00000086 System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_mA09EE763D304CA3EC70F9FADE7C5A00D023928C1 (void);
// 0x00000087 System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m35D27D16A1BE5F7C721D508D62D1F3F56CDD2FA0 (void);
// 0x00000088 System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_mF9C36862FF6418664F54F9CC804A8EFADD4E91C7 (void);
// 0x00000089 System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_mC42951349834EF78DB980FE627670188C38C749B (void);
// 0x0000008A System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_mCE318CF220099289A13C48874313F1770B610857 (void);
// 0x0000008B System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_m47A9256687BDFD67EAE370636B3AB21E21D6A873 (void);
// 0x0000008C System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_m55EF26CC11FC576459CEE011C42B68E143087891 (void);
// 0x0000008D System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_m8BACC41FFA3CE1E491EE311A5008B8E345DDB843 (void);
// 0x0000008E System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_mB379A242D3FAD0F7E369D592903B1F5D7B710FED (void);
// 0x0000008F System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_mFB09A732744D5C593D66EABF79B0FFA5939C094E (void);
// 0x00000090 System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_m9635CDE20D007A404C2D8F09E0E3D474F83404BD (void);
// 0x00000091 System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_m82C7D0644ADE776B309F6FC160584D247D7EAD7B (void);
// 0x00000092 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_m1EECC5609C55D8ACE8A015D095176613DF1CC729 (void);
// 0x00000093 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m899F6FDE18D7A082B51E81253115D11211B9E306 (void);
// 0x00000094 System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_m882B221771B08AED27D03D81A7C25061E8DE7399 (void);
// 0x00000095 System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB8A061DA69769417CF2DB406083A6B9FEC9216BD (void);
// 0x00000096 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_m96C640E17307465DC89EC0216DEC638A1434FB7F (void);
// 0x00000097 System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m896EEE48086AA67D02FFA51CE50AF7184343D25B (void);
// 0x00000098 System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_m6560BF08A20093CD0A7F371A961262AFE2CFAA2F (void);
// 0x00000099 System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_m463C891946C6D01804C9EE73A4D81FF502DA4C0F (void);
// 0x0000009A System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_m96AE1681250812FEC30E561808E2E044744FB708 (void);
// 0x0000009B System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m5A5D81FC9F5C9DF02AD3EAC993CAF47D8A0BF8F9 (void);
// 0x0000009C System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m2FFD59A5BA1BFD83AD2FE7F7A8C3E121F7B9C4DD (void);
// 0x0000009D UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_m50B172C75266E7615011784535B2C8FF7F90EFCD (void);
// 0x0000009E System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_mE1969FED4C69A06BF296EB3C57D655E70067C268 (void);
// 0x0000009F System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_mC6835AABE659C8DBA5BA4A183685551B85D452BC (void);
// 0x000000A0 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_m0C10F1442F3AFF382988656986D6043B8E15A143 (void);
// 0x000000A1 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_mB08192042C7064309C074E7E2A091ACB1F4DEFD9 (void);
// 0x000000A2 System.Boolean TMPro.Examples.SkewTextExample/<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_m1D50FC174732CA47C8CE389F223587AACDF85545 (void);
// 0x000000A3 System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBB2D90533557FC705A99905EA0757D55CF5BFEFF (void);
// 0x000000A4 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_m98CD8E7319A8FBC6CAFF79C5CBA0A29C7325967E (void);
// 0x000000A5 System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m8E6A1A9AD6B87208DFEB11A4769BB5A79E90E1D3 (void);
// 0x000000A6 System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m874F3F209362AC48D33F5EBC7FA4C5723DE8D50C (void);
// 0x000000A7 System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m08F7A0207E87622D65043CE711211D59031C9A69 (void);
// 0x000000A8 System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m3A0460D571FD6B64C52567910D0224FCF55DC940 (void);
// 0x000000A9 System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m2C945EA0297D6A2611DC3F26D0E2F1C42B797691 (void);
// 0x000000AA System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_mEDAE106674BE14AFC3648C69A3E41D1CC637F39B (void);
// 0x000000AB System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m7580AA61513EE7771AD425B610ED95E172DB0776 (void);
// 0x000000AC System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_m0AC7AB661D2D6CE5983B8C1FB09F3FE593E3A4B1 (void);
// 0x000000AD System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_m0C6B6A2FB73C5C15FBE49311AE41B9119C681982 (void);
// 0x000000AE System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_m0F2BDABCF5F504D32E8B9094176D3B8BFAB934D7 (void);
// 0x000000AF System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_mD575777EFB0EC8EE29584A8DC787FC868FB5E972 (void);
// 0x000000B0 System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mC069E261BD7C80FCD49F4691AFE6CF517D57EBF0 (void);
// 0x000000B1 System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mB1857DAF79087421D71FCB8072B494A684316CE9 (void);
// 0x000000B2 System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m06978A868DAD9B374DD540E993226DFCAF1E7042 (void);
// 0x000000B3 System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mCA3F1A95EC6AED62E2133F35C9745CD9DD7F4AB2 (void);
// 0x000000B4 System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m1AD16AC94C97A5781DCD37D2D0BC5AB3D440C7EF (void);
// 0x000000B5 System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_m31E66283C7016176B606C31E6E36C86DEA576538 (void);
// 0x000000B6 System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_m0C32303C51A3BDB40380743E8042E64F265106E5 (void);
// 0x000000B7 System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_m0BA96F8F4B2FF0BC68CC45B403AEC38D81F2E09A (void);
// 0x000000B8 System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_m8B1DF00F5307B2363E9DB846E9C2BDA1EF8F35E2 (void);
// 0x000000B9 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_m9CF43FFA0C0095DFDBE147C1CC62DF56CC53788F (void);
// 0x000000BA System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_m40D418048B15375DBDDEB1C0E08460E5109D7349 (void);
// 0x000000BB System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_m8FC596D84378365BF7E6B882E0D8497E43290652 (void);
// 0x000000BC System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_mC25FC673DF7903FDCB2106D1F3E86F51A7B744CB (void);
// 0x000000BD System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m87E464E40A7CB4BFB1640D9455FC4D4E256774C1 (void);
// 0x000000BE System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_m6C263D36038359489A6D5093D48ADA52D1A512BF (void);
// 0x000000BF System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m1DBA213DDC2FAA2F93F58ECCA5449A31468CE7AA (void);
// 0x000000C0 System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_mA647A689554A9E5BD07A83FC33B47D86C1A5A6CB (void);
// 0x000000C1 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_mD6244A767EE3E832CC86E4973F3E32A5C52C7AE3 (void);
// 0x000000C2 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_mD24E329F485441800296BDABC6CFF3E29B6E3538 (void);
// 0x000000C3 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_mDF251BF4B3BB77DF313BEA93940221D8FEEB45CE (void);
// 0x000000C4 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_mCA921480DC3BCD6BF36A416DE271B8479C6AE1D8 (void);
// 0x000000C5 System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m2D57105EB1EA16EFFE705DB501FE6BF674AC9B14 (void);
// 0x000000C6 System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_m40EF80772527433E176CBF074A8D693686D043AD (void);
// 0x000000C7 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m02CDF44F571DBA212E72CE767D76C61C98E45BB9 (void);
// 0x000000C8 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_m04178DCC7FC7FFD4A454FF9875759C2BF3AE477D (void);
// 0x000000C9 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m466029DF08916A28183C65D155CBA4D97AD90085 (void);
// 0x000000CA System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_m888C1A972775B5F346E95E96196B893940B27673 (void);
// 0x000000CB System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_m8CC7F7BF10E3233E801720D103E944EC7A8F11E5 (void);
// 0x000000CC System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_mFC97D188C55093B468D2E441955AEC2F9BDC412C (void);
// 0x000000CD System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m4C9AC18BF54ABEE99F1DC416139CE15BA8A1F7B5 (void);
// 0x000000CE System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_mEFCCB302387387EE1DC8887DA8FA7D70E0F4211F (void);
// 0x000000CF System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m9AF2AB9EE959F0770A815A889F0182AA1E24822C (void);
// 0x000000D0 System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m37AB7AE2F364CEC3F6181C53750D67E08B07B63B (void);
// 0x000000D1 System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_mF4141FAE2172EC71B08DF8BD04CA1E0DAC518A55 (void);
// 0x000000D2 System.Void TMPro.Examples.TeleType/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mE761C315080CB9D9ECC5E13541382FD8F4CF6A70 (void);
// 0x000000D3 System.Void TMPro.Examples.TeleType/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m32D3DFDFEBC3CB106D26E913802FD00E209C12FF (void);
// 0x000000D4 System.Boolean TMPro.Examples.TeleType/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m5E32F1699168847254169554317E6A21C09BF04B (void);
// 0x000000D5 System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC97655E37D3DEC57375D73AAE9E98572E8F81E27 (void);
// 0x000000D6 System.Void TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m8CDA19A57B0E471F332E9C43B122FEC63FB14FB4 (void);
// 0x000000D7 System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mA02188F69F7BC357C743F33D3A643A7ECD0DBF69 (void);
// 0x000000D8 System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_mF9F53B0B1DCE3946C143F71447C087297A17AECE (void);
// 0x000000D9 System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m56193F67296DBDD9F554100249B2C2E0DF4C96F0 (void);
// 0x000000DA System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_m25D719AA368ECE39E41C4DBA260F2F901057F2B1 (void);
// 0x000000DB System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_mF7739529823689923F4D489D6070CC1C09AB9178 (void);
// 0x000000DC System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m238472B614F4F8493A2A4159D5BA023B631F1602 (void);
// 0x000000DD System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_m270C2B31ECEBC901F31CC780869FF0BF9ADBFFCF (void);
// 0x000000DE System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_m2831A10B9CBE63D3B6A96764F84F98860FDDEA87 (void);
// 0x000000DF System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_mE04116D942A1DC7E2A902EEF2D73A9BC70547055 (void);
// 0x000000E0 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_mC622CB57F2C8C2C5E5D6592FA61D37D9B2DD2F4E (void);
// 0x000000E1 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m8581BA87F52BA3D2EC25328353D57DCEF2A57932 (void);
// 0x000000E2 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_mC90DB377F1C559B997DE55C556295404BC007863 (void);
// 0x000000E3 System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F82F9A02F414D7485FD3AC3003ED386F5329009 (void);
// 0x000000E4 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_m0A85EE143D0136BDEF293624A8B419EDFBCAC183 (void);
// 0x000000E5 System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m52987689D9C37472E3A5BB72A11F676BDA9708C8 (void);
// 0x000000E6 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_mF28D666A0DA72F5541416C9A900D63FFF1826D3C (void);
// 0x000000E7 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m1BB12C3BC2792B0BFDB4BAE14EA6C876A64FEED2 (void);
// 0x000000E8 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_mF9D0045F75224C085DA0269FFA799E17D4CB3ACA (void);
// 0x000000E9 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m78510D10295A5465DF7FA6CC3062988A0A8C21F0 (void);
// 0x000000EA System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_m5DBF478AE8BD7F6A592EEAA1D0955006E69E36F6 (void);
// 0x000000EB System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mA8E2269E365F70178BEEC3156303BAB8F7115642 (void);
// 0x000000EC System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_mFEABF0FEE47328274683F4899B377117C757C9E1 (void);
// 0x000000ED System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m0ACA6020BBC364CBC2EF08C8472CD11B76066D4E (void);
// 0x000000EE System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_m214F7E4C44FAE3A3635443BD41F2645BEF83D07C (void);
// 0x000000EF System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_m634EC4DD456EB9596D9ACF27710D5F28095EEDA1 (void);
// 0x000000F0 System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_mB062CBF6B48ADA7E92D72849264414C1ED0A4F98 (void);
// 0x000000F1 System.Void TMPro.Examples.TextMeshProFloatingText::.cctor()
extern void TextMeshProFloatingText__cctor_m86FD22D3091D14EA4924C3F0CDA132B30031FFFB (void);
// 0x000000F2 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m328AB7CF0E2A3A56BC77FC55140903098CF87CFE (void);
// 0x000000F3 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m052F727B1032933B1DD2CABD4B44CFBA23662047 (void);
// 0x000000F4 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m8078CEDC5A5DEB0D8415DC1F80465065FFB7B6CA (void);
// 0x000000F5 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7C348F98BC1901EB30404695B7548067947121D8 (void);
// 0x000000F6 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_m62DBBFD80A8879E56C9BAB8B279CADA3B0D5E95F (void);
// 0x000000F7 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mC6FA041905BD4EEE785DFA63B708CD601C8C0C2A (void);
// 0x000000F8 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m27613E638D919598A9C8C2DDCA2C157E69A7D399 (void);
// 0x000000F9 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_mBB9A19DD082FE5F45831A42F6ABFF7310B783653 (void);
// 0x000000FA System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m5874F6D7A5B3DBDFB3E9CA33459D0BCF624BA3C5 (void);
// 0x000000FB System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m033D07D6C2A22DE4239E5B7F15C7535C1AE709F9 (void);
// 0x000000FC System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mB883AE948B2D5B213E28C2C08FC4FAC846090596 (void);
// 0x000000FD System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_mDB3B40C77FDD1B4D3FCB62E34C424BCB9F61715D (void);
// 0x000000FE System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_mD0F9DAFA485D992EE3155E4DC07101973539ABE2 (void);
// 0x000000FF System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_mED51273FEAFDECDACA309E0EDAD4D84530B40C8E (void);
// 0x00000100 System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_mA484641AF80E15BBE6ACD6DBE2CD6A112A6A6B92 (void);
// 0x00000101 System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_mB13FEC65FA2E52A5BD69C9280C4A834AB9E80B67 (void);
// 0x00000102 System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_m9C670DD8F01640079CA88868FC60B26F92A8426C (void);
// 0x00000103 System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m4EE5EFAAA9D9F65C811848AE36551E8137B3FDF1 (void);
// 0x00000104 System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m820FE9B9D4FD4DF13E46AC69083C8CF5C39020C8 (void);
// 0x00000105 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m69255FD61F9CB7F7AFD1468325DB6EDA90C3DD83 (void);
// 0x00000106 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m9E8F17AF5EB46169BCFBA2152681B7A4A8B870D9 (void);
// 0x00000107 System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m60B256083EA4FEB47FBE4D0A4E6A2E40506A7BE7 (void);
// 0x00000108 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF8FBC07125C99541937A71DE72A3348131A4FFD9 (void);
// 0x00000109 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m176E827425D6A6561CD6A941517F502A1AFD610E (void);
// 0x0000010A System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m25C8138AE53031CDDF90D50063B1286ED44F4266 (void);
// 0x0000010B System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_mBD36C30F9C0C86C06BC4DC6AFA89D4D9BE32137C (void);
// 0x0000010C System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_m5DBB397E98FFE9F3AEE0B4E7EF3ECF3BCDDEC022 (void);
// 0x0000010D System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_m9A4C23D5BFAFBC80171B3CC2CEC70FC55CC3956D (void);
// 0x0000010E System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_mB1E16567B0539D52986D404B4C62D04287839393 (void);
// 0x0000010F System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_mDA0B68137BB6AAF18974D43D5671481B07F01854 (void);
// 0x00000110 System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_m76363C66669DF0DD2070901DEFFF39D24C242B90 (void);
// 0x00000111 System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_m810752457B6B201D5FCA16DD9D7FD0547C73A66A (void);
// 0x00000112 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m896A9095AFF2B1F7D823B4D14CFC2527EB3152CC (void);
// 0x00000113 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_mD72B273D37EA2543C5C0B88211549EC0FA3E720D (void);
// 0x00000114 System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mB4B5A3E3D2294D96ADFB2E3A3496E1120DF95E5B (void);
// 0x00000115 System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m36CA4792794DA1C9908A9C9FC4874ACF82804AB8 (void);
// 0x00000116 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m627446844CA02880583B92428889EA9D9CEC04CC (void);
// 0x00000117 System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m7669B30A4AC65F85F8EBA4BDC640F43B74C478EC (void);
// 0x00000118 System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m370B438BD9DFEE065CEEA6909C89DFAC2DE72453 (void);
// 0x00000119 System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m8C9742B24A0516B19E9B9EEF8B3E00AEF9F273A7 (void);
// 0x0000011A System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_m8638D26DC36898D64A45E091B907858AD112DC35 (void);
// 0x0000011B System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_mB0DAAD7A718A3672087BD9200D41DA7602EEE39F (void);
// 0x0000011C System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_m4C12FD42A37E534C404594849050B7483830AE71 (void);
// 0x0000011D System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_mAEF171DDF17D8B42259C3C2F6486E550DE014BF1 (void);
// 0x0000011E System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m4163F3E6E774AF69910ADCEDBD2106B3E0B4B17B (void);
// 0x0000011F System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_mCAC1B99791AE7DD5F3F96BC4CB7645898301C2CE (void);
// 0x00000120 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m4EEC65E3452BBDCB67C9ABC7409C73FEACF9BC2F (void);
// 0x00000121 System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mC90F022F41945DEF9ED06E858D032F5754441157 (void);
// 0x00000122 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBC451FD1BCDDB66EAEAA07F399199A9DC63AFB23 (void);
// 0x00000123 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m74DE64545E3327D66FB6C069A5190BFC0FF683AC (void);
// 0x00000124 System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_mD3DFEB94E1B0A0153083715008C670364D32C097 (void);
// 0x00000125 System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_mFEC3543DAF8F57A0A034F312AE333C64D0720E01 (void);
// 0x00000126 System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_mD79056ECD2020B2296E8706B150DA95F9F925D70 (void);
// 0x00000127 System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m22CF9F7C2867971192D9109997A7557ED34B06EE (void);
// 0x00000128 System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_mEC4D4CEF675A36B236B541855B25452BFF3CAE01 (void);
// 0x00000129 System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_mDCC1525913C80D4D01BE6E57D290032E84EAAA52 (void);
// 0x0000012A System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_m4C64F1552C5539AE431814B8DE4673B764D26576 (void);
// 0x0000012B System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m29041B8CF90385D7ADFDE3DB2DA56B4F183D4969 (void);
// 0x0000012C System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_mD5440310F221693B051892C32E5825ACF2910B0C (void);
// 0x0000012D System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m85345564110FD83A156AE3D768E78E41E8D464C0 (void);
// 0x0000012E System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_mB184ABC2C84B0A4B134E026869BFBD531B234081 (void);
// 0x0000012F System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C64AB0762FDA81B311143C78B0B7314A05B40F7 (void);
// 0x00000130 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mD0C3021D6DEB76A541E9A6F93CF17691D62B417C (void);
// 0x00000131 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mAD8B239C2AFA0DAE135EED3CFB49F0816D150048 (void);
// 0x00000132 System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_mCED2795CD8617FB459D74A6C9B3E8E5E08AA30A1 (void);
// 0x00000133 System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_mDFEAE2F85132963A28A0AE9AAE061A2710A87B66 (void);
// 0x00000134 System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_m9E7C6F3B64A33C0EDF60F2F864981A5FFF147113 (void);
// 0x00000135 System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_mE306D1DA952D2254909ED9F1FE699A051B385136 (void);
// 0x00000136 System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mF83B227AFCD10F9FB6FD132DA0F4B8FDD23D2B0F (void);
// 0x00000137 System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_m7373CF7A5AC35CDBC74CE80BD56B04123A91C0C7 (void);
// 0x00000138 System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_mAC448B16E14E3F3175FC882D1FB0C39EC553EE21 (void);
// 0x00000139 System.Void TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m8141BB14AD7BC732E8A9AC7D0C4AB59A55CA526B (void);
// 0x0000013A System.Int32 TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_mA10F7FD56C7EB37EBF7BC1C764D4D10A9F7C61E6 (void);
// 0x0000013B System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_mA2C6461F1E88C3131B5C4E6FFD7C75771C73D09A (void);
// 0x0000013C System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mE5843C784E5D25537D577419267FAA093836BBA6 (void);
// 0x0000013D System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_mF52BA74121774AC25AD645A6AB43556C2B2BC4FB (void);
// 0x0000013E System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m02F8B326B53D4BB33A54254ED664BA615DAC4E24 (void);
// 0x0000013F System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m06892291F75733C0A0525F44054E4CEA8D9BD58C (void);
// 0x00000140 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m2F4836BAA267C17023363704B71B53454934A6E0 (void);
// 0x00000141 System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_m325723A91F3B6B3541926775C0DA1BD8FA4B75D0 (void);
// 0x00000142 System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_mC7442B0A9CC1A12FDAA128C215A66FDD34EC35F6 (void);
// 0x00000143 UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m8074EF4A6C2BB055960B5AA7DD12FECB81F4DC7E (void);
// 0x00000144 System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_m453D126F0994CE92A6A3D2F620B81BADD85F7DBF (void);
// 0x00000145 System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_mFB18E87CB3089AB1EB0DB1D3AC9C53AB381145E9 (void);
// 0x00000146 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m76DC34194319F4AF8CFB96066F3764402BEB5279 (void);
// 0x00000147 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_mE963A8C458C7424814637CF46BBDDEC46CAD7F9C (void);
// 0x00000148 System.Boolean TMPro.Examples.WarpTextExample/<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_m9924A340CE9833FFA7A34E26A3DD75C5E51AAEDC (void);
// 0x00000149 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m32CF5062AEB89B0ECAE1021AF873CD799923FDFF (void);
// 0x0000014A System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m46782447641AAFD07BA0D6D363B5FA2FC2282837 (void);
// 0x0000014B System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_mDB18401834B9740BFAECF2C624EF0C8ACED4E9D2 (void);
// 0x0000014C Microsoft.MixedReality.Toolkit.Dwell.DwellHandler Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::get_DwellHandler()
extern void BaseDwellSample_get_DwellHandler_mC114D08153104E01605702BD59AF73888F1E6CE9 (void);
// 0x0000014D System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::set_DwellHandler(Microsoft.MixedReality.Toolkit.Dwell.DwellHandler)
extern void BaseDwellSample_set_DwellHandler_m73E008459D501A72BB8C968B6BAF6575D5265348 (void);
// 0x0000014E System.Boolean Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::get_IsDwelling()
extern void BaseDwellSample_get_IsDwelling_m9E0E0CD2600EB5782A736E1C8EAC08773126F873 (void);
// 0x0000014F System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::set_IsDwelling(System.Boolean)
extern void BaseDwellSample_set_IsDwelling_m18391541F4D24E4F07E99244BE58C23997F65E4A (void);
// 0x00000150 System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::Awake()
extern void BaseDwellSample_Awake_mE27137E205C875CAFDC5A68C1DCFBCEE4483CAAA (void);
// 0x00000151 System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::DwellStarted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void BaseDwellSample_DwellStarted_mCF844F3E102A29C2408574C2346787EC05DDFEAB (void);
// 0x00000152 System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::DwellIntended(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void BaseDwellSample_DwellIntended_m2D4A73E52370E2561402606AFC2CC3B8821FC616 (void);
// 0x00000153 System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::DwellCanceled(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void BaseDwellSample_DwellCanceled_mB794C48E5F4A3A3F24875DC788102FCB21288638 (void);
// 0x00000154 System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::DwellCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void BaseDwellSample_DwellCompleted_mBE3F3E2E50D6BA46AD9EEAA37AD8EFEF90381808 (void);
// 0x00000155 System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::ButtonExecute()
extern void BaseDwellSample_ButtonExecute_m7C80090AE4591E2754DC18509EEEBBAAEF091D8D (void);
// 0x00000156 System.Void Microsoft.MixedReality.Toolkit.Dwell.BaseDwellSample::.ctor()
extern void BaseDwellSample__ctor_m3D2F88E08FAA4EB4EA2487CFC539DEF9BB044C35 (void);
// 0x00000157 System.Void Microsoft.MixedReality.Toolkit.Dwell.InstantDwellSample::Update()
extern void InstantDwellSample_Update_m3FF0B0611C4720FFED1F58C1C696301608E3876D (void);
// 0x00000158 System.Void Microsoft.MixedReality.Toolkit.Dwell.InstantDwellSample::DwellCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void InstantDwellSample_DwellCompleted_m06086AEA00F283BFB795003941548DE685FBC90F (void);
// 0x00000159 System.Void Microsoft.MixedReality.Toolkit.Dwell.InstantDwellSample::ButtonExecute()
extern void InstantDwellSample_ButtonExecute_m13603FD4A7B15908271B128D07168F4ED5A4170E (void);
// 0x0000015A System.Void Microsoft.MixedReality.Toolkit.Dwell.InstantDwellSample::.ctor()
extern void InstantDwellSample__ctor_m79634CE899182B8FF0A5C9BF78867B456C3F42BA (void);
// 0x0000015B System.Void Microsoft.MixedReality.Toolkit.Dwell.ListItemDwell::Awake()
extern void ListItemDwell_Awake_mC8F41E088876688E181662329BC1ACFB906945FA (void);
// 0x0000015C System.Void Microsoft.MixedReality.Toolkit.Dwell.ListItemDwell::Update()
extern void ListItemDwell_Update_m6CDBB0AA928BDC45F4D163217DCC50168BDD1270 (void);
// 0x0000015D System.Void Microsoft.MixedReality.Toolkit.Dwell.ListItemDwell::DwellCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void ListItemDwell_DwellCompleted_m1C73E6044630B10E13976CCB2A412FAEDB7796F7 (void);
// 0x0000015E System.Void Microsoft.MixedReality.Toolkit.Dwell.ListItemDwell::ButtonExecute()
extern void ListItemDwell_ButtonExecute_m9BA94FB2DC544F6A023A1B5589F84478371B7F15 (void);
// 0x0000015F System.Void Microsoft.MixedReality.Toolkit.Dwell.ListItemDwell::.ctor()
extern void ListItemDwell__ctor_mB6E8EBEE58A60E459658E00C9C518BA671870399 (void);
// 0x00000160 System.Void Microsoft.MixedReality.Toolkit.Dwell.ToggleDwellSample::Update()
extern void ToggleDwellSample_Update_m36A77F7BD8D577E9DB0E54964BCE3F2C04EA192D (void);
// 0x00000161 System.Void Microsoft.MixedReality.Toolkit.Dwell.ToggleDwellSample::DwellIntended(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void ToggleDwellSample_DwellIntended_m5899E646C78FB7093B888AC63FDB814F3DDE5B94 (void);
// 0x00000162 System.Void Microsoft.MixedReality.Toolkit.Dwell.ToggleDwellSample::DwellStarted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void ToggleDwellSample_DwellStarted_m5003812EFAFF3DAF8C22783769F934CFB9E11F81 (void);
// 0x00000163 System.Void Microsoft.MixedReality.Toolkit.Dwell.ToggleDwellSample::DwellCanceled(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void ToggleDwellSample_DwellCanceled_mD91CF69114E163EED925BEEE4605BF170268CC80 (void);
// 0x00000164 System.Void Microsoft.MixedReality.Toolkit.Dwell.ToggleDwellSample::DwellCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void ToggleDwellSample_DwellCompleted_m74F9402EF0BF3C1B74C7E46624B8DE2077F953BE (void);
// 0x00000165 System.Void Microsoft.MixedReality.Toolkit.Dwell.ToggleDwellSample::ButtonExecute()
extern void ToggleDwellSample_ButtonExecute_m184D3DA722763CA68D6187F114D201B4EB4BDCD2 (void);
// 0x00000166 System.Void Microsoft.MixedReality.Toolkit.Dwell.ToggleDwellSample::.ctor()
extern void ToggleDwellSample__ctor_m57E41F049E3EB0755C07C83CA628209936F66D17 (void);
// 0x00000167 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::Start()
extern void DemoSceneUnderstandingController_Start_m47717E3598089742D2A3303C037A4CB525063692 (void);
// 0x00000168 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::OnEnable()
extern void DemoSceneUnderstandingController_OnEnable_m2F0F9FCDE8E7F215D922B5838C2E8C31E49568E8 (void);
// 0x00000169 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::OnDisable()
extern void DemoSceneUnderstandingController_OnDisable_mA512931FA6F655A8C59DB41F113AA6BF01916CC3 (void);
// 0x0000016A System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::OnDestroy()
extern void DemoSceneUnderstandingController_OnDestroy_mAF8DF2D7CCEC0F1ACA0A8900F403A40EAEC10273 (void);
// 0x0000016B System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::OnObservationAdded(Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessEventData`1<Microsoft.MixedReality.Toolkit.Experimental.SpatialAwareness.SpatialAwarenessSceneObject>)
extern void DemoSceneUnderstandingController_OnObservationAdded_mE5546987FF369BD3DE39B09308A33FF64CF6AD2F (void);
// 0x0000016C System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::OnObservationUpdated(Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessEventData`1<Microsoft.MixedReality.Toolkit.Experimental.SpatialAwareness.SpatialAwarenessSceneObject>)
extern void DemoSceneUnderstandingController_OnObservationUpdated_m1AC359A33DAB5AF353B14BF33605D517DF8EDD53 (void);
// 0x0000016D System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::OnObservationRemoved(Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessEventData`1<Microsoft.MixedReality.Toolkit.Experimental.SpatialAwareness.SpatialAwarenessSceneObject>)
extern void DemoSceneUnderstandingController_OnObservationRemoved_m8E0DE889D799462722E13831CFB3F5CEB65110C8 (void);
// 0x0000016E System.Collections.Generic.IReadOnlyDictionary`2<System.Int32,Microsoft.MixedReality.Toolkit.Experimental.SpatialAwareness.SpatialAwarenessSceneObject> Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::GetSceneObjectsOfType(Microsoft.MixedReality.Toolkit.SpatialAwareness.SpatialAwarenessSurfaceTypes)
extern void DemoSceneUnderstandingController_GetSceneObjectsOfType_m88A1DD6CAD512CE6BDA637399EDB3BF1A67A7836 (void);
// 0x0000016F System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::UpdateScene()
extern void DemoSceneUnderstandingController_UpdateScene_m82E8775487E2296361CA0715AF6B28D6153A541D (void);
// 0x00000170 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::SaveScene()
extern void DemoSceneUnderstandingController_SaveScene_mBE7E5D939D5F07001D58B03ED4B8503144C1178E (void);
// 0x00000171 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ClearScene()
extern void DemoSceneUnderstandingController_ClearScene_m442EAB06F5144B65E1C831158373955730EB77C2 (void);
// 0x00000172 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleAutoUpdate()
extern void DemoSceneUnderstandingController_ToggleAutoUpdate_m00B4186ADC3D4188A36C4DE187864DB65CF1A1CD (void);
// 0x00000173 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleOcclusionMask()
extern void DemoSceneUnderstandingController_ToggleOcclusionMask_m63D49573C306D380B6AAB8F5675D49809D9FEAA4 (void);
// 0x00000174 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleGeneratePlanes()
extern void DemoSceneUnderstandingController_ToggleGeneratePlanes_m22D40456913B713BE25453FBA743FA24CE4DBFE8 (void);
// 0x00000175 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleGenerateMeshes()
extern void DemoSceneUnderstandingController_ToggleGenerateMeshes_m780323EE623313F5E6B6966D83E85322D6CFCD63 (void);
// 0x00000176 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleFloors()
extern void DemoSceneUnderstandingController_ToggleFloors_m73873782469A5D450096695844AE5D8752D53431 (void);
// 0x00000177 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleWalls()
extern void DemoSceneUnderstandingController_ToggleWalls_mC6C9A213B0959E4B571E87EB92764FC285CC472D (void);
// 0x00000178 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleCeilings()
extern void DemoSceneUnderstandingController_ToggleCeilings_m11C31CF1AA7BF247EAA89D5DAD9F96CD7AB915E1 (void);
// 0x00000179 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::TogglePlatforms()
extern void DemoSceneUnderstandingController_TogglePlatforms_m21111095977E0BF1360981FED149CD96036BC122 (void);
// 0x0000017A System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleInferRegions()
extern void DemoSceneUnderstandingController_ToggleInferRegions_m16957597ABD7FC5FAAEB65BB1075FEAD4344DD4A (void);
// 0x0000017B System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleWorld()
extern void DemoSceneUnderstandingController_ToggleWorld_m9F4CFBA323B5CE97A9C59684E604A7562AB9FBE3 (void);
// 0x0000017C System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleBackground()
extern void DemoSceneUnderstandingController_ToggleBackground_m6DFF9BF5F9968E3B7EE5CF312F45A262356408A8 (void);
// 0x0000017D System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleCompletelyInferred()
extern void DemoSceneUnderstandingController_ToggleCompletelyInferred_mDB18B7B05AD25080AB57D732C92317C25246E062 (void);
// 0x0000017E System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::InitToggleButtonState()
extern void DemoSceneUnderstandingController_InitToggleButtonState_m528CFC0969C5496F611C75DA52C751EC7A9B809F (void);
// 0x0000017F UnityEngine.Color Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ColorForSurfaceType(Microsoft.MixedReality.Toolkit.SpatialAwareness.SpatialAwarenessSurfaceTypes)
extern void DemoSceneUnderstandingController_ColorForSurfaceType_mABF77F25C7623F0ADD2790D11B9357AB633BEB53 (void);
// 0x00000180 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ClearAndUpdateObserver()
extern void DemoSceneUnderstandingController_ClearAndUpdateObserver_mC8BB24CAB3243D8204D81A29DE83FD97E5B42645 (void);
// 0x00000181 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::ToggleObservedSurfaceType(Microsoft.MixedReality.Toolkit.SpatialAwareness.SpatialAwarenessSurfaceTypes)
extern void DemoSceneUnderstandingController_ToggleObservedSurfaceType_m87284E1E5156CB0AD80FBFF64C465F014E18A62C (void);
// 0x00000182 System.Void Microsoft.MixedReality.Toolkit.Experimental.SceneUnderstanding.DemoSceneUnderstandingController::.ctor()
extern void DemoSceneUnderstandingController__ctor_mBB76E0DE347FA8439A529450F0ED4ED8345DD9A1 (void);
// 0x00000183 System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.KeyboardTest::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void KeyboardTest_OnPointerDown_mF036F7DDA6676AB249BFBC783747009FF697CFED (void);
// 0x00000184 System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.KeyboardTest::UpdateText(System.String)
extern void KeyboardTest_UpdateText_m0F5DF7FECD3B4B5A56CF3B47086A8A1454D7B00A (void);
// 0x00000185 System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.KeyboardTest::DisableKeyboard(System.Object,System.EventArgs)
extern void KeyboardTest_DisableKeyboard_m822B6E839D2F082C3CB4983EBC629954BB783AF9 (void);
// 0x00000186 System.Void Microsoft.MixedReality.Toolkit.Experimental.UI.KeyboardTest::.ctor()
extern void KeyboardTest__ctor_m8288D21CD0D276005CEEE13B5BFDB9826E4F3B18 (void);
// 0x00000187 System.Void Microsoft.MixedReality.Toolkit.Experimental.Joystick.JoystickSliders::Start()
extern void JoystickSliders_Start_mA36CFC6517537A051702C8649AF92F3BAF9EB388 (void);
// 0x00000188 System.Void Microsoft.MixedReality.Toolkit.Experimental.Joystick.JoystickSliders::CalculateValues()
extern void JoystickSliders_CalculateValues_m4D43C5C1E07AF2E53801576CFE7B83FF04C0DEC5 (void);
// 0x00000189 System.Void Microsoft.MixedReality.Toolkit.Experimental.Joystick.JoystickSliders::UpdateSliderValues()
extern void JoystickSliders_UpdateSliderValues_m92223F35F88652EE85F6237C499F66C0F7555FA5 (void);
// 0x0000018A System.Void Microsoft.MixedReality.Toolkit.Experimental.Joystick.JoystickSliders::.ctor()
extern void JoystickSliders__ctor_m2D5ADB1CB0E2631433A5BCDF8F24B3E3C910C0D6 (void);
// 0x0000018B System.Void Microsoft.MixedReality.Toolkit.Experimental.SurfacePulse.HideOnDevice::Start()
extern void HideOnDevice_Start_m6C50EC47A90EF24A222FFE59BBD7461595DCA43D (void);
// 0x0000018C System.Void Microsoft.MixedReality.Toolkit.Experimental.SurfacePulse.HideOnDevice::.ctor()
extern void HideOnDevice__ctor_mB4CC4F8072A0F6C554869AA0DBF01528D8B6304E (void);
// 0x0000018D System.Void Microsoft.MixedReality.Toolkit.Examples.MicrophoneAmplitudeDemo::.ctor()
extern void MicrophoneAmplitudeDemo__ctor_m3EE4EB4A3BD42EF42ED9AD76D5A005B56B1FA05A (void);
// 0x0000018E System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::OnEnable()
extern void GestureTester_OnEnable_m70A8A8C20E61CD1F0B3DB652ACF5A56C592CFCC4 (void);
// 0x0000018F System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::OnGestureStarted(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void GestureTester_OnGestureStarted_mEA2FEEBF17350DC96CF4F233AF706AA6C632DFEA (void);
// 0x00000190 System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::OnGestureUpdated(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void GestureTester_OnGestureUpdated_m72980D81524BE33F7E88230CE656CFDBD5260B1A (void);
// 0x00000191 System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::OnGestureUpdated(Microsoft.MixedReality.Toolkit.Input.InputEventData`1<UnityEngine.Vector3>)
extern void GestureTester_OnGestureUpdated_m1B5DF61098C445900D8710EF2D6CC651888F338E (void);
// 0x00000192 System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::OnGestureCompleted(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void GestureTester_OnGestureCompleted_mB119D5D0E643291D2567D851920FAE6D0944CDD9 (void);
// 0x00000193 System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::OnGestureCompleted(Microsoft.MixedReality.Toolkit.Input.InputEventData`1<UnityEngine.Vector3>)
extern void GestureTester_OnGestureCompleted_mCA726ED8A8C365B25BFE7B4F2ED948BECF7AFCE4 (void);
// 0x00000194 System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::OnGestureCanceled(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void GestureTester_OnGestureCanceled_m0986BE7C254B0A54A1AA092F1EC2BFF0A63D1AFE (void);
// 0x00000195 System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::SetIndicator(UnityEngine.GameObject,System.String,UnityEngine.Material)
extern void GestureTester_SetIndicator_m590B0880833C15AFBF23801A9BD557332061A1EA (void);
// 0x00000196 System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::SetIndicator(UnityEngine.GameObject,System.String,UnityEngine.Material,UnityEngine.Vector3)
extern void GestureTester_SetIndicator_m5AF23BBC0F813D44B7A1A41E25D5A24600408986 (void);
// 0x00000197 System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::ShowRails(UnityEngine.Vector3)
extern void GestureTester_ShowRails_m4AB34B32BF07DD03126AA9CCC450DD0AE27C63D1 (void);
// 0x00000198 System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::HideRails()
extern void GestureTester_HideRails_mE2211F37C515D7DFCA11F36A21BCF4FD0CAFE52E (void);
// 0x00000199 System.Void Microsoft.MixedReality.Toolkit.Examples.GestureTester::.ctor()
extern void GestureTester__ctor_m4624CFEF2F475213497E12EA328D0A9BA8A2C8F9 (void);
// 0x0000019A System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::Awake()
extern void GrabTouchExample_Awake_mD6D8B580364828993664012512795E13B4CA1306 (void);
// 0x0000019B System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::OnInputDown(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void GrabTouchExample_OnInputDown_mD8EC039D758E604EE91BC758CF03F2C22B8D7599 (void);
// 0x0000019C System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::OnInputUp(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void GrabTouchExample_OnInputUp_m1DA424062780EA4257361641D057A8E2B5E164A4 (void);
// 0x0000019D System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::OnInputPressed(Microsoft.MixedReality.Toolkit.Input.InputEventData`1<System.Single>)
extern void GrabTouchExample_OnInputPressed_m68784165F4E0C9DA9B2E950068DCFE29A5809040 (void);
// 0x0000019E System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::OnPositionInputChanged(Microsoft.MixedReality.Toolkit.Input.InputEventData`1<UnityEngine.Vector2>)
extern void GrabTouchExample_OnPositionInputChanged_m0B2E9346D0FB09C7A4CC8F425BF1CF8884A6EE7F (void);
// 0x0000019F System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::OnTouchCompleted(Microsoft.MixedReality.Toolkit.Input.HandTrackingInputEventData)
extern void GrabTouchExample_OnTouchCompleted_mB7634D7E820DEC8E06E5FC2B9B06416E12ADCC9B (void);
// 0x000001A0 System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::OnTouchStarted(Microsoft.MixedReality.Toolkit.Input.HandTrackingInputEventData)
extern void GrabTouchExample_OnTouchStarted_mCED80CE8B31029F9EC4B42834602843FBE2AF110 (void);
// 0x000001A1 System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::OnTouchUpdated(Microsoft.MixedReality.Toolkit.Input.HandTrackingInputEventData)
extern void GrabTouchExample_OnTouchUpdated_mE53F76B9DD63FE228CD8B7EB76E20D116F9657A5 (void);
// 0x000001A2 System.Void Microsoft.MixedReality.Toolkit.Examples.GrabTouchExample::.ctor()
extern void GrabTouchExample__ctor_m1451CE02830A4C64F08BAFD558E49332C2C9F67F (void);
// 0x000001A3 System.Void Microsoft.MixedReality.Toolkit.Examples.LeapMotionOrientationDisplay::.ctor()
extern void LeapMotionOrientationDisplay__ctor_m5D700FA5C8C430D95A4CA82250D5B54505CD7C3D (void);
// 0x000001A4 System.Void Microsoft.MixedReality.Toolkit.Examples.RotateWithPan::OnEnable()
extern void RotateWithPan_OnEnable_m237C9A936C5CFF0AC9A3055E7614A5E1428563C7 (void);
// 0x000001A5 System.Void Microsoft.MixedReality.Toolkit.Examples.RotateWithPan::OnDisable()
extern void RotateWithPan_OnDisable_m3A88289CE6C11FB1AF4200183F6CC10178767D06 (void);
// 0x000001A6 System.Void Microsoft.MixedReality.Toolkit.Examples.RotateWithPan::OnPanEnded(Microsoft.MixedReality.Toolkit.UI.HandPanEventData)
extern void RotateWithPan_OnPanEnded_mE96F5C8E146A27275B4A9A54251704E88CF87483 (void);
// 0x000001A7 System.Void Microsoft.MixedReality.Toolkit.Examples.RotateWithPan::OnPanning(Microsoft.MixedReality.Toolkit.UI.HandPanEventData)
extern void RotateWithPan_OnPanning_m3D1251B46F6B6D975053A3F506E12382AAC9BC65 (void);
// 0x000001A8 System.Void Microsoft.MixedReality.Toolkit.Examples.RotateWithPan::OnPanStarted(Microsoft.MixedReality.Toolkit.UI.HandPanEventData)
extern void RotateWithPan_OnPanStarted_m045EE3892171374450C66B309F65269B7747B58E (void);
// 0x000001A9 System.Void Microsoft.MixedReality.Toolkit.Examples.RotateWithPan::.ctor()
extern void RotateWithPan__ctor_m2487B6068019BED02B78A2F15524EBF85EE825C7 (void);
// 0x000001AA System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo::Start()
extern void WidgetElasticDemo_Start_m314B8AB55DFA1DE50A0445BF59AB65FC650464FE (void);
// 0x000001AB System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo::Update()
extern void WidgetElasticDemo_Update_m16A4BF507722AA9F79C585D4F3E3426D95607B31 (void);
// 0x000001AC System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo::ToggleInflate()
extern void WidgetElasticDemo_ToggleInflate_mE2836D801C0C5B63F60CD3D3B7161476687E2F4D (void);
// 0x000001AD System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo::DeflateCoroutine()
extern void WidgetElasticDemo_DeflateCoroutine_mAFCF1FE1810464E142CA6140D747438EE7CE4BC7 (void);
// 0x000001AE System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo::InflateCoroutine()
extern void WidgetElasticDemo_InflateCoroutine_m19FE65F593F306036730601D94D30F2307A02EAD (void);
// 0x000001AF System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo::.ctor()
extern void WidgetElasticDemo__ctor_m5CD27BC1B0AB35AA62F6ECFFD77B2DA74C9ADF8F (void);
// 0x000001B0 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo::.cctor()
extern void WidgetElasticDemo__cctor_mB90C7AAF82DFF50FDE07EBD8FAB2F715A97A9B94 (void);
// 0x000001B1 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<DeflateCoroutine>d__17::.ctor(System.Int32)
extern void U3CDeflateCoroutineU3Ed__17__ctor_mCE59BFC0F867BA58A8EBFA0509AE6353BCAFBF40 (void);
// 0x000001B2 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<DeflateCoroutine>d__17::System.IDisposable.Dispose()
extern void U3CDeflateCoroutineU3Ed__17_System_IDisposable_Dispose_mFF3E272F263C9873B5972D43624F29AE8917EF6E (void);
// 0x000001B3 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<DeflateCoroutine>d__17::MoveNext()
extern void U3CDeflateCoroutineU3Ed__17_MoveNext_m7A116DFA353531D5333977EA13EE2D7E660E0F76 (void);
// 0x000001B4 System.Object Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<DeflateCoroutine>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDeflateCoroutineU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD3D31C0D3D48BF83F1024764C3BFBEEE1504B054 (void);
// 0x000001B5 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<DeflateCoroutine>d__17::System.Collections.IEnumerator.Reset()
extern void U3CDeflateCoroutineU3Ed__17_System_Collections_IEnumerator_Reset_m6432C3B69E4C3EC48FD04166F910F93750C361C9 (void);
// 0x000001B6 System.Object Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<DeflateCoroutine>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CDeflateCoroutineU3Ed__17_System_Collections_IEnumerator_get_Current_m85DEF7CC0E93830CB2218DB8E323FFDCA2B572A9 (void);
// 0x000001B7 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<InflateCoroutine>d__18::.ctor(System.Int32)
extern void U3CInflateCoroutineU3Ed__18__ctor_m004C3F0C3E72B5B25663F8B4AC86AB8570EBDE07 (void);
// 0x000001B8 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<InflateCoroutine>d__18::System.IDisposable.Dispose()
extern void U3CInflateCoroutineU3Ed__18_System_IDisposable_Dispose_m175FA6717CE8675FEC96F60413B3A08ED6C9B056 (void);
// 0x000001B9 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<InflateCoroutine>d__18::MoveNext()
extern void U3CInflateCoroutineU3Ed__18_MoveNext_m4A59DFFAB5053C3806D78559F2D329ABBA015A96 (void);
// 0x000001BA System.Object Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<InflateCoroutine>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInflateCoroutineU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m440D830708196E0CE60BEC34E2B1F28D3CD40A2E (void);
// 0x000001BB System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<InflateCoroutine>d__18::System.Collections.IEnumerator.Reset()
extern void U3CInflateCoroutineU3Ed__18_System_Collections_IEnumerator_Reset_mB20C406EC02C71E87A765F0EC617CE421FA3FB5E (void);
// 0x000001BC System.Object Microsoft.MixedReality.Toolkit.Examples.Experimental.Demos.WidgetElasticDemo/<InflateCoroutine>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CInflateCoroutineU3Ed__18_System_Collections_IEnumerator_get_Current_m5C619A4BDCB54466183141A0CD0E43E4BD23A4C7 (void);
// 0x000001BD UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::get_DialogPrefabLarge()
extern void DialogExampleController_get_DialogPrefabLarge_m34AE8F7E66144C9824792EC401A1621FB412297D (void);
// 0x000001BE System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::set_DialogPrefabLarge(UnityEngine.GameObject)
extern void DialogExampleController_set_DialogPrefabLarge_m6C587A8C53DC767187272DACBF711BA35B9D10DC (void);
// 0x000001BF UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::get_DialogPrefabMedium()
extern void DialogExampleController_get_DialogPrefabMedium_m0B43AC5C0F4FF95D34F047A2F98F7309B51CA748 (void);
// 0x000001C0 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::set_DialogPrefabMedium(UnityEngine.GameObject)
extern void DialogExampleController_set_DialogPrefabMedium_m63BD2071CF0612353A337395F8A09A78188DB6F6 (void);
// 0x000001C1 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::get_DialogPrefabSmall()
extern void DialogExampleController_get_DialogPrefabSmall_m5D4B294AEEE5001FEF88A15BBC4246E5CD5CBC47 (void);
// 0x000001C2 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::set_DialogPrefabSmall(UnityEngine.GameObject)
extern void DialogExampleController_set_DialogPrefabSmall_m2D5C23ED437D48A51F2045E5F9C3FEC8429791BB (void);
// 0x000001C3 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::OpenConfirmationDialogLarge()
extern void DialogExampleController_OpenConfirmationDialogLarge_m6DC6B10708AC3238CCCE2DDB16C01B12FEF2CDAC (void);
// 0x000001C4 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::OpenChoiceDialogLarge()
extern void DialogExampleController_OpenChoiceDialogLarge_mDA051D336A3E4ED0C71CBC9D5CC352A02AF22F75 (void);
// 0x000001C5 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::OpenConfirmationDialogMedium()
extern void DialogExampleController_OpenConfirmationDialogMedium_mC771E82CD3278A01193C610A684835D9B4DD0142 (void);
// 0x000001C6 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::OpenChoiceDialogMedium()
extern void DialogExampleController_OpenChoiceDialogMedium_m8E911DF0D82D85AD7992FF2C31EE09EDB6582FDE (void);
// 0x000001C7 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::OpenConfirmationDialogSmall()
extern void DialogExampleController_OpenConfirmationDialogSmall_mF03B1B2FC2E8D99AA81181FB2BA8651D72676EDA (void);
// 0x000001C8 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::OpenChoiceDialogSmall()
extern void DialogExampleController_OpenChoiceDialogSmall_m54DE0690D68641FE03DD92BF380C31F19E5B7701 (void);
// 0x000001C9 System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::OnClosedDialogEvent(Microsoft.MixedReality.Toolkit.UI.DialogResult)
extern void DialogExampleController_OnClosedDialogEvent_mD28735E27B58D1D5730676DB0C773A9FD5931568 (void);
// 0x000001CA System.Void Microsoft.MixedReality.Toolkit.Examples.Experimental.DialogTest.DialogExampleController::.ctor()
extern void DialogExampleController__ctor_m978AEB93463870F1A685D6B1B69EA525E229224B (void);
// 0x000001CB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.LoFiFilterSelection::Start()
extern void LoFiFilterSelection_Start_mC296F036389757EF5A1E70F29EC83E084B1FB6B2 (void);
// 0x000001CC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.LoFiFilterSelection::OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void LoFiFilterSelection_OnPointerClicked_m7A080587E792B207736264CB6D3C73E18047C945 (void);
// 0x000001CD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.LoFiFilterSelection::OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void LoFiFilterSelection_OnPointerDown_mC21EBB996B950C647527890378F8A3D6A6B2E6E9 (void);
// 0x000001CE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.LoFiFilterSelection::OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void LoFiFilterSelection_OnPointerDragged_m785FD8AE632CE5B504BC6C0486F07CE1E29B7C7A (void);
// 0x000001CF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.LoFiFilterSelection::OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void LoFiFilterSelection_OnPointerUp_m47753925E6E1B2B6B203C876934917DC4DA52510 (void);
// 0x000001D0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.LoFiFilterSelection::SetEmitterMaterial(Microsoft.MixedReality.Toolkit.Audio.AudioLoFiSourceQuality)
extern void LoFiFilterSelection_SetEmitterMaterial_mD45B1B223FDCFA883F4A5BE0F3176EEA75ECCEFD (void);
// 0x000001D1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.LoFiFilterSelection::.ctor()
extern void LoFiFilterSelection__ctor_m1115BDF686DA47D84F353E1B2BEB8770E9578710 (void);
// 0x000001D2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.TextToSpeechSample::Awake()
extern void TextToSpeechSample_Awake_mE9F5EDD43360B507E57BF929B38AAD4E5F916DBB (void);
// 0x000001D3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.TextToSpeechSample::Speak()
extern void TextToSpeechSample_Speak_m0FD5993C1E29BE25527926906CC116D1978B966E (void);
// 0x000001D4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.TextToSpeechSample::.ctor()
extern void TextToSpeechSample__ctor_m85245EB56EEEAF6619326EBEC40A716B9CC98FBE (void);
// 0x000001D5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::Awake()
extern void BoundaryVisualizationDemo_Awake_m4F9E66F17AA7769A12B5A011320F83CACE40EA3A (void);
// 0x000001D6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::Start()
extern void BoundaryVisualizationDemo_Start_m2D0CC7B0759402E61B2349374122BB6392A983FE (void);
// 0x000001D7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::Update()
extern void BoundaryVisualizationDemo_Update_m904677D86EC9005DA52A88FAFF0C1EFFD8E0E5A5 (void);
// 0x000001D8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::OnEnable()
extern void BoundaryVisualizationDemo_OnEnable_m773B8029FA40F4917E84E7080D104401A69E7F84 (void);
// 0x000001D9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::OnDisable()
extern void BoundaryVisualizationDemo_OnDisable_m8B23D1E44FB9DA230FA1D6188420221A38BABEFE (void);
// 0x000001DA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::OnBoundaryVisualizationChanged(Microsoft.MixedReality.Toolkit.Boundary.BoundaryEventData)
extern void BoundaryVisualizationDemo_OnBoundaryVisualizationChanged_m0227D50F7AF977B57C03E836DD181804EC22ABA0 (void);
// 0x000001DB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::AddMarkers()
extern void BoundaryVisualizationDemo_AddMarkers_mD61AD96C12C75D64C759CEA5235BAC8F2D1E256A (void);
// 0x000001DC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundaryVisualizationDemo::.ctor()
extern void BoundaryVisualizationDemo__ctor_m96D750966F7F44684DD80FCC2482C8CDDBB20528 (void);
// 0x000001DD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DebugTextOutput::SetTextWithTimestamp(System.String)
extern void DebugTextOutput_SetTextWithTimestamp_m6BCE9753AFEE36E4A0BE99D638CE0F77E7CA413E (void);
// 0x000001DE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DebugTextOutput::.ctor()
extern void DebugTextOutput__ctor_m3BB983214A1010AE8A008A5F1EF3249EB1AD6D2E (void);
// 0x000001DF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DemoTouchButton::Awake()
extern void DemoTouchButton_Awake_mC4A4CAF46F371265CC2DB871D929412FDA01659B (void);
// 0x000001E0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DemoTouchButton::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_mE924C72CC220C6CABADD39BB13D13EF4A3243591 (void);
// 0x000001E1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DemoTouchButton::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m40EB4A51F87E932C9598D4355AA1DF242A322CCC (void);
// 0x000001E2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DemoTouchButton::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m209B708E4EFE0BFD43B64BB968031509279B0E65 (void);
// 0x000001E3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DemoTouchButton::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m72B985EBF82D6EE9B52CD6202E970D61FF9E8BE6 (void);
// 0x000001E4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DemoTouchButton::.ctor()
extern void DemoTouchButton__ctor_m8BCD15F7E056F727CEF502B2EA5FC591506CBAB0 (void);
// 0x000001E5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HandInteractionTouch::Start()
extern void HandInteractionTouch_Start_mD3BA08B4A89D4926A723C393A9C70D500490DF19 (void);
// 0x000001E6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HandInteractionTouch::Microsoft.MixedReality.Toolkit.Input.IMixedRealityTouchHandler.OnTouchCompleted(Microsoft.MixedReality.Toolkit.Input.HandTrackingInputEventData)
extern void HandInteractionTouch_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchCompleted_m9C0206D9EDD306EF541ACDDFE82576472B7AC68F (void);
// 0x000001E7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HandInteractionTouch::Microsoft.MixedReality.Toolkit.Input.IMixedRealityTouchHandler.OnTouchStarted(Microsoft.MixedReality.Toolkit.Input.HandTrackingInputEventData)
extern void HandInteractionTouch_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchStarted_mB3EDEED3985D372DDDB4864C652379E3B6DC33A5 (void);
// 0x000001E8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HandInteractionTouch::Microsoft.MixedReality.Toolkit.Input.IMixedRealityTouchHandler.OnTouchUpdated(Microsoft.MixedReality.Toolkit.Input.HandTrackingInputEventData)
extern void HandInteractionTouch_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchUpdated_mE1FB4240389F33FF0914843E9DE4C0356F74DC70 (void);
// 0x000001E9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HandInteractionTouch::.ctor()
extern void HandInteractionTouch__ctor_m14E2F5CC195D4A829CC784360478A5E734F54592 (void);
// 0x000001EA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HandInteractionTouchRotate::Microsoft.MixedReality.Toolkit.Input.IMixedRealityTouchHandler.OnTouchUpdated(Microsoft.MixedReality.Toolkit.Input.HandTrackingInputEventData)
extern void HandInteractionTouchRotate_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchUpdated_m6678F97F9406CF49BABD56674B968BBBBBF89F2E (void);
// 0x000001EB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HandInteractionTouchRotate::.ctor()
extern void HandInteractionTouchRotate__ctor_m4D89B4BFFC3F017788F454C4BB76A0554F7C4497 (void);
// 0x000001EC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.LaunchUri::Launch(System.String)
extern void LaunchUri_Launch_m3C25F44EF5B182B3940F50332E1592321FFCE568 (void);
// 0x000001ED System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.LaunchUri::.ctor()
extern void LaunchUri__ctor_mC549649011B7E99566FC263F88565A2A81AEB357 (void);
// 0x000001EE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.LeapCoreAssetsDetector::Start()
extern void LeapCoreAssetsDetector_Start_m013F637E718EC68FB1CFBB905B7A81208393688C (void);
// 0x000001EF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.LeapCoreAssetsDetector::.ctor()
extern void LeapCoreAssetsDetector__ctor_mD7FD61BEA9C55A6F7D254AC9590757E84AC06DCE (void);
// 0x000001F0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverTrackedTargetType::ChangeTrackedTargetTypeHead()
extern void SolverTrackedTargetType_ChangeTrackedTargetTypeHead_mFC6151E789470A07F74819B3E5ED5332ACD01415 (void);
// 0x000001F1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverTrackedTargetType::ChangeTrackedTargetTypeHandJoint()
extern void SolverTrackedTargetType_ChangeTrackedTargetTypeHandJoint_mB2141D91F91DBF5ABAB7FB936F5406CAA85E0C12 (void);
// 0x000001F2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverTrackedTargetType::.ctor()
extern void SolverTrackedTargetType__ctor_m9BC23444B1D1B785648CBB2ABDC0CDF1F38C568D (void);
// 0x000001F3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SystemKeyboardExample::OpenSystemKeyboard()
extern void SystemKeyboardExample_OpenSystemKeyboard_mD6EEEA5E18B625B476003EDB032FB0C680B5123E (void);
// 0x000001F4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SystemKeyboardExample::Start()
extern void SystemKeyboardExample_Start_m4C9FC22515C45198FB7F13876C65A3B0E463DDF1 (void);
// 0x000001F5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SystemKeyboardExample::Update()
extern void SystemKeyboardExample_Update_mD5BDA8992F7826C6F639E360FE37551ACEFC8F63 (void);
// 0x000001F6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SystemKeyboardExample::.ctor()
extern void SystemKeyboardExample__ctor_mF14201EABFE1DB91BC803ABC424450E0817CC8C1 (void);
// 0x000001F7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SystemKeyboardExample::<Start>b__5_0()
extern void SystemKeyboardExample_U3CStartU3Eb__5_0_m541CA735B73D753F9564EC8F92050CD1194DEE63 (void);
// 0x000001F8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SystemKeyboardExample::<Start>b__5_1()
extern void SystemKeyboardExample_U3CStartU3Eb__5_1_mD9B2785A96F31F655646DC94D36E0F22330A5CFD (void);
// 0x000001F9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.TetheredPlacement::Start()
extern void TetheredPlacement_Start_mCAEDECC31F526157956588C68E52E9032CB83D18 (void);
// 0x000001FA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.TetheredPlacement::LateUpdate()
extern void TetheredPlacement_LateUpdate_m54F132D958420609D9C938E78A409357FEB3AC6A (void);
// 0x000001FB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.TetheredPlacement::LockSpawnPoint()
extern void TetheredPlacement_LockSpawnPoint_m5540BAAEB8FD8FC6B463A5F1F023B01A1AE023D0 (void);
// 0x000001FC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.TetheredPlacement::.ctor()
extern void TetheredPlacement__ctor_m75DB8161CD3FB3E533E26567BCE022070B3AA8AC (void);
// 0x000001FD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ToggleBoundingBox::Awake()
extern void ToggleBoundingBox_Awake_mA129C2E367448FE8EC5A0FE8DD286CF8DA2EE359 (void);
// 0x000001FE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ToggleBoundingBox::ToggleBoundingBoxActiveState()
extern void ToggleBoundingBox_ToggleBoundingBoxActiveState_mA664325D9D2DD707DC4B7B532E53E3A3C550D391 (void);
// 0x000001FF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ToggleBoundingBox::.ctor()
extern void ToggleBoundingBox__ctor_m7C58483B241D2B98FEF236F26F249B9CE0B2273F (void);
// 0x00000200 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DisablePointersExample::Start()
extern void DisablePointersExample_Start_mD74D037DE47A0DEF5B7D39132AA0F639427EDE19 (void);
// 0x00000201 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DisablePointersExample::ResetExample()
extern void DisablePointersExample_ResetExample_m7C65AFBF9FC92B11F9803EDC966641F08CE785EF (void);
// 0x00000202 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DisablePointersExample::Update()
extern void DisablePointersExample_Update_m5500DD608FF437AAC90BF817FA48C7B664A92017 (void);
// 0x00000203 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DisablePointersExample::SetToggleHelper(Microsoft.MixedReality.Toolkit.UI.Interactable,System.String,Microsoft.MixedReality.Toolkit.Input.InputSourceType)
// 0x00000204 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.DisablePointersExample::.ctor()
extern void DisablePointersExample__ctor_m3D417578DC2C84B4642E26CF713892E6493FE906 (void);
// 0x00000205 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.Rotator::Rotate()
extern void Rotator_Rotate_mBC6459FDCB67429C5657346A5411C26C7C46B482 (void);
// 0x00000206 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.Rotator::.ctor()
extern void Rotator__ctor_mB353144F2DD4A3D25F2D2B788DAD0F186FE9F391 (void);
// 0x00000207 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.InputDataExample::Update()
extern void InputDataExample_Update_m1FB3ED5AE43DDEA37EF80D7DCBD35B743AAA4D60 (void);
// 0x00000208 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.InputDataExample::Start()
extern void InputDataExample_Start_m0295CFD17E51CDB956A958AC72D69650BF8AB8BA (void);
// 0x00000209 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.InputDataExample::.ctor()
extern void InputDataExample__ctor_m974174F68A7FA082CD73D95EE0286FFCBAA03627 (void);
// 0x0000020A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.InputDataExampleGizmo::SetIsDataAvailable(System.Boolean)
extern void InputDataExampleGizmo_SetIsDataAvailable_m5C834CA6E6E32AABBFE1F81D4D774D140B5655EC (void);
// 0x0000020B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.InputDataExampleGizmo::Update()
extern void InputDataExampleGizmo_Update_m8ADCA594907A724BE844805845806EA80F005683 (void);
// 0x0000020C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.InputDataExampleGizmo::.ctor()
extern void InputDataExampleGizmo__ctor_m7FB7A964A1B920B51E0F27DBCC87071518CB670F (void);
// 0x0000020D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SpawnOnPointerEvent::Spawn(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void SpawnOnPointerEvent_Spawn_m2F5717311C18915A9E446386064EF5F4595E17B9 (void);
// 0x0000020E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SpawnOnPointerEvent::.ctor()
extern void SpawnOnPointerEvent__ctor_m3D2D3C713CEAABF6BE5B6DB0A60F89FAD6838F8C (void);
// 0x0000020F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.PrimaryPointerHandlerExample::OnEnable()
extern void PrimaryPointerHandlerExample_OnEnable_m9BA8EB141907B2AA21211D2E25BFC570B55E7F97 (void);
// 0x00000210 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.PrimaryPointerHandlerExample::OnPrimaryPointerChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void PrimaryPointerHandlerExample_OnPrimaryPointerChanged_mFEBF3000D8558060AF0D4D11D84F1D9D6DFEFAD3 (void);
// 0x00000211 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.PrimaryPointerHandlerExample::OnDisable()
extern void PrimaryPointerHandlerExample_OnDisable_mB1B7794497285DFF164CCD179BB19919DEC5B0C1 (void);
// 0x00000212 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.PrimaryPointerHandlerExample::.ctor()
extern void PrimaryPointerHandlerExample__ctor_m94B57931B01591601E84A05F76CFC4E120989D63 (void);
// 0x00000213 Microsoft.MixedReality.Toolkit.UI.ScrollingObjectCollection Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::get_ScrollView()
extern void ScrollableListPopulator_get_ScrollView_m8E06C23AD8D2D7C6693B3CDEF554A4D9A6B8C769 (void);
// 0x00000214 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::set_ScrollView(Microsoft.MixedReality.Toolkit.UI.ScrollingObjectCollection)
extern void ScrollableListPopulator_set_ScrollView_m60E4E8436AFF962E9C0C4A4C72ABB96CD4450670 (void);
// 0x00000215 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::get_DynamicItem()
extern void ScrollableListPopulator_get_DynamicItem_m8241F91467F6F9854FA304423B73F787D877D150 (void);
// 0x00000216 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::set_DynamicItem(UnityEngine.GameObject)
extern void ScrollableListPopulator_set_DynamicItem_m15D67C8E22310BA648BF229B0858ECEED02EE938 (void);
// 0x00000217 System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::get_NumItems()
extern void ScrollableListPopulator_get_NumItems_mAC07553EE72902DF560139F31B5A3C72B76C083F (void);
// 0x00000218 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::set_NumItems(System.Int32)
extern void ScrollableListPopulator_set_NumItems_m2D9E9659DABC3410E12A2959DF8E2C4AF9F2EB50 (void);
// 0x00000219 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::get_LazyLoad()
extern void ScrollableListPopulator_get_LazyLoad_mA87F32E1AAA6622CEC54AFD6486213A067358F90 (void);
// 0x0000021A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::set_LazyLoad(System.Boolean)
extern void ScrollableListPopulator_set_LazyLoad_mE1C013DD9E2A6A349421E5E39D478C17BF7FEA6B (void);
// 0x0000021B System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::get_ItemsPerFrame()
extern void ScrollableListPopulator_get_ItemsPerFrame_m408964D16CD670391BA9F924DA6FA16EEEE873E9 (void);
// 0x0000021C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::set_ItemsPerFrame(System.Int32)
extern void ScrollableListPopulator_set_ItemsPerFrame_m3B9A824F083EA0B6A713B64C6E0B5C90740AF516 (void);
// 0x0000021D UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::get_Loader()
extern void ScrollableListPopulator_get_Loader_mDDF2762A1EC96D0E019BA2C370274CD65DB1562D (void);
// 0x0000021E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::set_Loader(UnityEngine.GameObject)
extern void ScrollableListPopulator_set_Loader_mC8501A2994B604A72CA61709431548D2312AF067 (void);
// 0x0000021F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::OnEnable()
extern void ScrollableListPopulator_OnEnable_mD0DF2213A5E104795B7F4C12C4A1F41B461DE93C (void);
// 0x00000220 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::MakeScrollingList()
extern void ScrollableListPopulator_MakeScrollingList_m97AEB71FA5A62D40ED672774BF81748546538C92 (void);
// 0x00000221 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::UpdateListOverTime(UnityEngine.GameObject,System.Int32)
extern void ScrollableListPopulator_UpdateListOverTime_m9C3F25617584DDF0E49A4E5FE3F0434FC554F228 (void);
// 0x00000222 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::MakeItem(UnityEngine.GameObject)
extern void ScrollableListPopulator_MakeItem_m55C5C48C22A2BDFDD1D1E5A3B744F07DF590BAC2 (void);
// 0x00000223 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator::.ctor()
extern void ScrollableListPopulator__ctor_m2C0E31FADE5A160801D3388D0716DCCD94B25EA1 (void);
// 0x00000224 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator/<UpdateListOverTime>d__33::.ctor(System.Int32)
extern void U3CUpdateListOverTimeU3Ed__33__ctor_m6EEEA2C4C3863DE786A4FB33D9FCD7DCBB83AC7A (void);
// 0x00000225 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator/<UpdateListOverTime>d__33::System.IDisposable.Dispose()
extern void U3CUpdateListOverTimeU3Ed__33_System_IDisposable_Dispose_mC45591ACB830F32EADCEB7990E91CE8347678FE7 (void);
// 0x00000226 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator/<UpdateListOverTime>d__33::MoveNext()
extern void U3CUpdateListOverTimeU3Ed__33_MoveNext_m68C215F85564411F4E7DD98408B0D5BC0894316E (void);
// 0x00000227 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator/<UpdateListOverTime>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateListOverTimeU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE4E49E62B8DCC9D5A741F6E62D8B43615027F87C (void);
// 0x00000228 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator/<UpdateListOverTime>d__33::System.Collections.IEnumerator.Reset()
extern void U3CUpdateListOverTimeU3Ed__33_System_Collections_IEnumerator_Reset_m9839718D6F2442EB8A25E4942DA9C744A69674F0 (void);
// 0x00000229 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollableListPopulator/<UpdateListOverTime>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateListOverTimeU3Ed__33_System_Collections_IEnumerator_get_Current_m9CA3175850108F9ED839FB1B592FF078FAD74C6B (void);
// 0x0000022A Microsoft.MixedReality.Toolkit.UI.ScrollingObjectCollection Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollablePagination::get_ScrollView()
extern void ScrollablePagination_get_ScrollView_m97F66B6BDF2A9AE2A10BE2E570F848CBEBC56B32 (void);
// 0x0000022B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollablePagination::set_ScrollView(Microsoft.MixedReality.Toolkit.UI.ScrollingObjectCollection)
extern void ScrollablePagination_set_ScrollView_mF5CA58C99567FEBA92DD13DDB6C3434F3FD10968 (void);
// 0x0000022C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollablePagination::ScrollByTier(System.Int32)
extern void ScrollablePagination_ScrollByTier_mE29EA35FE680FB74723ED5CF662C33FEAD5550FB (void);
// 0x0000022D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ScrollablePagination::.ctor()
extern void ScrollablePagination__ctor_m541C254B920CA9B1CF2C85B89EA575C86F24051B (void);
// 0x0000022E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HideTapToPlaceLabel::Start()
extern void HideTapToPlaceLabel_Start_m7E79A585524689DCCE57509E37C6A80867FF2320 (void);
// 0x0000022F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HideTapToPlaceLabel::AddTapToPlaceListeners()
extern void HideTapToPlaceLabel_AddTapToPlaceListeners_m179AD9312535108DABC7A83B897317BAF95F6FDF (void);
// 0x00000230 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HideTapToPlaceLabel::.ctor()
extern void HideTapToPlaceLabel__ctor_m6794BC057A5ECD54D34ECB538B144C64ADB87B2F (void);
// 0x00000231 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HideTapToPlaceLabel::<AddTapToPlaceListeners>b__3_0()
extern void HideTapToPlaceLabel_U3CAddTapToPlaceListenersU3Eb__3_0_m336168FCCF071E355A2405EF347038D93A48E42D (void);
// 0x00000232 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.HideTapToPlaceLabel::<AddTapToPlaceListeners>b__3_1()
extern void HideTapToPlaceLabel_U3CAddTapToPlaceListenersU3Eb__3_1_m1AA8691520D7DF4DE9A4D8AD73CC0CC243564E0B (void);
// 0x00000233 Microsoft.MixedReality.Toolkit.Utilities.TrackedObjectType Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::get_TrackedType()
extern void SolverExampleManager_get_TrackedType_mD7140CA43F1FF42C74AF9A549C26783B93A945CA (void);
// 0x00000234 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::set_TrackedType(Microsoft.MixedReality.Toolkit.Utilities.TrackedObjectType)
extern void SolverExampleManager_set_TrackedType_m93D5FD20B76FBB0DFE21ACAAE6E0FE4FEABCCEDF (void);
// 0x00000235 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::Awake()
extern void SolverExampleManager_Awake_m4E769F29BFB0A89453A366CD03ED22513CCFF3CA (void);
// 0x00000236 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::SetTrackedHead()
extern void SolverExampleManager_SetTrackedHead_m509A9C92475C1A36517BCBBC0C06CDF268EB60F6 (void);
// 0x00000237 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::SetTrackedController()
extern void SolverExampleManager_SetTrackedController_mA270F274CD1E4F03B5EFA56C2C5CBB06037AEA1E (void);
// 0x00000238 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::SetTrackedHands()
extern void SolverExampleManager_SetTrackedHands_m83DC980F63DC0260A53EB143DE29BDCDC6411201 (void);
// 0x00000239 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::SetTrackedCustom()
extern void SolverExampleManager_SetTrackedCustom_m0AA12923EA60ED6DFB526677035BEB0576887139 (void);
// 0x0000023A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::SetRadialView()
extern void SolverExampleManager_SetRadialView_m670CA059C79CABC24199F095C01278ECD46267F0 (void);
// 0x0000023B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::SetOrbital()
extern void SolverExampleManager_SetOrbital_m461EF1ACCA0797E42DD2267A407BB376FB879F28 (void);
// 0x0000023C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::SetSurfaceMagnetism()
extern void SolverExampleManager_SetSurfaceMagnetism_m6FF07116667BBEEDAFC0F764763AE79176D23C8C (void);
// 0x0000023D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::AddSolver()
// 0x0000023E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::RefreshSolverHandler()
extern void SolverExampleManager_RefreshSolverHandler_m4414E721F34A92E45708BB6B722D7F9010105D06 (void);
// 0x0000023F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::DestroySolver()
extern void SolverExampleManager_DestroySolver_mEA4002B933892316B9F99E63A9E81AECE5694724 (void);
// 0x00000240 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SolverExampleManager::.ctor()
extern void SolverExampleManager__ctor_m9246B15AE77ACCA0C5561E0C47F584C92332C0C8 (void);
// 0x00000241 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ClearSpatialObservations::ToggleObservers()
extern void ClearSpatialObservations_ToggleObservers_mBAB6166FC8149B938DDC6D9AAC0EBF95FEA20A75 (void);
// 0x00000242 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ClearSpatialObservations::.ctor()
extern void ClearSpatialObservations__ctor_m1E0D835CD191371274E4684286C6B89F3F429208 (void);
// 0x00000243 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ShowPlaneFindingInstructions::Start()
extern void ShowPlaneFindingInstructions_Start_m247E693FF28DFAEC5CDEACB5663C0910FD6EA90A (void);
// 0x00000244 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ShowPlaneFindingInstructions::.ctor()
extern void ShowPlaneFindingInstructions__ctor_m3A95CA6D0732D9CCCFCBEBD979EA091EBA53E106 (void);
// 0x00000245 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::OnEnable()
extern void BoundingBoxExampleTest_OnEnable_mF5699F759D368C849DEE2C19385F36471CA602C6 (void);
// 0x00000246 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::OnDisable()
extern void BoundingBoxExampleTest_OnDisable_m2B097058C7B209E3F3F1E9A90B0F10C1289EDE6B (void);
// 0x00000247 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::Start()
extern void BoundingBoxExampleTest_Start_m42BE5174E19AA69CB82DDABFF198774CCC798C7D (void);
// 0x00000248 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::SetStatus(System.String)
extern void BoundingBoxExampleTest_SetStatus_m31062624D016BAE3E3CD5184D561C99807C60842 (void);
// 0x00000249 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::Sequence()
extern void BoundingBoxExampleTest_Sequence_m0A42C1BD0F1E8F21CD3D8C3008AEB03AE3AE2240 (void);
// 0x0000024A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::DebugDrawObjectBounds(UnityEngine.Bounds)
extern void BoundingBoxExampleTest_DebugDrawObjectBounds_m5114ED7B2CC59FA2FFA8A219CD083B8FE312669C (void);
// 0x0000024B System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::WaitForSpeechCommand()
extern void BoundingBoxExampleTest_WaitForSpeechCommand_mBC53747078DBA387C49DB0E4848A9329E34CDC0D (void);
// 0x0000024C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::OnSpeechKeywordRecognized(Microsoft.MixedReality.Toolkit.Input.SpeechEventData)
extern void BoundingBoxExampleTest_OnSpeechKeywordRecognized_m1093AC9F44847EA731215651AA64651FE0801F0C (void);
// 0x0000024D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::.ctor()
extern void BoundingBoxExampleTest__ctor_mF2E87AD7C2294A6C60EBE7B9F276AA9A8A9370C2 (void);
// 0x0000024E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::<Sequence>b__12_0(Microsoft.MixedReality.Toolkit.UI.ManipulationEventData)
extern void BoundingBoxExampleTest_U3CSequenceU3Eb__12_0_m4F95AD13CBBA6BB3ED84AB44126AD26BC933DB67 (void);
// 0x0000024F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest::<Sequence>b__12_1(Microsoft.MixedReality.Toolkit.UI.ManipulationEventData)
extern void BoundingBoxExampleTest_U3CSequenceU3Eb__12_1_mD27CA529D7D5FD14AA5EAE422601F8CC5E583AD2 (void);
// 0x00000250 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<Sequence>d__12::.ctor(System.Int32)
extern void U3CSequenceU3Ed__12__ctor_m34250F7FFD94CF51D0BA8F66FAA696A74EBED421 (void);
// 0x00000251 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<Sequence>d__12::System.IDisposable.Dispose()
extern void U3CSequenceU3Ed__12_System_IDisposable_Dispose_m1EA4A1FA54F48C6410C00ECF692E454555F19E2A (void);
// 0x00000252 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<Sequence>d__12::MoveNext()
extern void U3CSequenceU3Ed__12_MoveNext_mFDEDE85DFB303C120702369CC6898588F49C022D (void);
// 0x00000253 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<Sequence>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSequenceU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m966AE4CFB2CB37D234767C012EC7095857E00B82 (void);
// 0x00000254 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<Sequence>d__12::System.Collections.IEnumerator.Reset()
extern void U3CSequenceU3Ed__12_System_Collections_IEnumerator_Reset_m847F3DF0C15BDD6E2D270512B111204A7A1028CC (void);
// 0x00000255 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<Sequence>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CSequenceU3Ed__12_System_Collections_IEnumerator_get_Current_mC6C5FB9C66089B57910B291CC5B9B396A2B6D0B5 (void);
// 0x00000256 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<WaitForSpeechCommand>d__14::.ctor(System.Int32)
extern void U3CWaitForSpeechCommandU3Ed__14__ctor_mB5943136E45C53386CF88C696988515A51E4A9A4 (void);
// 0x00000257 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<WaitForSpeechCommand>d__14::System.IDisposable.Dispose()
extern void U3CWaitForSpeechCommandU3Ed__14_System_IDisposable_Dispose_m241B40EFB3E896C86BD1572454BFDB2D695FEE84 (void);
// 0x00000258 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<WaitForSpeechCommand>d__14::MoveNext()
extern void U3CWaitForSpeechCommandU3Ed__14_MoveNext_mA7D26C53C3E266512878588E29CEAD493F5F9C5A (void);
// 0x00000259 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<WaitForSpeechCommand>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForSpeechCommandU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m10D6B994F0EF6EDF684CC5C0A0AC7B33EA6316EB (void);
// 0x0000025A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<WaitForSpeechCommand>d__14::System.Collections.IEnumerator.Reset()
extern void U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_Reset_m3292A30EC9B1AEAAB25EEF38748317038D3881FC (void);
// 0x0000025B System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundingBoxExampleTest/<WaitForSpeechCommand>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_get_Current_m2DBE575A1FE49BAEB66D584B50F2C0A91772C4B2 (void);
// 0x0000025C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::OnEnable()
extern void BoundsControlRuntimeExample_OnEnable_mD8784BD79AC23038F9A2687773D59FA91F933560 (void);
// 0x0000025D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::OnDisable()
extern void BoundsControlRuntimeExample_OnDisable_m2E1D8525126090E2F48ED4B15BFBC958709B936A (void);
// 0x0000025E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::Start()
extern void BoundsControlRuntimeExample_Start_mCA617717AF8B12A189B9742E38C4FFB4B88B5416 (void);
// 0x0000025F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::SetStatus(System.String)
extern void BoundsControlRuntimeExample_SetStatus_m40DBB2066DB922FCAE03D30DCB67952BBD51B883 (void);
// 0x00000260 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::Sequence()
extern void BoundsControlRuntimeExample_Sequence_mADF6858AFE4D2FAB573185743D611BD3C7C26CCF (void);
// 0x00000261 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::DebugDrawObjectBounds(UnityEngine.Bounds)
extern void BoundsControlRuntimeExample_DebugDrawObjectBounds_m983446412CDCE92FD973C904A3239BB29E0A7784 (void);
// 0x00000262 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::WaitForSpeechCommand()
extern void BoundsControlRuntimeExample_WaitForSpeechCommand_mBB8AF5BD87EFE0B9311F1327DB13626525442F3C (void);
// 0x00000263 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::OnSpeechKeywordRecognized(Microsoft.MixedReality.Toolkit.Input.SpeechEventData)
extern void BoundsControlRuntimeExample_OnSpeechKeywordRecognized_mFD7B66318D4D2CFA12A3DA6C14A6BE7A9BF2DB0E (void);
// 0x00000264 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::.ctor()
extern void BoundsControlRuntimeExample__ctor_m0F00BA1BCC38CBEA3861B95DACE6E98AFEBFB2F2 (void);
// 0x00000265 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::<Sequence>b__12_0(Microsoft.MixedReality.Toolkit.UI.ManipulationEventData)
extern void BoundsControlRuntimeExample_U3CSequenceU3Eb__12_0_m36BA64048BC4CFEE4DA52E5D2146DA1F21C6B826 (void);
// 0x00000266 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample::<Sequence>b__12_1(Microsoft.MixedReality.Toolkit.UI.ManipulationEventData)
extern void BoundsControlRuntimeExample_U3CSequenceU3Eb__12_1_mC4E12695D5E3AF83B72379DE584C30395A510730 (void);
// 0x00000267 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<Sequence>d__12::.ctor(System.Int32)
extern void U3CSequenceU3Ed__12__ctor_mB596D0726FB14CE0546E56694FB5B2CACB0CF45C (void);
// 0x00000268 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<Sequence>d__12::System.IDisposable.Dispose()
extern void U3CSequenceU3Ed__12_System_IDisposable_Dispose_mF11A785D0A2418CAA49D61132D659F18559AF866 (void);
// 0x00000269 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<Sequence>d__12::MoveNext()
extern void U3CSequenceU3Ed__12_MoveNext_m7604948CBEB77C5759BE0CB6DF15D7DCA93F1295 (void);
// 0x0000026A System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<Sequence>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSequenceU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5F0E02B2B5C6D0F72716E6F36D0C6639ECD72379 (void);
// 0x0000026B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<Sequence>d__12::System.Collections.IEnumerator.Reset()
extern void U3CSequenceU3Ed__12_System_Collections_IEnumerator_Reset_m43D114449892836ED049B28D22F2E047E47C3D6B (void);
// 0x0000026C System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<Sequence>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CSequenceU3Ed__12_System_Collections_IEnumerator_get_Current_mE027BFD6EE3F6E9724BAC26E0B59C0F8C2C35A7E (void);
// 0x0000026D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<WaitForSpeechCommand>d__14::.ctor(System.Int32)
extern void U3CWaitForSpeechCommandU3Ed__14__ctor_mC11BB974A74E2A51DC40095516466073D190FF36 (void);
// 0x0000026E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<WaitForSpeechCommand>d__14::System.IDisposable.Dispose()
extern void U3CWaitForSpeechCommandU3Ed__14_System_IDisposable_Dispose_mF17BD015850AF82D586F2E502A0F98DFDF1CF681 (void);
// 0x0000026F System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<WaitForSpeechCommand>d__14::MoveNext()
extern void U3CWaitForSpeechCommandU3Ed__14_MoveNext_m6948658B17A39BA0B2F4506F82C2F9746602C9AC (void);
// 0x00000270 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<WaitForSpeechCommand>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForSpeechCommandU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C339347BA3254B9C3D4ED05B7EC66545A274319 (void);
// 0x00000271 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<WaitForSpeechCommand>d__14::System.Collections.IEnumerator.Reset()
extern void U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_Reset_m546A0C9B8314A5766CE101397F9E7DC39CB10DE6 (void);
// 0x00000272 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.BoundsControlRuntimeExample/<WaitForSpeechCommand>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_get_Current_m6DE668CFDD0D0BCCFAE54D3D6F21AFAE511AFB38 (void);
// 0x00000273 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::NextLayout()
extern void GridObjectLayoutControl_NextLayout_m1499CAA82D16FBACE09A80C507B8352E1B102304 (void);
// 0x00000274 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::PreviousLayout()
extern void GridObjectLayoutControl_PreviousLayout_m285230E1E2B8C2F32DBBC5BCF78EA1BC258727C2 (void);
// 0x00000275 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::RunTest()
extern void GridObjectLayoutControl_RunTest_mED12A96A86E2B58A9884E045802877B0E984539D (void);
// 0x00000276 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::Start()
extern void GridObjectLayoutControl_Start_mFE0051D0D2D35F8B617F8F7FDA4CBD9ECA781751 (void);
// 0x00000277 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::UpdateUI()
extern void GridObjectLayoutControl_UpdateUI_mAD29EFFB04BEC522668D753A6574D356F868CF87 (void);
// 0x00000278 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::TestAnchors()
extern void GridObjectLayoutControl_TestAnchors_mE3AA69D08ED0CF4432BFBE26768657FA5755555E (void);
// 0x00000279 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::PrintGrid(Microsoft.MixedReality.Toolkit.Utilities.GridObjectCollection,System.String,System.String,System.Text.StringBuilder)
extern void GridObjectLayoutControl_PrintGrid_m2621F3217BCF6A73D69EEF218346904962E0F71B (void);
// 0x0000027A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl::.ctor()
extern void GridObjectLayoutControl__ctor_m0E595A658BED0BAA77CD0CC0FC1359315AC9F73B (void);
// 0x0000027B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl/<TestAnchors>d__7::.ctor(System.Int32)
extern void U3CTestAnchorsU3Ed__7__ctor_m431EC69B94B62747171BF91FA9EBA93C74F1AD56 (void);
// 0x0000027C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl/<TestAnchors>d__7::System.IDisposable.Dispose()
extern void U3CTestAnchorsU3Ed__7_System_IDisposable_Dispose_m8C7D9B0F8F63A73C58000A6C42F7E017F5A01BAA (void);
// 0x0000027D System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl/<TestAnchors>d__7::MoveNext()
extern void U3CTestAnchorsU3Ed__7_MoveNext_mA0BD8E63446DA71018E8226B0AF287C0C1C9E0C7 (void);
// 0x0000027E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl/<TestAnchors>d__7::<>m__Finally1()
extern void U3CTestAnchorsU3Ed__7_U3CU3Em__Finally1_m94CE60E49131A18A6125AC2B2920D2C60B1AC8DC (void);
// 0x0000027F System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl/<TestAnchors>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTestAnchorsU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C0EB33A01381805945D1A8E56E4D2794FABEFE4 (void);
// 0x00000280 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl/<TestAnchors>d__7::System.Collections.IEnumerator.Reset()
extern void U3CTestAnchorsU3Ed__7_System_Collections_IEnumerator_Reset_mAC0260AC48A259E59C1D6B027E9A1461C84A50AA (void);
// 0x00000281 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.GridObjectLayoutControl/<TestAnchors>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CTestAnchorsU3Ed__7_System_Collections_IEnumerator_get_Current_m8537D1C2951BE0CF00056F85E74785D071BE37BC (void);
// 0x00000282 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ChangeManipulation::Start()
extern void ChangeManipulation_Start_m9B5859543BE13431D4E6F3A350EB4F4BC59739AE (void);
// 0x00000283 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ChangeManipulation::Update()
extern void ChangeManipulation_Update_mB7214E4AA6802C60FF959EF4DFA1FBED9D772865 (void);
// 0x00000284 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ChangeManipulation::TryStopManipulation()
extern void ChangeManipulation_TryStopManipulation_mA82D4D8EB72A25D1F90B69AB9A081DFA41B49575 (void);
// 0x00000285 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ChangeManipulation::.ctor()
extern void ChangeManipulation__ctor_m5A7AEE890A234FCE014AD4D42B7F5F36E2AB36E7 (void);
// 0x00000286 UnityEngine.Transform Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::get_FrontBounds()
extern void ReturnToBounds_get_FrontBounds_m5D51CD36EB3B67CAEE1F0C5C8B7752F062441363 (void);
// 0x00000287 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::set_FrontBounds(UnityEngine.Transform)
extern void ReturnToBounds_set_FrontBounds_m93122CE58D003DB5724530FB71988C6E393CA35F (void);
// 0x00000288 UnityEngine.Transform Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::get_BackBounds()
extern void ReturnToBounds_get_BackBounds_m2D8CACB168818D5473120072F3B355B9CFCB90BF (void);
// 0x00000289 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::set_BackBounds(UnityEngine.Transform)
extern void ReturnToBounds_set_BackBounds_m38AFF96BF3EC6F5B91CCFD945E769560EBCEC01A (void);
// 0x0000028A UnityEngine.Transform Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::get_LeftBounds()
extern void ReturnToBounds_get_LeftBounds_mE3FECA465276F8E6674C007CAA9853CEB5DAC000 (void);
// 0x0000028B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::set_LeftBounds(UnityEngine.Transform)
extern void ReturnToBounds_set_LeftBounds_m0FC9854D68BA0B5BC941308AB94E28FBE2944783 (void);
// 0x0000028C UnityEngine.Transform Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::get_RightBounds()
extern void ReturnToBounds_get_RightBounds_m853F1CCFCDCE8096557BB2EBAA026DCFDEFF30B0 (void);
// 0x0000028D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::set_RightBounds(UnityEngine.Transform)
extern void ReturnToBounds_set_RightBounds_mC394B19424E76DC0DDF18625F2A8DA055403D668 (void);
// 0x0000028E UnityEngine.Transform Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::get_BottomBounds()
extern void ReturnToBounds_get_BottomBounds_m421C9034BAB83EFB5FEDBD104B00A0EDAA02019C (void);
// 0x0000028F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::set_BottomBounds(UnityEngine.Transform)
extern void ReturnToBounds_set_BottomBounds_m959F6831D884B544087EB90ABE463F5A0B3D6DAC (void);
// 0x00000290 UnityEngine.Transform Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::get_TopBounds()
extern void ReturnToBounds_get_TopBounds_m74584E35257F2761C3F5F4A9AE179B61AD5A5885 (void);
// 0x00000291 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::set_TopBounds(UnityEngine.Transform)
extern void ReturnToBounds_set_TopBounds_mE6AA5D30B720C8362FA11AB5CC149EE64AD32F5B (void);
// 0x00000292 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::Start()
extern void ReturnToBounds_Start_mDED4547120AC1099709C47D01BCD4ECE064B74C7 (void);
// 0x00000293 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::Update()
extern void ReturnToBounds_Update_m673D1B21CAE747CBB605DBB115FD107D4217F515 (void);
// 0x00000294 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReturnToBounds::.ctor()
extern void ReturnToBounds__ctor_mDFD3274B3143B8D6BDD47B611D4C9CE4FF284EF2 (void);
// 0x00000295 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo::OnClickBar()
extern void ProgressIndicatorDemo_OnClickBar_m0669C13770871FBA4D50E9D0B8CE1BA94D2C7938 (void);
// 0x00000296 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo::OnClickRotating()
extern void ProgressIndicatorDemo_OnClickRotating_mB38E459130B2EDCB290F3122A96AE6B696FD4F6D (void);
// 0x00000297 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo::OnClickOrbs()
extern void ProgressIndicatorDemo_OnClickOrbs_m3EC71B07E84D63A79BBDE831E9DE6CA03EBC5723 (void);
// 0x00000298 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo::HandleButtonClick(Microsoft.MixedReality.Toolkit.UI.IProgressIndicator)
extern void ProgressIndicatorDemo_HandleButtonClick_m83BDE61656ADAE9FA660C25015AF0ED3CE1489A2 (void);
// 0x00000299 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo::OnEnable()
extern void ProgressIndicatorDemo_OnEnable_m23C6807F2ACC1D876D31B851A1B47D9F530CDFAE (void);
// 0x0000029A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo::Update()
extern void ProgressIndicatorDemo_Update_m6A6926AF826F45B1BEEBB84E3EC3B714EDA4FFA8 (void);
// 0x0000029B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo::OpenProgressIndicator(Microsoft.MixedReality.Toolkit.UI.IProgressIndicator)
extern void ProgressIndicatorDemo_OpenProgressIndicator_m21F6477AF93FE257DF3D39A22301C610B7AA2EC3 (void);
// 0x0000029C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo::.ctor()
extern void ProgressIndicatorDemo__ctor_mC6F99148E68725F7FECBDF033BF9BFE6834C1FC8 (void);
// 0x0000029D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo/<HandleButtonClick>d__14::.ctor()
extern void U3CHandleButtonClickU3Ed__14__ctor_m848977C20B13093F1E9EACBFC57FA09F7455B1BB (void);
// 0x0000029E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo/<HandleButtonClick>d__14::MoveNext()
extern void U3CHandleButtonClickU3Ed__14_MoveNext_m4AC6E6BA1609365296EED5AC2B8638D9251931DA (void);
// 0x0000029F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo/<HandleButtonClick>d__14::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CHandleButtonClickU3Ed__14_SetStateMachine_m6763E06BEC587E1ABE97242AC32B2C93DA60BB30 (void);
// 0x000002A0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo/<OpenProgressIndicator>d__17::.ctor()
extern void U3COpenProgressIndicatorU3Ed__17__ctor_m6E691D5CD4FB79BEE77CF65B876075D95A35466B (void);
// 0x000002A1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo/<OpenProgressIndicator>d__17::MoveNext()
extern void U3COpenProgressIndicatorU3Ed__17_MoveNext_mB1E044A6F8C94FD36A0D2D409E2DEF69BA395066 (void);
// 0x000002A2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ProgressIndicatorDemo/<OpenProgressIndicator>d__17::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3COpenProgressIndicatorU3Ed__17_SetStateMachine_m8402426A9BB1212956FF5CDA08A962A3FA14D274 (void);
// 0x000002A3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SliderLunarLander::OnSliderUpdated(Microsoft.MixedReality.Toolkit.UI.SliderEventData)
extern void SliderLunarLander_OnSliderUpdated_mC16BE001428FDAF910F4CF7C61305207EC7EF333 (void);
// 0x000002A4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.SliderLunarLander::.ctor()
extern void SliderLunarLander__ctor_mB94D8A45DEC6B5C3BB0D135768EDE4FC38DBAF63 (void);
// 0x000002A5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.MixedRealityCapabilityDemo::Start()
extern void MixedRealityCapabilityDemo_Start_mDB1E25EF111E12F6EE4F0F10629AE2B8ED9248A4 (void);
// 0x000002A6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.MixedRealityCapabilityDemo::.ctor()
extern void MixedRealityCapabilityDemo__ctor_m261D71EA1F79A30189D5890748E17BDDAC1215F9 (void);
// 0x000002A7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReadingMode.ReadingModeSceneBehavior::Update()
extern void ReadingModeSceneBehavior_Update_m2F659AD4FB9815286524185BA754A81EF4C90DCC (void);
// 0x000002A8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReadingMode.ReadingModeSceneBehavior::EnableReadingMode()
extern void ReadingModeSceneBehavior_EnableReadingMode_mDF52B1777A6F182347152396BEE754B22B1F991C (void);
// 0x000002A9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReadingMode.ReadingModeSceneBehavior::DisableReadingMode()
extern void ReadingModeSceneBehavior_DisableReadingMode_m8E98A3230FBB49929A665A7C5AB5ABAE9436FDF2 (void);
// 0x000002AA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.ReadingMode.ReadingModeSceneBehavior::.ctor()
extern void ReadingModeSceneBehavior__ctor_m65BFF2ECDB24DC6FFBAB18658956B1F7D394B6E4 (void);
// 0x000002AB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::Awake()
extern void ColorTap_Awake_mD7C1B08ECE260E2297A99D4DE1CD09CB45A8BC1A (void);
// 0x000002AC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusHandler.OnFocusEnter(Microsoft.MixedReality.Toolkit.Input.FocusEventData)
extern void ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusEnter_m6E44F2727094C8EBF8FCC9958CEF0FA2056A0756 (void);
// 0x000002AD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusHandler.OnFocusExit(Microsoft.MixedReality.Toolkit.Input.FocusEventData)
extern void ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusExit_mAD8E03E07BE24EA179115A8DDE719EB11B61E214 (void);
// 0x000002AE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_mB5900C15340DA3DABAF4D9E251638AE616D8D270 (void);
// 0x000002AF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m4DF037B7B105FB94966D9B680B514E6D1D60A774 (void);
// 0x000002B0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m735B13348165A2E9FDC807EABE2B46A684F6348A (void);
// 0x000002B1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m34EBD45A2E1601302609152233B5548B0F5175FE (void);
// 0x000002B2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ColorTap::.ctor()
extern void ColorTap__ctor_mBEE7EFEC383886F34D6B31808A60C8CB909AFFBB (void);
// 0x000002B3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FollowEyeGazeGazeProvider::Update()
extern void FollowEyeGazeGazeProvider_Update_m1B5E7680F82F544506762FC2E796FB3A168C0E70 (void);
// 0x000002B4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FollowEyeGazeGazeProvider::.ctor()
extern void FollowEyeGazeGazeProvider__ctor_mE290DF9824F98B561DED1EF303858488DE0BB2E8 (void);
// 0x000002B5 Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeSaccadeProvider Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::get_EyeSaccadeProvider()
extern void PanZoomBase_get_EyeSaccadeProvider_m54122982D1F541DC3B9BD123A277676A041B503C (void);
// 0x000002B6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Initialize()
// 0x000002B7 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ComputePanSpeed(System.Single,System.Single,System.Single)
// 0x000002B8 System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomDir(System.Boolean)
// 0x000002B9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomIn()
// 0x000002BA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomOut()
// 0x000002BB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::UpdatePanZoom()
// 0x000002BC System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::UpdateCursorPosInHitBox()
// 0x000002BD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Start()
extern void PanZoomBase_Start_mB2AC9F50D6E4CDE24357BB56C46A5E2497D19F44 (void);
// 0x000002BE UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::get_CustomColliderSizeOnLookAt()
extern void PanZoomBase_get_CustomColliderSizeOnLookAt_m613A2D50BFA678C0F17548AA4D41E2587971AB53 (void);
// 0x000002BF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::AutoPan()
extern void PanZoomBase_AutoPan_m9C44A4C5452309E37AB0BDA46919E8E215C36789 (void);
// 0x000002C0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::PanHorizontally(System.Single)
extern void PanZoomBase_PanHorizontally_m87DC9B06CBB3939D282DB83075F0813346184979 (void);
// 0x000002C1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::PanVertically(System.Single)
extern void PanZoomBase_PanVertically_mCD7E30718310841D216EEC36B9F76CC811EDBBC8 (void);
// 0x000002C2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::EnableHandZoom()
extern void PanZoomBase_EnableHandZoom_m1E702AFDD5BCEA24E1ED1853D0651C20C328D135 (void);
// 0x000002C3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::DisableHandZoom()
extern void PanZoomBase_DisableHandZoom_m17EB5237914603616422A93CB367B415FE97A3CF (void);
// 0x000002C4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomStart(System.Boolean)
extern void PanZoomBase_ZoomStart_mC8BC6AC9AE224B2FB82FB92E6566B9F6B407B744 (void);
// 0x000002C5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomInStart()
extern void PanZoomBase_ZoomInStart_m12FBEE8B853EB825BF5BEA07D6B7EB11BE1AE20E (void);
// 0x000002C6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomOutStart()
extern void PanZoomBase_ZoomOutStart_m14C030096D7114826959DAC20F70AB282B7421EA (void);
// 0x000002C7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomStop()
extern void PanZoomBase_ZoomStop_mDDB1EA77FD19AD798650A29BFE9CB0EB924D7D83 (void);
// 0x000002C8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::NavigationStart()
extern void PanZoomBase_NavigationStart_m45E26026D5B1300E61FE56A03F6B2213CDBFCAD7 (void);
// 0x000002C9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::NavigationStop()
extern void PanZoomBase_NavigationStop_m827D2FF96466349D96A118EBE0ECB04BA7C15EA6 (void);
// 0x000002CA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::NavigationUpdate(UnityEngine.Vector3)
extern void PanZoomBase_NavigationUpdate_m7C484203D9CD7694E091AB0A7C0DAC1C61D6BFC9 (void);
// 0x000002CB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Update()
extern void PanZoomBase_Update_m00F45CB496D3631BF0ECF92278C2B3286150E8FF (void);
// 0x000002CC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::SetSkimProofUpdateSpeed(System.Single)
extern void PanZoomBase_SetSkimProofUpdateSpeed_m4699A1857A766D3248AF8B3DE9CE91F8F7799F88 (void);
// 0x000002CD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ResetNormFixator()
extern void PanZoomBase_ResetNormFixator_m47151A514877BA6094235191DDE84BC83363D92C (void);
// 0x000002CE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::IncrementNormFixator()
extern void PanZoomBase_IncrementNormFixator_m30ECAE4BB7A61BB17CFDD3771D058856E9A9F67B (void);
// 0x000002CF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ResetScroll_OnSaccade()
extern void PanZoomBase_ResetScroll_OnSaccade_mE6268E2B9B8D2DB5E2161406B36108AD05D64E08 (void);
// 0x000002D0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::LateUpdate()
extern void PanZoomBase_LateUpdate_mCBF740F4D940AC7772456D15E85662E54384789F (void);
// 0x000002D1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::AutomaticGazePanning()
extern void PanZoomBase_AutomaticGazePanning_mD2990C63556FEAD2E7DCC7B5F695B1E9071D3BBB (void);
// 0x000002D2 UnityEngine.BoxCollider Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::get_MyCollider()
extern void PanZoomBase_get_MyCollider_mDF7F61FAD9952D54BA5EBAB60B08D0D21CA3B38F (void);
// 0x000002D3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::set_MyCollider(UnityEngine.BoxCollider)
extern void PanZoomBase_set_MyCollider_m05BBD9058905EA503703B2C0B31B33F18922330B (void);
// 0x000002D4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::PanUpDown(System.Single)
extern void PanZoomBase_PanUpDown_m5A78755C13BD68821BC544DE6675A90AE54169E2 (void);
// 0x000002D5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::PanLeftRight(System.Single)
extern void PanZoomBase_PanLeftRight_m38B9B3671A522A4A9EB7D794429A34DD3157011E (void);
// 0x000002D6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::UpdateZoom()
extern void PanZoomBase_UpdateZoom_m1CC7ACF55C1C36D719FFD0DE85FA42F192B7A98A (void);
// 0x000002D7 UnityEngine.Vector2 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::LimitScaling(UnityEngine.Vector2)
extern void PanZoomBase_LimitScaling_mF3A761450398BA48138F31672A25B437382C1F96 (void);
// 0x000002D8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomIn_Timed()
extern void PanZoomBase_ZoomIn_Timed_m83891CE9E6A77964A252587EBF9DBA50EB038976 (void);
// 0x000002D9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomOut_Timed()
extern void PanZoomBase_ZoomOut_Timed_m2A4E50E77F583A0B534BC31650E857CC126605F6 (void);
// 0x000002DA System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::ZoomAndStop(System.Boolean)
extern void PanZoomBase_ZoomAndStop_m70C71BF56D1145AB3CDF2E00A8EB01D238FBCECD (void);
// 0x000002DB System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::UpdateValues(T&,T)
// 0x000002DC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::StartFocusing()
extern void PanZoomBase_StartFocusing_mDCEB350C0CC6666D3B1B05153662A2D46B057F33 (void);
// 0x000002DD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::StopFocusing()
extern void PanZoomBase_StopFocusing_m4168EC474B568F3C6E68BE20035FCE84BCE7629F (void);
// 0x000002DE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_mB2B6486DC654182010BB9CCE877A183C5CC86F8F (void);
// 0x000002DF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m80F04936779BA459AAC68DA9E10E9A9416374662 (void);
// 0x000002E0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m11D3CA9CB5A43BB170A9311F61CE08EBA8AEB50C (void);
// 0x000002E1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m22BA6F218C16DC807D6FA43F51FBE011C68C1387 (void);
// 0x000002E2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusHandler.OnFocusEnter(Microsoft.MixedReality.Toolkit.Input.FocusEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusEnter_m2C8A1F5B2F6373FFE26C6F33339B2AC4FF95B4F3 (void);
// 0x000002E3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusHandler.OnFocusExit(Microsoft.MixedReality.Toolkit.Input.FocusEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusExit_m5D9A62A14CBC02469086895CC60039490BD4B594 (void);
// 0x000002E4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourceStateHandler.OnSourceDetected(Microsoft.MixedReality.Toolkit.Input.SourceStateEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceDetected_m6F1D3E4E95248CE8CD902CE1D43FE2E92E3EEC74 (void);
// 0x000002E5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourceStateHandler.OnSourceLost(Microsoft.MixedReality.Toolkit.Input.SourceStateEventData)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceLost_m30DD157C7E9D8951BAFA2882776A48E33756AEBA (void);
// 0x000002E6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::Microsoft.MixedReality.Toolkit.Input.IMixedRealityHandJointHandler.OnHandJointsUpdated(Microsoft.MixedReality.Toolkit.Input.InputEventData`1<System.Collections.Generic.IDictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>>)
extern void PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityHandJointHandler_OnHandJointsUpdated_m0C96D00490A1DDD90055D4833606476271408577 (void);
// 0x000002E7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase::.ctor()
extern void PanZoomBase__ctor_m24873AA0FC327596CDD96E8BE0C37690D939DEF1 (void);
// 0x000002E8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase/<ZoomAndStop>d__78::.ctor(System.Int32)
extern void U3CZoomAndStopU3Ed__78__ctor_m46258496853C97A3C755A49B27F1D14CBCDA8AC4 (void);
// 0x000002E9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase/<ZoomAndStop>d__78::System.IDisposable.Dispose()
extern void U3CZoomAndStopU3Ed__78_System_IDisposable_Dispose_m3125E7B1AF6696673562538B404794532B5C6581 (void);
// 0x000002EA System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase/<ZoomAndStop>d__78::MoveNext()
extern void U3CZoomAndStopU3Ed__78_MoveNext_m2C96529214D8702563C7965B55318600265CB607 (void);
// 0x000002EB System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase/<ZoomAndStop>d__78::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CZoomAndStopU3Ed__78_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5007A408115112E9A302CE1D2D2D6C8912A0B878 (void);
// 0x000002EC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase/<ZoomAndStop>d__78::System.Collections.IEnumerator.Reset()
extern void U3CZoomAndStopU3Ed__78_System_Collections_IEnumerator_Reset_m377FFA4C86BEBD8E6385A94CAFD88AC4F7389826 (void);
// 0x000002ED System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBase/<ZoomAndStop>d__78::System.Collections.IEnumerator.get_Current()
extern void U3CZoomAndStopU3Ed__78_System_Collections_IEnumerator_get_Current_m40F497BEDB0DA316498CD3F4F0964E601517371A (void);
// 0x000002EE System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::get_IsValid()
extern void PanZoomBaseRectTransf_get_IsValid_m5D29DF4CB76F95941777C7A6BB307AB49E6BC01B (void);
// 0x000002EF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::Initialize()
extern void PanZoomBaseRectTransf_Initialize_m304AB17FDCF5BA852AF94FC535610C4CC13E7435 (void);
// 0x000002F0 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::ComputePanSpeed(System.Single,System.Single,System.Single)
extern void PanZoomBaseRectTransf_ComputePanSpeed_mFFA0C0424D22DCEECD2C4B8F5611565436571DBB (void);
// 0x000002F1 System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::ZoomDir(System.Boolean)
extern void PanZoomBaseRectTransf_ZoomDir_m612F30B26B787EB6188A88028AE85E4A1B6C72C3 (void);
// 0x000002F2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::ZoomIn()
extern void PanZoomBaseRectTransf_ZoomIn_m4A9318A8FA4A71EAD40854278E99B72620075084 (void);
// 0x000002F3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::ZoomOut()
extern void PanZoomBaseRectTransf_ZoomOut_mF0DB9B8493D9798EB33E0AFA7A218BF0AF480B4A (void);
// 0x000002F4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::UpdatePanZoom()
extern void PanZoomBaseRectTransf_UpdatePanZoom_m3894AC6AD57A0ACB8BDFF78C7E1854F31B939063 (void);
// 0x000002F5 UnityEngine.Vector2 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::LimitPanning()
extern void PanZoomBaseRectTransf_LimitPanning_m894E46A488DDC62BA5CDCA5AB7C5A933A8A5F9D6 (void);
// 0x000002F6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::ZoomInOut_RectTransform(System.Single,UnityEngine.Vector2)
extern void PanZoomBaseRectTransf_ZoomInOut_RectTransform_mD0BE9A3412C516F2553E7FBED0972793CEF45FDF (void);
// 0x000002F7 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::UpdateCursorPosInHitBox()
extern void PanZoomBaseRectTransf_UpdateCursorPosInHitBox_m3B3AD8F39A81A1F91FCA4AE2F2AD4DEECA589C72 (void);
// 0x000002F8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseRectTransf::.ctor()
extern void PanZoomBaseRectTransf__ctor_m11AB56AF46AE8BB2F20D994F6BED8B0573148F26 (void);
// 0x000002F9 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::get_TextureShaderProperty()
extern void PanZoomBaseTexture_get_TextureShaderProperty_m4B45BEB6957C6D3FD827E779DB034D500DC97997 (void);
// 0x000002FA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::set_TextureShaderProperty(System.String)
extern void PanZoomBaseTexture_set_TextureShaderProperty_m772C0F604825DB0A208BE1B660796DB03260F722 (void);
// 0x000002FB System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::get_IsValid()
extern void PanZoomBaseTexture_get_IsValid_mCE13EEB04D55551F584DB342CD76C1D76E56B71D (void);
// 0x000002FC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::Initialize()
extern void PanZoomBaseTexture_Initialize_m8C7221CA442C5308A726ACB64BC04FE7329AC99F (void);
// 0x000002FD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::Initialize(System.Single)
extern void PanZoomBaseTexture_Initialize_m05A14D9F9F033E583D5D601378A6750911205561 (void);
// 0x000002FE System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::ComputePanSpeed(System.Single,System.Single,System.Single)
extern void PanZoomBaseTexture_ComputePanSpeed_mD4AD55BC5485C2C65F48143A9EDB7DE8F5C848C0 (void);
// 0x000002FF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::UpdatePanZoom()
extern void PanZoomBaseTexture_UpdatePanZoom_m3B9D782F6D52133334A9FBDB699CAB6BE353E3A8 (void);
// 0x00000300 System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::ZoomDir(System.Boolean)
extern void PanZoomBaseTexture_ZoomDir_mDAD8C9A1472E8A9D60A82867E2F61C2BA46D3CFE (void);
// 0x00000301 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::ZoomIn()
extern void PanZoomBaseTexture_ZoomIn_m21E4133ADDD195303FEE61014F5BEFFDCC474397 (void);
// 0x00000302 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::ZoomOut()
extern void PanZoomBaseTexture_ZoomOut_mA9E5E70557BB57E307D9CC56BE6BD5C7A61EFDB2 (void);
// 0x00000303 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::ZoomInOut(System.Single,UnityEngine.Vector2)
extern void PanZoomBaseTexture_ZoomInOut_m24ACD9B538DAE3E0B6649F45D936CAEC4D081EF0 (void);
// 0x00000304 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::UpdateCursorPosInHitBox()
extern void PanZoomBaseTexture_UpdateCursorPosInHitBox_mBD58ACC99C53F27EB3762979D3A611083A5E21E9 (void);
// 0x00000305 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomBaseTexture::.ctor()
extern void PanZoomBaseTexture__ctor_mDBCA76CCAABABB178FA4C8D80C757D3CCC91D1BC (void);
// 0x00000306 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomRectTransf::Start()
extern void PanZoomRectTransf_Start_m27D8EDC74352F194FB3327D4807D1D1A32B012B1 (void);
// 0x00000307 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomRectTransf::Update()
extern void PanZoomRectTransf_Update_m04B2A6C04DB780365FFC1A9E89BFF0B75F4507C3 (void);
// 0x00000308 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomRectTransf::.ctor()
extern void PanZoomRectTransf__ctor_m64773AF9E1050728E16DD1BBDF7EAFFDEAEFDA01 (void);
// 0x00000309 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomTexture::Start()
extern void PanZoomTexture_Start_m988C5BEA2E31A4DE6537237C5B9EDB00C8E15CD9 (void);
// 0x0000030A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomTexture::Update()
extern void PanZoomTexture_Update_m55FA693B468DAF2C500041B333168091D851DAD8 (void);
// 0x0000030B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.PanZoomTexture::.ctor()
extern void PanZoomTexture__ctor_m7456429EE8743EEB9BB9F89A8C56E9CA731778DA (void);
// 0x0000030C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ScrollRectTransf::Start()
extern void ScrollRectTransf_Start_m1A80351FA46983239AB43136A7A64B0D26E7AA86 (void);
// 0x0000030D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ScrollRectTransf::UpdatePivot()
extern void ScrollRectTransf_UpdatePivot_mA8C57285A8BD701CE048C1242DF15C146A4B04B0 (void);
// 0x0000030E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ScrollRectTransf::Update()
extern void ScrollRectTransf_Update_m7927734A419213852CD8551DF8462E214DD60483 (void);
// 0x0000030F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ScrollRectTransf::.ctor()
extern void ScrollRectTransf__ctor_m4AFE6A078A07889149870AEC308EDD6027A06BBB (void);
// 0x00000310 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ScrollTexture::Start()
extern void ScrollTexture_Start_mCDC3C04C78ABE8C199C71D26434744594EF35435 (void);
// 0x00000311 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ScrollTexture::Update()
extern void ScrollTexture_Update_mF33FD2AA64F1B11074C6566F7DD65FABCB26B7E1 (void);
// 0x00000312 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ScrollTexture::.ctor()
extern void ScrollTexture__ctor_m4EA45B40632FD8F435A91F1C3D746D8303DD58F2 (void);
// 0x00000313 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::Start()
extern void TargetMoveToCamera_Start_m72D92E90A57197B12903E158E4B576C3E25DA470 (void);
// 0x00000314 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::Update()
extern void TargetMoveToCamera_Update_m284F3EACD09398F6528DBEDE0F344D406E0A9F50 (void);
// 0x00000315 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::OnEyeFocusStop()
extern void TargetMoveToCamera_OnEyeFocusStop_m126BEFCE18BF4127DA5F6616BA3C3C88FEE8EBE2 (void);
// 0x00000316 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::OnSelect()
extern void TargetMoveToCamera_OnSelect_m8785E4CEA463BED545179F47C1C9CD8B8BEE9A88 (void);
// 0x00000317 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::TransitionToUser()
extern void TargetMoveToCamera_TransitionToUser_mE30CBC5A23DC23FC9472BF63B774C5D491077D12 (void);
// 0x00000318 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::ReturnHome()
extern void TargetMoveToCamera_ReturnHome_m39C5FA950E85529D6C4471501D4BF63DE0A54A10 (void);
// 0x00000319 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::TransitionToCamera()
extern void TargetMoveToCamera_TransitionToCamera_m4C90FB2B0849B3ADCB7452EB93CB62F96CB336FC (void);
// 0x0000031A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetMoveToCamera::.ctor()
extern void TargetMoveToCamera__ctor_mA92726478EE7996B37EC09376DACC1D093B6C990 (void);
// 0x0000031B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.GrabReleaseDetector::Awake()
extern void GrabReleaseDetector_Awake_m2963BDEBC9FD6BE02DD8C9D0C4CB786DE3FA7D55 (void);
// 0x0000031C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.GrabReleaseDetector::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m4A611D12979750CB06A4EED2D2B633F9CCE71D25 (void);
// 0x0000031D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.GrabReleaseDetector::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m54A6050F9D224F51E6F3CDA582E438D436B1A018 (void);
// 0x0000031E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.GrabReleaseDetector::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m9612388FAF341C7237A2017F9F07ACE436647953 (void);
// 0x0000031F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.GrabReleaseDetector::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m72853C89D7E9F30CE6F12A43D3AEA2D59D3E28ED (void);
// 0x00000320 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.GrabReleaseDetector::.ctor()
extern void GrabReleaseDetector__ctor_mEE4B722D4A1972E140F74E7F6F327CC8E3D0478B (void);
// 0x00000321 Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeGazeProvider Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::get_EyeTrackingProvider()
extern void MoveObjByEyeGaze_get_EyeTrackingProvider_m336C129B68E5A3529AB3E7A056D62D900190CD83 (void);
// 0x00000322 System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::get_ConstrX()
extern void MoveObjByEyeGaze_get_ConstrX_m48BD1AE4EBC4675A967A6C2241E2252447AA4A66 (void);
// 0x00000323 System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::get_ConstrY()
extern void MoveObjByEyeGaze_get_ConstrY_m599664BA2429E9B6EE18C6063DA234FB3E1DAAC0 (void);
// 0x00000324 System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::get_ConstrZ()
extern void MoveObjByEyeGaze_get_ConstrZ_m9BC540EC9B034AD4A92FA9388D94229C0DCFAC4D (void);
// 0x00000325 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Start()
extern void MoveObjByEyeGaze_Start_m0F33CE4286DFEEB158ED7DCFB68C968245D4FCEF (void);
// 0x00000326 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Update()
extern void MoveObjByEyeGaze_Update_m44A1FBF92CD8632470AFF5D79EFC6AD8F37421AF (void);
// 0x00000327 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealitySpeechHandler.OnSpeechKeywordRecognized(Microsoft.MixedReality.Toolkit.Input.SpeechEventData)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySpeechHandler_OnSpeechKeywordRecognized_mB9F589637679B37E4BDC6064F21719A9790A4D1B (void);
// 0x00000328 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealityHandJointHandler.OnHandJointsUpdated(Microsoft.MixedReality.Toolkit.Input.InputEventData`1<System.Collections.Generic.IDictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>>)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityHandJointHandler_OnHandJointsUpdated_m71910ECEF3CEC5139583564A72593D8139D33C46 (void);
// 0x00000329 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourceStateHandler.OnSourceDetected(Microsoft.MixedReality.Toolkit.Input.SourceStateEventData)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceDetected_m73714E747BB21C58C7EE6728A45C15F46F7A87C0 (void);
// 0x0000032A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourceStateHandler.OnSourceLost(Microsoft.MixedReality.Toolkit.Input.SourceStateEventData)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceLost_mB3FB97B77A33343E1752E418A5884ADFF5029528 (void);
// 0x0000032B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m674BA79F19175ADE919700CE681F299AAF8FFC55 (void);
// 0x0000032C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m81FB02E5750AE48D25B7348735B0FBE06C4FE0AA (void);
// 0x0000032D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m33C699C65EE1B4FC84A6F69351E1C8E2C0A5A6DB (void);
// 0x0000032E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::HandDrag_Start()
extern void MoveObjByEyeGaze_HandDrag_Start_mD3A04FBAB7007BF88778D89FB1F6A51F158AC2E4 (void);
// 0x0000032F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::HandDrag_Stop()
extern void MoveObjByEyeGaze_HandDrag_Stop_m2681D5C0EBBB6CF997F565EDE16808B7F6B2D681 (void);
// 0x00000330 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::IsLookingAwayFromTarget()
extern void MoveObjByEyeGaze_IsLookingAwayFromTarget_m622E3E51CEA34044427CAD693EE1FE8682F4EE12 (void);
// 0x00000331 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::IsLookingAwayFromPreview()
extern void MoveObjByEyeGaze_IsLookingAwayFromPreview_mBAB6326B5BE5313FF34EC1D6F1B71873065C17A0 (void);
// 0x00000332 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::IsDestinationPlausible()
extern void MoveObjByEyeGaze_IsDestinationPlausible_m8CC26A9F21323171CF4E93658451E4F1C0A8173B (void);
// 0x00000333 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::GetValidPlacemLocation(UnityEngine.GameObject)
extern void MoveObjByEyeGaze_GetValidPlacemLocation_m9E6457DBBDBEE091F5BAF0849EF39C26F0FFAA5B (void);
// 0x00000334 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::ActivatePreview()
extern void MoveObjByEyeGaze_ActivatePreview_m667CF698B7713081E3A111B78DAD822D0F0A73CF (void);
// 0x00000335 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::DeactivatePreview()
extern void MoveObjByEyeGaze_DeactivatePreview_m5C4DF9D16DDAFE8A8EC82C9A906D763EB5B9E918 (void);
// 0x00000336 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::DragAndDrop_Start()
extern void MoveObjByEyeGaze_DragAndDrop_Start_m88EDE02D1A9076A1142C4C2D489E1B36914B089A (void);
// 0x00000337 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::DragAndDrop_Finish()
extern void MoveObjByEyeGaze_DragAndDrop_Finish_m8E85507C87DA918B7DC4E78CC9A141546863D632 (void);
// 0x00000338 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::RelativeMoveUpdate(UnityEngine.Vector3)
extern void MoveObjByEyeGaze_RelativeMoveUpdate_mBAE5199A648F488784AFD4CE50F4288AE97144F4 (void);
// 0x00000339 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Angle_InitialGazeToCurrGazeDir()
extern void MoveObjByEyeGaze_Angle_InitialGazeToCurrGazeDir_mF23EE5E7B9CD89DE0EC10D565DBE87F24C665240 (void);
// 0x0000033A System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Angle_ToCurrHitTarget(UnityEngine.GameObject)
extern void MoveObjByEyeGaze_Angle_ToCurrHitTarget_m1B9E32722BDE0569E78F5C0EA3169388E98D828D (void);
// 0x0000033B System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::HeadIsInMotion()
extern void MoveObjByEyeGaze_HeadIsInMotion_m9037DA4115540A814AF3B5EC575601E5D11C9E88 (void);
// 0x0000033C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::MoveTargetBy(UnityEngine.Vector3)
extern void MoveObjByEyeGaze_MoveTargetBy_m8908213B4AAE31C4111832861EB63695EB5F3790 (void);
// 0x0000033D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::UpdateSliderTextOutput()
extern void MoveObjByEyeGaze_UpdateSliderTextOutput_mFDDBB80EEC30810500286A813A0E2FE154EAB5F3 (void);
// 0x0000033E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::ConstrainMovement()
extern void MoveObjByEyeGaze_ConstrainMovement_m3DA7851E020B48428ACE50307479F08B14037F33 (void);
// 0x0000033F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::OnDrop_SnapToClosestDecimal()
extern void MoveObjByEyeGaze_OnDrop_SnapToClosestDecimal_m0C5F70697BD4F1CBFAC4C593C4CC14AB336EDED0 (void);
// 0x00000340 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::MoveTargetTo(UnityEngine.Vector3)
extern void MoveObjByEyeGaze_MoveTargetTo_m228E30AA6B44CDAD0B22E121BD9A4D0AB2629CB3 (void);
// 0x00000341 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::ShouldObjBeWarped(System.Single,System.Single,System.Boolean)
extern void MoveObjByEyeGaze_ShouldObjBeWarped_mC95DBF31981D139CA827E49BDB3735AC1D37F57B (void);
// 0x00000342 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_mCAE262E5B4CAA21620126A508A3D7AE640D6B5E0 (void);
// 0x00000343 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::.ctor()
extern void MoveObjByEyeGaze__ctor_mBD819DCE9B8D5CC138F5382CABE3A200345EC3FA (void);
// 0x00000344 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveObjByEyeGaze::.cctor()
extern void MoveObjByEyeGaze__cctor_m7FB0427D658EE5A0E27EF2B9EAB2EE58D1E199F8 (void);
// 0x00000345 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.SnapTo::.ctor()
extern void SnapTo__ctor_mF4D4880CE5DB9F6DB0ADABF525CA3D10E9F05D16 (void);
// 0x00000346 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TransportToRespawnLocation::OnTriggerEnter(UnityEngine.Collider)
extern void TransportToRespawnLocation_OnTriggerEnter_mA850BAFF9F11345F472E9FE28C3550EED9DFD0D7 (void);
// 0x00000347 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TransportToRespawnLocation::.ctor()
extern void TransportToRespawnLocation__ctor_m5D0DB46A5F515D6884DA71D276ED2110793A1B19 (void);
// 0x00000348 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TriggerZonePlaceObjsWithin::Start()
extern void TriggerZonePlaceObjsWithin_Start_m246E45AB8167036A4B02D465FCAE86A814ED59C5 (void);
// 0x00000349 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TriggerZonePlaceObjsWithin::OnTriggerEnter(UnityEngine.Collider)
extern void TriggerZonePlaceObjsWithin_OnTriggerEnter_m98916B848FF7D1E4085679CDB26987125743F21A (void);
// 0x0000034A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TriggerZonePlaceObjsWithin::OnTriggerExit(UnityEngine.Collider)
extern void TriggerZonePlaceObjsWithin_OnTriggerExit_mD086942CCEC41280C4EFC29EBB7F78253A3DD880 (void);
// 0x0000034B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TriggerZonePlaceObjsWithin::CheckForCompletion()
extern void TriggerZonePlaceObjsWithin_CheckForCompletion_m440150358C1FB7755D603901AA9F78DE4219F1DA (void);
// 0x0000034C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TriggerZonePlaceObjsWithin::TargetDropped()
extern void TriggerZonePlaceObjsWithin_TargetDropped_m859CFD75A06F26401E998D0FB0608CD67FB36DB7 (void);
// 0x0000034D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TriggerZonePlaceObjsWithin::AreWeThereYet()
extern void TriggerZonePlaceObjsWithin_AreWeThereYet_m01BCD8624946ECA4D29571A385B6D28F286D0BB8 (void);
// 0x0000034E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TriggerZonePlaceObjsWithin::.ctor()
extern void TriggerZonePlaceObjsWithin__ctor_m867D05BC3A41D66ABAD1A8E1747ECB2968BF0336 (void);
// 0x0000034F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.HitBehaviorDestroyOnSelect::Start()
extern void HitBehaviorDestroyOnSelect_Start_m4CB517A6B8F87AB4CBEE53D3EC38F517373ED2FC (void);
// 0x00000350 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.HitBehaviorDestroyOnSelect::SetUpAudio()
extern void HitBehaviorDestroyOnSelect_SetUpAudio_m8B0F3E98DCCC6CE0166ECFA4FD1A2CCDA8D114E8 (void);
// 0x00000351 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.HitBehaviorDestroyOnSelect::PlayAudioOnHit(UnityEngine.AudioClip)
extern void HitBehaviorDestroyOnSelect_PlayAudioOnHit_mF75E687F7E41FB5E38368B3EF48845DDC47524F2 (void);
// 0x00000352 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.HitBehaviorDestroyOnSelect::PlayAnimationOnHit()
extern void HitBehaviorDestroyOnSelect_PlayAnimationOnHit_m48C4E431021FF0BC4BE9FC1601CEBC717040F19C (void);
// 0x00000353 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.HitBehaviorDestroyOnSelect::TargetSelected()
extern void HitBehaviorDestroyOnSelect_TargetSelected_mF86B135B771C313788A5AFE96273E3B6EF1A5A6E (void);
// 0x00000354 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.HitBehaviorDestroyOnSelect::HandleTargetGridIterator()
extern void HitBehaviorDestroyOnSelect_HandleTargetGridIterator_mF145FAD82C3A4252205D46B459D942674DA76994 (void);
// 0x00000355 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.HitBehaviorDestroyOnSelect::.ctor()
extern void HitBehaviorDestroyOnSelect__ctor_mE8E52B5245DD6F5675080A0C9194BE2BBC8ED6A2 (void);
// 0x00000356 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.RotateWithConstSpeedDir::RotateTarget()
extern void RotateWithConstSpeedDir_RotateTarget_mD1B5545675EB19EF4F9E8E9D3360CBC880057C34 (void);
// 0x00000357 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.RotateWithConstSpeedDir::.ctor()
extern void RotateWithConstSpeedDir__ctor_m995649A4F8887A7A33AF4BF206AC6699B1D375CD (void);
// 0x00000358 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::Start()
extern void TargetGroupCreatorRadial_Start_m66F301C7C6384FD65D66D96E82D96052EEE66F80 (void);
// 0x00000359 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::Update()
extern void TargetGroupCreatorRadial_Update_m8DD24A9F114F6407CDB2B19A2664A54BE46C5702 (void);
// 0x0000035A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::HideTemplates()
extern void TargetGroupCreatorRadial_HideTemplates_m7087FD60F8818065D450297639DF8FCA195F6641 (void);
// 0x0000035B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::CreateNewTargets_RadialLayout()
extern void TargetGroupCreatorRadial_CreateNewTargets_RadialLayout_m9FF8607025878F9EE0A480BD4188E5FCAC8D6D1F (void);
// 0x0000035C UnityEngine.GameObject[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::get_InstantiatedObjects()
extern void TargetGroupCreatorRadial_get_InstantiatedObjects_mA358E93DF5E75935BD56C3B5F4F63CE50E7BE891 (void);
// 0x0000035D UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::GetRandomTemplate()
extern void TargetGroupCreatorRadial_GetRandomTemplate_mE9D6EB4736337DA29EE58875756BEE0482A83546 (void);
// 0x0000035E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::InstantiateRadialLayoutedTarget(System.Single,System.Single,System.Int32)
extern void TargetGroupCreatorRadial_InstantiateRadialLayoutedTarget_mAD79FC30E20B0C1C78F78103C1365B4D2E418020 (void);
// 0x0000035F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::KeepConstantVisAngleTargetSize()
extern void TargetGroupCreatorRadial_KeepConstantVisAngleTargetSize_m58EE489BD05A461D07AE9101C831C94B19F6FD03 (void);
// 0x00000360 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::KeepFacingTheCamera()
extern void TargetGroupCreatorRadial_KeepFacingTheCamera_mFD42FD736FC11B4EC5521DF1A9A559C1974A194E (void);
// 0x00000361 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupCreatorRadial::.ctor()
extern void TargetGroupCreatorRadial__ctor_mCE2A8B91ACEA15746610603A48856EBCC0F8FBC8 (void);
// 0x00000362 UnityEngine.Color Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::get_HighlightColor()
extern void TargetGroupIterator_get_HighlightColor_m083DDDF4865468A9B19ED3F7A42665B56F68F111 (void);
// 0x00000363 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::add_OnAllTargetsSelected(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler)
extern void TargetGroupIterator_add_OnAllTargetsSelected_m6064AFDC7FD0A4F707C0C0B66CD34F2AA7BF872F (void);
// 0x00000364 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::remove_OnAllTargetsSelected(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler)
extern void TargetGroupIterator_remove_OnAllTargetsSelected_m5AF09D842ECCE703EF50F6A8731AB37D8533AE62 (void);
// 0x00000365 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::add_OnTargetSelected(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler)
extern void TargetGroupIterator_add_OnTargetSelected_mBBDB4D90C8A878BAF5BFCF91388D545305DA2BD9 (void);
// 0x00000366 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::remove_OnTargetSelected(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler)
extern void TargetGroupIterator_remove_OnTargetSelected_m18F381E83CBF9A077ABBAE7A92638A6AB32DED47 (void);
// 0x00000367 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Start()
extern void TargetGroupIterator_Start_m6ACE67059A5021DC332678CEEAED701914A3ABAD (void);
// 0x00000368 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Update()
extern void TargetGroupIterator_Update_mBAF050C57E8B3B85E2F32A5409423B6ABCE487F7 (void);
// 0x00000369 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::ResetAmountOfTries()
extern void TargetGroupIterator_ResetAmountOfTries_m74D6789CD72E33D75E98B9C928E2AF29A586E5C4 (void);
// 0x0000036A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::ResetIterator()
extern void TargetGroupIterator_ResetIterator_m1B5E24F5FBFC313513F96AD730DFE508C0EE103B (void);
// 0x0000036B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::ProceedToNextTarget()
extern void TargetGroupIterator_ProceedToNextTarget_m9FFA11487537EDA6CBE333A8D2E8F1D785ED7389 (void);
// 0x0000036C UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::get_CurrentTarget()
extern void TargetGroupIterator_get_CurrentTarget_m0ED64E1FFC9AC072EB9294E0934D28307E8E8F17 (void);
// 0x0000036D UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::get_PreviousTarget()
extern void TargetGroupIterator_get_PreviousTarget_mE060A8E9C7B1188E30282799D2126356950D6EE6 (void);
// 0x0000036E System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::get_CurrentTargetIsValid()
extern void TargetGroupIterator_get_CurrentTargetIsValid_m759CE7004BFFCDD9128660F9383D78C2D365AFFD (void);
// 0x0000036F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::SaveOriginalColors()
extern void TargetGroupIterator_SaveOriginalColors_m551E7C5E83505E61D02D17D01C7CB4775F4C55F2 (void);
// 0x00000370 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::HighlightTarget()
extern void TargetGroupIterator_HighlightTarget_mF265B61BD7202A40AC23A25B22DB3FCBD20A8443 (void);
// 0x00000371 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::HighlightTarget(UnityEngine.Color)
extern void TargetGroupIterator_HighlightTarget_m41B48287652F6F159FF41DFEF70B72EC3B03CF81 (void);
// 0x00000372 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::ShowVisualMarkerForCurrTarget()
extern void TargetGroupIterator_ShowVisualMarkerForCurrTarget_mA8A75B955180BB6E96F06B7A879B3C1369476274 (void);
// 0x00000373 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::HideVisualMarkerForCurrTarget()
extern void TargetGroupIterator_HideVisualMarkerForCurrTarget_mEAF942A0EF88BB64ADC71D40B93498373006FA41 (void);
// 0x00000374 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::UnhighlightTarget()
extern void TargetGroupIterator_UnhighlightTarget_m31A3A77529D9DE0DE658F6BC8F4DE786E0AE89D9 (void);
// 0x00000375 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::ShowHighlights()
extern void TargetGroupIterator_ShowHighlights_m1E62EE12181E2E91982EDE32F8FCCE77D152512C (void);
// 0x00000376 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::HideHighlights()
extern void TargetGroupIterator_HideHighlights_mA9C0CCDBE2A501818B6584058F7FA8F49CEE6BD0 (void);
// 0x00000377 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Fire_OnAllTargetsSelected()
extern void TargetGroupIterator_Fire_OnAllTargetsSelected_mB6732EC948BABDDB37BDDE03C24FF55EFF44B320 (void);
// 0x00000378 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Fire_OnTargetSelected()
extern void TargetGroupIterator_Fire_OnTargetSelected_m1AF6B9C1FECA4F688473933656F384555AC78D6E (void);
// 0x00000379 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m9213592B0421CED1DFC9C04DB4AE33E45101893F (void);
// 0x0000037A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_mA603FEA3262922686FEA95C132F8EE9FFDE4810E (void);
// 0x0000037B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_mFCA335518E11A82D84E9DB94AB162C3AD105DE2D (void);
// 0x0000037C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m070C70433317092CBC96732BAD2CFA4E86DCD5E3 (void);
// 0x0000037D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator::.ctor()
extern void TargetGroupIterator__ctor_m8E0F974EEE05CDB77907A9A4BED3B55D64B25A21 (void);
// 0x0000037E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler::.ctor(System.Object,System.IntPtr)
extern void TargetGroupEventHandler__ctor_m3F732709E7AD11567FFDE713803ABA5A97A834D0 (void);
// 0x0000037F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler::Invoke()
extern void TargetGroupEventHandler_Invoke_m85E1C36EE9D152613F51B9C16940A215FF3996C0 (void);
// 0x00000380 System.IAsyncResult Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler::BeginInvoke(System.AsyncCallback,System.Object)
extern void TargetGroupEventHandler_BeginInvoke_m930A1ED6EEB1D8E246FC74E887523ACD5F723B99 (void);
// 0x00000381 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetGroupIterator/TargetGroupEventHandler::EndInvoke(System.IAsyncResult)
extern void TargetGroupEventHandler_EndInvoke_m4A8862A53841A96DC3B1656910229E8DE8B6B3E3 (void);
// 0x00000382 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ToggleGameObject::ShowIt()
extern void ToggleGameObject_ShowIt_m16D765A39E1A3692D3DBC9F9CA5E6F5998FD548D (void);
// 0x00000383 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ToggleGameObject::HideIt()
extern void ToggleGameObject_HideIt_m1068655D1624272B6270B1ED2D5017FEFBBE0044 (void);
// 0x00000384 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ToggleGameObject::ShowIt(System.Boolean)
extern void ToggleGameObject_ShowIt_m9741EAA78D6170C2197DF5889D3AFBA9058817F4 (void);
// 0x00000385 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ToggleGameObject::.ctor()
extern void ToggleGameObject__ctor_m50C0ABFA8FAA7F3EFBF8E31D411A8023EDDBEB65 (void);
// 0x00000386 Microsoft.MixedReality.Toolkit.Input.EyeTrackingTarget Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::get_EyeTarget()
extern void DrawOnTexture_get_EyeTarget_mF9CAE5599F22C24F49F145274A482BF6BC88925B (void);
// 0x00000387 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::Start()
extern void DrawOnTexture_Start_m905A5F2E0BED0A32B40801512AA24322BC75EA60 (void);
// 0x00000388 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::OnLookAt()
extern void DrawOnTexture_OnLookAt_mD7D300F123F51E805B4BC8A508E84E4274A38E70 (void);
// 0x00000389 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::DrawAtThisHitPos(UnityEngine.Vector3)
extern void DrawOnTexture_DrawAtThisHitPos_m675E2F1AA0DF5153F9AE4CF6D19472386A924C91 (void);
// 0x0000038A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::DrawAt(UnityEngine.Vector2,UnityEngine.Color)
extern void DrawOnTexture_DrawAt_mA6096EE3937FA04B497C2257FEBA1F667C0FEB34 (void);
// 0x0000038B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::DrawAt2(UnityEngine.Vector2,System.Int32,System.Single)
extern void DrawOnTexture_DrawAt2_mE0142B77FAFDFDEBAEF22E784E230236A43B81EE (void);
// 0x0000038C System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::DrawAt(UnityEngine.Vector2)
extern void DrawOnTexture_DrawAt_m5CCF53A676F5EF6B8E413C510A70B1ABD8EB42DD (void);
// 0x0000038D System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::ComputeHeatmapAt(UnityEngine.Vector2,System.Boolean,System.Boolean)
extern void DrawOnTexture_ComputeHeatmapAt_m4234A80A7ECCD004A0549561897E1FDCBF25898E (void);
// 0x0000038E System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::ComputeHeatmapColorAt(UnityEngine.Vector2,UnityEngine.Vector2,System.Nullable`1<UnityEngine.Color>&)
extern void DrawOnTexture_ComputeHeatmapColorAt_mAFDE4C406957E36412932A3CBEEC244D00391029 (void);
// 0x0000038F UnityEngine.Renderer Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::get_MyRenderer()
extern void DrawOnTexture_get_MyRenderer_mF322108589FA4083A7FBB87425225BA316A7531D (void);
// 0x00000390 UnityEngine.Texture2D Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::get_MyDrawTexture()
extern void DrawOnTexture_get_MyDrawTexture_m6B6D53DFB9F2D728ADB8C349C3E026093CA58886 (void);
// 0x00000391 System.Nullable`1<UnityEngine.Vector2> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::GetCursorPosInTexture(UnityEngine.Vector3)
extern void DrawOnTexture_GetCursorPosInTexture_m071AF778967026980B47777140A88241247A4ED9 (void);
// 0x00000392 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture::.ctor()
extern void DrawOnTexture__ctor_m0D39FD2D2B1D765EDA45F381CBC222A29E279F34 (void);
// 0x00000393 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<DrawAt>d__19::.ctor(System.Int32)
extern void U3CDrawAtU3Ed__19__ctor_m0669E9BEA6952B3F3DD94D06CBB8ABE617E39B24 (void);
// 0x00000394 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<DrawAt>d__19::System.IDisposable.Dispose()
extern void U3CDrawAtU3Ed__19_System_IDisposable_Dispose_m6E4D0EBCBAE804E9BCE39A65E328D04E34213B52 (void);
// 0x00000395 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<DrawAt>d__19::MoveNext()
extern void U3CDrawAtU3Ed__19_MoveNext_mB2543CCB3D3BF2C12A793FF9FFF206AB49B7038E (void);
// 0x00000396 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<DrawAt>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDrawAtU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB8FE5A3C65743AF09977A975BCC7D237891A637 (void);
// 0x00000397 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<DrawAt>d__19::System.Collections.IEnumerator.Reset()
extern void U3CDrawAtU3Ed__19_System_Collections_IEnumerator_Reset_m928AB931B01F942ECA5D6B39A97E2DDB4B316A66 (void);
// 0x00000398 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<DrawAt>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CDrawAtU3Ed__19_System_Collections_IEnumerator_get_Current_mCF5A234BE92720D5D3143BAA4F6FA6A2E7C5C7C2 (void);
// 0x00000399 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<ComputeHeatmapAt>d__20::.ctor(System.Int32)
extern void U3CComputeHeatmapAtU3Ed__20__ctor_mB446BCBDF6A86972E2CCA7C4D41D2BC187574100 (void);
// 0x0000039A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<ComputeHeatmapAt>d__20::System.IDisposable.Dispose()
extern void U3CComputeHeatmapAtU3Ed__20_System_IDisposable_Dispose_m037E1BB2B14C2C41D8DC547D74349C2DB0BE76FA (void);
// 0x0000039B System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<ComputeHeatmapAt>d__20::MoveNext()
extern void U3CComputeHeatmapAtU3Ed__20_MoveNext_mF905BCF702462540B727726EB149DD6DBB3D695E (void);
// 0x0000039C System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<ComputeHeatmapAt>d__20::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CComputeHeatmapAtU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01119328E1F744FF691CE5485CC2BADFC8766CC2 (void);
// 0x0000039D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<ComputeHeatmapAt>d__20::System.Collections.IEnumerator.Reset()
extern void U3CComputeHeatmapAtU3Ed__20_System_Collections_IEnumerator_Reset_mAF888F7D9CB6C49243C9B4D75E73BBB14A77D906 (void);
// 0x0000039E System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DrawOnTexture/<ComputeHeatmapAt>d__20::System.Collections.IEnumerator.get_Current()
extern void U3CComputeHeatmapAtU3Ed__20_System_Collections_IEnumerator_get_Current_mA0A6E63023754E4F80F25FB667CE8E119770D05A (void);
// 0x0000039F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnSelectVisualizerInputController::Awake()
extern void OnSelectVisualizerInputController_Awake_mCF057F60CBE20DBFEBA3B3FB85C3FF3E0E793B31 (void);
// 0x000003A0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnSelectVisualizerInputController::OnTargetSelected()
extern void OnSelectVisualizerInputController_OnTargetSelected_m4A080DC91A1C2046FB3B771CAB925F7C253720AB (void);
// 0x000003A1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnSelectVisualizerInputController::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m04437D31608C0815803E06B0F2F6AA608E03E6A3 (void);
// 0x000003A2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnSelectVisualizerInputController::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m386597D97037AC73236C5BFB3DAB5C2CC038E8D0 (void);
// 0x000003A3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnSelectVisualizerInputController::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m3C274542CE438C15D38CFBD1908CC7A85C466DCC (void);
// 0x000003A4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnSelectVisualizerInputController::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_mAB7BDF579FE2FAFA7F6357464FB96F6CA2E4412B (void);
// 0x000003A5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnSelectVisualizerInputController::.ctor()
extern void OnSelectVisualizerInputController__ctor_mE6025F87B108706D374A32810E2F3ACA8E273E1E (void);
// 0x000003A6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::Start()
extern void ParticleHeatmap_Start_m93A043CAAD81FBD2B433792F8A290DDAE4A59106 (void);
// 0x000003A7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::SetParticle(UnityEngine.Vector3)
extern void ParticleHeatmap_SetParticle_m3D5AB90D8727A3F3D67A3A30F9C1E892D10C8E01 (void);
// 0x000003A8 System.Nullable`1<UnityEngine.Vector3> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::GetPositionOfParticle(System.Int32)
extern void ParticleHeatmap_GetPositionOfParticle_m4F62616C5737EAFB1EB5124DF8DEA1682E0C56BA (void);
// 0x000003A9 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::DetermineNormalizedIntensity(UnityEngine.Vector3,System.Single)
extern void ParticleHeatmap_DetermineNormalizedIntensity_m63D6E0AD36B9FD32C896768D7860C0F4D568EA68 (void);
// 0x000003AA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::UpdateColorForAllParticles()
extern void ParticleHeatmap_UpdateColorForAllParticles_mC7F07C6A43CE1C908135AE7B31A92CDE217863DA (void);
// 0x000003AB System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::saturate(System.Single)
extern void ParticleHeatmap_saturate_mC50AE74CFB227532D21466D1B015426331CFB418 (void);
// 0x000003AC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::DisplayParticles()
extern void ParticleHeatmap_DisplayParticles_m794364EFDB05C334CB4D9144FB573EA03B8B71A9 (void);
// 0x000003AD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::ShowHeatmap()
extern void ParticleHeatmap_ShowHeatmap_m0D81B1B4D45E6A2A16F60C13E1621A85913FF50A (void);
// 0x000003AE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::HideHeatmap()
extern void ParticleHeatmap_HideHeatmap_m6584A67A3DD1F7DEB94045A4B9633A51A5E16121 (void);
// 0x000003AF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap::.ctor()
extern void ParticleHeatmap__ctor_m740DB1EBF60A2432EE5A3DEA0827F6E645958E2D (void);
// 0x000003B0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmapParticleData::.ctor()
extern void ParticleHeatmapParticleData__ctor_mAA7D86C0881A9CEC23CF85D0E066505DF476EF01 (void);
// 0x000003B1 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer::get_Instance()
extern void AudioFeedbackPlayer_get_Instance_m714F2501DD250566ABEF9269938D4F9FEFDBF577 (void);
// 0x000003B2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer::set_Instance(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer)
extern void AudioFeedbackPlayer_set_Instance_m3C0D461B9B45905B85975D30EFB5DCAACACAC802 (void);
// 0x000003B3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer::Start()
extern void AudioFeedbackPlayer_Start_m3F861DC14FFFEE6C1CD80D372815E621CFE3B66D (void);
// 0x000003B4 UnityEngine.AudioSource Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer::SetupAudioSource(UnityEngine.GameObject)
extern void AudioFeedbackPlayer_SetupAudioSource_m1BF539B2278642BD4E2387360A159758D0460FD0 (void);
// 0x000003B5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer::PlaySound(UnityEngine.AudioClip)
extern void AudioFeedbackPlayer_PlaySound_m41CF59BFA0DF554B49CBD599ABC73A24B795FC9F (void);
// 0x000003B6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.AudioFeedbackPlayer::.ctor()
extern void AudioFeedbackPlayer__ctor_mD99CFEC83CAC9BA797BFEDB7D14185DD1724AC00 (void);
// 0x000003B7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FollowEyeGaze::Update()
extern void FollowEyeGaze_Update_mEBA4DBFAAE8F0F71FFA529F1EBF006791AA1AFD4 (void);
// 0x000003B8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FollowEyeGaze::.ctor()
extern void FollowEyeGaze__ctor_m79A0A2BAE81BFAE8B9F7F812CC6A5D43FD9397FF (void);
// 0x000003B9 UnityEngine.TextMesh Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.SpeechVisualFeedback::get_MyTextMesh()
extern void SpeechVisualFeedback_get_MyTextMesh_mA5BA5DA178D1165693194CE8769A2B8EABC71EC4 (void);
// 0x000003BA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.SpeechVisualFeedback::UpdateTextMesh(System.String)
extern void SpeechVisualFeedback_UpdateTextMesh_mC96EA13ED47E143D248AD06E4F1E1963A9822D8C (void);
// 0x000003BB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.SpeechVisualFeedback::ShowVisualFeedback(System.String)
extern void SpeechVisualFeedback_ShowVisualFeedback_m579D646436BF229B9080874D0077E451B433F38C (void);
// 0x000003BC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.SpeechVisualFeedback::Update()
extern void SpeechVisualFeedback_Update_mC26B26E519A2BC1241606008CE937EF5EEFB8BBD (void);
// 0x000003BD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.SpeechVisualFeedback::Microsoft.MixedReality.Toolkit.Input.IMixedRealitySpeechHandler.OnSpeechKeywordRecognized(Microsoft.MixedReality.Toolkit.Input.SpeechEventData)
extern void SpeechVisualFeedback_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySpeechHandler_OnSpeechKeywordRecognized_m3FDD5FF7FACC4255A7155EEFA7E53AEF648899A9 (void);
// 0x000003BE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.SpeechVisualFeedback::.ctor()
extern void SpeechVisualFeedback__ctor_m18571039E9FBDDFF98EACF883BC863CDC4D25566 (void);
// 0x000003BF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::Start()
extern void BlendOut_Start_m241515FB431B398AEED3C7FC2BDF4323CBFA9233 (void);
// 0x000003C0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::InitialSetup()
extern void BlendOut_InitialSetup_mF3AC41E6E11EE886D098A87D3BF2B826F2235487 (void);
// 0x000003C1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::Engage()
extern void BlendOut_Engage_m8F412B4D08313E477687DD3C6628135FDD97E50D (void);
// 0x000003C2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::Disengage()
extern void BlendOut_Disengage_m2767FCB7CC2493E82AB1435E37460E63312D1A54 (void);
// 0x000003C3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::DwellSucceeded()
extern void BlendOut_DwellSucceeded_mD0282E1D6E921B7DDD06CED85CE20748D3D034E6 (void);
// 0x000003C4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::Update()
extern void BlendOut_Update_m7D8A18B94B8C977C8721903E412232F98723AF1A (void);
// 0x000003C5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::SlowlyBlendOut()
extern void BlendOut_SlowlyBlendOut_m97616D3FC5021B30ECA72A62121BB6036FE24063 (void);
// 0x000003C6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::ChangeTransparency(System.Single)
extern void BlendOut_ChangeTransparency_m6C8883E832053D97CDE401CCCCE9B1288396EA0F (void);
// 0x000003C7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::ChangeTransparency(System.Single,System.String)
extern void BlendOut_ChangeTransparency_mE8007D95762537707ED181DF1BEDBE329F9F7C5E (void);
// 0x000003C8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::Materials_BlendOut(UnityEngine.Material[],System.Single,System.String)
extern void BlendOut_Materials_BlendOut_mDEB0306A1AD110807A36B8D0F6614960AFD07926 (void);
// 0x000003C9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.BlendOut::.ctor()
extern void BlendOut__ctor_mB15101D9050670C620FCAA8E8E22391F6DAB000B (void);
// 0x000003CA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::Start()
extern void ChangeSize_Start_m18155EBAB41D111EC6354ACACF646614EDB0ACF0 (void);
// 0x000003CB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::Engage()
extern void ChangeSize_Engage_m3FF1769A92E5B4C43E4F160E8122B1D80C62AFDF (void);
// 0x000003CC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::Disengage()
extern void ChangeSize_Disengage_m6A97E4D4201E4B7896895E2A206A089797411C76 (void);
// 0x000003CD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::InitialSetup()
extern void ChangeSize_InitialSetup_m1BCC0277A8DBBCD7D92BB77D2F45EA955CC07A75 (void);
// 0x000003CE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::Update()
extern void ChangeSize_Update_m8333EAE80A526D567E7EF138CF0C931822D48FAF (void);
// 0x000003CF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::OnLookAt_IncreaseTargetSize()
extern void ChangeSize_OnLookAt_IncreaseTargetSize_m40265EB1254E63C1494CB4D7D1A89A180A0E3AF0 (void);
// 0x000003D0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::OnLookAway_ReturnToOriginalTargetSize()
extern void ChangeSize_OnLookAway_ReturnToOriginalTargetSize_m2D68AA274997CC4B3F23B5900482F4AC33422AAE (void);
// 0x000003D1 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::AreWeThereYet(UnityEngine.Vector3)
extern void ChangeSize_AreWeThereYet_m5EE4E70D7F62D95E9B44CE84B2F4660348478D38 (void);
// 0x000003D2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeSize::.ctor()
extern void ChangeSize__ctor_mCB5C8E44FBDDADD50970A11E6BF698C606E8DF66 (void);
// 0x000003D3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::Start()
extern void FaceUser_Start_m8E335136FD670B14CC70441CF45879BDD42E77E4 (void);
// 0x000003D4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::InitialSetup()
extern void FaceUser_InitialSetup_m4A48630823629C6A1D8640788BD051920BD32850 (void);
// 0x000003D5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::Update()
extern void FaceUser_Update_m9B22F17D7FC9B88CEB23DB9D84F907F1AAED470F (void);
// 0x000003D6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::TurnToUser(UnityEngine.Vector3,UnityEngine.Vector3)
extern void FaceUser_TurnToUser_mE3F6D108819EB310D0CD91F3521034DCD67900BE (void);
// 0x000003D7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::ReturnToOriginalRotation(UnityEngine.Vector3,UnityEngine.Vector3)
extern void FaceUser_ReturnToOriginalRotation_m8B4B8C4A70CD01DB9005AA90624D25F616D3927F (void);
// 0x000003D8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::Engage()
extern void FaceUser_Engage_mBC678ED36B8D1ECFB99C74E345C319A40F2440CC (void);
// 0x000003D9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::Disengage()
extern void FaceUser_Disengage_m53808BBD1D4CEB25EE80BDCC2B2376D7F37F30D0 (void);
// 0x000003DA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.FaceUser::.ctor()
extern void FaceUser__ctor_m9FB1100FC8BC7455C34C91587C053625035FC83D (void);
// 0x000003DB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepFacingCamera::Awake()
extern void KeepFacingCamera_Awake_m2F44505EF068A0CC6BBA1C2A7776E4464B71EC9B (void);
// 0x000003DC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepFacingCamera::Start()
extern void KeepFacingCamera_Start_m5C7C2CAA92807C442272D00547283C22ED09767C (void);
// 0x000003DD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepFacingCamera::Update()
extern void KeepFacingCamera_Update_m6FAEDD89AEB88911435A094D72C9A21CA3FD6EE8 (void);
// 0x000003DE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepFacingCamera::.ctor()
extern void KeepFacingCamera__ctor_mDE9E1A8CD37235BB5FECA6B754DC0409EA1E0EB7 (void);
// 0x000003DF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene::LoadScene()
extern void LoadAdditiveScene_LoadScene_mD0E07D6C03ECD2D6F104F61FFDC3A31B2BDDD624 (void);
// 0x000003E0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene::LoadScene(System.String)
extern void LoadAdditiveScene_LoadScene_mEFE9FC059E9A875E326EB76C2B38051CBFC00135 (void);
// 0x000003E1 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene::LoadNewScene(System.String)
extern void LoadAdditiveScene_LoadNewScene_m8369E583996CD92D5D211F38E219CDAA014E43FE (void);
// 0x000003E2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene::.ctor()
extern void LoadAdditiveScene__ctor_mA5E6B5B24C523261916D49F98A0444766D23428F (void);
// 0x000003E3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene::.cctor()
extern void LoadAdditiveScene__cctor_mADAD19F88CA77993C2A9A031A40829A87D8A291C (void);
// 0x000003E4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene/<LoadNewScene>d__6::.ctor(System.Int32)
extern void U3CLoadNewSceneU3Ed__6__ctor_mAAA7D7B2D2A941AB339DDE43B676DA5EB7C3F25A (void);
// 0x000003E5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene/<LoadNewScene>d__6::System.IDisposable.Dispose()
extern void U3CLoadNewSceneU3Ed__6_System_IDisposable_Dispose_mAC49FC02DB74158443E728F2B937460842721A3B (void);
// 0x000003E6 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene/<LoadNewScene>d__6::MoveNext()
extern void U3CLoadNewSceneU3Ed__6_MoveNext_m93C69EFE0389088D33ADFF071445E4E7FDA19CA2 (void);
// 0x000003E7 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene/<LoadNewScene>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadNewSceneU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9CF41C95C56DA29A06DC641A6B10B958B14D43D3 (void);
// 0x000003E8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene/<LoadNewScene>d__6::System.Collections.IEnumerator.Reset()
extern void U3CLoadNewSceneU3Ed__6_System_Collections_IEnumerator_Reset_m1F4F5F43E8C334FD2178951548219825F2978BA6 (void);
// 0x000003E9 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.LoadAdditiveScene/<LoadNewScene>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CLoadNewSceneU3Ed__6_System_Collections_IEnumerator_get_Current_m7D090F16DFB2D3B4CFB78A558428242744FEF519 (void);
// 0x000003EA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveWithCamera::Update()
extern void MoveWithCamera_Update_mFF19AC0F6FB0A882B3E13B0DABC565093498514C (void);
// 0x000003EB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.MoveWithCamera::.ctor()
extern void MoveWithCamera__ctor_m6FF81EC64D652E5ECAF80EDBDD722D0CBA27E08D (void);
// 0x000003EC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLoadStartScene::Start()
extern void OnLoadStartScene_Start_m09F7D0D4AE5E8B1BF0B19E5755795848FAD88CDC (void);
// 0x000003ED System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLoadStartScene::LoadOnDevice()
extern void OnLoadStartScene_LoadOnDevice_m95428AA284DEA8058CECDFC1AD17E9AF22153C3E (void);
// 0x000003EE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLoadStartScene::LoadNewScene()
extern void OnLoadStartScene_LoadNewScene_m79AE41CB5137BE74B0B5F3AAD74F724DBBCAFF3C (void);
// 0x000003EF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLoadStartScene::.ctor()
extern void OnLoadStartScene__ctor_mD6D116B5C7DA8558CFF1BD17777DB8EBD1338747 (void);
// 0x000003F0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLookAtRotateByEyeGaze::OnEyeFocusStay()
extern void OnLookAtRotateByEyeGaze_OnEyeFocusStay_m49A0360C7A86BF18977B595F9194CAAAF9EE1245 (void);
// 0x000003F1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLookAtRotateByEyeGaze::RotateHitTarget()
extern void OnLookAtRotateByEyeGaze_RotateHitTarget_mC14CB1AFB8C50D6B04A67E727B792EDD59DA99F9 (void);
// 0x000003F2 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLookAtRotateByEyeGaze::ClampAngleInDegree(System.Single,System.Single,System.Single)
extern void OnLookAtRotateByEyeGaze_ClampAngleInDegree_m06322835FFEC2E6565E9DB7B511C57A61A054565 (void);
// 0x000003F3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.OnLookAtRotateByEyeGaze::.ctor()
extern void OnLookAtRotateByEyeGaze__ctor_mA3C99F72CC060205765337FED5DC28F537C73D49 (void);
// 0x000003F4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::Start()
extern void DwellSelection_Start_m0200E21183FE3DE2C71759D8F329F8A8E01EF631 (void);
// 0x000003F5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::EtTarget_OnTargetSelected(System.Object,Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetEventArgs)
extern void DwellSelection_EtTarget_OnTargetSelected_mFD1B1EF9BA42027E744345ACB7AB0BE1E4331CC1 (void);
// 0x000003F6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::EnableDwell()
extern void DwellSelection_EnableDwell_m5846798A9E8A5CD6A9B48418F0B8B8866BB69834 (void);
// 0x000003F7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::DisableDwell()
extern void DwellSelection_DisableDwell_m3F412C4FDD4E1B15A4C0077286E0690B18D98C4A (void);
// 0x000003F8 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::get_UseDwell()
extern void DwellSelection_get_UseDwell_mE993FF99112A7523B8CF81CFFD5E6954602CF928 (void);
// 0x000003F9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::OnEyeFocusStart()
extern void DwellSelection_OnEyeFocusStart_mDB8D7DBB9BA1F373A7123FC2EF2E0D499B3EAEB9 (void);
// 0x000003FA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::OnEyeFocusStay()
extern void DwellSelection_OnEyeFocusStay_m1C7D029AA6D1A126DB560A6D6CCBA5474866B0E2 (void);
// 0x000003FB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::OnEyeFocusStop()
extern void DwellSelection_OnEyeFocusStop_m35E6FDE67D05AF53FA96C1873FFC6C3009769577 (void);
// 0x000003FC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::StartDwellFeedback()
extern void DwellSelection_StartDwellFeedback_m720C36A5468801956420C40D55A857FD656D7553 (void);
// 0x000003FD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::ResetDwellFeedback()
extern void DwellSelection_ResetDwellFeedback_m46C6C947DCA9A095ECE5A07C47B6A222E6842EC3 (void);
// 0x000003FE UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::get_SpeedSizeChangePerSecond()
extern void DwellSelection_get_SpeedSizeChangePerSecond_m486279D730F882EBBCD2C2244885882ADB9B2C4A (void);
// 0x000003FF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::Update()
extern void DwellSelection_Update_m731C4250937110A1757D34036B99059FAEFAAA3D (void);
// 0x00000400 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::ClampVector3(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void DwellSelection_ClampVector3_m5A438F1FCD0E8E9F7CED6428AD415C9A8AC2ABAD (void);
// 0x00000401 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::UpdateTransparency(System.Single)
extern void DwellSelection_UpdateTransparency_mF3E939B57E5376755188CA757D8F531B87528631 (void);
// 0x00000402 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::LerpTransparency(System.Single,System.Single,System.Single)
extern void DwellSelection_LerpTransparency_mF6BD0EFC6C0CD666181B0673F9DD6092B0042EDD (void);
// 0x00000403 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m71BCDC31185A913ED89388D566A3B0A2FA343374 (void);
// 0x00000404 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_mB6B665B0334B21BD0C6FA24A37556639114F121D (void);
// 0x00000405 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m874E3926315D791602EAEA793ED5F3CF037707F1 (void);
// 0x00000406 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_mAA5B2AB1C789D5C8906C5B06BD51C1AAB26F30BA (void);
// 0x00000407 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::get_TextureShaderProperty()
extern void DwellSelection_get_TextureShaderProperty_m6D319C321495A6F25F988FD4905C9BE6DFCE0578 (void);
// 0x00000408 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::set_TextureShaderProperty(System.String)
extern void DwellSelection_set_TextureShaderProperty_m70DC993A731F4EF1283B62208C46AFC71FDFA538 (void);
// 0x00000409 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::.ctor()
extern void DwellSelection__ctor_m979DBD4A6D9528F4CCEA4F9E0DF11D8AD051CCC1 (void);
// 0x0000040A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DwellSelection::.cctor()
extern void DwellSelection__cctor_m551FCFFE9641A3046BD43AEEEFC3451F65A67C91 (void);
// 0x0000040B Microsoft.MixedReality.Toolkit.Input.EyeTrackingTarget Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetEventArgs::get_HitTarget()
extern void TargetEventArgs_get_HitTarget_m8388656A38F227CCD16F8BCF83A22047ECFDA41B (void);
// 0x0000040C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetEventArgs::set_HitTarget(Microsoft.MixedReality.Toolkit.Input.EyeTrackingTarget)
extern void TargetEventArgs_set_HitTarget_mC1AE547E25147BE85A735C663443ABEFDE11B4F0 (void);
// 0x0000040D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.TargetEventArgs::.ctor(Microsoft.MixedReality.Toolkit.Input.EyeTrackingTarget)
extern void TargetEventArgs__ctor_mD30806FADBFFE5CC12D78C23BF965C01CB6195FB (void);
// 0x0000040E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeRenderMode::ChangeRenderModes(UnityEngine.Material,Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ChangeRenderMode/BlendMode)
extern void ChangeRenderMode_ChangeRenderModes_m4D2AD991BB7A67A4B3E95B0F58818D34CE12F5A4 (void);
// 0x0000040F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DoNotRender::Start()
extern void DoNotRender_Start_m4259CCE2CDBBA1D052F3D89C0C3B45DAA85B7D48 (void);
// 0x00000410 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.DoNotRender::.ctor()
extern void DoNotRender__ctor_m896C686D87E71F51EA7CC846DE075DEAC7E3B218 (void);
// 0x00000411 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeCalibrationChecker::Update()
extern void EyeCalibrationChecker_Update_mB417F03C9A5AE2018BC8E21C6688DDFB5AA9B293 (void);
// 0x00000412 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeCalibrationChecker::.ctor()
extern void EyeCalibrationChecker__ctor_m2062E826890171E2174E63DCAFA68DEB96B1D1F2 (void);
// 0x00000413 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepThisAlive Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepThisAlive::get_Instance()
extern void KeepThisAlive_get_Instance_m6E3E6968684933E52E1A47F41083AC48FF464465 (void);
// 0x00000414 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepThisAlive::set_Instance(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepThisAlive)
extern void KeepThisAlive_set_Instance_mA2A10EE2DD42672547E38F84945744FB35AF8640 (void);
// 0x00000415 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepThisAlive::Awake()
extern void KeepThisAlive_Awake_mC82B2BAB9D5DDE853437891859A52049ED4E157A (void);
// 0x00000416 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepThisAlive::Start()
extern void KeepThisAlive_Start_m2DE87B65FFA56E0F7814687114DCEC08CF8C76C4 (void);
// 0x00000417 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.KeepThisAlive::.ctor()
extern void KeepThisAlive__ctor_m0401E6CD503FDAE3C620F9C5D8A7C1BBF5644FCB (void);
// 0x00000418 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.StatusText Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.StatusText::get_Instance()
extern void StatusText_get_Instance_mA5D6F917BF8C0CE492E402048665A16E91171762 (void);
// 0x00000419 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.StatusText::Awake()
extern void StatusText_Awake_m1A1B8FC2DA65C9F4B6519C321D7C2579E02E7128 (void);
// 0x0000041A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.StatusText::Start()
extern void StatusText_Start_m18A92E43D8562E33453B15C2BB65F0B798143481 (void);
// 0x0000041B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.StatusText::Log(System.String,System.Boolean)
extern void StatusText_Log_m760562A30E2863BC5E8B8A0E40BB56B89691DBDC (void);
// 0x0000041C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.StatusText::.ctor()
extern void StatusText__ctor_mB2FEC1698B210DBE1A2CFA635A75D491C4F04341 (void);
// 0x0000041D System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::GetValidFilename(System.String)
extern void EyeTrackingDemoUtils_GetValidFilename_m3533AC1EFB6CD3A8C0B570923F341E28B4319610 (void);
// 0x0000041E System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::GetFullName(UnityEngine.GameObject)
extern void EyeTrackingDemoUtils_GetFullName_mC347D6F086625BD180DDBB42BF2DEF754C1F6639 (void);
// 0x0000041F System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::GetFullName(UnityEngine.GameObject,System.Boolean&)
extern void EyeTrackingDemoUtils_GetFullName_m80D9ACDF0541ADA14FBBA0A92FEC90553AC1B733 (void);
// 0x00000420 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::Normalize(System.Single,System.Single,System.Single)
extern void EyeTrackingDemoUtils_Normalize_mCADC77F16D1F9C624B9851FCA0D10E2D2C630B87 (void);
// 0x00000421 T[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::RandomizeListOrder(T[])
// 0x00000422 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::VisAngleInDegreesToMeters(UnityEngine.Vector3,System.Single)
extern void EyeTrackingDemoUtils_VisAngleInDegreesToMeters_m6D8D82BA4FAF6F068ACA474A906920F0480A8266 (void);
// 0x00000423 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::VisAngleInDegreesToMeters(System.Single,System.Single)
extern void EyeTrackingDemoUtils_VisAngleInDegreesToMeters_mDE4DD9A3C3F1622F8647E044EE101E3F7AFA39B2 (void);
// 0x00000424 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::LoadNewScene(System.String)
extern void EyeTrackingDemoUtils_LoadNewScene_mBC2C038E7D63C54B409884FC2841ECB88F1670D6 (void);
// 0x00000425 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::LoadNewScene(System.String,System.Single)
extern void EyeTrackingDemoUtils_LoadNewScene_mDDC18CE3E1DB98A62D89FB6C89E485D1B68BF4F2 (void);
// 0x00000426 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::GameObject_ChangeColor(UnityEngine.GameObject,UnityEngine.Color,System.Nullable`1<UnityEngine.Color>&,System.Boolean)
extern void EyeTrackingDemoUtils_GameObject_ChangeColor_m7CC5CE3076D62FC81F1544584E3D63F7C1786DDB (void);
// 0x00000427 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::GameObject_ChangeTransparency(UnityEngine.GameObject,System.Single)
extern void EyeTrackingDemoUtils_GameObject_ChangeTransparency_m1421C17BB27D71E7806437FF09ED61FA569594D6 (void);
// 0x00000428 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::GameObject_ChangeTransparency(UnityEngine.GameObject,System.Single,System.Single&)
extern void EyeTrackingDemoUtils_GameObject_ChangeTransparency_m6E8BBA1A73EFE378E6A6BFA428436FE099F1B6E6 (void);
// 0x00000429 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils::Renderers_ChangeTransparency(UnityEngine.Renderer[],System.Single,System.Single&)
extern void EyeTrackingDemoUtils_Renderers_ChangeTransparency_mFFB2985F54D2BB8B801C4C6D9579F783E160E94D (void);
// 0x0000042A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils/<LoadNewScene>d__8::.ctor(System.Int32)
extern void U3CLoadNewSceneU3Ed__8__ctor_m401003DDD285C5623BEB280831A31A466D16A964 (void);
// 0x0000042B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils/<LoadNewScene>d__8::System.IDisposable.Dispose()
extern void U3CLoadNewSceneU3Ed__8_System_IDisposable_Dispose_mD06F424BF364A4726F11674C65C0CA9EEEAFE2CC (void);
// 0x0000042C System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils/<LoadNewScene>d__8::MoveNext()
extern void U3CLoadNewSceneU3Ed__8_MoveNext_mD0328A163C898776ECE5545895012768D4200123 (void);
// 0x0000042D System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils/<LoadNewScene>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadNewSceneU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6D49FF64BD8C8DE4A6EA2EC600BD12FF3390B71E (void);
// 0x0000042E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils/<LoadNewScene>d__8::System.Collections.IEnumerator.Reset()
extern void U3CLoadNewSceneU3Ed__8_System_Collections_IEnumerator_Reset_mC163ABF19EDAEDE1B729059AFF32EC00D9760CED (void);
// 0x0000042F System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.EyeTrackingDemoUtils/<LoadNewScene>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CLoadNewSceneU3Ed__8_System_Collections_IEnumerator_get_Current_m4E0C826DFFCFD5E5854D74C1DDB3B2F680731F1C (void);
// 0x00000430 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::Start()
extern void OnLookAtShowHoverFeedback_Start_mC42027FB29BC07500E307896156AE8795841259E (void);
// 0x00000431 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::Update()
extern void OnLookAtShowHoverFeedback_Update_m1A5F0E4755D242A50C7629D127CFBA4DD3952C33 (void);
// 0x00000432 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::OnDestroy()
extern void OnLookAtShowHoverFeedback_OnDestroy_m577A5F6EFC5D1EEB21BD5455B397923604288EFD (void);
// 0x00000433 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::NormalizedInterest_Dwell()
extern void OnLookAtShowHoverFeedback_NormalizedInterest_Dwell_m46248903D6C203574487FBB8382E78BF9046BDAC (void);
// 0x00000434 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::NormalizedDisinterest_LookAway()
extern void OnLookAtShowHoverFeedback_NormalizedDisinterest_LookAway_m77B04187C69C5A13AD985F6471A7D4736AF7FBA6 (void);
// 0x00000435 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::DestroyLocalFeedback()
extern void OnLookAtShowHoverFeedback_DestroyLocalFeedback_m9FC2B5CCD3E7F4909D6F68ECF03BD8577DB83462 (void);
// 0x00000436 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::ShowFeedback(System.Single)
extern void OnLookAtShowHoverFeedback_ShowFeedback_m2DD737C913EBB583F9153F819087EDA6206B8C59 (void);
// 0x00000437 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::ShowFeedback_Overlay(System.Single)
extern void OnLookAtShowHoverFeedback_ShowFeedback_Overlay_mDFAA0EB93210CCE81059144FB9E00166426EFDDB (void);
// 0x00000438 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::ShowFeedback_Highlight(System.Single)
extern void OnLookAtShowHoverFeedback_ShowFeedback_Highlight_mE49C9BE174E75FF25B96E210937278D069539423 (void);
// 0x00000439 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::TransitionAdjustedInterest(System.Single)
extern void OnLookAtShowHoverFeedback_TransitionAdjustedInterest_m09E7EF77F3E93CE57A2FD7028273D856F7B0FE7E (void);
// 0x0000043A System.Double Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::get_LookAwayTimeInMs()
extern void OnLookAtShowHoverFeedback_get_LookAwayTimeInMs_m84A4D873B727DB725D7F918606F39396B2C2231F (void);
// 0x0000043B System.Double Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::get_DwellTimeInMs()
extern void OnLookAtShowHoverFeedback_get_DwellTimeInMs_mDFE0E399D6DF539D5F8496A8BA85F9C82FBA6896 (void);
// 0x0000043C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::OnLookAtStop()
extern void OnLookAtShowHoverFeedback_OnLookAtStop_m82B20E68E701C0C3260600B3BAE960081F3DCFF9 (void);
// 0x0000043D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::OnLookAtStart()
extern void OnLookAtShowHoverFeedback_OnLookAtStart_mABA332BFAE039BD27008EEFEC9718FB9C6713465 (void);
// 0x0000043E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::SaveOriginalColor()
extern void OnLookAtShowHoverFeedback_SaveOriginalColor_mF179D58C8ACBAB4A3A0015184325502708292733 (void);
// 0x0000043F UnityEngine.Color[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::GetColorsByProperty(System.String,UnityEngine.Renderer[])
extern void OnLookAtShowHoverFeedback_GetColorsByProperty_mE7331C9E98EA872097A2C3F9840B953508AE65A8 (void);
// 0x00000440 UnityEngine.Color Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::BlendColors(UnityEngine.Color,UnityEngine.Color,System.Single)
extern void OnLookAtShowHoverFeedback_BlendColors_mA5CE600AC46E4A874C554E769F364033010BDEFC (void);
// 0x00000441 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::divBlendColor(System.Single,System.Single,System.Single)
extern void OnLookAtShowHoverFeedback_divBlendColor_m779E436E8A28F563BDAF47CABC769A9F3C6C539A (void);
// 0x00000442 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Targeting.OnLookAtShowHoverFeedback::.ctor()
extern void OnLookAtShowHoverFeedback__ctor_mA6B6AF6424D4E92C1B97C684B1401687704DD917 (void);
// 0x00000443 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers::RunSync(System.Func`1<System.Threading.Tasks.Task>)
extern void AsyncHelpers_RunSync_m7656CD620C0DB02D30F397EF3D446A10BCE5D0AA (void);
// 0x00000444 T Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers::RunSync(System.Func`1<System.Threading.Tasks.Task`1<T>>)
// 0x00000445 System.Exception Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::get_InnerException()
extern void ExclusiveSynchronizationContext_get_InnerException_m4409BF574355A708E7AD7B501A23394DC8194BDB (void);
// 0x00000446 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::set_InnerException(System.Exception)
extern void ExclusiveSynchronizationContext_set_InnerException_m8E8500331FAEEC67FA3687E1AB92B8B6AFCCE153 (void);
// 0x00000447 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::Send(System.Threading.SendOrPostCallback,System.Object)
extern void ExclusiveSynchronizationContext_Send_m848930E6C929ACF2DDD1EBD9B82A09D49894B72C (void);
// 0x00000448 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::Post(System.Threading.SendOrPostCallback,System.Object)
extern void ExclusiveSynchronizationContext_Post_m2884588D960FF0873E27192A6CE01CE070BD0573 (void);
// 0x00000449 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::EndMessageLoop()
extern void ExclusiveSynchronizationContext_EndMessageLoop_mB29443F8BDEFB6445EDF3818E7A588000FFCCDCF (void);
// 0x0000044A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::BeginMessageLoop()
extern void ExclusiveSynchronizationContext_BeginMessageLoop_m6AC60C2B4FA9E4CD7841A99CD11CE5ACEA75FA78 (void);
// 0x0000044B System.Threading.SynchronizationContext Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::CreateCopy()
extern void ExclusiveSynchronizationContext_CreateCopy_mFECF393B1AB4ECA7D64E12B476B9B1C2C9B8C17E (void);
// 0x0000044C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::.ctor()
extern void ExclusiveSynchronizationContext__ctor_m4523ADFE91001ACA98CD8E30463279800357064A (void);
// 0x0000044D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/ExclusiveSynchronizationContext::<EndMessageLoop>b__9_0(System.Object)
extern void ExclusiveSynchronizationContext_U3CEndMessageLoopU3Eb__9_0_m2269F5C62E97D5684D03C180C278A730562819C4 (void);
// 0x0000044E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass0_0::.ctor()
extern void U3CU3Ec__DisplayClass0_0__ctor_m0A40F6B82AE3D3383693F8ED2D10A4C4B8311A58 (void);
// 0x0000044F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass0_0::<RunSync>b__0(System.Object)
extern void U3CU3Ec__DisplayClass0_0_U3CRunSyncU3Eb__0_mE0B82500FB231395120B4EA8482680957CFEE75A (void);
// 0x00000450 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass0_0/<<RunSync>b__0>d::.ctor()
extern void U3CU3CRunSyncU3Eb__0U3Ed__ctor_m3B2E40C1737968F6C28F3AD3506D9C38291688F5 (void);
// 0x00000451 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass0_0/<<RunSync>b__0>d::MoveNext()
extern void U3CU3CRunSyncU3Eb__0U3Ed_MoveNext_m7B8B6DD784CAE08A72B3207DE2F30821A1FB1C3F (void);
// 0x00000452 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass0_0/<<RunSync>b__0>d::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CU3CRunSyncU3Eb__0U3Ed_SetStateMachine_m9CD2295E60C2CDDFF44C6A2CA4ACF079D62B4BE0 (void);
// 0x00000453 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass1_0`1::.ctor()
// 0x00000454 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass1_0`1::<RunSync>b__0(System.Object)
// 0x00000455 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass1_0`1/<<RunSync>b__0>d::.ctor()
// 0x00000456 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass1_0`1/<<RunSync>b__0>d::MoveNext()
// 0x00000457 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.AsyncHelpers/<>c__DisplayClass1_0`1/<<RunSync>b__0>d::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
// 0x00000458 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::SetUserName(System.String)
extern void BasicInputLogger_SetUserName_m9EFC6D8F9A3DAFAD2211A9F1A235FD8F316B1211 (void);
// 0x00000459 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::SetSessionDescr(System.String)
extern void BasicInputLogger_SetSessionDescr_m0E9F6709FC1D035596131B8689AD7D4991387AB4 (void);
// 0x0000045A System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::get_LogDirectory()
extern void BasicInputLogger_get_LogDirectory_m62691878EE1DDB0F42D4BD25210D30DDCE453B7C (void);
// 0x0000045B System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::GetHeader()
// 0x0000045C System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::GetFileName()
// 0x0000045D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::CreateNewLogFile()
extern void BasicInputLogger_CreateNewLogFile_mA8AB2F74DDF248A632C2C07FED6A9F121C5F24AC (void);
// 0x0000045E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::CheckIfInitialized()
extern void BasicInputLogger_CheckIfInitialized_m172E2A10D56544A492F36379390236E48B831C3E (void);
// 0x0000045F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::ResetLog()
extern void BasicInputLogger_ResetLog_mCC6887B2578E0CBD5750A67B6A46AB223897B82F (void);
// 0x00000460 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::get_FormattedTimeStamp()
extern void BasicInputLogger_get_FormattedTimeStamp_m42275F6D37795B25A14B1BE2BE7B17E5A493216A (void);
// 0x00000461 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::AddLeadingZeroToSingleDigitIntegers(System.Int32)
extern void BasicInputLogger_AddLeadingZeroToSingleDigitIntegers_m9B1111BAF1BEF8FE36CC28B5543681230F494222 (void);
// 0x00000462 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::Append(System.String)
extern void BasicInputLogger_Append_m3A6FB5522D93EFD6FE9685BC66500B8044AEB794 (void);
// 0x00000463 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::LoadLogs()
extern void BasicInputLogger_LoadLogs_m7F0D6AA20D8D240CD2A26D43CB0604DD4A59B853 (void);
// 0x00000464 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::SaveLogs()
extern void BasicInputLogger_SaveLogs_m8E427165D7149A23C324257EC8E8F0C2637FF3B1 (void);
// 0x00000465 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::get_Filename()
extern void BasicInputLogger_get_Filename_m54CD3CA10F35462CBBE4C5D11539C9E519F4213D (void);
// 0x00000466 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::get_FilenameWithTimestamp()
extern void BasicInputLogger_get_FilenameWithTimestamp_m4C0355C86CED8B6474BA48248016118FBEC950B7 (void);
// 0x00000467 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::get_FilenameNoTimestamp()
extern void BasicInputLogger_get_FilenameNoTimestamp_m0690087D1AF9E7083D02C147CE23205B806D5644 (void);
// 0x00000468 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::OnDestroy()
extern void BasicInputLogger_OnDestroy_m07C4CB956DF8AA78518E01565763D1F260E3855C (void);
// 0x00000469 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger::.ctor()
extern void BasicInputLogger__ctor_mCDED1AC979CBD78FB0DFD0729DCC9A0A5C5F818F (void);
// 0x0000046A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger/<CreateNewLogFile>d__14::.ctor()
extern void U3CCreateNewLogFileU3Ed__14__ctor_mEEA0AE3041B0D8CBF1E387428C278A32A14B0B46 (void);
// 0x0000046B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger/<CreateNewLogFile>d__14::MoveNext()
extern void U3CCreateNewLogFileU3Ed__14_MoveNext_m4F75BD58B788E80CFA742F24516CC6A08A688754 (void);
// 0x0000046C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger/<CreateNewLogFile>d__14::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCreateNewLogFileU3Ed__14_SetStateMachine_m6F238B4288F208E800147D5C1CEABA0527113273 (void);
// 0x0000046D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger/<LoadLogs>d__21::.ctor()
extern void U3CLoadLogsU3Ed__21__ctor_mC815507963FE629C202476F38526CE536A4E6F1B (void);
// 0x0000046E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger/<LoadLogs>d__21::MoveNext()
extern void U3CLoadLogsU3Ed__21_MoveNext_m3D4C327E67AA2C6D410647FEA422BD364DEF58F6 (void);
// 0x0000046F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger/<LoadLogs>d__21::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CLoadLogsU3Ed__21_SetStateMachine_m2E9C43D81C1E46818BC390C7402EB5F366A44F4D (void);
// 0x00000470 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger/<SaveLogs>d__22::.ctor()
extern void U3CSaveLogsU3Ed__22__ctor_mA15CEEFB6A4D7658B6B70791D33DD1AE04FECA53 (void);
// 0x00000471 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger/<SaveLogs>d__22::MoveNext()
extern void U3CSaveLogsU3Ed__22_MoveNext_mA6BFA52E1838A7BA82E8C945CE50B45C24077B8C (void);
// 0x00000472 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.BasicInputLogger/<SaveLogs>d__22::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSaveLogsU3Ed__22_SetStateMachine_m61A95B0BF84898DFF17E30CC4BDF6756503A7F54 (void);
// 0x00000473 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.CustomInputLogger::CustomAppend(System.String)
extern void CustomInputLogger_CustomAppend_m352B0C93623E93DE35BB70F87B3338E235470642 (void);
// 0x00000474 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.CustomInputLogger::CreateNewLog()
extern void CustomInputLogger_CreateNewLog_m63E7EE113D0FBBAAEE7D94009AEEC3F3B7156C52 (void);
// 0x00000475 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.CustomInputLogger::StartLogging()
extern void CustomInputLogger_StartLogging_m4A87A8548790322E40D6182CC67C97F149B12F20 (void);
// 0x00000476 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.CustomInputLogger::StopLoggingAndSave()
extern void CustomInputLogger_StopLoggingAndSave_mEE6746F9E54C8C0970B93E9AA1F8A9C82CAE177D (void);
// 0x00000477 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.CustomInputLogger::CancelLogging()
extern void CustomInputLogger_CancelLogging_m497154BAAD16D304FC9FE404760C6DC87C14EB98 (void);
// 0x00000478 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.CustomInputLogger::.ctor()
extern void CustomInputLogger__ctor_m9E289B363B74750CE12BA52C05AF7E9BE89C720E (void);
// 0x00000479 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::Start()
extern void InputPointerVisualizer_Start_m3F5E035F824ADE5837B07AF329D9B056FE40AB2C (void);
// 0x0000047A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::ResetVisualizations()
extern void InputPointerVisualizer_ResetVisualizations_mFC52CAE9A4A737DAEAE5292B555C55A226188D67 (void);
// 0x0000047B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::InitPointClouds(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap&,Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap,System.Int32)
extern void InputPointerVisualizer_InitPointClouds_mC0DE9CA29AD734A012A4FDDD615B86434F2408E6 (void);
// 0x0000047C System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::InitVisArrayObj(UnityEngine.GameObject[]&,UnityEngine.GameObject,System.Int32)
extern void InputPointerVisualizer_InitVisArrayObj_m05D28602A09E52891E583D2D5A12DFC441F2BEAC (void);
// 0x0000047D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::ResetVis()
extern void InputPointerVisualizer_ResetVis_mFC891DFE40B2E6E4ED1A35C5CBCE7C1307962055 (void);
// 0x0000047E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::ResetVis(UnityEngine.GameObject[]&)
extern void InputPointerVisualizer_ResetVis_m4C3D7EC1ED8149ACA9F37D6A4F2ED3F3C57FA1C3 (void);
// 0x0000047F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::ResetPointCloudVis(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap&)
extern void InputPointerVisualizer_ResetPointCloudVis_m049688BD070C6AB22FEF60535864820E36332093 (void);
// 0x00000480 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::SetActive_DataVis(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer/VisModes)
extern void InputPointerVisualizer_SetActive_DataVis_m5959A5480E78EB05321B0A54403A6839ED077047 (void);
// 0x00000481 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::SetActive_DataVis(System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Boolean)
extern void InputPointerVisualizer_SetActive_DataVis_mCF2E22079F2AF8F475E44B431713A29C15B71DA8 (void);
// 0x00000482 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::SetActive_DataVis(UnityEngine.GameObject[]&,System.Boolean)
extern void InputPointerVisualizer_SetActive_DataVis_m642C606192EED483F50E983C744FBA6F41FAC5C1 (void);
// 0x00000483 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::SetActive_PointCloudVis(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap&,System.Boolean)
extern void InputPointerVisualizer_SetActive_PointCloudVis_mB52B80C7BC62A86B701073CD1D8AC8E0820FE654 (void);
// 0x00000484 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::UpdateDataVisPos(UnityEngine.GameObject[]&,System.Int32,UnityEngine.Vector3,System.Boolean)
extern void InputPointerVisualizer_UpdateDataVisPos_mA8537AB95250ECA716BA5896A7E3F9FE7D8E52D4 (void);
// 0x00000485 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::UpdateVis_PointCloud(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.ParticleHeatmap&,System.Int32,UnityEngine.Vector3,System.Boolean)
extern void InputPointerVisualizer_UpdateVis_PointCloud_mBD51E9CDE7421319696381608D0C1F05D538D60E (void);
// 0x00000486 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::UpdateConnectorLines(UnityEngine.GameObject[]&,System.Int32,UnityEngine.Vector3,UnityEngine.Vector3,System.Boolean)
extern void InputPointerVisualizer_UpdateConnectorLines_m532AB04C8FDD62BB3130D0298F831BBEF20ED6C1 (void);
// 0x00000487 System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::GetPrevLineIndex(System.Int32,System.Int32)
extern void InputPointerVisualizer_GetPrevLineIndex_m841105FA299FBC913AEC839261945F01CD52BC0C (void);
// 0x00000488 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::UpdateDataVis(UnityEngine.Ray)
extern void InputPointerVisualizer_UpdateDataVis_mA4B538EE155C8A648BDB7BDD6F0587D00BBCB950 (void);
// 0x00000489 System.Nullable`1<UnityEngine.Vector3> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::PerformHitTest(UnityEngine.Ray)
extern void InputPointerVisualizer_PerformHitTest_m2878332499224112CBF61C1D7BE7F04B6D1787E0 (void);
// 0x0000048A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::Update()
extern void InputPointerVisualizer_Update_mBE331944C6BF5EC9884493D226930361F093D405 (void);
// 0x0000048B System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::IsDwelling()
extern void InputPointerVisualizer_IsDwelling_mA106B66A3754E0AB5073270E316C6E9530FBDD6E (void);
// 0x0000048C System.Int32 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::get_AmountOfSamples()
extern void InputPointerVisualizer_get_AmountOfSamples_mD0A66CF4E74338190B56F943BB74495E7A54FA9C (void);
// 0x0000048D System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::set_AmountOfSamples(System.Int32)
extern void InputPointerVisualizer_set_AmountOfSamples_m66184A93726408C5B1D0CFFED187390124C65054 (void);
// 0x0000048E System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::ToggleAppState()
extern void InputPointerVisualizer_ToggleAppState_mEA16DA03BCA71DC2F7306C609A25E361409C199A (void);
// 0x0000048F System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::PauseApp()
extern void InputPointerVisualizer_PauseApp_m1E04903F7D4ABA7B10A835C34016E3726192C404 (void);
// 0x00000490 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::UnpauseApp()
extern void InputPointerVisualizer_UnpauseApp_m1CBF057069470A9311E09323C896982FB1769CE9 (void);
// 0x00000491 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::SetAppState(System.Boolean)
extern void InputPointerVisualizer_SetAppState_m4587663C118D7B47B141383792876066D04CD379 (void);
// 0x00000492 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer::.ctor()
extern void InputPointerVisualizer__ctor_mCDC4F3097CDE7321A664BAA65C9E900104A44E36 (void);
// 0x00000493 System.String[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.LogStructure::GetHeaderColumns()
extern void LogStructure_GetHeaderColumns_mF74FD887B53B3F7587AB7BC8A6DE19553C516057 (void);
// 0x00000494 System.Object[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.LogStructure::GetData(System.String,System.String,Microsoft.MixedReality.Toolkit.Input.EyeTrackingTarget)
extern void LogStructure_GetData_mA862E5C33AA9646D4F0DC373CFC8A947392EBDE4 (void);
// 0x00000495 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.LogStructure::.ctor()
extern void LogStructure__ctor_m379E801EE8F6A2613EEFE9A600A9DFB03F084911 (void);
// 0x00000496 Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeGazeProvider Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.LogStructureEyeGaze::get_EyeTrackingProvider()
extern void LogStructureEyeGaze_get_EyeTrackingProvider_mD464DCFAEE72299E10969C9409E5742765332CDA (void);
// 0x00000497 System.String[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.LogStructureEyeGaze::GetHeaderColumns()
extern void LogStructureEyeGaze_GetHeaderColumns_m6A8EFF0419A69B84F7CEFD3951D14390B9F7E679 (void);
// 0x00000498 System.Object[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.LogStructureEyeGaze::GetData(System.String,System.String,Microsoft.MixedReality.Toolkit.Input.EyeTrackingTarget)
extern void LogStructureEyeGaze_GetData_m124FC286EDD57662E1B8A5714A09222FF9B88627 (void);
// 0x00000499 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.LogStructureEyeGaze::.ctor()
extern void LogStructureEyeGaze__ctor_m4D6F72C20BFD24B78E800B808D604D85CBE16185 (void);
// 0x0000049A System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::Start()
extern void UserInputPlayback_Start_m51DA7CD6D117F2E488D3C99986C772549C485FEE (void);
// 0x0000049B System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::ResetCurrentStream()
extern void UserInputPlayback_ResetCurrentStream_mFE25FF476B17E3140379E901FB5CF7F70CBCD668 (void);
// 0x0000049C System.Threading.Tasks.Task`1<System.Boolean> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UWP_Load()
extern void UserInputPlayback_UWP_Load_m5788A70494218FE373050086B3654F96B15AAA46 (void);
// 0x0000049D System.Threading.Tasks.Task`1<System.Boolean> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UWP_LoadNewFile(System.String)
extern void UserInputPlayback_UWP_LoadNewFile_mF348B60237E4F7DCEA10BC345BAEC56B9513FDAE (void);
// 0x0000049E System.Threading.Tasks.Task`1<System.Boolean> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UWP_FileExists(System.String,System.String)
extern void UserInputPlayback_UWP_FileExists_m418F422637A601D32632A7EDD1A33DEB9FB624E0 (void);
// 0x0000049F System.Threading.Tasks.Task`1<System.Boolean> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UWP_ReadData(Windows.Storage.StorageFile)
extern void UserInputPlayback_UWP_ReadData_m67177F6AB330EE92B655AF4C87F16D7AC8A85E50 (void);
// 0x000004A0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::LoadNewFile(System.String)
extern void UserInputPlayback_LoadNewFile_m358C527E0A59B7CE0E384B7D67BB0A0AE0FA8E46 (void);
// 0x000004A1 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::TryParseStringToVector3(System.String,System.String,System.String,System.Boolean&)
extern void UserInputPlayback_TryParseStringToVector3_mFEC60C385896F4BD479F78BDDD27B55782ABB1B0 (void);
// 0x000004A2 System.Single Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::ParseStringToFloat(System.String)
extern void UserInputPlayback_ParseStringToFloat_m6F1986BA3151C23D06A30FCC6E0ABB147F3EAF47 (void);
// 0x000004A3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::Load()
extern void UserInputPlayback_Load_mC4E52E21FD1C341EA58995BAB4D19A0070D6182D (void);
// 0x000004A4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::LoadInEditor()
extern void UserInputPlayback_LoadInEditor_m43D44D6603B9775B231E1F428A100CF505819617 (void);
// 0x000004A5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::LoadInUWP()
extern void UserInputPlayback_LoadInUWP_m64594642768FF5FE2AB13AA978790AEF6420CB65 (void);
// 0x000004A6 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::get_FileName()
extern void UserInputPlayback_get_FileName_mC534CFE3D8A7127BA62D2C5E9D9A16448E93CBBA (void);
// 0x000004A7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::set_IsPlaying(System.Boolean)
extern void UserInputPlayback_set_IsPlaying_mCBC869EE68F90686CAD123225EEA43AD43205C64 (void);
// 0x000004A8 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::get_IsPlaying()
extern void UserInputPlayback_get_IsPlaying_mEC33D4DE5699A80B1228F6515DEBB43DE45C7A91 (void);
// 0x000004A9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::Play()
extern void UserInputPlayback_Play_m8FC518FECA8D6E792E2F24D1BB4BABFC4CB747CE (void);
// 0x000004AA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::Pause()
extern void UserInputPlayback_Pause_m977CE7D0C36366177DBFDABAB528F8765211AD82 (void);
// 0x000004AB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::Clear()
extern void UserInputPlayback_Clear_m02D5D1110CFB864EA63543270502D0CB84166344 (void);
// 0x000004AC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::SpeedUp()
extern void UserInputPlayback_SpeedUp_mF525595B4DA89F5ED9DFF7E330808FE4E73C1FCE (void);
// 0x000004AD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::SlowDown()
extern void UserInputPlayback_SlowDown_m06514F5B367060497E63F4188084739FF4848E3C (void);
// 0x000004AE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::ShowAllAndFreeze()
extern void UserInputPlayback_ShowAllAndFreeze_m68C9145E245E10521D38262F74B2B554A04C4257 (void);
// 0x000004AF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::ShowAllAndFreeze(Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer,Microsoft.MixedReality.Toolkit.Input.InputSourceType)
extern void UserInputPlayback_ShowAllAndFreeze_m540AD6F91B1D042AF3D5ED121DE15E3CE4C30AF5 (void);
// 0x000004B0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::ShowHeatmap()
extern void UserInputPlayback_ShowHeatmap_mB8A7D1570E1AEC920DB6FE951C33FCD63301887F (void);
// 0x000004B1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::LoadingStatus_Hide()
extern void UserInputPlayback_LoadingStatus_Hide_mECA81B64C2437E0C42BB45D31D4EDFA223DA3427 (void);
// 0x000004B2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::LoadingStatus_Show()
extern void UserInputPlayback_LoadingStatus_Show_m31CBED33A38E6BA8E9C84401F5EAFCEBB05319D7 (void);
// 0x000004B3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UpdateLoadingStatus(System.Int32,System.Int32)
extern void UserInputPlayback_UpdateLoadingStatus_mFE56924D2B5A6A07C9A360A4FD08A121203DE257 (void);
// 0x000004B4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::Log(System.String)
extern void UserInputPlayback_Log_m555134D76149170AFEBE17A6E5B2143D02CF22AA (void);
// 0x000004B5 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::PopulateHeatmap()
extern void UserInputPlayback_PopulateHeatmap_m12AFD4C59AE8898EFB84E27AA69D1C526324ECB8 (void);
// 0x000004B6 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UpdateStatus(System.Single)
extern void UserInputPlayback_UpdateStatus_mD6B38FDE0F453849A87799311B028E23B7DB175B (void);
// 0x000004B7 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::AddToCounter(System.Single)
extern void UserInputPlayback_AddToCounter_m41084542E3C5DCC962EB9929A40BF5E61922298D (void);
// 0x000004B8 System.Nullable`1<UnityEngine.Ray> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::GetEyeRay(System.String[])
extern void UserInputPlayback_GetEyeRay_mC28F82A17D108935AE20E543E5C0EE75DBF6FB1B (void);
// 0x000004B9 System.Nullable`1<UnityEngine.Ray> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::GetRay(System.String,System.String,System.String,System.String,System.String,System.String)
extern void UserInputPlayback_GetRay_m0E409269280797FD566D4C165D6DD0C15E1543F9 (void);
// 0x000004BA System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::get_DataIsLoaded()
extern void UserInputPlayback_get_DataIsLoaded_m27E61A989410CE611E4251F7ACA5495FFF0972D4 (void);
// 0x000004BB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UpdateTimestampForNextReplay(System.String[])
extern void UserInputPlayback_UpdateTimestampForNextReplay_mF29A1ADE0183804B63587F89C192398E9BB8A0C1 (void);
// 0x000004BC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UpdateEyeGazeSignal(System.String[],Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer)
extern void UserInputPlayback_UpdateEyeGazeSignal_m857663D1C7CF1A10885BAA8AA382F7979511823B (void);
// 0x000004BD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UpdateHeadGazeSignal(System.String[],Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer)
extern void UserInputPlayback_UpdateHeadGazeSignal_m4BD59F502198C90F9EC5581057CB6D7709A686F1 (void);
// 0x000004BE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::UpdateTargetingSignal(System.String,System.String,System.String,System.String,System.String,System.String,UnityEngine.Ray,Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.InputPointerVisualizer)
extern void UserInputPlayback_UpdateTargetingSignal_m9DA09BE081CDDFA6E9C9C3D37E12394FDF905DCF (void);
// 0x000004BF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::Update()
extern void UserInputPlayback_Update_mD152E1FBACA6E44859E96A9709487EF98CBF9D2D (void);
// 0x000004C0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::PlayNext()
extern void UserInputPlayback_PlayNext_m95254BFC4B059A6F70F1042030C0213FEF52EEDC (void);
// 0x000004C1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::.ctor()
extern void UserInputPlayback__ctor_m60A9DD1036BEAA556A5751CB498DAC2C63C22495 (void);
// 0x000004C2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::.cctor()
extern void UserInputPlayback__cctor_m21101850924FB6B6E9CB5D707C00F0E9B2E2CBEA (void);
// 0x000004C3 System.Threading.Tasks.Task`1<System.Boolean> Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback::<ShowAllAndFreeze>b__34_0()
extern void UserInputPlayback_U3CShowAllAndFreezeU3Eb__34_0_mDF45375F98D731AB4729A0CA2E208E5B88EE62C2 (void);
// 0x000004C4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_Load>d__12::.ctor()
extern void U3CUWP_LoadU3Ed__12__ctor_m591A2015586AFB486910D5ECDB00185FBF2E9150 (void);
// 0x000004C5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_Load>d__12::MoveNext()
extern void U3CUWP_LoadU3Ed__12_MoveNext_m077A66752D48B5D767DED22742A3D135C0211FB3 (void);
// 0x000004C6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_Load>d__12::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CUWP_LoadU3Ed__12_SetStateMachine_m7ED88E33A30408B900C9A7A3C35A4E02BFC5683E (void);
// 0x000004C7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_LoadNewFile>d__13::.ctor()
extern void U3CUWP_LoadNewFileU3Ed__13__ctor_mC8454DF517F5F15CD6FBE069DC6E686C166A182C (void);
// 0x000004C8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_LoadNewFile>d__13::MoveNext()
extern void U3CUWP_LoadNewFileU3Ed__13_MoveNext_m0839C0504BC89E3A5FA8492B5B10ED67A68689FB (void);
// 0x000004C9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_LoadNewFile>d__13::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CUWP_LoadNewFileU3Ed__13_SetStateMachine_m22C14D0803818A1A528BDA6D6179361CE22C7233 (void);
// 0x000004CA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_FileExists>d__14::.ctor()
extern void U3CUWP_FileExistsU3Ed__14__ctor_m6A3F94CFFED0ABCC8D8D81A8B1B5958A9046AFCD (void);
// 0x000004CB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_FileExists>d__14::MoveNext()
extern void U3CUWP_FileExistsU3Ed__14_MoveNext_mAD5711484E8F38EC1F51E475060E1D78EB279E74 (void);
// 0x000004CC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_FileExists>d__14::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CUWP_FileExistsU3Ed__14_SetStateMachine_m3A2B9C0F394A24619019FE3C65461944E89E98DD (void);
// 0x000004CD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_ReadData>d__15::.ctor()
extern void U3CUWP_ReadDataU3Ed__15__ctor_m87B05716339B712314C0A6557B10C9EF2F05FDB5 (void);
// 0x000004CE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_ReadData>d__15::MoveNext()
extern void U3CUWP_ReadDataU3Ed__15_MoveNext_m9E20CA73029D89308F6E58413D241125964F6B01 (void);
// 0x000004CF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UWP_ReadData>d__15::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CUWP_ReadDataU3Ed__15_SetStateMachine_mADF140A5B17E52067F652E349087443412A6D68C (void);
// 0x000004D0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<LoadInUWP>d__21::.ctor()
extern void U3CLoadInUWPU3Ed__21__ctor_mCAF86BBCF11030B57FF92BB58954A6595252FD0C (void);
// 0x000004D1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<LoadInUWP>d__21::MoveNext()
extern void U3CLoadInUWPU3Ed__21_MoveNext_m41DF334615BFEDDE4965A3AB46C847FB025147E2 (void);
// 0x000004D2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<LoadInUWP>d__21::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CLoadInUWPU3Ed__21_SetStateMachine_m8FFD898942DB58C8CC1D604F4286C93A5FF9E428 (void);
// 0x000004D3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<PopulateHeatmap>d__42::.ctor(System.Int32)
extern void U3CPopulateHeatmapU3Ed__42__ctor_m102DF160804926625E30794716CD29865EC82DD9 (void);
// 0x000004D4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<PopulateHeatmap>d__42::System.IDisposable.Dispose()
extern void U3CPopulateHeatmapU3Ed__42_System_IDisposable_Dispose_m1097B7B94EB08BA3EF350FE660AD7B9956863392 (void);
// 0x000004D5 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<PopulateHeatmap>d__42::MoveNext()
extern void U3CPopulateHeatmapU3Ed__42_MoveNext_m630D8415D254C155DCB064D523A0D7DB332CC972 (void);
// 0x000004D6 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<PopulateHeatmap>d__42::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CPopulateHeatmapU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0976BC78E7B504C287CB571F0DE0B71DF8413CC6 (void);
// 0x000004D7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<PopulateHeatmap>d__42::System.Collections.IEnumerator.Reset()
extern void U3CPopulateHeatmapU3Ed__42_System_Collections_IEnumerator_Reset_m83A7B4025FE7ECAD0E352634B658870920480462 (void);
// 0x000004D8 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<PopulateHeatmap>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CPopulateHeatmapU3Ed__42_System_Collections_IEnumerator_get_Current_m661273649CF08D4B0CE3715EA1ACE2A6F8530DD3 (void);
// 0x000004D9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UpdateStatus>d__43::.ctor(System.Int32)
extern void U3CUpdateStatusU3Ed__43__ctor_m970C1666C59016AEFE7E3CB5257A6E95BA8A2B91 (void);
// 0x000004DA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UpdateStatus>d__43::System.IDisposable.Dispose()
extern void U3CUpdateStatusU3Ed__43_System_IDisposable_Dispose_m2BF8311A1FA147B50BAA705D688E487697456AFB (void);
// 0x000004DB System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UpdateStatus>d__43::MoveNext()
extern void U3CUpdateStatusU3Ed__43_MoveNext_m2CA8D758330DE6C0D48EB7B44B2E849F2F1ADEFF (void);
// 0x000004DC System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UpdateStatus>d__43::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateStatusU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE88D22C380772911600CEDC6B6C20DECF74174DC (void);
// 0x000004DD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UpdateStatus>d__43::System.Collections.IEnumerator.Reset()
extern void U3CUpdateStatusU3Ed__43_System_Collections_IEnumerator_Reset_mA92CE780FB948B861106556A7D441579F0E52951 (void);
// 0x000004DE System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<UpdateStatus>d__43::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateStatusU3Ed__43_System_Collections_IEnumerator_get_Current_m4E0F683876005E30B881179D5A5255E3BC229525 (void);
// 0x000004DF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<AddToCounter>d__44::.ctor(System.Int32)
extern void U3CAddToCounterU3Ed__44__ctor_m6FB296DA9BFA0BA393DAD6F0B6CC62FE117DEA6B (void);
// 0x000004E0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<AddToCounter>d__44::System.IDisposable.Dispose()
extern void U3CAddToCounterU3Ed__44_System_IDisposable_Dispose_mDCE82C6860FA2A2FF993B0E52EFD549B701BBAE9 (void);
// 0x000004E1 System.Boolean Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<AddToCounter>d__44::MoveNext()
extern void U3CAddToCounterU3Ed__44_MoveNext_m1B18E7D4FD16FC3F9B72724FDDC696B10F704E61 (void);
// 0x000004E2 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<AddToCounter>d__44::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAddToCounterU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA8B93ED764B147113D8B736B485AE53DFD39DB52 (void);
// 0x000004E3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<AddToCounter>d__44::System.Collections.IEnumerator.Reset()
extern void U3CAddToCounterU3Ed__44_System_Collections_IEnumerator_Reset_m331A758A780FF4767AF4D233962A09D5E9D11954 (void);
// 0x000004E4 System.Object Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputPlayback/<AddToCounter>d__44::System.Collections.IEnumerator.get_Current()
extern void U3CAddToCounterU3Ed__44_System_Collections_IEnumerator_get_Current_mA947986D577DD4062D7BFC7DBA9A0D6608A0CADC (void);
// 0x000004E5 Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::get_Instance()
extern void UserInputRecorder_get_Instance_m06936372DDBAE9401846AF06970E2DB4BFB42AC4 (void);
// 0x000004E6 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::GetHeader()
extern void UserInputRecorder_GetHeader_m306D653D5A260615A44732DD9E18C2F0A23B454F (void);
// 0x000004E7 System.Object[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::GetData_Part1()
extern void UserInputRecorder_GetData_Part1_mC89E9E2407D04916974DE65F9B344AD616CA58E4 (void);
// 0x000004E8 System.Object[] Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::MergeObjArrays(System.Object[],System.Object[])
extern void UserInputRecorder_MergeObjArrays_m432BF1067846943B995EE7D7D0E7BEE2BBF397CC (void);
// 0x000004E9 System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::GetFileName()
extern void UserInputRecorder_GetFileName_mD3965ED6EB325AB3B7667C3FA95BC70DDA9E3F85 (void);
// 0x000004EA System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::LimitStringLength(System.String,System.Int32)
extern void UserInputRecorder_LimitStringLength_m663BD16DD8C88C5C0B5CC8DD7FF7D195CBB56558 (void);
// 0x000004EB System.String Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::GetStringFormat(System.Object[])
extern void UserInputRecorder_GetStringFormat_m91B13F46B2F229A7190F4DEEBE83208E72CABC31 (void);
// 0x000004EC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::UpdateLog(System.String,System.String,Microsoft.MixedReality.Toolkit.Input.EyeTrackingTarget)
extern void UserInputRecorder_UpdateLog_m1890778B90ECB9E3157DBA5545CF4BE64F939707 (void);
// 0x000004ED System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::CustomAppend(System.String)
extern void UserInputRecorder_CustomAppend_m9F53F1A9873FB51A8604DCA68C6F370A23994122 (void);
// 0x000004EE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::UpdateLog()
extern void UserInputRecorder_UpdateLog_m85FC6427E796D99B30378633B5E0A3939046E9A6 (void);
// 0x000004EF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::Update()
extern void UserInputRecorder_Update_m9860349CF376ADC02F0A076788335640AB128A5A (void);
// 0x000004F0 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::OnDestroy()
extern void UserInputRecorder_OnDestroy_m9FCD1FA483A4325E124A6A4D00F671B5D43BBE85 (void);
// 0x000004F1 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorder::.ctor()
extern void UserInputRecorder__ctor_mBAC246A8CCE3E16C9BF1731D423B019227B655E8 (void);
// 0x000004F2 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::PlayAudio(UnityEngine.AudioClip)
extern void UserInputRecorderFeedback_PlayAudio_mB1012BA0413A6F56031446B59CB9953B76266363 (void);
// 0x000004F3 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::UpdateStatusText(System.String)
extern void UserInputRecorderFeedback_UpdateStatusText_mC6BCEFE34863E7A8E1BE4E9C68465B5C031D133E (void);
// 0x000004F4 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::ResetStatusText()
extern void UserInputRecorderFeedback_ResetStatusText_mFE0D7238A0F369A068CCAD27F46632C147FD4294 (void);
// 0x000004F5 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::Update()
extern void UserInputRecorderFeedback_Update_mA86AAB0647AF04743F319FC8DE8385837F1B4995 (void);
// 0x000004F6 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::StartRecording()
extern void UserInputRecorderFeedback_StartRecording_m597E886FD203FA383991BD349A52FFEB5F68552D (void);
// 0x000004F7 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::StopRecording()
extern void UserInputRecorderFeedback_StopRecording_m2991870378EC883878AF4F70ED3034C581818D82 (void);
// 0x000004F8 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::LoadData()
extern void UserInputRecorderFeedback_LoadData_mAEF93C1C7D72581C713696B8D8AB09E519078700 (void);
// 0x000004F9 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::StartReplay()
extern void UserInputRecorderFeedback_StartReplay_mDB511EB238E57FC9B88AD61B59F4E0B23B1E7DBC (void);
// 0x000004FA System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::PauseReplay()
extern void UserInputRecorderFeedback_PauseReplay_m3DC27510B2C972C5298546A8172765B3C57C4199 (void);
// 0x000004FB System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderFeedback::.ctor()
extern void UserInputRecorderFeedback__ctor_m6025BE8DF951F0BCE5C0549613A755E01E38585C (void);
// 0x000004FC System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::Start()
extern void UserInputRecorderUIController_Start_m194ACE075C08181F70372B3308BD0DE2A3DB8B54 (void);
// 0x000004FD System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::StartRecording()
extern void UserInputRecorderUIController_StartRecording_m85AEAE54B880CB2E3BDB369453F73A788316CDD8 (void);
// 0x000004FE System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::StopRecording()
extern void UserInputRecorderUIController_StopRecording_mEB93E3F2B7F537436E8BB9F7AE4CE605B5C36905 (void);
// 0x000004FF System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::RecordingUI_Reset(System.Boolean)
extern void UserInputRecorderUIController_RecordingUI_Reset_m09E03DE3ACB436AE7FCD83486F85DDE7AC3887E8 (void);
// 0x00000500 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::LoadData()
extern void UserInputRecorderUIController_LoadData_mC8CE0E2F299FB4368259803ACE4F0E6EE3E3C25F (void);
// 0x00000501 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::ReplayUI_SetActive(System.Boolean)
extern void UserInputRecorderUIController_ReplayUI_SetActive_mEA0A639E776B20C8319F0FDB6067C241C0444A1B (void);
// 0x00000502 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::StartReplay()
extern void UserInputRecorderUIController_StartReplay_m2D3403C6A835556B5F467244EE77EBDC47BC8187 (void);
// 0x00000503 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::PauseReplay()
extern void UserInputRecorderUIController_PauseReplay_mECFEA8EAC5444AA7A18E3C1BB56595221E6CD473 (void);
// 0x00000504 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::ResetPlayback(System.Boolean,System.Boolean)
extern void UserInputRecorderUIController_ResetPlayback_m3AB46A57B54B4AC79B77F9AE3B7A0858951AD28C (void);
// 0x00000505 System.Void Microsoft.MixedReality.Toolkit.Examples.Demos.EyeTracking.Logging.UserInputRecorderUIController::.ctor()
extern void UserInputRecorderUIController__ctor_mBD9DC54CE0E68A054D6546365DA3083184688813 (void);
static Il2CppMethodPointer s_methodPointers[1285] = 
{
	ARFeatheredPlaneMeshVisualizer_get_featheringWidth_m8BE519A22AD47F3A760464845108D0944511C87D,
	ARFeatheredPlaneMeshVisualizer_set_featheringWidth_mD87ACEDD2FF4BC88F656401F29F1D1810A1104FF,
	ARFeatheredPlaneMeshVisualizer_Awake_m993B679E7BFB88DC915BA81EFF12E0E87C15A7F2,
	ARFeatheredPlaneMeshVisualizer_OnEnable_m155B23B8A1DF0DD73F1162A0345C4A12C7F604EF,
	ARFeatheredPlaneMeshVisualizer_OnDisable_mAE620C99AE5DE85F1C82B9E9A35C6780C8C7E67D,
	ARFeatheredPlaneMeshVisualizer_ARPlane_boundaryUpdated_m40EC1ED3A7A6F667F0449A09A480089094B7A24E,
	ARFeatheredPlaneMeshVisualizer_GenerateBoundaryUVs_mB3462F5523511A902C34362EDD0516C249F88B75,
	ARFeatheredPlaneMeshVisualizer__ctor_mB44A8435E0EC4636C44C3B033562170947741D68,
	ARFeatheredPlaneMeshVisualizer__cctor_mEC2B60FD20D1136E61018EB6E9529C2E4A2050B2,
	AnchorCreator_get_AnchorPrefab_mCBC6DDFB725DC05F6F1610FE7D322B357A08536C,
	AnchorCreator_set_AnchorPrefab_m9F7395B86B99482963E7BFDDF264CD2CEBA143BA,
	AnchorCreator_RemoveAllAnchors_m34875F8B1A50A4C417E4C417D0B339A88A9AC92E,
	AnchorCreator_Awake_m56775052F5AE0008240A8A71DDB205E8E9B8A7E6,
	AnchorCreator_Update_mF7D8BE0D5E2E77762285F666F94095AA2A5FDED2,
	AnchorCreator__ctor_m0081A533A02EB12D0C1D6243FB8CF360DB688F55,
	AnchorCreator__cctor_m690F180F316103AF8B707D214C588406D5D9ED64,
	ButtonCodeChecker_Update_mB6C8E767CE3F4467E311422EC7C6CD9AFBF9037F,
	ButtonCodeChecker__ctor_m8A91B4F891ADAE8DB48A87F76818C66D830A2C1F,
	ButtonNumberChange_Start_m3FD0A889466638FF628B7F63208186D5B242893F,
	ButtonNumberChange_WriteInConsole_m7232E7FC3BD64B26556219FFF72C926C2F8E5AC4,
	ButtonNumberChange_AddToNumber_m7F57F5D278DE5EF415EB77435E86F557B803A10B,
	ButtonNumberChange__ctor_mFB24C8BB0F772D7C2C9025101AFA70A046682574,
	ButtonOrder_Start_m241D3CD2CDBDB2EF5BEFB8FC0B5DE00155A00F3A,
	ButtonOrder_StartTheGlow_m3DCEF84F43779917F18F8EE982376F14BCC24C93,
	ButtonOrder_ObjectGlow_mFFACD508BE48F738C0B6F6670043CCD24E8D9A28,
	ButtonOrder__ctor_mB6A5CCFB39A9AD500AAEBFE68994034176BF58E6,
	U3CObjectGlowU3Ed__4__ctor_mFFF411C1EF1D8807296A811F1F11ED26AD45D30D,
	U3CObjectGlowU3Ed__4_System_IDisposable_Dispose_mDDAB1BA6F77D08AA6F33C428CC442A4F6DDD67C0,
	U3CObjectGlowU3Ed__4_MoveNext_mB3E6AB0065460FD0B812E7812A886749DF2A7C04,
	U3CObjectGlowU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE1361ED0C0996CF5D8FFF29168BDB0CC68A62A85,
	U3CObjectGlowU3Ed__4_System_Collections_IEnumerator_Reset_m49133519AB880CF16DE2DB819F3D2D174E7F9FBE,
	U3CObjectGlowU3Ed__4_System_Collections_IEnumerator_get_Current_mCF9EF965D329EA8A4BA33843423711DF62E304D6,
	GemStoneScript_Start_m5CF5ED869E1C269767B4B8F63CA2131D99D5D020,
	GemStoneScript_Update_m3FCD4A056EB88E5242C8BE34452200859712783C,
	GemStoneScript_OnTriggerEnter_mE61C6314D01C53FEBB7A41FEA8D216EB842BD0FD,
	GemStoneScript_openBox_mAD9EB2014659890F7578637FF8C22151F0F8B18C,
	GemStoneScript__ctor_m18894504D13CD149B012E0480CDBAC973B07DD1D,
	nailPulling_Start_m7E1B7BD93F6F5A5BECC4F3C5353A7954D4F524A0,
	nailPulling_Update_m4CF554D8635E89421C6048A6FD6148415BB056F3,
	nailPulling_ResetNails_m73FF2A2082B044FD519FA31A2A2626251B838891,
	nailPulling_destroyNails_m93290212E621256E37F808019B0DE23805119435,
	nailPulling_StartTheGlow_m7AEF03D14F55C66E411D6756CD4B7D01BAC69A50,
	nailPulling_ObjectGlow_m671AFA1FB9F8625C9ED1C3EC4F1DFF56D06486EE,
	nailPulling__ctor_mC80A4D1F829F11A75A0402DEBCD5B19F3E609813,
	U3CObjectGlowU3Ed__13__ctor_m36705611765608E1AB5180A973D57E7E35844585,
	U3CObjectGlowU3Ed__13_System_IDisposable_Dispose_m5B1BAE725BBB3C5F71C815456DA1E0DF40A6D6B3,
	U3CObjectGlowU3Ed__13_MoveNext_m1CF0BF92BC16BC0B34E9091F28D64074C14BE9BC,
	U3CObjectGlowU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m33227A5F49FE9DCF93DC806DF8654EEAF52C2F4B,
	U3CObjectGlowU3Ed__13_System_Collections_IEnumerator_Reset_m49EB597CCB8F39CD4041F0C029EED9BBDA1EC76B,
	U3CObjectGlowU3Ed__13_System_Collections_IEnumerator_get_Current_mC162CD86B5EF34EF47ADE853F214878EF5101AAA,
	pipeCheck_Start_mB4C4F062F02C075A12348B3A20E2E47CEF31774C,
	pipeCheck_Update_mEF602F1BD191510FF0390CB5C076E9FCD542CEC7,
	pipeCheck_allRight_m3F664F6E4448F3ED79D5F1B0350AF543FFD71B39,
	pipeCheck__ctor_m0D33F7F47D62231E6C109CE2CE4A31C18E3BA62D,
	test_Start_m0E5D7128828095615C6BABB85F034A1FC262D37C,
	test_Update_m43D0AB83A5D3BC6F8ED9CAB780EDE28726FCE7D9,
	test__ctor_mCF2B5CF07AD306E14B3243CF42DB5687A10B7D42,
	testZeiger_Start_m788ACC30F8104D546645652663745C0EA999C7FA,
	testZeiger_Update_m64B6267F81C2D0B657A0D69D6E1215856ACA1AF4,
	testZeiger_klZeigerInPosition_m1AA09E1FE7558762DE7DF6C5CD56A69FCA04D946,
	testZeiger_grZeigerInPosition_m332ECB4F91C0DCD854055447C7AFCABB1A4C7AAD,
	testZeiger__ctor_m12B8F70EC344427B9A2AC2A432FFA8F382FD2265,
	PipeRotator_Update_m9B9900894A57953119A20D4020E78BEE65AB85EF,
	PipeRotator_RotatePipe_m33AF82BF65142654308B03B4E8F752BCDFB8894C,
	PipeRotator_Flag_mF5D9CFD2ED9269D897690035EA2623B60A6A77DB,
	PipeRotator__ctor_m4751A2A6758A6810473900C064CF153960167731,
	ChatController_OnEnable_mF20A5E1E9CD63D8800DD813ECDAD8EAEAF7192E1,
	ChatController_OnDisable_mDE9EDE3922A33C294F174FDF695AAA83750C0F5C,
	ChatController_AddToChatOutput_m89A6C97B3E7BD66E156D8FD8CCD64541DB57F9B2,
	ChatController__ctor_m158B20E594A208D0C5F6333DD917D46FB62F4EC8,
	DropdownSample_OnButtonClick_m79337F8A102B280E0E63457496D2F115D321D74E,
	DropdownSample__ctor_m7E0CECA26A98DAA6F28A1B922C908C54730D1289,
	EnvMapAnimator_Awake_mD5E7612C9869A91B6A0681B9D592C7D8D9445D1E,
	EnvMapAnimator_Start_m3432946DE1A3B40667B9D4CE90384F765C9A1788,
	EnvMapAnimator__ctor_mCF816FEE8C1CB473929754FF4C93B3CAB21EADEE,
	U3CStartU3Ed__4__ctor_m0450ADD262F7838968EDCEA567F1046D7AA157E2,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m0E6287744F66D21C42C549B857E1A4A3BECDBC8A,
	U3CStartU3Ed__4_MoveNext_m99BBB7693C8CB2F4660E3077F949F5A0AEED1489,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1A4BE78025454A74E21F9730A5269AE2E2461258,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m912B63F0B462B11EAC1AA20D7DF921990F53BF82,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m4C9D59B07EECBC8BF4F62D136E6A833D1CF3DB8E,
	TMP_DigitValidator_Validate_m4EF3DF30BE68E1A21BE472A0A3BA1FC20EDFC347,
	TMP_DigitValidator__ctor_m27DF8132999BAC27C2849CE216E6723BA8F35AB8,
	TMP_PhoneNumberValidator_Validate_m5A4A4EBF5A9028BB0FA786671211B9E213AD4B45,
	TMP_PhoneNumberValidator__ctor_m375CFE89D55D4F9090D220BF021B692285A430AB,
	TMP_TextEventHandler_get_onCharacterSelection_m20150DBB2AA6AE1E0FF93C92F5A1C9829162D98B,
	TMP_TextEventHandler_set_onCharacterSelection_m93EE620535D6E26B880160D8B634385C25CAC811,
	TMP_TextEventHandler_get_onSpriteSelection_m4488683A472FCDC75B48E6CBE3C93F95CE324F77,
	TMP_TextEventHandler_set_onSpriteSelection_m2B028D4954742D0438CA3F79FAC273A9EED03025,
	TMP_TextEventHandler_get_onWordSelection_mAD7565769E8437E05EAACDB21D7F5A3E2FED0937,
	TMP_TextEventHandler_set_onWordSelection_m4422C3D459E4BC0F143362CF55614EE0D1612C98,
	TMP_TextEventHandler_get_onLineSelection_mB62A6F4286161E57D306C87007163288DAC6CD9C,
	TMP_TextEventHandler_set_onLineSelection_mFADF41D9509B00E31037ED15F4DE7E94F81DFC77,
	TMP_TextEventHandler_get_onLinkSelection_mD1E4073940CC30686C8720AA612D865D3383F8A7,
	TMP_TextEventHandler_set_onLinkSelection_m2AF582259BC5F7361AF19BF7045B7D222F5876E7,
	TMP_TextEventHandler_Awake_m68E0ED04A66A6C28DC19E2146D68E029F68B0550,
	TMP_TextEventHandler_LateUpdate_m3394ECD6DA959CB037A680CFBC5A628A51493C46,
	TMP_TextEventHandler_OnPointerEnter_m07E27AA290A22918830E5722A2BD42D9F6255351,
	TMP_TextEventHandler_OnPointerExit_m532319C274AE6D05BE14F26487CED4043A3B9595,
	TMP_TextEventHandler_SendOnCharacterSelection_mD31D487759CB8AA8C35E450DBB3CDE60E2D11BEF,
	TMP_TextEventHandler_SendOnSpriteSelection_m11A38EB9C3BDB99606E8CDDCD5EC4E3434DA2CEB,
	TMP_TextEventHandler_SendOnWordSelection_m56034E6F2C416674D8E47F4F5EC7DFE8ADF16EDD,
	TMP_TextEventHandler_SendOnLineSelection_m2276E25EE1D24547D63DDFE3FB3E47C74A7D9781,
	TMP_TextEventHandler_SendOnLinkSelection_mA21BD08A006D3D620A97629C395BE2693361A98B,
	TMP_TextEventHandler__ctor_m428CBE312988192C610700CA48B74C735E9FBDC6,
	CharacterSelectionEvent__ctor_m687770B1255B5BF9631660AEAD91F79910478C0A,
	SpriteSelectionEvent__ctor_mE314A5C368E728BAAEDED124763A20248037CF3A,
	WordSelectionEvent__ctor_mDCF4270A199AC5D4479A161B45D9D11CD4B1BEF1,
	LineSelectionEvent__ctor_m8F84A7A2C4818BE3F27D51986218B84BABDFC370,
	LinkSelectionEvent__ctor_m2EF8B73807E199B9C528AEB298E181822675F52F,
	Benchmark01_Start_mB7AE105F2B9A718DA0034E36DCB2DD5D3CE08E79,
	Benchmark01__ctor_m3B820F7C607C8C6572B857CABB8BCCBFA3106DB3,
	U3CStartU3Ed__10__ctor_mF134B42BDB16CFE45959D2238A883F3782D96A6B,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m73EE988CEE5D33849663BBF874B890EFDF4D6C2C,
	U3CStartU3Ed__10_MoveNext_m1A2DA78BE014DF35ABE2623FF7E4DE3E6CE73FC7,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m40C85CAAA5F91860372CE6EC7DE2BC0651008A47,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m9B240EF4945F412941C11524E6ADD12C718C96CB,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m99360A58385EF846DA9324753FFD814763486E44,
	Benchmark01_UGUI_Start_m34BF3D36C4148DD9B6AFF435494132D134DB1FEB,
	Benchmark01_UGUI__ctor_mB0EEBE63C2DA47C794890B995079A8D56A8EB8FC,
	U3CStartU3Ed__10__ctor_mBD96F83AB86F1DADD0205D48950AD9472C49BB3C,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mA9BCB5EB21EAD06A6B3CAA5F637E4E3E61690A15,
	U3CStartU3Ed__10_MoveNext_m67056499AB7657EA01F01A2244439C4AB723305F,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m73A9DBD9986792A9C873E94820A327719DAD13B2,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m8673C491BA40498DA72FCF82D0C35CDB2B2C1302,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m1403238260593386B4796ABC7CE5DE15710C066E,
	Benchmark02_Start_m859244102E662AABC6EADDF3FDDBCB70BF2B5FB9,
	Benchmark02__ctor_mDC57831288611477556FBDF923656A7C8749E13A,
	Benchmark03_Awake_mE99D6A79FC839F0D59743FAD941491C7C8E547F4,
	Benchmark03_Start_mB1E011A054E5B0EA0B63D5962F0998A1C7581553,
	Benchmark03__ctor_mA5799BD9758B18722EC7CE1A24BE236AE20CB44E,
	Benchmark04_Start_mA03959F172F3569D71A1343425D27BAD52FEA3BD,
	Benchmark04__ctor_m8032BF50C54C71AA79A81686361E19F47264D3E7,
	CameraController_Awake_mA09EE763D304CA3EC70F9FADE7C5A00D023928C1,
	CameraController_Start_m35D27D16A1BE5F7C721D508D62D1F3F56CDD2FA0,
	CameraController_LateUpdate_mF9C36862FF6418664F54F9CC804A8EFADD4E91C7,
	CameraController_GetPlayerInput_mC42951349834EF78DB980FE627670188C38C749B,
	CameraController__ctor_mCE318CF220099289A13C48874313F1770B610857,
	ObjectSpin_Awake_m47A9256687BDFD67EAE370636B3AB21E21D6A873,
	ObjectSpin_Update_m55EF26CC11FC576459CEE011C42B68E143087891,
	ObjectSpin__ctor_m8BACC41FFA3CE1E491EE311A5008B8E345DDB843,
	ShaderPropAnimator_Awake_mB379A242D3FAD0F7E369D592903B1F5D7B710FED,
	ShaderPropAnimator_Start_mFB09A732744D5C593D66EABF79B0FFA5939C094E,
	ShaderPropAnimator_AnimateProperties_m9635CDE20D007A404C2D8F09E0E3D474F83404BD,
	ShaderPropAnimator__ctor_m82C7D0644ADE776B309F6FC160584D247D7EAD7B,
	U3CAnimatePropertiesU3Ed__6__ctor_m1EECC5609C55D8ACE8A015D095176613DF1CC729,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m899F6FDE18D7A082B51E81253115D11211B9E306,
	U3CAnimatePropertiesU3Ed__6_MoveNext_m882B221771B08AED27D03D81A7C25061E8DE7399,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB8A061DA69769417CF2DB406083A6B9FEC9216BD,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_m96C640E17307465DC89EC0216DEC638A1434FB7F,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_m896EEE48086AA67D02FFA51CE50AF7184343D25B,
	SimpleScript_Start_m6560BF08A20093CD0A7F371A961262AFE2CFAA2F,
	SimpleScript_Update_m463C891946C6D01804C9EE73A4D81FF502DA4C0F,
	SimpleScript__ctor_m96AE1681250812FEC30E561808E2E044744FB708,
	SkewTextExample_Awake_m5A5D81FC9F5C9DF02AD3EAC993CAF47D8A0BF8F9,
	SkewTextExample_Start_m2FFD59A5BA1BFD83AD2FE7F7A8C3E121F7B9C4DD,
	SkewTextExample_CopyAnimationCurve_m50B172C75266E7615011784535B2C8FF7F90EFCD,
	SkewTextExample_WarpText_mE1969FED4C69A06BF296EB3C57D655E70067C268,
	SkewTextExample__ctor_mC6835AABE659C8DBA5BA4A183685551B85D452BC,
	U3CWarpTextU3Ed__7__ctor_m0C10F1442F3AFF382988656986D6043B8E15A143,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_mB08192042C7064309C074E7E2A091ACB1F4DEFD9,
	U3CWarpTextU3Ed__7_MoveNext_m1D50FC174732CA47C8CE389F223587AACDF85545,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBB2D90533557FC705A99905EA0757D55CF5BFEFF,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_m98CD8E7319A8FBC6CAFF79C5CBA0A29C7325967E,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m8E6A1A9AD6B87208DFEB11A4769BB5A79E90E1D3,
	TMP_ExampleScript_01_Awake_m874F3F209362AC48D33F5EBC7FA4C5723DE8D50C,
	TMP_ExampleScript_01_Update_m08F7A0207E87622D65043CE711211D59031C9A69,
	TMP_ExampleScript_01__ctor_m3A0460D571FD6B64C52567910D0224FCF55DC940,
	TMP_FrameRateCounter_Awake_m2C945EA0297D6A2611DC3F26D0E2F1C42B797691,
	TMP_FrameRateCounter_Start_mEDAE106674BE14AFC3648C69A3E41D1CC637F39B,
	TMP_FrameRateCounter_Update_m7580AA61513EE7771AD425B610ED95E172DB0776,
	TMP_FrameRateCounter_Set_FrameCounter_Position_m0AC7AB661D2D6CE5983B8C1FB09F3FE593E3A4B1,
	TMP_FrameRateCounter__ctor_m0C6B6A2FB73C5C15FBE49311AE41B9119C681982,
	TMP_TextEventCheck_OnEnable_m0F2BDABCF5F504D32E8B9094176D3B8BFAB934D7,
	TMP_TextEventCheck_OnDisable_mD575777EFB0EC8EE29584A8DC787FC868FB5E972,
	TMP_TextEventCheck_OnCharacterSelection_mC069E261BD7C80FCD49F4691AFE6CF517D57EBF0,
	TMP_TextEventCheck_OnSpriteSelection_mB1857DAF79087421D71FCB8072B494A684316CE9,
	TMP_TextEventCheck_OnWordSelection_m06978A868DAD9B374DD540E993226DFCAF1E7042,
	TMP_TextEventCheck_OnLineSelection_mCA3F1A95EC6AED62E2133F35C9745CD9DD7F4AB2,
	TMP_TextEventCheck_OnLinkSelection_m1AD16AC94C97A5781DCD37D2D0BC5AB3D440C7EF,
	TMP_TextEventCheck__ctor_m31E66283C7016176B606C31E6E36C86DEA576538,
	TMP_TextInfoDebugTool__ctor_m0C32303C51A3BDB40380743E8042E64F265106E5,
	TMP_TextSelector_A_Awake_m0BA96F8F4B2FF0BC68CC45B403AEC38D81F2E09A,
	TMP_TextSelector_A_LateUpdate_m8B1DF00F5307B2363E9DB846E9C2BDA1EF8F35E2,
	TMP_TextSelector_A_OnPointerEnter_m9CF43FFA0C0095DFDBE147C1CC62DF56CC53788F,
	TMP_TextSelector_A_OnPointerExit_m40D418048B15375DBDDEB1C0E08460E5109D7349,
	TMP_TextSelector_A__ctor_m8FC596D84378365BF7E6B882E0D8497E43290652,
	TMP_TextSelector_B_Awake_mC25FC673DF7903FDCB2106D1F3E86F51A7B744CB,
	TMP_TextSelector_B_OnEnable_m87E464E40A7CB4BFB1640D9455FC4D4E256774C1,
	TMP_TextSelector_B_OnDisable_m6C263D36038359489A6D5093D48ADA52D1A512BF,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m1DBA213DDC2FAA2F93F58ECCA5449A31468CE7AA,
	TMP_TextSelector_B_LateUpdate_mA647A689554A9E5BD07A83FC33B47D86C1A5A6CB,
	TMP_TextSelector_B_OnPointerEnter_mD6244A767EE3E832CC86E4973F3E32A5C52C7AE3,
	TMP_TextSelector_B_OnPointerExit_mD24E329F485441800296BDABC6CFF3E29B6E3538,
	TMP_TextSelector_B_OnPointerClick_mDF251BF4B3BB77DF313BEA93940221D8FEEB45CE,
	TMP_TextSelector_B_OnPointerUp_mCA921480DC3BCD6BF36A416DE271B8479C6AE1D8,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m2D57105EB1EA16EFFE705DB501FE6BF674AC9B14,
	TMP_TextSelector_B__ctor_m40EF80772527433E176CBF074A8D693686D043AD,
	TMP_UiFrameRateCounter_Awake_m02CDF44F571DBA212E72CE767D76C61C98E45BB9,
	TMP_UiFrameRateCounter_Start_m04178DCC7FC7FFD4A454FF9875759C2BF3AE477D,
	TMP_UiFrameRateCounter_Update_m466029DF08916A28183C65D155CBA4D97AD90085,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_m888C1A972775B5F346E95E96196B893940B27673,
	TMP_UiFrameRateCounter__ctor_m8CC7F7BF10E3233E801720D103E944EC7A8F11E5,
	TMPro_InstructionOverlay_Awake_mFC97D188C55093B468D2E441955AEC2F9BDC412C,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m4C9AC18BF54ABEE99F1DC416139CE15BA8A1F7B5,
	TMPro_InstructionOverlay__ctor_mEFCCB302387387EE1DC8887DA8FA7D70E0F4211F,
	TeleType_Awake_m9AF2AB9EE959F0770A815A889F0182AA1E24822C,
	TeleType_Start_m37AB7AE2F364CEC3F6181C53750D67E08B07B63B,
	TeleType__ctor_mF4141FAE2172EC71B08DF8BD04CA1E0DAC518A55,
	U3CStartU3Ed__4__ctor_mE761C315080CB9D9ECC5E13541382FD8F4CF6A70,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m32D3DFDFEBC3CB106D26E913802FD00E209C12FF,
	U3CStartU3Ed__4_MoveNext_m5E32F1699168847254169554317E6A21C09BF04B,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC97655E37D3DEC57375D73AAE9E98572E8F81E27,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m8CDA19A57B0E471F332E9C43B122FEC63FB14FB4,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mA02188F69F7BC357C743F33D3A643A7ECD0DBF69,
	TextConsoleSimulator_Awake_mF9F53B0B1DCE3946C143F71447C087297A17AECE,
	TextConsoleSimulator_Start_m56193F67296DBDD9F554100249B2C2E0DF4C96F0,
	TextConsoleSimulator_OnEnable_m25D719AA368ECE39E41C4DBA260F2F901057F2B1,
	TextConsoleSimulator_OnDisable_mF7739529823689923F4D489D6070CC1C09AB9178,
	TextConsoleSimulator_ON_TEXT_CHANGED_m238472B614F4F8493A2A4159D5BA023B631F1602,
	TextConsoleSimulator_RevealCharacters_m270C2B31ECEBC901F31CC780869FF0BF9ADBFFCF,
	TextConsoleSimulator_RevealWords_m2831A10B9CBE63D3B6A96764F84F98860FDDEA87,
	TextConsoleSimulator__ctor_mE04116D942A1DC7E2A902EEF2D73A9BC70547055,
	U3CRevealCharactersU3Ed__7__ctor_mC622CB57F2C8C2C5E5D6592FA61D37D9B2DD2F4E,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m8581BA87F52BA3D2EC25328353D57DCEF2A57932,
	U3CRevealCharactersU3Ed__7_MoveNext_mC90DB377F1C559B997DE55C556295404BC007863,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6F82F9A02F414D7485FD3AC3003ED386F5329009,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_m0A85EE143D0136BDEF293624A8B419EDFBCAC183,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_m52987689D9C37472E3A5BB72A11F676BDA9708C8,
	U3CRevealWordsU3Ed__8__ctor_mF28D666A0DA72F5541416C9A900D63FFF1826D3C,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m1BB12C3BC2792B0BFDB4BAE14EA6C876A64FEED2,
	U3CRevealWordsU3Ed__8_MoveNext_mF9D0045F75224C085DA0269FFA799E17D4CB3ACA,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m78510D10295A5465DF7FA6CC3062988A0A8C21F0,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_m5DBF478AE8BD7F6A592EEAA1D0955006E69E36F6,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mA8E2269E365F70178BEEC3156303BAB8F7115642,
	TextMeshProFloatingText_Awake_mFEABF0FEE47328274683F4899B377117C757C9E1,
	TextMeshProFloatingText_Start_m0ACA6020BBC364CBC2EF08C8472CD11B76066D4E,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_m214F7E4C44FAE3A3635443BD41F2645BEF83D07C,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_m634EC4DD456EB9596D9ACF27710D5F28095EEDA1,
	TextMeshProFloatingText__ctor_mB062CBF6B48ADA7E92D72849264414C1ED0A4F98,
	TextMeshProFloatingText__cctor_m86FD22D3091D14EA4924C3F0CDA132B30031FFFB,
	U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m328AB7CF0E2A3A56BC77FC55140903098CF87CFE,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m052F727B1032933B1DD2CABD4B44CFBA23662047,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m8078CEDC5A5DEB0D8415DC1F80465065FFB7B6CA,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7C348F98BC1901EB30404695B7548067947121D8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_m62DBBFD80A8879E56C9BAB8B279CADA3B0D5E95F,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mC6FA041905BD4EEE785DFA63B708CD601C8C0C2A,
	U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m27613E638D919598A9C8C2DDCA2C157E69A7D399,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_mBB9A19DD082FE5F45831A42F6ABFF7310B783653,
	U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m5874F6D7A5B3DBDFB3E9CA33459D0BCF624BA3C5,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m033D07D6C2A22DE4239E5B7F15C7535C1AE709F9,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mB883AE948B2D5B213E28C2C08FC4FAC846090596,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_mDB3B40C77FDD1B4D3FCB62E34C424BCB9F61715D,
	TextMeshSpawner_Awake_mD0F9DAFA485D992EE3155E4DC07101973539ABE2,
	TextMeshSpawner_Start_mED51273FEAFDECDACA309E0EDAD4D84530B40C8E,
	TextMeshSpawner__ctor_mA484641AF80E15BBE6ACD6DBE2CD6A112A6A6B92,
	VertexColorCycler_Awake_mB13FEC65FA2E52A5BD69C9280C4A834AB9E80B67,
	VertexColorCycler_Start_m9C670DD8F01640079CA88868FC60B26F92A8426C,
	VertexColorCycler_AnimateVertexColors_m4EE5EFAAA9D9F65C811848AE36551E8137B3FDF1,
	VertexColorCycler__ctor_m820FE9B9D4FD4DF13E46AC69083C8CF5C39020C8,
	U3CAnimateVertexColorsU3Ed__3__ctor_m69255FD61F9CB7F7AFD1468325DB6EDA90C3DD83,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m9E8F17AF5EB46169BCFBA2152681B7A4A8B870D9,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m60B256083EA4FEB47FBE4D0A4E6A2E40506A7BE7,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF8FBC07125C99541937A71DE72A3348131A4FFD9,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m176E827425D6A6561CD6A941517F502A1AFD610E,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m25C8138AE53031CDDF90D50063B1286ED44F4266,
	VertexJitter_Awake_mBD36C30F9C0C86C06BC4DC6AFA89D4D9BE32137C,
	VertexJitter_OnEnable_m5DBB397E98FFE9F3AEE0B4E7EF3ECF3BCDDEC022,
	VertexJitter_OnDisable_m9A4C23D5BFAFBC80171B3CC2CEC70FC55CC3956D,
	VertexJitter_Start_mB1E16567B0539D52986D404B4C62D04287839393,
	VertexJitter_ON_TEXT_CHANGED_mDA0B68137BB6AAF18974D43D5671481B07F01854,
	VertexJitter_AnimateVertexColors_m76363C66669DF0DD2070901DEFFF39D24C242B90,
	VertexJitter__ctor_m810752457B6B201D5FCA16DD9D7FD0547C73A66A,
	U3CAnimateVertexColorsU3Ed__11__ctor_m896A9095AFF2B1F7D823B4D14CFC2527EB3152CC,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_mD72B273D37EA2543C5C0B88211549EC0FA3E720D,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mB4B5A3E3D2294D96ADFB2E3A3496E1120DF95E5B,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m36CA4792794DA1C9908A9C9FC4874ACF82804AB8,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m627446844CA02880583B92428889EA9D9CEC04CC,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m7669B30A4AC65F85F8EBA4BDC640F43B74C478EC,
	VertexShakeA_Awake_m370B438BD9DFEE065CEEA6909C89DFAC2DE72453,
	VertexShakeA_OnEnable_m8C9742B24A0516B19E9B9EEF8B3E00AEF9F273A7,
	VertexShakeA_OnDisable_m8638D26DC36898D64A45E091B907858AD112DC35,
	VertexShakeA_Start_mB0DAAD7A718A3672087BD9200D41DA7602EEE39F,
	VertexShakeA_ON_TEXT_CHANGED_m4C12FD42A37E534C404594849050B7483830AE71,
	VertexShakeA_AnimateVertexColors_mAEF171DDF17D8B42259C3C2F6486E550DE014BF1,
	VertexShakeA__ctor_m4163F3E6E774AF69910ADCEDBD2106B3E0B4B17B,
	U3CAnimateVertexColorsU3Ed__11__ctor_mCAC1B99791AE7DD5F3F96BC4CB7645898301C2CE,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m4EEC65E3452BBDCB67C9ABC7409C73FEACF9BC2F,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mC90F022F41945DEF9ED06E858D032F5754441157,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBC451FD1BCDDB66EAEAA07F399199A9DC63AFB23,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m74DE64545E3327D66FB6C069A5190BFC0FF683AC,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_mD3DFEB94E1B0A0153083715008C670364D32C097,
	VertexShakeB_Awake_mFEC3543DAF8F57A0A034F312AE333C64D0720E01,
	VertexShakeB_OnEnable_mD79056ECD2020B2296E8706B150DA95F9F925D70,
	VertexShakeB_OnDisable_m22CF9F7C2867971192D9109997A7557ED34B06EE,
	VertexShakeB_Start_mEC4D4CEF675A36B236B541855B25452BFF3CAE01,
	VertexShakeB_ON_TEXT_CHANGED_mDCC1525913C80D4D01BE6E57D290032E84EAAA52,
	VertexShakeB_AnimateVertexColors_m4C64F1552C5539AE431814B8DE4673B764D26576,
	VertexShakeB__ctor_m29041B8CF90385D7ADFDE3DB2DA56B4F183D4969,
	U3CAnimateVertexColorsU3Ed__10__ctor_mD5440310F221693B051892C32E5825ACF2910B0C,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m85345564110FD83A156AE3D768E78E41E8D464C0,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_mB184ABC2C84B0A4B134E026869BFBD531B234081,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C64AB0762FDA81B311143C78B0B7314A05B40F7,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mD0C3021D6DEB76A541E9A6F93CF17691D62B417C,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mAD8B239C2AFA0DAE135EED3CFB49F0816D150048,
	VertexZoom_Awake_mCED2795CD8617FB459D74A6C9B3E8E5E08AA30A1,
	VertexZoom_OnEnable_mDFEAE2F85132963A28A0AE9AAE061A2710A87B66,
	VertexZoom_OnDisable_m9E7C6F3B64A33C0EDF60F2F864981A5FFF147113,
	VertexZoom_Start_mE306D1DA952D2254909ED9F1FE699A051B385136,
	VertexZoom_ON_TEXT_CHANGED_mF83B227AFCD10F9FB6FD132DA0F4B8FDD23D2B0F,
	VertexZoom_AnimateVertexColors_m7373CF7A5AC35CDBC74CE80BD56B04123A91C0C7,
	VertexZoom__ctor_mAC448B16E14E3F3175FC882D1FB0C39EC553EE21,
	U3CU3Ec__DisplayClass10_0__ctor_m8141BB14AD7BC732E8A9AC7D0C4AB59A55CA526B,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_mA10F7FD56C7EB37EBF7BC1C764D4D10A9F7C61E6,
	U3CAnimateVertexColorsU3Ed__10__ctor_mA2C6461F1E88C3131B5C4E6FFD7C75771C73D09A,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mE5843C784E5D25537D577419267FAA093836BBA6,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_mF52BA74121774AC25AD645A6AB43556C2B2BC4FB,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m02F8B326B53D4BB33A54254ED664BA615DAC4E24,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m06892291F75733C0A0525F44054E4CEA8D9BD58C,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m2F4836BAA267C17023363704B71B53454934A6E0,
	WarpTextExample_Awake_m325723A91F3B6B3541926775C0DA1BD8FA4B75D0,
	WarpTextExample_Start_mC7442B0A9CC1A12FDAA128C215A66FDD34EC35F6,
	WarpTextExample_CopyAnimationCurve_m8074EF4A6C2BB055960B5AA7DD12FECB81F4DC7E,
	WarpTextExample_WarpText_m453D126F0994CE92A6A3D2F620B81BADD85F7DBF,
	WarpTextExample__ctor_mFB18E87CB3089AB1EB0DB1D3AC9C53AB381145E9,
	U3CWarpTextU3Ed__8__ctor_m76DC34194319F4AF8CFB96066F3764402BEB5279,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_mE963A8C458C7424814637CF46BBDDEC46CAD7F9C,
	U3CWarpTextU3Ed__8_MoveNext_m9924A340CE9833FFA7A34E26A3DD75C5E51AAEDC,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m32CF5062AEB89B0ECAE1021AF873CD799923FDFF,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m46782447641AAFD07BA0D6D363B5FA2FC2282837,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_mDB18401834B9740BFAECF2C624EF0C8ACED4E9D2,
	BaseDwellSample_get_DwellHandler_mC114D08153104E01605702BD59AF73888F1E6CE9,
	BaseDwellSample_set_DwellHandler_m73E008459D501A72BB8C968B6BAF6575D5265348,
	BaseDwellSample_get_IsDwelling_m9E0E0CD2600EB5782A736E1C8EAC08773126F873,
	BaseDwellSample_set_IsDwelling_m18391541F4D24E4F07E99244BE58C23997F65E4A,
	BaseDwellSample_Awake_mE27137E205C875CAFDC5A68C1DCFBCEE4483CAAA,
	BaseDwellSample_DwellStarted_mCF844F3E102A29C2408574C2346787EC05DDFEAB,
	BaseDwellSample_DwellIntended_m2D4A73E52370E2561402606AFC2CC3B8821FC616,
	BaseDwellSample_DwellCanceled_mB794C48E5F4A3A3F24875DC788102FCB21288638,
	BaseDwellSample_DwellCompleted_mBE3F3E2E50D6BA46AD9EEAA37AD8EFEF90381808,
	BaseDwellSample_ButtonExecute_m7C80090AE4591E2754DC18509EEEBBAAEF091D8D,
	BaseDwellSample__ctor_m3D2F88E08FAA4EB4EA2487CFC539DEF9BB044C35,
	InstantDwellSample_Update_m3FF0B0611C4720FFED1F58C1C696301608E3876D,
	InstantDwellSample_DwellCompleted_m06086AEA00F283BFB795003941548DE685FBC90F,
	InstantDwellSample_ButtonExecute_m13603FD4A7B15908271B128D07168F4ED5A4170E,
	InstantDwellSample__ctor_m79634CE899182B8FF0A5C9BF78867B456C3F42BA,
	ListItemDwell_Awake_mC8F41E088876688E181662329BC1ACFB906945FA,
	ListItemDwell_Update_m6CDBB0AA928BDC45F4D163217DCC50168BDD1270,
	ListItemDwell_DwellCompleted_m1C73E6044630B10E13976CCB2A412FAEDB7796F7,
	ListItemDwell_ButtonExecute_m9BA94FB2DC544F6A023A1B5589F84478371B7F15,
	ListItemDwell__ctor_mB6E8EBEE58A60E459658E00C9C518BA671870399,
	ToggleDwellSample_Update_m36A77F7BD8D577E9DB0E54964BCE3F2C04EA192D,
	ToggleDwellSample_DwellIntended_m5899E646C78FB7093B888AC63FDB814F3DDE5B94,
	ToggleDwellSample_DwellStarted_m5003812EFAFF3DAF8C22783769F934CFB9E11F81,
	ToggleDwellSample_DwellCanceled_mD91CF69114E163EED925BEEE4605BF170268CC80,
	ToggleDwellSample_DwellCompleted_m74F9402EF0BF3C1B74C7E46624B8DE2077F953BE,
	ToggleDwellSample_ButtonExecute_m184D3DA722763CA68D6187F114D201B4EB4BDCD2,
	ToggleDwellSample__ctor_m57E41F049E3EB0755C07C83CA628209936F66D17,
	DemoSceneUnderstandingController_Start_m47717E3598089742D2A3303C037A4CB525063692,
	DemoSceneUnderstandingController_OnEnable_m2F0F9FCDE8E7F215D922B5838C2E8C31E49568E8,
	DemoSceneUnderstandingController_OnDisable_mA512931FA6F655A8C59DB41F113AA6BF01916CC3,
	DemoSceneUnderstandingController_OnDestroy_mAF8DF2D7CCEC0F1ACA0A8900F403A40EAEC10273,
	DemoSceneUnderstandingController_OnObservationAdded_mE5546987FF369BD3DE39B09308A33FF64CF6AD2F,
	DemoSceneUnderstandingController_OnObservationUpdated_m1AC359A33DAB5AF353B14BF33605D517DF8EDD53,
	DemoSceneUnderstandingController_OnObservationRemoved_m8E0DE889D799462722E13831CFB3F5CEB65110C8,
	DemoSceneUnderstandingController_GetSceneObjectsOfType_m88A1DD6CAD512CE6BDA637399EDB3BF1A67A7836,
	DemoSceneUnderstandingController_UpdateScene_m82E8775487E2296361CA0715AF6B28D6153A541D,
	DemoSceneUnderstandingController_SaveScene_mBE7E5D939D5F07001D58B03ED4B8503144C1178E,
	DemoSceneUnderstandingController_ClearScene_m442EAB06F5144B65E1C831158373955730EB77C2,
	DemoSceneUnderstandingController_ToggleAutoUpdate_m00B4186ADC3D4188A36C4DE187864DB65CF1A1CD,
	DemoSceneUnderstandingController_ToggleOcclusionMask_m63D49573C306D380B6AAB8F5675D49809D9FEAA4,
	DemoSceneUnderstandingController_ToggleGeneratePlanes_m22D40456913B713BE25453FBA743FA24CE4DBFE8,
	DemoSceneUnderstandingController_ToggleGenerateMeshes_m780323EE623313F5E6B6966D83E85322D6CFCD63,
	DemoSceneUnderstandingController_ToggleFloors_m73873782469A5D450096695844AE5D8752D53431,
	DemoSceneUnderstandingController_ToggleWalls_mC6C9A213B0959E4B571E87EB92764FC285CC472D,
	DemoSceneUnderstandingController_ToggleCeilings_m11C31CF1AA7BF247EAA89D5DAD9F96CD7AB915E1,
	DemoSceneUnderstandingController_TogglePlatforms_m21111095977E0BF1360981FED149CD96036BC122,
	DemoSceneUnderstandingController_ToggleInferRegions_m16957597ABD7FC5FAAEB65BB1075FEAD4344DD4A,
	DemoSceneUnderstandingController_ToggleWorld_m9F4CFBA323B5CE97A9C59684E604A7562AB9FBE3,
	DemoSceneUnderstandingController_ToggleBackground_m6DFF9BF5F9968E3B7EE5CF312F45A262356408A8,
	DemoSceneUnderstandingController_ToggleCompletelyInferred_mDB18B7B05AD25080AB57D732C92317C25246E062,
	DemoSceneUnderstandingController_InitToggleButtonState_m528CFC0969C5496F611C75DA52C751EC7A9B809F,
	DemoSceneUnderstandingController_ColorForSurfaceType_mABF77F25C7623F0ADD2790D11B9357AB633BEB53,
	DemoSceneUnderstandingController_ClearAndUpdateObserver_mC8BB24CAB3243D8204D81A29DE83FD97E5B42645,
	DemoSceneUnderstandingController_ToggleObservedSurfaceType_m87284E1E5156CB0AD80FBFF64C465F014E18A62C,
	DemoSceneUnderstandingController__ctor_mBB76E0DE347FA8439A529450F0ED4ED8345DD9A1,
	KeyboardTest_OnPointerDown_mF036F7DDA6676AB249BFBC783747009FF697CFED,
	KeyboardTest_UpdateText_m0F5DF7FECD3B4B5A56CF3B47086A8A1454D7B00A,
	KeyboardTest_DisableKeyboard_m822B6E839D2F082C3CB4983EBC629954BB783AF9,
	KeyboardTest__ctor_m8288D21CD0D276005CEEE13B5BFDB9826E4F3B18,
	JoystickSliders_Start_mA36CFC6517537A051702C8649AF92F3BAF9EB388,
	JoystickSliders_CalculateValues_m4D43C5C1E07AF2E53801576CFE7B83FF04C0DEC5,
	JoystickSliders_UpdateSliderValues_m92223F35F88652EE85F6237C499F66C0F7555FA5,
	JoystickSliders__ctor_m2D5ADB1CB0E2631433A5BCDF8F24B3E3C910C0D6,
	HideOnDevice_Start_m6C50EC47A90EF24A222FFE59BBD7461595DCA43D,
	HideOnDevice__ctor_mB4CC4F8072A0F6C554869AA0DBF01528D8B6304E,
	MicrophoneAmplitudeDemo__ctor_m3EE4EB4A3BD42EF42ED9AD76D5A005B56B1FA05A,
	GestureTester_OnEnable_m70A8A8C20E61CD1F0B3DB652ACF5A56C592CFCC4,
	GestureTester_OnGestureStarted_mEA2FEEBF17350DC96CF4F233AF706AA6C632DFEA,
	GestureTester_OnGestureUpdated_m72980D81524BE33F7E88230CE656CFDBD5260B1A,
	GestureTester_OnGestureUpdated_m1B5DF61098C445900D8710EF2D6CC651888F338E,
	GestureTester_OnGestureCompleted_mB119D5D0E643291D2567D851920FAE6D0944CDD9,
	GestureTester_OnGestureCompleted_mCA726ED8A8C365B25BFE7B4F2ED948BECF7AFCE4,
	GestureTester_OnGestureCanceled_m0986BE7C254B0A54A1AA092F1EC2BFF0A63D1AFE,
	GestureTester_SetIndicator_m590B0880833C15AFBF23801A9BD557332061A1EA,
	GestureTester_SetIndicator_m5AF23BBC0F813D44B7A1A41E25D5A24600408986,
	GestureTester_ShowRails_m4AB34B32BF07DD03126AA9CCC450DD0AE27C63D1,
	GestureTester_HideRails_mE2211F37C515D7DFCA11F36A21BCF4FD0CAFE52E,
	GestureTester__ctor_m4624CFEF2F475213497E12EA328D0A9BA8A2C8F9,
	GrabTouchExample_Awake_mD6D8B580364828993664012512795E13B4CA1306,
	GrabTouchExample_OnInputDown_mD8EC039D758E604EE91BC758CF03F2C22B8D7599,
	GrabTouchExample_OnInputUp_m1DA424062780EA4257361641D057A8E2B5E164A4,
	GrabTouchExample_OnInputPressed_m68784165F4E0C9DA9B2E950068DCFE29A5809040,
	GrabTouchExample_OnPositionInputChanged_m0B2E9346D0FB09C7A4CC8F425BF1CF8884A6EE7F,
	GrabTouchExample_OnTouchCompleted_mB7634D7E820DEC8E06E5FC2B9B06416E12ADCC9B,
	GrabTouchExample_OnTouchStarted_mCED80CE8B31029F9EC4B42834602843FBE2AF110,
	GrabTouchExample_OnTouchUpdated_mE53F76B9DD63FE228CD8B7EB76E20D116F9657A5,
	GrabTouchExample__ctor_m1451CE02830A4C64F08BAFD558E49332C2C9F67F,
	LeapMotionOrientationDisplay__ctor_m5D700FA5C8C430D95A4CA82250D5B54505CD7C3D,
	RotateWithPan_OnEnable_m237C9A936C5CFF0AC9A3055E7614A5E1428563C7,
	RotateWithPan_OnDisable_m3A88289CE6C11FB1AF4200183F6CC10178767D06,
	RotateWithPan_OnPanEnded_mE96F5C8E146A27275B4A9A54251704E88CF87483,
	RotateWithPan_OnPanning_m3D1251B46F6B6D975053A3F506E12382AAC9BC65,
	RotateWithPan_OnPanStarted_m045EE3892171374450C66B309F65269B7747B58E,
	RotateWithPan__ctor_m2487B6068019BED02B78A2F15524EBF85EE825C7,
	WidgetElasticDemo_Start_m314B8AB55DFA1DE50A0445BF59AB65FC650464FE,
	WidgetElasticDemo_Update_m16A4BF507722AA9F79C585D4F3E3426D95607B31,
	WidgetElasticDemo_ToggleInflate_mE2836D801C0C5B63F60CD3D3B7161476687E2F4D,
	WidgetElasticDemo_DeflateCoroutine_mAFCF1FE1810464E142CA6140D747438EE7CE4BC7,
	WidgetElasticDemo_InflateCoroutine_m19FE65F593F306036730601D94D30F2307A02EAD,
	WidgetElasticDemo__ctor_m5CD27BC1B0AB35AA62F6ECFFD77B2DA74C9ADF8F,
	WidgetElasticDemo__cctor_mB90C7AAF82DFF50FDE07EBD8FAB2F715A97A9B94,
	U3CDeflateCoroutineU3Ed__17__ctor_mCE59BFC0F867BA58A8EBFA0509AE6353BCAFBF40,
	U3CDeflateCoroutineU3Ed__17_System_IDisposable_Dispose_mFF3E272F263C9873B5972D43624F29AE8917EF6E,
	U3CDeflateCoroutineU3Ed__17_MoveNext_m7A116DFA353531D5333977EA13EE2D7E660E0F76,
	U3CDeflateCoroutineU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD3D31C0D3D48BF83F1024764C3BFBEEE1504B054,
	U3CDeflateCoroutineU3Ed__17_System_Collections_IEnumerator_Reset_m6432C3B69E4C3EC48FD04166F910F93750C361C9,
	U3CDeflateCoroutineU3Ed__17_System_Collections_IEnumerator_get_Current_m85DEF7CC0E93830CB2218DB8E323FFDCA2B572A9,
	U3CInflateCoroutineU3Ed__18__ctor_m004C3F0C3E72B5B25663F8B4AC86AB8570EBDE07,
	U3CInflateCoroutineU3Ed__18_System_IDisposable_Dispose_m175FA6717CE8675FEC96F60413B3A08ED6C9B056,
	U3CInflateCoroutineU3Ed__18_MoveNext_m4A59DFFAB5053C3806D78559F2D329ABBA015A96,
	U3CInflateCoroutineU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m440D830708196E0CE60BEC34E2B1F28D3CD40A2E,
	U3CInflateCoroutineU3Ed__18_System_Collections_IEnumerator_Reset_mB20C406EC02C71E87A765F0EC617CE421FA3FB5E,
	U3CInflateCoroutineU3Ed__18_System_Collections_IEnumerator_get_Current_m5C619A4BDCB54466183141A0CD0E43E4BD23A4C7,
	DialogExampleController_get_DialogPrefabLarge_m34AE8F7E66144C9824792EC401A1621FB412297D,
	DialogExampleController_set_DialogPrefabLarge_m6C587A8C53DC767187272DACBF711BA35B9D10DC,
	DialogExampleController_get_DialogPrefabMedium_m0B43AC5C0F4FF95D34F047A2F98F7309B51CA748,
	DialogExampleController_set_DialogPrefabMedium_m63BD2071CF0612353A337395F8A09A78188DB6F6,
	DialogExampleController_get_DialogPrefabSmall_m5D4B294AEEE5001FEF88A15BBC4246E5CD5CBC47,
	DialogExampleController_set_DialogPrefabSmall_m2D5C23ED437D48A51F2045E5F9C3FEC8429791BB,
	DialogExampleController_OpenConfirmationDialogLarge_m6DC6B10708AC3238CCCE2DDB16C01B12FEF2CDAC,
	DialogExampleController_OpenChoiceDialogLarge_mDA051D336A3E4ED0C71CBC9D5CC352A02AF22F75,
	DialogExampleController_OpenConfirmationDialogMedium_mC771E82CD3278A01193C610A684835D9B4DD0142,
	DialogExampleController_OpenChoiceDialogMedium_m8E911DF0D82D85AD7992FF2C31EE09EDB6582FDE,
	DialogExampleController_OpenConfirmationDialogSmall_mF03B1B2FC2E8D99AA81181FB2BA8651D72676EDA,
	DialogExampleController_OpenChoiceDialogSmall_m54DE0690D68641FE03DD92BF380C31F19E5B7701,
	DialogExampleController_OnClosedDialogEvent_mD28735E27B58D1D5730676DB0C773A9FD5931568,
	DialogExampleController__ctor_m978AEB93463870F1A685D6B1B69EA525E229224B,
	LoFiFilterSelection_Start_mC296F036389757EF5A1E70F29EC83E084B1FB6B2,
	LoFiFilterSelection_OnPointerClicked_m7A080587E792B207736264CB6D3C73E18047C945,
	LoFiFilterSelection_OnPointerDown_mC21EBB996B950C647527890378F8A3D6A6B2E6E9,
	LoFiFilterSelection_OnPointerDragged_m785FD8AE632CE5B504BC6C0486F07CE1E29B7C7A,
	LoFiFilterSelection_OnPointerUp_m47753925E6E1B2B6B203C876934917DC4DA52510,
	LoFiFilterSelection_SetEmitterMaterial_mD45B1B223FDCFA883F4A5BE0F3176EEA75ECCEFD,
	LoFiFilterSelection__ctor_m1115BDF686DA47D84F353E1B2BEB8770E9578710,
	TextToSpeechSample_Awake_mE9F5EDD43360B507E57BF929B38AAD4E5F916DBB,
	TextToSpeechSample_Speak_m0FD5993C1E29BE25527926906CC116D1978B966E,
	TextToSpeechSample__ctor_m85245EB56EEEAF6619326EBEC40A716B9CC98FBE,
	BoundaryVisualizationDemo_Awake_m4F9E66F17AA7769A12B5A011320F83CACE40EA3A,
	BoundaryVisualizationDemo_Start_m2D0CC7B0759402E61B2349374122BB6392A983FE,
	BoundaryVisualizationDemo_Update_m904677D86EC9005DA52A88FAFF0C1EFFD8E0E5A5,
	BoundaryVisualizationDemo_OnEnable_m773B8029FA40F4917E84E7080D104401A69E7F84,
	BoundaryVisualizationDemo_OnDisable_m8B23D1E44FB9DA230FA1D6188420221A38BABEFE,
	BoundaryVisualizationDemo_OnBoundaryVisualizationChanged_m0227D50F7AF977B57C03E836DD181804EC22ABA0,
	BoundaryVisualizationDemo_AddMarkers_mD61AD96C12C75D64C759CEA5235BAC8F2D1E256A,
	BoundaryVisualizationDemo__ctor_m96D750966F7F44684DD80FCC2482C8CDDBB20528,
	DebugTextOutput_SetTextWithTimestamp_m6BCE9753AFEE36E4A0BE99D638CE0F77E7CA413E,
	DebugTextOutput__ctor_m3BB983214A1010AE8A008A5F1EF3249EB1AD6D2E,
	DemoTouchButton_Awake_mC4A4CAF46F371265CC2DB871D929412FDA01659B,
	DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_mE924C72CC220C6CABADD39BB13D13EF4A3243591,
	DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m40EB4A51F87E932C9598D4355AA1DF242A322CCC,
	DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m209B708E4EFE0BFD43B64BB968031509279B0E65,
	DemoTouchButton_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m72B985EBF82D6EE9B52CD6202E970D61FF9E8BE6,
	DemoTouchButton__ctor_m8BCD15F7E056F727CEF502B2EA5FC591506CBAB0,
	HandInteractionTouch_Start_mD3BA08B4A89D4926A723C393A9C70D500490DF19,
	HandInteractionTouch_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchCompleted_m9C0206D9EDD306EF541ACDDFE82576472B7AC68F,
	HandInteractionTouch_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchStarted_mB3EDEED3985D372DDDB4864C652379E3B6DC33A5,
	HandInteractionTouch_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchUpdated_mE1FB4240389F33FF0914843E9DE4C0356F74DC70,
	HandInteractionTouch__ctor_m14E2F5CC195D4A829CC784360478A5E734F54592,
	HandInteractionTouchRotate_Microsoft_MixedReality_Toolkit_Input_IMixedRealityTouchHandler_OnTouchUpdated_m6678F97F9406CF49BABD56674B968BBBBBF89F2E,
	HandInteractionTouchRotate__ctor_m4D89B4BFFC3F017788F454C4BB76A0554F7C4497,
	LaunchUri_Launch_m3C25F44EF5B182B3940F50332E1592321FFCE568,
	LaunchUri__ctor_mC549649011B7E99566FC263F88565A2A81AEB357,
	LeapCoreAssetsDetector_Start_m013F637E718EC68FB1CFBB905B7A81208393688C,
	LeapCoreAssetsDetector__ctor_mD7FD61BEA9C55A6F7D254AC9590757E84AC06DCE,
	SolverTrackedTargetType_ChangeTrackedTargetTypeHead_mFC6151E789470A07F74819B3E5ED5332ACD01415,
	SolverTrackedTargetType_ChangeTrackedTargetTypeHandJoint_mB2141D91F91DBF5ABAB7FB936F5406CAA85E0C12,
	SolverTrackedTargetType__ctor_m9BC23444B1D1B785648CBB2ABDC0CDF1F38C568D,
	SystemKeyboardExample_OpenSystemKeyboard_mD6EEEA5E18B625B476003EDB032FB0C680B5123E,
	SystemKeyboardExample_Start_m4C9FC22515C45198FB7F13876C65A3B0E463DDF1,
	SystemKeyboardExample_Update_mD5BDA8992F7826C6F639E360FE37551ACEFC8F63,
	SystemKeyboardExample__ctor_mF14201EABFE1DB91BC803ABC424450E0817CC8C1,
	SystemKeyboardExample_U3CStartU3Eb__5_0_m541CA735B73D753F9564EC8F92050CD1194DEE63,
	SystemKeyboardExample_U3CStartU3Eb__5_1_mD9B2785A96F31F655646DC94D36E0F22330A5CFD,
	TetheredPlacement_Start_mCAEDECC31F526157956588C68E52E9032CB83D18,
	TetheredPlacement_LateUpdate_m54F132D958420609D9C938E78A409357FEB3AC6A,
	TetheredPlacement_LockSpawnPoint_m5540BAAEB8FD8FC6B463A5F1F023B01A1AE023D0,
	TetheredPlacement__ctor_m75DB8161CD3FB3E533E26567BCE022070B3AA8AC,
	ToggleBoundingBox_Awake_mA129C2E367448FE8EC5A0FE8DD286CF8DA2EE359,
	ToggleBoundingBox_ToggleBoundingBoxActiveState_mA664325D9D2DD707DC4B7B532E53E3A3C550D391,
	ToggleBoundingBox__ctor_m7C58483B241D2B98FEF236F26F249B9CE0B2273F,
	DisablePointersExample_Start_mD74D037DE47A0DEF5B7D39132AA0F639427EDE19,
	DisablePointersExample_ResetExample_m7C65AFBF9FC92B11F9803EDC966641F08CE785EF,
	DisablePointersExample_Update_m5500DD608FF437AAC90BF817FA48C7B664A92017,
	NULL,
	DisablePointersExample__ctor_m3D417578DC2C84B4642E26CF713892E6493FE906,
	Rotator_Rotate_mBC6459FDCB67429C5657346A5411C26C7C46B482,
	Rotator__ctor_mB353144F2DD4A3D25F2D2B788DAD0F186FE9F391,
	InputDataExample_Update_m1FB3ED5AE43DDEA37EF80D7DCBD35B743AAA4D60,
	InputDataExample_Start_m0295CFD17E51CDB956A958AC72D69650BF8AB8BA,
	InputDataExample__ctor_m974174F68A7FA082CD73D95EE0286FFCBAA03627,
	InputDataExampleGizmo_SetIsDataAvailable_m5C834CA6E6E32AABBFE1F81D4D774D140B5655EC,
	InputDataExampleGizmo_Update_m8ADCA594907A724BE844805845806EA80F005683,
	InputDataExampleGizmo__ctor_m7FB7A964A1B920B51E0F27DBCC87071518CB670F,
	SpawnOnPointerEvent_Spawn_m2F5717311C18915A9E446386064EF5F4595E17B9,
	SpawnOnPointerEvent__ctor_m3D2D3C713CEAABF6BE5B6DB0A60F89FAD6838F8C,
	PrimaryPointerHandlerExample_OnEnable_m9BA8EB141907B2AA21211D2E25BFC570B55E7F97,
	PrimaryPointerHandlerExample_OnPrimaryPointerChanged_mFEBF3000D8558060AF0D4D11D84F1D9D6DFEFAD3,
	PrimaryPointerHandlerExample_OnDisable_mB1B7794497285DFF164CCD179BB19919DEC5B0C1,
	PrimaryPointerHandlerExample__ctor_m94B57931B01591601E84A05F76CFC4E120989D63,
	ScrollableListPopulator_get_ScrollView_m8E06C23AD8D2D7C6693B3CDEF554A4D9A6B8C769,
	ScrollableListPopulator_set_ScrollView_m60E4E8436AFF962E9C0C4A4C72ABB96CD4450670,
	ScrollableListPopulator_get_DynamicItem_m8241F91467F6F9854FA304423B73F787D877D150,
	ScrollableListPopulator_set_DynamicItem_m15D67C8E22310BA648BF229B0858ECEED02EE938,
	ScrollableListPopulator_get_NumItems_mAC07553EE72902DF560139F31B5A3C72B76C083F,
	ScrollableListPopulator_set_NumItems_m2D9E9659DABC3410E12A2959DF8E2C4AF9F2EB50,
	ScrollableListPopulator_get_LazyLoad_mA87F32E1AAA6622CEC54AFD6486213A067358F90,
	ScrollableListPopulator_set_LazyLoad_mE1C013DD9E2A6A349421E5E39D478C17BF7FEA6B,
	ScrollableListPopulator_get_ItemsPerFrame_m408964D16CD670391BA9F924DA6FA16EEEE873E9,
	ScrollableListPopulator_set_ItemsPerFrame_m3B9A824F083EA0B6A713B64C6E0B5C90740AF516,
	ScrollableListPopulator_get_Loader_mDDF2762A1EC96D0E019BA2C370274CD65DB1562D,
	ScrollableListPopulator_set_Loader_mC8501A2994B604A72CA61709431548D2312AF067,
	ScrollableListPopulator_OnEnable_mD0DF2213A5E104795B7F4C12C4A1F41B461DE93C,
	ScrollableListPopulator_MakeScrollingList_m97AEB71FA5A62D40ED672774BF81748546538C92,
	ScrollableListPopulator_UpdateListOverTime_m9C3F25617584DDF0E49A4E5FE3F0434FC554F228,
	ScrollableListPopulator_MakeItem_m55C5C48C22A2BDFDD1D1E5A3B744F07DF590BAC2,
	ScrollableListPopulator__ctor_m2C0E31FADE5A160801D3388D0716DCCD94B25EA1,
	U3CUpdateListOverTimeU3Ed__33__ctor_m6EEEA2C4C3863DE786A4FB33D9FCD7DCBB83AC7A,
	U3CUpdateListOverTimeU3Ed__33_System_IDisposable_Dispose_mC45591ACB830F32EADCEB7990E91CE8347678FE7,
	U3CUpdateListOverTimeU3Ed__33_MoveNext_m68C215F85564411F4E7DD98408B0D5BC0894316E,
	U3CUpdateListOverTimeU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE4E49E62B8DCC9D5A741F6E62D8B43615027F87C,
	U3CUpdateListOverTimeU3Ed__33_System_Collections_IEnumerator_Reset_m9839718D6F2442EB8A25E4942DA9C744A69674F0,
	U3CUpdateListOverTimeU3Ed__33_System_Collections_IEnumerator_get_Current_m9CA3175850108F9ED839FB1B592FF078FAD74C6B,
	ScrollablePagination_get_ScrollView_m97F66B6BDF2A9AE2A10BE2E570F848CBEBC56B32,
	ScrollablePagination_set_ScrollView_mF5CA58C99567FEBA92DD13DDB6C3434F3FD10968,
	ScrollablePagination_ScrollByTier_mE29EA35FE680FB74723ED5CF662C33FEAD5550FB,
	ScrollablePagination__ctor_m541C254B920CA9B1CF2C85B89EA575C86F24051B,
	HideTapToPlaceLabel_Start_m7E79A585524689DCCE57509E37C6A80867FF2320,
	HideTapToPlaceLabel_AddTapToPlaceListeners_m179AD9312535108DABC7A83B897317BAF95F6FDF,
	HideTapToPlaceLabel__ctor_m6794BC057A5ECD54D34ECB538B144C64ADB87B2F,
	HideTapToPlaceLabel_U3CAddTapToPlaceListenersU3Eb__3_0_m336168FCCF071E355A2405EF347038D93A48E42D,
	HideTapToPlaceLabel_U3CAddTapToPlaceListenersU3Eb__3_1_m1AA8691520D7DF4DE9A4D8AD73CC0CC243564E0B,
	SolverExampleManager_get_TrackedType_mD7140CA43F1FF42C74AF9A549C26783B93A945CA,
	SolverExampleManager_set_TrackedType_m93D5FD20B76FBB0DFE21ACAAE6E0FE4FEABCCEDF,
	SolverExampleManager_Awake_m4E769F29BFB0A89453A366CD03ED22513CCFF3CA,
	SolverExampleManager_SetTrackedHead_m509A9C92475C1A36517BCBBC0C06CDF268EB60F6,
	SolverExampleManager_SetTrackedController_mA270F274CD1E4F03B5EFA56C2C5CBB06037AEA1E,
	SolverExampleManager_SetTrackedHands_m83DC980F63DC0260A53EB143DE29BDCDC6411201,
	SolverExampleManager_SetTrackedCustom_m0AA12923EA60ED6DFB526677035BEB0576887139,
	SolverExampleManager_SetRadialView_m670CA059C79CABC24199F095C01278ECD46267F0,
	SolverExampleManager_SetOrbital_m461EF1ACCA0797E42DD2267A407BB376FB879F28,
	SolverExampleManager_SetSurfaceMagnetism_m6FF07116667BBEEDAFC0F764763AE79176D23C8C,
	NULL,
	SolverExampleManager_RefreshSolverHandler_m4414E721F34A92E45708BB6B722D7F9010105D06,
	SolverExampleManager_DestroySolver_mEA4002B933892316B9F99E63A9E81AECE5694724,
	SolverExampleManager__ctor_m9246B15AE77ACCA0C5561E0C47F584C92332C0C8,
	ClearSpatialObservations_ToggleObservers_mBAB6166FC8149B938DDC6D9AAC0EBF95FEA20A75,
	ClearSpatialObservations__ctor_m1E0D835CD191371274E4684286C6B89F3F429208,
	ShowPlaneFindingInstructions_Start_m247E693FF28DFAEC5CDEACB5663C0910FD6EA90A,
	ShowPlaneFindingInstructions__ctor_m3A95CA6D0732D9CCCFCBEBD979EA091EBA53E106,
	BoundingBoxExampleTest_OnEnable_mF5699F759D368C849DEE2C19385F36471CA602C6,
	BoundingBoxExampleTest_OnDisable_m2B097058C7B209E3F3F1E9A90B0F10C1289EDE6B,
	BoundingBoxExampleTest_Start_m42BE5174E19AA69CB82DDABFF198774CCC798C7D,
	BoundingBoxExampleTest_SetStatus_m31062624D016BAE3E3CD5184D561C99807C60842,
	BoundingBoxExampleTest_Sequence_m0A42C1BD0F1E8F21CD3D8C3008AEB03AE3AE2240,
	BoundingBoxExampleTest_DebugDrawObjectBounds_m5114ED7B2CC59FA2FFA8A219CD083B8FE312669C,
	BoundingBoxExampleTest_WaitForSpeechCommand_mBC53747078DBA387C49DB0E4848A9329E34CDC0D,
	BoundingBoxExampleTest_OnSpeechKeywordRecognized_m1093AC9F44847EA731215651AA64651FE0801F0C,
	BoundingBoxExampleTest__ctor_mF2E87AD7C2294A6C60EBE7B9F276AA9A8A9370C2,
	BoundingBoxExampleTest_U3CSequenceU3Eb__12_0_m4F95AD13CBBA6BB3ED84AB44126AD26BC933DB67,
	BoundingBoxExampleTest_U3CSequenceU3Eb__12_1_mD27CA529D7D5FD14AA5EAE422601F8CC5E583AD2,
	U3CSequenceU3Ed__12__ctor_m34250F7FFD94CF51D0BA8F66FAA696A74EBED421,
	U3CSequenceU3Ed__12_System_IDisposable_Dispose_m1EA4A1FA54F48C6410C00ECF692E454555F19E2A,
	U3CSequenceU3Ed__12_MoveNext_mFDEDE85DFB303C120702369CC6898588F49C022D,
	U3CSequenceU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m966AE4CFB2CB37D234767C012EC7095857E00B82,
	U3CSequenceU3Ed__12_System_Collections_IEnumerator_Reset_m847F3DF0C15BDD6E2D270512B111204A7A1028CC,
	U3CSequenceU3Ed__12_System_Collections_IEnumerator_get_Current_mC6C5FB9C66089B57910B291CC5B9B396A2B6D0B5,
	U3CWaitForSpeechCommandU3Ed__14__ctor_mB5943136E45C53386CF88C696988515A51E4A9A4,
	U3CWaitForSpeechCommandU3Ed__14_System_IDisposable_Dispose_m241B40EFB3E896C86BD1572454BFDB2D695FEE84,
	U3CWaitForSpeechCommandU3Ed__14_MoveNext_mA7D26C53C3E266512878588E29CEAD493F5F9C5A,
	U3CWaitForSpeechCommandU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m10D6B994F0EF6EDF684CC5C0A0AC7B33EA6316EB,
	U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_Reset_m3292A30EC9B1AEAAB25EEF38748317038D3881FC,
	U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_get_Current_m2DBE575A1FE49BAEB66D584B50F2C0A91772C4B2,
	BoundsControlRuntimeExample_OnEnable_mD8784BD79AC23038F9A2687773D59FA91F933560,
	BoundsControlRuntimeExample_OnDisable_m2E1D8525126090E2F48ED4B15BFBC958709B936A,
	BoundsControlRuntimeExample_Start_mCA617717AF8B12A189B9742E38C4FFB4B88B5416,
	BoundsControlRuntimeExample_SetStatus_m40DBB2066DB922FCAE03D30DCB67952BBD51B883,
	BoundsControlRuntimeExample_Sequence_mADF6858AFE4D2FAB573185743D611BD3C7C26CCF,
	BoundsControlRuntimeExample_DebugDrawObjectBounds_m983446412CDCE92FD973C904A3239BB29E0A7784,
	BoundsControlRuntimeExample_WaitForSpeechCommand_mBB8AF5BD87EFE0B9311F1327DB13626525442F3C,
	BoundsControlRuntimeExample_OnSpeechKeywordRecognized_mFD7B66318D4D2CFA12A3DA6C14A6BE7A9BF2DB0E,
	BoundsControlRuntimeExample__ctor_m0F00BA1BCC38CBEA3861B95DACE6E98AFEBFB2F2,
	BoundsControlRuntimeExample_U3CSequenceU3Eb__12_0_m36BA64048BC4CFEE4DA52E5D2146DA1F21C6B826,
	BoundsControlRuntimeExample_U3CSequenceU3Eb__12_1_mC4E12695D5E3AF83B72379DE584C30395A510730,
	U3CSequenceU3Ed__12__ctor_mB596D0726FB14CE0546E56694FB5B2CACB0CF45C,
	U3CSequenceU3Ed__12_System_IDisposable_Dispose_mF11A785D0A2418CAA49D61132D659F18559AF866,
	U3CSequenceU3Ed__12_MoveNext_m7604948CBEB77C5759BE0CB6DF15D7DCA93F1295,
	U3CSequenceU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5F0E02B2B5C6D0F72716E6F36D0C6639ECD72379,
	U3CSequenceU3Ed__12_System_Collections_IEnumerator_Reset_m43D114449892836ED049B28D22F2E047E47C3D6B,
	U3CSequenceU3Ed__12_System_Collections_IEnumerator_get_Current_mE027BFD6EE3F6E9724BAC26E0B59C0F8C2C35A7E,
	U3CWaitForSpeechCommandU3Ed__14__ctor_mC11BB974A74E2A51DC40095516466073D190FF36,
	U3CWaitForSpeechCommandU3Ed__14_System_IDisposable_Dispose_mF17BD015850AF82D586F2E502A0F98DFDF1CF681,
	U3CWaitForSpeechCommandU3Ed__14_MoveNext_m6948658B17A39BA0B2F4506F82C2F9746602C9AC,
	U3CWaitForSpeechCommandU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C339347BA3254B9C3D4ED05B7EC66545A274319,
	U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_Reset_m546A0C9B8314A5766CE101397F9E7DC39CB10DE6,
	U3CWaitForSpeechCommandU3Ed__14_System_Collections_IEnumerator_get_Current_m6DE668CFDD0D0BCCFAE54D3D6F21AFAE511AFB38,
	GridObjectLayoutControl_NextLayout_m1499CAA82D16FBACE09A80C507B8352E1B102304,
	GridObjectLayoutControl_PreviousLayout_m285230E1E2B8C2F32DBBC5BCF78EA1BC258727C2,
	GridObjectLayoutControl_RunTest_mED12A96A86E2B58A9884E045802877B0E984539D,
	GridObjectLayoutControl_Start_mFE0051D0D2D35F8B617F8F7FDA4CBD9ECA781751,
	GridObjectLayoutControl_UpdateUI_mAD29EFFB04BEC522668D753A6574D356F868CF87,
	GridObjectLayoutControl_TestAnchors_mE3AA69D08ED0CF4432BFBE26768657FA5755555E,
	GridObjectLayoutControl_PrintGrid_m2621F3217BCF6A73D69EEF218346904962E0F71B,
	GridObjectLayoutControl__ctor_m0E595A658BED0BAA77CD0CC0FC1359315AC9F73B,
	U3CTestAnchorsU3Ed__7__ctor_m431EC69B94B62747171BF91FA9EBA93C74F1AD56,
	U3CTestAnchorsU3Ed__7_System_IDisposable_Dispose_m8C7D9B0F8F63A73C58000A6C42F7E017F5A01BAA,
	U3CTestAnchorsU3Ed__7_MoveNext_mA0BD8E63446DA71018E8226B0AF287C0C1C9E0C7,
	U3CTestAnchorsU3Ed__7_U3CU3Em__Finally1_m94CE60E49131A18A6125AC2B2920D2C60B1AC8DC,
	U3CTestAnchorsU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3C0EB33A01381805945D1A8E56E4D2794FABEFE4,
	U3CTestAnchorsU3Ed__7_System_Collections_IEnumerator_Reset_mAC0260AC48A259E59C1D6B027E9A1461C84A50AA,
	U3CTestAnchorsU3Ed__7_System_Collections_IEnumerator_get_Current_m8537D1C2951BE0CF00056F85E74785D071BE37BC,
	ChangeManipulation_Start_m9B5859543BE13431D4E6F3A350EB4F4BC59739AE,
	ChangeManipulation_Update_mB7214E4AA6802C60FF959EF4DFA1FBED9D772865,
	ChangeManipulation_TryStopManipulation_mA82D4D8EB72A25D1F90B69AB9A081DFA41B49575,
	ChangeManipulation__ctor_m5A7AEE890A234FCE014AD4D42B7F5F36E2AB36E7,
	ReturnToBounds_get_FrontBounds_m5D51CD36EB3B67CAEE1F0C5C8B7752F062441363,
	ReturnToBounds_set_FrontBounds_m93122CE58D003DB5724530FB71988C6E393CA35F,
	ReturnToBounds_get_BackBounds_m2D8CACB168818D5473120072F3B355B9CFCB90BF,
	ReturnToBounds_set_BackBounds_m38AFF96BF3EC6F5B91CCFD945E769560EBCEC01A,
	ReturnToBounds_get_LeftBounds_mE3FECA465276F8E6674C007CAA9853CEB5DAC000,
	ReturnToBounds_set_LeftBounds_m0FC9854D68BA0B5BC941308AB94E28FBE2944783,
	ReturnToBounds_get_RightBounds_m853F1CCFCDCE8096557BB2EBAA026DCFDEFF30B0,
	ReturnToBounds_set_RightBounds_mC394B19424E76DC0DDF18625F2A8DA055403D668,
	ReturnToBounds_get_BottomBounds_m421C9034BAB83EFB5FEDBD104B00A0EDAA02019C,
	ReturnToBounds_set_BottomBounds_m959F6831D884B544087EB90ABE463F5A0B3D6DAC,
	ReturnToBounds_get_TopBounds_m74584E35257F2761C3F5F4A9AE179B61AD5A5885,
	ReturnToBounds_set_TopBounds_mE6AA5D30B720C8362FA11AB5CC149EE64AD32F5B,
	ReturnToBounds_Start_mDED4547120AC1099709C47D01BCD4ECE064B74C7,
	ReturnToBounds_Update_m673D1B21CAE747CBB605DBB115FD107D4217F515,
	ReturnToBounds__ctor_mDFD3274B3143B8D6BDD47B611D4C9CE4FF284EF2,
	ProgressIndicatorDemo_OnClickBar_m0669C13770871FBA4D50E9D0B8CE1BA94D2C7938,
	ProgressIndicatorDemo_OnClickRotating_mB38E459130B2EDCB290F3122A96AE6B696FD4F6D,
	ProgressIndicatorDemo_OnClickOrbs_m3EC71B07E84D63A79BBDE831E9DE6CA03EBC5723,
	ProgressIndicatorDemo_HandleButtonClick_m83BDE61656ADAE9FA660C25015AF0ED3CE1489A2,
	ProgressIndicatorDemo_OnEnable_m23C6807F2ACC1D876D31B851A1B47D9F530CDFAE,
	ProgressIndicatorDemo_Update_m6A6926AF826F45B1BEEBB84E3EC3B714EDA4FFA8,
	ProgressIndicatorDemo_OpenProgressIndicator_m21F6477AF93FE257DF3D39A22301C610B7AA2EC3,
	ProgressIndicatorDemo__ctor_mC6F99148E68725F7FECBDF033BF9BFE6834C1FC8,
	U3CHandleButtonClickU3Ed__14__ctor_m848977C20B13093F1E9EACBFC57FA09F7455B1BB,
	U3CHandleButtonClickU3Ed__14_MoveNext_m4AC6E6BA1609365296EED5AC2B8638D9251931DA,
	U3CHandleButtonClickU3Ed__14_SetStateMachine_m6763E06BEC587E1ABE97242AC32B2C93DA60BB30,
	U3COpenProgressIndicatorU3Ed__17__ctor_m6E691D5CD4FB79BEE77CF65B876075D95A35466B,
	U3COpenProgressIndicatorU3Ed__17_MoveNext_mB1E044A6F8C94FD36A0D2D409E2DEF69BA395066,
	U3COpenProgressIndicatorU3Ed__17_SetStateMachine_m8402426A9BB1212956FF5CDA08A962A3FA14D274,
	SliderLunarLander_OnSliderUpdated_mC16BE001428FDAF910F4CF7C61305207EC7EF333,
	SliderLunarLander__ctor_mB94D8A45DEC6B5C3BB0D135768EDE4FC38DBAF63,
	MixedRealityCapabilityDemo_Start_mDB1E25EF111E12F6EE4F0F10629AE2B8ED9248A4,
	MixedRealityCapabilityDemo__ctor_m261D71EA1F79A30189D5890748E17BDDAC1215F9,
	ReadingModeSceneBehavior_Update_m2F659AD4FB9815286524185BA754A81EF4C90DCC,
	ReadingModeSceneBehavior_EnableReadingMode_mDF52B1777A6F182347152396BEE754B22B1F991C,
	ReadingModeSceneBehavior_DisableReadingMode_m8E98A3230FBB49929A665A7C5AB5ABAE9436FDF2,
	ReadingModeSceneBehavior__ctor_m65BFF2ECDB24DC6FFBAB18658956B1F7D394B6E4,
	ColorTap_Awake_mD7C1B08ECE260E2297A99D4DE1CD09CB45A8BC1A,
	ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusEnter_m6E44F2727094C8EBF8FCC9958CEF0FA2056A0756,
	ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusExit_mAD8E03E07BE24EA179115A8DDE719EB11B61E214,
	ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_mB5900C15340DA3DABAF4D9E251638AE616D8D270,
	ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m4DF037B7B105FB94966D9B680B514E6D1D60A774,
	ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m735B13348165A2E9FDC807EABE2B46A684F6348A,
	ColorTap_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m34EBD45A2E1601302609152233B5548B0F5175FE,
	ColorTap__ctor_mBEE7EFEC383886F34D6B31808A60C8CB909AFFBB,
	FollowEyeGazeGazeProvider_Update_m1B5E7680F82F544506762FC2E796FB3A168C0E70,
	FollowEyeGazeGazeProvider__ctor_mE290DF9824F98B561DED1EF303858488DE0BB2E8,
	PanZoomBase_get_EyeSaccadeProvider_m54122982D1F541DC3B9BD123A277676A041B503C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	PanZoomBase_Start_mB2AC9F50D6E4CDE24357BB56C46A5E2497D19F44,
	PanZoomBase_get_CustomColliderSizeOnLookAt_m613A2D50BFA678C0F17548AA4D41E2587971AB53,
	PanZoomBase_AutoPan_m9C44A4C5452309E37AB0BDA46919E8E215C36789,
	PanZoomBase_PanHorizontally_m87DC9B06CBB3939D282DB83075F0813346184979,
	PanZoomBase_PanVertically_mCD7E30718310841D216EEC36B9F76CC811EDBBC8,
	PanZoomBase_EnableHandZoom_m1E702AFDD5BCEA24E1ED1853D0651C20C328D135,
	PanZoomBase_DisableHandZoom_m17EB5237914603616422A93CB367B415FE97A3CF,
	PanZoomBase_ZoomStart_mC8BC6AC9AE224B2FB82FB92E6566B9F6B407B744,
	PanZoomBase_ZoomInStart_m12FBEE8B853EB825BF5BEA07D6B7EB11BE1AE20E,
	PanZoomBase_ZoomOutStart_m14C030096D7114826959DAC20F70AB282B7421EA,
	PanZoomBase_ZoomStop_mDDB1EA77FD19AD798650A29BFE9CB0EB924D7D83,
	PanZoomBase_NavigationStart_m45E26026D5B1300E61FE56A03F6B2213CDBFCAD7,
	PanZoomBase_NavigationStop_m827D2FF96466349D96A118EBE0ECB04BA7C15EA6,
	PanZoomBase_NavigationUpdate_m7C484203D9CD7694E091AB0A7C0DAC1C61D6BFC9,
	PanZoomBase_Update_m00F45CB496D3631BF0ECF92278C2B3286150E8FF,
	PanZoomBase_SetSkimProofUpdateSpeed_m4699A1857A766D3248AF8B3DE9CE91F8F7799F88,
	PanZoomBase_ResetNormFixator_m47151A514877BA6094235191DDE84BC83363D92C,
	PanZoomBase_IncrementNormFixator_m30ECAE4BB7A61BB17CFDD3771D058856E9A9F67B,
	PanZoomBase_ResetScroll_OnSaccade_mE6268E2B9B8D2DB5E2161406B36108AD05D64E08,
	PanZoomBase_LateUpdate_mCBF740F4D940AC7772456D15E85662E54384789F,
	PanZoomBase_AutomaticGazePanning_mD2990C63556FEAD2E7DCC7B5F695B1E9071D3BBB,
	PanZoomBase_get_MyCollider_mDF7F61FAD9952D54BA5EBAB60B08D0D21CA3B38F,
	PanZoomBase_set_MyCollider_m05BBD9058905EA503703B2C0B31B33F18922330B,
	PanZoomBase_PanUpDown_m5A78755C13BD68821BC544DE6675A90AE54169E2,
	PanZoomBase_PanLeftRight_m38B9B3671A522A4A9EB7D794429A34DD3157011E,
	PanZoomBase_UpdateZoom_m1CC7ACF55C1C36D719FFD0DE85FA42F192B7A98A,
	PanZoomBase_LimitScaling_mF3A761450398BA48138F31672A25B437382C1F96,
	PanZoomBase_ZoomIn_Timed_m83891CE9E6A77964A252587EBF9DBA50EB038976,
	PanZoomBase_ZoomOut_Timed_m2A4E50E77F583A0B534BC31650E857CC126605F6,
	PanZoomBase_ZoomAndStop_m70C71BF56D1145AB3CDF2E00A8EB01D238FBCECD,
	NULL,
	PanZoomBase_StartFocusing_mDCEB350C0CC6666D3B1B05153662A2D46B057F33,
	PanZoomBase_StopFocusing_m4168EC474B568F3C6E68BE20035FCE84BCE7629F,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_mB2B6486DC654182010BB9CCE877A183C5CC86F8F,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m80F04936779BA459AAC68DA9E10E9A9416374662,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m11D3CA9CB5A43BB170A9311F61CE08EBA8AEB50C,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m22BA6F218C16DC807D6FA43F51FBE011C68C1387,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusEnter_m2C8A1F5B2F6373FFE26C6F33339B2AC4FF95B4F3,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusHandler_OnFocusExit_m5D9A62A14CBC02469086895CC60039490BD4B594,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceDetected_m6F1D3E4E95248CE8CD902CE1D43FE2E92E3EEC74,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceLost_m30DD157C7E9D8951BAFA2882776A48E33756AEBA,
	PanZoomBase_Microsoft_MixedReality_Toolkit_Input_IMixedRealityHandJointHandler_OnHandJointsUpdated_m0C96D00490A1DDD90055D4833606476271408577,
	PanZoomBase__ctor_m24873AA0FC327596CDD96E8BE0C37690D939DEF1,
	U3CZoomAndStopU3Ed__78__ctor_m46258496853C97A3C755A49B27F1D14CBCDA8AC4,
	U3CZoomAndStopU3Ed__78_System_IDisposable_Dispose_m3125E7B1AF6696673562538B404794532B5C6581,
	U3CZoomAndStopU3Ed__78_MoveNext_m2C96529214D8702563C7965B55318600265CB607,
	U3CZoomAndStopU3Ed__78_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5007A408115112E9A302CE1D2D2D6C8912A0B878,
	U3CZoomAndStopU3Ed__78_System_Collections_IEnumerator_Reset_m377FFA4C86BEBD8E6385A94CAFD88AC4F7389826,
	U3CZoomAndStopU3Ed__78_System_Collections_IEnumerator_get_Current_m40F497BEDB0DA316498CD3F4F0964E601517371A,
	PanZoomBaseRectTransf_get_IsValid_m5D29DF4CB76F95941777C7A6BB307AB49E6BC01B,
	PanZoomBaseRectTransf_Initialize_m304AB17FDCF5BA852AF94FC535610C4CC13E7435,
	PanZoomBaseRectTransf_ComputePanSpeed_mFFA0C0424D22DCEECD2C4B8F5611565436571DBB,
	PanZoomBaseRectTransf_ZoomDir_m612F30B26B787EB6188A88028AE85E4A1B6C72C3,
	PanZoomBaseRectTransf_ZoomIn_m4A9318A8FA4A71EAD40854278E99B72620075084,
	PanZoomBaseRectTransf_ZoomOut_mF0DB9B8493D9798EB33E0AFA7A218BF0AF480B4A,
	PanZoomBaseRectTransf_UpdatePanZoom_m3894AC6AD57A0ACB8BDFF78C7E1854F31B939063,
	PanZoomBaseRectTransf_LimitPanning_m894E46A488DDC62BA5CDCA5AB7C5A933A8A5F9D6,
	PanZoomBaseRectTransf_ZoomInOut_RectTransform_mD0BE9A3412C516F2553E7FBED0972793CEF45FDF,
	PanZoomBaseRectTransf_UpdateCursorPosInHitBox_m3B3AD8F39A81A1F91FCA4AE2F2AD4DEECA589C72,
	PanZoomBaseRectTransf__ctor_m11AB56AF46AE8BB2F20D994F6BED8B0573148F26,
	PanZoomBaseTexture_get_TextureShaderProperty_m4B45BEB6957C6D3FD827E779DB034D500DC97997,
	PanZoomBaseTexture_set_TextureShaderProperty_m772C0F604825DB0A208BE1B660796DB03260F722,
	PanZoomBaseTexture_get_IsValid_mCE13EEB04D55551F584DB342CD76C1D76E56B71D,
	PanZoomBaseTexture_Initialize_m8C7221CA442C5308A726ACB64BC04FE7329AC99F,
	PanZoomBaseTexture_Initialize_m05A14D9F9F033E583D5D601378A6750911205561,
	PanZoomBaseTexture_ComputePanSpeed_mD4AD55BC5485C2C65F48143A9EDB7DE8F5C848C0,
	PanZoomBaseTexture_UpdatePanZoom_m3B9D782F6D52133334A9FBDB699CAB6BE353E3A8,
	PanZoomBaseTexture_ZoomDir_mDAD8C9A1472E8A9D60A82867E2F61C2BA46D3CFE,
	PanZoomBaseTexture_ZoomIn_m21E4133ADDD195303FEE61014F5BEFFDCC474397,
	PanZoomBaseTexture_ZoomOut_mA9E5E70557BB57E307D9CC56BE6BD5C7A61EFDB2,
	PanZoomBaseTexture_ZoomInOut_m24ACD9B538DAE3E0B6649F45D936CAEC4D081EF0,
	PanZoomBaseTexture_UpdateCursorPosInHitBox_mBD58ACC99C53F27EB3762979D3A611083A5E21E9,
	PanZoomBaseTexture__ctor_mDBCA76CCAABABB178FA4C8D80C757D3CCC91D1BC,
	PanZoomRectTransf_Start_m27D8EDC74352F194FB3327D4807D1D1A32B012B1,
	PanZoomRectTransf_Update_m04B2A6C04DB780365FFC1A9E89BFF0B75F4507C3,
	PanZoomRectTransf__ctor_m64773AF9E1050728E16DD1BBDF7EAFFDEAEFDA01,
	PanZoomTexture_Start_m988C5BEA2E31A4DE6537237C5B9EDB00C8E15CD9,
	PanZoomTexture_Update_m55FA693B468DAF2C500041B333168091D851DAD8,
	PanZoomTexture__ctor_m7456429EE8743EEB9BB9F89A8C56E9CA731778DA,
	ScrollRectTransf_Start_m1A80351FA46983239AB43136A7A64B0D26E7AA86,
	ScrollRectTransf_UpdatePivot_mA8C57285A8BD701CE048C1242DF15C146A4B04B0,
	ScrollRectTransf_Update_m7927734A419213852CD8551DF8462E214DD60483,
	ScrollRectTransf__ctor_m4AFE6A078A07889149870AEC308EDD6027A06BBB,
	ScrollTexture_Start_mCDC3C04C78ABE8C199C71D26434744594EF35435,
	ScrollTexture_Update_mF33FD2AA64F1B11074C6566F7DD65FABCB26B7E1,
	ScrollTexture__ctor_m4EA45B40632FD8F435A91F1C3D746D8303DD58F2,
	TargetMoveToCamera_Start_m72D92E90A57197B12903E158E4B576C3E25DA470,
	TargetMoveToCamera_Update_m284F3EACD09398F6528DBEDE0F344D406E0A9F50,
	TargetMoveToCamera_OnEyeFocusStop_m126BEFCE18BF4127DA5F6616BA3C3C88FEE8EBE2,
	TargetMoveToCamera_OnSelect_m8785E4CEA463BED545179F47C1C9CD8B8BEE9A88,
	TargetMoveToCamera_TransitionToUser_mE30CBC5A23DC23FC9472BF63B774C5D491077D12,
	TargetMoveToCamera_ReturnHome_m39C5FA950E85529D6C4471501D4BF63DE0A54A10,
	TargetMoveToCamera_TransitionToCamera_m4C90FB2B0849B3ADCB7452EB93CB62F96CB336FC,
	TargetMoveToCamera__ctor_mA92726478EE7996B37EC09376DACC1D093B6C990,
	GrabReleaseDetector_Awake_m2963BDEBC9FD6BE02DD8C9D0C4CB786DE3FA7D55,
	GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m4A611D12979750CB06A4EED2D2B633F9CCE71D25,
	GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m54A6050F9D224F51E6F3CDA582E438D436B1A018,
	GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m9612388FAF341C7237A2017F9F07ACE436647953,
	GrabReleaseDetector_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m72853C89D7E9F30CE6F12A43D3AEA2D59D3E28ED,
	GrabReleaseDetector__ctor_mEE4B722D4A1972E140F74E7F6F327CC8E3D0478B,
	MoveObjByEyeGaze_get_EyeTrackingProvider_m336C129B68E5A3529AB3E7A056D62D900190CD83,
	MoveObjByEyeGaze_get_ConstrX_m48BD1AE4EBC4675A967A6C2241E2252447AA4A66,
	MoveObjByEyeGaze_get_ConstrY_m599664BA2429E9B6EE18C6063DA234FB3E1DAAC0,
	MoveObjByEyeGaze_get_ConstrZ_m9BC540EC9B034AD4A92FA9388D94229C0DCFAC4D,
	MoveObjByEyeGaze_Start_m0F33CE4286DFEEB158ED7DCFB68C968245D4FCEF,
	MoveObjByEyeGaze_Update_m44A1FBF92CD8632470AFF5D79EFC6AD8F37421AF,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySpeechHandler_OnSpeechKeywordRecognized_mB9F589637679B37E4BDC6064F21719A9790A4D1B,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityHandJointHandler_OnHandJointsUpdated_m71910ECEF3CEC5139583564A72593D8139D33C46,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceDetected_m73714E747BB21C58C7EE6728A45C15F46F7A87C0,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceLost_mB3FB97B77A33343E1752E418A5884ADFF5029528,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m674BA79F19175ADE919700CE681F299AAF8FFC55,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m81FB02E5750AE48D25B7348735B0FBE06C4FE0AA,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m33C699C65EE1B4FC84A6F69351E1C8E2C0A5A6DB,
	MoveObjByEyeGaze_HandDrag_Start_mD3A04FBAB7007BF88778D89FB1F6A51F158AC2E4,
	MoveObjByEyeGaze_HandDrag_Stop_m2681D5C0EBBB6CF997F565EDE16808B7F6B2D681,
	MoveObjByEyeGaze_IsLookingAwayFromTarget_m622E3E51CEA34044427CAD693EE1FE8682F4EE12,
	MoveObjByEyeGaze_IsLookingAwayFromPreview_mBAB6326B5BE5313FF34EC1D6F1B71873065C17A0,
	MoveObjByEyeGaze_IsDestinationPlausible_m8CC26A9F21323171CF4E93658451E4F1C0A8173B,
	MoveObjByEyeGaze_GetValidPlacemLocation_m9E6457DBBDBEE091F5BAF0849EF39C26F0FFAA5B,
	MoveObjByEyeGaze_ActivatePreview_m667CF698B7713081E3A111B78DAD822D0F0A73CF,
	MoveObjByEyeGaze_DeactivatePreview_m5C4DF9D16DDAFE8A8EC82C9A906D763EB5B9E918,
	MoveObjByEyeGaze_DragAndDrop_Start_m88EDE02D1A9076A1142C4C2D489E1B36914B089A,
	MoveObjByEyeGaze_DragAndDrop_Finish_m8E85507C87DA918B7DC4E78CC9A141546863D632,
	MoveObjByEyeGaze_RelativeMoveUpdate_mBAE5199A648F488784AFD4CE50F4288AE97144F4,
	MoveObjByEyeGaze_Angle_InitialGazeToCurrGazeDir_mF23EE5E7B9CD89DE0EC10D565DBE87F24C665240,
	MoveObjByEyeGaze_Angle_ToCurrHitTarget_m1B9E32722BDE0569E78F5C0EA3169388E98D828D,
	MoveObjByEyeGaze_HeadIsInMotion_m9037DA4115540A814AF3B5EC575601E5D11C9E88,
	MoveObjByEyeGaze_MoveTargetBy_m8908213B4AAE31C4111832861EB63695EB5F3790,
	MoveObjByEyeGaze_UpdateSliderTextOutput_mFDDBB80EEC30810500286A813A0E2FE154EAB5F3,
	MoveObjByEyeGaze_ConstrainMovement_m3DA7851E020B48428ACE50307479F08B14037F33,
	MoveObjByEyeGaze_OnDrop_SnapToClosestDecimal_m0C5F70697BD4F1CBFAC4C593C4CC14AB336EDED0,
	MoveObjByEyeGaze_MoveTargetTo_m228E30AA6B44CDAD0B22E121BD9A4D0AB2629CB3,
	MoveObjByEyeGaze_ShouldObjBeWarped_mC95DBF31981D139CA827E49BDB3735AC1D37F57B,
	MoveObjByEyeGaze_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_mCAE262E5B4CAA21620126A508A3D7AE640D6B5E0,
	MoveObjByEyeGaze__ctor_mBD819DCE9B8D5CC138F5382CABE3A200345EC3FA,
	MoveObjByEyeGaze__cctor_m7FB0427D658EE5A0E27EF2B9EAB2EE58D1E199F8,
	SnapTo__ctor_mF4D4880CE5DB9F6DB0ADABF525CA3D10E9F05D16,
	TransportToRespawnLocation_OnTriggerEnter_mA850BAFF9F11345F472E9FE28C3550EED9DFD0D7,
	TransportToRespawnLocation__ctor_m5D0DB46A5F515D6884DA71D276ED2110793A1B19,
	TriggerZonePlaceObjsWithin_Start_m246E45AB8167036A4B02D465FCAE86A814ED59C5,
	TriggerZonePlaceObjsWithin_OnTriggerEnter_m98916B848FF7D1E4085679CDB26987125743F21A,
	TriggerZonePlaceObjsWithin_OnTriggerExit_mD086942CCEC41280C4EFC29EBB7F78253A3DD880,
	TriggerZonePlaceObjsWithin_CheckForCompletion_m440150358C1FB7755D603901AA9F78DE4219F1DA,
	TriggerZonePlaceObjsWithin_TargetDropped_m859CFD75A06F26401E998D0FB0608CD67FB36DB7,
	TriggerZonePlaceObjsWithin_AreWeThereYet_m01BCD8624946ECA4D29571A385B6D28F286D0BB8,
	TriggerZonePlaceObjsWithin__ctor_m867D05BC3A41D66ABAD1A8E1747ECB2968BF0336,
	HitBehaviorDestroyOnSelect_Start_m4CB517A6B8F87AB4CBEE53D3EC38F517373ED2FC,
	HitBehaviorDestroyOnSelect_SetUpAudio_m8B0F3E98DCCC6CE0166ECFA4FD1A2CCDA8D114E8,
	HitBehaviorDestroyOnSelect_PlayAudioOnHit_mF75E687F7E41FB5E38368B3EF48845DDC47524F2,
	HitBehaviorDestroyOnSelect_PlayAnimationOnHit_m48C4E431021FF0BC4BE9FC1601CEBC717040F19C,
	HitBehaviorDestroyOnSelect_TargetSelected_mF86B135B771C313788A5AFE96273E3B6EF1A5A6E,
	HitBehaviorDestroyOnSelect_HandleTargetGridIterator_mF145FAD82C3A4252205D46B459D942674DA76994,
	HitBehaviorDestroyOnSelect__ctor_mE8E52B5245DD6F5675080A0C9194BE2BBC8ED6A2,
	RotateWithConstSpeedDir_RotateTarget_mD1B5545675EB19EF4F9E8E9D3360CBC880057C34,
	RotateWithConstSpeedDir__ctor_m995649A4F8887A7A33AF4BF206AC6699B1D375CD,
	TargetGroupCreatorRadial_Start_m66F301C7C6384FD65D66D96E82D96052EEE66F80,
	TargetGroupCreatorRadial_Update_m8DD24A9F114F6407CDB2B19A2664A54BE46C5702,
	TargetGroupCreatorRadial_HideTemplates_m7087FD60F8818065D450297639DF8FCA195F6641,
	TargetGroupCreatorRadial_CreateNewTargets_RadialLayout_m9FF8607025878F9EE0A480BD4188E5FCAC8D6D1F,
	TargetGroupCreatorRadial_get_InstantiatedObjects_mA358E93DF5E75935BD56C3B5F4F63CE50E7BE891,
	TargetGroupCreatorRadial_GetRandomTemplate_mE9D6EB4736337DA29EE58875756BEE0482A83546,
	TargetGroupCreatorRadial_InstantiateRadialLayoutedTarget_mAD79FC30E20B0C1C78F78103C1365B4D2E418020,
	TargetGroupCreatorRadial_KeepConstantVisAngleTargetSize_m58EE489BD05A461D07AE9101C831C94B19F6FD03,
	TargetGroupCreatorRadial_KeepFacingTheCamera_mFD42FD736FC11B4EC5521DF1A9A559C1974A194E,
	TargetGroupCreatorRadial__ctor_mCE2A8B91ACEA15746610603A48856EBCC0F8FBC8,
	TargetGroupIterator_get_HighlightColor_m083DDDF4865468A9B19ED3F7A42665B56F68F111,
	TargetGroupIterator_add_OnAllTargetsSelected_m6064AFDC7FD0A4F707C0C0B66CD34F2AA7BF872F,
	TargetGroupIterator_remove_OnAllTargetsSelected_m5AF09D842ECCE703EF50F6A8731AB37D8533AE62,
	TargetGroupIterator_add_OnTargetSelected_mBBDB4D90C8A878BAF5BFCF91388D545305DA2BD9,
	TargetGroupIterator_remove_OnTargetSelected_m18F381E83CBF9A077ABBAE7A92638A6AB32DED47,
	TargetGroupIterator_Start_m6ACE67059A5021DC332678CEEAED701914A3ABAD,
	TargetGroupIterator_Update_mBAF050C57E8B3B85E2F32A5409423B6ABCE487F7,
	TargetGroupIterator_ResetAmountOfTries_m74D6789CD72E33D75E98B9C928E2AF29A586E5C4,
	TargetGroupIterator_ResetIterator_m1B5E24F5FBFC313513F96AD730DFE508C0EE103B,
	TargetGroupIterator_ProceedToNextTarget_m9FFA11487537EDA6CBE333A8D2E8F1D785ED7389,
	TargetGroupIterator_get_CurrentTarget_m0ED64E1FFC9AC072EB9294E0934D28307E8E8F17,
	TargetGroupIterator_get_PreviousTarget_mE060A8E9C7B1188E30282799D2126356950D6EE6,
	TargetGroupIterator_get_CurrentTargetIsValid_m759CE7004BFFCDD9128660F9383D78C2D365AFFD,
	TargetGroupIterator_SaveOriginalColors_m551E7C5E83505E61D02D17D01C7CB4775F4C55F2,
	TargetGroupIterator_HighlightTarget_mF265B61BD7202A40AC23A25B22DB3FCBD20A8443,
	TargetGroupIterator_HighlightTarget_m41B48287652F6F159FF41DFEF70B72EC3B03CF81,
	TargetGroupIterator_ShowVisualMarkerForCurrTarget_mA8A75B955180BB6E96F06B7A879B3C1369476274,
	TargetGroupIterator_HideVisualMarkerForCurrTarget_mEAF942A0EF88BB64ADC71D40B93498373006FA41,
	TargetGroupIterator_UnhighlightTarget_m31A3A77529D9DE0DE658F6BC8F4DE786E0AE89D9,
	TargetGroupIterator_ShowHighlights_m1E62EE12181E2E91982EDE32F8FCCE77D152512C,
	TargetGroupIterator_HideHighlights_mA9C0CCDBE2A501818B6584058F7FA8F49CEE6BD0,
	TargetGroupIterator_Fire_OnAllTargetsSelected_mB6732EC948BABDDB37BDDE03C24FF55EFF44B320,
	TargetGroupIterator_Fire_OnTargetSelected_m1AF6B9C1FECA4F688473933656F384555AC78D6E,
	TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m9213592B0421CED1DFC9C04DB4AE33E45101893F,
	TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_mA603FEA3262922686FEA95C132F8EE9FFDE4810E,
	TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_mFCA335518E11A82D84E9DB94AB162C3AD105DE2D,
	TargetGroupIterator_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m070C70433317092CBC96732BAD2CFA4E86DCD5E3,
	TargetGroupIterator__ctor_m8E0F974EEE05CDB77907A9A4BED3B55D64B25A21,
	TargetGroupEventHandler__ctor_m3F732709E7AD11567FFDE713803ABA5A97A834D0,
	TargetGroupEventHandler_Invoke_m85E1C36EE9D152613F51B9C16940A215FF3996C0,
	TargetGroupEventHandler_BeginInvoke_m930A1ED6EEB1D8E246FC74E887523ACD5F723B99,
	TargetGroupEventHandler_EndInvoke_m4A8862A53841A96DC3B1656910229E8DE8B6B3E3,
	ToggleGameObject_ShowIt_m16D765A39E1A3692D3DBC9F9CA5E6F5998FD548D,
	ToggleGameObject_HideIt_m1068655D1624272B6270B1ED2D5017FEFBBE0044,
	ToggleGameObject_ShowIt_m9741EAA78D6170C2197DF5889D3AFBA9058817F4,
	ToggleGameObject__ctor_m50C0ABFA8FAA7F3EFBF8E31D411A8023EDDBEB65,
	DrawOnTexture_get_EyeTarget_mF9CAE5599F22C24F49F145274A482BF6BC88925B,
	DrawOnTexture_Start_m905A5F2E0BED0A32B40801512AA24322BC75EA60,
	DrawOnTexture_OnLookAt_mD7D300F123F51E805B4BC8A508E84E4274A38E70,
	DrawOnTexture_DrawAtThisHitPos_m675E2F1AA0DF5153F9AE4CF6D19472386A924C91,
	DrawOnTexture_DrawAt_mA6096EE3937FA04B497C2257FEBA1F667C0FEB34,
	DrawOnTexture_DrawAt2_mE0142B77FAFDFDEBAEF22E784E230236A43B81EE,
	DrawOnTexture_DrawAt_m5CCF53A676F5EF6B8E413C510A70B1ABD8EB42DD,
	DrawOnTexture_ComputeHeatmapAt_m4234A80A7ECCD004A0549561897E1FDCBF25898E,
	DrawOnTexture_ComputeHeatmapColorAt_mAFDE4C406957E36412932A3CBEEC244D00391029,
	DrawOnTexture_get_MyRenderer_mF322108589FA4083A7FBB87425225BA316A7531D,
	DrawOnTexture_get_MyDrawTexture_m6B6D53DFB9F2D728ADB8C349C3E026093CA58886,
	DrawOnTexture_GetCursorPosInTexture_m071AF778967026980B47777140A88241247A4ED9,
	DrawOnTexture__ctor_m0D39FD2D2B1D765EDA45F381CBC222A29E279F34,
	U3CDrawAtU3Ed__19__ctor_m0669E9BEA6952B3F3DD94D06CBB8ABE617E39B24,
	U3CDrawAtU3Ed__19_System_IDisposable_Dispose_m6E4D0EBCBAE804E9BCE39A65E328D04E34213B52,
	U3CDrawAtU3Ed__19_MoveNext_mB2543CCB3D3BF2C12A793FF9FFF206AB49B7038E,
	U3CDrawAtU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFB8FE5A3C65743AF09977A975BCC7D237891A637,
	U3CDrawAtU3Ed__19_System_Collections_IEnumerator_Reset_m928AB931B01F942ECA5D6B39A97E2DDB4B316A66,
	U3CDrawAtU3Ed__19_System_Collections_IEnumerator_get_Current_mCF5A234BE92720D5D3143BAA4F6FA6A2E7C5C7C2,
	U3CComputeHeatmapAtU3Ed__20__ctor_mB446BCBDF6A86972E2CCA7C4D41D2BC187574100,
	U3CComputeHeatmapAtU3Ed__20_System_IDisposable_Dispose_m037E1BB2B14C2C41D8DC547D74349C2DB0BE76FA,
	U3CComputeHeatmapAtU3Ed__20_MoveNext_mF905BCF702462540B727726EB149DD6DBB3D695E,
	U3CComputeHeatmapAtU3Ed__20_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01119328E1F744FF691CE5485CC2BADFC8766CC2,
	U3CComputeHeatmapAtU3Ed__20_System_Collections_IEnumerator_Reset_mAF888F7D9CB6C49243C9B4D75E73BBB14A77D906,
	U3CComputeHeatmapAtU3Ed__20_System_Collections_IEnumerator_get_Current_mA0A6E63023754E4F80F25FB667CE8E119770D05A,
	OnSelectVisualizerInputController_Awake_mCF057F60CBE20DBFEBA3B3FB85C3FF3E0E793B31,
	OnSelectVisualizerInputController_OnTargetSelected_m4A080DC91A1C2046FB3B771CAB925F7C253720AB,
	OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m04437D31608C0815803E06B0F2F6AA608E03E6A3,
	OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m386597D97037AC73236C5BFB3DAB5C2CC038E8D0,
	OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m3C274542CE438C15D38CFBD1908CC7A85C466DCC,
	OnSelectVisualizerInputController_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_mAB7BDF579FE2FAFA7F6357464FB96F6CA2E4412B,
	OnSelectVisualizerInputController__ctor_mE6025F87B108706D374A32810E2F3ACA8E273E1E,
	ParticleHeatmap_Start_m93A043CAAD81FBD2B433792F8A290DDAE4A59106,
	ParticleHeatmap_SetParticle_m3D5AB90D8727A3F3D67A3A30F9C1E892D10C8E01,
	ParticleHeatmap_GetPositionOfParticle_m4F62616C5737EAFB1EB5124DF8DEA1682E0C56BA,
	ParticleHeatmap_DetermineNormalizedIntensity_m63D6E0AD36B9FD32C896768D7860C0F4D568EA68,
	ParticleHeatmap_UpdateColorForAllParticles_mC7F07C6A43CE1C908135AE7B31A92CDE217863DA,
	ParticleHeatmap_saturate_mC50AE74CFB227532D21466D1B015426331CFB418,
	ParticleHeatmap_DisplayParticles_m794364EFDB05C334CB4D9144FB573EA03B8B71A9,
	ParticleHeatmap_ShowHeatmap_m0D81B1B4D45E6A2A16F60C13E1621A85913FF50A,
	ParticleHeatmap_HideHeatmap_m6584A67A3DD1F7DEB94045A4B9633A51A5E16121,
	ParticleHeatmap__ctor_m740DB1EBF60A2432EE5A3DEA0827F6E645958E2D,
	ParticleHeatmapParticleData__ctor_mAA7D86C0881A9CEC23CF85D0E066505DF476EF01,
	AudioFeedbackPlayer_get_Instance_m714F2501DD250566ABEF9269938D4F9FEFDBF577,
	AudioFeedbackPlayer_set_Instance_m3C0D461B9B45905B85975D30EFB5DCAACACAC802,
	AudioFeedbackPlayer_Start_m3F861DC14FFFEE6C1CD80D372815E621CFE3B66D,
	AudioFeedbackPlayer_SetupAudioSource_m1BF539B2278642BD4E2387360A159758D0460FD0,
	AudioFeedbackPlayer_PlaySound_m41CF59BFA0DF554B49CBD599ABC73A24B795FC9F,
	AudioFeedbackPlayer__ctor_mD99CFEC83CAC9BA797BFEDB7D14185DD1724AC00,
	FollowEyeGaze_Update_mEBA4DBFAAE8F0F71FFA529F1EBF006791AA1AFD4,
	FollowEyeGaze__ctor_m79A0A2BAE81BFAE8B9F7F812CC6A5D43FD9397FF,
	SpeechVisualFeedback_get_MyTextMesh_mA5BA5DA178D1165693194CE8769A2B8EABC71EC4,
	SpeechVisualFeedback_UpdateTextMesh_mC96EA13ED47E143D248AD06E4F1E1963A9822D8C,
	SpeechVisualFeedback_ShowVisualFeedback_m579D646436BF229B9080874D0077E451B433F38C,
	SpeechVisualFeedback_Update_mC26B26E519A2BC1241606008CE937EF5EEFB8BBD,
	SpeechVisualFeedback_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySpeechHandler_OnSpeechKeywordRecognized_m3FDD5FF7FACC4255A7155EEFA7E53AEF648899A9,
	SpeechVisualFeedback__ctor_m18571039E9FBDDFF98EACF883BC863CDC4D25566,
	BlendOut_Start_m241515FB431B398AEED3C7FC2BDF4323CBFA9233,
	BlendOut_InitialSetup_mF3AC41E6E11EE886D098A87D3BF2B826F2235487,
	BlendOut_Engage_m8F412B4D08313E477687DD3C6628135FDD97E50D,
	BlendOut_Disengage_m2767FCB7CC2493E82AB1435E37460E63312D1A54,
	BlendOut_DwellSucceeded_mD0282E1D6E921B7DDD06CED85CE20748D3D034E6,
	BlendOut_Update_m7D8A18B94B8C977C8721903E412232F98723AF1A,
	BlendOut_SlowlyBlendOut_m97616D3FC5021B30ECA72A62121BB6036FE24063,
	BlendOut_ChangeTransparency_m6C8883E832053D97CDE401CCCCE9B1288396EA0F,
	BlendOut_ChangeTransparency_mE8007D95762537707ED181DF1BEDBE329F9F7C5E,
	BlendOut_Materials_BlendOut_mDEB0306A1AD110807A36B8D0F6614960AFD07926,
	BlendOut__ctor_mB15101D9050670C620FCAA8E8E22391F6DAB000B,
	ChangeSize_Start_m18155EBAB41D111EC6354ACACF646614EDB0ACF0,
	ChangeSize_Engage_m3FF1769A92E5B4C43E4F160E8122B1D80C62AFDF,
	ChangeSize_Disengage_m6A97E4D4201E4B7896895E2A206A089797411C76,
	ChangeSize_InitialSetup_m1BCC0277A8DBBCD7D92BB77D2F45EA955CC07A75,
	ChangeSize_Update_m8333EAE80A526D567E7EF138CF0C931822D48FAF,
	ChangeSize_OnLookAt_IncreaseTargetSize_m40265EB1254E63C1494CB4D7D1A89A180A0E3AF0,
	ChangeSize_OnLookAway_ReturnToOriginalTargetSize_m2D68AA274997CC4B3F23B5900482F4AC33422AAE,
	ChangeSize_AreWeThereYet_m5EE4E70D7F62D95E9B44CE84B2F4660348478D38,
	ChangeSize__ctor_mCB5C8E44FBDDADD50970A11E6BF698C606E8DF66,
	FaceUser_Start_m8E335136FD670B14CC70441CF45879BDD42E77E4,
	FaceUser_InitialSetup_m4A48630823629C6A1D8640788BD051920BD32850,
	FaceUser_Update_m9B22F17D7FC9B88CEB23DB9D84F907F1AAED470F,
	FaceUser_TurnToUser_mE3F6D108819EB310D0CD91F3521034DCD67900BE,
	FaceUser_ReturnToOriginalRotation_m8B4B8C4A70CD01DB9005AA90624D25F616D3927F,
	FaceUser_Engage_mBC678ED36B8D1ECFB99C74E345C319A40F2440CC,
	FaceUser_Disengage_m53808BBD1D4CEB25EE80BDCC2B2376D7F37F30D0,
	FaceUser__ctor_m9FB1100FC8BC7455C34C91587C053625035FC83D,
	KeepFacingCamera_Awake_m2F44505EF068A0CC6BBA1C2A7776E4464B71EC9B,
	KeepFacingCamera_Start_m5C7C2CAA92807C442272D00547283C22ED09767C,
	KeepFacingCamera_Update_m6FAEDD89AEB88911435A094D72C9A21CA3FD6EE8,
	KeepFacingCamera__ctor_mDE9E1A8CD37235BB5FECA6B754DC0409EA1E0EB7,
	LoadAdditiveScene_LoadScene_mD0E07D6C03ECD2D6F104F61FFDC3A31B2BDDD624,
	LoadAdditiveScene_LoadScene_mEFE9FC059E9A875E326EB76C2B38051CBFC00135,
	LoadAdditiveScene_LoadNewScene_m8369E583996CD92D5D211F38E219CDAA014E43FE,
	LoadAdditiveScene__ctor_mA5E6B5B24C523261916D49F98A0444766D23428F,
	LoadAdditiveScene__cctor_mADAD19F88CA77993C2A9A031A40829A87D8A291C,
	U3CLoadNewSceneU3Ed__6__ctor_mAAA7D7B2D2A941AB339DDE43B676DA5EB7C3F25A,
	U3CLoadNewSceneU3Ed__6_System_IDisposable_Dispose_mAC49FC02DB74158443E728F2B937460842721A3B,
	U3CLoadNewSceneU3Ed__6_MoveNext_m93C69EFE0389088D33ADFF071445E4E7FDA19CA2,
	U3CLoadNewSceneU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9CF41C95C56DA29A06DC641A6B10B958B14D43D3,
	U3CLoadNewSceneU3Ed__6_System_Collections_IEnumerator_Reset_m1F4F5F43E8C334FD2178951548219825F2978BA6,
	U3CLoadNewSceneU3Ed__6_System_Collections_IEnumerator_get_Current_m7D090F16DFB2D3B4CFB78A558428242744FEF519,
	MoveWithCamera_Update_mFF19AC0F6FB0A882B3E13B0DABC565093498514C,
	MoveWithCamera__ctor_m6FF81EC64D652E5ECAF80EDBDD722D0CBA27E08D,
	OnLoadStartScene_Start_m09F7D0D4AE5E8B1BF0B19E5755795848FAD88CDC,
	OnLoadStartScene_LoadOnDevice_m95428AA284DEA8058CECDFC1AD17E9AF22153C3E,
	OnLoadStartScene_LoadNewScene_m79AE41CB5137BE74B0B5F3AAD74F724DBBCAFF3C,
	OnLoadStartScene__ctor_mD6D116B5C7DA8558CFF1BD17777DB8EBD1338747,
	OnLookAtRotateByEyeGaze_OnEyeFocusStay_m49A0360C7A86BF18977B595F9194CAAAF9EE1245,
	OnLookAtRotateByEyeGaze_RotateHitTarget_mC14CB1AFB8C50D6B04A67E727B792EDD59DA99F9,
	OnLookAtRotateByEyeGaze_ClampAngleInDegree_m06322835FFEC2E6565E9DB7B511C57A61A054565,
	OnLookAtRotateByEyeGaze__ctor_mA3C99F72CC060205765337FED5DC28F537C73D49,
	DwellSelection_Start_m0200E21183FE3DE2C71759D8F329F8A8E01EF631,
	DwellSelection_EtTarget_OnTargetSelected_mFD1B1EF9BA42027E744345ACB7AB0BE1E4331CC1,
	DwellSelection_EnableDwell_m5846798A9E8A5CD6A9B48418F0B8B8866BB69834,
	DwellSelection_DisableDwell_m3F412C4FDD4E1B15A4C0077286E0690B18D98C4A,
	DwellSelection_get_UseDwell_mE993FF99112A7523B8CF81CFFD5E6954602CF928,
	DwellSelection_OnEyeFocusStart_mDB8D7DBB9BA1F373A7123FC2EF2E0D499B3EAEB9,
	DwellSelection_OnEyeFocusStay_m1C7D029AA6D1A126DB560A6D6CCBA5474866B0E2,
	DwellSelection_OnEyeFocusStop_m35E6FDE67D05AF53FA96C1873FFC6C3009769577,
	DwellSelection_StartDwellFeedback_m720C36A5468801956420C40D55A857FD656D7553,
	DwellSelection_ResetDwellFeedback_m46C6C947DCA9A095ECE5A07C47B6A222E6842EC3,
	DwellSelection_get_SpeedSizeChangePerSecond_m486279D730F882EBBCD2C2244885882ADB9B2C4A,
	DwellSelection_Update_m731C4250937110A1757D34036B99059FAEFAAA3D,
	DwellSelection_ClampVector3_m5A438F1FCD0E8E9F7CED6428AD415C9A8AC2ABAD,
	DwellSelection_UpdateTransparency_mF3E939B57E5376755188CA757D8F531B87528631,
	DwellSelection_LerpTransparency_mF6BD0EFC6C0CD666181B0673F9DD6092B0042EDD,
	DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_m71BCDC31185A913ED89388D566A3B0A2FA343374,
	DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_mB6B665B0334B21BD0C6FA24A37556639114F121D,
	DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m874E3926315D791602EAEA793ED5F3CF037707F1,
	DwellSelection_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_mAA5B2AB1C789D5C8906C5B06BD51C1AAB26F30BA,
	DwellSelection_get_TextureShaderProperty_m6D319C321495A6F25F988FD4905C9BE6DFCE0578,
	DwellSelection_set_TextureShaderProperty_m70DC993A731F4EF1283B62208C46AFC71FDFA538,
	DwellSelection__ctor_m979DBD4A6D9528F4CCEA4F9E0DF11D8AD051CCC1,
	DwellSelection__cctor_m551FCFFE9641A3046BD43AEEEFC3451F65A67C91,
	TargetEventArgs_get_HitTarget_m8388656A38F227CCD16F8BCF83A22047ECFDA41B,
	TargetEventArgs_set_HitTarget_mC1AE547E25147BE85A735C663443ABEFDE11B4F0,
	TargetEventArgs__ctor_mD30806FADBFFE5CC12D78C23BF965C01CB6195FB,
	ChangeRenderMode_ChangeRenderModes_m4D2AD991BB7A67A4B3E95B0F58818D34CE12F5A4,
	DoNotRender_Start_m4259CCE2CDBBA1D052F3D89C0C3B45DAA85B7D48,
	DoNotRender__ctor_m896C686D87E71F51EA7CC846DE075DEAC7E3B218,
	EyeCalibrationChecker_Update_mB417F03C9A5AE2018BC8E21C6688DDFB5AA9B293,
	EyeCalibrationChecker__ctor_m2062E826890171E2174E63DCAFA68DEB96B1D1F2,
	KeepThisAlive_get_Instance_m6E3E6968684933E52E1A47F41083AC48FF464465,
	KeepThisAlive_set_Instance_mA2A10EE2DD42672547E38F84945744FB35AF8640,
	KeepThisAlive_Awake_mC82B2BAB9D5DDE853437891859A52049ED4E157A,
	KeepThisAlive_Start_m2DE87B65FFA56E0F7814687114DCEC08CF8C76C4,
	KeepThisAlive__ctor_m0401E6CD503FDAE3C620F9C5D8A7C1BBF5644FCB,
	StatusText_get_Instance_mA5D6F917BF8C0CE492E402048665A16E91171762,
	StatusText_Awake_m1A1B8FC2DA65C9F4B6519C321D7C2579E02E7128,
	StatusText_Start_m18A92E43D8562E33453B15C2BB65F0B798143481,
	StatusText_Log_m760562A30E2863BC5E8B8A0E40BB56B89691DBDC,
	StatusText__ctor_mB2FEC1698B210DBE1A2CFA635A75D491C4F04341,
	EyeTrackingDemoUtils_GetValidFilename_m3533AC1EFB6CD3A8C0B570923F341E28B4319610,
	EyeTrackingDemoUtils_GetFullName_mC347D6F086625BD180DDBB42BF2DEF754C1F6639,
	EyeTrackingDemoUtils_GetFullName_m80D9ACDF0541ADA14FBBA0A92FEC90553AC1B733,
	EyeTrackingDemoUtils_Normalize_mCADC77F16D1F9C624B9851FCA0D10E2D2C630B87,
	NULL,
	EyeTrackingDemoUtils_VisAngleInDegreesToMeters_m6D8D82BA4FAF6F068ACA474A906920F0480A8266,
	EyeTrackingDemoUtils_VisAngleInDegreesToMeters_mDE4DD9A3C3F1622F8647E044EE101E3F7AFA39B2,
	EyeTrackingDemoUtils_LoadNewScene_mBC2C038E7D63C54B409884FC2841ECB88F1670D6,
	EyeTrackingDemoUtils_LoadNewScene_mDDC18CE3E1DB98A62D89FB6C89E485D1B68BF4F2,
	EyeTrackingDemoUtils_GameObject_ChangeColor_m7CC5CE3076D62FC81F1544584E3D63F7C1786DDB,
	EyeTrackingDemoUtils_GameObject_ChangeTransparency_m1421C17BB27D71E7806437FF09ED61FA569594D6,
	EyeTrackingDemoUtils_GameObject_ChangeTransparency_m6E8BBA1A73EFE378E6A6BFA428436FE099F1B6E6,
	EyeTrackingDemoUtils_Renderers_ChangeTransparency_mFFB2985F54D2BB8B801C4C6D9579F783E160E94D,
	U3CLoadNewSceneU3Ed__8__ctor_m401003DDD285C5623BEB280831A31A466D16A964,
	U3CLoadNewSceneU3Ed__8_System_IDisposable_Dispose_mD06F424BF364A4726F11674C65C0CA9EEEAFE2CC,
	U3CLoadNewSceneU3Ed__8_MoveNext_mD0328A163C898776ECE5545895012768D4200123,
	U3CLoadNewSceneU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6D49FF64BD8C8DE4A6EA2EC600BD12FF3390B71E,
	U3CLoadNewSceneU3Ed__8_System_Collections_IEnumerator_Reset_mC163ABF19EDAEDE1B729059AFF32EC00D9760CED,
	U3CLoadNewSceneU3Ed__8_System_Collections_IEnumerator_get_Current_m4E0C826DFFCFD5E5854D74C1DDB3B2F680731F1C,
	OnLookAtShowHoverFeedback_Start_mC42027FB29BC07500E307896156AE8795841259E,
	OnLookAtShowHoverFeedback_Update_m1A5F0E4755D242A50C7629D127CFBA4DD3952C33,
	OnLookAtShowHoverFeedback_OnDestroy_m577A5F6EFC5D1EEB21BD5455B397923604288EFD,
	OnLookAtShowHoverFeedback_NormalizedInterest_Dwell_m46248903D6C203574487FBB8382E78BF9046BDAC,
	OnLookAtShowHoverFeedback_NormalizedDisinterest_LookAway_m77B04187C69C5A13AD985F6471A7D4736AF7FBA6,
	OnLookAtShowHoverFeedback_DestroyLocalFeedback_m9FC2B5CCD3E7F4909D6F68ECF03BD8577DB83462,
	OnLookAtShowHoverFeedback_ShowFeedback_m2DD737C913EBB583F9153F819087EDA6206B8C59,
	OnLookAtShowHoverFeedback_ShowFeedback_Overlay_mDFAA0EB93210CCE81059144FB9E00166426EFDDB,
	OnLookAtShowHoverFeedback_ShowFeedback_Highlight_mE49C9BE174E75FF25B96E210937278D069539423,
	OnLookAtShowHoverFeedback_TransitionAdjustedInterest_m09E7EF77F3E93CE57A2FD7028273D856F7B0FE7E,
	OnLookAtShowHoverFeedback_get_LookAwayTimeInMs_m84A4D873B727DB725D7F918606F39396B2C2231F,
	OnLookAtShowHoverFeedback_get_DwellTimeInMs_mDFE0E399D6DF539D5F8496A8BA85F9C82FBA6896,
	OnLookAtShowHoverFeedback_OnLookAtStop_m82B20E68E701C0C3260600B3BAE960081F3DCFF9,
	OnLookAtShowHoverFeedback_OnLookAtStart_mABA332BFAE039BD27008EEFEC9718FB9C6713465,
	OnLookAtShowHoverFeedback_SaveOriginalColor_mF179D58C8ACBAB4A3A0015184325502708292733,
	OnLookAtShowHoverFeedback_GetColorsByProperty_mE7331C9E98EA872097A2C3F9840B953508AE65A8,
	OnLookAtShowHoverFeedback_BlendColors_mA5CE600AC46E4A874C554E769F364033010BDEFC,
	OnLookAtShowHoverFeedback_divBlendColor_m779E436E8A28F563BDAF47CABC769A9F3C6C539A,
	OnLookAtShowHoverFeedback__ctor_mA6B6AF6424D4E92C1B97C684B1401687704DD917,
	AsyncHelpers_RunSync_m7656CD620C0DB02D30F397EF3D446A10BCE5D0AA,
	NULL,
	ExclusiveSynchronizationContext_get_InnerException_m4409BF574355A708E7AD7B501A23394DC8194BDB,
	ExclusiveSynchronizationContext_set_InnerException_m8E8500331FAEEC67FA3687E1AB92B8B6AFCCE153,
	ExclusiveSynchronizationContext_Send_m848930E6C929ACF2DDD1EBD9B82A09D49894B72C,
	ExclusiveSynchronizationContext_Post_m2884588D960FF0873E27192A6CE01CE070BD0573,
	ExclusiveSynchronizationContext_EndMessageLoop_mB29443F8BDEFB6445EDF3818E7A588000FFCCDCF,
	ExclusiveSynchronizationContext_BeginMessageLoop_m6AC60C2B4FA9E4CD7841A99CD11CE5ACEA75FA78,
	ExclusiveSynchronizationContext_CreateCopy_mFECF393B1AB4ECA7D64E12B476B9B1C2C9B8C17E,
	ExclusiveSynchronizationContext__ctor_m4523ADFE91001ACA98CD8E30463279800357064A,
	ExclusiveSynchronizationContext_U3CEndMessageLoopU3Eb__9_0_m2269F5C62E97D5684D03C180C278A730562819C4,
	U3CU3Ec__DisplayClass0_0__ctor_m0A40F6B82AE3D3383693F8ED2D10A4C4B8311A58,
	U3CU3Ec__DisplayClass0_0_U3CRunSyncU3Eb__0_mE0B82500FB231395120B4EA8482680957CFEE75A,
	U3CU3CRunSyncU3Eb__0U3Ed__ctor_m3B2E40C1737968F6C28F3AD3506D9C38291688F5,
	U3CU3CRunSyncU3Eb__0U3Ed_MoveNext_m7B8B6DD784CAE08A72B3207DE2F30821A1FB1C3F,
	U3CU3CRunSyncU3Eb__0U3Ed_SetStateMachine_m9CD2295E60C2CDDFF44C6A2CA4ACF079D62B4BE0,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BasicInputLogger_SetUserName_m9EFC6D8F9A3DAFAD2211A9F1A235FD8F316B1211,
	BasicInputLogger_SetSessionDescr_m0E9F6709FC1D035596131B8689AD7D4991387AB4,
	BasicInputLogger_get_LogDirectory_m62691878EE1DDB0F42D4BD25210D30DDCE453B7C,
	NULL,
	NULL,
	BasicInputLogger_CreateNewLogFile_mA8AB2F74DDF248A632C2C07FED6A9F121C5F24AC,
	BasicInputLogger_CheckIfInitialized_m172E2A10D56544A492F36379390236E48B831C3E,
	BasicInputLogger_ResetLog_mCC6887B2578E0CBD5750A67B6A46AB223897B82F,
	BasicInputLogger_get_FormattedTimeStamp_m42275F6D37795B25A14B1BE2BE7B17E5A493216A,
	BasicInputLogger_AddLeadingZeroToSingleDigitIntegers_m9B1111BAF1BEF8FE36CC28B5543681230F494222,
	BasicInputLogger_Append_m3A6FB5522D93EFD6FE9685BC66500B8044AEB794,
	BasicInputLogger_LoadLogs_m7F0D6AA20D8D240CD2A26D43CB0604DD4A59B853,
	BasicInputLogger_SaveLogs_m8E427165D7149A23C324257EC8E8F0C2637FF3B1,
	BasicInputLogger_get_Filename_m54CD3CA10F35462CBBE4C5D11539C9E519F4213D,
	BasicInputLogger_get_FilenameWithTimestamp_m4C0355C86CED8B6474BA48248016118FBEC950B7,
	BasicInputLogger_get_FilenameNoTimestamp_m0690087D1AF9E7083D02C147CE23205B806D5644,
	BasicInputLogger_OnDestroy_m07C4CB956DF8AA78518E01565763D1F260E3855C,
	BasicInputLogger__ctor_mCDED1AC979CBD78FB0DFD0729DCC9A0A5C5F818F,
	U3CCreateNewLogFileU3Ed__14__ctor_mEEA0AE3041B0D8CBF1E387428C278A32A14B0B46,
	U3CCreateNewLogFileU3Ed__14_MoveNext_m4F75BD58B788E80CFA742F24516CC6A08A688754,
	U3CCreateNewLogFileU3Ed__14_SetStateMachine_m6F238B4288F208E800147D5C1CEABA0527113273,
	U3CLoadLogsU3Ed__21__ctor_mC815507963FE629C202476F38526CE536A4E6F1B,
	U3CLoadLogsU3Ed__21_MoveNext_m3D4C327E67AA2C6D410647FEA422BD364DEF58F6,
	U3CLoadLogsU3Ed__21_SetStateMachine_m2E9C43D81C1E46818BC390C7402EB5F366A44F4D,
	U3CSaveLogsU3Ed__22__ctor_mA15CEEFB6A4D7658B6B70791D33DD1AE04FECA53,
	U3CSaveLogsU3Ed__22_MoveNext_mA6BFA52E1838A7BA82E8C945CE50B45C24077B8C,
	U3CSaveLogsU3Ed__22_SetStateMachine_m61A95B0BF84898DFF17E30CC4BDF6756503A7F54,
	CustomInputLogger_CustomAppend_m352B0C93623E93DE35BB70F87B3338E235470642,
	CustomInputLogger_CreateNewLog_m63E7EE113D0FBBAAEE7D94009AEEC3F3B7156C52,
	CustomInputLogger_StartLogging_m4A87A8548790322E40D6182CC67C97F149B12F20,
	CustomInputLogger_StopLoggingAndSave_mEE6746F9E54C8C0970B93E9AA1F8A9C82CAE177D,
	CustomInputLogger_CancelLogging_m497154BAAD16D304FC9FE404760C6DC87C14EB98,
	CustomInputLogger__ctor_m9E289B363B74750CE12BA52C05AF7E9BE89C720E,
	InputPointerVisualizer_Start_m3F5E035F824ADE5837B07AF329D9B056FE40AB2C,
	InputPointerVisualizer_ResetVisualizations_mFC52CAE9A4A737DAEAE5292B555C55A226188D67,
	InputPointerVisualizer_InitPointClouds_mC0DE9CA29AD734A012A4FDDD615B86434F2408E6,
	InputPointerVisualizer_InitVisArrayObj_m05D28602A09E52891E583D2D5A12DFC441F2BEAC,
	InputPointerVisualizer_ResetVis_mFC891DFE40B2E6E4ED1A35C5CBCE7C1307962055,
	InputPointerVisualizer_ResetVis_m4C3D7EC1ED8149ACA9F37D6A4F2ED3F3C57FA1C3,
	InputPointerVisualizer_ResetPointCloudVis_m049688BD070C6AB22FEF60535864820E36332093,
	InputPointerVisualizer_SetActive_DataVis_m5959A5480E78EB05321B0A54403A6839ED077047,
	InputPointerVisualizer_SetActive_DataVis_mCF2E22079F2AF8F475E44B431713A29C15B71DA8,
	InputPointerVisualizer_SetActive_DataVis_m642C606192EED483F50E983C744FBA6F41FAC5C1,
	InputPointerVisualizer_SetActive_PointCloudVis_mB52B80C7BC62A86B701073CD1D8AC8E0820FE654,
	InputPointerVisualizer_UpdateDataVisPos_mA8537AB95250ECA716BA5896A7E3F9FE7D8E52D4,
	InputPointerVisualizer_UpdateVis_PointCloud_mBD51E9CDE7421319696381608D0C1F05D538D60E,
	InputPointerVisualizer_UpdateConnectorLines_m532AB04C8FDD62BB3130D0298F831BBEF20ED6C1,
	InputPointerVisualizer_GetPrevLineIndex_m841105FA299FBC913AEC839261945F01CD52BC0C,
	InputPointerVisualizer_UpdateDataVis_mA4B538EE155C8A648BDB7BDD6F0587D00BBCB950,
	InputPointerVisualizer_PerformHitTest_m2878332499224112CBF61C1D7BE7F04B6D1787E0,
	InputPointerVisualizer_Update_mBE331944C6BF5EC9884493D226930361F093D405,
	InputPointerVisualizer_IsDwelling_mA106B66A3754E0AB5073270E316C6E9530FBDD6E,
	InputPointerVisualizer_get_AmountOfSamples_mD0A66CF4E74338190B56F943BB74495E7A54FA9C,
	InputPointerVisualizer_set_AmountOfSamples_m66184A93726408C5B1D0CFFED187390124C65054,
	InputPointerVisualizer_ToggleAppState_mEA16DA03BCA71DC2F7306C609A25E361409C199A,
	InputPointerVisualizer_PauseApp_m1E04903F7D4ABA7B10A835C34016E3726192C404,
	InputPointerVisualizer_UnpauseApp_m1CBF057069470A9311E09323C896982FB1769CE9,
	InputPointerVisualizer_SetAppState_m4587663C118D7B47B141383792876066D04CD379,
	InputPointerVisualizer__ctor_mCDC4F3097CDE7321A664BAA65C9E900104A44E36,
	LogStructure_GetHeaderColumns_mF74FD887B53B3F7587AB7BC8A6DE19553C516057,
	LogStructure_GetData_mA862E5C33AA9646D4F0DC373CFC8A947392EBDE4,
	LogStructure__ctor_m379E801EE8F6A2613EEFE9A600A9DFB03F084911,
	LogStructureEyeGaze_get_EyeTrackingProvider_mD464DCFAEE72299E10969C9409E5742765332CDA,
	LogStructureEyeGaze_GetHeaderColumns_m6A8EFF0419A69B84F7CEFD3951D14390B9F7E679,
	LogStructureEyeGaze_GetData_m124FC286EDD57662E1B8A5714A09222FF9B88627,
	LogStructureEyeGaze__ctor_m4D6F72C20BFD24B78E800B808D604D85CBE16185,
	UserInputPlayback_Start_m51DA7CD6D117F2E488D3C99986C772549C485FEE,
	UserInputPlayback_ResetCurrentStream_mFE25FF476B17E3140379E901FB5CF7F70CBCD668,
	UserInputPlayback_UWP_Load_m5788A70494218FE373050086B3654F96B15AAA46,
	UserInputPlayback_UWP_LoadNewFile_mF348B60237E4F7DCEA10BC345BAEC56B9513FDAE,
	UserInputPlayback_UWP_FileExists_m418F422637A601D32632A7EDD1A33DEB9FB624E0,
	UserInputPlayback_UWP_ReadData_m67177F6AB330EE92B655AF4C87F16D7AC8A85E50,
	UserInputPlayback_LoadNewFile_m358C527E0A59B7CE0E384B7D67BB0A0AE0FA8E46,
	UserInputPlayback_TryParseStringToVector3_mFEC60C385896F4BD479F78BDDD27B55782ABB1B0,
	UserInputPlayback_ParseStringToFloat_m6F1986BA3151C23D06A30FCC6E0ABB147F3EAF47,
	UserInputPlayback_Load_mC4E52E21FD1C341EA58995BAB4D19A0070D6182D,
	UserInputPlayback_LoadInEditor_m43D44D6603B9775B231E1F428A100CF505819617,
	UserInputPlayback_LoadInUWP_m64594642768FF5FE2AB13AA978790AEF6420CB65,
	UserInputPlayback_get_FileName_mC534CFE3D8A7127BA62D2C5E9D9A16448E93CBBA,
	UserInputPlayback_set_IsPlaying_mCBC869EE68F90686CAD123225EEA43AD43205C64,
	UserInputPlayback_get_IsPlaying_mEC33D4DE5699A80B1228F6515DEBB43DE45C7A91,
	UserInputPlayback_Play_m8FC518FECA8D6E792E2F24D1BB4BABFC4CB747CE,
	UserInputPlayback_Pause_m977CE7D0C36366177DBFDABAB528F8765211AD82,
	UserInputPlayback_Clear_m02D5D1110CFB864EA63543270502D0CB84166344,
	UserInputPlayback_SpeedUp_mF525595B4DA89F5ED9DFF7E330808FE4E73C1FCE,
	UserInputPlayback_SlowDown_m06514F5B367060497E63F4188084739FF4848E3C,
	UserInputPlayback_ShowAllAndFreeze_m68C9145E245E10521D38262F74B2B554A04C4257,
	UserInputPlayback_ShowAllAndFreeze_m540AD6F91B1D042AF3D5ED121DE15E3CE4C30AF5,
	UserInputPlayback_ShowHeatmap_mB8A7D1570E1AEC920DB6FE951C33FCD63301887F,
	UserInputPlayback_LoadingStatus_Hide_mECA81B64C2437E0C42BB45D31D4EDFA223DA3427,
	UserInputPlayback_LoadingStatus_Show_m31CBED33A38E6BA8E9C84401F5EAFCEBB05319D7,
	UserInputPlayback_UpdateLoadingStatus_mFE56924D2B5A6A07C9A360A4FD08A121203DE257,
	UserInputPlayback_Log_m555134D76149170AFEBE17A6E5B2143D02CF22AA,
	UserInputPlayback_PopulateHeatmap_m12AFD4C59AE8898EFB84E27AA69D1C526324ECB8,
	UserInputPlayback_UpdateStatus_mD6B38FDE0F453849A87799311B028E23B7DB175B,
	UserInputPlayback_AddToCounter_m41084542E3C5DCC962EB9929A40BF5E61922298D,
	UserInputPlayback_GetEyeRay_mC28F82A17D108935AE20E543E5C0EE75DBF6FB1B,
	UserInputPlayback_GetRay_m0E409269280797FD566D4C165D6DD0C15E1543F9,
	UserInputPlayback_get_DataIsLoaded_m27E61A989410CE611E4251F7ACA5495FFF0972D4,
	UserInputPlayback_UpdateTimestampForNextReplay_mF29A1ADE0183804B63587F89C192398E9BB8A0C1,
	UserInputPlayback_UpdateEyeGazeSignal_m857663D1C7CF1A10885BAA8AA382F7979511823B,
	UserInputPlayback_UpdateHeadGazeSignal_m4BD59F502198C90F9EC5581057CB6D7709A686F1,
	UserInputPlayback_UpdateTargetingSignal_m9DA09BE081CDDFA6E9C9C3D37E12394FDF905DCF,
	UserInputPlayback_Update_mD152E1FBACA6E44859E96A9709487EF98CBF9D2D,
	UserInputPlayback_PlayNext_m95254BFC4B059A6F70F1042030C0213FEF52EEDC,
	UserInputPlayback__ctor_m60A9DD1036BEAA556A5751CB498DAC2C63C22495,
	UserInputPlayback__cctor_m21101850924FB6B6E9CB5D707C00F0E9B2E2CBEA,
	UserInputPlayback_U3CShowAllAndFreezeU3Eb__34_0_mDF45375F98D731AB4729A0CA2E208E5B88EE62C2,
	U3CUWP_LoadU3Ed__12__ctor_m591A2015586AFB486910D5ECDB00185FBF2E9150,
	U3CUWP_LoadU3Ed__12_MoveNext_m077A66752D48B5D767DED22742A3D135C0211FB3,
	U3CUWP_LoadU3Ed__12_SetStateMachine_m7ED88E33A30408B900C9A7A3C35A4E02BFC5683E,
	U3CUWP_LoadNewFileU3Ed__13__ctor_mC8454DF517F5F15CD6FBE069DC6E686C166A182C,
	U3CUWP_LoadNewFileU3Ed__13_MoveNext_m0839C0504BC89E3A5FA8492B5B10ED67A68689FB,
	U3CUWP_LoadNewFileU3Ed__13_SetStateMachine_m22C14D0803818A1A528BDA6D6179361CE22C7233,
	U3CUWP_FileExistsU3Ed__14__ctor_m6A3F94CFFED0ABCC8D8D81A8B1B5958A9046AFCD,
	U3CUWP_FileExistsU3Ed__14_MoveNext_mAD5711484E8F38EC1F51E475060E1D78EB279E74,
	U3CUWP_FileExistsU3Ed__14_SetStateMachine_m3A2B9C0F394A24619019FE3C65461944E89E98DD,
	U3CUWP_ReadDataU3Ed__15__ctor_m87B05716339B712314C0A6557B10C9EF2F05FDB5,
	U3CUWP_ReadDataU3Ed__15_MoveNext_m9E20CA73029D89308F6E58413D241125964F6B01,
	U3CUWP_ReadDataU3Ed__15_SetStateMachine_mADF140A5B17E52067F652E349087443412A6D68C,
	U3CLoadInUWPU3Ed__21__ctor_mCAF86BBCF11030B57FF92BB58954A6595252FD0C,
	U3CLoadInUWPU3Ed__21_MoveNext_m41DF334615BFEDDE4965A3AB46C847FB025147E2,
	U3CLoadInUWPU3Ed__21_SetStateMachine_m8FFD898942DB58C8CC1D604F4286C93A5FF9E428,
	U3CPopulateHeatmapU3Ed__42__ctor_m102DF160804926625E30794716CD29865EC82DD9,
	U3CPopulateHeatmapU3Ed__42_System_IDisposable_Dispose_m1097B7B94EB08BA3EF350FE660AD7B9956863392,
	U3CPopulateHeatmapU3Ed__42_MoveNext_m630D8415D254C155DCB064D523A0D7DB332CC972,
	U3CPopulateHeatmapU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0976BC78E7B504C287CB571F0DE0B71DF8413CC6,
	U3CPopulateHeatmapU3Ed__42_System_Collections_IEnumerator_Reset_m83A7B4025FE7ECAD0E352634B658870920480462,
	U3CPopulateHeatmapU3Ed__42_System_Collections_IEnumerator_get_Current_m661273649CF08D4B0CE3715EA1ACE2A6F8530DD3,
	U3CUpdateStatusU3Ed__43__ctor_m970C1666C59016AEFE7E3CB5257A6E95BA8A2B91,
	U3CUpdateStatusU3Ed__43_System_IDisposable_Dispose_m2BF8311A1FA147B50BAA705D688E487697456AFB,
	U3CUpdateStatusU3Ed__43_MoveNext_m2CA8D758330DE6C0D48EB7B44B2E849F2F1ADEFF,
	U3CUpdateStatusU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE88D22C380772911600CEDC6B6C20DECF74174DC,
	U3CUpdateStatusU3Ed__43_System_Collections_IEnumerator_Reset_mA92CE780FB948B861106556A7D441579F0E52951,
	U3CUpdateStatusU3Ed__43_System_Collections_IEnumerator_get_Current_m4E0F683876005E30B881179D5A5255E3BC229525,
	U3CAddToCounterU3Ed__44__ctor_m6FB296DA9BFA0BA393DAD6F0B6CC62FE117DEA6B,
	U3CAddToCounterU3Ed__44_System_IDisposable_Dispose_mDCE82C6860FA2A2FF993B0E52EFD549B701BBAE9,
	U3CAddToCounterU3Ed__44_MoveNext_m1B18E7D4FD16FC3F9B72724FDDC696B10F704E61,
	U3CAddToCounterU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA8B93ED764B147113D8B736B485AE53DFD39DB52,
	U3CAddToCounterU3Ed__44_System_Collections_IEnumerator_Reset_m331A758A780FF4767AF4D233962A09D5E9D11954,
	U3CAddToCounterU3Ed__44_System_Collections_IEnumerator_get_Current_mA947986D577DD4062D7BFC7DBA9A0D6608A0CADC,
	UserInputRecorder_get_Instance_m06936372DDBAE9401846AF06970E2DB4BFB42AC4,
	UserInputRecorder_GetHeader_m306D653D5A260615A44732DD9E18C2F0A23B454F,
	UserInputRecorder_GetData_Part1_mC89E9E2407D04916974DE65F9B344AD616CA58E4,
	UserInputRecorder_MergeObjArrays_m432BF1067846943B995EE7D7D0E7BEE2BBF397CC,
	UserInputRecorder_GetFileName_mD3965ED6EB325AB3B7667C3FA95BC70DDA9E3F85,
	UserInputRecorder_LimitStringLength_m663BD16DD8C88C5C0B5CC8DD7FF7D195CBB56558,
	UserInputRecorder_GetStringFormat_m91B13F46B2F229A7190F4DEEBE83208E72CABC31,
	UserInputRecorder_UpdateLog_m1890778B90ECB9E3157DBA5545CF4BE64F939707,
	UserInputRecorder_CustomAppend_m9F53F1A9873FB51A8604DCA68C6F370A23994122,
	UserInputRecorder_UpdateLog_m85FC6427E796D99B30378633B5E0A3939046E9A6,
	UserInputRecorder_Update_m9860349CF376ADC02F0A076788335640AB128A5A,
	UserInputRecorder_OnDestroy_m9FCD1FA483A4325E124A6A4D00F671B5D43BBE85,
	UserInputRecorder__ctor_mBAC246A8CCE3E16C9BF1731D423B019227B655E8,
	UserInputRecorderFeedback_PlayAudio_mB1012BA0413A6F56031446B59CB9953B76266363,
	UserInputRecorderFeedback_UpdateStatusText_mC6BCEFE34863E7A8E1BE4E9C68465B5C031D133E,
	UserInputRecorderFeedback_ResetStatusText_mFE0D7238A0F369A068CCAD27F46632C147FD4294,
	UserInputRecorderFeedback_Update_mA86AAB0647AF04743F319FC8DE8385837F1B4995,
	UserInputRecorderFeedback_StartRecording_m597E886FD203FA383991BD349A52FFEB5F68552D,
	UserInputRecorderFeedback_StopRecording_m2991870378EC883878AF4F70ED3034C581818D82,
	UserInputRecorderFeedback_LoadData_mAEF93C1C7D72581C713696B8D8AB09E519078700,
	UserInputRecorderFeedback_StartReplay_mDB511EB238E57FC9B88AD61B59F4E0B23B1E7DBC,
	UserInputRecorderFeedback_PauseReplay_m3DC27510B2C972C5298546A8172765B3C57C4199,
	UserInputRecorderFeedback__ctor_m6025BE8DF951F0BCE5C0549613A755E01E38585C,
	UserInputRecorderUIController_Start_m194ACE075C08181F70372B3308BD0DE2A3DB8B54,
	UserInputRecorderUIController_StartRecording_m85AEAE54B880CB2E3BDB369453F73A788316CDD8,
	UserInputRecorderUIController_StopRecording_mEB93E3F2B7F537436E8BB9F7AE4CE605B5C36905,
	UserInputRecorderUIController_RecordingUI_Reset_m09E03DE3ACB436AE7FCD83486F85DDE7AC3887E8,
	UserInputRecorderUIController_LoadData_mC8CE0E2F299FB4368259803ACE4F0E6EE3E3C25F,
	UserInputRecorderUIController_ReplayUI_SetActive_mEA0A639E776B20C8319F0FDB6067C241C0444A1B,
	UserInputRecorderUIController_StartReplay_m2D3403C6A835556B5F467244EE77EBDC47BC8187,
	UserInputRecorderUIController_PauseReplay_mECFEA8EAC5444AA7A18E3C1BB56595221E6CD473,
	UserInputRecorderUIController_ResetPlayback_m3AB46A57B54B4AC79B77F9AE3B7A0858951AD28C,
	UserInputRecorderUIController__ctor_mBD9DC54CE0E68A054D6546365DA3083184688813,
};
static const int32_t s_InvokerIndices[1285] = 
{
	4755,
	3926,
	4797,
	4797,
	4797,
	3762,
	3886,
	4797,
	7059,
	4712,
	3886,
	4797,
	4797,
	4797,
	4797,
	7059,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4712,
	4797,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4797,
	4797,
	3886,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4712,
	4797,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4797,
	4797,
	4748,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4748,
	4748,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	3886,
	4797,
	4797,
	4797,
	4797,
	4712,
	4797,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	880,
	4797,
	880,
	4797,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4797,
	4797,
	3886,
	3886,
	1850,
	1850,
	1206,
	1206,
	1220,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4712,
	4797,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4712,
	4797,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4712,
	4797,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4797,
	4797,
	4797,
	4797,
	4797,
	2993,
	4712,
	4797,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	3850,
	4797,
	4797,
	4797,
	1850,
	1850,
	1206,
	1206,
	1220,
	4797,
	4797,
	4797,
	4797,
	3886,
	3886,
	4797,
	4797,
	4797,
	4797,
	3886,
	4797,
	3886,
	3886,
	3886,
	3886,
	3850,
	4797,
	4797,
	4797,
	4797,
	3850,
	4797,
	4797,
	3850,
	4797,
	4797,
	4712,
	4797,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4797,
	4797,
	4797,
	4797,
	3886,
	2993,
	2993,
	4797,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4797,
	4797,
	4712,
	4712,
	4797,
	7059,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4797,
	4797,
	4797,
	4797,
	4797,
	4712,
	4797,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4797,
	4797,
	4797,
	4797,
	3886,
	4712,
	4797,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4797,
	4797,
	4797,
	4797,
	3886,
	4712,
	4797,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4797,
	4797,
	4797,
	4797,
	3886,
	4712,
	4797,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4797,
	4797,
	4797,
	4797,
	3886,
	4712,
	4797,
	4797,
	1395,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4797,
	4797,
	2993,
	4712,
	4797,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4712,
	3886,
	4748,
	3920,
	4797,
	3886,
	3886,
	3886,
	3886,
	4797,
	4797,
	4797,
	3886,
	4797,
	4797,
	4797,
	4797,
	3886,
	4797,
	4797,
	4797,
	3886,
	3886,
	3886,
	3886,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	3886,
	3886,
	3886,
	2987,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	2465,
	4797,
	3850,
	4797,
	3886,
	3886,
	2224,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	3886,
	3886,
	3886,
	3886,
	3886,
	3886,
	1223,
	811,
	3962,
	4797,
	4797,
	4797,
	3886,
	3886,
	3886,
	3886,
	3886,
	3886,
	3886,
	4797,
	4797,
	4797,
	4797,
	3886,
	3886,
	3886,
	4797,
	4797,
	4797,
	4797,
	4712,
	4712,
	4797,
	7059,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	3886,
	4797,
	4797,
	3886,
	3886,
	3886,
	3886,
	3850,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	3886,
	4797,
	4797,
	3886,
	4797,
	4797,
	3886,
	3886,
	3886,
	3886,
	4797,
	4797,
	3886,
	3886,
	3886,
	4797,
	3886,
	4797,
	3886,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	-1,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	3920,
	4797,
	4797,
	3886,
	4797,
	4797,
	2224,
	4797,
	4797,
	4712,
	3886,
	4712,
	3886,
	4676,
	3850,
	4748,
	3920,
	4676,
	3850,
	4712,
	3886,
	4797,
	4797,
	1541,
	3886,
	4797,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4712,
	3886,
	3850,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4676,
	3850,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	-1,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	3886,
	4712,
	3775,
	4712,
	3886,
	4797,
	3886,
	3886,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4797,
	4797,
	4797,
	3886,
	4712,
	3775,
	4712,
	3886,
	4797,
	3886,
	3886,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4797,
	4797,
	4797,
	4797,
	4797,
	4712,
	809,
	4797,
	3850,
	4797,
	4748,
	4797,
	4712,
	4797,
	4712,
	4797,
	4797,
	4797,
	4797,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	3886,
	4797,
	4797,
	3886,
	4797,
	4797,
	4797,
	3886,
	4797,
	4797,
	3886,
	3886,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	3886,
	3886,
	3886,
	3886,
	3886,
	3886,
	4797,
	4797,
	4797,
	4712,
	4797,
	1124,
	2780,
	4797,
	4797,
	4797,
	4748,
	4797,
	4791,
	4797,
	3926,
	3926,
	4797,
	4797,
	3920,
	4797,
	4797,
	4797,
	4797,
	4797,
	3962,
	4797,
	3926,
	4797,
	4797,
	4797,
	4797,
	4797,
	4712,
	3886,
	3926,
	3926,
	4797,
	3585,
	4797,
	4797,
	2996,
	-1,
	4797,
	4797,
	3886,
	3886,
	3886,
	3886,
	3886,
	3886,
	3886,
	3886,
	3886,
	4797,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4748,
	4797,
	1124,
	2780,
	4797,
	4797,
	4797,
	4788,
	2267,
	4748,
	4797,
	4712,
	3886,
	4748,
	4797,
	3926,
	1124,
	4797,
	2780,
	4797,
	4797,
	2267,
	4748,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	3886,
	3886,
	3886,
	3886,
	4797,
	4712,
	4676,
	4676,
	4676,
	4797,
	4797,
	3886,
	3886,
	3886,
	3886,
	3886,
	3886,
	3886,
	4797,
	4797,
	4748,
	4748,
	4748,
	3590,
	4797,
	4797,
	4797,
	4797,
	3962,
	4755,
	3534,
	4748,
	3962,
	4797,
	4797,
	4797,
	3962,
	1108,
	3886,
	4797,
	7059,
	4797,
	3886,
	4797,
	4797,
	3886,
	3886,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	3534,
	4797,
	4797,
	4748,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4712,
	4712,
	1264,
	4797,
	4797,
	4797,
	4607,
	3886,
	3886,
	3886,
	3886,
	4797,
	4797,
	4797,
	4797,
	4797,
	4712,
	4712,
	4748,
	4797,
	4797,
	3779,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	3886,
	3886,
	3886,
	3886,
	4797,
	2219,
	4797,
	1542,
	3886,
	4797,
	4797,
	3920,
	4797,
	4712,
	4797,
	4797,
	3962,
	2275,
	1269,
	3002,
	1002,
	1113,
	4712,
	4712,
	2414,
	4797,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4797,
	4797,
	3886,
	3886,
	3886,
	3886,
	4797,
	4797,
	3962,
	2415,
	1803,
	4797,
	3536,
	4797,
	4797,
	4797,
	4797,
	4797,
	7026,
	6921,
	4797,
	2993,
	3886,
	4797,
	4797,
	4797,
	4712,
	3886,
	3886,
	4797,
	3886,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	3926,
	2263,
	1242,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	3426,
	4797,
	4797,
	4797,
	4797,
	2283,
	2283,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	3886,
	2993,
	4797,
	7059,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	1124,
	4797,
	4797,
	2224,
	4797,
	4797,
	4748,
	4797,
	4797,
	4797,
	4797,
	4797,
	4791,
	4797,
	1137,
	3926,
	1124,
	3886,
	3886,
	3886,
	3886,
	4712,
	3886,
	4797,
	7059,
	4712,
	3886,
	3886,
	6495,
	4797,
	4797,
	4797,
	4797,
	7026,
	6921,
	4797,
	4797,
	4797,
	7026,
	4797,
	4797,
	2227,
	4797,
	6795,
	6795,
	6203,
	5880,
	-1,
	6403,
	6362,
	6795,
	6213,
	5635,
	6503,
	6027,
	6027,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	4797,
	4797,
	4797,
	4755,
	4755,
	4797,
	3926,
	3926,
	3926,
	3536,
	4623,
	4623,
	4797,
	4797,
	4797,
	1542,
	874,
	1124,
	4797,
	6921,
	-1,
	4712,
	3886,
	2224,
	2224,
	4797,
	4797,
	4712,
	4797,
	3886,
	4797,
	3886,
	4797,
	4797,
	3886,
	-1,
	-1,
	-1,
	-1,
	-1,
	3886,
	3886,
	4712,
	4712,
	4712,
	4797,
	4797,
	4797,
	4712,
	2987,
	3352,
	4797,
	4797,
	4712,
	4712,
	4712,
	4797,
	4797,
	4797,
	4797,
	3886,
	4797,
	4797,
	3886,
	4797,
	4797,
	3886,
	3886,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	1151,
	1151,
	4797,
	3760,
	3760,
	3850,
	311,
	1826,
	1826,
	736,
	736,
	241,
	1395,
	3907,
	2416,
	4797,
	4748,
	4676,
	3850,
	4797,
	4797,
	4797,
	3920,
	4797,
	4712,
	969,
	4797,
	4712,
	4712,
	969,
	4797,
	4797,
	4797,
	4712,
	2993,
	1542,
	2993,
	3886,
	725,
	3534,
	4797,
	4797,
	4797,
	4712,
	3920,
	4748,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	2217,
	4797,
	4797,
	4797,
	2020,
	3886,
	4712,
	2997,
	2997,
	2413,
	98,
	4748,
	3886,
	2224,
	2224,
	49,
	4797,
	4797,
	4797,
	7059,
	4712,
	4797,
	4797,
	3886,
	4797,
	4797,
	3886,
	4797,
	4797,
	3886,
	4797,
	4797,
	3886,
	4797,
	4797,
	3886,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	3850,
	4797,
	4748,
	4712,
	4797,
	4712,
	7026,
	4712,
	4712,
	1542,
	4712,
	1541,
	6795,
	1223,
	3886,
	4797,
	4797,
	4797,
	4797,
	3886,
	3886,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	4797,
	3920,
	4797,
	3920,
	4797,
	4797,
	2255,
	4797,
};
static const Il2CppTokenRangePair s_rgctxIndices[7] = 
{
	{ 0x020000C4, { 10, 3 } },
	{ 0x020000C5, { 13, 5 } },
	{ 0x06000203, { 0, 1 } },
	{ 0x0600023D, { 1, 2 } },
	{ 0x060002DB, { 3, 3 } },
	{ 0x06000421, { 6, 1 } },
	{ 0x06000444, { 7, 3 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[18] = 
{
	{ (Il2CppRGCTXDataType)3, 51782 },
	{ (Il2CppRGCTXDataType)3, 50894 },
	{ (Il2CppRGCTXDataType)2, 444 },
	{ (Il2CppRGCTXDataType)3, 15070 },
	{ (Il2CppRGCTXDataType)2, 3676 },
	{ (Il2CppRGCTXDataType)3, 15069 },
	{ (Il2CppRGCTXDataType)2, 10765 },
	{ (Il2CppRGCTXDataType)2, 1202 },
	{ (Il2CppRGCTXDataType)3, 149 },
	{ (Il2CppRGCTXDataType)3, 150 },
	{ (Il2CppRGCTXDataType)2, 1175 },
	{ (Il2CppRGCTXDataType)3, 0 },
	{ (Il2CppRGCTXDataType)3, 50216 },
	{ (Il2CppRGCTXDataType)3, 16237 },
	{ (Il2CppRGCTXDataType)3, 42459 },
	{ (Il2CppRGCTXDataType)3, 41822 },
	{ (Il2CppRGCTXDataType)3, 50180 },
	{ (Il2CppRGCTXDataType)3, 41821 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	1285,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	7,
	s_rgctxIndices,
	18,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
