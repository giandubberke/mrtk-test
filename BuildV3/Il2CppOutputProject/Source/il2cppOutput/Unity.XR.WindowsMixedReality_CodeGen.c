﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Unity.XR.WindowsMR.WindowsMRUsages::.cctor()
extern void WindowsMRUsages__cctor_m3D3E8790574F113E6431A9CFA70AC8EBD49189E0 (void);
// 0x00000002 System.Void UnityEngine.XR.WindowsMRInternals.WindowsMRInternal::.cctor()
extern void WindowsMRInternal__cctor_mD5020D9B8904047143F4DD4D0FC3C138D3A6F278 (void);
// 0x00000003 System.Void UnityEngine.XR.WindowsMRInternals.WindowsMRInternal::UnityWindowsMR_EmulationLibs_SetPluginFolderPaths(System.String)
extern void WindowsMRInternal_UnityWindowsMR_EmulationLibs_SetPluginFolderPaths_m9306CEA22BD88F583ED16C14E01DE83CF7F7A0A3 (void);
// 0x00000004 System.Void UnityEngine.XR.WindowsMRInternals.WindowsMRInternal::.ctor()
extern void WindowsMRInternal__ctor_m038CD74CFF61AA75CD38B1295C037D0265A309C4 (void);
// 0x00000005 System.Void UnityEngine.XR.WindowsMR.NativeApi::UnityWindowsMR_refPoints_start()
extern void NativeApi_UnityWindowsMR_refPoints_start_m9C3BF892D03C13CE76C6AA521EF40AC346F20F23 (void);
// 0x00000006 System.Void UnityEngine.XR.WindowsMR.NativeApi::UnityWindowsMR_refPoints_stop()
extern void NativeApi_UnityWindowsMR_refPoints_stop_m4CB12EBFDCE5E7AD770720FBEB6C513FD0164227 (void);
// 0x00000007 System.Void UnityEngine.XR.WindowsMR.NativeApi::UnityWindowsMR_refPoints_onDestroy()
extern void NativeApi_UnityWindowsMR_refPoints_onDestroy_mC127F30E396474B701E0A6F6979650F1A08F675A (void);
// 0x00000008 System.Void* UnityEngine.XR.WindowsMR.NativeApi::UnityWindowsMR_refPoints_acquireChanges(System.Void*&,System.Int32&,System.Void*&,System.Int32&,System.Void*&,System.Int32&,System.Int32&)
extern void NativeApi_UnityWindowsMR_refPoints_acquireChanges_m25451D94AF29B52A4AB59FA79FEC7822D19AB54A (void);
// 0x00000009 System.Void UnityEngine.XR.WindowsMR.NativeApi::UnityWindowsMR_refPoints_releaseChanges(System.Void*)
extern void NativeApi_UnityWindowsMR_refPoints_releaseChanges_m5BA3A089F36DB2FD24A0E244A5E8F866C320A37A (void);
// 0x0000000A System.Boolean UnityEngine.XR.WindowsMR.NativeApi::UnityWindowsMR_refPoints_tryAdd(UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRAnchor&)
extern void NativeApi_UnityWindowsMR_refPoints_tryAdd_mB4C1C91407874F2D6249C60C4E1928C1F33DFFB4 (void);
// 0x0000000B System.Boolean UnityEngine.XR.WindowsMR.NativeApi::UnityWindowsMR_refPoints_tryRemove(UnityEngine.XR.ARSubsystems.TrackableId)
extern void NativeApi_UnityWindowsMR_refPoints_tryRemove_m70F0D502BEB00FC4BBDAA43290278EF4B43AE0D8 (void);
// 0x0000000C System.Void UnityEngine.XR.WindowsMR.WindowsMRAnchorSubsystem::RegisterDescriptor()
extern void WindowsMRAnchorSubsystem_RegisterDescriptor_mAB0469AE262603106845FFF4960FABC2C0423E2F (void);
// 0x0000000D System.Void UnityEngine.XR.WindowsMR.WindowsMRAnchorSubsystem::.ctor()
extern void WindowsMRAnchorSubsystem__ctor_m57630B41DD7A72E0B5989DD4C0C4B2A090C0C914 (void);
// 0x0000000E System.Void UnityEngine.XR.WindowsMR.WindowsMRAnchorSubsystem/WindowsMRProvider::Start()
extern void WindowsMRProvider_Start_m24FCDC8D6DE5ED818DCFFC935A99CB8C856D0DAC (void);
// 0x0000000F System.Void UnityEngine.XR.WindowsMR.WindowsMRAnchorSubsystem/WindowsMRProvider::Stop()
extern void WindowsMRProvider_Stop_m3D898745F14BE48639CA64C4885B8821234706D9 (void);
// 0x00000010 System.Void UnityEngine.XR.WindowsMR.WindowsMRAnchorSubsystem/WindowsMRProvider::Destroy()
extern void WindowsMRProvider_Destroy_m00461C208F6734C01EFB5B7EA37FD6F393B6806C (void);
// 0x00000011 UnityEngine.XR.ARSubsystems.TrackableChanges`1<UnityEngine.XR.ARSubsystems.XRAnchor> UnityEngine.XR.WindowsMR.WindowsMRAnchorSubsystem/WindowsMRProvider::GetChanges(UnityEngine.XR.ARSubsystems.XRAnchor,Unity.Collections.Allocator)
extern void WindowsMRProvider_GetChanges_mFDCD5B8A8DE642AA77A49A693A689D30D9D92043 (void);
// 0x00000012 System.Boolean UnityEngine.XR.WindowsMR.WindowsMRAnchorSubsystem/WindowsMRProvider::TryAddAnchor(UnityEngine.Pose,UnityEngine.XR.ARSubsystems.XRAnchor&)
extern void WindowsMRProvider_TryAddAnchor_m9CF29D14BC115E65D6BAB77AEF296914D06A6E9B (void);
// 0x00000013 System.Boolean UnityEngine.XR.WindowsMR.WindowsMRAnchorSubsystem/WindowsMRProvider::TryRemoveAnchor(UnityEngine.XR.ARSubsystems.TrackableId)
extern void WindowsMRProvider_TryRemoveAnchor_mFA91393D3E9B94E9196B80D8C68BF6819B000385 (void);
// 0x00000014 System.Void UnityEngine.XR.WindowsMR.WindowsMRAnchorSubsystem/WindowsMRProvider::.ctor()
extern void WindowsMRProvider__ctor_mDA307C80111BC8B089C5B052FB8D1DD4DA9F24EC (void);
// 0x00000015 System.Void UnityEngine.XR.WindowsMR.FeatureApi::SetFeatureRequested(UnityEngine.XR.ARSubsystems.Feature,System.Boolean)
extern void FeatureApi_SetFeatureRequested_m68076BE5A3C67FDE1BE5012CEDDDE950472C4CB5 (void);
// 0x00000016 UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.WindowsMR.FeatureApi::get_RequestedFeatures()
extern void FeatureApi_get_RequestedFeatures_mFB5372D18A54D1589959D7CB5008AA65F1DC4673 (void);
// 0x00000017 Unity.Collections.NativeArray`1<UnityEngine.XR.WindowsMR.WindowsMRHoldGestureEvent> UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem::get_holdGestureEvents()
extern void WindowsMRGestureSubsystem_get_holdGestureEvents_m835C1731B08A0D7E64B701795E08668402007DDC (void);
// 0x00000018 Unity.Collections.NativeArray`1<UnityEngine.XR.WindowsMR.WindowsMRManipulationGestureEvent> UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem::get_manipulationGestureEvents()
extern void WindowsMRGestureSubsystem_get_manipulationGestureEvents_mDE2A52E9C69598DEE55022F803BA7ABC59C22B49 (void);
// 0x00000019 Unity.Collections.NativeArray`1<UnityEngine.XR.WindowsMR.WindowsMRNavigationGestureEvent> UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem::get_navigationGestureEvents()
extern void WindowsMRGestureSubsystem_get_navigationGestureEvents_mDF579B8A385FF11EEC092D843B66ABA238975894 (void);
// 0x0000001A Unity.Collections.NativeArray`1<UnityEngine.XR.WindowsMR.WindowsMRTappedGestureEvent> UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem::get_tappedGestureEvents()
extern void WindowsMRGestureSubsystem_get_tappedGestureEvents_mFE3FCB3D4D018C45864ABABE64CE78D976E7554E (void);
// 0x0000001B UnityEngine.XR.InteractionSubsystems.XRGestureSubsystem/Provider UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem::CreateProvider()
extern void WindowsMRGestureSubsystem_CreateProvider_mCE1AAC022D7EDF4101B43AB479CA8665967E7E02 (void);
// 0x0000001C System.Boolean UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem::SetEnableManipulationGesture(System.Boolean)
extern void WindowsMRGestureSubsystem_SetEnableManipulationGesture_m2B953B6D34239BE33C9DAE745368CFB3804E8ACE (void);
// 0x0000001D System.Boolean UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem::SetEnableNavigationGesture(System.Boolean)
extern void WindowsMRGestureSubsystem_SetEnableNavigationGesture_m98AF0437167F3412FD251777B4935CBDBB97A1A2 (void);
// 0x0000001E System.Void UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem::RegisterDescriptor()
extern void WindowsMRGestureSubsystem_RegisterDescriptor_m51BE6A1D750D1B24BADEA396AF9BADD515152DB4 (void);
// 0x0000001F UnityEngine.XR.InteractionSubsystems.GestureId UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem::GetNextGUID()
extern void WindowsMRGestureSubsystem_GetNextGUID_mCAFD7110250C3330F09696214E81FF8DE03CEADC (void);
// 0x00000020 System.Void UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem::.ctor()
extern void WindowsMRGestureSubsystem__ctor_m78B5BEE872561E8D35E1755385B098112528D2C3 (void);
// 0x00000021 System.Void UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem::.cctor()
extern void WindowsMRGestureSubsystem__cctor_m1602650A1D7132522E4D5992619A45BBBAB0AA97 (void);
// 0x00000022 System.Void UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/WindowsMRGestureProvider::.ctor(UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem)
extern void WindowsMRGestureProvider__ctor_mA04198031707F3B34AF35CA504A75CAC7689CE35 (void);
// 0x00000023 System.Void UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/WindowsMRGestureProvider::Start()
extern void WindowsMRGestureProvider_Start_m71387CF62855FEEEBE2CB46B8BA66716D7BF4D8F (void);
// 0x00000024 System.Void UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/WindowsMRGestureProvider::Stop()
extern void WindowsMRGestureProvider_Stop_mDA357182AB707168E52D59A2816764E295091927 (void);
// 0x00000025 System.Void UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/WindowsMRGestureProvider::Update()
extern void WindowsMRGestureProvider_Update_mC450709588DAEAB57ADA028AC5B6D155FA9C3C86 (void);
// 0x00000026 System.Void UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/WindowsMRGestureProvider::RetrieveGestureEvents(UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/WindowsMRGestureProvider/GetGestureEventsPtrFunction,Unity.Collections.NativeArray`1<T>&)
// 0x00000027 System.Void UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/WindowsMRGestureProvider::RetrieveAllGestureEvents()
extern void WindowsMRGestureProvider_RetrieveAllGestureEvents_m54B05DFA5B516E1274EB32F16CE8BC79BCCAF2D5 (void);
// 0x00000028 System.Void UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/WindowsMRGestureProvider::Destroy()
extern void WindowsMRGestureProvider_Destroy_mC91404D26F033BB2B7C7A14FD7E3A35E253FC5FE (void);
// 0x00000029 System.Boolean UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/WindowsMRGestureProvider::SetEnableManipulationGesture(System.Boolean)
extern void WindowsMRGestureProvider_SetEnableManipulationGesture_m648642E50116C3D6C0196DD6DC930EEC5FC64BC4 (void);
// 0x0000002A System.Boolean UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/WindowsMRGestureProvider::SetEnableNavigationGesture(System.Boolean)
extern void WindowsMRGestureProvider_SetEnableNavigationGesture_mA9956AD7DB20A6E09BDE5A5BC465A609F2FE683C (void);
// 0x0000002B Unity.Collections.NativeArray`1<UnityEngine.XR.WindowsMR.WindowsMRHoldGestureEvent> UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/WindowsMRGestureProvider::get_holdGestureEvents()
extern void WindowsMRGestureProvider_get_holdGestureEvents_mC4F61F0F923190C315A7C1FF4EC47FC9E848CE75 (void);
// 0x0000002C Unity.Collections.NativeArray`1<UnityEngine.XR.WindowsMR.WindowsMRManipulationGestureEvent> UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/WindowsMRGestureProvider::get_manipulationGestureEvents()
extern void WindowsMRGestureProvider_get_manipulationGestureEvents_mF3C0B8F022717BA75E60B4D25D5AFE3BBD7499D1 (void);
// 0x0000002D Unity.Collections.NativeArray`1<UnityEngine.XR.WindowsMR.WindowsMRNavigationGestureEvent> UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/WindowsMRGestureProvider::get_navigationGestureEvents()
extern void WindowsMRGestureProvider_get_navigationGestureEvents_m4396B2839123D403BC77F706BD35C75CF2C48739 (void);
// 0x0000002E Unity.Collections.NativeArray`1<UnityEngine.XR.WindowsMR.WindowsMRTappedGestureEvent> UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/WindowsMRGestureProvider::get_tappedGestureEvents()
extern void WindowsMRGestureProvider_get_tappedGestureEvents_m536D1C5781E7879B343D029B81F8F024153D3704 (void);
// 0x0000002F System.Void UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/WindowsMRGestureProvider/GetGestureEventsPtrFunction::.ctor(System.Object,System.IntPtr)
extern void GetGestureEventsPtrFunction__ctor_m33C2C1337DA78792600E4A26B47DAD9AB10DE5DB (void);
// 0x00000030 System.Void* UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/WindowsMRGestureProvider/GetGestureEventsPtrFunction::Invoke(System.Int32&,System.Int32&)
extern void GetGestureEventsPtrFunction_Invoke_m9183A48EEB89837ED13990399767C664E943AA4E (void);
// 0x00000031 System.IAsyncResult UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/WindowsMRGestureProvider/GetGestureEventsPtrFunction::BeginInvoke(System.Int32&,System.Int32&,System.AsyncCallback,System.Object)
extern void GetGestureEventsPtrFunction_BeginInvoke_m1AAE64ECB80C91BD2A96E96A8BDE5FBC17D1742D (void);
// 0x00000032 System.Void* UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/WindowsMRGestureProvider/GetGestureEventsPtrFunction::EndInvoke(System.Int32&,System.Int32&,System.IAsyncResult)
extern void GetGestureEventsPtrFunction_EndInvoke_m27E0BAC2A76C7237349C47306B57DE3B350E5630 (void);
// 0x00000033 System.Void UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/NativeApi::UnityWindowsMR_GesturesUpdate()
extern void NativeApi_UnityWindowsMR_GesturesUpdate_m105715666D54A509B20F5D823FDA5D7843F86242 (void);
// 0x00000034 System.Void UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/NativeApi::UnityWindowsMR_GesturesStart()
extern void NativeApi_UnityWindowsMR_GesturesStart_m47E7B0053FE2AFD2CB4CC52D2A33793A5D662776 (void);
// 0x00000035 System.Void UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/NativeApi::UnityWindowsMR_GesturesDestroy()
extern void NativeApi_UnityWindowsMR_GesturesDestroy_m42B3DC67DE8E59C7088BE748703B72649359A490 (void);
// 0x00000036 System.Void UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/NativeApi::UnityWindowsMR_GesturesStop()
extern void NativeApi_UnityWindowsMR_GesturesStop_m3A52410586A3E5FC545FD72B56B4EF26049BF89D (void);
// 0x00000037 System.Void* UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/NativeApi::UnityWindowsMR_GesturesGetHoldGestureEventsPtr(System.Int32&,System.Int32&)
extern void NativeApi_UnityWindowsMR_GesturesGetHoldGestureEventsPtr_m007F2ED563B3C9F6A78B72B471D2D8922A41696E (void);
// 0x00000038 System.Void* UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/NativeApi::UnityWindowsMR_GesturesGetManipulationGestureEventsPtr(System.Int32&,System.Int32&)
extern void NativeApi_UnityWindowsMR_GesturesGetManipulationGestureEventsPtr_mD2BA69B9525B0201DC0229FB0D4BF3C1F4D6D722 (void);
// 0x00000039 System.Void* UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/NativeApi::UnityWindowsMR_GesturesGetNavigationGestureEventsPtr(System.Int32&,System.Int32&)
extern void NativeApi_UnityWindowsMR_GesturesGetNavigationGestureEventsPtr_m08B57A2BBFE8CB3257D3FCB2969E1026A2BF3EA5 (void);
// 0x0000003A System.Void* UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/NativeApi::UnityWindowsMR_GesturesGetTappedGestureEventsPtr(System.Int32&,System.Int32&)
extern void NativeApi_UnityWindowsMR_GesturesGetTappedGestureEventsPtr_m96F91B88ACE51F7C48D79EF81D5BD3C3C4689577 (void);
// 0x0000003B System.Boolean UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/NativeApi::UnityWindowsMR_GesturesSetEnableManipulationGesture(System.Boolean)
extern void NativeApi_UnityWindowsMR_GesturesSetEnableManipulationGesture_mDADE197B2456CB9D80562972458208EB18805DF2 (void);
// 0x0000003C System.Boolean UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem/NativeApi::UnityWindowsMR_GesturesSetEnableNavigationGesture(System.Boolean)
extern void NativeApi_UnityWindowsMR_GesturesSetEnableNavigationGesture_m970E855C29FD51C237BCAB9C45DD1B29CA540905 (void);
// 0x0000003D UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem UnityEngine.XR.WindowsMR.WindowsMRGestures::get_gestureSubsystem()
extern void WindowsMRGestures_get_gestureSubsystem_mCB77A743ECC56D5A40ECFBC2EDCAAB106C16FE44 (void);
// 0x0000003E System.Void UnityEngine.XR.WindowsMR.WindowsMRGestures::set_gestureSubsystem(UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem)
extern void WindowsMRGestures_set_gestureSubsystem_m2F5C44E348B4AB1E27D3EECEF2FCDE70BC23261D (void);
// 0x0000003F System.Void UnityEngine.XR.WindowsMR.WindowsMRGestures::add_onHoldChanged(System.Action`1<UnityEngine.XR.WindowsMR.WindowsMRHoldGestureEvent>)
extern void WindowsMRGestures_add_onHoldChanged_m328A5433CB0D2E89E4DDB4A647F251700839873F (void);
// 0x00000040 System.Void UnityEngine.XR.WindowsMR.WindowsMRGestures::remove_onHoldChanged(System.Action`1<UnityEngine.XR.WindowsMR.WindowsMRHoldGestureEvent>)
extern void WindowsMRGestures_remove_onHoldChanged_m47A0A57066419050A94DE0885E1D3FB32D4A5FCA (void);
// 0x00000041 System.Void UnityEngine.XR.WindowsMR.WindowsMRGestures::add_onManipulationChanged(System.Action`1<UnityEngine.XR.WindowsMR.WindowsMRManipulationGestureEvent>)
extern void WindowsMRGestures_add_onManipulationChanged_m9BAC67C47F466F99CF772330FCFF8F4A998A71D3 (void);
// 0x00000042 System.Void UnityEngine.XR.WindowsMR.WindowsMRGestures::remove_onManipulationChanged(System.Action`1<UnityEngine.XR.WindowsMR.WindowsMRManipulationGestureEvent>)
extern void WindowsMRGestures_remove_onManipulationChanged_m38EB412593B1C979BBF541F4C3EAD5E29C78B0F4 (void);
// 0x00000043 System.Void UnityEngine.XR.WindowsMR.WindowsMRGestures::add_onNavigationChanged(System.Action`1<UnityEngine.XR.WindowsMR.WindowsMRNavigationGestureEvent>)
extern void WindowsMRGestures_add_onNavigationChanged_m23678825E6CA8718C536EF895C107D1B8CFEAF98 (void);
// 0x00000044 System.Void UnityEngine.XR.WindowsMR.WindowsMRGestures::remove_onNavigationChanged(System.Action`1<UnityEngine.XR.WindowsMR.WindowsMRNavigationGestureEvent>)
extern void WindowsMRGestures_remove_onNavigationChanged_mA720579EA3F7260C18788D8400420C6FE3E441D9 (void);
// 0x00000045 System.Void UnityEngine.XR.WindowsMR.WindowsMRGestures::add_onTappedChanged(System.Action`1<UnityEngine.XR.WindowsMR.WindowsMRTappedGestureEvent>)
extern void WindowsMRGestures_add_onTappedChanged_mA5EFD4421826488C4579C0B27A2E09B5F355F965 (void);
// 0x00000046 System.Void UnityEngine.XR.WindowsMR.WindowsMRGestures::remove_onTappedChanged(System.Action`1<UnityEngine.XR.WindowsMR.WindowsMRTappedGestureEvent>)
extern void WindowsMRGestures_remove_onTappedChanged_mFF97BB3820F3B2CB25F3459BCD92E80A9BFAB581 (void);
// 0x00000047 System.Void UnityEngine.XR.WindowsMR.WindowsMRGestures::add_onActivate(System.Action`1<UnityEngine.XR.InteractionSubsystems.ActivateGestureEvent>)
extern void WindowsMRGestures_add_onActivate_mC6028A20DADE8043E02706671EC3CA004BCD7B3D (void);
// 0x00000048 System.Void UnityEngine.XR.WindowsMR.WindowsMRGestures::remove_onActivate(System.Action`1<UnityEngine.XR.InteractionSubsystems.ActivateGestureEvent>)
extern void WindowsMRGestures_remove_onActivate_m9968E16BE11A2F6F500DDA24BC28D75E18E6894B (void);
// 0x00000049 UnityEngine.XR.WindowsMR.WindowsMRGestureSubsystem UnityEngine.XR.WindowsMR.WindowsMRGestures::GetGestureSubsystemIfNeeded()
extern void WindowsMRGestures_GetGestureSubsystemIfNeeded_m94F38F8C2A43A68E7A0A0D48D9E270128A7B9944 (void);
// 0x0000004A System.Void UnityEngine.XR.WindowsMR.WindowsMRGestures::Start()
extern void WindowsMRGestures_Start_m016B42743DACA6F7279E2085FE435194DCE38F24 (void);
// 0x0000004B System.Void UnityEngine.XR.WindowsMR.WindowsMRGestures::OnValidate()
extern void WindowsMRGestures_OnValidate_m6DE1F642001DB8E048D3CAD1838A73D8C8C8DDF7 (void);
// 0x0000004C System.Void UnityEngine.XR.WindowsMR.WindowsMRGestures::Update()
extern void WindowsMRGestures_Update_mC8282BB806EC1FCD84197E222C281CECE5CC1932 (void);
// 0x0000004D System.Void UnityEngine.XR.WindowsMR.WindowsMRGestures::.ctor()
extern void WindowsMRGestures__ctor_m8D1EA29967E87CA55E90B31A58742F7FC527D8F7 (void);
// 0x0000004E UnityEngine.XR.InteractionSubsystems.GestureId UnityEngine.XR.WindowsMR.WindowsMRHoldGestureEvent::get_id()
extern void WindowsMRHoldGestureEvent_get_id_m6D8F5DE158524197B3E9A921C2274C29D8ED6AF3 (void);
// 0x0000004F UnityEngine.XR.InteractionSubsystems.GestureState UnityEngine.XR.WindowsMR.WindowsMRHoldGestureEvent::get_state()
extern void WindowsMRHoldGestureEvent_get_state_mEE42F56DDE2EC21257E7122B992E55BB062C2D4F (void);
// 0x00000050 System.String UnityEngine.XR.WindowsMR.WindowsMRHoldGestureEvent::ToString()
extern void WindowsMRHoldGestureEvent_ToString_m87D4381289ADC2F1A251700BC84E5968B2E01DC6 (void);
// 0x00000051 System.Boolean UnityEngine.XR.WindowsMR.WindowsMRHoldGestureEvent::Equals(System.Object)
extern void WindowsMRHoldGestureEvent_Equals_m56B218188024BACAE6C47C5CDD088801D9313C6E (void);
// 0x00000052 System.Int32 UnityEngine.XR.WindowsMR.WindowsMRHoldGestureEvent::GetHashCode()
extern void WindowsMRHoldGestureEvent_GetHashCode_m8F2502D512F5C48BB9DF4DB25ADDCF1408B27F7A (void);
// 0x00000053 System.Boolean UnityEngine.XR.WindowsMR.WindowsMRHoldGestureEvent::Equals(UnityEngine.XR.WindowsMR.WindowsMRHoldGestureEvent)
extern void WindowsMRHoldGestureEvent_Equals_mEAD4A09537B715ECABF41E2F6AA5C7DCDCC64F33 (void);
// 0x00000054 UnityEngine.XR.InteractionSubsystems.GestureId UnityEngine.XR.WindowsMR.WindowsMRManipulationGestureEvent::get_id()
extern void WindowsMRManipulationGestureEvent_get_id_mA4D42C442AE74BDCB6C2A3AE10096458C86B0F00 (void);
// 0x00000055 UnityEngine.XR.InteractionSubsystems.GestureState UnityEngine.XR.WindowsMR.WindowsMRManipulationGestureEvent::get_state()
extern void WindowsMRManipulationGestureEvent_get_state_mAE0B1DD206F527881FD05F5C0A02A13D0BB3D8C7 (void);
// 0x00000056 UnityEngine.Vector3 UnityEngine.XR.WindowsMR.WindowsMRManipulationGestureEvent::get_cumulativeDelta()
extern void WindowsMRManipulationGestureEvent_get_cumulativeDelta_m8159C668C6A2E207801668C9961ADCCEE03B383F (void);
// 0x00000057 System.String UnityEngine.XR.WindowsMR.WindowsMRManipulationGestureEvent::ToString()
extern void WindowsMRManipulationGestureEvent_ToString_mB763E2D65DD38FC5F7AFA42824D023FF5943179D (void);
// 0x00000058 System.Boolean UnityEngine.XR.WindowsMR.WindowsMRManipulationGestureEvent::Equals(System.Object)
extern void WindowsMRManipulationGestureEvent_Equals_mE627F94BCC8D89061D669A4E151B55DD85A862BE (void);
// 0x00000059 System.Int32 UnityEngine.XR.WindowsMR.WindowsMRManipulationGestureEvent::GetHashCode()
extern void WindowsMRManipulationGestureEvent_GetHashCode_mFEECAF5AB7081A6C3B49FCEAAA48BBA6455D8EA4 (void);
// 0x0000005A System.Boolean UnityEngine.XR.WindowsMR.WindowsMRManipulationGestureEvent::Equals(UnityEngine.XR.WindowsMR.WindowsMRManipulationGestureEvent)
extern void WindowsMRManipulationGestureEvent_Equals_m6D3678EC3350E411E4CE20F74A060431A4B3BC4D (void);
// 0x0000005B UnityEngine.XR.InteractionSubsystems.GestureId UnityEngine.XR.WindowsMR.WindowsMRNavigationGestureEvent::get_id()
extern void WindowsMRNavigationGestureEvent_get_id_m6AA1C10874F845A4832FCB7352A8E4A200737A4F (void);
// 0x0000005C UnityEngine.XR.InteractionSubsystems.GestureState UnityEngine.XR.WindowsMR.WindowsMRNavigationGestureEvent::get_state()
extern void WindowsMRNavigationGestureEvent_get_state_m85B727B4B39939571352B5FBC1BEBBDF2723895F (void);
// 0x0000005D UnityEngine.Vector3 UnityEngine.XR.WindowsMR.WindowsMRNavigationGestureEvent::get_normalizedOffset()
extern void WindowsMRNavigationGestureEvent_get_normalizedOffset_m10CD89AAAA3020E56E087B2D349790C82005A343 (void);
// 0x0000005E System.String UnityEngine.XR.WindowsMR.WindowsMRNavigationGestureEvent::ToString()
extern void WindowsMRNavigationGestureEvent_ToString_m62E7AC359CD0C94B843B0348297C742CE9DB596E (void);
// 0x0000005F System.Boolean UnityEngine.XR.WindowsMR.WindowsMRNavigationGestureEvent::Equals(System.Object)
extern void WindowsMRNavigationGestureEvent_Equals_m2BC1623B58B628CF58FBB637A43E22812F397269 (void);
// 0x00000060 System.Int32 UnityEngine.XR.WindowsMR.WindowsMRNavigationGestureEvent::GetHashCode()
extern void WindowsMRNavigationGestureEvent_GetHashCode_m7319B536FE9E03BE170744D0A5025A3C666F98FD (void);
// 0x00000061 System.Boolean UnityEngine.XR.WindowsMR.WindowsMRNavigationGestureEvent::Equals(UnityEngine.XR.WindowsMR.WindowsMRNavigationGestureEvent)
extern void WindowsMRNavigationGestureEvent_Equals_mF04C6DDC5E5A614DB2DD168E32645870E13C99BF (void);
// 0x00000062 UnityEngine.XR.InteractionSubsystems.GestureId UnityEngine.XR.WindowsMR.WindowsMRTappedGestureEvent::get_id()
extern void WindowsMRTappedGestureEvent_get_id_mCF2F486894D4DDF592B5CFFE918F3F1E764911F9 (void);
// 0x00000063 UnityEngine.XR.InteractionSubsystems.GestureState UnityEngine.XR.WindowsMR.WindowsMRTappedGestureEvent::get_state()
extern void WindowsMRTappedGestureEvent_get_state_mF6039113078438AC618BDD0451A05A1F72566A52 (void);
// 0x00000064 System.Int32 UnityEngine.XR.WindowsMR.WindowsMRTappedGestureEvent::get_tappedCount()
extern void WindowsMRTappedGestureEvent_get_tappedCount_m1C3775FCF65723A87630119978CCA1AB81769136 (void);
// 0x00000065 System.String UnityEngine.XR.WindowsMR.WindowsMRTappedGestureEvent::ToString()
extern void WindowsMRTappedGestureEvent_ToString_m26DC834236CF0F10B818A2565D06207A18C9D9DE (void);
// 0x00000066 System.Boolean UnityEngine.XR.WindowsMR.WindowsMRTappedGestureEvent::Equals(System.Object)
extern void WindowsMRTappedGestureEvent_Equals_mD86527B1974A47F50042CC65CE6E0CE93655680B (void);
// 0x00000067 System.Int32 UnityEngine.XR.WindowsMR.WindowsMRTappedGestureEvent::GetHashCode()
extern void WindowsMRTappedGestureEvent_GetHashCode_mB512A915C951F132527EE4B0E6DE7EE073841C48 (void);
// 0x00000068 System.Boolean UnityEngine.XR.WindowsMR.WindowsMRTappedGestureEvent::Equals(UnityEngine.XR.WindowsMR.WindowsMRTappedGestureEvent)
extern void WindowsMRTappedGestureEvent_Equals_mABD39CE8DD91E3D967199BF869FA47F3B70CE439 (void);
// 0x00000069 System.Void UnityEngine.XR.WindowsMR.WindowsMRSessionSubsystem::RegisterDescriptor()
extern void WindowsMRSessionSubsystem_RegisterDescriptor_m73439E623C9E00BF613B760FE61E4BE1883BAB54 (void);
// 0x0000006A System.Void UnityEngine.XR.WindowsMR.WindowsMRSessionSubsystem::.ctor()
extern void WindowsMRSessionSubsystem__ctor_mD15A2CD789E730766104A1A6E765EA7C60F8CFED (void);
// 0x0000006B System.Void UnityEngine.XR.WindowsMR.WindowsMRSessionSubsystem/WindowsMRProvider::.ctor()
extern void WindowsMRProvider__ctor_m4A3FF4D4655B462A0071F46643E7BA4092A2E9D2 (void);
// 0x0000006C UnityEngine.XR.ARSubsystems.Promise`1<UnityEngine.XR.ARSubsystems.SessionAvailability> UnityEngine.XR.WindowsMR.WindowsMRSessionSubsystem/WindowsMRProvider::GetAvailabilityAsync()
extern void WindowsMRProvider_GetAvailabilityAsync_m53FA642CFCA72479E1C201A5A914CF6024BB2561 (void);
// 0x0000006D UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.WindowsMR.WindowsMRSessionSubsystem/WindowsMRProvider::get_trackingState()
extern void WindowsMRProvider_get_trackingState_mB83B60D84CDD264A1DCF84FE4C1D1D7F450905CB (void);
// 0x0000006E UnityEngine.XR.ARSubsystems.NotTrackingReason UnityEngine.XR.WindowsMR.WindowsMRSessionSubsystem/WindowsMRProvider::get_notTrackingReason()
extern void WindowsMRProvider_get_notTrackingReason_m2B5E3A4B952885ED10C738020E392E53284703B1 (void);
// 0x0000006F System.Void UnityEngine.XR.WindowsMR.WindowsMRSessionSubsystem/WindowsMRProvider::Destroy()
extern void WindowsMRProvider_Destroy_m3483C848E702BDCB773CDA06EB3A80D2347938EB (void);
// 0x00000070 System.Void UnityEngine.XR.WindowsMR.WindowsMRSessionSubsystem/WindowsMRProvider::Stop()
extern void WindowsMRProvider_Stop_m5C2A414212AABD3B3757124E6DEF12A6070A0C73 (void);
// 0x00000071 System.Void UnityEngine.XR.WindowsMR.WindowsMRSessionSubsystem/WindowsMRProvider::Start()
extern void WindowsMRProvider_Start_m1ADCFD1C8E426AC99AB4FF97BBD1B915B31E56E5 (void);
// 0x00000072 System.Void UnityEngine.XR.WindowsMR.WindowsMRSessionSubsystem/WindowsMRProvider::Reset()
extern void WindowsMRProvider_Reset_m0EE77F20778AEA40B270F34A5D5D10AA70667895 (void);
// 0x00000073 UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.WindowsMR.WindowsMRSessionSubsystem/WindowsMRProvider::get_requestedFeatures()
extern void WindowsMRProvider_get_requestedFeatures_mCD8BEFE1EAB16E348DC2F9BE4103E2EBA54D4355 (void);
// 0x00000074 UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.WindowsMR.WindowsMRSessionSubsystem/WindowsMRProvider::get_requestedTrackingMode()
extern void WindowsMRProvider_get_requestedTrackingMode_m4026DE438DCD67C1A3573A52AC310DD333AF5A64 (void);
// 0x00000075 System.Void UnityEngine.XR.WindowsMR.WindowsMRSessionSubsystem/WindowsMRProvider::set_requestedTrackingMode(UnityEngine.XR.ARSubsystems.Feature)
extern void WindowsMRProvider_set_requestedTrackingMode_mDF1576DD78C9D8F332BA26FC76B6A0A22E6EE5F5 (void);
// 0x00000076 UnityEngine.XR.ARSubsystems.Feature UnityEngine.XR.WindowsMR.WindowsMRSessionSubsystem/WindowsMRProvider::get_currentTrackingMode()
extern void WindowsMRProvider_get_currentTrackingMode_m479CB3BF04B2658E7E2219D5B6A3651228E62E38 (void);
// 0x00000077 Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.ConfigurationDescriptor> UnityEngine.XR.WindowsMR.WindowsMRSessionSubsystem/WindowsMRProvider::GetConfigurationDescriptors(Unity.Collections.Allocator)
extern void WindowsMRProvider_GetConfigurationDescriptors_mBD1E8AB093A51E6685E8916AC39747838DC11077 (void);
// 0x00000078 System.Void UnityEngine.XR.WindowsMR.WindowsMRSessionSubsystem/NativeApi::UnityWindowsMR_session_construct()
extern void NativeApi_UnityWindowsMR_session_construct_m0772E1E49E93894E66C7E67DE52498F411934A1D (void);
// 0x00000079 System.Void UnityEngine.XR.WindowsMR.WindowsMRSessionSubsystem/NativeApi::UnityWindowsMR_session_destroy()
extern void NativeApi_UnityWindowsMR_session_destroy_mED894C23E7A76288878480A76E269DF873713445 (void);
// 0x0000007A System.Void UnityEngine.XR.WindowsMR.WindowsMRSessionSubsystem/NativeApi::UnityWindowsMR_session_pause()
extern void NativeApi_UnityWindowsMR_session_pause_mB099D3E3A8CBCAFE10980620C540F819A384FCEA (void);
// 0x0000007B System.Void UnityEngine.XR.WindowsMR.WindowsMRSessionSubsystem/NativeApi::UnityWindowsMR_session_resume()
extern void NativeApi_UnityWindowsMR_session_resume_mAD3AE6795D01A9865280A9C025BF946E552420C9 (void);
// 0x0000007C System.Void UnityEngine.XR.WindowsMR.WindowsMRSessionSubsystem/NativeApi::UnityWindowsMR_session_reset()
extern void NativeApi_UnityWindowsMR_session_reset_m14CA2F89C86AA96F023A47480A8EFCF6D9090168 (void);
// 0x0000007D UnityEngine.XR.ARSubsystems.TrackingState UnityEngine.XR.WindowsMR.WindowsMRSessionSubsystem/NativeApi::UnityWindowsMR_session_getTrackingState()
extern void NativeApi_UnityWindowsMR_session_getTrackingState_m7792190745A241CAF1F38C3730BD705F43ED5619 (void);
// 0x0000007E UnityEngine.XR.ARSubsystems.NotTrackingReason UnityEngine.XR.WindowsMR.WindowsMRSessionSubsystem/NativeApi::UnityWindowsMR_session_getNotTrackingReason()
extern void NativeApi_UnityWindowsMR_session_getNotTrackingReason_mC8D47C664D9C751729F6F7B052C67E0DCB842E37 (void);
// 0x0000007F System.IntPtr UnityEngine.XR.WindowsMR.WindowsMREnvironment::get_OriginSpatialCoordinateSystem()
extern void WindowsMREnvironment_get_OriginSpatialCoordinateSystem_m3609FBD7226520512F8F2A8A5D0FFC1E2A0B015E (void);
// 0x00000080 System.IntPtr UnityEngine.XR.WindowsMR.WindowsMREnvironment::get_CurrentHolographicRenderFrame()
extern void WindowsMREnvironment_get_CurrentHolographicRenderFrame_mCCAE9BE6650F820FCD43EAE2403DA10884732FCC (void);
// 0x00000081 System.Void UnityEngine.XR.WindowsMR.WindowsMRInput::GetCurrentSourceStates(UnityEngine.XR.XRInputSubsystem,System.Collections.Generic.List`1<System.Object>)
extern void WindowsMRInput_GetCurrentSourceStates_m39D0AA035DFA6D7CAC042B6293C7DC10970DDF36 (void);
// 0x00000082 System.Int32 UnityEngine.XR.WindowsMR.WindowsMRInput/NativeApi::GetCountOfSourceStates()
extern void NativeApi_GetCountOfSourceStates_m90A532EFC11524CF3C52F15489944BE5A6414460 (void);
// 0x00000083 System.Void UnityEngine.XR.WindowsMR.WindowsMRInput/NativeApi::GetAllSourceStates(System.IntPtr[],System.Int32)
extern void NativeApi_GetAllSourceStates_mDB6021165C7146885EFB3B13CE0306ABED0B936E (void);
// 0x00000084 System.Void UnityEngine.XR.WindowsMR.WindowsMRInput/NativeApi::ReleaseAllSourceStates(System.IntPtr[],System.Int32)
extern void NativeApi_ReleaseAllSourceStates_mDF2407698A295271486AD53BD7FD993EE13674CD (void);
// 0x00000085 System.Void UnityEngine.XR.WindowsMR.WindowsMRExtensions::SetBoundingVolumeOrientedBox(UnityEngine.XR.XRMeshSubsystem,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Quaternion)
extern void WindowsMRExtensions_SetBoundingVolumeOrientedBox_mD1E8C44B923142D5123D13041D50504647963C02 (void);
// 0x00000086 System.Void UnityEngine.XR.WindowsMR.WindowsMRExtensions::SetBoundingVolumeSphere(UnityEngine.XR.XRMeshSubsystem,UnityEngine.Vector3,System.Single)
extern void WindowsMRExtensions_SetBoundingVolumeSphere_mBAE41AC8530E7A8C68311A65BD7FAA1E5EDAD96D (void);
// 0x00000087 System.Void UnityEngine.XR.WindowsMR.WindowsMRExtensions::.cctor()
extern void WindowsMRExtensions__cctor_m3C1EA92ED95503D4E6273D0A429F5F842BA7B74A (void);
// 0x00000088 System.Void UnityEngine.XR.WindowsMR.WindowsMRExtensions/NativeApi::SetBoundingVolumeOrientedBox(UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMROrientedBox)
extern void NativeApi_SetBoundingVolumeOrientedBox_mAB3006409DF0359C342B295CF2F2F6EF88F51315 (void);
// 0x00000089 System.Void UnityEngine.XR.WindowsMR.WindowsMRExtensions/NativeApi::SetBoundingVolumeSphere(UnityEngine.XR.WindowsMR.WindowsMRExtensions/WMRSphere)
extern void NativeApi_SetBoundingVolumeSphere_m0413E84F7FD625C7E737F2FCEDE7D5A426A7F628 (void);
// 0x0000008A System.IntPtr UnityEngine.XR.WindowsMR.Native::GetOriginSpatialCoordinateSystem()
extern void Native_GetOriginSpatialCoordinateSystem_m1400DF2AAD12B9B75674A76AA60FFA82CDC27AF6 (void);
// 0x0000008B System.IntPtr UnityEngine.XR.WindowsMR.Native::GetCurrentHolographicRenderFrame()
extern void Native_GetCurrentHolographicRenderFrame_m0E6DE189ECA50AF884CFF40CF8FA181EADB781B7 (void);
// 0x0000008C UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.WindowsMR.Input.WMRHMD::get_userPresence()
extern void WMRHMD_get_userPresence_mE250009C6A5996E0F9225C26CAE0A7499B5073EC (void);
// 0x0000008D System.Void UnityEngine.XR.WindowsMR.Input.WMRHMD::set_userPresence(UnityEngine.InputSystem.Controls.ButtonControl)
extern void WMRHMD_set_userPresence_m97E7E5957B457D492C1C884822A95CAD9A71717D (void);
// 0x0000008E UnityEngine.InputSystem.Controls.IntegerControl UnityEngine.XR.WindowsMR.Input.WMRHMD::get_trackingState()
extern void WMRHMD_get_trackingState_mDDED981606A0277F2F5ACDC5F4DD56BF916C0BA4 (void);
// 0x0000008F System.Void UnityEngine.XR.WindowsMR.Input.WMRHMD::set_trackingState(UnityEngine.InputSystem.Controls.IntegerControl)
extern void WMRHMD_set_trackingState_m3A4A22848DC24EA9A144AB0A9DC4B8CB5A45A4DE (void);
// 0x00000090 UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.WindowsMR.Input.WMRHMD::get_isTracked()
extern void WMRHMD_get_isTracked_m13F9D8053C4B4B4EE8B5D9C6C71848E5D24777D6 (void);
// 0x00000091 System.Void UnityEngine.XR.WindowsMR.Input.WMRHMD::set_isTracked(UnityEngine.InputSystem.Controls.ButtonControl)
extern void WMRHMD_set_isTracked_m33E07F738A9DE0DE4F8BFE217D656B79A346CBC8 (void);
// 0x00000092 UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.WMRHMD::get_devicePosition()
extern void WMRHMD_get_devicePosition_mD38E71699EFD74E83AD6ADCBCF8BF875727832C4 (void);
// 0x00000093 System.Void UnityEngine.XR.WindowsMR.Input.WMRHMD::set_devicePosition(UnityEngine.InputSystem.Controls.Vector3Control)
extern void WMRHMD_set_devicePosition_mDEF13BDACC8B05A4BBD7A6EC74948A941B4573EB (void);
// 0x00000094 UnityEngine.InputSystem.Controls.QuaternionControl UnityEngine.XR.WindowsMR.Input.WMRHMD::get_deviceRotation()
extern void WMRHMD_get_deviceRotation_m916CE81BE0DD817CC2BC7D254270AA00383F2284 (void);
// 0x00000095 System.Void UnityEngine.XR.WindowsMR.Input.WMRHMD::set_deviceRotation(UnityEngine.InputSystem.Controls.QuaternionControl)
extern void WMRHMD_set_deviceRotation_mD192F52E7CB6DBE3D2DF97254C51A8C5638E62C6 (void);
// 0x00000096 UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.WMRHMD::get_leftEyePosition()
extern void WMRHMD_get_leftEyePosition_m7FF47BFD967D7D96B1269D0336801F23C47B2CCA (void);
// 0x00000097 System.Void UnityEngine.XR.WindowsMR.Input.WMRHMD::set_leftEyePosition(UnityEngine.InputSystem.Controls.Vector3Control)
extern void WMRHMD_set_leftEyePosition_m37D98146C9199AE945BD2872D6F143BB1A54C8D0 (void);
// 0x00000098 UnityEngine.InputSystem.Controls.QuaternionControl UnityEngine.XR.WindowsMR.Input.WMRHMD::get_leftEyeRotation()
extern void WMRHMD_get_leftEyeRotation_mE51CDFAF945796CB5A89367CA0768C20C4189794 (void);
// 0x00000099 System.Void UnityEngine.XR.WindowsMR.Input.WMRHMD::set_leftEyeRotation(UnityEngine.InputSystem.Controls.QuaternionControl)
extern void WMRHMD_set_leftEyeRotation_m41C47B25D4F053AC46DE0AFDA6BD5755F412EA76 (void);
// 0x0000009A UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.WMRHMD::get_rightEyePosition()
extern void WMRHMD_get_rightEyePosition_m9B9D00605940BB3C02770B558E6485B23AE366C1 (void);
// 0x0000009B System.Void UnityEngine.XR.WindowsMR.Input.WMRHMD::set_rightEyePosition(UnityEngine.InputSystem.Controls.Vector3Control)
extern void WMRHMD_set_rightEyePosition_mAE74B014C60A8176BFA8EE29B99F9FFEEA597F0D (void);
// 0x0000009C UnityEngine.InputSystem.Controls.QuaternionControl UnityEngine.XR.WindowsMR.Input.WMRHMD::get_rightEyeRotation()
extern void WMRHMD_get_rightEyeRotation_mC750974C92EFD95E2AA826C13ECB603E705685DF (void);
// 0x0000009D System.Void UnityEngine.XR.WindowsMR.Input.WMRHMD::set_rightEyeRotation(UnityEngine.InputSystem.Controls.QuaternionControl)
extern void WMRHMD_set_rightEyeRotation_m4819571D884C6E2E18C351A8FD2B4FA9581F4A5D (void);
// 0x0000009E UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.WMRHMD::get_centerEyePosition()
extern void WMRHMD_get_centerEyePosition_m6E29DE62172A14D5C15F5CF6ED852E7F9F4D4D44 (void);
// 0x0000009F System.Void UnityEngine.XR.WindowsMR.Input.WMRHMD::set_centerEyePosition(UnityEngine.InputSystem.Controls.Vector3Control)
extern void WMRHMD_set_centerEyePosition_m8D08BEACA0B9D8598D3DA8B7CA4B053508DE9BA0 (void);
// 0x000000A0 UnityEngine.InputSystem.Controls.QuaternionControl UnityEngine.XR.WindowsMR.Input.WMRHMD::get_centerEyeRotation()
extern void WMRHMD_get_centerEyeRotation_m99421D102985927CA47C74923A98F65AF4FDA8F0 (void);
// 0x000000A1 System.Void UnityEngine.XR.WindowsMR.Input.WMRHMD::set_centerEyeRotation(UnityEngine.InputSystem.Controls.QuaternionControl)
extern void WMRHMD_set_centerEyeRotation_mF95966E6DE98A7B8F0765E8807DC1A184D447ED1 (void);
// 0x000000A2 UnityEngine.InputSystem.Controls.AxisControl UnityEngine.XR.WindowsMR.Input.WMRHMD::get_positionAccuracy()
extern void WMRHMD_get_positionAccuracy_m56AB1381DDCED12DB594347595620C496E216D02 (void);
// 0x000000A3 System.Void UnityEngine.XR.WindowsMR.Input.WMRHMD::set_positionAccuracy(UnityEngine.InputSystem.Controls.AxisControl)
extern void WMRHMD_set_positionAccuracy_m4A3C528DC10CF9AD3A275ADD5E2D916678BE1ADB (void);
// 0x000000A4 System.Void UnityEngine.XR.WindowsMR.Input.WMRHMD::FinishSetup()
extern void WMRHMD_FinishSetup_m17F7620B18EA41DA7F8529D66C7B3879968A0A1C (void);
// 0x000000A5 System.Void UnityEngine.XR.WindowsMR.Input.WMRHMD::.ctor()
extern void WMRHMD__ctor_m80AF3356D8275092C4F5651A0274AEE55DD75570 (void);
// 0x000000A6 UnityEngine.InputSystem.Controls.IntegerControl UnityEngine.XR.WindowsMR.Input.HololensHand::get_trackingState()
extern void HololensHand_get_trackingState_m6F26DC1E40F483A4DED2DFA116987F5EF0DB3E20 (void);
// 0x000000A7 System.Void UnityEngine.XR.WindowsMR.Input.HololensHand::set_trackingState(UnityEngine.InputSystem.Controls.IntegerControl)
extern void HololensHand_set_trackingState_m5E068F6BBF94766F51FD007F4B1CEF56079AA5DD (void);
// 0x000000A8 UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.WindowsMR.Input.HololensHand::get_isTracked()
extern void HololensHand_get_isTracked_mB13AE7436293F2D7482C4C9512882005A3AFE4CA (void);
// 0x000000A9 System.Void UnityEngine.XR.WindowsMR.Input.HololensHand::set_isTracked(UnityEngine.InputSystem.Controls.ButtonControl)
extern void HololensHand_set_isTracked_m94D8839BC5AC3F5A6E5B197817E2D66E3517CCB5 (void);
// 0x000000AA UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.HololensHand::get_devicePosition()
extern void HololensHand_get_devicePosition_m3131DDD1303040415B41E13FC6A8C4CEC01FB13A (void);
// 0x000000AB System.Void UnityEngine.XR.WindowsMR.Input.HololensHand::set_devicePosition(UnityEngine.InputSystem.Controls.Vector3Control)
extern void HololensHand_set_devicePosition_mE06EC0F1AB5117F5AB9688E53CAB81812F2D0506 (void);
// 0x000000AC UnityEngine.InputSystem.Controls.QuaternionControl UnityEngine.XR.WindowsMR.Input.HololensHand::get_deviceRotation()
extern void HololensHand_get_deviceRotation_m06E2FFB6F08290C64F90CD94E7C9288A29337F84 (void);
// 0x000000AD System.Void UnityEngine.XR.WindowsMR.Input.HololensHand::set_deviceRotation(UnityEngine.InputSystem.Controls.QuaternionControl)
extern void HololensHand_set_deviceRotation_m46D92571A73B3874ECA19E37F0C003CC87F5AF26 (void);
// 0x000000AE UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.HololensHand::get_deviceVelocity()
extern void HololensHand_get_deviceVelocity_m3E9B0341DBA04DB239FAB929E675A640C72B1AF0 (void);
// 0x000000AF System.Void UnityEngine.XR.WindowsMR.Input.HololensHand::set_deviceVelocity(UnityEngine.InputSystem.Controls.Vector3Control)
extern void HololensHand_set_deviceVelocity_m75BD4887DC1FC24A1AF7F71177513573793BD417 (void);
// 0x000000B0 UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.WindowsMR.Input.HololensHand::get_airTap()
extern void HololensHand_get_airTap_m43C630B775CA6AA239CD2D7D8E6555CC780860B3 (void);
// 0x000000B1 System.Void UnityEngine.XR.WindowsMR.Input.HololensHand::set_airTap(UnityEngine.InputSystem.Controls.ButtonControl)
extern void HololensHand_set_airTap_mA6993834739A66FC5C3055A4EAD78E924989521B (void);
// 0x000000B2 UnityEngine.InputSystem.Controls.AxisControl UnityEngine.XR.WindowsMR.Input.HololensHand::get_sourceLossRisk()
extern void HololensHand_get_sourceLossRisk_m40189FE55885D2E2C6568768BBC1D2832B988BAB (void);
// 0x000000B3 System.Void UnityEngine.XR.WindowsMR.Input.HololensHand::set_sourceLossRisk(UnityEngine.InputSystem.Controls.AxisControl)
extern void HololensHand_set_sourceLossRisk_m3ABD3539583331B3205DC11ED06D23307CAC69E8 (void);
// 0x000000B4 UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.HololensHand::get_sourceLossMitigationDirection()
extern void HololensHand_get_sourceLossMitigationDirection_m2E51839B42B5792DB14478450169FA7C9DA82AC8 (void);
// 0x000000B5 System.Void UnityEngine.XR.WindowsMR.Input.HololensHand::set_sourceLossMitigationDirection(UnityEngine.InputSystem.Controls.Vector3Control)
extern void HololensHand_set_sourceLossMitigationDirection_m4B39773038A6CC56EE1FDD59AA5669C38B74D5F1 (void);
// 0x000000B6 UnityEngine.InputSystem.Controls.AxisControl UnityEngine.XR.WindowsMR.Input.HololensHand::get_positionAccuracy()
extern void HololensHand_get_positionAccuracy_m36502E588DD52DA8729639FFA06B3F5EE5E2536A (void);
// 0x000000B7 System.Void UnityEngine.XR.WindowsMR.Input.HololensHand::set_positionAccuracy(UnityEngine.InputSystem.Controls.AxisControl)
extern void HololensHand_set_positionAccuracy_mABFB0281D561DB0DAD722F3FD01AE5974A4C5EB7 (void);
// 0x000000B8 System.Void UnityEngine.XR.WindowsMR.Input.HololensHand::FinishSetup()
extern void HololensHand_FinishSetup_m8972EC9CBA3497720BC52314983E23B0D8AB69C7 (void);
// 0x000000B9 System.Void UnityEngine.XR.WindowsMR.Input.HololensHand::.ctor()
extern void HololensHand__ctor_m34D09C7823D96317C371127A37E9F8944CD1C3A9 (void);
// 0x000000BA UnityEngine.InputSystem.Controls.Vector2Control UnityEngine.XR.WindowsMR.Input.WMRSpatialController::get_joystick()
extern void WMRSpatialController_get_joystick_m01FFA6978EB4A3DE439AF7305F3559CCA9390E03 (void);
// 0x000000BB System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::set_joystick(UnityEngine.InputSystem.Controls.Vector2Control)
extern void WMRSpatialController_set_joystick_m180F2CA6E556F7F7453C70BBA718DDEB687C5DA5 (void);
// 0x000000BC UnityEngine.InputSystem.Controls.Vector2Control UnityEngine.XR.WindowsMR.Input.WMRSpatialController::get_touchpad()
extern void WMRSpatialController_get_touchpad_mA5EC0CCA14D691D04AC5805262DE404E294E4501 (void);
// 0x000000BD System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::set_touchpad(UnityEngine.InputSystem.Controls.Vector2Control)
extern void WMRSpatialController_set_touchpad_mB7C2C9EE9686408850CA0A0BE6F137E933554266 (void);
// 0x000000BE UnityEngine.InputSystem.Controls.AxisControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::get_grip()
extern void WMRSpatialController_get_grip_m3E728CF2615A9C561114E56368E0C2146EFD434D (void);
// 0x000000BF System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::set_grip(UnityEngine.InputSystem.Controls.AxisControl)
extern void WMRSpatialController_set_grip_mF2DFD491EB568530B5EB50A5037B2F9F0F1EA96B (void);
// 0x000000C0 UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::get_gripPressed()
extern void WMRSpatialController_get_gripPressed_m79E0D2F268F0FFFF9F07A84B1510A01459E1D38C (void);
// 0x000000C1 System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::set_gripPressed(UnityEngine.InputSystem.Controls.ButtonControl)
extern void WMRSpatialController_set_gripPressed_mF1F0F6B0C59A6C4EAFCE27383347BE0B27515033 (void);
// 0x000000C2 UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::get_menu()
extern void WMRSpatialController_get_menu_m51FAFF3A397061C92E6252A4B72D9175A28E444F (void);
// 0x000000C3 System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::set_menu(UnityEngine.InputSystem.Controls.ButtonControl)
extern void WMRSpatialController_set_menu_mF15F24A814268CE2D58014FCA189D821027F6A84 (void);
// 0x000000C4 UnityEngine.InputSystem.Controls.AxisControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::get_trigger()
extern void WMRSpatialController_get_trigger_mB94DD988F8A5B56AF86E1A63B23F999DA8168302 (void);
// 0x000000C5 System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::set_trigger(UnityEngine.InputSystem.Controls.AxisControl)
extern void WMRSpatialController_set_trigger_m5641774570232B6014153DF52B9216BA62C34403 (void);
// 0x000000C6 UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::get_triggerPressed()
extern void WMRSpatialController_get_triggerPressed_m925022C1AE8B824C4156711A69708C6A282747CD (void);
// 0x000000C7 System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::set_triggerPressed(UnityEngine.InputSystem.Controls.ButtonControl)
extern void WMRSpatialController_set_triggerPressed_m6A1302BD9CB84BAF80F7C5528D58D43DF915A6D8 (void);
// 0x000000C8 UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::get_joystickClicked()
extern void WMRSpatialController_get_joystickClicked_mD05C85E7AE3B3864313989A1782A2BFE2AA8825F (void);
// 0x000000C9 System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::set_joystickClicked(UnityEngine.InputSystem.Controls.ButtonControl)
extern void WMRSpatialController_set_joystickClicked_m5F79A8038280FEEC79B538D8631CE54E5D562BBD (void);
// 0x000000CA UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::get_touchpadClicked()
extern void WMRSpatialController_get_touchpadClicked_m8CA409CDC823610C51C6C54099D71DA493FE8B75 (void);
// 0x000000CB System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::set_touchpadClicked(UnityEngine.InputSystem.Controls.ButtonControl)
extern void WMRSpatialController_set_touchpadClicked_m109831C23706E7079C6CB0C2BFC9FE5994AFE3F1 (void);
// 0x000000CC UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::get_touchpadTouched()
extern void WMRSpatialController_get_touchpadTouched_mD084752D326F3EB3B3C78D38C0DBBAD306CA15A7 (void);
// 0x000000CD System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::set_touchpadTouched(UnityEngine.InputSystem.Controls.ButtonControl)
extern void WMRSpatialController_set_touchpadTouched_m5ED4A15FBD116FE2D20E27E57303F382E4351110 (void);
// 0x000000CE UnityEngine.InputSystem.Controls.IntegerControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::get_trackingState()
extern void WMRSpatialController_get_trackingState_m25DFE11A727D6E190BC7D7D8B62BA8BAD65F4524 (void);
// 0x000000CF System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::set_trackingState(UnityEngine.InputSystem.Controls.IntegerControl)
extern void WMRSpatialController_set_trackingState_m4205E90313F3987960E26E94A78DF862282422CB (void);
// 0x000000D0 UnityEngine.InputSystem.Controls.ButtonControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::get_isTracked()
extern void WMRSpatialController_get_isTracked_mDAC38BF7CB2CA3D234E460E43C2BC5805529B3E7 (void);
// 0x000000D1 System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::set_isTracked(UnityEngine.InputSystem.Controls.ButtonControl)
extern void WMRSpatialController_set_isTracked_m39E40EEED3294D2FE18B46A2B44B52AB58D4D15B (void);
// 0x000000D2 UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.WMRSpatialController::get_devicePosition()
extern void WMRSpatialController_get_devicePosition_mE132BFBE314C56B64E3FC528C1F9D3EAE840FB72 (void);
// 0x000000D3 System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::set_devicePosition(UnityEngine.InputSystem.Controls.Vector3Control)
extern void WMRSpatialController_set_devicePosition_m13B3E8D9ADE07B6E236442D2C054C1B746EDB5D3 (void);
// 0x000000D4 UnityEngine.InputSystem.Controls.QuaternionControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::get_deviceRotation()
extern void WMRSpatialController_get_deviceRotation_m9714FC360D2A3851322301EF1AA9AFEF99CB50D8 (void);
// 0x000000D5 System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::set_deviceRotation(UnityEngine.InputSystem.Controls.QuaternionControl)
extern void WMRSpatialController_set_deviceRotation_mC48929DC77737895EDC223644277496E3EDF3301 (void);
// 0x000000D6 UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.WMRSpatialController::get_deviceVelocity()
extern void WMRSpatialController_get_deviceVelocity_mCBC19404FD1DEE7793AD0D215C5D674703A3BA5A (void);
// 0x000000D7 System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::set_deviceVelocity(UnityEngine.InputSystem.Controls.Vector3Control)
extern void WMRSpatialController_set_deviceVelocity_mDD090097ED56AAF555290370FF7C5BC48C6951C2 (void);
// 0x000000D8 UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.WMRSpatialController::get_deviceAngularVelocity()
extern void WMRSpatialController_get_deviceAngularVelocity_m61710D147BF8A1BCC392606D4152AD4DCC28539D (void);
// 0x000000D9 System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::set_deviceAngularVelocity(UnityEngine.InputSystem.Controls.Vector3Control)
extern void WMRSpatialController_set_deviceAngularVelocity_m1D594A16EC4E3834575BCA4E886DDDCC2A3E0F4D (void);
// 0x000000DA UnityEngine.InputSystem.Controls.AxisControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::get_batteryLevel()
extern void WMRSpatialController_get_batteryLevel_mFDD6EEEFB9C4E5B5F8C407B1186C3A97850DBDAF (void);
// 0x000000DB System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::set_batteryLevel(UnityEngine.InputSystem.Controls.AxisControl)
extern void WMRSpatialController_set_batteryLevel_mDD5A2A6448ECA0DBB4EA4C494A63FC315CE668A7 (void);
// 0x000000DC UnityEngine.InputSystem.Controls.AxisControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::get_sourceLossRisk()
extern void WMRSpatialController_get_sourceLossRisk_mFE8050D76A7AE85AE6D0BAF3DCD1E490A5714038 (void);
// 0x000000DD System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::set_sourceLossRisk(UnityEngine.InputSystem.Controls.AxisControl)
extern void WMRSpatialController_set_sourceLossRisk_mB7F13A3EB8C33F92BCFE8E16A6DA955864010FDC (void);
// 0x000000DE UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.WMRSpatialController::get_sourceLossMitigationDirection()
extern void WMRSpatialController_get_sourceLossMitigationDirection_mA6715F87EE6434B816162B1C863E6631117BB325 (void);
// 0x000000DF System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::set_sourceLossMitigationDirection(UnityEngine.InputSystem.Controls.Vector3Control)
extern void WMRSpatialController_set_sourceLossMitigationDirection_mF3E6B3E687F733097E744F83244E5E12777993CC (void);
// 0x000000E0 UnityEngine.InputSystem.Controls.Vector3Control UnityEngine.XR.WindowsMR.Input.WMRSpatialController::get_pointerPosition()
extern void WMRSpatialController_get_pointerPosition_m88355902601A836CEC1E491B9116C77AB87C7252 (void);
// 0x000000E1 System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::set_pointerPosition(UnityEngine.InputSystem.Controls.Vector3Control)
extern void WMRSpatialController_set_pointerPosition_m170B8BC715547FB0BB891E1AD462C4638787985E (void);
// 0x000000E2 UnityEngine.InputSystem.Controls.QuaternionControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::get_pointerRotation()
extern void WMRSpatialController_get_pointerRotation_m7EDE9C6FE0811A89323FF9BDEAA56A531ED9B5B3 (void);
// 0x000000E3 System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::set_pointerRotation(UnityEngine.InputSystem.Controls.QuaternionControl)
extern void WMRSpatialController_set_pointerRotation_mD5CB9074B77D800F12DD99BF8EB77BD4B8EFA22D (void);
// 0x000000E4 UnityEngine.InputSystem.Controls.AxisControl UnityEngine.XR.WindowsMR.Input.WMRSpatialController::get_positionAccuracy()
extern void WMRSpatialController_get_positionAccuracy_m9ACA49CCEAD6A0C2F769E0025CEE32EC58E8C4C7 (void);
// 0x000000E5 System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::set_positionAccuracy(UnityEngine.InputSystem.Controls.AxisControl)
extern void WMRSpatialController_set_positionAccuracy_m9E8440AA709735D8204C1C570D23F52FE5AC2FDC (void);
// 0x000000E6 System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::FinishSetup()
extern void WMRSpatialController_FinishSetup_mF521FA5201A0D54A0C5E5A4957B0C71AC4C38B78 (void);
// 0x000000E7 System.Void UnityEngine.XR.WindowsMR.Input.WMRSpatialController::.ctor()
extern void WMRSpatialController__ctor_m44214281530DB29E911EE81B4EEAB775E9737AC7 (void);
static Il2CppMethodPointer s_methodPointers[231] = 
{
	WindowsMRUsages__cctor_m3D3E8790574F113E6431A9CFA70AC8EBD49189E0,
	WindowsMRInternal__cctor_mD5020D9B8904047143F4DD4D0FC3C138D3A6F278,
	WindowsMRInternal_UnityWindowsMR_EmulationLibs_SetPluginFolderPaths_m9306CEA22BD88F583ED16C14E01DE83CF7F7A0A3,
	WindowsMRInternal__ctor_m038CD74CFF61AA75CD38B1295C037D0265A309C4,
	NativeApi_UnityWindowsMR_refPoints_start_m9C3BF892D03C13CE76C6AA521EF40AC346F20F23,
	NativeApi_UnityWindowsMR_refPoints_stop_m4CB12EBFDCE5E7AD770720FBEB6C513FD0164227,
	NativeApi_UnityWindowsMR_refPoints_onDestroy_mC127F30E396474B701E0A6F6979650F1A08F675A,
	NativeApi_UnityWindowsMR_refPoints_acquireChanges_m25451D94AF29B52A4AB59FA79FEC7822D19AB54A,
	NativeApi_UnityWindowsMR_refPoints_releaseChanges_m5BA3A089F36DB2FD24A0E244A5E8F866C320A37A,
	NativeApi_UnityWindowsMR_refPoints_tryAdd_mB4C1C91407874F2D6249C60C4E1928C1F33DFFB4,
	NativeApi_UnityWindowsMR_refPoints_tryRemove_m70F0D502BEB00FC4BBDAA43290278EF4B43AE0D8,
	WindowsMRAnchorSubsystem_RegisterDescriptor_mAB0469AE262603106845FFF4960FABC2C0423E2F,
	WindowsMRAnchorSubsystem__ctor_m57630B41DD7A72E0B5989DD4C0C4B2A090C0C914,
	WindowsMRProvider_Start_m24FCDC8D6DE5ED818DCFFC935A99CB8C856D0DAC,
	WindowsMRProvider_Stop_m3D898745F14BE48639CA64C4885B8821234706D9,
	WindowsMRProvider_Destroy_m00461C208F6734C01EFB5B7EA37FD6F393B6806C,
	WindowsMRProvider_GetChanges_mFDCD5B8A8DE642AA77A49A693A689D30D9D92043,
	WindowsMRProvider_TryAddAnchor_m9CF29D14BC115E65D6BAB77AEF296914D06A6E9B,
	WindowsMRProvider_TryRemoveAnchor_mFA91393D3E9B94E9196B80D8C68BF6819B000385,
	WindowsMRProvider__ctor_mDA307C80111BC8B089C5B052FB8D1DD4DA9F24EC,
	FeatureApi_SetFeatureRequested_m68076BE5A3C67FDE1BE5012CEDDDE950472C4CB5,
	FeatureApi_get_RequestedFeatures_mFB5372D18A54D1589959D7CB5008AA65F1DC4673,
	WindowsMRGestureSubsystem_get_holdGestureEvents_m835C1731B08A0D7E64B701795E08668402007DDC,
	WindowsMRGestureSubsystem_get_manipulationGestureEvents_mDE2A52E9C69598DEE55022F803BA7ABC59C22B49,
	WindowsMRGestureSubsystem_get_navigationGestureEvents_mDF579B8A385FF11EEC092D843B66ABA238975894,
	WindowsMRGestureSubsystem_get_tappedGestureEvents_mFE3FCB3D4D018C45864ABABE64CE78D976E7554E,
	WindowsMRGestureSubsystem_CreateProvider_mCE1AAC022D7EDF4101B43AB479CA8665967E7E02,
	WindowsMRGestureSubsystem_SetEnableManipulationGesture_m2B953B6D34239BE33C9DAE745368CFB3804E8ACE,
	WindowsMRGestureSubsystem_SetEnableNavigationGesture_m98AF0437167F3412FD251777B4935CBDBB97A1A2,
	WindowsMRGestureSubsystem_RegisterDescriptor_m51BE6A1D750D1B24BADEA396AF9BADD515152DB4,
	WindowsMRGestureSubsystem_GetNextGUID_mCAFD7110250C3330F09696214E81FF8DE03CEADC,
	WindowsMRGestureSubsystem__ctor_m78B5BEE872561E8D35E1755385B098112528D2C3,
	WindowsMRGestureSubsystem__cctor_m1602650A1D7132522E4D5992619A45BBBAB0AA97,
	WindowsMRGestureProvider__ctor_mA04198031707F3B34AF35CA504A75CAC7689CE35,
	WindowsMRGestureProvider_Start_m71387CF62855FEEEBE2CB46B8BA66716D7BF4D8F,
	WindowsMRGestureProvider_Stop_mDA357182AB707168E52D59A2816764E295091927,
	WindowsMRGestureProvider_Update_mC450709588DAEAB57ADA028AC5B6D155FA9C3C86,
	NULL,
	WindowsMRGestureProvider_RetrieveAllGestureEvents_m54B05DFA5B516E1274EB32F16CE8BC79BCCAF2D5,
	WindowsMRGestureProvider_Destroy_mC91404D26F033BB2B7C7A14FD7E3A35E253FC5FE,
	WindowsMRGestureProvider_SetEnableManipulationGesture_m648642E50116C3D6C0196DD6DC930EEC5FC64BC4,
	WindowsMRGestureProvider_SetEnableNavigationGesture_mA9956AD7DB20A6E09BDE5A5BC465A609F2FE683C,
	WindowsMRGestureProvider_get_holdGestureEvents_mC4F61F0F923190C315A7C1FF4EC47FC9E848CE75,
	WindowsMRGestureProvider_get_manipulationGestureEvents_mF3C0B8F022717BA75E60B4D25D5AFE3BBD7499D1,
	WindowsMRGestureProvider_get_navigationGestureEvents_m4396B2839123D403BC77F706BD35C75CF2C48739,
	WindowsMRGestureProvider_get_tappedGestureEvents_m536D1C5781E7879B343D029B81F8F024153D3704,
	GetGestureEventsPtrFunction__ctor_m33C2C1337DA78792600E4A26B47DAD9AB10DE5DB,
	GetGestureEventsPtrFunction_Invoke_m9183A48EEB89837ED13990399767C664E943AA4E,
	GetGestureEventsPtrFunction_BeginInvoke_m1AAE64ECB80C91BD2A96E96A8BDE5FBC17D1742D,
	GetGestureEventsPtrFunction_EndInvoke_m27E0BAC2A76C7237349C47306B57DE3B350E5630,
	NativeApi_UnityWindowsMR_GesturesUpdate_m105715666D54A509B20F5D823FDA5D7843F86242,
	NativeApi_UnityWindowsMR_GesturesStart_m47E7B0053FE2AFD2CB4CC52D2A33793A5D662776,
	NativeApi_UnityWindowsMR_GesturesDestroy_m42B3DC67DE8E59C7088BE748703B72649359A490,
	NativeApi_UnityWindowsMR_GesturesStop_m3A52410586A3E5FC545FD72B56B4EF26049BF89D,
	NativeApi_UnityWindowsMR_GesturesGetHoldGestureEventsPtr_m007F2ED563B3C9F6A78B72B471D2D8922A41696E,
	NativeApi_UnityWindowsMR_GesturesGetManipulationGestureEventsPtr_mD2BA69B9525B0201DC0229FB0D4BF3C1F4D6D722,
	NativeApi_UnityWindowsMR_GesturesGetNavigationGestureEventsPtr_m08B57A2BBFE8CB3257D3FCB2969E1026A2BF3EA5,
	NativeApi_UnityWindowsMR_GesturesGetTappedGestureEventsPtr_m96F91B88ACE51F7C48D79EF81D5BD3C3C4689577,
	NativeApi_UnityWindowsMR_GesturesSetEnableManipulationGesture_mDADE197B2456CB9D80562972458208EB18805DF2,
	NativeApi_UnityWindowsMR_GesturesSetEnableNavigationGesture_m970E855C29FD51C237BCAB9C45DD1B29CA540905,
	WindowsMRGestures_get_gestureSubsystem_mCB77A743ECC56D5A40ECFBC2EDCAAB106C16FE44,
	WindowsMRGestures_set_gestureSubsystem_m2F5C44E348B4AB1E27D3EECEF2FCDE70BC23261D,
	WindowsMRGestures_add_onHoldChanged_m328A5433CB0D2E89E4DDB4A647F251700839873F,
	WindowsMRGestures_remove_onHoldChanged_m47A0A57066419050A94DE0885E1D3FB32D4A5FCA,
	WindowsMRGestures_add_onManipulationChanged_m9BAC67C47F466F99CF772330FCFF8F4A998A71D3,
	WindowsMRGestures_remove_onManipulationChanged_m38EB412593B1C979BBF541F4C3EAD5E29C78B0F4,
	WindowsMRGestures_add_onNavigationChanged_m23678825E6CA8718C536EF895C107D1B8CFEAF98,
	WindowsMRGestures_remove_onNavigationChanged_mA720579EA3F7260C18788D8400420C6FE3E441D9,
	WindowsMRGestures_add_onTappedChanged_mA5EFD4421826488C4579C0B27A2E09B5F355F965,
	WindowsMRGestures_remove_onTappedChanged_mFF97BB3820F3B2CB25F3459BCD92E80A9BFAB581,
	WindowsMRGestures_add_onActivate_mC6028A20DADE8043E02706671EC3CA004BCD7B3D,
	WindowsMRGestures_remove_onActivate_m9968E16BE11A2F6F500DDA24BC28D75E18E6894B,
	WindowsMRGestures_GetGestureSubsystemIfNeeded_m94F38F8C2A43A68E7A0A0D48D9E270128A7B9944,
	WindowsMRGestures_Start_m016B42743DACA6F7279E2085FE435194DCE38F24,
	WindowsMRGestures_OnValidate_m6DE1F642001DB8E048D3CAD1838A73D8C8C8DDF7,
	WindowsMRGestures_Update_mC8282BB806EC1FCD84197E222C281CECE5CC1932,
	WindowsMRGestures__ctor_m8D1EA29967E87CA55E90B31A58742F7FC527D8F7,
	WindowsMRHoldGestureEvent_get_id_m6D8F5DE158524197B3E9A921C2274C29D8ED6AF3,
	WindowsMRHoldGestureEvent_get_state_mEE42F56DDE2EC21257E7122B992E55BB062C2D4F,
	WindowsMRHoldGestureEvent_ToString_m87D4381289ADC2F1A251700BC84E5968B2E01DC6,
	WindowsMRHoldGestureEvent_Equals_m56B218188024BACAE6C47C5CDD088801D9313C6E,
	WindowsMRHoldGestureEvent_GetHashCode_m8F2502D512F5C48BB9DF4DB25ADDCF1408B27F7A,
	WindowsMRHoldGestureEvent_Equals_mEAD4A09537B715ECABF41E2F6AA5C7DCDCC64F33,
	WindowsMRManipulationGestureEvent_get_id_mA4D42C442AE74BDCB6C2A3AE10096458C86B0F00,
	WindowsMRManipulationGestureEvent_get_state_mAE0B1DD206F527881FD05F5C0A02A13D0BB3D8C7,
	WindowsMRManipulationGestureEvent_get_cumulativeDelta_m8159C668C6A2E207801668C9961ADCCEE03B383F,
	WindowsMRManipulationGestureEvent_ToString_mB763E2D65DD38FC5F7AFA42824D023FF5943179D,
	WindowsMRManipulationGestureEvent_Equals_mE627F94BCC8D89061D669A4E151B55DD85A862BE,
	WindowsMRManipulationGestureEvent_GetHashCode_mFEECAF5AB7081A6C3B49FCEAAA48BBA6455D8EA4,
	WindowsMRManipulationGestureEvent_Equals_m6D3678EC3350E411E4CE20F74A060431A4B3BC4D,
	WindowsMRNavigationGestureEvent_get_id_m6AA1C10874F845A4832FCB7352A8E4A200737A4F,
	WindowsMRNavigationGestureEvent_get_state_m85B727B4B39939571352B5FBC1BEBBDF2723895F,
	WindowsMRNavigationGestureEvent_get_normalizedOffset_m10CD89AAAA3020E56E087B2D349790C82005A343,
	WindowsMRNavigationGestureEvent_ToString_m62E7AC359CD0C94B843B0348297C742CE9DB596E,
	WindowsMRNavigationGestureEvent_Equals_m2BC1623B58B628CF58FBB637A43E22812F397269,
	WindowsMRNavigationGestureEvent_GetHashCode_m7319B536FE9E03BE170744D0A5025A3C666F98FD,
	WindowsMRNavigationGestureEvent_Equals_mF04C6DDC5E5A614DB2DD168E32645870E13C99BF,
	WindowsMRTappedGestureEvent_get_id_mCF2F486894D4DDF592B5CFFE918F3F1E764911F9,
	WindowsMRTappedGestureEvent_get_state_mF6039113078438AC618BDD0451A05A1F72566A52,
	WindowsMRTappedGestureEvent_get_tappedCount_m1C3775FCF65723A87630119978CCA1AB81769136,
	WindowsMRTappedGestureEvent_ToString_m26DC834236CF0F10B818A2565D06207A18C9D9DE,
	WindowsMRTappedGestureEvent_Equals_mD86527B1974A47F50042CC65CE6E0CE93655680B,
	WindowsMRTappedGestureEvent_GetHashCode_mB512A915C951F132527EE4B0E6DE7EE073841C48,
	WindowsMRTappedGestureEvent_Equals_mABD39CE8DD91E3D967199BF869FA47F3B70CE439,
	WindowsMRSessionSubsystem_RegisterDescriptor_m73439E623C9E00BF613B760FE61E4BE1883BAB54,
	WindowsMRSessionSubsystem__ctor_mD15A2CD789E730766104A1A6E765EA7C60F8CFED,
	WindowsMRProvider__ctor_m4A3FF4D4655B462A0071F46643E7BA4092A2E9D2,
	WindowsMRProvider_GetAvailabilityAsync_m53FA642CFCA72479E1C201A5A914CF6024BB2561,
	WindowsMRProvider_get_trackingState_mB83B60D84CDD264A1DCF84FE4C1D1D7F450905CB,
	WindowsMRProvider_get_notTrackingReason_m2B5E3A4B952885ED10C738020E392E53284703B1,
	WindowsMRProvider_Destroy_m3483C848E702BDCB773CDA06EB3A80D2347938EB,
	WindowsMRProvider_Stop_m5C2A414212AABD3B3757124E6DEF12A6070A0C73,
	WindowsMRProvider_Start_m1ADCFD1C8E426AC99AB4FF97BBD1B915B31E56E5,
	WindowsMRProvider_Reset_m0EE77F20778AEA40B270F34A5D5D10AA70667895,
	WindowsMRProvider_get_requestedFeatures_mCD8BEFE1EAB16E348DC2F9BE4103E2EBA54D4355,
	WindowsMRProvider_get_requestedTrackingMode_m4026DE438DCD67C1A3573A52AC310DD333AF5A64,
	WindowsMRProvider_set_requestedTrackingMode_mDF1576DD78C9D8F332BA26FC76B6A0A22E6EE5F5,
	WindowsMRProvider_get_currentTrackingMode_m479CB3BF04B2658E7E2219D5B6A3651228E62E38,
	WindowsMRProvider_GetConfigurationDescriptors_mBD1E8AB093A51E6685E8916AC39747838DC11077,
	NativeApi_UnityWindowsMR_session_construct_m0772E1E49E93894E66C7E67DE52498F411934A1D,
	NativeApi_UnityWindowsMR_session_destroy_mED894C23E7A76288878480A76E269DF873713445,
	NativeApi_UnityWindowsMR_session_pause_mB099D3E3A8CBCAFE10980620C540F819A384FCEA,
	NativeApi_UnityWindowsMR_session_resume_mAD3AE6795D01A9865280A9C025BF946E552420C9,
	NativeApi_UnityWindowsMR_session_reset_m14CA2F89C86AA96F023A47480A8EFCF6D9090168,
	NativeApi_UnityWindowsMR_session_getTrackingState_m7792190745A241CAF1F38C3730BD705F43ED5619,
	NativeApi_UnityWindowsMR_session_getNotTrackingReason_mC8D47C664D9C751729F6F7B052C67E0DCB842E37,
	WindowsMREnvironment_get_OriginSpatialCoordinateSystem_m3609FBD7226520512F8F2A8A5D0FFC1E2A0B015E,
	WindowsMREnvironment_get_CurrentHolographicRenderFrame_mCCAE9BE6650F820FCD43EAE2403DA10884732FCC,
	WindowsMRInput_GetCurrentSourceStates_m39D0AA035DFA6D7CAC042B6293C7DC10970DDF36,
	NativeApi_GetCountOfSourceStates_m90A532EFC11524CF3C52F15489944BE5A6414460,
	NativeApi_GetAllSourceStates_mDB6021165C7146885EFB3B13CE0306ABED0B936E,
	NativeApi_ReleaseAllSourceStates_mDF2407698A295271486AD53BD7FD993EE13674CD,
	WindowsMRExtensions_SetBoundingVolumeOrientedBox_mD1E8C44B923142D5123D13041D50504647963C02,
	WindowsMRExtensions_SetBoundingVolumeSphere_mBAE41AC8530E7A8C68311A65BD7FAA1E5EDAD96D,
	WindowsMRExtensions__cctor_m3C1EA92ED95503D4E6273D0A429F5F842BA7B74A,
	NativeApi_SetBoundingVolumeOrientedBox_mAB3006409DF0359C342B295CF2F2F6EF88F51315,
	NativeApi_SetBoundingVolumeSphere_m0413E84F7FD625C7E737F2FCEDE7D5A426A7F628,
	Native_GetOriginSpatialCoordinateSystem_m1400DF2AAD12B9B75674A76AA60FFA82CDC27AF6,
	Native_GetCurrentHolographicRenderFrame_m0E6DE189ECA50AF884CFF40CF8FA181EADB781B7,
	WMRHMD_get_userPresence_mE250009C6A5996E0F9225C26CAE0A7499B5073EC,
	WMRHMD_set_userPresence_m97E7E5957B457D492C1C884822A95CAD9A71717D,
	WMRHMD_get_trackingState_mDDED981606A0277F2F5ACDC5F4DD56BF916C0BA4,
	WMRHMD_set_trackingState_m3A4A22848DC24EA9A144AB0A9DC4B8CB5A45A4DE,
	WMRHMD_get_isTracked_m13F9D8053C4B4B4EE8B5D9C6C71848E5D24777D6,
	WMRHMD_set_isTracked_m33E07F738A9DE0DE4F8BFE217D656B79A346CBC8,
	WMRHMD_get_devicePosition_mD38E71699EFD74E83AD6ADCBCF8BF875727832C4,
	WMRHMD_set_devicePosition_mDEF13BDACC8B05A4BBD7A6EC74948A941B4573EB,
	WMRHMD_get_deviceRotation_m916CE81BE0DD817CC2BC7D254270AA00383F2284,
	WMRHMD_set_deviceRotation_mD192F52E7CB6DBE3D2DF97254C51A8C5638E62C6,
	WMRHMD_get_leftEyePosition_m7FF47BFD967D7D96B1269D0336801F23C47B2CCA,
	WMRHMD_set_leftEyePosition_m37D98146C9199AE945BD2872D6F143BB1A54C8D0,
	WMRHMD_get_leftEyeRotation_mE51CDFAF945796CB5A89367CA0768C20C4189794,
	WMRHMD_set_leftEyeRotation_m41C47B25D4F053AC46DE0AFDA6BD5755F412EA76,
	WMRHMD_get_rightEyePosition_m9B9D00605940BB3C02770B558E6485B23AE366C1,
	WMRHMD_set_rightEyePosition_mAE74B014C60A8176BFA8EE29B99F9FFEEA597F0D,
	WMRHMD_get_rightEyeRotation_mC750974C92EFD95E2AA826C13ECB603E705685DF,
	WMRHMD_set_rightEyeRotation_m4819571D884C6E2E18C351A8FD2B4FA9581F4A5D,
	WMRHMD_get_centerEyePosition_m6E29DE62172A14D5C15F5CF6ED852E7F9F4D4D44,
	WMRHMD_set_centerEyePosition_m8D08BEACA0B9D8598D3DA8B7CA4B053508DE9BA0,
	WMRHMD_get_centerEyeRotation_m99421D102985927CA47C74923A98F65AF4FDA8F0,
	WMRHMD_set_centerEyeRotation_mF95966E6DE98A7B8F0765E8807DC1A184D447ED1,
	WMRHMD_get_positionAccuracy_m56AB1381DDCED12DB594347595620C496E216D02,
	WMRHMD_set_positionAccuracy_m4A3C528DC10CF9AD3A275ADD5E2D916678BE1ADB,
	WMRHMD_FinishSetup_m17F7620B18EA41DA7F8529D66C7B3879968A0A1C,
	WMRHMD__ctor_m80AF3356D8275092C4F5651A0274AEE55DD75570,
	HololensHand_get_trackingState_m6F26DC1E40F483A4DED2DFA116987F5EF0DB3E20,
	HololensHand_set_trackingState_m5E068F6BBF94766F51FD007F4B1CEF56079AA5DD,
	HololensHand_get_isTracked_mB13AE7436293F2D7482C4C9512882005A3AFE4CA,
	HololensHand_set_isTracked_m94D8839BC5AC3F5A6E5B197817E2D66E3517CCB5,
	HololensHand_get_devicePosition_m3131DDD1303040415B41E13FC6A8C4CEC01FB13A,
	HololensHand_set_devicePosition_mE06EC0F1AB5117F5AB9688E53CAB81812F2D0506,
	HololensHand_get_deviceRotation_m06E2FFB6F08290C64F90CD94E7C9288A29337F84,
	HololensHand_set_deviceRotation_m46D92571A73B3874ECA19E37F0C003CC87F5AF26,
	HololensHand_get_deviceVelocity_m3E9B0341DBA04DB239FAB929E675A640C72B1AF0,
	HololensHand_set_deviceVelocity_m75BD4887DC1FC24A1AF7F71177513573793BD417,
	HololensHand_get_airTap_m43C630B775CA6AA239CD2D7D8E6555CC780860B3,
	HololensHand_set_airTap_mA6993834739A66FC5C3055A4EAD78E924989521B,
	HololensHand_get_sourceLossRisk_m40189FE55885D2E2C6568768BBC1D2832B988BAB,
	HololensHand_set_sourceLossRisk_m3ABD3539583331B3205DC11ED06D23307CAC69E8,
	HololensHand_get_sourceLossMitigationDirection_m2E51839B42B5792DB14478450169FA7C9DA82AC8,
	HololensHand_set_sourceLossMitigationDirection_m4B39773038A6CC56EE1FDD59AA5669C38B74D5F1,
	HololensHand_get_positionAccuracy_m36502E588DD52DA8729639FFA06B3F5EE5E2536A,
	HololensHand_set_positionAccuracy_mABFB0281D561DB0DAD722F3FD01AE5974A4C5EB7,
	HololensHand_FinishSetup_m8972EC9CBA3497720BC52314983E23B0D8AB69C7,
	HololensHand__ctor_m34D09C7823D96317C371127A37E9F8944CD1C3A9,
	WMRSpatialController_get_joystick_m01FFA6978EB4A3DE439AF7305F3559CCA9390E03,
	WMRSpatialController_set_joystick_m180F2CA6E556F7F7453C70BBA718DDEB687C5DA5,
	WMRSpatialController_get_touchpad_mA5EC0CCA14D691D04AC5805262DE404E294E4501,
	WMRSpatialController_set_touchpad_mB7C2C9EE9686408850CA0A0BE6F137E933554266,
	WMRSpatialController_get_grip_m3E728CF2615A9C561114E56368E0C2146EFD434D,
	WMRSpatialController_set_grip_mF2DFD491EB568530B5EB50A5037B2F9F0F1EA96B,
	WMRSpatialController_get_gripPressed_m79E0D2F268F0FFFF9F07A84B1510A01459E1D38C,
	WMRSpatialController_set_gripPressed_mF1F0F6B0C59A6C4EAFCE27383347BE0B27515033,
	WMRSpatialController_get_menu_m51FAFF3A397061C92E6252A4B72D9175A28E444F,
	WMRSpatialController_set_menu_mF15F24A814268CE2D58014FCA189D821027F6A84,
	WMRSpatialController_get_trigger_mB94DD988F8A5B56AF86E1A63B23F999DA8168302,
	WMRSpatialController_set_trigger_m5641774570232B6014153DF52B9216BA62C34403,
	WMRSpatialController_get_triggerPressed_m925022C1AE8B824C4156711A69708C6A282747CD,
	WMRSpatialController_set_triggerPressed_m6A1302BD9CB84BAF80F7C5528D58D43DF915A6D8,
	WMRSpatialController_get_joystickClicked_mD05C85E7AE3B3864313989A1782A2BFE2AA8825F,
	WMRSpatialController_set_joystickClicked_m5F79A8038280FEEC79B538D8631CE54E5D562BBD,
	WMRSpatialController_get_touchpadClicked_m8CA409CDC823610C51C6C54099D71DA493FE8B75,
	WMRSpatialController_set_touchpadClicked_m109831C23706E7079C6CB0C2BFC9FE5994AFE3F1,
	WMRSpatialController_get_touchpadTouched_mD084752D326F3EB3B3C78D38C0DBBAD306CA15A7,
	WMRSpatialController_set_touchpadTouched_m5ED4A15FBD116FE2D20E27E57303F382E4351110,
	WMRSpatialController_get_trackingState_m25DFE11A727D6E190BC7D7D8B62BA8BAD65F4524,
	WMRSpatialController_set_trackingState_m4205E90313F3987960E26E94A78DF862282422CB,
	WMRSpatialController_get_isTracked_mDAC38BF7CB2CA3D234E460E43C2BC5805529B3E7,
	WMRSpatialController_set_isTracked_m39E40EEED3294D2FE18B46A2B44B52AB58D4D15B,
	WMRSpatialController_get_devicePosition_mE132BFBE314C56B64E3FC528C1F9D3EAE840FB72,
	WMRSpatialController_set_devicePosition_m13B3E8D9ADE07B6E236442D2C054C1B746EDB5D3,
	WMRSpatialController_get_deviceRotation_m9714FC360D2A3851322301EF1AA9AFEF99CB50D8,
	WMRSpatialController_set_deviceRotation_mC48929DC77737895EDC223644277496E3EDF3301,
	WMRSpatialController_get_deviceVelocity_mCBC19404FD1DEE7793AD0D215C5D674703A3BA5A,
	WMRSpatialController_set_deviceVelocity_mDD090097ED56AAF555290370FF7C5BC48C6951C2,
	WMRSpatialController_get_deviceAngularVelocity_m61710D147BF8A1BCC392606D4152AD4DCC28539D,
	WMRSpatialController_set_deviceAngularVelocity_m1D594A16EC4E3834575BCA4E886DDDCC2A3E0F4D,
	WMRSpatialController_get_batteryLevel_mFDD6EEEFB9C4E5B5F8C407B1186C3A97850DBDAF,
	WMRSpatialController_set_batteryLevel_mDD5A2A6448ECA0DBB4EA4C494A63FC315CE668A7,
	WMRSpatialController_get_sourceLossRisk_mFE8050D76A7AE85AE6D0BAF3DCD1E490A5714038,
	WMRSpatialController_set_sourceLossRisk_mB7F13A3EB8C33F92BCFE8E16A6DA955864010FDC,
	WMRSpatialController_get_sourceLossMitigationDirection_mA6715F87EE6434B816162B1C863E6631117BB325,
	WMRSpatialController_set_sourceLossMitigationDirection_mF3E6B3E687F733097E744F83244E5E12777993CC,
	WMRSpatialController_get_pointerPosition_m88355902601A836CEC1E491B9116C77AB87C7252,
	WMRSpatialController_set_pointerPosition_m170B8BC715547FB0BB891E1AD462C4638787985E,
	WMRSpatialController_get_pointerRotation_m7EDE9C6FE0811A89323FF9BDEAA56A531ED9B5B3,
	WMRSpatialController_set_pointerRotation_mD5CB9074B77D800F12DD99BF8EB77BD4B8EFA22D,
	WMRSpatialController_get_positionAccuracy_m9ACA49CCEAD6A0C2F769E0025CEE32EC58E8C4C7,
	WMRSpatialController_set_positionAccuracy_m9E8440AA709735D8204C1C570D23F52FE5AC2FDC,
	WMRSpatialController_FinishSetup_mF521FA5201A0D54A0C5E5A4957B0C71AC4C38B78,
	WMRSpatialController__ctor_m44214281530DB29E911EE81B4EEAB775E9737AC7,
};
extern void WindowsMRHoldGestureEvent_get_id_m6D8F5DE158524197B3E9A921C2274C29D8ED6AF3_AdjustorThunk (void);
extern void WindowsMRHoldGestureEvent_get_state_mEE42F56DDE2EC21257E7122B992E55BB062C2D4F_AdjustorThunk (void);
extern void WindowsMRHoldGestureEvent_ToString_m87D4381289ADC2F1A251700BC84E5968B2E01DC6_AdjustorThunk (void);
extern void WindowsMRHoldGestureEvent_Equals_m56B218188024BACAE6C47C5CDD088801D9313C6E_AdjustorThunk (void);
extern void WindowsMRHoldGestureEvent_GetHashCode_m8F2502D512F5C48BB9DF4DB25ADDCF1408B27F7A_AdjustorThunk (void);
extern void WindowsMRHoldGestureEvent_Equals_mEAD4A09537B715ECABF41E2F6AA5C7DCDCC64F33_AdjustorThunk (void);
extern void WindowsMRManipulationGestureEvent_get_id_mA4D42C442AE74BDCB6C2A3AE10096458C86B0F00_AdjustorThunk (void);
extern void WindowsMRManipulationGestureEvent_get_state_mAE0B1DD206F527881FD05F5C0A02A13D0BB3D8C7_AdjustorThunk (void);
extern void WindowsMRManipulationGestureEvent_get_cumulativeDelta_m8159C668C6A2E207801668C9961ADCCEE03B383F_AdjustorThunk (void);
extern void WindowsMRManipulationGestureEvent_ToString_mB763E2D65DD38FC5F7AFA42824D023FF5943179D_AdjustorThunk (void);
extern void WindowsMRManipulationGestureEvent_Equals_mE627F94BCC8D89061D669A4E151B55DD85A862BE_AdjustorThunk (void);
extern void WindowsMRManipulationGestureEvent_GetHashCode_mFEECAF5AB7081A6C3B49FCEAAA48BBA6455D8EA4_AdjustorThunk (void);
extern void WindowsMRManipulationGestureEvent_Equals_m6D3678EC3350E411E4CE20F74A060431A4B3BC4D_AdjustorThunk (void);
extern void WindowsMRNavigationGestureEvent_get_id_m6AA1C10874F845A4832FCB7352A8E4A200737A4F_AdjustorThunk (void);
extern void WindowsMRNavigationGestureEvent_get_state_m85B727B4B39939571352B5FBC1BEBBDF2723895F_AdjustorThunk (void);
extern void WindowsMRNavigationGestureEvent_get_normalizedOffset_m10CD89AAAA3020E56E087B2D349790C82005A343_AdjustorThunk (void);
extern void WindowsMRNavigationGestureEvent_ToString_m62E7AC359CD0C94B843B0348297C742CE9DB596E_AdjustorThunk (void);
extern void WindowsMRNavigationGestureEvent_Equals_m2BC1623B58B628CF58FBB637A43E22812F397269_AdjustorThunk (void);
extern void WindowsMRNavigationGestureEvent_GetHashCode_m7319B536FE9E03BE170744D0A5025A3C666F98FD_AdjustorThunk (void);
extern void WindowsMRNavigationGestureEvent_Equals_mF04C6DDC5E5A614DB2DD168E32645870E13C99BF_AdjustorThunk (void);
extern void WindowsMRTappedGestureEvent_get_id_mCF2F486894D4DDF592B5CFFE918F3F1E764911F9_AdjustorThunk (void);
extern void WindowsMRTappedGestureEvent_get_state_mF6039113078438AC618BDD0451A05A1F72566A52_AdjustorThunk (void);
extern void WindowsMRTappedGestureEvent_get_tappedCount_m1C3775FCF65723A87630119978CCA1AB81769136_AdjustorThunk (void);
extern void WindowsMRTappedGestureEvent_ToString_m26DC834236CF0F10B818A2565D06207A18C9D9DE_AdjustorThunk (void);
extern void WindowsMRTappedGestureEvent_Equals_mD86527B1974A47F50042CC65CE6E0CE93655680B_AdjustorThunk (void);
extern void WindowsMRTappedGestureEvent_GetHashCode_mB512A915C951F132527EE4B0E6DE7EE073841C48_AdjustorThunk (void);
extern void WindowsMRTappedGestureEvent_Equals_mABD39CE8DD91E3D967199BF869FA47F3B70CE439_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[27] = 
{
	{ 0x0600004E, WindowsMRHoldGestureEvent_get_id_m6D8F5DE158524197B3E9A921C2274C29D8ED6AF3_AdjustorThunk },
	{ 0x0600004F, WindowsMRHoldGestureEvent_get_state_mEE42F56DDE2EC21257E7122B992E55BB062C2D4F_AdjustorThunk },
	{ 0x06000050, WindowsMRHoldGestureEvent_ToString_m87D4381289ADC2F1A251700BC84E5968B2E01DC6_AdjustorThunk },
	{ 0x06000051, WindowsMRHoldGestureEvent_Equals_m56B218188024BACAE6C47C5CDD088801D9313C6E_AdjustorThunk },
	{ 0x06000052, WindowsMRHoldGestureEvent_GetHashCode_m8F2502D512F5C48BB9DF4DB25ADDCF1408B27F7A_AdjustorThunk },
	{ 0x06000053, WindowsMRHoldGestureEvent_Equals_mEAD4A09537B715ECABF41E2F6AA5C7DCDCC64F33_AdjustorThunk },
	{ 0x06000054, WindowsMRManipulationGestureEvent_get_id_mA4D42C442AE74BDCB6C2A3AE10096458C86B0F00_AdjustorThunk },
	{ 0x06000055, WindowsMRManipulationGestureEvent_get_state_mAE0B1DD206F527881FD05F5C0A02A13D0BB3D8C7_AdjustorThunk },
	{ 0x06000056, WindowsMRManipulationGestureEvent_get_cumulativeDelta_m8159C668C6A2E207801668C9961ADCCEE03B383F_AdjustorThunk },
	{ 0x06000057, WindowsMRManipulationGestureEvent_ToString_mB763E2D65DD38FC5F7AFA42824D023FF5943179D_AdjustorThunk },
	{ 0x06000058, WindowsMRManipulationGestureEvent_Equals_mE627F94BCC8D89061D669A4E151B55DD85A862BE_AdjustorThunk },
	{ 0x06000059, WindowsMRManipulationGestureEvent_GetHashCode_mFEECAF5AB7081A6C3B49FCEAAA48BBA6455D8EA4_AdjustorThunk },
	{ 0x0600005A, WindowsMRManipulationGestureEvent_Equals_m6D3678EC3350E411E4CE20F74A060431A4B3BC4D_AdjustorThunk },
	{ 0x0600005B, WindowsMRNavigationGestureEvent_get_id_m6AA1C10874F845A4832FCB7352A8E4A200737A4F_AdjustorThunk },
	{ 0x0600005C, WindowsMRNavigationGestureEvent_get_state_m85B727B4B39939571352B5FBC1BEBBDF2723895F_AdjustorThunk },
	{ 0x0600005D, WindowsMRNavigationGestureEvent_get_normalizedOffset_m10CD89AAAA3020E56E087B2D349790C82005A343_AdjustorThunk },
	{ 0x0600005E, WindowsMRNavigationGestureEvent_ToString_m62E7AC359CD0C94B843B0348297C742CE9DB596E_AdjustorThunk },
	{ 0x0600005F, WindowsMRNavigationGestureEvent_Equals_m2BC1623B58B628CF58FBB637A43E22812F397269_AdjustorThunk },
	{ 0x06000060, WindowsMRNavigationGestureEvent_GetHashCode_m7319B536FE9E03BE170744D0A5025A3C666F98FD_AdjustorThunk },
	{ 0x06000061, WindowsMRNavigationGestureEvent_Equals_mF04C6DDC5E5A614DB2DD168E32645870E13C99BF_AdjustorThunk },
	{ 0x06000062, WindowsMRTappedGestureEvent_get_id_mCF2F486894D4DDF592B5CFFE918F3F1E764911F9_AdjustorThunk },
	{ 0x06000063, WindowsMRTappedGestureEvent_get_state_mF6039113078438AC618BDD0451A05A1F72566A52_AdjustorThunk },
	{ 0x06000064, WindowsMRTappedGestureEvent_get_tappedCount_m1C3775FCF65723A87630119978CCA1AB81769136_AdjustorThunk },
	{ 0x06000065, WindowsMRTappedGestureEvent_ToString_m26DC834236CF0F10B818A2565D06207A18C9D9DE_AdjustorThunk },
	{ 0x06000066, WindowsMRTappedGestureEvent_Equals_mD86527B1974A47F50042CC65CE6E0CE93655680B_AdjustorThunk },
	{ 0x06000067, WindowsMRTappedGestureEvent_GetHashCode_mB512A915C951F132527EE4B0E6DE7EE073841C48_AdjustorThunk },
	{ 0x06000068, WindowsMRTappedGestureEvent_Equals_mABD39CE8DD91E3D967199BF869FA47F3B70CE439_AdjustorThunk },
};
static const int32_t s_InvokerIndices[231] = 
{
	7059,
	7059,
	6921,
	4797,
	7059,
	7059,
	7059,
	4952,
	6910,
	6323,
	6859,
	7059,
	4797,
	4797,
	4797,
	4797,
	1288,
	1721,
	3415,
	4797,
	6457,
	7015,
	4531,
	4532,
	4533,
	4534,
	4712,
	3385,
	3385,
	7059,
	7005,
	4797,
	7059,
	3886,
	4797,
	4797,
	4797,
	-1,
	4797,
	4797,
	3385,
	3385,
	4531,
	4532,
	4533,
	4534,
	2219,
	1290,
	574,
	873,
	7059,
	7059,
	7059,
	7059,
	6053,
	6053,
	6053,
	6053,
	6855,
	6855,
	4712,
	3886,
	3886,
	3886,
	3886,
	3886,
	3886,
	3886,
	3886,
	3886,
	3886,
	3886,
	4712,
	4797,
	4797,
	4797,
	4797,
	4638,
	4676,
	4712,
	3352,
	4676,
	3434,
	4638,
	4676,
	4791,
	4712,
	3352,
	4676,
	3435,
	4638,
	4676,
	4791,
	4712,
	3352,
	4676,
	3436,
	4638,
	4676,
	4676,
	4712,
	3352,
	4676,
	3437,
	7059,
	4797,
	4797,
	4712,
	4676,
	4676,
	4797,
	4797,
	4797,
	4797,
	4677,
	4677,
	3851,
	4677,
	2406,
	7059,
	7059,
	7059,
	7059,
	7059,
	7014,
	7014,
	7016,
	7016,
	6500,
	7014,
	6495,
	6495,
	5664,
	6033,
	7059,
	6933,
	6934,
	7016,
	7016,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4797,
	4797,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4797,
	4797,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4712,
	3886,
	4797,
	4797,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x06000026, { 0, 7 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[7] = 
{
	{ (Il2CppRGCTXDataType)3, 33014 },
	{ (Il2CppRGCTXDataType)3, 33013 },
	{ (Il2CppRGCTXDataType)3, 33012 },
	{ (Il2CppRGCTXDataType)2, 8060 },
	{ (Il2CppRGCTXDataType)3, 33011 },
	{ (Il2CppRGCTXDataType)3, 52288 },
	{ (Il2CppRGCTXDataType)3, 51629 },
};
extern const CustomAttributesCacheGenerator g_Unity_XR_WindowsMixedReality_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Unity_XR_WindowsMixedReality_CodeGenModule;
const Il2CppCodeGenModule g_Unity_XR_WindowsMixedReality_CodeGenModule = 
{
	"Unity.XR.WindowsMixedReality.dll",
	231,
	s_methodPointers,
	27,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	7,
	s_rgctxValues,
	NULL,
	g_Unity_XR_WindowsMixedReality_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
