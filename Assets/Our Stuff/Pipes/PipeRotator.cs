using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeRotator : MonoBehaviour
{
    bool canRotate = true;
    public bool rightRotation = false;
    public int myRotation; //put in postion (0: rotX=0 -> 3: rotX=270)
    int rotationNumber;
    

    private void Update()
    {
        Debug.Log("pipe: "+rightRotation + "rotNum: " + rotationNumber);
    }
    public void RotatePipe()
    {
        if (canRotate)
        {
            gameObject.transform.Rotate(90, 0, 0);                          //Rotation in 90 Degrees
  

            if (rotationNumber < 3)
                rotationNumber += 1;
            else rotationNumber = 0;

            if (myRotation == rotationNumber) rightRotation = true;         //Check if the pipe is in the right Position
            else rightRotation = false;

            canRotate = false;
            Invoke("Flag", 1f);                                             //Hinders spamming interaction
        }
    }

    void Flag()
    {
        canRotate = true;
    }


}
