using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonCodeChecker : MonoBehaviour
{
    public ButtonNumberChange[] button;
    public GameObject drawer;

    public Transform endPos;
    public int numberOne;
    public int numberTwo;
    public int numberThree;
    private void Update()
    {
        if(button[0].currentScore == numberOne && button[1].currentScore == numberTwo && button[2].currentScore == numberThree)
        {
            Debug.Log("jup");

            drawer.transform.position = Vector3.Lerp(drawer.transform.position, endPos.position, 0.6f * Time.deltaTime);
        }
    }

}
