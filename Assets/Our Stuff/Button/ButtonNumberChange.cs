using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class ButtonNumberChange : MonoBehaviour
{
    public TextMeshPro numberText;
    public int currentScore;

    void Start()
    {
        if(!numberText)numberText = GetComponentInChildren<TextMeshPro>();
        
    }

    public void WriteInConsole()
    {
        Debug.Log("BUTTON PRESSED");
    }
    public void AddToNumber()
    {
        if (currentScore < 9) currentScore += 1;
        else currentScore = 0;

        numberText.text = ""+currentScore;
    }
}
