using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;

public class testZeiger : MonoBehaviour
{
    private bool klZeigerFlag = false;
    private bool grZeigerFlag = false;
    public float zGrZeiger;                                     //Z Rotation from big Pointer
    public float zKlZeiger;                                     //Z Rotation from little Pointer
    public float targetRotationKlein;                           //Target rotation in Degrees for little Pointer
    public float targetRotationGroß;                            //Target rotation in Degrees for big Pointer
    public GameObject großerZeiger;
    public GameObject kleinerZeiger;
    public GameObject glowingNumber;
    public AudioSource confirmSound;
    public Material glowMaterial;

    public float tolleranz;

    // Start is called before the first frame update
    void Start()
    {
        zGrZeiger = großerZeiger.transform.eulerAngles.z;
        zKlZeiger = kleinerZeiger.transform.eulerAngles.z;
    }

    // Update is called once per frame
    void Update()
    {
        zGrZeiger = großerZeiger.transform.eulerAngles.z;
        zKlZeiger = kleinerZeiger.transform.eulerAngles.z;



        if (klZeigerInPosition() && grZeigerInPosition())
        {
            Debug.Log("okaybeide");
            //Deactivate both Pointer if they are positioned right and light up the number for the code
            großerZeiger.GetComponent<ObjectManipulator>().enabled = false;
            kleinerZeiger.GetComponent<ObjectManipulator>().enabled = false;
            glowingNumber.GetComponent<MeshRenderer>().material = glowMaterial;
        }
       
            klZeigerInPosition();
            grZeigerInPosition();
        

    }
    bool klZeigerInPosition()                                                                   //Check if little Pointer is on the right Position
    {
        if(zKlZeiger >= targetRotationKlein - tolleranz && zKlZeiger <= targetRotationKlein + tolleranz)
        {
            if (klZeigerFlag == false)
            {
                Debug.Log("okay klZeiger");
                confirmSound.Play();
                klZeigerFlag = true;
            }
            return true;
        }
        klZeigerFlag = false;
        return false;
    }

    bool grZeigerInPosition()                                                                   //Checks if big Pointer is on the right Position
    {
        if (zGrZeiger >= targetRotationGroß - tolleranz && zGrZeiger <= targetRotationGroß + tolleranz)
        {
            if (grZeigerFlag == false)
            {
                Debug.Log("okay grZeiger");
                confirmSound.Play();
                grZeigerFlag = true;
            }
            return true;
        }
        grZeigerFlag = false;
        return false;
    }
}
