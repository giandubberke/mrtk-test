using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.UI;


public class GemStoneScript : MonoBehaviour
{
    public GameObject gemStone;
    public GameObject boxDeckel;
    public GameObject presentPlait;
    public Transform plaitFinalPos;

    public Transform gemStoneTarget;
    public bool gemStoneTrue = false;
    public float endRotation;
    public AudioSource rickRoll;
    public GameObject vid;

    public AudioSource gemAudio;
    bool gemAudioBool = true;
    [SerializeField] bool audioactive = true;

    private bool vidstarted;

    private void Start()
    {
        vidstarted = true;
        if (!gemStoneTarget) gemStoneTarget = gameObject.transform;
    }


    private void Update()//del later
    {
        Debug.Log(gemStoneTrue);
        if (gemStoneTrue)
        {
            var endRot = Quaternion.Euler(endRotation, boxDeckel.transform.eulerAngles.y, boxDeckel.transform.eulerAngles.z);
            boxDeckel.transform.rotation = Quaternion.Lerp(boxDeckel.transform.rotation, endRot, 0.3f * Time.deltaTime);

            if (vidstarted)
            {
                if (audioactive)
                {
                    rickRoll.PlayDelayed(3.5f);
                    
                }
                vidstarted = false;
            }

            presentPlait.transform.position = Vector3.MoveTowards(presentPlait.transform.position, plaitFinalPos.position, 0.08f * Time.deltaTime);
        }
    }


    private void OnTriggerEnter(Collider collider)
    {
        if (collider.gameObject == gemStone)
        {
            gemStone.transform.position = gemStoneTarget.position;
            gemStone.transform.rotation = gemStoneTarget.rotation;
            gemStone.transform.parent = gemStoneTarget;

            if (gemAudioBool)
            {
                gemAudio.Play();
                gemAudioBool = false;
            }

            Invoke("openBox", 1f);
            gemStone.GetComponent<NearInteractionGrabbable>().enabled = false;
            gemStone.GetComponent<ObjectManipulator>().enabled = false;
        }
    }

    void openBox()
    {
        gemStoneTrue = true;

    }
}
