using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class pipeCheck : MonoBehaviour
{
    public PipeRotator[] pipeRotator;
    public GameObject[] glowingObjects;
    public Material glowMaterial;
    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        allRight();
        Debug.Log("it is: "+ allRight());    
    }

    private bool allRight()
    {
        for (int i = 0; i < pipeRotator.Length; ++i)
        {
            if (pipeRotator[i].rightRotation == false)                                          //Check if the Rotation of all Pipes in PipeRotator are in right Position
            {
                return false;
            }
        }
        foreach (var item in glowingObjects)                                                    //Let all objects assigned to glowingObjects glow
        {
            item.GetComponent<MeshRenderer>().material = glowMaterial;
        }
        return true;
    }
}
