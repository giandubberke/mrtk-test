using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonOrder : MonoBehaviour
{
    public GameObject[] glowObjects;
    public nailPulling nailPulling;
    private void Start()
    {
       if(!nailPulling) nailPulling = GetComponent<nailPulling>();
    }

    public void StartTheGlow()
    {
        nailPulling.ResetNails();
        StartCoroutine(ObjectGlow());  
    }

    IEnumerator ObjectGlow()
    {
        for (int i = 0; i < glowObjects.Length; i++)
        {
            glowObjects[i].GetComponent<MeshRenderer>().material.color = new Color(255, 255, 255);
            yield return new WaitForSeconds(2);
            glowObjects[i].GetComponent<MeshRenderer>().material.color = new Color(0, 0, 0);
        }
    }
}
