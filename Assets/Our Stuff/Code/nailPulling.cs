using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class nailPulling : MonoBehaviour
{
    public bool isSolved = false;
    private bool[] jesusTakenDown;      //Bool Array with which Nail is taken out
    public GameObject[] nails;          //all nails in an Array
    private Vector3[] startPosition;    //Start position from nails
    private int numberOfNails;
    public GameObject fallingPlate;     //Falls down to reveal Number
    public Material startMaterial;
    public Material glowMaterial;

    // Start is called before the first frame update
    void Start()
    {
        //moved = false;
        startPosition = new Vector3[nails.Length];      //assign Array length from nails to Start Position
        numberOfNails = nails.Length;
        jesusTakenDown = new bool[nails.Length];        //assign Array length from nails to Boolean Array

        //Setup Rigidbody for the falling Plate
        fallingPlate.GetComponent<Rigidbody>().isKinematic = true;
        fallingPlate.GetComponent<Rigidbody>().useGravity = false;

        for (int i = 0; i < numberOfNails; i++)
        {
            startPosition[i] = nails[i].transform.position;             //assign all nails a starting position

            //Setup Rigidbody for the nails
            nails[i].GetComponent<Rigidbody>().isKinematic = true;
            nails[i].GetComponent<Rigidbody>().useGravity = false;

        }
    }


    // Update is called once per frame
    void Update()
    {
        for (int i = 0; i < numberOfNails; i++)
        {
            if ((nails[i].transform.position != startPosition[i]))              //Checks if the Nail was moved
            {
                //Activating Gravity for pulled nails
                nails[i].GetComponent<Rigidbody>().isKinematic = false;
                nails[i].GetComponent<Rigidbody>().useGravity = true;
                if (i == 0)                                                     //Checks if it was the first Nail in the Array
                {
                    jesusTakenDown[0] = true;
        
                }
                else if (jesusTakenDown[i - 1] && i > 0)                        //Checks if the nail prior in the Array was pulled
                {
                    jesusTakenDown[i] = true;
                    
                }
                else
                {
                    ResetNails();                                               //Reset all nails to Start position if the wrong nail was pulled
                   
                }

            }
            if (jesusTakenDown[numberOfNails - 1])                              //Checks if the last nail was pulled
            {
                isSolved = true;

                //Activates Rigidbody
                fallingPlate.GetComponent<Rigidbody>().isKinematic = false;
                fallingPlate.GetComponent<Rigidbody>().useGravity = true;

                Invoke("destroyNails", 4f);
            }
        }
    }

    public void ResetNails()
    {
        //Reset everything to start
        for (int i = 0; i < numberOfNails; i++)
        {
            nails[i].transform.position = startPosition[i];
            jesusTakenDown[i] = false;
            nails[i].GetComponent<Rigidbody>().isKinematic = true;
            nails[i].GetComponent<Rigidbody>().useGravity = false;           
        }    
    }

    void destroyNails()
    {
        for (int i = 0; i < numberOfNails; i++)
        {
            nails[i].SetActive(false);
            fallingPlate.SetActive(false);
        }
    }

    public void StartTheGlow()                      //Put every nail to their Start and changes color to white in the order of the nails Array
    {
        ResetNails();
        StartCoroutine(ObjectGlow());
    }

    IEnumerator ObjectGlow()
    {
        for (int i = 0; i < nails.Length; i++)
        {
            nails[i].GetComponentInChildren<MeshRenderer>().material = glowMaterial;
            yield return new WaitForSeconds(2);
            nails[i].GetComponentInChildren<MeshRenderer>().material = startMaterial;
        }
    }
    //IEnumerator deactivateNails(int i)
    //{
    //    yield return new WaitForSeconds(4);
    //    nails[i].SetActive(false);
    //}
}